/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.awt.ColorComboBox
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.options.colors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory;
import org.netbeans.modules.options.colors.CategoryComparator;
import org.netbeans.modules.options.colors.CategoryRenderer;
import org.netbeans.modules.options.colors.ColorComboBoxSupport;
import org.netbeans.modules.options.colors.ColorModel;
import org.netbeans.modules.options.colors.ColorValue;
import org.netbeans.modules.options.colors.spi.FontsColorsController;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.ColorComboBox;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class SyntaxColoringPanel
extends JPanel
implements ActionListener,
PropertyChangeListener,
FontsColorsController,
ItemListener {
    private ColorModel.Preview preview;
    private RequestProcessor.Task selectTask;
    private ColorModel colorModel = null;
    private String currentLanguage;
    private String currentProfile;
    private Map<String, Map<String, Vector<AttributeSet>>> profiles = new HashMap<String, Map<String, Vector<AttributeSet>>>();
    private Map<String, Set<String>> toBeSaved = new HashMap<String, Set<String>>();
    private boolean listen = false;
    private boolean changed = false;
    private static Logger log = Logger.getLogger(SyntaxColoringPanel.class.getName());
    private JButton bFont;
    private JComboBox cbBackground;
    private JComboBox cbEffectColor;
    private JComboBox<String> cbEffects;
    private JComboBox cbForeground;
    private final JComboBox<String> cbLanguage = new JComboBox();
    private JLabel lBackground;
    private JList<AttributeSet> lCategories;
    private JLabel lCategory;
    private JLabel lEffectColor;
    private JLabel lEffects;
    private JLabel lFont;
    private JLabel lForeground;
    private JLabel lLanguage;
    private JLabel lPreview;
    private JPanel pPreview;
    private JScrollPane spCategories;
    private JScrollPane spPreview;
    private JTextField tfFont;
    private final RequestProcessor.Task updatecbLanguage;
    private boolean blink;
    private int blinkSequence;
    private RequestProcessor.Task task;
    private Map<String, String> languageToMimeType;
    private Map<String, Map<String, Vector<AttributeSet>>> defaults;
    private static Map<String, String> convertALC = new HashMap<String, String>();

    public SyntaxColoringPanel() {
        this.updatecbLanguage = new RequestProcessor("SyntaxColoringPanel2").create(new Runnable(){

            @Override
            public void run() {
                final ArrayList<String> languages = new ArrayList<String>(SyntaxColoringPanel.this.colorModel.getLanguages());
                EventQueue.invokeLater(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        languages.remove("text/x-all-languages");
                        Collections.sort(languages, new LanguagesComparator());
                        Iterator it = languages.iterator();
                        JComboBox jComboBox = SyntaxColoringPanel.this.cbLanguage;
                        synchronized (jComboBox) {
                            Object lastLanguage = SyntaxColoringPanel.this.cbLanguage.getSelectedItem();
                            SyntaxColoringPanel.this.cbLanguage.removeAllItems();
                            while (it.hasNext()) {
                                SyntaxColoringPanel.this.cbLanguage.addItem(it.next());
                            }
                            SyntaxColoringPanel.this.listen = true;
                            if (lastLanguage != null) {
                                SyntaxColoringPanel.this.cbLanguage.setSelectedItem(lastLanguage);
                            } else {
                                SyntaxColoringPanel.this.cbLanguage.setSelectedIndex(0);
                            }
                            SyntaxColoringPanel.this.updateLanguageCombobox();
                        }
                    }
                });
            }

        });
        this.blink = true;
        this.blinkSequence = 0;
        this.task = new RequestProcessor("SyntaxColoringPanel").create(new Runnable(){

            @Override
            public void run() {
                if (EventQueue.isDispatchThread()) {
                    SyntaxColoringPanel.this.updatePreview();
                    if (SyntaxColoringPanel.this.blinkSequence == 0) {
                        return;
                    }
                    SyntaxColoringPanel.this.blinkSequence--;
                    SyntaxColoringPanel.this.task.schedule(250);
                } else {
                    EventQueue.invokeLater(this);
                }
            }
        });
        this.defaults = new HashMap<String, Map<String, Vector<AttributeSet>>>();
        this.initComponents();
        this.setName(SyntaxColoringPanel.loc("Syntax_coloring_tab"));
        this.cbLanguage.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Languages"));
        this.cbLanguage.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Languages"));
        this.lCategories.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Categories"));
        this.lCategories.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Categories"));
        this.bFont.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Font"));
        this.bFont.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Font"));
        this.cbForeground.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Foreground_Chooser"));
        this.cbForeground.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Foreground_Chooser"));
        this.cbBackground.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Background_Chooser"));
        this.cbBackground.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Background_Chooser"));
        this.cbEffects.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Efects_Color_Chooser"));
        this.cbEffects.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Efects_Color_Chooser"));
        this.cbEffectColor.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Efects_Color"));
        this.cbEffectColor.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Efects_Color"));
        this.cbLanguage.addActionListener(this);
        this.cbLanguage.addKeyListener((KeyListener)new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent evt) {
                if (evt.getKeyCode() == 10 || evt.getKeyCode() == 32) {
                    SyntaxColoringPanel.this.updateLanguageCombobox();
                }
            }
        });
        this.lCategories.setSelectionMode(0);
        this.lCategories.setVisibleRowCount(3);
        this.lCategories.setCellRenderer(new CategoryRenderer());
        this.lCategories.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!SyntaxColoringPanel.this.listen) {
                    return;
                }
                SyntaxColoringPanel.this.selectTask.schedule(200);
            }
        });
        this.lCategories.setSelectedIndex(0);
        this.tfFont.setEditable(false);
        this.bFont.addActionListener(this);
        this.bFont.setMargin(new Insets(0, 0, 0, 0));
        this.cbForeground.addItemListener(this);
        this.cbBackground.addItemListener(this);
        this.cbEffects.addItem(SyntaxColoringPanel.loc("CTL_Effects_None"));
        this.cbEffects.addItem(SyntaxColoringPanel.loc("CTL_Effects_Underlined"));
        this.cbEffects.addItem(SyntaxColoringPanel.loc("CTL_Effects_Wave_Underlined"));
        this.cbEffects.addItem(SyntaxColoringPanel.loc("CTL_Effects_Strike_Through"));
        this.cbEffects.getAccessibleContext().setAccessibleName(SyntaxColoringPanel.loc("AN_Effects"));
        this.cbEffects.getAccessibleContext().setAccessibleDescription(SyntaxColoringPanel.loc("AD_Effects"));
        this.cbEffects.addActionListener(this);
        this.cbEffectColor.addItemListener(this);
        SyntaxColoringPanel.loc(this.bFont, "CTL_Font_button");
        SyntaxColoringPanel.loc(this.lBackground, "CTL_Background_label");
        SyntaxColoringPanel.loc(this.lCategory, "CTL_Category");
        SyntaxColoringPanel.loc(this.lEffectColor, "CTL_Effects_color");
        SyntaxColoringPanel.loc(this.lEffects, "CTL_Effects_label");
        SyntaxColoringPanel.loc(this.lFont, "CTL_Font");
        SyntaxColoringPanel.loc(this.lForeground, "CTL_Foreground_label");
        SyntaxColoringPanel.loc(this.lLanguage, "CTL_Languages");
        SyntaxColoringPanel.loc(this.lPreview, "CTL_Preview");
        this.selectTask = new RequestProcessor("SyntaxColoringPanel1").create(new Runnable(){

            @Override
            public void run() {
                if (EventQueue.isDispatchThread()) {
                    SyntaxColoringPanel.this.refreshUI();
                    if (!SyntaxColoringPanel.this.blink) {
                        return;
                    }
                    SyntaxColoringPanel.this.startBlinking();
                } else {
                    EventQueue.invokeLater(this);
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateLanguageCombobox() {
        if (!this.listen) {
            return;
        }
        JComboBox<String> jComboBox = this.cbLanguage;
        synchronized (jComboBox) {
            this.setCurrentLanguage((String)this.cbLanguage.getSelectedItem());
        }
        this.fireChanged();
    }

    private void initComponents() {
        this.lLanguage = new JLabel();
        this.lCategory = new JLabel();
        this.spCategories = new JScrollPane();
        this.lCategories = new JList();
        this.lPreview = new JLabel();
        this.spPreview = new JScrollPane();
        this.pPreview = new JPanel();
        this.lFont = new JLabel();
        this.lForeground = new JLabel();
        this.lBackground = new JLabel();
        this.lEffects = new JLabel();
        this.lEffectColor = new JLabel();
        this.cbForeground = new ColorComboBox();
        this.cbBackground = new ColorComboBox();
        this.cbEffects = new JComboBox();
        this.cbEffectColor = new ColorComboBox();
        this.tfFont = new JTextField();
        this.bFont = new JButton();
        this.lLanguage.setLabelFor(this.cbLanguage);
        this.lLanguage.setText("Language:");
        this.lCategory.setLabelFor(this.lCategories);
        this.lCategory.setText("Category:");
        this.spCategories.setViewportView(this.lCategories);
        this.lPreview.setText("Preview:");
        this.spPreview.setBorder(BorderFactory.createEtchedBorder());
        this.spPreview.setAutoscrolls(true);
        this.pPreview.setAutoscrolls(true);
        this.pPreview.setLayout(new BorderLayout());
        this.spPreview.setViewportView(this.pPreview);
        this.lFont.setLabelFor(this.bFont);
        this.lFont.setText("Font:");
        this.lForeground.setLabelFor(this.cbForeground);
        this.lForeground.setText("Foreground:");
        this.lBackground.setLabelFor(this.cbBackground);
        this.lBackground.setText("Background:");
        this.lEffects.setLabelFor(this.cbEffects);
        this.lEffects.setText("Effects:");
        this.lEffectColor.setLabelFor(this.cbEffectColor);
        this.lEffectColor.setText("Effect Color:");
        this.bFont.setText("...");
        this.bFont.setMargin(new Insets(2, 2, 2, 2));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.spPreview, -1, 425, 32767).addGroup(layout.createSequentialGroup().addComponent(this.lLanguage).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbLanguage, -2, -1, -2)).addComponent(this.lCategory).addGroup(layout.createSequentialGroup().addComponent(this.spCategories, -1, 201, 32767).addGap(20, 20, 20).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lEffectColor).addComponent(this.lForeground).addComponent(this.lFont).addComponent(this.lEffects).addComponent(this.lBackground)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.tfFont, -1, 69, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.bFont)).addComponent(this.cbForeground, 0, 97, 32767).addComponent(this.cbBackground, 0, 97, 32767).addComponent(this.cbEffects, 0, 97, 32767).addComponent(this.cbEffectColor, 0, 97, 32767))).addComponent(this.lPreview)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lLanguage).addComponent(this.cbLanguage, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lCategory).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.spCategories, -2, 130, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lPreview)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lFont).addComponent(this.tfFont, -2, -1, -2).addComponent(this.bFont)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lForeground).addComponent(this.cbForeground, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lBackground).addComponent(this.cbBackground, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lEffects).addComponent(this.cbEffects, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lEffectColor).addComponent(this.cbEffectColor, -2, -1, -2)))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.spPreview, -1, 153, 32767).addContainerGap()));
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (!this.listen) {
            return;
        }
        if (evt.getSource() == this.cbEffects) {
            if (this.cbEffects.getSelectedIndex() == 0) {
                this.cbEffectColor.setSelectedItem(null);
            }
            this.cbEffectColor.setEnabled(this.cbEffects.getSelectedIndex() > 0);
            this.updateData();
        } else if (evt.getSource() == this.cbLanguage) {
            if (evt.getModifiers() == 16) {
                this.updateLanguageCombobox();
            }
        } else if (evt.getSource() == this.bFont) {
            PropertyEditor pe = PropertyEditorManager.findEditor(Font.class);
            AttributeSet category = this.getCurrentCategory();
            if (category == null) {
                return;
            }
            Font f = this.getFont(category);
            pe.setValue(f);
            DialogDescriptor dd = new DialogDescriptor((Object)pe.getCustomEditor(), SyntaxColoringPanel.loc("CTL_Font_Chooser"));
            dd.setOptions(new Object[]{DialogDescriptor.OK_OPTION, SyntaxColoringPanel.loc("CTL_Font_Inherited"), DialogDescriptor.CANCEL_OPTION});
            DialogDisplayer.getDefault().createDialog(dd).setVisible(true);
            if (dd.getValue() == DialogDescriptor.OK_OPTION) {
                f = (Font)pe.getValue();
                category = this.modifyFont(category, f);
                this.replaceCurrrentCategory(category);
                this.setToBeSaved(this.currentProfile, this.currentLanguage);
                this.refreshUI();
            } else if (dd.getValue().equals(SyntaxColoringPanel.loc("CTL_Font_Inherited"))) {
                String fontName = (String)this.getDefault(this.currentLanguage, category, StyleConstants.FontFamily);
                int style = 0;
                if (Boolean.TRUE.equals(this.getDefault(this.currentLanguage, category, StyleConstants.Bold))) {
                    ++style;
                }
                if (Boolean.TRUE.equals(this.getDefault(this.currentLanguage, category, StyleConstants.Italic))) {
                    style += 2;
                }
                Integer size = (Integer)this.getDefault(this.currentLanguage, category, StyleConstants.FontSize);
                assert (size != null);
                f = new Font(fontName, style, size);
                category = this.modifyFont(category, f);
                this.replaceCurrrentCategory(category);
                this.setToBeSaved(this.currentProfile, this.currentLanguage);
                this.refreshUI();
            }
        } else if (evt.getSource() instanceof JComboBox) {
            this.updateData();
        }
        this.fireChanged();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (!this.listen || evt.getPropertyName() == null) {
            return;
        }
        if ("currentAElement".equals(evt.getPropertyName())) {
            String converted;
            String currentCategory = (String)evt.getNewValue();
            Vector<AttributeSet> categories = this.getCategories(this.currentProfile, this.currentLanguage);
            if (this.currentLanguage.equals(ColorModel.ALL_LANGUAGES) && (converted = convertALC.get(currentCategory)) != null) {
                currentCategory = converted;
            }
            for (int i = 0; i < categories.size(); ++i) {
                AttributeSet as = categories.get(i);
                if (!currentCategory.equals(as.getAttribute(StyleConstants.NameAttribute))) continue;
                this.blink = false;
                this.lCategories.setSelectedIndex(i);
                this.lCategories.ensureIndexIsVisible(i);
                this.blink = true;
                break;
            }
        }
    }

    @Override
    public void update(ColorModel colorModel) {
        this.colorModel = colorModel;
        this.currentProfile = colorModel.getCurrentProfile();
        this.currentLanguage = ColorModel.ALL_LANGUAGES;
        if (this.preview != null) {
            this.preview.removePropertyChangeListener("currentAElement", this);
        }
        Component component = colorModel.getSyntaxColoringPreviewComponent(this.currentLanguage);
        this.preview = (ColorModel.Preview)component;
        this.pPreview.removeAll();
        this.pPreview.add("Center", component);
        this.preview.addPropertyChangeListener("currentAElement", this);
        this.listen = false;
        this.updatecbLanguage.schedule(20);
        this.changed = false;
    }

    @Override
    public void cancel() {
        this.toBeSaved = new HashMap<String, Set<String>>();
        this.profiles = new HashMap<String, Map<String, Vector<AttributeSet>>>();
        this.changed = false;
    }

    @Override
    public void applyChanges() {
        if (this.colorModel == null) {
            return;
        }
        for (String profile : this.toBeSaved.keySet()) {
            Set<String> toBeSavedLanguages = this.toBeSaved.get(profile);
            Map<String, Vector<AttributeSet>> schemeMap = this.profiles.get(profile);
            for (String languageName : toBeSavedLanguages) {
                this.colorModel.setCategories(profile, languageName, (Collection)schemeMap.get(languageName));
            }
        }
        this.toBeSaved = new HashMap<String, Set<String>>();
        this.profiles = new HashMap<String, Map<String, Vector<AttributeSet>>>();
        this.changed = false;
    }

    @Override
    public boolean isChanged() {
        return this.changed;
    }

    @Override
    public void setCurrentProfile(String currentProfile) {
        String oldProfile = this.currentProfile;
        this.currentProfile = currentProfile;
        if (!this.colorModel.getProfiles().contains(currentProfile) && !this.profiles.containsKey(currentProfile)) {
            this.cloneScheme(oldProfile, currentProfile);
        }
        Vector<AttributeSet> categories = this.getCategories(currentProfile, this.currentLanguage);
        this.lCategories.setListData(categories);
        this.blink = false;
        this.lCategories.setSelectedIndex(0);
        this.blink = true;
        this.refreshUI();
        this.fireChanged();
    }

    @Override
    public void deleteProfile(String profile) {
        HashMap<String, Vector<AttributeSet>> m = new HashMap<String, Vector<AttributeSet>>();
        boolean custom = this.colorModel.isCustomProfile(profile);
        for (String language : this.colorModel.getLanguages()) {
            if (custom) {
                m.put(language, null);
                continue;
            }
            m.put(language, this.getDefaults(profile, language));
        }
        this.profiles.put(profile, m);
        this.toBeSaved.put(profile, new HashSet<String>(this.colorModel.getLanguages()));
        if (!custom) {
            this.refreshUI();
        }
        this.fireChanged();
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    private void cloneScheme(String oldScheme, String newScheme) {
        HashMap m = new HashMap();
        for (String language : this.colorModel.getLanguages()) {
            Vector<AttributeSet> v = this.getCategories(oldScheme, language);
            Vector<SimpleAttributeSet> newV = new Vector<SimpleAttributeSet>();
            for (AttributeSet attributeSet : v) {
                newV.add(new SimpleAttributeSet(attributeSet));
            }
            m.put(language, new Vector(newV));
            this.setToBeSaved(newScheme, language);
        }
        this.profiles.put(newScheme, m);
    }

    Collection<AttributeSet> getAllLanguages() {
        return this.getCategories(this.currentProfile, ColorModel.ALL_LANGUAGES);
    }

    Collection<AttributeSet> getSyntaxColorings() {
        return this.getCategories(this.currentProfile, this.currentLanguage);
    }

    private void setCurrentLanguage(String language) {
        if (language == null) {
            return;
        }
        this.currentLanguage = language;
        this.blink = false;
        this.lCategories.setListData(this.getCategories(this.currentProfile, this.currentLanguage));
        this.lCategories.setSelectedIndex(0);
        this.blink = true;
        this.refreshUI();
    }

    private static String loc(String key) {
        return NbBundle.getMessage(SyntaxColoringPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)SyntaxColoringPanel.loc(key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)SyntaxColoringPanel.loc(key));
        }
    }

    private void updateData() {
        int i = this.lCategories.getSelectedIndex();
        if (i < 0) {
            return;
        }
        AttributeSet category = this.getCurrentCategory();
        Color underline = null;
        Color wave = null;
        Color strikethrough = null;
        if (this.cbEffects.getSelectedIndex() == 1) {
            underline = ((ColorComboBox)this.cbEffectColor).getSelectedColor();
        }
        if (this.cbEffects.getSelectedIndex() == 2) {
            wave = ((ColorComboBox)this.cbEffectColor).getSelectedColor();
        }
        if (this.cbEffects.getSelectedIndex() == 3) {
            strikethrough = ((ColorComboBox)this.cbEffectColor).getSelectedColor();
        }
        SimpleAttributeSet c = category != null ? new SimpleAttributeSet(category) : new SimpleAttributeSet();
        Color color = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbBackground);
        if (color != null) {
            c.addAttribute(StyleConstants.Background, color);
        } else {
            c.removeAttribute(StyleConstants.Background);
        }
        color = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbForeground);
        if (color != null) {
            c.addAttribute(StyleConstants.Foreground, color);
        } else {
            c.removeAttribute(StyleConstants.Foreground);
        }
        if (underline != null) {
            c.addAttribute(StyleConstants.Underline, underline);
        } else {
            c.removeAttribute(StyleConstants.Underline);
        }
        if (strikethrough != null) {
            c.addAttribute(StyleConstants.StrikeThrough, strikethrough);
        } else {
            c.removeAttribute(StyleConstants.StrikeThrough);
        }
        if (wave != null) {
            c.addAttribute(EditorStyleConstants.WaveUnderlineColor, wave);
        } else {
            c.removeAttribute(EditorStyleConstants.WaveUnderlineColor);
        }
        this.replaceCurrrentCategory(c);
        this.setToBeSaved(this.currentProfile, this.currentLanguage);
        this.updatePreview();
    }

    private void startBlinking() {
        this.blinkSequence = 5;
        this.task.schedule(0);
    }

    private void updatePreview() {
        Collection<AttributeSet> syntaxColorings = this.getSyntaxColorings();
        Collection<AttributeSet> allLanguages = this.getAllLanguages();
        if (this.blinkSequence % 2 == 1) {
            if (ColorModel.ALL_LANGUAGES.equals(this.currentLanguage)) {
                allLanguages = this.invertCategory(allLanguages, this.getCurrentCategory());
            } else {
                syntaxColorings = this.invertCategory(syntaxColorings, this.getCurrentCategory());
            }
        }
        this.preview.setParameters(this.currentLanguage, allLanguages, Collections.<AttributeSet>emptySet(), syntaxColorings);
    }

    private Collection<AttributeSet> invertCategory(Collection<AttributeSet> c, AttributeSet category) {
        if (category == null) {
            return c;
        }
        ArrayList<AttributeSet> result = new ArrayList<AttributeSet>(c);
        int i = result.indexOf(category);
        SimpleAttributeSet as = new SimpleAttributeSet(category);
        Color highlight = (Color)this.getValue(this.currentLanguage, category, StyleConstants.Background);
        if (highlight == null) {
            return result;
        }
        Color newColor = new Color(255 - highlight.getRed(), 255 - highlight.getGreen(), 255 - highlight.getBlue());
        as.addAttribute(StyleConstants.Underline, newColor);
        result.set(i, as);
        return result;
    }

    private void refreshUI() {
        this.listen = false;
        AttributeSet category = this.getCurrentCategory();
        if (category == null) {
            this.tfFont.setText("");
            this.bFont.setEnabled(false);
            this.cbEffects.setEnabled(false);
            this.cbForeground.setEnabled(false);
            ((ColorComboBox)this.cbForeground).setSelectedColor(null);
            this.cbBackground.setEnabled(false);
            this.cbBackground.setSelectedItem(new ColorValue(null, null));
            this.cbEffectColor.setEnabled(false);
            this.cbEffectColor.setSelectedItem(new ColorValue(null, null));
            this.updatePreview();
            return;
        }
        this.bFont.setEnabled(true);
        this.cbEffects.setEnabled(true);
        this.cbForeground.setEnabled(true);
        this.cbBackground.setEnabled(true);
        Color inheritedForeground = (Color)this.getDefault(this.currentLanguage, category, StyleConstants.Foreground);
        if (inheritedForeground == null) {
            inheritedForeground = Color.black;
        }
        ColorComboBoxSupport.setInheritedColor((ColorComboBox)this.cbForeground, inheritedForeground);
        Color inheritedBackground = (Color)this.getDefault(this.currentLanguage, category, StyleConstants.Background);
        if (inheritedBackground == null) {
            inheritedBackground = Color.white;
        }
        ColorComboBoxSupport.setInheritedColor((ColorComboBox)this.cbBackground, inheritedBackground);
        String font = this.fontToString(category);
        this.tfFont.setText(font);
        ColorComboBoxSupport.setSelectedColor((ColorComboBox)this.cbForeground, (Color)category.getAttribute(StyleConstants.Foreground));
        ColorComboBoxSupport.setSelectedColor((ColorComboBox)this.cbBackground, (Color)category.getAttribute(StyleConstants.Background));
        if (category.getAttribute(StyleConstants.Underline) != null) {
            this.cbEffects.setSelectedIndex(1);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)category.getAttribute(StyleConstants.Underline));
        } else if (category.getAttribute(EditorStyleConstants.WaveUnderlineColor) != null) {
            this.cbEffects.setSelectedIndex(2);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)category.getAttribute(EditorStyleConstants.WaveUnderlineColor));
        } else if (category.getAttribute(StyleConstants.StrikeThrough) != null) {
            this.cbEffects.setSelectedIndex(3);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)category.getAttribute(StyleConstants.StrikeThrough));
        } else if (this.getDefault(this.currentLanguage, category, StyleConstants.Underline) != null) {
            this.cbEffects.setSelectedIndex(1);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)this.getDefault(this.currentLanguage, category, StyleConstants.Underline));
        } else if (this.getDefault(this.currentLanguage, category, EditorStyleConstants.WaveUnderlineColor) != null) {
            this.cbEffects.setSelectedIndex(2);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)this.getDefault(this.currentLanguage, category, EditorStyleConstants.WaveUnderlineColor));
        } else if (this.getDefault(this.currentLanguage, category, StyleConstants.StrikeThrough) != null) {
            this.cbEffects.setSelectedIndex(3);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)this.getDefault(this.currentLanguage, category, StyleConstants.StrikeThrough));
        } else {
            this.cbEffects.setSelectedIndex(0);
            this.cbEffectColor.setEnabled(false);
            this.cbEffectColor.setSelectedIndex(-1);
        }
        this.updatePreview();
        this.listen = true;
    }

    private void setToBeSaved(String currentProfile, String currentLanguage) {
        Set<String> s = this.toBeSaved.get(currentProfile);
        if (s == null) {
            s = new HashSet<String>();
            this.toBeSaved.put(currentProfile, s);
        }
        s.add(currentLanguage);
    }

    private void fireChanged() {
        boolean isChanged = false;
        for (String profile : this.toBeSaved.keySet()) {
            Set<String> toBeSavedLanguages = this.toBeSaved.get(profile);
            Map<String, Vector<AttributeSet>> schemeMap = this.profiles.get(profile);
            for (String languageName : toBeSavedLanguages) {
                String[] mimePath = this.getMimePath(languageName);
                if (mimePath == null) continue;
                FontColorSettingsFactory fcs = EditorSettings.getDefault().getFontColorSettings(mimePath);
                Collection allFontColors = fcs.getAllFontColors(profile);
                Map<String, AttributeSet> savedSyntax = this.toMap(allFontColors);
                Vector<AttributeSet> attributeSet = schemeMap.get(languageName);
                Map<String, AttributeSet> currentSyntax = this.toMap(attributeSet);
                if (savedSyntax == null || currentSyntax == null) continue;
                if (savedSyntax.size() >= currentSyntax.size()) {
                    isChanged |= this.checkMaps(languageName, savedSyntax, currentSyntax);
                    continue;
                }
                isChanged |= this.checkMaps(languageName, currentSyntax, savedSyntax);
            }
        }
        this.changed = isChanged;
    }

    private boolean checkMaps(String languageName, Map<String, AttributeSet> savedMap, Map<String, AttributeSet> currentMap) {
        boolean isChanged = false;
        for (String name : savedMap.keySet()) {
            AttributeSet savedAS;
            AttributeSet currentAS;
            if (!currentMap.containsKey(name) || !(isChanged |= this.isFontChanged(languageName, currentAS = currentMap.get(name), savedAS = savedMap.get(name)) || (Color)currentAS.getAttribute(StyleConstants.Foreground) != (Color)savedAS.getAttribute(StyleConstants.Foreground) || (Color)currentAS.getAttribute(StyleConstants.Background) != (Color)savedAS.getAttribute(StyleConstants.Background) || (Color)currentAS.getAttribute(StyleConstants.Underline) != (Color)savedAS.getAttribute(StyleConstants.Underline) || (Color)currentAS.getAttribute(StyleConstants.StrikeThrough) != (Color)savedAS.getAttribute(StyleConstants.StrikeThrough) || (Color)currentAS.getAttribute(EditorStyleConstants.WaveUnderlineColor) != (Color)savedAS.getAttribute(EditorStyleConstants.WaveUnderlineColor))) continue;
            return true;
        }
        return isChanged;
    }

    private boolean isFontChanged(String language, AttributeSet currentAS, AttributeSet savedAS) {
        Font savedFont;
        Boolean italic;
        int style;
        String name = (String)this.getValue(language, currentAS, StyleConstants.FontFamily);
        assert (name != null);
        Integer size = (Integer)this.getValue(language, currentAS, StyleConstants.FontSize);
        assert (size != null);
        Boolean bold = (Boolean)this.getValue(language, currentAS, StyleConstants.Bold);
        if (bold == null) {
            bold = Boolean.FALSE;
        }
        if ((italic = (Boolean)this.getValue(language, currentAS, StyleConstants.Italic)) == null) {
            italic = Boolean.FALSE;
        }
        int n = style = bold != false ? 1 : 0;
        if (italic.booleanValue()) {
            style += 2;
        }
        Font currentFont = new Font(name, style, size);
        name = (String)this.getValue(language, savedAS, StyleConstants.FontFamily);
        assert (name != null);
        size = (Integer)this.getValue(language, savedAS, StyleConstants.FontSize);
        assert (size != null);
        bold = (Boolean)this.getValue(language, savedAS, StyleConstants.Bold);
        if (bold == null) {
            bold = Boolean.FALSE;
        }
        if ((italic = (Boolean)this.getValue(language, savedAS, StyleConstants.Italic)) == null) {
            italic = Boolean.FALSE;
        }
        int n2 = style = bold != false ? 1 : 0;
        if (italic.booleanValue()) {
            style += 2;
        }
        return !currentFont.equals(savedFont = new Font(name, style, size));
    }

    private String[] getMimePath(String language) {
        if (language.equals(ColorModel.ALL_LANGUAGES)) {
            return new String[0];
        }
        String mimeType = this.getLanguageToMimeTypeMap().get(language);
        if (mimeType == null) {
            log.log(Level.WARNING, "Invalid language ''{0}''", language);
            return null;
        }
        return new String[]{mimeType};
    }

    private Map<String, String> getLanguageToMimeTypeMap() {
        if (this.languageToMimeType == null) {
            this.languageToMimeType = new HashMap<String, String>();
            Set mimeTypes = EditorSettings.getDefault().getMimeTypes();
            for (String mimeType : mimeTypes) {
                String name = EditorSettings.getDefault().getLanguageName(mimeType);
                if (name.equals(mimeType)) continue;
                this.languageToMimeType.put(name, mimeType);
            }
            this.languageToMimeType.put(ColorModel.ALL_LANGUAGES, "Defaults");
        }
        return this.languageToMimeType;
    }

    private Map<String, AttributeSet> toMap(Collection<AttributeSet> categories) {
        if (categories == null) {
            return null;
        }
        HashMap<String, AttributeSet> result = new HashMap<String, AttributeSet>();
        for (AttributeSet as : categories) {
            result.put((String)as.getAttribute(StyleConstants.NameAttribute), as);
        }
        return result;
    }

    private Vector<AttributeSet> getCategories(String profile, String language) {
        Vector<AttributeSet> v;
        if (this.colorModel == null) {
            return null;
        }
        Map<String, Vector<AttributeSet>> m = this.profiles.get(profile);
        if (m == null) {
            m = new HashMap<String, Vector<AttributeSet>>();
            this.profiles.put(profile, m);
        }
        if ((v = m.get(language)) == null) {
            Collection<AttributeSet> c = this.colorModel.getCategories(profile, language);
            if (c == null) {
                c = Collections.emptySet();
            }
            ArrayList<AttributeSet> l = new ArrayList<AttributeSet>(c);
            Collections.sort(l, new CategoryComparator());
            v = new Vector<AttributeSet>(l);
            m.put(language, v);
        }
        return v;
    }

    private Vector<AttributeSet> getDefaults(String profile, String language) {
        Vector<AttributeSet> v;
        Map<String, Vector<AttributeSet>> m = this.defaults.get(profile);
        if (m == null) {
            m = new HashMap<String, Vector<AttributeSet>>();
            this.defaults.put(profile, m);
        }
        if ((v = m.get(language)) == null) {
            Collection<AttributeSet> c = this.colorModel.getDefaults(profile, language);
            ArrayList<AttributeSet> l = new ArrayList<AttributeSet>(c);
            Collections.sort(l, new CategoryComparator());
            v = new Vector<AttributeSet>(l);
            m.put(language, v);
        }
        return new Vector<AttributeSet>(v);
    }

    private AttributeSet getCurrentCategory() {
        int i = this.lCategories.getSelectedIndex();
        Vector<AttributeSet> c = this.getCategories(this.currentProfile, this.currentLanguage);
        return i >= 0 && i < c.size() ? c.get(i) : null;
    }

    private void replaceCurrrentCategory(AttributeSet newValues) {
        int i = this.lCategories.getSelectedIndex();
        this.getCategories(this.currentProfile, this.currentLanguage).set(i, newValues);
    }

    private AttributeSet getCategory(String profile, String language, String name) {
        Vector<AttributeSet> v = this.getCategories(profile, language);
        for (AttributeSet c : v) {
            if (!c.getAttribute(StyleConstants.NameAttribute).equals(name)) continue;
            return c;
        }
        return null;
    }

    private Object getValue(String language, AttributeSet category, Object key) {
        if (category.isDefined(key)) {
            return category.getAttribute(key);
        }
        return this.getDefault(language, category, key);
    }

    private Object getDefault(String language, AttributeSet category, Object key) {
        AttributeSet defaultAS;
        String name = (String)category.getAttribute(EditorStyleConstants.Default);
        if (name == null) {
            name = "default";
        }
        if (!name.equals(category.getAttribute(StyleConstants.NameAttribute)) && (defaultAS = this.getCategory(this.currentProfile, language, name)) != null) {
            return this.getValue(language, defaultAS, key);
        }
        if (!language.equals(ColorModel.ALL_LANGUAGES) && (defaultAS = this.getCategory(this.currentProfile, ColorModel.ALL_LANGUAGES, name)) != null) {
            return this.getValue(ColorModel.ALL_LANGUAGES, defaultAS, key);
        }
        if (key == StyleConstants.FontFamily || key == StyleConstants.FontSize) {
            FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FontColorSettings.class);
            return fcs.getFontColors("default").getAttribute(key);
        }
        return null;
    }

    private Font getFont(AttributeSet category) {
        Boolean italic;
        int style;
        String name = (String)this.getValue(this.currentLanguage, category, StyleConstants.FontFamily);
        assert (name != null);
        Integer size = (Integer)this.getValue(this.currentLanguage, category, StyleConstants.FontSize);
        assert (size != null);
        Boolean bold = (Boolean)this.getValue(this.currentLanguage, category, StyleConstants.Bold);
        if (bold == null) {
            bold = Boolean.FALSE;
        }
        if ((italic = (Boolean)this.getValue(this.currentLanguage, category, StyleConstants.Italic)) == null) {
            italic = Boolean.FALSE;
        }
        int n = style = bold != false ? 1 : 0;
        if (italic.booleanValue()) {
            style += 2;
        }
        return new Font(name, style, size);
    }

    private AttributeSet modifyFont(AttributeSet category, Font f) {
        String fontName = f.getName();
        Integer fontSize = new Integer(f.getSize());
        Boolean bold = f.isBold();
        Boolean italic = f.isItalic();
        boolean isDefault = "default".equals(category.getAttribute(StyleConstants.NameAttribute));
        if (fontName.equals(this.getDefault(this.currentLanguage, category, StyleConstants.FontFamily)) && !isDefault) {
            fontName = null;
        }
        if (fontSize.equals(this.getDefault(this.currentLanguage, category, StyleConstants.FontSize)) && !isDefault) {
            fontSize = null;
        }
        if (bold.equals(this.getDefault(this.currentLanguage, category, StyleConstants.Bold))) {
            bold = null;
        } else if (bold.equals(Boolean.FALSE) && this.getDefault(this.currentLanguage, category, StyleConstants.Bold) == null) {
            bold = null;
        }
        if (italic.equals(this.getDefault(this.currentLanguage, category, StyleConstants.Italic))) {
            italic = null;
        } else if (italic.equals(Boolean.FALSE) && this.getDefault(this.currentLanguage, category, StyleConstants.Italic) == null) {
            italic = null;
        }
        SimpleAttributeSet c = new SimpleAttributeSet(category);
        if (fontName != null) {
            c.addAttribute(StyleConstants.FontFamily, fontName);
        } else {
            c.removeAttribute(StyleConstants.FontFamily);
        }
        if (fontSize != null) {
            c.addAttribute(StyleConstants.FontSize, fontSize);
        } else {
            c.removeAttribute(StyleConstants.FontSize);
        }
        if (bold != null) {
            c.addAttribute(StyleConstants.Bold, bold);
        } else {
            c.removeAttribute(StyleConstants.Bold);
        }
        if (italic != null) {
            c.addAttribute(StyleConstants.Italic, italic);
        } else {
            c.removeAttribute(StyleConstants.Italic);
        }
        return c;
    }

    private String fontToString(AttributeSet category) {
        if ("default".equals(category.getAttribute(StyleConstants.NameAttribute))) {
            Boolean italic;
            StringBuffer sb = new StringBuffer();
            sb.append(this.getValue(this.currentLanguage, category, StyleConstants.FontFamily));
            sb.append(' ');
            sb.append(this.getValue(this.currentLanguage, category, StyleConstants.FontSize));
            Boolean bold = (Boolean)this.getValue(this.currentLanguage, category, StyleConstants.Bold);
            if (bold != null && bold.booleanValue()) {
                sb.append(' ').append(SyntaxColoringPanel.loc("CTL_Bold"));
            }
            if ((italic = (Boolean)this.getValue(this.currentLanguage, category, StyleConstants.Italic)) != null && italic.booleanValue()) {
                sb.append(' ').append(SyntaxColoringPanel.loc("CTL_Italic"));
            }
            return sb.toString();
        }
        boolean def = false;
        StringBuffer sb = new StringBuffer();
        if (category.getAttribute(StyleConstants.FontFamily) != null) {
            sb.append('+').append(category.getAttribute(StyleConstants.FontFamily));
        } else {
            def = true;
        }
        if (category.getAttribute(StyleConstants.FontSize) != null) {
            sb.append('+').append(category.getAttribute(StyleConstants.FontSize));
        } else {
            def = true;
        }
        if (Boolean.TRUE.equals(category.getAttribute(StyleConstants.Bold))) {
            sb.append('+').append(SyntaxColoringPanel.loc("CTL_Bold"));
        }
        if (Boolean.FALSE.equals(category.getAttribute(StyleConstants.Bold))) {
            sb.append('-').append(SyntaxColoringPanel.loc("CTL_Bold"));
        }
        if (Boolean.TRUE.equals(category.getAttribute(StyleConstants.Italic))) {
            sb.append('+').append(SyntaxColoringPanel.loc("CTL_Italic"));
        }
        if (Boolean.FALSE.equals(category.getAttribute(StyleConstants.Italic))) {
            sb.append('-').append(SyntaxColoringPanel.loc("CTL_Italic"));
        }
        if (def) {
            sb.insert(0, SyntaxColoringPanel.loc("CTL_Inherited"));
            return sb.toString();
        }
        String result = sb.toString();
        return result.replace('+', ' ');
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == 2 || !this.listen) {
            return;
        }
        this.updateData();
        this.fireChanged();
    }

    static {
        convertALC.put("character", "char");
        convertALC.put("errors", "error");
        convertALC.put("literal", "keyword");
        convertALC.put("keyword-directive", "keyword");
    }

    private static final class LanguagesComparator
    implements Comparator<String> {
        private LanguagesComparator() {
        }

        @Override
        public int compare(String o1, String o2) {
            if (o1.equals(ColorModel.ALL_LANGUAGES)) {
                return o2.equals(ColorModel.ALL_LANGUAGES) ? 0 : -1;
            }
            if (o2.equals(ColorModel.ALL_LANGUAGES)) {
                return 1;
            }
            return o1.compareTo(o2);
        }
    }

}

