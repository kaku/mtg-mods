/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.modules.options.colors;

import java.util.Collection;
import java.util.EnumSet;
import org.netbeans.modules.options.colors.AllLanguagesLexer;
import org.netbeans.modules.options.colors.AllLanguagesTokenId;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class AllLanguageHierarchy
extends LanguageHierarchy<AllLanguagesTokenId> {
    protected synchronized Collection<AllLanguagesTokenId> createTokenIds() {
        return EnumSet.allOf(AllLanguagesTokenId.class);
    }

    protected Lexer<AllLanguagesTokenId> createLexer(LexerRestartInfo<AllLanguagesTokenId> info) {
        return new AllLanguagesLexer(info);
    }

    protected String mimeType() {
        return "text/x-all-languages";
    }
}

