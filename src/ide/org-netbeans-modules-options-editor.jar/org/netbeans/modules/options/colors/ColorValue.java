/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.options.colors.ColorComboBoxSupport;
import org.openide.util.NbBundle;

class ColorValue {
    public static final ColorValue CUSTOM_COLOR = new ColorValue(ColorValue.loc("Custom"), null);
    private static Map<Color, String> colorMap = new HashMap<Color, String>();
    String text;
    Color color;

    ColorValue(Color color) {
        this.color = color;
        this.text = colorMap.get(color);
        if (this.text != null) {
            return;
        }
        StringBuffer sb = new StringBuffer();
        sb.append('[').append(color.getRed()).append(',').append(color.getGreen()).append(',').append(color.getBlue()).append(']');
        this.text = sb.toString();
    }

    ColorValue(String text, Color color) {
        this.text = text;
        this.color = color;
    }

    private static String loc(String key) {
        return NbBundle.getMessage(ColorComboBoxSupport.class, (String)key);
    }

    static {
        colorMap.put(Color.BLACK, ColorValue.loc("Black"));
        colorMap.put(Color.BLUE, ColorValue.loc("Blue"));
        colorMap.put(Color.CYAN, ColorValue.loc("Cyan"));
        colorMap.put(Color.DARK_GRAY, ColorValue.loc("Dark_Gray"));
        colorMap.put(Color.GRAY, ColorValue.loc("Gray"));
        colorMap.put(Color.GREEN, ColorValue.loc("Green"));
        colorMap.put(Color.LIGHT_GRAY, ColorValue.loc("Light_Gray"));
        colorMap.put(Color.MAGENTA, ColorValue.loc("Magenta"));
        colorMap.put(Color.ORANGE, ColorValue.loc("Orange"));
        colorMap.put(Color.PINK, ColorValue.loc("Pink"));
        colorMap.put(Color.RED, ColorValue.loc("Red"));
        colorMap.put(Color.WHITE, ColorValue.loc("White"));
        colorMap.put(Color.YELLOW, ColorValue.loc("Yellow"));
    }
}

