/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.TokenId
 */
package org.netbeans.modules.options.colors;

import org.netbeans.api.lexer.TokenId;

public enum AllLanguagesTokenId implements TokenId
{
    COMMENT("comment"),
    KEYWORD("keyword"),
    OPERATOR("operator"),
    SEPARATOR("separator"),
    STRING("string"),
    CHARACTER("char"),
    NUMBER("number"),
    WHITESPACE("whitespace"),
    IDENTIFIER("identifier"),
    ERROR("error");
    
    private String name;

    private AllLanguagesTokenId(String name) {
        this.name = name;
    }

    public String primaryCategory() {
        return this.name;
    }
}

