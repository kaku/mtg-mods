/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.openide.awt.ColorComboBox
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.options.colors.CategoryComparator;
import org.netbeans.modules.options.colors.CategoryRenderer;
import org.netbeans.modules.options.colors.ColorComboBoxSupport;
import org.netbeans.modules.options.colors.ColorModel;
import org.netbeans.modules.options.colors.SyntaxColoringPanel;
import org.netbeans.modules.options.colors.spi.FontsColorsController;
import org.openide.awt.ColorComboBox;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class AnnotationsPanel
extends JPanel
implements ActionListener,
ItemListener,
FontsColorsController {
    private ColorModel colorModel;
    private boolean listen = false;
    private String currentScheme;
    private Map<String, List<AttributeSet>> schemes = new HashMap<String, List<AttributeSet>>();
    private Set<String> toBeSaved = new HashSet<String>();
    private boolean changed = false;
    private JComboBox cbBackground;
    private JComboBox cbEffectColor;
    private JComboBox cbEffects;
    private JComboBox cbForeground;
    private JScrollPane cpCategories;
    private JList<AttributeSet> lCategories;
    private JLabel lCategory;
    private JLabel lEffectColor;
    private JLabel lEffects;
    private JLabel lForeground;
    private JLabel lbackground;
    private Map<String, List<AttributeSet>> profileToDefaults = new HashMap<String, List<AttributeSet>>();

    public AnnotationsPanel() {
        this.initComponents();
        this.setName(AnnotationsPanel.loc("Annotations_tab"));
        this.cbForeground.getAccessibleContext().setAccessibleName(AnnotationsPanel.loc("AN_Foreground_Chooser"));
        this.cbForeground.getAccessibleContext().setAccessibleDescription(AnnotationsPanel.loc("AD_Foreground_Chooser"));
        this.cbBackground.getAccessibleContext().setAccessibleName(AnnotationsPanel.loc("AN_Background_Chooser"));
        this.cbBackground.getAccessibleContext().setAccessibleDescription(AnnotationsPanel.loc("AD_Background_Chooser"));
        this.cbEffectColor.getAccessibleContext().setAccessibleName(AnnotationsPanel.loc("AN_Wave_Underlined"));
        this.cbEffectColor.getAccessibleContext().setAccessibleDescription(AnnotationsPanel.loc("AD_Wave_Underlined"));
        this.lCategories.getAccessibleContext().setAccessibleName(AnnotationsPanel.loc("AN_Categories"));
        this.lCategories.getAccessibleContext().setAccessibleDescription(AnnotationsPanel.loc("AD_Categories"));
        this.lCategories.setSelectionMode(0);
        this.lCategories.setVisibleRowCount(3);
        this.lCategories.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!AnnotationsPanel.this.listen) {
                    return;
                }
                AnnotationsPanel.this.refreshUI();
            }
        });
        this.lCategories.setCellRenderer(new CategoryRenderer());
        this.cbForeground.addItemListener(this);
        this.cbBackground.addItemListener(this);
        this.cbEffectColor.addItemListener(this);
        this.lCategory.setLabelFor(this.lCategories);
        AnnotationsPanel.loc(this.lCategory, "CTL_Category");
        AnnotationsPanel.loc(this.lForeground, "CTL_Foreground_label");
        AnnotationsPanel.loc(this.lEffectColor, "CTL_Effects_color");
        AnnotationsPanel.loc(this.lbackground, "CTL_Background_label");
        this.cbEffects.addItem(AnnotationsPanel.loc("CTL_Effects_None"));
        this.cbEffects.addItem(AnnotationsPanel.loc("CTL_Effects_Wave_Underlined"));
        this.cbEffects.getAccessibleContext().setAccessibleName(AnnotationsPanel.loc("AN_Effects"));
        this.cbEffects.getAccessibleContext().setAccessibleDescription(AnnotationsPanel.loc("AD_Effects"));
        this.cbEffects.addActionListener(this);
    }

    private void initComponents() {
        this.lCategory = new JLabel();
        this.cpCategories = new JScrollPane();
        this.lCategories = new JList();
        this.lForeground = new JLabel();
        this.lbackground = new JLabel();
        this.lEffectColor = new JLabel();
        this.cbForeground = new ColorComboBox();
        this.cbBackground = new ColorComboBox();
        this.cbEffectColor = new ColorComboBox();
        this.lEffects = new JLabel();
        this.cbEffects = new JComboBox();
        this.lCategory.setText(NbBundle.getMessage(AnnotationsPanel.class, (String)"CTL_Category"));
        this.cpCategories.setViewportView(this.lCategories);
        this.lForeground.setText(NbBundle.getMessage(AnnotationsPanel.class, (String)"CTL_Foreground_label"));
        this.lbackground.setText(NbBundle.getMessage(AnnotationsPanel.class, (String)"CTL_Background_label"));
        this.lEffectColor.setText(NbBundle.getMessage(AnnotationsPanel.class, (String)"CTL_Effects_color"));
        this.lEffects.setLabelFor(this.cbEffects);
        this.lEffects.setText(NbBundle.getMessage(AnnotationsPanel.class, (String)"CTL_Effects_label"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addComponent(this.cpCategories, -1, 168, 32767).addGap(20, 20, 20).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lbackground).addComponent(this.lForeground).addComponent(this.lEffects).addComponent(this.lEffectColor)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.cbEffectColor, 0, -1, 32767).addComponent(this.cbForeground, GroupLayout.Alignment.TRAILING, 0, 138, 32767).addComponent(this.cbBackground, GroupLayout.Alignment.TRAILING, 0, -1, 32767).addComponent(this.cbEffects, 0, -1, 32767))).addComponent(this.lCategory)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.lCategory).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lForeground).addComponent(this.cbForeground, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lbackground).addComponent(this.cbBackground, -2, -1, -2)).addGap(7, 7, 7).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lEffects).addComponent(this.cbEffects, -2, -1, -2)).addGap(10, 10, 10).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lEffectColor).addComponent(this.cbEffectColor, -2, -1, -2)).addGap(0, 0, 32767)).addComponent(this.cpCategories, -1, 258, 32767)).addContainerGap()));
        this.lEffects.getAccessibleContext().setAccessibleName("&Effect:");
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (!this.listen) {
            return;
        }
        if (evt.getSource() == this.cbEffects) {
            if (this.cbEffects.getSelectedIndex() == 0) {
                this.cbEffectColor.setSelectedItem(null);
            }
            this.cbEffectColor.setEnabled(this.cbEffects.getSelectedIndex() > 0);
            this.updateData();
        }
        this.updateData();
        this.fireChanged();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == 2) {
            return;
        }
        if (!this.listen) {
            return;
        }
        this.updateData();
        this.fireChanged();
    }

    @Override
    public void update(ColorModel colorModel) {
        this.colorModel = colorModel;
        this.listen = false;
        this.currentScheme = colorModel.getCurrentProfile();
        this.lCategories.setListData(this.getAnnotations(this.currentScheme).toArray(new AttributeSet[0]));
        if (this.lCategories.getModel().getSize() > 0) {
            this.lCategories.setSelectedIndex(0);
        }
        this.refreshUI();
        this.listen = true;
        this.changed = false;
    }

    @Override
    public void cancel() {
        this.toBeSaved = new HashSet<String>();
        this.schemes = new HashMap<String, List<AttributeSet>>();
        this.changed = false;
    }

    @Override
    public void applyChanges() {
        if (this.colorModel == null) {
            return;
        }
        for (String scheme : this.toBeSaved) {
            this.colorModel.setAnnotations(scheme, this.getAnnotations(scheme));
        }
        this.toBeSaved = new HashSet<String>();
        this.schemes = new HashMap<String, List<AttributeSet>>();
        this.changed = false;
    }

    @Override
    public boolean isChanged() {
        return this.changed;
    }

    @Override
    public void setCurrentProfile(String currentScheme) {
        if (currentScheme == this.currentScheme) {
            return;
        }
        String oldScheme = this.currentScheme;
        this.currentScheme = currentScheme;
        List<AttributeSet> v = this.getAnnotations(currentScheme);
        if (v == null) {
            v = this.getAnnotations(oldScheme);
            this.schemes.put(currentScheme, new Vector<AttributeSet>(v));
            this.toBeSaved.add(currentScheme);
            v = this.getAnnotations(currentScheme);
        }
        this.toBeSaved.add(currentScheme);
        this.lCategories.setListData(v.toArray(new AttributeSet[0]));
        if (this.lCategories.getModel().getSize() > 0) {
            this.lCategories.setSelectedIndex(0);
        }
        this.refreshUI();
        this.fireChanged();
    }

    @Override
    public void deleteProfile(String scheme) {
        if (this.colorModel.isCustomProfile(scheme)) {
            this.schemes.remove(scheme);
            this.toBeSaved.remove(scheme);
        } else {
            this.schemes.put(scheme, this.getDefaults(scheme));
            this.lCategories.setListData(this.getAnnotations(scheme).toArray(new AttributeSet[0]));
            this.lCategories.repaint();
            this.lCategories.setSelectedIndex(0);
            this.refreshUI();
            this.toBeSaved.add(scheme);
        }
        this.fireChanged();
    }

    @Override
    public JComponent getComponent() {
        return this;
    }

    private static String loc(String key) {
        return NbBundle.getMessage(SyntaxColoringPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)AnnotationsPanel.loc(key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)AnnotationsPanel.loc(key));
        }
    }

    private void updateData() {
        List<AttributeSet> annotations = this.getAnnotations(this.currentScheme);
        int index = this.lCategories.getSelectedIndex();
        SimpleAttributeSet c = new SimpleAttributeSet(annotations.get(index));
        Color color = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbBackground);
        if (color != null) {
            c.addAttribute(StyleConstants.Background, color);
        } else {
            c.removeAttribute(StyleConstants.Background);
        }
        color = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbForeground);
        if (color != null) {
            c.addAttribute(StyleConstants.Foreground, color);
        } else {
            c.removeAttribute(StyleConstants.Foreground);
        }
        Color wave = null;
        if (this.cbEffects.getSelectedIndex() == 1) {
            wave = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbEffectColor);
        }
        if (wave != null) {
            c.addAttribute(EditorStyleConstants.WaveUnderlineColor, wave);
        } else {
            c.removeAttribute(EditorStyleConstants.WaveUnderlineColor);
        }
        annotations.set(index, c);
        this.toBeSaved.add(this.currentScheme);
    }

    private void fireChanged() {
        boolean isChanged = false;
        for (String profile : this.toBeSaved) {
            List<AttributeSet> attributeSet = this.getAnnotations(profile);
            Map<String, AttributeSet> savedAnnotations = this.toMap(this.getDefaults(profile));
            Map<String, AttributeSet> currentAnnotations = this.toMap(attributeSet);
            if (savedAnnotations == null || currentAnnotations == null) continue;
            isChanged = savedAnnotations.size() >= currentAnnotations.size() ? (isChanged |= this.checkMaps(savedAnnotations, currentAnnotations)) : (isChanged |= this.checkMaps(currentAnnotations, savedAnnotations));
            if (!isChanged) continue;
            this.changed = true;
            return;
        }
        this.changed = isChanged;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private boolean checkMaps(Map<String, AttributeSet> savedMap, Map<String, AttributeSet> currentMap) {
        isChanged = false;
        i$ = savedMap.keySet().iterator();
        do lbl-1000: // 3 sources:
        {
            if (i$.hasNext() == false) return isChanged;
            name = i$.next();
            if (!currentMap.containsKey(name)) ** GOTO lbl-1000
            currentAS = currentMap.get(name);
            savedAS = savedMap.get(name);
            currentForeground = (Color)currentAS.getAttribute(StyleConstants.Foreground);
            savedForeground = (Color)savedAS.getAttribute(StyleConstants.Foreground);
            currentBackground = (Color)currentAS.getAttribute(StyleConstants.Background);
            savedBackground = (Color)savedAS.getAttribute(StyleConstants.Background);
            currentWave = (Color)currentAS.getAttribute(EditorStyleConstants.WaveUnderlineColor);
            savedWave = (Color)savedAS.getAttribute(EditorStyleConstants.WaveUnderlineColor);
            if (currentForeground != null) ** GOTO lbl18
            if (savedForeground == null) ** GOTO lbl19
            ** GOTO lbl-1000
lbl18: // 1 sources:
            if (!currentForeground.equals(savedForeground)) ** GOTO lbl-1000
lbl19: // 2 sources:
            if (currentBackground != null) ** GOTO lbl22
            if (savedBackground == null) ** GOTO lbl23
            ** GOTO lbl-1000
lbl22: // 1 sources:
            if (!currentBackground.equals(savedBackground)) ** GOTO lbl-1000
lbl23: // 2 sources:
            if (currentWave == null ? savedWave != null : currentWave.equals(savedWave) == false) lbl-1000: // 5 sources:
            {
                v0 = true;
                continue;
            }
            v0 = false;
        } while (!(isChanged |= v0));
        return true;
    }

    private Map<String, AttributeSet> toMap(Collection<AttributeSet> categories) {
        if (categories == null) {
            return null;
        }
        HashMap<String, AttributeSet> result = new HashMap<String, AttributeSet>();
        for (AttributeSet as : categories) {
            result.put((String)as.getAttribute(StyleConstants.NameAttribute), as);
        }
        return result;
    }

    private void refreshUI() {
        int index = this.lCategories.getSelectedIndex();
        if (index < 0) {
            this.cbForeground.setEnabled(false);
            this.cbBackground.setEnabled(false);
            this.cbEffectColor.setEnabled(false);
            return;
        }
        this.cbForeground.setEnabled(true);
        this.cbBackground.setEnabled(true);
        this.cbEffectColor.setEnabled(true);
        this.listen = false;
        AttributeSet defAs = this.getDefaultColoring();
        if (defAs != null) {
            Color inheritedForeground = (Color)defAs.getAttribute(StyleConstants.Foreground);
            if (inheritedForeground == null) {
                inheritedForeground = Color.black;
            }
            ColorComboBoxSupport.setInheritedColor((ColorComboBox)this.cbForeground, inheritedForeground);
            Color inheritedBackground = (Color)defAs.getAttribute(StyleConstants.Background);
            if (inheritedBackground == null) {
                inheritedBackground = Color.white;
            }
            ColorComboBoxSupport.setInheritedColor((ColorComboBox)this.cbBackground, inheritedBackground);
        }
        List<AttributeSet> annotations = this.getAnnotations(this.currentScheme);
        AttributeSet c = annotations.get(index);
        ColorComboBoxSupport.setSelectedColor((ColorComboBox)this.cbForeground, (Color)c.getAttribute(StyleConstants.Foreground));
        ColorComboBoxSupport.setSelectedColor((ColorComboBox)this.cbBackground, (Color)c.getAttribute(StyleConstants.Background));
        if (c.getAttribute(EditorStyleConstants.WaveUnderlineColor) != null) {
            this.cbEffects.setSelectedIndex(1);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)c.getAttribute(EditorStyleConstants.WaveUnderlineColor));
        } else {
            this.cbEffects.setSelectedIndex(0);
            this.cbEffectColor.setEnabled(false);
            this.cbEffectColor.setSelectedIndex(-1);
        }
        this.listen = true;
    }

    private AttributeSet getDefaultColoring() {
        Collection<AttributeSet> defaults = this.colorModel.getCategories(this.currentScheme, ColorModel.ALL_LANGUAGES);
        for (AttributeSet as : defaults) {
            String name = (String)as.getAttribute(StyleConstants.NameAttribute);
            if (name == null || !"default".equals(name)) continue;
            return as;
        }
        return null;
    }

    private List<AttributeSet> getAnnotations(String scheme) {
        if (!this.schemes.containsKey(scheme)) {
            Collection<AttributeSet> c = this.colorModel.getAnnotations(this.currentScheme);
            if (c == null) {
                return null;
            }
            ArrayList<AttributeSet> l = new ArrayList<AttributeSet>(c);
            Collections.sort(l, new CategoryComparator());
            this.schemes.put(scheme, new ArrayList<AttributeSet>(l));
        }
        return this.schemes.get(scheme);
    }

    private List<AttributeSet> getDefaults(String profile) {
        if (!this.profileToDefaults.containsKey(profile)) {
            Collection<AttributeSet> c = this.colorModel.getAnnotationsDefaults(profile);
            ArrayList<AttributeSet> l = new ArrayList<AttributeSet>(c);
            Collections.sort(l, new CategoryComparator());
            this.profileToDefaults.put(profile, l);
        }
        List<AttributeSet> defaultprofile = this.profileToDefaults.get(profile);
        return new ArrayList<AttributeSet>(defaultprofile);
    }

}

