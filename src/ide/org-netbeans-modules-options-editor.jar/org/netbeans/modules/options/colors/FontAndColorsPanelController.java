/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.options.colors;

import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.modules.options.colors.FontAndColorsPanel;
import org.netbeans.modules.options.colors.spi.FontsColorsController;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;

public final class FontAndColorsPanelController
extends OptionsPanelController {
    private final Lookup.Result<? extends FontsColorsController> lookupResult;
    private final LookupListener lookupListener;
    private Collection<? extends FontsColorsController> delegates;
    private FontAndColorsPanel component;

    public FontAndColorsPanelController() {
        this.lookupListener = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                FontAndColorsPanelController.this.rebuild();
            }
        };
        Lookup lookup = Lookups.forPath((String)"org-netbeans-modules-options-editor/OptionsDialogCategories/FontsColors");
        this.lookupResult = lookup.lookupResult(FontsColorsController.class);
        this.lookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupListener, this.lookupResult));
        this.rebuild();
    }

    public void update() {
        if (this.getFontAndColorsPanel() != null) {
            this.getFontAndColorsPanel().update();
        }
    }

    public void applyChanges() {
        if (this.getFontAndColorsPanel() != null) {
            this.getFontAndColorsPanel().applyChanges();
        }
    }

    public void cancel() {
        if (this.getFontAndColorsPanel() != null) {
            this.getFontAndColorsPanel().cancel();
        }
    }

    public boolean isValid() {
        if (this.getFontAndColorsPanel() != null) {
            return this.getFontAndColorsPanel().dataValid();
        }
        return true;
    }

    public boolean isChanged() {
        if (this.getFontAndColorsPanel() != null) {
            return this.getFontAndColorsPanel().isChanged();
        }
        return false;
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getFontAndColorsPanel();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.fontAndColorsPanel");
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        if (this.getFontAndColorsPanel() != null) {
            this.getFontAndColorsPanel().addPropertyChangeListener(l);
        }
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.getFontAndColorsPanel() != null) {
            this.getFontAndColorsPanel().removePropertyChangeListener(l);
        }
    }

    private synchronized FontAndColorsPanel getFontAndColorsPanel() {
        if (this.component == null && SwingUtilities.isEventDispatchThread()) {
            assert (!this.delegates.isEmpty());
            this.component = new FontAndColorsPanel(this.delegates);
        }
        return this.component;
    }

    private void rebuild() {
        this.delegates = this.lookupResult.allInstances();
        this.component = null;
    }

}

