/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.modules.editor.NbEditorKit
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.util.Comparator;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.editor.NbEditorKit;
import org.openide.util.NbBundle;

public final class CategoryComparator
implements Comparator<AttributeSet> {
    String default_string = NbBundle.getMessage(NbEditorKit.class, (String)"default");

    @Override
    public int compare(AttributeSet o1, AttributeSet o2) {
        String name_1 = CategoryComparator.name(o1);
        String name_2 = CategoryComparator.name(o2);
        if (name_1.startsWith(this.default_string)) {
            return name_2.startsWith(this.default_string) ? 0 : -1;
        }
        if (name_2.startsWith(this.default_string)) {
            return 1;
        }
        return name_1.compareTo(name_2);
    }

    private static String name(AttributeSet o) {
        return (String)o.getAttribute(EditorStyleConstants.DisplayName);
    }
}

