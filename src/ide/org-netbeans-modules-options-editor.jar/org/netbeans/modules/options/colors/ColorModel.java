/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationTypes
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.TokenCategory
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 *  org.netbeans.editor.TokenItem
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.FontColorSettingsFactory;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class ColorModel {
    private static final Logger LOG = Logger.getLogger(ColorModel.class.getName());
    public static final String ALL_LANGUAGES = NbBundle.getMessage(ColorModel.class, (String)"CTL_All_Languages");
    private static final String[] EMPTY_MIMEPATH = new String[0];
    private Map<String, String> languageToMimeType;

    public Set<String> getProfiles() {
        return EditorSettings.getDefault().getFontColorProfiles();
    }

    public String getCurrentProfile() {
        return EditorSettings.getDefault().getCurrentFontColorProfile();
    }

    public boolean isCustomProfile(String profile) {
        if (!this.getProfiles().contains(profile)) {
            return true;
        }
        return EditorSettings.getDefault().isCustomFontColorProfile(profile);
    }

    public void setCurrentProfile(String profile) {
        EditorSettings.getDefault().setCurrentFontColorProfile(profile);
    }

    public Collection<AttributeSet> getAnnotations(String profile) {
        Map annos = EditorSettings.getDefault().getAnnotations(profile);
        List<AttributeSet> annotations = this.processAnnotations(annos, false);
        return annotations;
    }

    public Collection<AttributeSet> getAnnotationsDefaults(String profile) {
        Map annos = EditorSettings.getDefault().getAnnotationDefaults(profile);
        List<AttributeSet> annotations = this.processAnnotations(annos, true);
        return annotations;
    }

    private List<AttributeSet> processAnnotations(Map<String, AttributeSet> annos, boolean isdefault) {
        ArrayList<AttributeSet> annotations = new ArrayList<AttributeSet>();
        Iterator it = AnnotationTypes.getTypes().getAnnotationTypeNames();
        while (it.hasNext()) {
            String description;
            String name = (String)it.next();
            AnnotationType annotationType = AnnotationTypes.getTypes().getType(name);
            if (!annotationType.isVisible() || (description = annotationType.getDescription()) == null) continue;
            SimpleAttributeSet category = new SimpleAttributeSet();
            category.addAttribute(EditorStyleConstants.DisplayName, description);
            category.addAttribute(StyleConstants.NameAttribute, annotationType.getName());
            URL iconURL = annotationType.getGlyph();
            Image image = null;
            image = iconURL.getProtocol().equals("nbresloc") ? ImageUtilities.loadImage((String)iconURL.getPath().substring(1)) : Toolkit.getDefaultToolkit().getImage(iconURL);
            if (image != null) {
                category.addAttribute("icon", new ImageIcon(image));
            }
            Color bgColor = annotationType.getHighlight();
            if (annotationType.isUseHighlightColor() && bgColor != null) {
                category.addAttribute(StyleConstants.Background, bgColor);
            }
            Color fgColor = annotationType.getForegroundColor();
            if (!annotationType.isInheritForegroundColor() && fgColor != null) {
                category.addAttribute(StyleConstants.Foreground, fgColor);
            }
            Color underColor = annotationType.getWaveUnderlineColor();
            if (annotationType.isUseWaveUnderlineColor() && underColor != null) {
                category.addAttribute(EditorStyleConstants.WaveUnderlineColor, underColor);
            }
            category.addAttribute("annotationType", (Object)annotationType);
            if (annos.containsKey(name)) {
                if (isdefault) {
                    category.removeAttribute(StyleConstants.Background);
                    category.removeAttribute(StyleConstants.Foreground);
                    category.removeAttribute(EditorStyleConstants.WaveUnderlineColor);
                }
                AttributeSet as = annos.get(name);
                category.addAttributes(as);
            }
            annotations.add(category);
        }
        return annotations;
    }

    public void setAnnotations(String profile, Collection<AttributeSet> annotations) {
        ArrayList<AttributeSet> annos = new ArrayList<AttributeSet>();
        for (AttributeSet category : annotations) {
            AnnotationType annotationType = (AnnotationType)category.getAttribute("annotationType");
            SimpleAttributeSet c = new SimpleAttributeSet();
            c.addAttribute(StyleConstants.NameAttribute, category.getAttribute(StyleConstants.NameAttribute));
            if (category.isDefined(StyleConstants.Background)) {
                annotationType.setUseHighlightColor(true);
                annotationType.setHighlight((Color)category.getAttribute(StyleConstants.Background));
                c.addAttribute(StyleConstants.Background, category.getAttribute(StyleConstants.Background));
            } else {
                annotationType.setUseHighlightColor(false);
            }
            if (category.isDefined(StyleConstants.Foreground)) {
                annotationType.setInheritForegroundColor(false);
                annotationType.setForegroundColor((Color)category.getAttribute(StyleConstants.Foreground));
                c.addAttribute(StyleConstants.Foreground, category.getAttribute(StyleConstants.Foreground));
            } else {
                annotationType.setInheritForegroundColor(true);
            }
            if (category.isDefined(EditorStyleConstants.WaveUnderlineColor)) {
                annotationType.setUseWaveUnderlineColor(true);
                annotationType.setWaveUnderlineColor((Color)category.getAttribute(EditorStyleConstants.WaveUnderlineColor));
                c.addAttribute(EditorStyleConstants.WaveUnderlineColor, category.getAttribute(EditorStyleConstants.WaveUnderlineColor));
            } else {
                annotationType.setUseWaveUnderlineColor(false);
            }
            annos.add(c);
        }
        EditorSettings.getDefault().setAnnotations(profile, ColorModel.toMap(annos));
    }

    public Collection<AttributeSet> getHighlightings(String profile) {
        Map m = EditorSettings.getDefault().getHighlightings(profile);
        if (m == null) {
            return null;
        }
        return m.values();
    }

    public Collection<AttributeSet> getHighlightingDefaults(String profile) {
        Collection<AttributeSet> r = EditorSettings.getDefault().getHighlightingDefaults(profile).values();
        return r;
    }

    public void setHighlightings(String profile, Collection<AttributeSet> highlihgtings) {
        EditorSettings.getDefault().setHighlightings(profile, ColorModel.toMap(highlihgtings));
    }

    public Set<String> getLanguages() {
        return this.getLanguageToMimeTypeMap().keySet();
    }

    public Collection<AttributeSet> getCategories(String profile, String language) {
        String[] mimePath = this.getMimePath(language);
        FontColorSettingsFactory fcs = EditorSettings.getDefault().getFontColorSettings(mimePath);
        return fcs.getAllFontColors(profile);
    }

    public Collection<AttributeSet> getDefaults(String profile, String language) {
        String[] mimePath = this.getMimePath(language);
        FontColorSettingsFactory fcs = EditorSettings.getDefault().getFontColorSettings(mimePath);
        return fcs.getAllFontColorDefaults(profile);
    }

    public void setCategories(String profile, String language, Collection<AttributeSet> categories) {
        String[] mimePath = this.getMimePath(language);
        FontColorSettingsFactory fcs = EditorSettings.getDefault().getFontColorSettings(mimePath);
        fcs.setAllFontColors(profile, categories);
    }

    public Component getSyntaxColoringPreviewComponent(String language) {
        String mimeType = this.getMimeType(language);
        return new Preview("test" + this.hashCode(), mimeType);
    }

    private String getMimeType(String language) {
        if (language.equals(ALL_LANGUAGES)) {
            return "";
        }
        String mimeType = this.getLanguageToMimeTypeMap().get(language);
        assert (mimeType != null);
        return mimeType;
    }

    private String[] getMimePath(String language) {
        if (language.equals(ALL_LANGUAGES)) {
            return EMPTY_MIMEPATH;
        }
        String mimeType = this.getLanguageToMimeTypeMap().get(language);
        assert (mimeType != null);
        return new String[]{mimeType};
    }

    private Map<String, String> getLanguageToMimeTypeMap() {
        if (this.languageToMimeType == null) {
            this.languageToMimeType = new HashMap<String, String>();
            Set mimeTypes = EditorSettings.getDefault().getMimeTypes();
            for (String mimeType : mimeTypes) {
                String name = EditorSettings.getDefault().getLanguageName(mimeType);
                if (name.equals(mimeType)) continue;
                this.languageToMimeType.put(name, mimeType);
            }
            this.languageToMimeType.put(ALL_LANGUAGES, "Defaults");
        }
        return this.languageToMimeType;
    }

    private static Map<String, AttributeSet> toMap(Collection<AttributeSet> categories) {
        if (categories == null) {
            return null;
        }
        HashMap<String, AttributeSet> result = new HashMap<String, AttributeSet>();
        for (AttributeSet as : categories) {
            result.put((String)as.getAttribute(StyleConstants.NameAttribute), as);
        }
        return result;
    }

    final class Preview
    extends JPanel {
        static final String PROP_CURRENT_ELEMENT = "currentAElement";
        private String testProfileName;
        private String currentMimeType;
        private JEditorPane editorPane;
        private boolean fireChanges;

        public Preview(String testProfileName, String mimeType) {
            super(new BorderLayout());
            this.fireChanges = false;
            this.testProfileName = testProfileName;
            SwingUtilities.invokeLater(new Runnable(ColorModel.this, mimeType){
                final /* synthetic */ ColorModel val$this$0;
                final /* synthetic */ String val$mimeType;

                @Override
                public void run() {
                    Preview.this.updateMimeType(this.val$mimeType);
                }
            });
            this.setCursor(Cursor.getPredefinedCursor(12));
        }

        public void setParameters(String language, final Collection<AttributeSet> defaults, final Collection<AttributeSet> highlightings, final Collection<AttributeSet> syntaxColorings) {
            final String mimeType = ColorModel.this.getMimeType(language);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (defaults != null) {
                        EditorSettings.getDefault().getFontColorSettings(EMPTY_MIMEPATH).setAllFontColors(Preview.this.testProfileName, defaults);
                    }
                    if (highlightings != null) {
                        EditorSettings.getDefault().setHighlightings(Preview.this.testProfileName, ColorModel.toMap(highlightings));
                    }
                    if (syntaxColorings != null && Preview.this.currentMimeType.length() != 0) {
                        FontColorSettingsFactory fcs = EditorSettings.getDefault().getFontColorSettings(new String[]{Preview.this.currentMimeType});
                        fcs.setAllFontColors(Preview.this.testProfileName, syntaxColorings);
                    }
                    Preview.this.updateMimeType(mimeType);
                }
            });
        }

        private void updateMimeType(String mimeType) {
            if (this.editorPane != null && mimeType.equals(this.currentMimeType)) {
                return;
            }
            this.fireChanges = false;
            this.currentMimeType = mimeType;
            String[] ret = this.loadPreviewExample(mimeType);
            String exampleText = ret[0];
            String exampleMimeType = ret[1];
            String hackMimeType = this.hackMimeType(exampleMimeType);
            this.removeAll();
            this.editorPane = new JEditorPane();
            this.add((Component)this.editorPane, "Center");
            EditorKit kit = CloneableEditorSupport.getEditorKit((String)hackMimeType);
            Document document = kit.createDefaultDocument();
            document.putProperty("mimeType", hackMimeType);
            this.editorPane.setEditorKit(kit);
            this.editorPane.setDocument(document);
            InputAttributes inputAttributes = new InputAttributes();
            Language language = Language.find((String)exampleMimeType);
            if (language != null) {
                inputAttributes.setValue(language, (Object)"OptionsDialog", (Object)Boolean.TRUE, true);
            }
            document.putProperty(InputAttributes.class, (Object)inputAttributes);
            this.editorPane.addCaretListener(new CaretListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void caretUpdate(CaretEvent e) {
                    String elementName;
                    if (!Preview.this.fireChanges) {
                        Preview.this.fireChanges = true;
                        return;
                    }
                    int offset = e.getDot();
                    elementName = null;
                    if (Preview.this.editorPane == null) {
                        return;
                    }
                    ((AbstractDocument)Preview.this.editorPane.getDocument()).readLock();
                    try {
                        TokenHierarchy th = TokenHierarchy.get((Document)Preview.this.editorPane.getDocument());
                        if (th != null) {
                            elementName = this.findLexerElement(th, offset);
                        } else {
                            SyntaxSupport ss = Utilities.getSyntaxSupport((JTextComponent)Preview.this.editorPane);
                            if (ss instanceof ExtSyntaxSupport) {
                                elementName = this.findSyntaxElement((ExtSyntaxSupport)ss, offset);
                            }
                        }
                    }
                    finally {
                        ((AbstractDocument)Preview.this.editorPane.getDocument()).readUnlock();
                    }
                    if (elementName != null) {
                        Preview.this.firePropertyChange("currentAElement", null, elementName);
                    }
                }

                private String findLexerElement(TokenHierarchy<Document> hierarchy, int offset) {
                    String elementName = null;
                    List sequences = hierarchy.embeddedTokenSequences(offset, false);
                    if (!sequences.isEmpty()) {
                        TokenSequence seq = (TokenSequence)sequences.get(sequences.size() - 1);
                        seq.move(offset);
                        if (seq.moveNext() && (elementName = seq.token().id().primaryCategory()) == null) {
                            elementName = seq.token().id().name();
                        }
                    }
                    return elementName;
                }

                private String findSyntaxElement(ExtSyntaxSupport syntax, int offset) {
                    try {
                        TokenItem tokenItem = syntax.getTokenChain(offset, offset + 1);
                        if (tokenItem == null) {
                            return null;
                        }
                        String elementName = tokenItem.getTokenContextPath().getNamePrefix();
                        elementName = tokenItem.getTokenID().getCategory() != null ? elementName + tokenItem.getTokenID().getCategory().getName() : elementName + tokenItem.getTokenID().getName();
                        return elementName;
                    }
                    catch (BadLocationException ble) {
                        LOG.log(Level.WARNING, null, ble);
                        return null;
                    }
                }
            });
            this.editorPane.setEnabled(false);
            this.editorPane.setText(exampleText);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (Preview.this.editorPane != null) {
                        Preview.this.editorPane.scrollRectToVisible(new Rectangle(0, 0, 10, 10));
                    }
                }
            });
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private String[] loadPreviewExample(String mimeType) {
            FileObject exampleFile = null;
            String exampleMimeType = null;
            if (mimeType == null || mimeType.length() == 0) {
                FileObject f = FileUtil.getConfigFile((String)"OptionsDialog/PreviewExamples");
                if (f != null && f.isFolder()) {
                    FileObject[] ff = f.getChildren();
                    for (int i = 0; i < ff.length; ++i) {
                        if (!ff[i].isData()) continue;
                        exampleFile = ff[i];
                        break;
                    }
                }
                if (exampleFile != null) {
                    exampleMimeType = exampleFile.getMIMEType().equals("content/unknown") ? "text/x-all-languages" : exampleFile.getMIMEType();
                }
            } else {
                exampleFile = FileUtil.getConfigFile((String)("OptionsDialog/PreviewExamples/" + mimeType));
                exampleMimeType = mimeType;
            }
            if (exampleFile != null && exampleFile.isValid() && exampleFile.getSize() > 0) {
                StringBuilder sb;
                sb = exampleFile.getSize() < Integer.MAX_VALUE ? new StringBuilder((int)exampleFile.getSize()) : new StringBuilder(Integer.MAX_VALUE);
                try {
                    InputStreamReader is = new InputStreamReader(exampleFile.getInputStream());
                    char[] buffer = new char[1024];
                    try {
                        int size;
                        while (0 < (size = is.read(buffer, 0, buffer.length))) {
                            sb.append(buffer, 0, size);
                        }
                    }
                    finally {
                        is.close();
                    }
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, "Can't read font & colors preview example", ioe);
                }
                return new String[]{sb.toString(), exampleMimeType};
            }
            if (exampleFile != null && !exampleFile.isValid()) {
                LOG.log(Level.WARNING, "Font & colors preview example is invalid " + (Object)exampleFile);
            }
            return new String[]{"", "text/plain"};
        }

        private String hackMimeType(String mimeType) {
            return this.testProfileName + "_" + mimeType;
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            this.editorPane = null;
        }

    }

}

