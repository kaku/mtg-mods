/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.colors;

import javax.swing.JComponent;
import org.netbeans.modules.options.colors.ColorModel;
import org.netbeans.modules.options.colors.HighlightingPanel;
import org.netbeans.modules.options.colors.spi.FontsColorsController;

public class HighlightingPanelController
implements FontsColorsController {
    private HighlightingPanel component = null;

    @Override
    public void update(ColorModel model) {
        this.getHighlightingPanel().update(model);
    }

    @Override
    public void setCurrentProfile(String profile) {
        this.getHighlightingPanel().setCurrentProfile(profile);
    }

    @Override
    public void deleteProfile(String profile) {
        this.getHighlightingPanel().deleteProfile(profile);
    }

    @Override
    public void applyChanges() {
        this.getHighlightingPanel().applyChanges();
    }

    @Override
    public void cancel() {
        this.getHighlightingPanel().cancel();
    }

    @Override
    public boolean isChanged() {
        return this.getHighlightingPanel().isChanged();
    }

    @Override
    public JComponent getComponent() {
        return this.getHighlightingPanel();
    }

    private synchronized HighlightingPanel getHighlightingPanel() {
        if (this.component == null) {
            this.component = new HighlightingPanel();
        }
        return this.component;
    }
}

