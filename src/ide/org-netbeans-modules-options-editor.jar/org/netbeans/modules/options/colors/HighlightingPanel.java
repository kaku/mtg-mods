/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.awt.ColorComboBox
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.colors.CategoryComparator;
import org.netbeans.modules.options.colors.CategoryRenderer;
import org.netbeans.modules.options.colors.ColorComboBoxSupport;
import org.netbeans.modules.options.colors.ColorModel;
import org.netbeans.modules.options.colors.SyntaxColoringPanel;
import org.openide.awt.ColorComboBox;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class HighlightingPanel
extends JPanel
implements ActionListener,
ItemListener {
    private ColorModel colorModel = null;
    private boolean listen = false;
    private String currentProfile;
    private Map<String, List<AttributeSet>> profileToCategories = new HashMap<String, List<AttributeSet>>();
    private Set<String> toBeSaved = new HashSet<String>();
    private boolean changed = false;
    private JComboBox cbBackground;
    private JComboBox cbEffectColor;
    private JComboBox<String> cbEffects;
    private JComboBox cbForeground;
    private JScrollPane cpCategories;
    private JLabel lBackground;
    private JList<AttributeSet> lCategories;
    private JLabel lCategory;
    private JLabel lEffectColor;
    private JLabel lEffects;
    private JLabel lForeground;
    private Map<String, List<AttributeSet>> profileToDefaults = new HashMap<String, List<AttributeSet>>();

    public HighlightingPanel() {
        this.initComponents();
        this.setName(HighlightingPanel.loc("Editor_tab"));
        this.lCategories.getAccessibleContext().setAccessibleName(HighlightingPanel.loc("AN_Categories"));
        this.lCategories.getAccessibleContext().setAccessibleDescription(HighlightingPanel.loc("AD_Categories"));
        this.cbForeground.getAccessibleContext().setAccessibleName(HighlightingPanel.loc("AN_Foreground_Chooser"));
        this.cbForeground.getAccessibleContext().setAccessibleDescription(HighlightingPanel.loc("AD_Foreground_Chooser"));
        this.cbBackground.getAccessibleContext().setAccessibleName(HighlightingPanel.loc("AN_Background_Chooser"));
        this.cbBackground.getAccessibleContext().setAccessibleDescription(HighlightingPanel.loc("AD_Background_Chooser"));
        this.lCategories.setSelectionMode(0);
        this.lCategories.setVisibleRowCount(3);
        this.lCategories.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!HighlightingPanel.this.listen) {
                    return;
                }
                HighlightingPanel.this.refreshUI();
            }
        });
        this.lCategories.setCellRenderer(new CategoryRenderer());
        this.cbForeground.addItemListener(this);
        this.cbBackground.addItemListener(this);
        this.cbEffects.addItem(HighlightingPanel.loc("CTL_Effects_None"));
        this.cbEffects.addItem(HighlightingPanel.loc("CTL_Effects_Underlined"));
        this.cbEffects.addItem(HighlightingPanel.loc("CTL_Effects_Wave_Underlined"));
        this.cbEffects.addItem(HighlightingPanel.loc("CTL_Effects_Strike_Through"));
        this.cbEffects.getAccessibleContext().setAccessibleName(HighlightingPanel.loc("AN_Effects"));
        this.cbEffects.getAccessibleContext().setAccessibleDescription(HighlightingPanel.loc("AD_Effects"));
        this.cbEffects.addActionListener(this);
        this.cbEffectColor.addItemListener(this);
        this.lCategory.setLabelFor(this.lCategories);
        HighlightingPanel.loc(this.lCategory, "CTL_Category");
        HighlightingPanel.loc(this.lForeground, "CTL_Foreground_label");
        HighlightingPanel.loc(this.lBackground, "CTL_Background_label");
    }

    private void initComponents() {
        this.lCategory = new JLabel();
        this.cpCategories = new JScrollPane();
        this.lCategories = new JList();
        this.lForeground = new JLabel();
        this.lBackground = new JLabel();
        this.cbBackground = new ColorComboBox();
        this.cbForeground = new ColorComboBox();
        this.cbEffects = new JComboBox();
        this.cbEffectColor = new ColorComboBox();
        this.lEffects = new JLabel();
        this.lEffectColor = new JLabel();
        this.lCategory.setText(NbBundle.getMessage(HighlightingPanel.class, (String)"CTL_Category"));
        this.cpCategories.setViewportView(this.lCategories);
        this.lForeground.setText(NbBundle.getMessage(HighlightingPanel.class, (String)"CTL_Foreground_label"));
        this.lBackground.setText(NbBundle.getMessage(HighlightingPanel.class, (String)"CTL_Background_label"));
        this.lEffects.setLabelFor(this.cbEffects);
        this.lEffects.setText(NbBundle.getMessage(HighlightingPanel.class, (String)"CTL_Effects_label"));
        this.lEffectColor.setLabelFor(this.cbEffectColor);
        this.lEffectColor.setText(NbBundle.getMessage(HighlightingPanel.class, (String)"CTL_Effects_color"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.cpCategories, -1, 206, 32767).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(20, 20, 20).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lBackground).addComponent(this.lForeground))).addGroup(layout.createSequentialGroup().addGap(18, 18, 18).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.lEffectColor).addComponent(this.lEffects)))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.cbBackground, GroupLayout.Alignment.TRAILING, 0, 49, 32767).addComponent(this.cbForeground, GroupLayout.Alignment.TRAILING, 0, 49, 32767).addComponent(this.cbEffects, 0, -1, 32767).addComponent(this.cbEffectColor, 0, -1, 32767))).addComponent(this.lCategory)).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addComponent(this.lCategory).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lForeground).addComponent(this.cbForeground, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lBackground).addComponent(this.cbBackground, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cbEffects, -2, -1, -2).addComponent(this.lEffects)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cbEffectColor, -2, -1, -2).addComponent(this.lEffectColor)).addGap(0, 0, 32767)).addComponent(this.cpCategories, -1, 189, 32767)).addContainerGap()));
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (!this.listen) {
            return;
        }
        if (evt.getSource() == this.cbEffects) {
            if (this.cbEffects.getSelectedIndex() == 0) {
                this.cbEffectColor.setSelectedItem(null);
            }
            this.cbEffectColor.setEnabled(this.cbEffects.getSelectedIndex() > 0);
            this.updateData();
        }
        this.updateData();
        this.fireChanged();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == 2) {
            return;
        }
        if (!this.listen) {
            return;
        }
        this.updateData();
        this.fireChanged();
    }

    public void update(ColorModel colorModel) {
        this.colorModel = colorModel;
        this.currentProfile = colorModel.getCurrentProfile();
        this.listen = false;
        this.setCurrentProfile(this.currentProfile);
        this.listen = true;
        this.changed = false;
    }

    public void cancel() {
        this.toBeSaved = new HashSet<String>();
        this.profileToCategories = new HashMap<String, List<AttributeSet>>();
        this.changed = false;
    }

    public void applyChanges() {
        if (this.colorModel == null) {
            return;
        }
        for (String profile : this.toBeSaved) {
            List<AttributeSet> cat = null;
            if (this.profileToCategories.containsKey(profile)) {
                cat = this.getCategories(profile);
            }
            this.colorModel.setHighlightings(profile, cat);
        }
        this.toBeSaved = new HashSet<String>();
        this.profileToCategories = new HashMap<String, List<AttributeSet>>();
        this.changed = false;
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void setCurrentProfile(String currentProfile) {
        String oldScheme = this.currentProfile;
        this.currentProfile = currentProfile;
        if (!this.colorModel.getProfiles().contains(currentProfile) && !this.profileToCategories.containsKey(currentProfile)) {
            List<AttributeSet> categories = this.getCategories(oldScheme);
            this.profileToCategories.put(currentProfile, new ArrayList<AttributeSet>(categories));
            this.toBeSaved.add(currentProfile);
        }
        this.lCategories.setListData(this.getCategories(currentProfile).toArray(new AttributeSet[0]));
        this.lCategories.repaint();
        this.lCategories.setSelectedIndex(0);
        this.refreshUI();
        this.fireChanged();
    }

    public void deleteProfile(String profile) {
        if (this.colorModel.isCustomProfile(profile)) {
            this.profileToCategories.remove(profile);
        } else {
            this.profileToCategories.put(profile, this.getDefaults(profile));
            this.lCategories.setListData(this.getCategories(profile).toArray(new AttributeSet[0]));
            this.lCategories.repaint();
            this.lCategories.setSelectedIndex(0);
            this.refreshUI();
        }
        this.toBeSaved.add(profile);
        this.fireChanged();
    }

    Collection<AttributeSet> getHighlightings() {
        return this.getCategories(this.currentProfile);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(SyntaxColoringPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)HighlightingPanel.loc(key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)HighlightingPanel.loc(key));
        }
    }

    private void updateData() {
        int index = this.lCategories.getSelectedIndex();
        if (index < 0) {
            return;
        }
        List<AttributeSet> categories = this.getCategories(this.currentProfile);
        AttributeSet category = categories.get(this.lCategories.getSelectedIndex());
        SimpleAttributeSet c = new SimpleAttributeSet(category);
        Color color = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbBackground);
        if (color != null) {
            c.addAttribute(StyleConstants.Background, color);
        } else {
            c.removeAttribute(StyleConstants.Background);
        }
        color = ColorComboBoxSupport.getSelectedColor((ColorComboBox)this.cbForeground);
        if (color != null) {
            c.addAttribute(StyleConstants.Foreground, color);
        } else {
            c.removeAttribute(StyleConstants.Foreground);
        }
        Color underline = null;
        Color wave = null;
        Color strikethrough = null;
        if (this.cbEffects.getSelectedIndex() == 1) {
            underline = ((ColorComboBox)this.cbEffectColor).getSelectedColor();
        }
        if (this.cbEffects.getSelectedIndex() == 2) {
            wave = ((ColorComboBox)this.cbEffectColor).getSelectedColor();
        }
        if (this.cbEffects.getSelectedIndex() == 3) {
            strikethrough = ((ColorComboBox)this.cbEffectColor).getSelectedColor();
        }
        if (underline != null) {
            c.addAttribute(StyleConstants.Underline, underline);
        } else {
            c.removeAttribute(StyleConstants.Underline);
        }
        if (strikethrough != null) {
            c.addAttribute(StyleConstants.StrikeThrough, strikethrough);
        } else {
            c.removeAttribute(StyleConstants.StrikeThrough);
        }
        if (wave != null) {
            c.addAttribute(EditorStyleConstants.WaveUnderlineColor, wave);
        } else {
            c.removeAttribute(EditorStyleConstants.WaveUnderlineColor);
        }
        categories.set(index, c);
        this.toBeSaved.add(this.currentProfile);
    }

    private void fireChanged() {
        boolean isChanged = false;
        for (String profile : this.toBeSaved) {
            if (!this.profileToCategories.containsKey(profile)) continue;
            List<AttributeSet> attributeSet = this.getCategories(profile);
            Map savedHighlightings = EditorSettings.getDefault().getHighlightings(profile);
            Map<String, AttributeSet> currentHighlightings = this.toMap(attributeSet);
            if (savedHighlightings == null || currentHighlightings == null) continue;
            if (savedHighlightings.size() >= currentHighlightings.size()) {
                isChanged |= this.checkMaps(savedHighlightings, currentHighlightings);
                continue;
            }
            isChanged |= this.checkMaps(currentHighlightings, savedHighlightings);
        }
        this.changed = isChanged;
    }

    private boolean checkMaps(Map<String, AttributeSet> savedMap, Map<String, AttributeSet> currentMap) {
        boolean isChanged = false;
        for (String name : savedMap.keySet()) {
            if (!currentMap.containsKey(name)) continue;
            AttributeSet currentAS = currentMap.get(name);
            AttributeSet savedAS = savedMap.get(name);
            isChanged |= (Color)currentAS.getAttribute(StyleConstants.Foreground) != (Color)savedAS.getAttribute(StyleConstants.Foreground) || (Color)currentAS.getAttribute(StyleConstants.Background) != (Color)savedAS.getAttribute(StyleConstants.Background) || (Color)currentAS.getAttribute(StyleConstants.Underline) != (Color)savedAS.getAttribute(StyleConstants.Underline) || (Color)currentAS.getAttribute(StyleConstants.StrikeThrough) != (Color)savedAS.getAttribute(StyleConstants.StrikeThrough) || (Color)currentAS.getAttribute(EditorStyleConstants.WaveUnderlineColor) != (Color)savedAS.getAttribute(EditorStyleConstants.WaveUnderlineColor);
        }
        return isChanged;
    }

    private Map<String, AttributeSet> toMap(Collection<AttributeSet> categories) {
        if (categories == null) {
            return null;
        }
        HashMap<String, AttributeSet> result = new HashMap<String, AttributeSet>();
        for (AttributeSet as : categories) {
            result.put((String)as.getAttribute(StyleConstants.NameAttribute), as);
        }
        return result;
    }

    private void refreshUI() {
        int index = this.lCategories.getSelectedIndex();
        if (index < 0) {
            this.cbForeground.setEnabled(false);
            this.cbBackground.setEnabled(false);
            return;
        }
        this.cbForeground.setEnabled(true);
        this.cbBackground.setEnabled(true);
        List<AttributeSet> categories = this.getCategories(this.currentProfile);
        AttributeSet category = categories.get(index);
        this.listen = false;
        AttributeSet defAs = this.getDefaultColoring();
        if (defAs != null) {
            Color inheritedForeground = (Color)defAs.getAttribute(StyleConstants.Foreground);
            if (inheritedForeground == null) {
                inheritedForeground = Color.black;
            }
            ColorComboBoxSupport.setInheritedColor((ColorComboBox)this.cbForeground, inheritedForeground);
            Color inheritedBackground = (Color)defAs.getAttribute(StyleConstants.Background);
            if (inheritedBackground == null) {
                inheritedBackground = Color.white;
            }
            ColorComboBoxSupport.setInheritedColor((ColorComboBox)this.cbBackground, inheritedBackground);
        }
        if (category.getAttribute(StyleConstants.Underline) != null) {
            this.cbEffects.setSelectedIndex(1);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)category.getAttribute(StyleConstants.Underline));
        } else if (category.getAttribute(EditorStyleConstants.WaveUnderlineColor) != null) {
            this.cbEffects.setSelectedIndex(2);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)category.getAttribute(EditorStyleConstants.WaveUnderlineColor));
        } else if (category.getAttribute(StyleConstants.StrikeThrough) != null) {
            this.cbEffects.setSelectedIndex(3);
            this.cbEffectColor.setEnabled(true);
            ((ColorComboBox)this.cbEffectColor).setSelectedColor((Color)category.getAttribute(StyleConstants.StrikeThrough));
        } else {
            this.cbEffects.setSelectedIndex(0);
            this.cbEffectColor.setEnabled(false);
            this.cbEffectColor.setSelectedIndex(-1);
        }
        ColorComboBoxSupport.setSelectedColor((ColorComboBox)this.cbForeground, (Color)category.getAttribute(StyleConstants.Foreground));
        ColorComboBoxSupport.setSelectedColor((ColorComboBox)this.cbBackground, (Color)category.getAttribute(StyleConstants.Background));
        this.listen = true;
    }

    private AttributeSet getDefaultColoring() {
        Collection<AttributeSet> defaults = this.colorModel.getCategories(this.currentProfile, ColorModel.ALL_LANGUAGES);
        for (AttributeSet as : defaults) {
            String name = (String)as.getAttribute(StyleConstants.NameAttribute);
            if (name == null || !"default".equals(name)) continue;
            return as;
        }
        return null;
    }

    private List<AttributeSet> getCategories(String profile) {
        if (this.colorModel == null) {
            return null;
        }
        if (!this.profileToCategories.containsKey(profile)) {
            Collection<AttributeSet> c = this.colorModel.getHighlightings(profile);
            if (c == null) {
                c = Collections.emptySet();
            }
            ArrayList<AttributeSet> l = new ArrayList<AttributeSet>(c);
            Collections.sort(l, new CategoryComparator());
            this.profileToCategories.put(profile, new ArrayList<AttributeSet>(l));
        }
        return this.profileToCategories.get(profile);
    }

    private List<AttributeSet> getDefaults(String profile) {
        if (!this.profileToDefaults.containsKey(profile)) {
            Collection<AttributeSet> c = this.colorModel.getHighlightingDefaults(profile);
            ArrayList<AttributeSet> l = new ArrayList<AttributeSet>(c);
            Collections.sort(l, new CategoryComparator());
            this.profileToDefaults.put(profile, l);
        }
        List<AttributeSet> defaultprofile = this.profileToDefaults.get(profile);
        return new ArrayList<AttributeSet>(defaultprofile);
    }

}

