/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 */
package org.netbeans.modules.options.colors;

import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.options.colors.AllLanguagesTokenId;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

class AllLanguagesLexer
implements Lexer<AllLanguagesTokenId> {
    private LexerRestartInfo<AllLanguagesTokenId> info;

    AllLanguagesLexer(LexerRestartInfo<AllLanguagesTokenId> info) {
        this.info = info;
    }

    public Token<AllLanguagesTokenId> nextToken() {
        LexerInput input = this.info.input();
        int i = input.read();
        switch (i) {
            case -1: {
                return null;
            }
            case 47: {
                i = input.read();
                if (i == 47) {
                    return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.COMMENT);
                }
                if (i == 42) {
                    i = input.read();
                    while (i != -1) {
                        while (i == 42) {
                            i = input.read();
                            if (i != 47) continue;
                            return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.COMMENT);
                        }
                        i = input.read();
                    }
                    return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.COMMENT);
                }
                if (i != -1) {
                    input.backup(1);
                }
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.OPERATOR);
            }
            case 43: 
            case 61: {
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.OPERATOR);
            }
            case 40: 
            case 41: 
            case 59: 
            case 123: 
            case 125: {
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.SEPARATOR);
            }
            case 9: 
            case 10: 
            case 13: 
            case 32: {
                while ((i = input.read()) == 32 || i == 10 || i == 13 || i == 9) {
                }
                if (i != -1) {
                    input.backup(1);
                }
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.WHITESPACE);
            }
            case 48: 
            case 49: 
            case 50: 
            case 51: 
            case 52: 
            case 53: 
            case 54: 
            case 55: 
            case 56: 
            case 57: {
                while ((i = input.read()) >= 48 && i <= 57) {
                }
                if (i == 46) {
                    while ((i = input.read()) >= 48 && i <= 57) {
                    }
                }
                input.backup(1);
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.NUMBER);
            }
            case 34: {
                do {
                    if ((i = input.read()) != 92) continue;
                    i = input.read();
                    i = input.read();
                } while (i != 34 && i != 10 && i != 13 && i != -1);
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.STRING);
            }
            case 39: {
                i = input.read();
                if (i == 92) {
                    i = input.read();
                }
                if ((i = input.read()) != 39) {
                    return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.ERROR);
                }
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.CHARACTER);
            }
        }
        if (i >= 97 && i <= 122 || i >= 65 && i <= 90) {
            while ((i = input.read()) >= 97 && i <= 122 || i >= 65 && i <= 90 || i >= 48 && i <= 57 || i == 95 || i == 45 || i == 126) {
            }
            input.backup(1);
            String id = input.readText().toString();
            if (id.equals("public") || id.equals("class")) {
                return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.KEYWORD);
            }
            return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.IDENTIFIER);
        }
        return this.info.tokenFactory().createToken((TokenId)AllLanguagesTokenId.ERROR);
    }

    public Object state() {
        return null;
    }

    public void release() {
    }
}

