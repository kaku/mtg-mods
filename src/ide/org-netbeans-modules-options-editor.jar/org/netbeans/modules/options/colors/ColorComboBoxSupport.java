/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ColorComboBox
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.awt.Color;
import java.util.HashSet;
import org.netbeans.modules.options.colors.ColorValue;
import org.openide.awt.ColorComboBox;
import org.openide.util.NbBundle;

public class ColorComboBoxSupport {
    private static ColorValue[] content = new ColorValue[]{new ColorValue(Color.BLACK), new ColorValue(Color.BLUE), new ColorValue(Color.CYAN), new ColorValue(Color.DARK_GRAY), new ColorValue(Color.GRAY), new ColorValue(Color.GREEN), new ColorValue(Color.LIGHT_GRAY), new ColorValue(Color.MAGENTA), new ColorValue(Color.ORANGE), new ColorValue(Color.PINK), new ColorValue(Color.RED), new ColorValue(Color.WHITE), new ColorValue(Color.YELLOW), new ColorValue(ColorComboBoxSupport.loc("CTL_None_Color"), null)};
    private static final HashSet<ColorComboBox> cbWithInheritedColor = new HashSet();

    static void setInheritedColor(ColorComboBox combo, Color color) {
        ColorValue[] ncontent = new ColorValue[content.length];
        System.arraycopy(content, 0, ncontent, 0, content.length);
        Color[] colors = new Color[content.length];
        String[] names = new String[content.length];
        for (int i = 0; i < colors.length; ++i) {
            colors[i] = ColorComboBoxSupport.content[i].color;
            names[i] = ColorComboBoxSupport.content[i].text;
        }
        if (color != null) {
            colors[ColorComboBoxSupport.content.length - 1] = color;
            names[ColorComboBoxSupport.content.length - 1] = ColorComboBoxSupport.loc("CTL_Inherited_Color");
            cbWithInheritedColor.add(combo);
        } else {
            colors[ColorComboBoxSupport.content.length - 1] = null;
            names[ColorComboBoxSupport.content.length - 1] = ColorComboBoxSupport.loc("CTL_None_Color");
        }
        combo.setModel(colors, names);
    }

    static void setSelectedColor(ColorComboBox combo, Color color) {
        if (null != color) {
            combo.setSelectedColor(color);
        } else {
            combo.setSelectedIndex(combo.getItemCount() - 2);
        }
    }

    static Color getSelectedColor(ColorComboBox combo) {
        int selIndex = combo.getSelectedIndex();
        if (selIndex == combo.getItemCount() - 2 && cbWithInheritedColor.contains((Object)combo)) {
            return null;
        }
        return combo.getSelectedColor();
    }

    private static String loc(String key) {
        return NbBundle.getMessage(ColorComboBoxSupport.class, (String)key);
    }
}

