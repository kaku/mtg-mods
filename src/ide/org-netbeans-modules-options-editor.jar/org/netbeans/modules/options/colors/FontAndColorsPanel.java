/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.colors;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.netbeans.modules.options.colors.ColorModel;
import org.netbeans.modules.options.colors.spi.FontsColorsController;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FontAndColorsPanel
extends JPanel
implements ActionListener {
    private final Collection<? extends FontsColorsController> panels;
    private ColorModel colorModel;
    private String currentProfile;
    private boolean listen = false;
    private JButton bDelete;
    private JButton bDuplicate;
    private JComboBox<String> cbProfile;
    private JLabel lProfile;
    private JTabbedPane tpCustomizers;

    public FontAndColorsPanel(Collection<? extends FontsColorsController> panels) {
        this.panels = panels;
        this.initComponents();
        this.cbProfile.getAccessibleContext().setAccessibleName(FontAndColorsPanel.loc("AN_Profiles"));
        this.cbProfile.getAccessibleContext().setAccessibleDescription(FontAndColorsPanel.loc("AD_Profiles"));
        this.bDelete.getAccessibleContext().setAccessibleName(FontAndColorsPanel.loc("AN_Delete"));
        this.bDelete.getAccessibleContext().setAccessibleDescription(FontAndColorsPanel.loc("AD_Delete"));
        this.bDuplicate.getAccessibleContext().setAccessibleName(FontAndColorsPanel.loc("AN_Clone"));
        this.bDuplicate.getAccessibleContext().setAccessibleDescription(FontAndColorsPanel.loc("AD_Clone"));
        this.tpCustomizers.getAccessibleContext().setAccessibleName(FontAndColorsPanel.loc("AN_Categories"));
        this.tpCustomizers.getAccessibleContext().setAccessibleDescription(FontAndColorsPanel.loc("AD_Categories"));
        FontAndColorsPanel.loc(this.lProfile, "CTL_Color_Profile_Name");
        this.cbProfile.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent evt) {
                if (!FontAndColorsPanel.this.listen) {
                    return;
                }
                FontAndColorsPanel.this.setCurrentProfile((String)FontAndColorsPanel.this.cbProfile.getSelectedItem());
            }
        });
        FontAndColorsPanel.loc(this.bDuplicate, "CTL_Create_New");
        this.bDuplicate.addActionListener(this);
        FontAndColorsPanel.loc(this.bDelete, "CTL_Delete");
        this.bDelete.addActionListener(this);
        JLabel label = new JLabel();
        for (FontsColorsController p : panels) {
            JComponent component = p.getComponent();
            component.setBorder(new EmptyBorder(8, 8, 8, 8));
            String tabName = component.getName();
            Mnemonics.setLocalizedText((JLabel)label, (String)tabName);
            this.tpCustomizers.addTab(label.getText(), component);
            int idx = Mnemonics.findMnemonicAmpersand((String)tabName);
            if (idx == -1 || idx + 1 >= tabName.length()) continue;
            int tabcount = this.tpCustomizers.getTabCount();
            assert (tabcount > 0);
            this.tpCustomizers.setMnemonicAt(tabcount - 1, Character.toUpperCase(tabName.charAt(idx + 1)));
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        for (ComponentListener l : this.getComponentListeners()) {
            super.removeComponentListener(l);
        }
    }

    private void initComponents() {
        this.lProfile = new JLabel();
        this.cbProfile = new JComboBox();
        this.tpCustomizers = new JTabbedPane();
        this.bDuplicate = new JButton();
        this.bDelete = new JButton();
        this.lProfile.setLabelFor(this.cbProfile);
        this.lProfile.setText("Profile:");
        this.bDuplicate.setText("Duplicate...");
        this.bDelete.setText("Delete");
        this.bDelete.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FontAndColorsPanel.this.bDeleteActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.lProfile).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbProfile, 0, 195, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.bDuplicate).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.bDelete)).addComponent(this.tpCustomizers, -1, 502, 32767));
        layout.linkSize(0, this.bDelete, this.bDuplicate);
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lProfile).addComponent(this.bDelete).addComponent(this.bDuplicate).addComponent(this.cbProfile, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.tpCustomizers, -1, 287, 32767)));
    }

    private void bDeleteActionPerformed(ActionEvent evt) {
    }

    private void setCurrentProfile(String profile) {
        if (this.colorModel.isCustomProfile(profile)) {
            FontAndColorsPanel.loc(this.bDelete, "CTL_Delete");
        } else {
            FontAndColorsPanel.loc(this.bDelete, "CTL_Restore");
        }
        this.currentProfile = profile;
        for (FontsColorsController c : this.panels) {
            c.setCurrentProfile(this.currentProfile);
        }
    }

    private void deleteCurrentProfile() {
        String currentProfile = (String)this.cbProfile.getSelectedItem();
        for (FontsColorsController c : this.panels) {
            c.deleteProfile(currentProfile);
        }
        if (this.colorModel.isCustomProfile(currentProfile)) {
            this.cbProfile.removeItem(currentProfile);
            this.cbProfile.setSelectedIndex(0);
        }
    }

    void update() {
        this.colorModel = new ColorModel();
        for (FontsColorsController c : this.panels) {
            c.update(this.colorModel);
        }
        this.currentProfile = this.colorModel.getCurrentProfile();
        if (this.colorModel.isCustomProfile(this.currentProfile)) {
            FontAndColorsPanel.loc(this.bDelete, "CTL_Delete");
        } else {
            FontAndColorsPanel.loc(this.bDelete, "CTL_Restore");
        }
        this.listen = false;
        Iterator<String> it = this.colorModel.getProfiles().iterator();
        this.cbProfile.removeAllItems();
        while (it.hasNext()) {
            this.cbProfile.addItem(it.next());
        }
        this.listen = true;
        this.cbProfile.setSelectedItem(this.currentProfile);
    }

    void applyChanges() {
        for (FontsColorsController c : this.panels) {
            c.applyChanges();
        }
        if (this.colorModel == null) {
            return;
        }
        this.colorModel.setCurrentProfile(this.currentProfile);
    }

    void cancel() {
        for (FontsColorsController c : this.panels) {
            c.cancel();
        }
    }

    boolean dataValid() {
        return true;
    }

    boolean isChanged() {
        if (this.currentProfile != null && this.colorModel != null && !this.currentProfile.equals(this.colorModel.getCurrentProfile())) {
            return true;
        }
        for (FontsColorsController c : this.panels) {
            if (!c.isChanged()) continue;
            return true;
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!this.listen) {
            return;
        }
        if (e.getSource() == this.bDuplicate) {
            NotifyDescriptor.InputLine il = new NotifyDescriptor.InputLine(FontAndColorsPanel.loc("CTL_Create_New_Profile_Message"), FontAndColorsPanel.loc("CTL_Create_New_Profile_Title"));
            il.setInputText(this.currentProfile);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)il);
            if (il.getValue() == NotifyDescriptor.OK_OPTION) {
                String newScheme = il.getInputText();
                for (int i = 0; i < this.cbProfile.getItemCount(); ++i) {
                    if (!newScheme.equals(this.cbProfile.getItemAt(i))) continue;
                    NotifyDescriptor.Message md = new NotifyDescriptor.Message((Object)FontAndColorsPanel.loc("CTL_Duplicate_Profile_Name"), 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)md);
                    return;
                }
                this.setCurrentProfile(newScheme);
                this.listen = false;
                this.cbProfile.addItem(il.getInputText());
                this.cbProfile.setSelectedItem(il.getInputText());
                this.listen = true;
            }
            return;
        }
        if (e.getSource() == this.bDelete) {
            this.deleteCurrentProfile();
            return;
        }
    }

    private static String loc(String key) {
        return NbBundle.getMessage(FontAndColorsPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)FontAndColorsPanel.loc(key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)FontAndColorsPanel.loc(key));
        }
    }

}

