/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.colors.spi;

import javax.swing.JComponent;
import org.netbeans.modules.options.colors.ColorModel;

public interface FontsColorsController {
    public void update(ColorModel var1);

    public void setCurrentProfile(String var1);

    public void deleteProfile(String var1);

    public void applyChanges();

    public void cancel();

    public boolean isChanged();

    public JComponent getComponent();
}

