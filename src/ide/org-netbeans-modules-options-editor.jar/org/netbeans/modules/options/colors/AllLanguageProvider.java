/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.netbeans.spi.lexer.LanguageProvider
 */
package org.netbeans.modules.options.colors;

import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.modules.options.colors.AllLanguageHierarchy;
import org.netbeans.modules.options.colors.AllLanguagesTokenId;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageProvider;

public class AllLanguageProvider
extends LanguageProvider {
    public Language<AllLanguagesTokenId> findLanguage(String mimeType) {
        if ("text/x-all-languages".equals(mimeType)) {
            return new AllLanguageHierarchy().language();
        }
        return null;
    }

    public LanguageEmbedding<?> findLanguageEmbedding(Token arg0, LanguagePath arg1, InputAttributes arg2) {
        return null;
    }
}

