/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.editor.completion;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class GeneralCompletionOptionsPanel
extends JPanel {
    private Preferences preferences;
    private final Map<String, Boolean> id2Saved = new HashMap<String, Boolean>();
    private JCheckBox cbAutoPopup;
    private JCheckBox cbCaseSensitive;
    private JCheckBox cbDocsAutoPopup;
    private JCheckBox cbInsertClosingBracketsAutomatically;
    private JCheckBox cbInsertSingleProposalsAutomatically;
    private JCheckBox cbJavadocNextToCC;
    private JCheckBox cbShowDeprecated;

    public GeneralCompletionOptionsPanel(Preferences p) {
        this.initComponents();
        this.preferences = p;
        this.cbInsertClosingBracketsAutomatically.setSelected(this.preferences.getBoolean("pair-characters-completion", true));
        this.cbAutoPopup.setSelected(this.preferences.getBoolean("completion-auto-popup", true));
        this.cbDocsAutoPopup.setSelected(this.preferences.getBoolean("javadoc-auto-popup", true));
        this.cbJavadocNextToCC.setSelected(this.preferences.getBoolean("javadoc-popup-next-to-cc", false));
        this.cbShowDeprecated.setSelected(this.preferences.getBoolean("show-deprecated-members", true));
        this.cbInsertSingleProposalsAutomatically.setSelected(this.preferences.getBoolean("completion-instant-substitution", true));
        this.cbCaseSensitive.setSelected(this.preferences.getBoolean("completion-case-sensitive", true));
        this.id2Saved.put("pair-characters-completion", this.cbInsertClosingBracketsAutomatically.isSelected());
        this.id2Saved.put("completion-auto-popup", this.cbAutoPopup.isSelected());
        this.id2Saved.put("javadoc-auto-popup", this.cbDocsAutoPopup.isSelected());
        this.id2Saved.put("javadoc-popup-next-to-cc", this.cbJavadocNextToCC.isSelected());
        this.id2Saved.put("show-deprecated-members", this.cbShowDeprecated.isSelected());
        this.id2Saved.put("completion-instant-substitution", this.cbInsertSingleProposalsAutomatically.isSelected());
        this.id2Saved.put("completion-case-sensitive", this.cbCaseSensitive.isSelected());
    }

    String getSavedValue(String key) {
        return Boolean.toString(this.id2Saved.get(key));
    }

    private void initComponents() {
        this.cbAutoPopup = new JCheckBox();
        this.cbDocsAutoPopup = new JCheckBox();
        this.cbJavadocNextToCC = new JCheckBox();
        this.cbInsertSingleProposalsAutomatically = new JCheckBox();
        this.cbCaseSensitive = new JCheckBox();
        this.cbShowDeprecated = new JCheckBox();
        this.cbInsertClosingBracketsAutomatically = new JCheckBox();
        this.setForeground(new Color(99, 130, 191));
        Mnemonics.setLocalizedText((AbstractButton)this.cbAutoPopup, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Auto_Popup_Completion_Window"));
        this.cbAutoPopup.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbAutoPopupActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cbDocsAutoPopup, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Auto_Popup_Documentation_Window"));
        this.cbDocsAutoPopup.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbDocsAutoPopupActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cbJavadocNextToCC, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Javadoc_Next_To_CC"));
        this.cbJavadocNextToCC.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbJavadocNextToCCActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cbInsertSingleProposalsAutomatically, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Insert_Single_Proposals_Automatically"));
        this.cbInsertSingleProposalsAutomatically.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbInsertSingleProposalsAutomaticallyActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cbCaseSensitive, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Case_Sensitive_Code_Completion"));
        this.cbCaseSensitive.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbCaseSensitiveActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cbShowDeprecated, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Show_Deprecated_Members"));
        this.cbShowDeprecated.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbShowDeprecatedActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.cbInsertClosingBracketsAutomatically, (String)NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"CTL_Pair_Character_Completion"));
        this.cbInsertClosingBracketsAutomatically.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                GeneralCompletionOptionsPanel.this.cbInsertClosingBracketsAutomaticallyActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.cbInsertClosingBracketsAutomatically).addComponent(this.cbShowDeprecated).addComponent(this.cbCaseSensitive).addComponent(this.cbDocsAutoPopup).addComponent(this.cbAutoPopup).addComponent(this.cbInsertSingleProposalsAutomatically).addComponent(this.cbJavadocNextToCC)).addContainerGap(46, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(6, 6, 6).addComponent(this.cbAutoPopup).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbDocsAutoPopup).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbJavadocNextToCC).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbInsertSingleProposalsAutomatically).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbCaseSensitive).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbShowDeprecated).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbInsertClosingBracketsAutomatically).addContainerGap(40, 32767)));
        this.cbAutoPopup.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Auto_Popup_Completion_Window"));
        this.cbAutoPopup.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Auto_Popup_Completion_Window"));
        this.cbDocsAutoPopup.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Auto_Popup_Documentation_Window"));
        this.cbDocsAutoPopup.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Auto_Popup_Documentation_Window"));
        this.cbJavadocNextToCC.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Javadoc_Next_To_CC"));
        this.cbJavadocNextToCC.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Javadoc_Next_To_CC"));
        this.cbInsertSingleProposalsAutomatically.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Insert_Single_Proposals_Automatically"));
        this.cbInsertSingleProposalsAutomatically.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Insert_Single_Proposals_Automatically"));
        this.cbCaseSensitive.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Case_Sensitive_Code_Completion"));
        this.cbCaseSensitive.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Case_Sensitive_Code_Completion"));
        this.cbShowDeprecated.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Show_Deprecated_Members"));
        this.cbShowDeprecated.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Show_Deprecated_Members"));
        this.cbInsertClosingBracketsAutomatically.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AN_Pair_Character_Completion"));
        this.cbInsertClosingBracketsAutomatically.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"AD_Pair_Character_Completion"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"GeneralCompletionOptionsPanel.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(GeneralCompletionOptionsPanel.class, (String)"GeneralCompletionOptionsPanel.AccessibleContext.accessibleDescription"));
    }

    private void cbAutoPopupActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("completion-auto-popup", this.cbAutoPopup.isSelected());
    }

    private void cbDocsAutoPopupActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("javadoc-auto-popup", this.cbDocsAutoPopup.isSelected());
    }

    private void cbInsertSingleProposalsAutomaticallyActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("completion-instant-substitution", this.cbInsertSingleProposalsAutomatically.isSelected());
    }

    private void cbCaseSensitiveActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("completion-case-sensitive", this.cbCaseSensitive.isSelected());
    }

    private void cbShowDeprecatedActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("show-deprecated-members", this.cbShowDeprecated.isSelected());
    }

    private void cbInsertClosingBracketsAutomaticallyActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("pair-characters-completion", this.cbInsertClosingBracketsAutomatically.isSelected());
    }

    private void cbJavadocNextToCCActionPerformed(ActionEvent evt) {
        this.preferences.putBoolean("javadoc-popup-next-to-cc", this.cbJavadocNextToCC.isSelected());
    }

}

