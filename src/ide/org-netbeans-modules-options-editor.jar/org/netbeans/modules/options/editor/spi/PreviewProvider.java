/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.editor.spi;

import javax.swing.JComponent;

public interface PreviewProvider {
    public JComponent getPreviewComponent();

    public void refreshPreview();
}

