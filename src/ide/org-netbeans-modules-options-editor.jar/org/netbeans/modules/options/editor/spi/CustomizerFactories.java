/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.HelpCtx
 */
package org.netbeans.modules.options.editor.spi;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.netbeans.modules.options.indentation.IndentationPanel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.HelpCtx;

final class CustomizerFactories {
    private static final Logger LOG = Logger.getLogger(CustomizerFactories.class.getName());

    CustomizerFactories() {
    }

    public static PreferencesCustomizer.Factory createDefaultTabsAndIndentsCustomizerFactory(final FileObject file) {
        return new PreferencesCustomizer.Factory(){

            @Override
            public PreferencesCustomizer create(Preferences preferences) {
                FileObject previewTextFile;
                String path;
                String mimeType;
                String customizerMimeType = null;
                PreviewProvider preview = null;
                String folderPath = file.getParent().getPath();
                if (folderPath.startsWith("OptionsDialog/Editor/Formatting/") && MimePath.validate((CharSequence)(mimeType = folderPath.substring("OptionsDialog/Editor/Formatting/".length())))) {
                    customizerMimeType = mimeType;
                }
                if ((path = (String)file.getAttribute("previewTextFile")) != null && (previewTextFile = FileUtil.getConfigFile((String)path)) != null) {
                    try {
                        preview = new IndentationPanel.TextPreview(preferences, customizerMimeType, previewTextFile);
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.WARNING, null, ioe);
                    }
                }
                if (preview == null && customizerMimeType != null && (previewTextFile = FileUtil.getConfigFile((String)("OptionsDialog/PreviewExamples/" + customizerMimeType))) != null && previewTextFile.isData()) {
                    try {
                        preview = new IndentationPanel.TextPreview(preferences, customizerMimeType, previewTextFile);
                    }
                    catch (IOException ioe) {
                        LOG.log(Level.WARNING, null, ioe);
                    }
                }
                if (preview == null) {
                    preview = new IndentationPanel.NoPreview();
                }
                return new SimpleTabsAndIndentsCustomizer(preview);
            }
        };
    }

    private static final class SimpleTabsAndIndentsCustomizer
    implements PreferencesCustomizer,
    PreviewProvider {
        private final PreviewProvider preview;

        public SimpleTabsAndIndentsCustomizer(PreviewProvider preview) {
            this.preview = preview;
        }

        @Override
        public String getId() {
            return "tabs-and-indents";
        }

        @Override
        public String getDisplayName() {
            return "tabs-and-indents";
        }

        @Override
        public HelpCtx getHelpCtx() {
            return null;
        }

        @Override
        public JComponent getComponent() {
            return new JPanel();
        }

        @Override
        public JComponent getPreviewComponent() {
            return this.preview.getPreviewComponent();
        }

        @Override
        public void refreshPreview() {
            this.preview.refreshPreview();
        }
    }

}

