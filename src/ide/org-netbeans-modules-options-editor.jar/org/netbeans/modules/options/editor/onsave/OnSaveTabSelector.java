/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.options.editor.onsave;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.onsave.OnSaveTabPanelController;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public final class OnSaveTabSelector {
    public static final String ON_SAVE_CUSTOMIZERS_FOLDER = "OptionsDialog/Editor/OnSave/";
    private final Map<String, PreferencesCustomizer> allCustomizers = new HashMap<String, PreferencesCustomizer>();
    private final PropertyChangeSupport pcs;
    private final OnSaveTabPanelController.PreferencesFactory preferencesFactory;
    private HashMap<String, MimeEntry> mimeType2Language;
    private List<String> sortedMimeTypes;
    private String selectedMimeType;

    OnSaveTabSelector(OnSaveTabPanelController.PreferencesFactory pf) {
        this.pcs = new PropertyChangeSupport(this);
        this.preferencesFactory = pf;
    }

    public synchronized List<String> getMimeTypes() {
        this.checkMimeTypesInited();
        return this.sortedMimeTypes;
    }

    public synchronized String getLanguageName(String mimeType) {
        this.checkMimeTypesInited();
        MimeEntry mimeEntry = this.mimeType2Language.get(mimeType);
        return mimeEntry != null ? mimeEntry.languageName : null;
    }

    public Preferences getPreferences(String mimeType) {
        return this.preferencesFactory.getPreferences(mimeType);
    }

    private void checkMimeTypesInited() {
        if (this.mimeType2Language == null) {
            Set allMimeTypes = EditorSettings.getDefault().getAllMimeTypes();
            this.mimeType2Language = new HashMap(allMimeTypes.size() + 1 << 1);
            String allLanguagesMimeType = "";
            String allLanguages = NbBundle.getMessage(OnSaveTabSelector.class, (String)"LBL_AllLanguages");
            this.mimeType2Language.put(allLanguagesMimeType, new MimeEntry(allLanguagesMimeType, allLanguages));
            ArrayList<MimeEntry> mimeEntries = new ArrayList<MimeEntry>(allMimeTypes.size());
            for (String mimeType : allMimeTypes) {
                String language;
                MimePath mimePath = MimePath.parse((String)mimeType);
                if (mimePath.size() > 1 || OnSaveTabSelector.isCompoundMimeType(mimeType) || (language = EditorSettings.getDefault().getLanguageName(mimeType)).equals(mimeType)) continue;
                MimeEntry mimeEntry = new MimeEntry(mimeType, language);
                this.mimeType2Language.put(mimeType, mimeEntry);
                mimeEntries.add(mimeEntry);
            }
            Collections.sort(mimeEntries);
            this.sortedMimeTypes = new ArrayList<String>(mimeEntries.size() + 1);
            this.sortedMimeTypes.add(allLanguagesMimeType);
            for (MimeEntry mimeEntry : mimeEntries) {
                this.sortedMimeTypes.add(mimeEntry.mimeType);
            }
        }
    }

    private static boolean isCompoundMimeType(String mimeType) {
        int idx = mimeType.lastIndexOf(43);
        return idx != -1 && idx < mimeType.length() - 1;
    }

    public String getSelectedMimeType() {
        return this.selectedMimeType;
    }

    public synchronized void setSelectedMimeType(String mimeType) {
        assert (this.getMimeTypes().contains(mimeType));
        if (this.selectedMimeType == null || !this.selectedMimeType.equals(mimeType)) {
            String old = this.selectedMimeType;
            this.selectedMimeType = mimeType;
            this.pcs.firePropertyChange(null, old, mimeType);
        }
    }

    public synchronized PreferencesCustomizer getSelectedCustomizer() {
        return this.getCustomizer(this.selectedMimeType);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    String getSavedValue(String mimeType, String key) {
        Lookup l;
        PreferencesCustomizer.CustomCustomizer customizer;
        PreferencesCustomizer prefsCustomizer = this.getCustomizer(mimeType);
        if (prefsCustomizer != null && (customizer = (PreferencesCustomizer.CustomCustomizer)(l = Lookups.forPath((String)("OptionsDialog/Editor/OnSave/" + mimeType))).lookup(PreferencesCustomizer.CustomCustomizer.class)) != null) {
            return customizer.getSavedValue(prefsCustomizer, key);
        }
        return null;
    }

    private PreferencesCustomizer getCustomizer(String mimeType) {
        PreferencesCustomizer customizer = this.allCustomizers.get(mimeType);
        if (customizer == null) {
            Preferences prefs = this.preferencesFactory.getPreferences(mimeType);
            if (mimeType.length() > 0) {
                Lookup l = Lookups.forPath((String)("OptionsDialog/Editor/OnSave/" + mimeType));
                PreferencesCustomizer.Factory factory = (PreferencesCustomizer.Factory)l.lookup(PreferencesCustomizer.Factory.class);
                if (factory != null) {
                    customizer = factory.create(prefs);
                }
            } else {
                customizer = null;
            }
            this.allCustomizers.put(this.selectedMimeType, customizer);
        }
        return customizer;
    }

    private static final class MimeEntry
    implements Comparable<MimeEntry> {
        final String mimeType;
        final String languageName;

        public MimeEntry(String mimeType, String languageName) {
            this.mimeType = mimeType;
            this.languageName = languageName;
        }

        @Override
        public int compareTo(MimeEntry mimeEntry) {
            return this.languageName.compareToIgnoreCase(mimeEntry.languageName);
        }

        public String toString() {
            return "mimeType=\"" + this.mimeType + "\", languageName=\"" + this.languageName + "\"";
        }
    }

}

