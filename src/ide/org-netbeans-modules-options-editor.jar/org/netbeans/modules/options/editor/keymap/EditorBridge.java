/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.netbeans.core.options.keymap.api.ShortcutAction
 *  org.netbeans.core.options.keymap.spi.KeymapManager
 *  org.netbeans.editor.BaseKit
 *  org.netbeans.modules.editor.NbEditorKit
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.KeyBindingSettingsFactory
 *  org.openide.ErrorManager
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.options.editor.keymap;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.MultiKeyBinding;
import org.netbeans.core.options.keymap.api.ShortcutAction;
import org.netbeans.core.options.keymap.spi.KeymapManager;
import org.netbeans.editor.BaseKit;
import org.netbeans.modules.editor.NbEditorKit;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.KeyBindingSettingsFactory;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public final class EditorBridge
extends KeymapManager {
    private static final Logger LOG = Logger.getLogger(EditorBridge.class.getName());
    private static final String EDITOR_BRIDGE = "EditorBridge";
    private Map<String, Set<ShortcutAction>> actions;
    private Map<String, EditorAction> editorActionsMap;
    private Map<String, Set<String>> actionNameToMimeTypes = new HashMap<String, Set<String>>();
    private EditorSettings editorSettings;
    private final Map<String, KeyBindingSettingsFactory> keyBindingSettings = new HashMap<String, KeyBindingSettingsFactory>();
    private static final String[] EMPTY = new String[0];
    private Listener listener;

    public EditorBridge() {
        super("EditorBridge");
    }

    public Map<String, Set<ShortcutAction>> getActions() {
        if (this.actions == null) {
            Map<String, String> categories = EditorBridge.readCategories();
            this.actions = new HashMap<String, Set<ShortcutAction>>();
            Map<String, EditorAction> tmpEditorActionsMap = Collections.unmodifiableMap(this.getEditorActionsMap());
            for (EditorAction action : tmpEditorActionsMap.values()) {
                Set<ShortcutAction> a;
                String category = categories.get(action.getId());
                if (category == null) {
                    category = NbBundle.getMessage(EditorBridge.class, (String)"CTL_Other");
                }
                if ((a = this.actions.get(category)) == null) {
                    a = new HashSet<ShortcutAction>();
                    this.actions.put(category, a);
                }
                a.add(action);
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine("Action='" + action.getId() + "' -> Category='" + category + "'");
            }
            this.actions.remove("Hidden");
        }
        return this.actions;
    }

    public void refreshActions() {
        this.editorActionsMap = null;
        this.actions = null;
        this.actionNameToMimeTypes = new HashMap<String, Set<String>>();
    }

    public String getCurrentProfile() {
        return this.getEditorSettings().getCurrentKeyMapProfile();
    }

    public void setCurrentProfile(String profile) {
        this.getEditorSettings().setCurrentKeyMapProfile(profile);
    }

    public boolean isCustomProfile(String profile) {
        return this.getEditorSettings().isCustomKeymapProfile(profile);
    }

    public Map<ShortcutAction, Set<String>> getKeymap(String profile) {
        HashMap<ShortcutAction, Set<String>> result = new HashMap<ShortcutAction, Set<String>>();
        this.readKeymap(profile, null, false, result);
        for (String mimeType : this.getEditorSettings().getMimeTypes()) {
            this.readKeymap(profile, mimeType, false, result);
        }
        return Collections.unmodifiableMap(result);
    }

    public Map<ShortcutAction, Set<String>> getDefaultKeymap(String profile) {
        HashMap<ShortcutAction, Set<String>> result = new HashMap<ShortcutAction, Set<String>>();
        this.readKeymap(profile, null, true, result);
        for (String mimeType : this.getEditorSettings().getMimeTypes()) {
            this.readKeymap(profile, mimeType, true, result);
        }
        return Collections.unmodifiableMap(result);
    }

    public void deleteProfile(String profile) {
        KeyBindingSettingsFactory kbs = this.getKeyBindingSettings(null);
        kbs.setKeyBindings(profile, null);
    }

    public void saveKeymap(String profile, Map<ShortcutAction, Set<String>> actionToShortcuts) {
        HashSet<Object> allMimes = new HashSet<Object>(this.getEditorSettings().getMimeTypes());
        allMimes.add(null);
        HashSet<String> actionIds = new HashSet<String>(actionToShortcuts.size());
        for (ShortcutAction action : actionToShortcuts.keySet()) {
            if (!((action = action.getKeymapManagerInstance("EditorBridge")) instanceof EditorAction)) continue;
            actionIds.add(((EditorAction)action).getId());
        }
        HashMap undefinedActions = new HashMap(allMimes.size());
        for (String s : allMimes) {
            KeyBindingSettingsFactory f = this.getKeyBindingSettings(s);
            List mkbs = f.getKeyBindings(profile);
            HashMap<String, ArrayList<MultiKeyBinding>> actionToMkb = new HashMap<String, ArrayList<MultiKeyBinding>>();
            for (MultiKeyBinding mkb : mkbs) {
                String an = mkb.getActionName();
                if (!actionIds.contains(an)) continue;
                ArrayList<MultiKeyBinding> bindings = (ArrayList<MultiKeyBinding>)actionToMkb.get(an);
                if (bindings == null) {
                    bindings = new ArrayList<MultiKeyBinding>(2);
                    actionToMkb.put(an, bindings);
                }
                bindings.add(mkb);
            }
            undefinedActions.put(s, actionToMkb);
        }
        HashMap mimeTypeToKeyBinding = new HashMap();
        for (ShortcutAction action2 : actionToShortcuts.keySet()) {
            Set<String> shortcuts = actionToShortcuts.get((Object)action2);
            if (!((action2 = action2.getKeymapManagerInstance("EditorBridge")) instanceof EditorAction)) continue;
            EditorAction editorAction = (EditorAction)action2;
            Set<String> mimeTypesPre = this.getMimeTypes(editorAction);
            assert (mimeTypesPre != null);
            LinkedHashSet<String> mimeTypes = new LinkedHashSet<String>(mimeTypesPre.size() + 1);
            mimeTypes.add(null);
            mimeTypes.addAll(mimeTypesPre);
            ArrayList<MultiKeyBinding> nullBindings = new ArrayList<MultiKeyBinding>();
            for (String shortcut : shortcuts) {
                MultiKeyBinding mkb = new MultiKeyBinding(EditorBridge.stringToKeyStrokes2(shortcut), editorAction.getId());
                boolean presentInDefault = mimeTypesPre.contains(null);
                for (String mimeType : mimeTypes) {
                    ArrayList<MultiKeyBinding> l;
                    boolean exists;
                    Map mimeActions = (Map)undefinedActions.get(mimeType);
                    boolean bl = exists = mimeActions != null && mimeActions.get(editorAction.getId()) != null;
                    if (mimeType != null) {
                        if (!exists && (presentInDefault || nullBindings.contains((Object)mkb))) {
                            continue;
                        }
                    } else {
                        if (!presentInDefault && !exists) continue;
                        nullBindings.add(mkb);
                        presentInDefault = true;
                    }
                    if ((l = (ArrayList<MultiKeyBinding>)mimeTypeToKeyBinding.get(mimeType)) == null) {
                        l = new ArrayList<MultiKeyBinding>();
                        mimeTypeToKeyBinding.put(mimeType, l);
                    }
                    l.add(mkb);
                }
            }
            for (String mimeType : mimeTypes) {
                Map m = (Map)undefinedActions.get(mimeType);
                if (m == null) continue;
                m.remove(editorAction.getId());
            }
        }
        for (String mimeType2 : this.keyBindingSettings.keySet()) {
            Map actionToMkb = (Map)undefinedActions.get(mimeType2);
            ArrayList l = (ArrayList)mimeTypeToKeyBinding.get(mimeType2);
            if (l == null) {
                l = new ArrayList(5);
                mimeTypeToKeyBinding.put(mimeType2, l);
            }
            for (List keys : actionToMkb.values()) {
                l.addAll(keys);
            }
        }
        for (String mimeType : this.keyBindingSettings.keySet()) {
            KeyBindingSettingsFactory kbs = this.keyBindingSettings.get(mimeType);
            kbs.setKeyBindings(profile, (List)mimeTypeToKeyBinding.get(mimeType));
        }
    }

    Map<String, EditorAction> getEditorActionsMap() {
        if (this.editorActionsMap == null) {
            this.editorActionsMap = new LinkedHashMap<String, EditorAction>();
            this.initActionMap(null, null);
            HashMap<String, EditorAction> emptyMimePathActions = new HashMap<String, EditorAction>(this.editorActionsMap);
            for (String mimeType : this.getEditorSettings().getMimeTypes()) {
                this.initActionMap(mimeType, emptyMimePathActions);
            }
        }
        return this.editorActionsMap;
    }

    private Set<String> getMimeTypes(EditorAction a) {
        this.getEditorActionsMap();
        return this.actionNameToMimeTypes.get(a.getId());
    }

    private void initActionMap(String mimeType, Map<String, EditorAction> emptyMimePathActions) {
        Object editorKit = null;
        if (mimeType == null) {
            editorKit = BaseKit.getKit(NbEditorKit.class);
        } else {
            Lookup mimeLookup = MimeLookup.getLookup((MimePath)MimePath.parse((String)mimeType));
            editorKit = (EditorKit)mimeLookup.lookup(EditorKit.class);
        }
        if (editorKit == null) {
            if (LOG.isLoggable(Level.WARNING)) {
                LOG.fine("EditorKit not found for: " + mimeType);
            }
            return;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Loading actions for '" + (mimeType == null ? "" : mimeType) + "' using " + editorKit);
        }
        Action[] as = editorKit.getActions();
        for (int i = 0; i < as.length; ++i) {
            Set<String> s;
            Object isHidden = as[i].getValue("no-keybinding");
            if (isHidden instanceof Boolean && ((Boolean)isHidden).booleanValue()) {
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine("! Action '" + as[i].getValue("Name") + "' is hidden, ignoring");
                continue;
            }
            EditorAction action = new EditorAction(as[i]);
            String id = action.getId();
            if (emptyMimePathActions != null && emptyMimePathActions.containsKey(id)) {
                if (!LOG.isLoggable(Level.FINEST)) continue;
                LOG.finest("Action '" + id + "' was already listed among all alnguages actions, skipping");
                continue;
            }
            this.getEditorActionsMap().put(id, action);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Action '" + id + "' loaded for '" + (mimeType == null ? "" : mimeType) + "'");
            }
            if ((s = this.actionNameToMimeTypes.get(id)) == null) {
                s = new LinkedHashSet<String>();
                this.actionNameToMimeTypes.put(id, s);
            }
            s.add(mimeType);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Actions for '" + (mimeType == null ? "" : mimeType) + "' loaded successfully");
        }
    }

    private EditorSettings getEditorSettings() {
        if (this.editorSettings == null) {
            this.editorSettings = EditorSettings.getDefault();
        }
        return this.editorSettings;
    }

    private KeyBindingSettingsFactory getKeyBindingSettings(String mimeType) {
        KeyBindingSettingsFactory kbs = this.keyBindingSettings.get(mimeType);
        if (kbs == null) {
            String[] arrstring;
            if (mimeType == null) {
                arrstring = EMPTY;
            } else {
                String[] arrstring2 = new String[1];
                arrstring = arrstring2;
                arrstring2[0] = mimeType;
            }
            kbs = EditorSettings.getDefault().getKeyBindingSettings(arrstring);
            this.keyBindingSettings.put(mimeType, kbs);
            this.getListener().add(kbs);
        }
        return kbs;
    }

    private Listener getListener() {
        if (this.listener == null) {
            this.listener = new Listener(this);
        }
        return this.listener;
    }

    private void readKeymap(String profile, String mimeType, boolean defaults, Map<ShortcutAction, Set<String>> map) {
        List keyBindings;
        KeyBindingSettingsFactory kbs = this.getKeyBindingSettings(mimeType);
        if (kbs == null) {
            return;
        }
        List list = keyBindings = defaults ? kbs.getKeyBindingDefaults(profile) : kbs.getKeyBindings(profile);
        if (keyBindings == null) {
            return;
        }
        Map<String, Set<String>> actionNameToShortcuts = EditorBridge.convertKeymap(keyBindings);
        for (String actionName : actionNameToShortcuts.keySet()) {
            Set<String> keyStrokes = actionNameToShortcuts.get(actionName);
            ShortcutAction action = this.getEditorActionsMap().get(actionName);
            if (action == null) {
                if (!LOG.isLoggable(Level.FINE)) continue;
                LOG.fine("action not found " + actionName);
                continue;
            }
            Set<String> s = map.get((Object)action);
            if (s == null) {
                s = new HashSet<String>();
                map.put(action, s);
            }
            s.addAll(keyStrokes);
        }
    }

    private static Map<String, Set<String>> convertKeymap(List<MultiKeyBinding> keyBindings) {
        HashMap<String, Set<String>> actionNameToShortcuts = new HashMap<String, Set<String>>();
        for (int i = 0; i < keyBindings.size(); ++i) {
            MultiKeyBinding mkb = keyBindings.get(i);
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < mkb.getKeyStrokeCount(); ++j) {
                if (j > 0) {
                    sb.append(' ');
                }
                sb.append(Utilities.keyToString((KeyStroke)((KeyStroke)mkb.getKeyStrokeList().get(j)), (boolean)true));
            }
            Set<String> keyStrokes = actionNameToShortcuts.get(mkb.getActionName());
            if (keyStrokes == null) {
                keyStrokes = new HashSet<String>();
                actionNameToShortcuts.put(mkb.getActionName(), keyStrokes);
            }
            keyStrokes.add(sb.toString());
        }
        return actionNameToShortcuts;
    }

    private static Map<String, String> readCategories() {
        HashMap<String, String> result = new HashMap<String, String>();
        FileObject fo = FileUtil.getConfigFile((String)"OptionsDialog/Actions");
        if (fo == null) {
            return result;
        }
        FileObject[] categories = fo.getChildren();
        for (int i = 0; i < categories.length; ++i) {
            String categoryName = categories[i].getName();
            String bundleName = (String)categories[i].getAttribute("SystemFileSystem.localizingBundle");
            if (bundleName != null) {
                try {
                    categoryName = NbBundle.getBundle((String)bundleName).getString(categories[i].getPath());
                }
                catch (MissingResourceException ex) {
                    ErrorManager.getDefault().notify((Throwable)ex);
                }
            }
            FileObject[] actions = categories[i].getChildren();
            for (int j = 0; j < actions.length; ++j) {
                if (actions[j].getExt().length() > 0) continue;
                String actionName = actions[j].getName();
                result.put(actionName, categoryName);
            }
        }
        return result;
    }

    public List<String> getProfiles() {
        return null;
    }

    private static KeyStroke[] stringToKeyStrokes2(String key) {
        ArrayList<KeyStroke> result = new ArrayList<KeyStroke>();
        StringTokenizer st = new StringTokenizer(key, " ");
        while (st.hasMoreTokens()) {
            String ks = st.nextToken().trim();
            KeyStroke keyStroke = Utilities.stringToKey((String)ks);
            if (keyStroke == null) {
                LOG.warning("'" + ks + "' is not a valid keystroke");
                return null;
            }
            result.add(keyStroke);
        }
        return result.toArray(new KeyStroke[result.size()]);
    }

    private static final class EditorAction
    implements ShortcutAction {
        private Action action;
        private String name;
        private String id;
        private String delegaitngActionId;

        public EditorAction(Action a) {
            this.action = a;
        }

        public String getDisplayName() {
            if (this.name == null) {
                try {
                    this.name = (String)this.action.getValue("ShortDescription");
                }
                catch (MissingResourceException mre) {
                    Throwable t = new Throwable("The action " + this.action + " crashed when accessing its short description.", mre);
                    LOG.log(Level.WARNING, null, t);
                    this.name = null;
                }
                if (this.name == null) {
                    LOG.warning("The action " + this.action + " doesn't provide short description, using its name.");
                    this.name = this.getId();
                }
                this.name = this.name.replaceAll("&", "").trim();
            }
            return this.name;
        }

        public String getId() {
            if (this.id == null) {
                this.id = (String)this.action.getValue("Name");
                assert (this.id != null);
            }
            return this.id;
        }

        public String getDelegatingActionId() {
            if (this.delegaitngActionId == null) {
                this.delegaitngActionId = (String)this.action.getValue("systemActionClassName");
                if (this.delegaitngActionId != null) {
                    this.delegaitngActionId = this.delegaitngActionId.replaceAll("\\.", "-");
                }
            }
            return this.delegaitngActionId;
        }

        public boolean equals(Object o) {
            if (!(o instanceof EditorAction)) {
                return false;
            }
            return ((EditorAction)o).getId().equals(this.getId());
        }

        public int hashCode() {
            return this.getId().hashCode();
        }

        public String toString() {
            return "EditorAction[" + this.getDisplayName() + ":" + this.getId() + "]";
        }

        public ShortcutAction getKeymapManagerInstance(String keymapManagerName) {
            if ("EditorBridge".equals(keymapManagerName)) {
                return this;
            }
            return null;
        }

        public Action getRealAction() {
            return this.action;
        }
    }

    private static class Listener
    implements PropertyChangeListener {
        private Reference<EditorBridge> model;
        private Set<KeyBindingSettingsFactory> factories = new HashSet<KeyBindingSettingsFactory>();

        Listener(EditorBridge model) {
            this.model = new WeakReference<EditorBridge>(model);
        }

        void add(KeyBindingSettingsFactory kbsf) {
            this.factories.add(kbsf);
            kbsf.addPropertyChangeListener((PropertyChangeListener)this);
        }

        private EditorBridge getModel() {
            EditorBridge m = this.model.get();
            if (m != null) {
                return m;
            }
            for (KeyBindingSettingsFactory kbsf : this.factories) {
                kbsf.removePropertyChangeListener((PropertyChangeListener)this);
            }
            this.factories = new HashSet<KeyBindingSettingsFactory>();
            return null;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            EditorBridge m = this.getModel();
            if (m == null) {
                return;
            }
        }
    }

}

