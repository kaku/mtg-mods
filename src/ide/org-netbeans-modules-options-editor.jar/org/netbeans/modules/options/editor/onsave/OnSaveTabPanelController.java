/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.editor.onsave;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeEvent;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.onsave.OnSaveTabPanel;
import org.netbeans.modules.options.editor.onsave.OnSaveTabSelector;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.indentation.ProxyPreferences;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public final class OnSaveTabPanelController
extends OptionsPanelController {
    private static final Logger LOG = Logger.getLogger(OnSaveTabPanelController.class.getName());
    private final PropertyChangeSupport pcs;
    private PreferencesFactory pf;
    private OnSaveTabPanel panel;
    private OnSaveTabSelector selector;
    private boolean changed;

    public OnSaveTabPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
        this.changed = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void update() {
        boolean fire;
        OnSaveTabPanelController onSaveTabPanelController = this;
        synchronized (onSaveTabPanelController) {
            LOG.fine("update");
            if (this.pf != null) {
                this.pf.destroy();
            }
            this.pf = new PreferencesFactory(new Callable(){

                public Object call() {
                    OnSaveTabPanelController.this.notifyChanged(true);
                    return null;
                }
            });
            this.selector = new OnSaveTabSelector(this.pf);
            this.panel.setSelector(this.selector);
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void applyChanges() {
        boolean fire;
        OnSaveTabPanelController onSaveTabPanelController = this;
        synchronized (onSaveTabPanelController) {
            LOG.fine("applyChanges");
            this.pf.applyChanges();
            for (String mimeType : EditorSettings.getDefault().getAllMimeTypes()) {
                LOG.fine("Cleaning up '" + mimeType + "' preferences");
                Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
            }
            this.pf.destroy();
            this.pf = null;
            this.panel.setSelector(null);
            this.selector = null;
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        boolean fire;
        OnSaveTabPanelController onSaveTabPanelController = this;
        synchronized (onSaveTabPanelController) {
            LOG.fine("cancel");
            this.pf.destroy();
            this.pf = null;
            this.panel.setSelector(null);
            this.selector = null;
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    public boolean isValid() {
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isChanged() {
        OnSaveTabPanelController onSaveTabPanelController = this;
        synchronized (onSaveTabPanelController) {
            return this.changed;
        }
    }

    private void firePrefsChanged() {
        boolean isChanged = false;
        for (String mimeType : this.pf.getAccessedMimeTypes()) {
            if (!(isChanged |= this.arePrefsChanged(mimeType))) continue;
            this.changed = true;
            return;
        }
        this.changed = isChanged;
    }

    private boolean arePrefsChanged(String mimeType) {
        boolean isChanged = false;
        Preferences prefs = this.selector.getPreferences(mimeType);
        Preferences savedPrefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        HashSet<String> hashSet = new HashSet<String>();
        try {
            hashSet.addAll(Arrays.asList(prefs.keys()));
            hashSet.addAll(Arrays.asList(savedPrefs.keys()));
        }
        catch (BackingStoreException ex) {
            return false;
        }
        for (String key : hashSet) {
            String current = prefs.get(key, null);
            String saved = savedPrefs.get(key, null);
            if (saved == null) {
                saved = key.equals("on-save-remove-trailing-whitespace") || key.equals("on-save-reformat") ? "never" : (key.equals("on-save-use-global-settings") ? "true" : this.selector.getSavedValue(mimeType, key));
            }
            boolean bl = current == null ? saved != null : !current.equals(saved);
            if (!(isChanged |= bl)) continue;
            return true;
        }
        return isChanged;
    }

    public HelpCtx getHelpCtx() {
        PreferencesCustomizer c = this.selector == null ? null : this.selector.getSelectedCustomizer();
        HelpCtx ctx = c == null ? null : c.getHelpCtx();
        return ctx != null ? ctx : new HelpCtx("netbeans.optionsDialog.editor.onSave");
    }

    public JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            this.panel = new OnSaveTabPanel();
        }
        return this.panel;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void notifyChanged(boolean changed) {
        boolean fire;
        OnSaveTabPanelController onSaveTabPanelController = this;
        synchronized (onSaveTabPanelController) {
            if (this.changed != changed) {
                this.changed = changed;
                fire = true;
            } else {
                fire = false;
            }
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", !changed, changed);
        }
        this.firePrefsChanged();
    }

    static final class PreferencesFactory
    implements PreferenceChangeListener,
    NodeChangeListener {
        private final Map<String, ProxyPreferences> mimeTypePreferences = new HashMap<String, ProxyPreferences>();
        private final PreferenceChangeListener weakPrefL;
        private final NodeChangeListener weakNodeL;
        private final Callable callback;

        public PreferencesFactory(Callable callback) {
            this.weakPrefL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)null);
            this.weakNodeL = (NodeChangeListener)WeakListeners.create(NodeChangeListener.class, (EventListener)this, (Object)null);
            this.callback = callback;
        }

        public Set<? extends String> getAccessedMimeTypes() {
            return this.mimeTypePreferences.keySet();
        }

        public void applyChanges() {
            for (String mimeType : this.mimeTypePreferences.keySet()) {
                ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
                pp.silence();
                try {
                    LOG.fine("    flushing pp for '" + mimeType + "'");
                    pp.flush();
                }
                catch (BackingStoreException ex) {
                    LOG.log(Level.WARNING, "Can't flush preferences for '" + mimeType + "'", ex);
                }
            }
        }

        public void destroy() {
            for (String mimeType : this.mimeTypePreferences.keySet()) {
                ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
                pp.removeNodeChangeListener(this.weakNodeL);
                pp.removePreferenceChangeListener(this.weakPrefL);
                pp.destroy();
                LOG.fine("destroying pp for '" + mimeType + "'");
            }
            this.mimeTypePreferences.clear();
        }

        public Preferences getPreferences(String mimeType) {
            ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
            try {
                if (pp != null && !pp.nodeExists("")) {
                    pp = null;
                }
            }
            catch (BackingStoreException bse) {
                // empty catch block
            }
            if (pp == null) {
                Preferences p = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                pp = ProxyPreferences.getProxyPreferences(this, p);
                pp.addPreferenceChangeListener(this.weakPrefL);
                pp.addNodeChangeListener(this.weakNodeL);
                this.mimeTypePreferences.put(mimeType, pp);
                LOG.fine("getPreferences('" + mimeType + "')");
            }
            return pp;
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

        @Override
        public void childAdded(NodeChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

        @Override
        public void childRemoved(NodeChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }

}

