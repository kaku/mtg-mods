/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.editor.onsave;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.options.editor.onsave.OnSaveCommonPanel;
import org.netbeans.modules.options.editor.onsave.OnSaveTabSelector;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class OnSaveTabPanel
extends JPanel
implements PropertyChangeListener {
    private OnSaveCommonPanel commonPanel;
    private OnSaveTabSelector selector;
    private PropertyChangeListener weakListener;
    private String storedMimeType = null;
    private JComboBox cboLanguage;
    private JPanel commonPanelContainer;
    private JPanel customPanelContainer;
    private JLabel lLanguage;

    public OnSaveTabPanel() {
        this.initComponents();
        this.cboLanguage.setRenderer(new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof String && OnSaveTabPanel.this.selector != null) {
                    value = OnSaveTabPanel.this.selector.getLanguageName((String)value);
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
        this.commonPanel = new OnSaveCommonPanel();
        this.commonPanelContainer.setLayout(new BorderLayout());
        this.commonPanelContainer.add((Component)this.commonPanel, "West");
        this.customPanelContainer.setLayout(new BorderLayout());
    }

    public void setSelector(OnSaveTabSelector selector) {
        if (selector == null) {
            this.storedMimeType = (String)this.cboLanguage.getSelectedItem();
        }
        if (this.selector != null) {
            this.selector.removePropertyChangeListener(this.weakListener);
        }
        this.selector = selector;
        if (this.selector != null) {
            this.weakListener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.selector);
            this.selector.addPropertyChangeListener(this.weakListener);
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
            String preSelectMimeType = null;
            for (String mimeType : this.selector.getMimeTypes()) {
                model.addElement(mimeType);
                if (!mimeType.equals(this.storedMimeType)) continue;
                preSelectMimeType = mimeType;
            }
            this.cboLanguage.setModel(model);
            if (preSelectMimeType == null) {
                JTextComponent pane = EditorRegistry.lastFocusedComponent();
                preSelectMimeType = pane != null ? (String)pane.getDocument().getProperty("mimeType") : "";
            }
            this.cboLanguage.setSelectedItem(preSelectMimeType);
            if (!preSelectMimeType.equals(this.cboLanguage.getSelectedItem())) {
                this.cboLanguage.setSelectedIndex(0);
            }
        } else {
            this.cboLanguage.setModel(new DefaultComboBoxModel());
        }
    }

    private void initComponents() {
        this.lLanguage = new JLabel();
        this.cboLanguage = new JComboBox();
        this.commonPanelContainer = new JPanel();
        this.customPanelContainer = new JPanel();
        this.lLanguage.setLabelFor(this.cboLanguage);
        Mnemonics.setLocalizedText((JLabel)this.lLanguage, (String)NbBundle.getMessage(OnSaveTabPanel.class, (String)"OnSaveTabPanel.lLanguage.text"));
        this.cboLanguage.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OnSaveTabPanel.this.languageChanged(evt);
            }
        });
        GroupLayout commonPanelContainerLayout = new GroupLayout(this.commonPanelContainer);
        this.commonPanelContainer.setLayout(commonPanelContainerLayout);
        commonPanelContainerLayout.setHorizontalGroup(commonPanelContainerLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 241, 32767));
        commonPanelContainerLayout.setVerticalGroup(commonPanelContainerLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 90, 32767));
        GroupLayout customPanelContainerLayout = new GroupLayout(this.customPanelContainer);
        this.customPanelContainer.setLayout(customPanelContainerLayout);
        customPanelContainerLayout.setHorizontalGroup(customPanelContainerLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        customPanelContainerLayout.setVerticalGroup(customPanelContainerLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 137, 32767));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.customPanelContainer, -1, -1, 32767).addComponent(this.commonPanelContainer, GroupLayout.Alignment.LEADING, -1, -1, 32767).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addComponent(this.lLanguage).addGap(3, 3, 3).addComponent(this.cboLanguage, -2, -1, -2).addGap(0, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lLanguage).addComponent(this.cboLanguage, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.commonPanelContainer, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.customPanelContainer, -1, -1, 32767).addContainerGap()));
    }

    private void languageChanged(ActionEvent evt) {
        this.selector.setSelectedMimeType((String)this.cboLanguage.getSelectedItem());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String mimeType = this.selector.getSelectedMimeType();
        Preferences prefs = this.selector.getPreferences(mimeType);
        Preferences globalPrefs = this.selector.getPreferences("");
        this.commonPanel.update(prefs, globalPrefs);
        this.customPanelContainer.setVisible(false);
        this.customPanelContainer.removeAll();
        PreferencesCustomizer c = this.selector.getSelectedCustomizer();
        if (c != null) {
            this.customPanelContainer.add((Component)c.getComponent(), "West");
        }
        this.customPanelContainer.setVisible(true);
    }

}

