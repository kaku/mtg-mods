/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.FolderBasedController;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.awt.Mnemonics;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public final class FolderBasedOptionPanel
extends JPanel
implements ActionListener {
    private final FolderBasedController controller;
    private String lastSelectedMimeType = null;
    private JTextField filter;
    private JLabel filterLabel;
    private JComboBox languageCombo;
    private JLabel languageLabel;
    private JPanel optionsPanel;

    FolderBasedOptionPanel(FolderBasedController controller, Document filterDocument, boolean allowFiltering) {
        this.controller = controller;
        this.initComponents();
        this.filter.setDocument(filterDocument);
        if (!allowFiltering) {
            this.filter.setVisible(false);
            this.filterLabel.setVisible(false);
        }
        DefaultListCellRenderer renderer = new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof String) {
                    value = EditorSettings.getDefault().getLanguageName((String)value);
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        };
        this.languageCombo.setRenderer(renderer);
        this.languageCombo.addActionListener(this);
        this.update();
    }

    void update() {
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
        for (String mimeType : this.controller.getMimeTypes()) {
            model.addElement(mimeType);
        }
        this.languageCombo.setModel(model);
        this.lastSelectedMimeType = this.controller.getSavedSelectedLanguage();
        if (this.lastSelectedMimeType != null && model.getSize() > 0) {
            this.languageCombo.setSelectedItem(this.lastSelectedMimeType);
            return;
        }
        JTextComponent pane = EditorRegistry.lastFocusedComponent();
        String preSelectMimeType = pane != null ? (String)pane.getDocument().getProperty("mimeType") : "";
        this.languageCombo.setSelectedItem(preSelectMimeType);
        if (!preSelectMimeType.equals(this.languageCombo.getSelectedItem()) && model.getSize() > 0) {
            this.languageCombo.setSelectedIndex(0);
        }
    }

    String getSelectedLanguage() {
        return (String)this.languageCombo.getSelectedItem();
    }

    private void initComponents() {
        this.languageLabel = new JLabel();
        this.languageCombo = new JComboBox();
        this.optionsPanel = new JPanel();
        this.filter = new JTextField();
        this.filterLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.languageLabel.setLabelFor(this.languageCombo);
        Mnemonics.setLocalizedText((JLabel)this.languageLabel, (String)NbBundle.getMessage(FolderBasedOptionPanel.class, (String)"LBL_Language"));
        this.languageCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.optionsPanel.setOpaque(false);
        this.optionsPanel.setLayout(new BorderLayout());
        this.filter.setColumns(10);
        this.filter.setText(NbBundle.getMessage(FolderBasedOptionPanel.class, (String)"FolderBasedOptionPanel.filter.text"));
        this.filterLabel.setLabelFor(this.filter);
        Mnemonics.setLocalizedText((JLabel)this.filterLabel, (String)NbBundle.getMessage(FolderBasedOptionPanel.class, (String)"FolderBasedOptionPanel.filterLabel.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.languageLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.languageCombo, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.filterLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.filter, -2, 114, -2).addContainerGap()).addComponent(this.optionsPanel, GroupLayout.Alignment.TRAILING, -1, -1, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.languageLabel).addComponent(this.languageCombo, -2, -1, -2).addComponent(this.filter, -2, -1, -2).addComponent(this.filterLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.optionsPanel, -1, 288, 32767)));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        OptionsPanelController opc;
        this.optionsPanel.setVisible(false);
        this.optionsPanel.removeAll();
        String mimeType = (String)this.languageCombo.getSelectedItem();
        if (mimeType != null && (opc = this.controller.getController(mimeType)) != null) {
            JComponent component = opc.getComponent(this.controller.getLookup());
            this.optionsPanel.add((Component)component, "Center");
            this.optionsPanel.setVisible(true);
        }
        this.searchEnableDisable();
        if (this.isShowing()) {
            this.lastSelectedMimeType = mimeType;
        }
    }

    void setCurrentMimeType(String key) {
        this.languageCombo.setSelectedItem(key);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.filter.setText("");
        this.lastSelectedMimeType = null;
    }

    void searchEnableDisable() {
        String mimeType = (String)this.languageCombo.getSelectedItem();
        this.filter.setEnabled(mimeType != null ? this.controller.supportsFilter(mimeType) : false);
    }

}

