/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.options.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.FolderBasedOptionPanel;
import org.netbeans.modules.options.editor.spi.OptionsFilter;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public final class FolderBasedController
extends OptionsPanelController
implements PropertyChangeListener {
    private static final String OPTIONS_SUB_FOLDER = "optionsSubFolder";
    private static final String HELP_CTX_ID = "helpContextId";
    private static final String ALLOW_FILTERING = "allowFiltering";
    private static final String BASE_FOLDER = "OptionsDialog/Editor/";
    private static FolderBasedController hintsController;
    private final PropertyChangeSupport pcs;
    private final String folder;
    private final HelpCtx helpCtx;
    private Lookup masterLookup;
    private FolderBasedOptionPanel panel;
    private Map<String, OptionsPanelController> mimeType2delegates;
    private final boolean allowFiltering;
    private final Set<String> supportFiltering;
    private final Document filterDocument;

    public static OptionsPanelController hints() {
        if (hintsController == null) {
            hintsController = new FolderBasedController("Hints/", "netbeans.optionsDialog.editor.hints", true);
        }
        return hintsController;
    }

    public static OptionsPanelController markOccurrences() {
        return new FolderBasedController("MarkOccurrences/", "netbeans.optionsDialog.editor.markOccurences", false);
    }

    public static OptionsPanelController create(Map args) {
        FolderBasedController folderBasedController = new FolderBasedController((String)args.get("optionsSubFolder"), (String)args.get("helpContextId"), (Boolean)args.get("allowFiltering"));
        return folderBasedController;
    }

    private FolderBasedController(String subFolder, String helpCtxId, boolean allowFiltering) {
        this.pcs = new PropertyChangeSupport(this);
        this.supportFiltering = new HashSet<String>();
        this.filterDocument = new PlainDocument();
        this.folder = subFolder != null ? "OptionsDialog/Editor/" + subFolder : "OptionsDialog/Editor/";
        this.helpCtx = helpCtxId != null ? new HelpCtx(helpCtxId) : null;
        this.allowFiltering = allowFiltering;
    }

    private void saveSelectedLanguage() {
        String selectedLanguage = this.panel.getSelectedLanguage();
        if (selectedLanguage != null) {
            NbPreferences.forModule(FolderBasedController.class).put(this.folder, selectedLanguage);
        }
    }

    String getSavedSelectedLanguage() {
        return NbPreferences.forModule(FolderBasedController.class).get(this.folder, null);
    }

    public final synchronized void update() {
        for (Map.Entry<String, OptionsPanelController> e : this.getMimeType2delegates().entrySet()) {
            OptionsFilter f = OptionsFilter.create(this.filterDocument, new FilteringUsedCallback(e.getKey()));
            ProxyLookup innerLookup = new ProxyLookup(new Lookup[]{this.masterLookup, Lookups.singleton((Object)f)});
            OptionsPanelController c = e.getValue();
            c.getComponent((Lookup)innerLookup);
            c.update();
        }
        assert (this.panel != null);
        this.panel.update();
    }

    public final synchronized void applyChanges() {
        Collection<OptionsPanelController> controllers = this.getMimeType2delegates().values();
        for (OptionsPanelController c : controllers) {
            c.applyChanges();
        }
        this.mimeType2delegates = null;
        this.saveSelectedLanguage();
    }

    public final synchronized void cancel() {
        Collection<OptionsPanelController> controllers = this.getMimeType2delegates().values();
        for (OptionsPanelController c : controllers) {
            c.cancel();
        }
        this.mimeType2delegates = null;
        this.saveSelectedLanguage();
    }

    public final synchronized boolean isValid() {
        Collection<OptionsPanelController> controllers = this.getMimeType2delegates().values();
        for (OptionsPanelController c : controllers) {
            if (c.isValid()) continue;
            return false;
        }
        return true;
    }

    public final synchronized boolean isChanged() {
        Collection<OptionsPanelController> controllers = this.getMimeType2delegates().values();
        for (OptionsPanelController c : controllers) {
            if (!c.isChanged()) continue;
            return true;
        }
        return false;
    }

    public final HelpCtx getHelpCtx() {
        return this.helpCtx;
    }

    public synchronized JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            this.masterLookup = masterLookup;
            for (Map.Entry<String, OptionsPanelController> e : this.getMimeType2delegates().entrySet()) {
                OptionsFilter f = OptionsFilter.create(this.filterDocument, new FilteringUsedCallback(e.getKey()));
                ProxyLookup innerLookup = new ProxyLookup(new Lookup[]{masterLookup, Lookups.singleton((Object)f)});
                OptionsPanelController controller = e.getValue();
                controller.getComponent((Lookup)innerLookup);
                controller.addPropertyChangeListener((PropertyChangeListener)this);
            }
            this.panel = new FolderBasedOptionPanel(this, this.filterDocument, this.allowFiltering);
        }
        return this.panel;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    public Lookup getLookup() {
        return super.getLookup();
    }

    protected void setCurrentSubcategory(String subpath) {
        for (Map.Entry<String, OptionsPanelController> e : this.getMimeType2delegates().entrySet()) {
            if (!subpath.startsWith(e.getKey())) continue;
            this.panel.setCurrentMimeType(e.getKey());
            subpath = subpath.substring(e.getKey().length());
            if (subpath.length() > 0 && subpath.startsWith("/")) {
                e.getValue().setSubcategory(subpath.substring(1));
            }
            return;
        }
        Logger.getLogger(FolderBasedController.class.getName()).log(Level.WARNING, "setCurrentSubcategory: cannot open: {0}", subpath);
    }

    Iterable<String> getMimeTypes() {
        return this.getMimeType2delegates().keySet();
    }

    OptionsPanelController getController(String mimeType) {
        return this.getMimeType2delegates().get(mimeType);
    }

    private Map<String, OptionsPanelController> getMimeType2delegates() {
        if (this.mimeType2delegates == null) {
            this.mimeType2delegates = new LinkedHashMap<String, OptionsPanelController>();
            for (String mimeType : EditorSettings.getDefault().getAllMimeTypes()) {
                Lookup l = Lookups.forPath((String)(this.folder + mimeType));
                OptionsPanelController controller = (OptionsPanelController)l.lookup(OptionsPanelController.class);
                if (controller == null) continue;
                this.mimeType2delegates.put(mimeType, controller);
            }
        }
        return this.mimeType2delegates;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.pcs.firePropertyChange(evt);
    }

    boolean supportsFilter(String mimeType) {
        return this.supportFiltering.contains(mimeType);
    }

    private final class FilteringUsedCallback
    implements Runnable {
        private final String mimeType;

        public FilteringUsedCallback(String mimeType) {
            this.mimeType = mimeType;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            FolderBasedOptionPanel panel;
            FolderBasedController.this.supportFiltering.add(this.mimeType);
            FolderBasedController folderBasedController = FolderBasedController.this;
            synchronized (folderBasedController) {
                panel = FolderBasedController.this.panel;
            }
            if (panel != null) {
                panel.searchEnableDisable();
            }
        }
    }

}

