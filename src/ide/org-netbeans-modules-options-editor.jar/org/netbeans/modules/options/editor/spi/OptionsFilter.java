/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.options.editor.spi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public final class OptionsFilter {
    private final Document doc;
    private final Runnable usedCallback;

    private OptionsFilter(Document doc, Runnable usedCallback) {
        this.doc = doc;
        this.usedCallback = usedCallback;
    }

    public void installFilteringModel(JTree tree, TreeModel source, Acceptor acceptor) {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Not in AWT Event Dispatch Thread");
        }
        this.usedCallback.run();
        tree.setModel(new FilteringTreeModel(source, this.doc, acceptor));
    }

    public static OptionsFilter create(Document doc, Runnable usedCallback) {
        return new OptionsFilter(doc, usedCallback);
    }

    private static final class FilteringTreeModel
    implements TreeModel,
    TreeModelListener,
    DocumentListener {
        private final TreeModel delegate;
        private final Document filter;
        private final Acceptor acceptor;
        private final Map<Object, List<Object>> category2Nodes = new HashMap<Object, List<Object>>();
        private final List<TreeModelListener> listeners = new LinkedList<TreeModelListener>();

        public FilteringTreeModel(TreeModel delegate, Document filter, Acceptor acceptor) {
            this.delegate = delegate;
            this.filter = filter;
            this.acceptor = acceptor;
            this.delegate.addTreeModelListener(this);
            this.filter.addDocumentListener(this);
            this.filter();
        }

        @Override
        public Object getRoot() {
            return this.delegate.getRoot();
        }

        @Override
        public Object getChild(Object parent, int index) {
            return this.category2Nodes.get(parent).get(index);
        }

        @Override
        public int getChildCount(Object parent) {
            return this.category2Nodes.get(parent).size();
        }

        @Override
        public boolean isLeaf(Object node) {
            return this.delegate.isLeaf(node);
        }

        @Override
        public void valueForPathChanged(TreePath path, Object newValue) {
            this.delegate.valueForPathChanged(path, newValue);
        }

        @Override
        public int getIndexOfChild(Object parent, Object child) {
            List<Object> catNodes = this.category2Nodes.get(parent);
            if (catNodes == null) {
                return -1;
            }
            return catNodes.indexOf(child);
        }

        @Override
        public synchronized void addTreeModelListener(TreeModelListener l) {
            this.listeners.add(l);
        }

        @Override
        public synchronized void removeTreeModelListener(TreeModelListener l) {
            this.listeners.remove(l);
        }

        private synchronized Iterable<? extends TreeModelListener> getListeners() {
            return new LinkedList<TreeModelListener>(this.listeners);
        }

        void filter() {
            final String[] term = new String[1];
            this.filter.render(new Runnable(){

                @Override
                public void run() {
                    try {
                        term[0] = FilteringTreeModel.this.filter.getText(0, FilteringTreeModel.this.filter.getLength());
                    }
                    catch (BadLocationException ex) {
                        throw new IllegalStateException(ex);
                    }
                }
            });
            this.category2Nodes.clear();
            this.filterNodes(this.delegate.getRoot(), term[0]);
            for (TreeModelListener l : this.getListeners()) {
                l.treeStructureChanged(new TreeModelEvent((Object)this, new Object[]{this.getRoot()}));
            }
        }

        private boolean filterNodes(Object currentNode, String term) {
            boolean accepted;
            boolean bl = accepted = term.isEmpty() || this.acceptor.accept(currentNode, term);
            if (this.delegate.isLeaf(currentNode)) {
                this.category2Nodes.put(currentNode, Collections.emptyList());
                return accepted;
            }
            ArrayList<Object> filtered = new ArrayList<Object>(this.delegate.getChildCount(currentNode));
            for (int c = 0; c < this.delegate.getChildCount(currentNode); ++c) {
                Object child = this.delegate.getChild(currentNode, c);
                if (!this.filterNodes(child, term)) continue;
                filtered.add(child);
                accepted |= true;
            }
            if (term.isEmpty() || accepted || currentNode == this.delegate.getRoot()) {
                this.category2Nodes.put(currentNode, filtered);
            }
            return accepted;
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.filter();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.filter();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
            if (e.getTreePath().getPathCount() > 1 && this.getIndexOfChild(e.getTreePath().getParentPath().getLastPathComponent(), e.getTreePath().getLastPathComponent()) == -1) {
                return;
            }
            LinkedList<Integer> childIndices = new LinkedList<Integer>();
            LinkedList<Object> children = new LinkedList<Object>();
            Object[] ch = e.getChildren();
            if (ch == null) {
                ArrayList<Object> l = Collections.list(((TreeNode)e.getTreePath().getLastPathComponent()).children());
                ch = l.toArray(new Object[l.size()]);
            }
            for (Object c : ch) {
                int i = this.getIndexOfChild(e.getTreePath().getLastPathComponent(), e.getTreePath().getLastPathComponent());
                if (i == -1) continue;
                childIndices.add(i);
                children.add(c);
            }
            int[] childIndicesArray = new int[childIndices.size()];
            int o = 0;
            for (Integer i : childIndices) {
                childIndicesArray[o++] = i;
            }
            TreeModelEvent nue = new TreeModelEvent((Object)this, e.getTreePath(), childIndicesArray, children.toArray(new Object[children.size()]));
            for (TreeModelListener l : this.getListeners()) {
                l.treeNodesChanged(nue);
            }
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            throw new UnsupportedOperationException("Currently not supported.");
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {
            throw new UnsupportedOperationException("Currently not supported.");
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {
            throw new UnsupportedOperationException("Currently not supported.");
        }

    }

    public static interface Acceptor {
        public boolean accept(Object var1, String var2);
    }

}

