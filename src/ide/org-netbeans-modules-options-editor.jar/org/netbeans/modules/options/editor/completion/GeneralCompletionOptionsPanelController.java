/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 */
package org.netbeans.modules.options.editor.completion;

import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.modules.options.editor.completion.GeneralCompletionOptionsPanel;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.util.HelpCtx;

public final class GeneralCompletionOptionsPanelController
implements PreferencesCustomizer {
    private Preferences preferences;
    private GeneralCompletionOptionsPanel generalCompletionOptionsPanel;

    public GeneralCompletionOptionsPanelController(Preferences p) {
        this.preferences = p;
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getDisplayName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.editor.codeCompletion");
    }

    @Override
    public JComponent getComponent() {
        if (this.generalCompletionOptionsPanel == null) {
            this.generalCompletionOptionsPanel = new GeneralCompletionOptionsPanel(this.preferences);
        }
        return this.generalCompletionOptionsPanel;
    }

    public static final class CustomCustomizerImpl
    extends PreferencesCustomizer.CustomCustomizer {
        @Override
        public String getSavedValue(PreferencesCustomizer customCustomizer, String key) {
            if (customCustomizer instanceof GeneralCompletionOptionsPanelController) {
                return ((GeneralCompletionOptionsPanel)customCustomizer.getComponent()).getSavedValue(key);
            }
            return null;
        }
    }

}

