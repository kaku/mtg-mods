/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 */
package org.netbeans.modules.options.editor.spi;

import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.openide.util.HelpCtx;

public interface PreferencesCustomizer {
    public static final String TABS_AND_INDENTS_ID = "tabs-and-indents";

    public String getId();

    public String getDisplayName();

    public HelpCtx getHelpCtx();

    public JComponent getComponent();

    public static class CustomCustomizer {
        public String getSavedValue(PreferencesCustomizer customCustomizer, String key) {
            return null;
        }
    }

    public static interface Factory {
        public PreferencesCustomizer create(Preferences var1);
    }

}

