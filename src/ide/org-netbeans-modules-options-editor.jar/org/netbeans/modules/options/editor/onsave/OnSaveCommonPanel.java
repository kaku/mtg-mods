/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.options.editor.onsave;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class OnSaveCommonPanel
extends JPanel {
    private static final String[] LINE_OPTIONS = new String[]{"never", "always", "modified-lines"};
    private boolean allLanguages;
    private Preferences preferences;
    private JCheckBox cbUseGlobalSettings;
    private JComboBox cboReformat;
    private JComboBox cboRemoveTrailingWhitespace;
    private JLabel lReformat;
    private JLabel lRemoveTrailingWhitespace;

    public OnSaveCommonPanel() {
        this.initComponents();
        OnSaveCommonPanel.loc(this.cbUseGlobalSettings, "Use_Global_Settings");
        OnSaveCommonPanel.loc(this.lRemoveTrailingWhitespace, "Remove_Trailing_Whitespace");
        OnSaveCommonPanel.loc(this.cboRemoveTrailingWhitespace, "Remove_Trailing_Whitespace");
        OnSaveCommonPanel.loc(this.lReformat, "Reformat");
        OnSaveCommonPanel.loc(this.cboReformat, "Reformat");
        this.cboRemoveTrailingWhitespace.setModel(new DefaultComboBoxModel<String>(LINE_OPTIONS));
        this.cboRemoveTrailingWhitespace.setRenderer(new RemoveTrailingWhitespaceRenderer(this.cboRemoveTrailingWhitespace.getRenderer()));
        this.cboReformat.setModel(new DefaultComboBoxModel<String>(LINE_OPTIONS));
        this.cboReformat.setRenderer(new RemoveTrailingWhitespaceRenderer(this.cboReformat.getRenderer()));
    }

    void update(Preferences preferences, Preferences globalPreferences) {
        boolean useGlobalSettings;
        this.allLanguages = preferences == globalPreferences;
        this.preferences = preferences;
        boolean bl = useGlobalSettings = !this.allLanguages && preferences.getBoolean("on-save-use-global-settings", Boolean.TRUE);
        if (useGlobalSettings) {
            preferences = globalPreferences;
        }
        this.cbUseGlobalSettings.setSelected(useGlobalSettings);
        this.cbUseGlobalSettings.setVisible(!this.allLanguages);
        String rtw = preferences.get("on-save-remove-trailing-whitespace", "never");
        this.cboRemoveTrailingWhitespace.setSelectedItem(rtw);
        String reformat = preferences.get("on-save-reformat", "never");
        this.cboReformat.setSelectedItem(reformat);
        this.updateEnabled();
    }

    void updateEnabled() {
        boolean enabled = !this.cbUseGlobalSettings.isSelected();
        this.cboRemoveTrailingWhitespace.setEnabled(enabled);
        this.cboReformat.setEnabled(enabled);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(OnSaveCommonPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (!(c instanceof JLabel)) {
            c.getAccessibleContext().setAccessibleName(OnSaveCommonPanel.loc("AN_" + key));
            c.getAccessibleContext().setAccessibleDescription(OnSaveCommonPanel.loc("AD_" + key));
        }
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)OnSaveCommonPanel.loc("CTL_" + key));
        } else if (c instanceof JLabel) {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)OnSaveCommonPanel.loc("CTL_" + key));
        }
    }

    private void initComponents() {
        this.cbUseGlobalSettings = new JCheckBox();
        this.lReformat = new JLabel();
        this.cboReformat = new JComboBox();
        this.lRemoveTrailingWhitespace = new JLabel();
        this.cboRemoveTrailingWhitespace = new JComboBox();
        this.setLayout(new GridBagLayout());
        this.cbUseGlobalSettings.setText("Use All Languages Settings");
        this.cbUseGlobalSettings.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OnSaveCommonPanel.this.cbUseGlobalSettingsActionPerformed(evt);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(8, 8, 0, 8);
        this.add((Component)this.cbUseGlobalSettings, gridBagConstraints);
        this.lReformat.setLabelFor(this.cboReformat);
        this.lReformat.setText("Reformat:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(8, 30, 0, 0);
        this.add((Component)this.lReformat, gridBagConstraints);
        this.cboReformat.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OnSaveCommonPanel.this.cboReformatActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(2, 12, 0, 0);
        this.add((Component)this.cboReformat, gridBagConstraints);
        this.lRemoveTrailingWhitespace.setLabelFor(this.cboRemoveTrailingWhitespace);
        this.lRemoveTrailingWhitespace.setText("Remove Trailing Whitespace From:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(8, 30, 0, 0);
        this.add((Component)this.lRemoveTrailingWhitespace, gridBagConstraints);
        this.cboRemoveTrailingWhitespace.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                OnSaveCommonPanel.this.cboRemoveTrailingWhitespaceActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(2, 12, 0, 0);
        this.add((Component)this.cboRemoveTrailingWhitespace, gridBagConstraints);
    }

    private void cboRemoveTrailingWhitespaceActionPerformed(ActionEvent evt) {
        if (this.preferences != null) {
            this.preferences.put("on-save-remove-trailing-whitespace", (String)this.cboRemoveTrailingWhitespace.getSelectedItem());
        }
    }

    private void cboReformatActionPerformed(ActionEvent evt) {
        if (this.preferences != null) {
            this.preferences.put("on-save-reformat", (String)this.cboReformat.getSelectedItem());
        }
    }

    private void cbUseGlobalSettingsActionPerformed(ActionEvent evt) {
        if (this.preferences != null) {
            this.preferences.putBoolean("on-save-use-global-settings", this.cbUseGlobalSettings.isSelected());
            this.updateEnabled();
        }
    }

    private static final class RemoveTrailingWhitespaceRenderer
    implements ListCellRenderer {
        private final ListCellRenderer defaultRenderer;

        public RemoveTrailingWhitespaceRenderer(ListCellRenderer defaultRenderer) {
            this.defaultRenderer = defaultRenderer;
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            return this.defaultRenderer.getListCellRendererComponent(list, NbBundle.getMessage(OnSaveCommonPanel.class, (String)("LINE_OPTION_" + value)), index, isSelected, cellHasFocus);
        }
    }

}

