/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.editor.completion;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeEvent;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.completion.CodeCompletionOptionsPanel;
import org.netbeans.modules.options.editor.completion.CodeCompletionOptionsSelector;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.indentation.ProxyPreferences;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public final class CodeCompletionOptionsPanelController
extends OptionsPanelController {
    private static final Logger LOG = Logger.getLogger(CodeCompletionOptionsPanelController.class.getName());
    private final PropertyChangeSupport pcs;
    private PreferencesFactory pf;
    private CodeCompletionOptionsPanel panel;
    private CodeCompletionOptionsSelector selector;
    private boolean changed;

    public CodeCompletionOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
        this.changed = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void update() {
        boolean fire;
        CodeCompletionOptionsPanelController codeCompletionOptionsPanelController = this;
        synchronized (codeCompletionOptionsPanelController) {
            LOG.fine("update");
            if (this.pf != null) {
                this.pf.destroy();
            }
            this.pf = new PreferencesFactory(new Callable(){

                public Object call() {
                    CodeCompletionOptionsPanelController.this.notifyChanged(true);
                    return null;
                }
            });
            this.selector = new CodeCompletionOptionsSelector(this.pf);
            this.panel.setSelector(this.selector);
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void applyChanges() {
        boolean fire;
        CodeCompletionOptionsPanelController codeCompletionOptionsPanelController = this;
        synchronized (codeCompletionOptionsPanelController) {
            LOG.fine("applyChanges");
            this.pf.applyChanges();
            for (String mimeType : EditorSettings.getDefault().getAllMimeTypes()) {
                LOG.fine("Cleaning up '" + mimeType + "' preferences");
                Preferences prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                prefs.remove("pair-characters-completion");
                prefs.remove("completion-auto-popup");
                prefs.remove("javadoc-auto-popup");
                prefs.remove("javadoc-popup-next-to-cc");
                prefs.remove("show-deprecated-members");
                prefs.remove("completion-instant-substitution");
                prefs.remove("completion-case-sensitive");
            }
            this.pf.destroy();
            this.pf = null;
            this.panel.setSelector(null);
            this.selector = null;
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        boolean fire;
        CodeCompletionOptionsPanelController codeCompletionOptionsPanelController = this;
        synchronized (codeCompletionOptionsPanelController) {
            LOG.fine("cancel");
            this.pf.destroy();
            this.pf = null;
            this.panel.setSelector(null);
            this.selector = null;
            fire = this.changed;
            this.changed = false;
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", true, false);
        }
    }

    public boolean isValid() {
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isChanged() {
        CodeCompletionOptionsPanelController codeCompletionOptionsPanelController = this;
        synchronized (codeCompletionOptionsPanelController) {
            return this.changed;
        }
    }

    private void firePrefsChanged() {
        boolean isChanged = false;
        for (String mimeType : this.pf.getAccessedMimeTypes()) {
            if (!(isChanged |= this.arePrefsChanged(mimeType))) continue;
            this.changed = true;
            return;
        }
        this.changed = isChanged;
    }

    private boolean arePrefsChanged(String mimeType) {
        boolean isChanged = false;
        Preferences prefs = this.pf.getPreferences(mimeType);
        Preferences savedPrefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        HashSet<String> hashSet = new HashSet<String>();
        try {
            hashSet.addAll(Arrays.asList(prefs.keys()));
            hashSet.addAll(Arrays.asList(savedPrefs.keys()));
        }
        catch (BackingStoreException ex) {
            return false;
        }
        for (String key : hashSet) {
            String current = prefs.get(key, null);
            String saved = savedPrefs.get(key, null);
            if (saved == null) {
                saved = this.selector.getSavedValue(mimeType, key);
            }
            boolean bl = current == null ? saved != null : !current.equals(saved);
            if (!(isChanged |= bl)) continue;
            return true;
        }
        return isChanged;
    }

    public HelpCtx getHelpCtx() {
        PreferencesCustomizer c = this.selector == null ? null : this.selector.getSelectedCustomizer();
        HelpCtx ctx = c == null ? null : c.getHelpCtx();
        return ctx != null ? ctx : new HelpCtx("netbeans.optionsDialog.editor.codeCompletion");
    }

    public JComponent getComponent(Lookup masterLookup) {
        if (this.panel == null) {
            this.panel = new CodeCompletionOptionsPanel();
        }
        return this.panel;
    }

    protected void setCurrentSubcategory(String subpath) {
        if (this.selector != null && this.selector.getMimeTypes().contains(subpath)) {
            this.selector.setSelectedMimeType(subpath);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void notifyChanged(boolean changed) {
        boolean fire;
        CodeCompletionOptionsPanelController codeCompletionOptionsPanelController = this;
        synchronized (codeCompletionOptionsPanelController) {
            if (this.changed != changed) {
                this.changed = changed;
                fire = true;
            } else {
                fire = false;
            }
        }
        if (fire) {
            this.pcs.firePropertyChange("changed", !changed, changed);
        }
        this.firePrefsChanged();
    }

    static final class PreferencesFactory
    implements PreferenceChangeListener,
    NodeChangeListener {
        private final Map<String, ProxyPreferences> mimeTypePreferences = new HashMap<String, ProxyPreferences>();
        private final PreferenceChangeListener weakPrefL;
        private final NodeChangeListener weakNodeL;
        private final Callable callback;

        public PreferencesFactory(Callable callback) {
            this.weakPrefL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)null);
            this.weakNodeL = (NodeChangeListener)WeakListeners.create(NodeChangeListener.class, (EventListener)this, (Object)null);
            this.callback = callback;
        }

        public Set<? extends String> getAccessedMimeTypes() {
            return this.mimeTypePreferences.keySet();
        }

        public void applyChanges() {
            for (String mimeType : this.mimeTypePreferences.keySet()) {
                ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
                pp.silence();
                try {
                    LOG.fine("    flushing pp for '" + mimeType + "'");
                    pp.flush();
                }
                catch (BackingStoreException ex) {
                    LOG.log(Level.WARNING, "Can't flush preferences for '" + mimeType + "'", ex);
                }
            }
        }

        public void destroy() {
            for (String mimeType : this.mimeTypePreferences.keySet()) {
                ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
                pp.removeNodeChangeListener(this.weakNodeL);
                pp.removePreferenceChangeListener(this.weakPrefL);
                pp.destroy();
                LOG.fine("destroying pp for '" + mimeType + "'");
            }
            this.mimeTypePreferences.clear();
        }

        public Preferences getPreferences(String mimeType) {
            ProxyPreferences pp = this.mimeTypePreferences.get(mimeType);
            try {
                if (pp != null && !pp.nodeExists("")) {
                    pp = null;
                }
            }
            catch (BackingStoreException bse) {
                // empty catch block
            }
            if (pp == null) {
                Preferences p = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                pp = ProxyPreferences.getProxyPreferences(this, p);
                pp.addPreferenceChangeListener(this.weakPrefL);
                pp.addNodeChangeListener(this.weakNodeL);
                this.mimeTypePreferences.put(mimeType, pp);
                LOG.fine("getPreferences('" + mimeType + "')");
            }
            return pp;
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

        @Override
        public void childAdded(NodeChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }

        @Override
        public void childRemoved(NodeChangeEvent evt) {
            try {
                this.callback.call();
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }

}

