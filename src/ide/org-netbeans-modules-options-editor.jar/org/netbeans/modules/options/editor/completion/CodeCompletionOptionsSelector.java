/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.options.editor.completion;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.completion.CodeCompletionOptionsPanelController;
import org.netbeans.modules.options.editor.completion.GeneralCompletionOptionsPanelController;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class CodeCompletionOptionsSelector {
    public static final String CODE_COMPLETION_CUSTOMIZERS_FOLDER = "OptionsDialog/Editor/CodeCompletion/";
    private final Map<String, PreferencesCustomizer> allCustomizers = new HashMap<String, PreferencesCustomizer>();
    private final PropertyChangeSupport pcs;
    private final CodeCompletionOptionsPanelController.PreferencesFactory pf;
    private Set<String> mimeTypes;
    private String selectedMimeType;

    CodeCompletionOptionsSelector(CodeCompletionOptionsPanelController.PreferencesFactory pf) {
        this.pcs = new PropertyChangeSupport(this);
        this.pf = pf;
    }

    public synchronized Set<? extends String> getMimeTypes() {
        if (this.mimeTypes == null) {
            this.mimeTypes = new HashSet<String>();
            this.mimeTypes.add("");
            for (String mimeType : EditorSettings.getDefault().getAllMimeTypes()) {
                Lookup l = Lookups.forPath((String)("OptionsDialog/Editor/CodeCompletion/" + mimeType));
                if (l.lookup(PreferencesCustomizer.Factory.class) == null) continue;
                this.mimeTypes.add(mimeType);
            }
        }
        return this.mimeTypes;
    }

    public synchronized void setSelectedMimeType(String mimeType) {
        assert (this.getMimeTypes().contains(mimeType));
        if (this.selectedMimeType == null || !this.selectedMimeType.equals(mimeType)) {
            String old = this.selectedMimeType;
            this.selectedMimeType = mimeType;
            this.pcs.firePropertyChange(null, old, mimeType);
        }
    }

    public synchronized PreferencesCustomizer getSelectedCustomizer() {
        return this.getCustomizer(this.selectedMimeType);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    String getSavedValue(String mimeType, String key) {
        PreferencesCustomizer prefsCustomizer = this.getCustomizer(mimeType);
        if (prefsCustomizer != null) {
            Lookup l = Lookups.forPath((String)("OptionsDialog/Editor/CodeCompletion/" + mimeType));
            PreferencesCustomizer.CustomCustomizer customizer = mimeType.isEmpty() ? (PreferencesCustomizer.CustomCustomizer)l.lookup(GeneralCompletionOptionsPanelController.CustomCustomizerImpl.class) : (PreferencesCustomizer.CustomCustomizer)l.lookup(PreferencesCustomizer.CustomCustomizer.class);
            if (customizer != null) {
                return customizer.getSavedValue(prefsCustomizer, key);
            }
        }
        return null;
    }

    private PreferencesCustomizer getCustomizer(String mimeType) {
        PreferencesCustomizer customizer = this.allCustomizers.get(mimeType);
        if (customizer == null) {
            Preferences prefs = this.pf.getPreferences(mimeType);
            if (mimeType.length() > 0) {
                Lookup l = Lookups.forPath((String)("OptionsDialog/Editor/CodeCompletion/" + mimeType));
                PreferencesCustomizer.Factory factory = (PreferencesCustomizer.Factory)l.lookup(PreferencesCustomizer.Factory.class);
                if (factory != null) {
                    customizer = factory.create(prefs);
                }
            } else {
                customizer = new GeneralCompletionOptionsPanelController(prefs);
            }
            this.allCustomizers.put(this.selectedMimeType, customizer);
        }
        return customizer;
    }
}

