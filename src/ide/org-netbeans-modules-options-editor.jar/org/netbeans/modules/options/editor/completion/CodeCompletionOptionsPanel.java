/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.options.editor.completion;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.options.editor.completion.CodeCompletionOptionsSelector;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class CodeCompletionOptionsPanel
extends JPanel
implements PropertyChangeListener {
    private CodeCompletionOptionsSelector selector;
    private PropertyChangeListener weakListener;
    private Object lastSelectedItem = null;
    private JComboBox cbLanguage;
    private JLabel lLanguage;
    private JPanel panel;

    public CodeCompletionOptionsPanel() {
        this.initComponents();
        this.cbLanguage.setRenderer(new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof String) {
                    value = ((String)value).length() > 0 ? EditorSettings.getDefault().getLanguageName((String)value) : NbBundle.getMessage(CodeCompletionOptionsPanel.class, (String)"LBL_AllLanguages");
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
    }

    public void setSelector(CodeCompletionOptionsSelector selector) {
        if (this.selector != null) {
            this.selector.removePropertyChangeListener(this.weakListener);
        }
        this.selector = selector;
        if (this.selector != null) {
            this.weakListener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.selector);
            this.selector.addPropertyChangeListener(this.weakListener);
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
            ArrayList<? extends String> mimeTypes = new ArrayList<String>();
            mimeTypes.addAll(selector.getMimeTypes());
            Collections.sort(mimeTypes, new LanguagesComparator());
            for (String mimeType : mimeTypes) {
                model.addElement(mimeType);
            }
            this.cbLanguage.setModel(model);
            if (this.lastSelectedItem != null) {
                this.cbLanguage.setSelectedItem(this.lastSelectedItem);
            } else {
                this.cbLanguage.setSelectedIndex(0);
            }
        } else {
            if (this.cbLanguage.getSelectedItem() != null) {
                this.lastSelectedItem = this.cbLanguage.getSelectedItem();
            }
            this.cbLanguage.setModel(new DefaultComboBoxModel());
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.panel.setVisible(false);
        this.panel.removeAll();
        PreferencesCustomizer c = this.selector.getSelectedCustomizer();
        if (c != null) {
            this.panel.add((Component)c.getComponent(), "Center");
        }
        this.panel.setVisible(true);
        this.cbLanguage.setSelectedItem(evt.getNewValue());
    }

    private void initComponents() {
        this.lLanguage = new JLabel();
        this.cbLanguage = new JComboBox();
        this.panel = new JPanel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.lLanguage.setLabelFor(this.cbLanguage);
        Mnemonics.setLocalizedText((JLabel)this.lLanguage, (String)NbBundle.getMessage(CodeCompletionOptionsPanel.class, (String)"CTL_Language"));
        this.cbLanguage.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeCompletionOptionsPanel.this.languageChanged(evt);
            }
        });
        this.panel.setOpaque(false);
        this.panel.setLayout(new BorderLayout());
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.lLanguage).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbLanguage, -2, -1, -2).addContainerGap()).addComponent(this.panel, -1, -1, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lLanguage).addComponent(this.cbLanguage, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.panel, -1, 111, 32767)));
    }

    private void languageChanged(ActionEvent evt) {
        this.selector.setSelectedMimeType((String)this.cbLanguage.getSelectedItem());
    }

    private static final class LanguagesComparator
    implements Comparator<String> {
        private LanguagesComparator() {
        }

        @Override
        public int compare(String mimeType1, String mimeType2) {
            if (mimeType1.length() == 0) {
                return mimeType2.length() == 0 ? 0 : -1;
            }
            if (mimeType2.length() == 0) {
                return 1;
            }
            String langName1 = EditorSettings.getDefault().getLanguageName(mimeType1);
            String langName2 = EditorSettings.getDefault().getLanguageName(mimeType2);
            return langName1.compareTo(langName2);
        }
    }

}

