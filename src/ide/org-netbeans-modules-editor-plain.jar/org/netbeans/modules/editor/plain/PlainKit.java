/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Syntax
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 *  org.netbeans.editor.ext.plain.PlainSyntax
 *  org.netbeans.modules.editor.NbEditorKit
 */
package org.netbeans.modules.editor.plain;

import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.editor.ext.plain.PlainSyntax;
import org.netbeans.modules.editor.NbEditorKit;

public class PlainKit
extends NbEditorKit {
    public static final String PLAIN_MIME_TYPE = "text/plain";

    public String getContentType() {
        return "text/plain";
    }

    public Syntax createSyntax(Document doc) {
        return new PlainSyntax();
    }

    public SyntaxSupport createSyntaxSupport(BaseDocument doc) {
        return new ExtSyntaxSupport(doc);
    }
}

