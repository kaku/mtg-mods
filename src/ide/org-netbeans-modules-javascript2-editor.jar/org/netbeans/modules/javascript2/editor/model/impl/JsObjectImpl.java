/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.AnonymousObject;
import org.netbeans.modules.javascript2.editor.model.impl.JsElementImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.OccurrenceImpl;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.openide.filesystems.FileObject;

public class JsObjectImpl
extends JsElementImpl
implements JsObject {
    protected final LinkedHashMap<String, JsObject> properties = new LinkedHashMap();
    private Identifier declarationName;
    private JsObject parent;
    private final List<Occurrence> occurrences = new ArrayList<Occurrence>();
    private final NavigableMap<Integer, Collection<TypeUsage>> assignments = new TreeMap<Integer, Collection<TypeUsage>>();
    private final boolean hasName;
    private Documentation documentation;
    protected JsElement.Kind kind;

    public JsObjectImpl(JsObject parent, Identifier name, OffsetRange offsetRange, String mimeType, String sourceLabel) {
        super(parent != null ? parent.getFileObject() : null, name.getName(), "prototype".equals(name.getName()), offsetRange, EnumSet.of(Modifier.PUBLIC), mimeType, sourceLabel);
        this.declarationName = name;
        this.parent = parent;
        this.hasName = name.getOffsetRange().getStart() != name.getOffsetRange().getEnd();
        this.kind = null;
    }

    public JsObjectImpl(JsObject parent, Identifier name, OffsetRange offsetRange, boolean isDeclared, Set<Modifier> modifiers, String mimeType, String sourceLabel) {
        super(parent != null ? parent.getFileObject() : null, name.getName(), isDeclared, offsetRange, modifiers, mimeType, sourceLabel);
        this.declarationName = name;
        this.parent = parent;
        this.hasName = name.getOffsetRange().getStart() != name.getOffsetRange().getEnd();
        this.kind = null;
    }

    public JsObjectImpl(JsObject parent, Identifier name, OffsetRange offsetRange, boolean isDeclared, String mimeType, String sourceLabel) {
        this(parent, name, offsetRange, isDeclared, EnumSet.of(Modifier.PUBLIC), mimeType, sourceLabel);
    }

    protected JsObjectImpl(JsObject parent, String name, boolean isDeclared, OffsetRange offsetRange, Set<Modifier> modifiers, String mimeType, String sourceLabel) {
        super(parent != null ? parent.getFileObject() : null, name, isDeclared, offsetRange, modifiers, mimeType, sourceLabel);
        this.declarationName = null;
        this.parent = parent;
        this.hasName = false;
    }

    @Override
    public Identifier getDeclarationName() {
        return this.declarationName;
    }

    public void setDeclarationName(Identifier declaration) {
        this.declarationName = declaration;
    }

    @Override
    public JsElement.Kind getJSKind() {
        if (this.kind != null) {
            return this.kind;
        }
        if (this.parent == null) {
            return JsElement.Kind.FILE;
        }
        if ("prototype".equals(this.getName())) {
            return JsElement.Kind.OBJECT;
        }
        if (this.isDeclared()) {
            if ("arguments".equals(this.getName())) {
                return JsElement.Kind.VARIABLE;
            }
            if (!this.getAssignmentForOffset(this.getDeclarationName().getOffsetRange().getEnd()).isEmpty() && this.hasOnlyVirtualProperties()) {
                if (this.getParent().getParent() == null || this.getModifiers().contains((Object)Modifier.PRIVATE)) {
                    return JsElement.Kind.VARIABLE;
                }
                return JsElement.Kind.PROPERTY;
            }
        } else if (!this.getProperties().isEmpty()) {
            return JsElement.Kind.OBJECT;
        }
        if (this.getProperties().isEmpty()) {
            if (this.getParent().isAnonymous() && this.getParent() instanceof AnonymousObject) {
                return JsElement.Kind.PROPERTY;
            }
            if (this.getParent().getParent() == null || this.getModifiers().contains((Object)Modifier.PRIVATE)) {
                return JsElement.Kind.VARIABLE;
            }
            if (this.getParent() instanceof JsFunction && this.isDeclared()) {
                return this.getModifiers().contains((Object)Modifier.PRIVATE) ? JsElement.Kind.VARIABLE : JsElement.Kind.PROPERTY;
            }
            return JsElement.Kind.PROPERTY;
        }
        return JsElement.Kind.OBJECT;
    }

    private boolean hasOnlyVirtualProperties() {
        for (JsObject property : this.getProperties().values()) {
            if (!property.isDeclared() && !"prototype".equals(property.getName())) continue;
            return false;
        }
        return true;
    }

    @Override
    public Map<String, ? extends JsObject> getProperties() {
        return this.properties;
    }

    @Override
    public void addProperty(String name, JsObject property) {
        this.properties.put(name, property);
    }

    @Override
    public JsObject getProperty(String name) {
        return this.properties.get(name);
    }

    @Override
    public JsObject getParent() {
        return this.parent;
    }

    public void setParent(JsObject newParent) {
        this.parent = newParent;
    }

    @Override
    public int getOffset() {
        return this.declarationName == null ? -1 : this.declarationName.getOffsetRange().getStart();
    }

    @Override
    public List<Occurrence> getOccurrences() {
        return this.occurrences;
    }

    @Override
    public void addOccurrence(OffsetRange offsetRange) {
        OccurrenceImpl occurrence = new OccurrenceImpl(offsetRange, this);
        if (!this.occurrences.contains(occurrence)) {
            this.occurrences.add(occurrence);
        }
    }

    public void addAssignment(Collection<TypeUsage> typeNames, int offset) {
        Collection<TypeUsage> types = this.assignments.get(offset);
        if (types == null) {
            types = new ArrayList<TypeUsage>();
            this.assignments.put(offset, types);
        }
        types.addAll(typeNames);
    }

    @Override
    public void addAssignment(TypeUsage typeName, int offset) {
        Collection<TypeUsage> types = this.assignments.get(offset);
        if (types == null) {
            types = new ArrayList<TypeUsage>();
            this.assignments.put(offset, types);
        }
        types.add(typeName);
    }

    @Override
    public Collection<? extends TypeUsage> getAssignmentForOffset(int offset) {
        AbstractCollection result = Collections.EMPTY_LIST;
        Map.Entry<Integer, Collection<TypeUsage>> found = this.assignments.floorEntry(offset);
        if (found != null) {
            result = new ArrayList<TypeUsage>(found.getValue());
        }
        if (result.isEmpty()) {
            HashSet resolved = new HashSet();
            result = resolved;
        }
        return result;
    }

    public int getCountOfAssignments() {
        return this.assignments.size();
    }

    @Override
    public Collection<? extends TypeUsage> getAssignments() {
        ArrayList<TypeUsage> values = new ArrayList<TypeUsage>();
        for (Collection<TypeUsage> types : this.assignments.values()) {
            values.addAll(types);
        }
        return Collections.unmodifiableCollection(values);
    }

    @Override
    public String getFullyQualifiedName() {
        if (this.getParent() == null) {
            return this.getName();
        }
        StringBuilder result = new StringBuilder();
        JsObject pObject = this;
        result.append(this.getName());
        while ((pObject = pObject.getParent()).getParent() != null) {
            result.insert(0, ".");
            result.insert(0, pObject.getName());
        }
        return result.toString();
    }

    @Override
    public boolean isAnonymous() {
        return false;
    }

    @Override
    public boolean containsOffset(int offset) {
        if (this.getOffsetRange().containsInclusive(offset)) {
            return true;
        }
        for (JsObject property : this.getProperties().values()) {
            if (property.getOffsetRange().containsInclusive(offset)) {
                return true;
            }
            if (!"prototype".equals(property.getName()) || !property.containsOffset(offset)) continue;
            return true;
        }
        return false;
    }

    @Override
    public boolean hasExactName() {
        return this.hasName;
    }

    public final void setJsKind(JsElement.Kind kind) {
        this.kind = kind;
    }

    protected Collection<TypeUsage> resolveAssignments(JsObject jsObject, int offset) {
        HashSet<String> visited = new HashSet<String>();
        return this.resolveAssignments(jsObject, offset, visited);
    }

    protected Collection<TypeUsage> resolveAssignments(JsObject jsObject, int offset, Collection<String> visited) {
        HashSet<TypeUsage> result = new HashSet<TypeUsage>();
        String fqn = jsObject.getFullyQualifiedName();
        if (visited.contains(fqn)) {
            return result;
        }
        visited.add(fqn);
        Collection offsetAssignments = Collections.EMPTY_LIST;
        Map.Entry<Integer, Collection<TypeUsage>> found = ((JsObjectImpl)jsObject).assignments.floorEntry(offset);
        if (found != null) {
            offsetAssignments = found.getValue();
        }
        if (offsetAssignments.isEmpty() && !jsObject.getProperties().isEmpty()) {
            result.add(new TypeUsageImpl(jsObject.getFullyQualifiedName(), jsObject.getOffset(), true));
        } else {
            for (TypeUsage assignment : offsetAssignments) {
                if (visited.contains(assignment.getType())) continue;
                if (assignment.isResolved()) {
                    result.add(assignment);
                    continue;
                }
                if (assignment.getType().startsWith("@")) {
                    result.addAll(ModelUtils.resolveTypeFromSemiType(jsObject, assignment));
                    continue;
                }
                DeclarationScope scope = ModelUtils.getDeclarationScope(jsObject);
                JsObject object = ModelUtils.getJsObjectByName(scope, assignment.getType());
                if (object == null) {
                    JsObject gloal = ModelUtils.getGlobalObject(jsObject);
                    object = ModelUtils.findJsObjectByName(gloal, assignment.getType());
                }
                if (object == null) continue;
                Collection<TypeUsage> resolvedFromObject = this.resolveAssignments(object, found != null ? found.getKey() : -1, visited);
                if (resolvedFromObject.isEmpty()) {
                    result.add(new TypeUsageImpl(object.getFullyQualifiedName(), assignment.getOffset(), true));
                    continue;
                }
                result.addAll(resolvedFromObject);
            }
        }
        return result;
    }

    public void resolveTypes(JsDocumentationHolder jsDocHolder) {
        Collection<? extends TypeUsage> protoAssignments;
        JsObject prototypeProperty;
        JsObject prototype;
        if (this.parent == null) {
            return;
        }
        ArrayList resolved = new ArrayList();
        for (Collection<TypeUsage> unresolved : this.assignments.values()) {
            resolved.clear();
            JsObject global = ModelUtils.getGlobalObject(this.parent);
            for (TypeUsage type : unresolved) {
                TypeUsage originalType;
                JsObject originalObject;
                ArrayList<TypeUsage> resolvedHere = new ArrayList<TypeUsage>();
                if (!type.isResolved()) {
                    resolvedHere.addAll(ModelUtils.resolveTypeFromSemiType(this, type));
                } else {
                    resolvedHere.add(type);
                }
                if (!type.getType().contains("this")) {
                    for (TypeUsage typeHere : resolvedHere) {
                        JsObject jsObject;
                        DeclarationScope declarationScope;
                        if (typeHere.getOffset() <= 0) continue;
                        String rType = typeHere.getType();
                        if (rType.startsWith("@exp;")) {
                            rType = rType.substring(5);
                            rType = rType.replace("@pro;", ".");
                        }
                        if ((jsObject = ModelUtils.findJsObjectByName(global, rType)) == null && rType.indexOf(46) == -1 && global instanceof DeclarationScope && (jsObject = ModelUtils.getJsObjectByName(declarationScope = ModelUtils.getDeclarationScope((DeclarationScope)((Object)global), typeHere.getOffset()), rType)) == null) {
                            JsObject decParent;
                            JsObject jsObject2 = decParent = this.parent.getJSKind() != JsElement.Kind.ANONYMOUS_OBJECT && this.parent.getJSKind() != JsElement.Kind.OBJECT_LITERAL ? this.parent : this.parent.getParent();
                            while (jsObject == null && decParent != null) {
                                jsObject = decParent.getProperty(rType);
                                decParent = decParent.getParent();
                            }
                        }
                        if (jsObject == null) continue;
                        if (typeHere.isResolved()) {
                            int index = rType.lastIndexOf(46);
                            int typeLength = index > -1 ? rType.length() - index - 1 : rType.length();
                            int offset = typeHere.getOffset();
                            ((JsObjectImpl)jsObject).addOccurrence(new OffsetRange(offset, jsObject.isAnonymous() ? offset : offset + typeLength));
                        }
                        JsObjectImpl.moveOccurrenceOfProperties((JsObjectImpl)jsObject, this);
                        JsObject parent = jsObject.getParent();
                        if (parent == null || !"window".equals(parent.getName())) continue;
                        for (JsObject property : this.getProperties().values()) {
                            JsObject gwProp;
                            if (!property.isDeclared() || (gwProp = jsObject.getProperty(property.getName())) != null) continue;
                            jsObject.addProperty(property.getName(), property);
                        }
                    }
                } else if (type.getType().equals("@this;") && resolvedHere.size() == 1 && (originalObject = ModelUtils.findJsObjectByName(global, (originalType = (TypeUsage)resolvedHere.iterator().next()).getType())) != null) {
                    JsObjectImpl newObject = new JsObjectImpl(this.parent, this.declarationName, this.getOffsetRange(), this.isDeclared(), this.getModifiers(), this.getMimeType(), this.getSourceLabel());
                    this.parent.addProperty(this.getName(), newObject);
                    ArrayList<JsObject> propertiesCopy = new ArrayList<JsObject>(this.properties.values());
                    for (JsObject property : propertiesCopy) {
                        ModelUtils.moveProperty(originalObject, property);
                    }
                    for (Occurrence occurrence : this.occurrences) {
                        newObject.addOccurrence(occurrence.getOffsetRange());
                    }
                    newObject.addAssignment(new TypeUsageImpl(originalObject.getFullyQualifiedName(), originalObject.getOffset(), true), this.assignments.keySet().iterator().next());
                }
                resolved.addAll(resolvedHere);
            }
            unresolved.clear();
            unresolved.addAll(resolved);
        }
        if (!this.isAnonymous() && this.assignments.isEmpty()) {
            JsObject global = ModelUtils.getGlobalObject(this.parent);
            ArrayList<Occurrence> correctedOccurrences = new ArrayList<Occurrence>();
            JsObjectImpl obAssignment = this.findRightTypeAssignment(this.getDeclarationName().getOffsetRange().getStart(), global);
            if (obAssignment != null && !obAssignment.getModifiers().contains((Object)Modifier.PRIVATE)) {
                obAssignment.addOccurrence(this.getDeclarationName().getOffsetRange());
            }
            for (Occurrence occurrence : new ArrayList<Occurrence>(this.occurrences)) {
                obAssignment = this.findRightTypeAssignment(occurrence.getOffsetRange().getStart(), global);
                if (obAssignment != null && !obAssignment.getModifiers().contains((Object)Modifier.PRIVATE)) {
                    obAssignment.addOccurrence(occurrence.getOffsetRange());
                    continue;
                }
                correctedOccurrences.add(occurrence);
            }
            if (this.occurrences.size() != correctedOccurrences.size()) {
                this.occurrences.clear();
                this.occurrences.addAll(correctedOccurrences);
            }
        }
        if ((prototype = this.getProperty("prototype")) != null && (protoAssignments = prototype.getAssignments()) != null && !protoAssignments.isEmpty()) {
            protoAssignments = new ArrayList<TypeUsage>(protoAssignments);
            Collection<? extends JsObject> variables = ModelUtils.getVariables(ModelUtils.getDeclarationScope(this));
            for (TypeUsage typeUsage : protoAssignments) {
                for (JsObject variable : variables) {
                    if (!typeUsage.getType().equals(variable.getName()) || typeUsage.getType().equals(variable.getFullyQualifiedName())) continue;
                    prototype.addAssignment(new TypeUsageImpl(variable.getFullyQualifiedName(), typeUsage.getOffset(), true), typeUsage.getOffset());
                }
            }
        }
        if (!this.isDeclared() && this.getParent() != null && (prototype = this.getParent().getProperty("prototype")) != null && (prototypeProperty = prototype.getProperty(this.getName())) != null && prototypeProperty.isDeclared()) {
            for (Occurrence occurrence : this.getOccurrences()) {
                prototypeProperty.addOccurrence(occurrence.getOffsetRange());
            }
            this.getParent().getProperties().remove(this.getName());
        }
    }

    protected void clearOccurrences() {
        this.occurrences.clear();
    }

    public static void moveOccurrenceOfProperties(JsObjectImpl original, JsObject created) {
        if (original.equals(created)) {
            return;
        }
        Collection<JsObject> prototypeChains = JsObjectImpl.findPrototypeChain(original);
        for (JsObject jsObject : prototypeChains) {
            for (JsObject origProperty : jsObject.getProperties().values()) {
                JsObjectImpl usedProperty;
                if (!origProperty.getModifiers().contains((Object)Modifier.PUBLIC) && !origProperty.getModifiers().contains((Object)Modifier.PROTECTED) || (usedProperty = (JsObjectImpl)created.getProperty(origProperty.getName())) == null) continue;
                JsObjectImpl.moveOccurrence((JsObjectImpl)origProperty, usedProperty);
                JsObjectImpl.moveOccurrenceOfProperties((JsObjectImpl)origProperty, usedProperty);
            }
            JsObject prototype = jsObject.getProperty("prototype");
            if (prototype == null) continue;
            JsObjectImpl.moveOccurrenceOfProperties((JsObjectImpl)prototype, created);
        }
    }

    public static void moveOccurrence(JsObjectImpl original, JsObject created) {
        original.addOccurrence(created.getDeclarationName() != null ? created.getDeclarationName().getOffsetRange() : OffsetRange.NONE);
        for (Occurrence occur : created.getOccurrences()) {
            original.addOccurrence(occur.getOffsetRange());
        }
        ((JsObjectImpl)created).clearOccurrences();
        if (original.isDeclared() && created.isDeclared()) {
            ((JsObjectImpl)created).setDeclared(false);
        }
    }

    public static Collection<JsObject> findPrototypeChain(JsObject object) {
        ArrayList<JsObject> chain = new ArrayList<JsObject>();
        chain.add(object);
        chain.addAll(JsObjectImpl.findPrototypeChain(object, new HashSet<String>()));
        return chain;
    }

    private static List<JsObject> findPrototypeChain(JsObject object, Set<String> alreadyCheck) {
        ArrayList<JsObject> result = new ArrayList<JsObject>();
        String fqn = object.getFullyQualifiedName();
        if (!alreadyCheck.contains(fqn)) {
            alreadyCheck.add(fqn);
            JsObject prototype = object.getProperty("prototype");
            if (prototype != null && !prototype.getAssignments().isEmpty()) {
                JsObject global = ModelUtils.getGlobalObject(object);
                for (TypeUsage type : prototype.getAssignments()) {
                    if (!type.isResolved()) {
                        Collection<TypeUsage> resolved = ModelUtils.resolveTypeFromSemiType(object, type);
                        for (TypeUsage rType : resolved) {
                            JsObject fObject;
                            if (!rType.isResolved() || (fObject = ModelUtils.findJsObjectByName(global, rType.getType())) == null) continue;
                            result.add(fObject);
                            result.addAll(JsObjectImpl.findPrototypeChain(fObject, alreadyCheck));
                        }
                        continue;
                    }
                    JsObject fObject = ModelUtils.findJsObjectByName(global, type.getType());
                    if (fObject == null) continue;
                    result.add(fObject);
                    result.addAll(JsObjectImpl.findPrototypeChain(fObject, alreadyCheck));
                }
            }
        }
        return result;
    }

    private JsObjectImpl findRightTypeAssignment(int offset, JsObject global) {
        JsObject current;
        Collection<? extends TypeUsage> findedAssignments;
        JsObject currentParent = this;
        ArrayList<String> propertyPath = new ArrayList<String>();
        do {
            current = currentParent;
            findedAssignments = current.getAssignmentForOffset(offset);
            propertyPath.add(current.getName());
            currentParent = current.getParent();
        } while (findedAssignments.isEmpty() && currentParent != null);
        for (TypeUsage type : findedAssignments) {
            current = ModelUtils.findJsObjectByName(global, type.getType());
            for (int i = propertyPath.size() - 2; i > -1 && current != null; current = current.getProperty((String)((String)propertyPath.get((int)i))), --i) {
            }
            if (current == null) continue;
            return current;
        }
        return null;
    }

    @Override
    public Documentation getDocumentation() {
        return this.documentation;
    }

    public void setDocumentation(Documentation doc) {
        this.documentation = doc;
    }

    @Override
    public boolean isDeprecated() {
        return this.getModifiers().contains((Object)Modifier.DEPRECATED);
    }

    public void setDeprecated(boolean depreceted) {
        if (depreceted) {
            this.getModifiers().add(Modifier.DEPRECATED);
        } else {
            this.getModifiers().remove((Object)Modifier.DEPRECATED);
        }
    }
}

