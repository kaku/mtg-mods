/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;

public class Utils {
    public static Collection<String> getDisplayNames(Collection<? extends Type> types) {
        ArrayList<String> displayNames = new ArrayList<String>(types.size());
        for (Type type : types) {
            String displayName = type.getDisplayName();
            if (displayName.isEmpty()) continue;
            displayNames.add(displayName);
        }
        Collections.sort(displayNames);
        return displayNames;
    }

    public static Collection<String> getDisplayNamesFromStrings(Collection<String> fqns) {
        ArrayList<String> displayNames = new ArrayList<String>(fqns.size());
        for (String fqn : fqns) {
            String displayName = ModelUtils.getDisplayName(fqn);
            if (displayName.length() == 0) continue;
            displayNames.add(displayName);
        }
        return displayNames;
    }
}

