/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$AstRule
 *  org.netbeans.modules.csl.api.Rule$ErrorRule
 *  org.netbeans.modules.csl.api.Rule$UserConfigurableRule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.netbeans.modules.csl.spi.ParserResult
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsAstRule;
import org.netbeans.modules.javascript2.editor.hints.JsConventionRule;
import org.netbeans.modules.javascript2.editor.hints.JsFunctionDocumentationRule;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;

public class JsHintsProvider
implements HintsProvider {
    private static final Logger LOGGER = Logger.getLogger(JsHintsProvider.class.getName());
    private volatile boolean cancel = false;

    @SuppressWarnings(value={"BC_UNCONFIRMED_CAST"})
    public void computeHints(HintsProvider.HintsManager manager, RuleContext context, List<Hint> hints) {
        List otherHints;
        this.resume();
        Map allHints = manager.getHints(false, context);
        List conventionHints = (List)allHints.get("jsconvention.option.hints");
        boolean countConventionHints = false;
        if (conventionHints != null) {
            for (Rule.AstRule astRule : conventionHints) {
                if (!manager.isEnabled((Rule.UserConfigurableRule)astRule)) continue;
                countConventionHints = true;
            }
        }
        if (countConventionHints && !this.cancel) {
            JsConventionRule rule = new JsConventionRule();
            this.invokeHint(rule, manager, context, hints, -1);
        }
        List documentationRules = (List)allHints.get("jsdocumentation.option.hints");
        boolean documentationHints = false;
        if (documentationRules != null) {
            for (Rule.AstRule astRule : documentationRules) {
                if (!manager.isEnabled((Rule.UserConfigurableRule)astRule)) continue;
                documentationHints = true;
            }
        }
        if (documentationHints && !this.cancel) {
            JsFunctionDocumentationRule rule = new JsFunctionDocumentationRule();
            this.invokeHint(rule, manager, context, hints, -1);
        }
        if ((otherHints = (List)allHints.get("js.other.hints")) != null && !this.cancel) {
            for (Rule.AstRule astRule : otherHints) {
                if (!manager.isEnabled((Rule.UserConfigurableRule)astRule)) continue;
                JsAstRule rule = (JsAstRule)astRule;
                this.invokeHint(rule, manager, context, hints, -1);
            }
        }
    }

    public void computeSuggestions(HintsProvider.HintsManager manager, RuleContext context, List<Hint> suggestions, int caretOffset) {
        this.resume();
        Map allSuggestions = manager.getHints(true, context);
        List otherHints = (List)allSuggestions.get("js.other.hints");
        if (otherHints != null && !this.cancel) {
            for (Rule.AstRule astRule : otherHints) {
                if (!manager.isEnabled((Rule.UserConfigurableRule)astRule)) continue;
                JsAstRule rule = (JsAstRule)astRule;
                this.invokeHint(rule, manager, context, suggestions, caretOffset);
            }
        }
    }

    public void computeSelectionHints(HintsProvider.HintsManager manager, RuleContext context, List<Hint> suggestions, int start, int end) {
    }

    public void computeErrors(HintsProvider.HintsManager manager, RuleContext context, List<Hint> hints, List<Error> unhandled) {
        this.resume();
        JsParserResult parserResult = (JsParserResult)context.parserResult;
        List<? extends Error> errors = parserResult.getDiagnostics();
        unhandled.addAll(errors);
    }

    public void cancel() {
        this.cancel = true;
    }

    private void resume() {
        this.cancel = false;
    }

    public List<Rule> getBuiltinRules() {
        return Collections.emptyList();
    }

    public RuleContext createRuleContext() {
        return new JsRuleContext();
    }

    private void invokeHint(JsAstRule rule, HintsProvider.HintsManager manager, RuleContext context, List<Hint> suggestions, int caretOffset) {
        try {
            rule.computeHints((JsRuleContext)context, suggestions, caretOffset, manager);
        }
        catch (BadLocationException ble) {
            // empty catch block
        }
    }

    private static class JsErrorRule
    implements Rule {
        private JsErrorRule() {
        }

        public boolean appliesTo(RuleContext context) {
            return true;
        }

        public String getDisplayName() {
            return Bundle.JsErrorRule_displayName();
        }

        public boolean showInTasklist() {
            return true;
        }

        public HintSeverity getDefaultSeverity() {
            return HintSeverity.ERROR;
        }
    }

    private static class JsSwitchRule
    implements Rule.ErrorRule {
        private JsSwitchRule() {
        }

        public Set<?> getCodes() {
            return Collections.emptySet();
        }

        public boolean appliesTo(RuleContext context) {
            return true;
        }

        public String getDisplayName() {
            return Bundle.JsSwitchRule_displayName();
        }

        public boolean showInTasklist() {
            return false;
        }

        public HintSeverity getDefaultSeverity() {
            return HintSeverity.INFO;
        }
    }

    public static class JsRuleContext
    extends RuleContext {
        private JsParserResult jsParserResult = null;

        public JsParserResult getJsParserResult() {
            if (this.jsParserResult == null) {
                this.jsParserResult = (JsParserResult)this.parserResult;
            }
            return this.jsParserResult;
        }
    }

}

