/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collections;
import java.util.Set;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.classpath.ClassPathProviderImpl;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public abstract class JsElementImpl
implements JsElement {
    private FileObject fileObject;
    private final String name;
    private boolean isDeclared;
    private final OffsetRange offsetRange;
    private final Set<Modifier> modifiers;
    private final String mimeType;
    private final String sourceLabel;

    public JsElementImpl(FileObject fileObject, String name, boolean isDeclared, OffsetRange offsetRange, Set<Modifier> modifiers, @NullAllowed String mimeType, @NullAllowed String sourceLabel) {
        this.fileObject = fileObject;
        this.name = name;
        this.offsetRange = offsetRange;
        this.modifiers = modifiers == null ? Collections.EMPTY_SET : modifiers;
        this.isDeclared = isDeclared;
        assert (mimeType == null || JsElementImpl.isCorrectMimeType(mimeType));
        this.mimeType = mimeType;
        this.sourceLabel = sourceLabel;
    }

    private static boolean isCorrectMimeType(String mt) {
        if ("text/javascript".equals(mt) || "text/x-json".equals(mt)) {
            return true;
        }
        MimePath mp = MimePath.get((String)mt);
        String inhType = mp.getInheritedType();
        return "text/javascript".equals(inhType) || "text/x-json".equals(inhType);
    }

    public ElementKind getKind() {
        return JsElementImpl.convertJsKindToElementKind(this.getJSKind());
    }

    public FileObject getFileObject() {
        return this.fileObject;
    }

    protected void setFileObject(FileObject fileObject) {
        this.fileObject = fileObject;
    }

    public String getMimeType() {
        if (this.mimeType != null) {
            return this.mimeType;
        }
        return "text/javascript";
    }

    public String getName() {
        return this.name;
    }

    public String getIn() {
        return null;
    }

    @Override
    public boolean isDeclared() {
        return this.isDeclared;
    }

    public void setDeclared(boolean isDeclared) {
        this.isDeclared = isDeclared;
    }

    public final OffsetRange getOffsetRange(ParserResult result) {
        int start = result.getSnapshot().getOriginalOffset(this.offsetRange.getStart());
        if (start < 0) {
            return OffsetRange.NONE;
        }
        int end = result.getSnapshot().getOriginalOffset(this.offsetRange.getEnd());
        return new OffsetRange(start, end);
    }

    @Override
    public final OffsetRange getOffsetRange() {
        return this.offsetRange;
    }

    @Override
    public int getOffset() {
        return this.offsetRange.getStart();
    }

    public Set<Modifier> getModifiers() {
        return this.modifiers;
    }

    public boolean signatureEquals(ElementHandle handle) {
        return false;
    }

    public void addModifier(Modifier modifier) {
        this.modifiers.add(modifier);
    }

    @CheckForNull
    @Override
    public String getSourceLabel() {
        return this.sourceLabel;
    }

    @Override
    public boolean isPlatform() {
        FileObject fo = this.getFileObject();
        if (fo != null) {
            return JsElementImpl.isInternalFile(fo);
        }
        return false;
    }

    private static boolean isInternalFile(FileObject file) {
        for (FileObject dir : ClassPathProviderImpl.getJsStubs()) {
            if (!dir.equals((Object)file) && !FileUtil.isParentOf((FileObject)dir, (FileObject)file)) continue;
            return true;
        }
        return false;
    }

    public static ElementKind convertJsKindToElementKind(JsElement.Kind jsKind) {
        ElementKind result = ElementKind.OTHER;
        switch (jsKind) {
            case CONSTRUCTOR: {
                result = ElementKind.CONSTRUCTOR;
                break;
            }
            case METHOD: 
            case FUNCTION: 
            case PROPERTY_GETTER: 
            case PROPERTY_SETTER: {
                result = ElementKind.METHOD;
                break;
            }
            case OBJECT: 
            case ANONYMOUS_OBJECT: 
            case OBJECT_LITERAL: {
                result = ElementKind.CLASS;
                break;
            }
            case PROPERTY: {
                result = ElementKind.FIELD;
                break;
            }
            case FILE: {
                result = ElementKind.FILE;
                break;
            }
            case PARAMETER: {
                result = ElementKind.PARAMETER;
                break;
            }
            case VARIABLE: {
                result = ElementKind.VARIABLE;
                break;
            }
            case FIELD: {
                result = ElementKind.FIELD;
                break;
            }
        }
        return result;
    }

}

