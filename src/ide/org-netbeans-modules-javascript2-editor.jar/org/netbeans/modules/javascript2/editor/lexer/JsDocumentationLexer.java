/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 */
package org.netbeans.modules.javascript2.editor.lexer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationColoringLexer;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsLexer;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

final class JsDocumentationLexer
implements Lexer<JsDocumentationTokenId> {
    private static final Logger LOGGER = Logger.getLogger(JsDocumentationLexer.class.getName());
    private final JsDocumentationColoringLexer coloringLexer;
    private TokenFactory<JsDocumentationTokenId> tokenFactory;

    private JsDocumentationLexer(LexerRestartInfo<JsDocumentationTokenId> info) {
        this.coloringLexer = new JsDocumentationColoringLexer(info);
        this.tokenFactory = info.tokenFactory();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static JsDocumentationLexer create(LexerRestartInfo<JsDocumentationTokenId> info) {
        Class<JsDocumentationLexer> class_ = JsDocumentationLexer.class;
        synchronized (JsDocumentationLexer.class) {
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return new JsDocumentationLexer(info);
        }
    }

    public Token<JsDocumentationTokenId> nextToken() {
        try {
            JsDocumentationTokenId tokenId = this.coloringLexer.nextToken();
            LOGGER.log(Level.FINEST, "Lexed token is {0}", (Object)tokenId);
            Token token = null;
            if (tokenId != null) {
                token = this.tokenFactory.createToken((TokenId)tokenId);
            }
            return token;
        }
        catch (IOException ex) {
            Logger.getLogger(JsLexer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object state() {
        return this.coloringLexer.getState();
    }

    public void release() {
    }
}

