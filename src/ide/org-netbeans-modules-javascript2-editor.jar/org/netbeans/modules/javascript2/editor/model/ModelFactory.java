/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.javascript2.editor.model;

import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;

public final class ModelFactory {
    private ModelFactory() {
    }

    @NonNull
    public static Model getModel(JsParserResult info) {
        return new Model(info);
    }
}

