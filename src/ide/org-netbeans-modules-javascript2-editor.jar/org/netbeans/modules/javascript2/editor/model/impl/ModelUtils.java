/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.Node
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import jdk.nashorn.internal.ir.Node;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.embedding.JsEmbeddingProvider;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsArray;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.JsWith;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.DeclarationScopeImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionReference;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelBuilder;
import org.netbeans.modules.javascript2.editor.model.impl.ModelExtender;
import org.netbeans.modules.javascript2.editor.model.impl.ParameterObject;
import org.netbeans.modules.javascript2.editor.model.impl.SemiTypeResolverVisitor;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.openide.filesystems.FileObject;

public class ModelUtils {
    public static final String PROTOTYPE = "prototype";
    public static final String ARGUMENTS = "arguments";
    private static final String GENERATED_FUNCTION_PREFIX = "_L";
    private static final String GENERATED_ANONYM_PREFIX = "Anonym$";
    private static final List<String> KNOWN_TYPES = Arrays.asList("Array", "String", "Boolean", "Number", "undefined");
    private static final Collection<JsTokenId> CTX_DELIMITERS = Arrays.asList(new JsTokenId[]{JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY, JsTokenId.OPERATOR_SEMICOLON});
    private static List<String> knownGlobalObjects = Arrays.asList("window", "document", "console", "clearInterval", "clearTimeout", "event", "frames", "history", "Image", "location", "name", "navigator", "Option", "parent", "screen", "setInterval", "setTimeout", "XMLHttpRequest", "JSON", "Date", "undefined", "Math", "Array", "Object", "Boolean", "null", "Number", "RegExp", "String", "undefined", "unresolved");

    public static JsObjectImpl getJsObject(ModelBuilder builder, List<Identifier> fqName, boolean isLHS) {
        int index;
        JsObject result = builder.getCurrentObject();
        JsObject tmpObject = null;
        String firstName = fqName.get(0).getName();
        while (tmpObject == null && result != null && result.getParent() != null) {
            if (result instanceof JsFunctionImpl) {
                tmpObject = ((JsFunctionImpl)result).getParameter(firstName);
            }
            if (tmpObject == null) {
                if (result.getProperty(firstName) != null) {
                    tmpObject = result;
                }
                result = result.getParent();
                continue;
            }
            result = tmpObject;
        }
        if (tmpObject == null) {
            JsObjectImpl current = builder.getCurrentObject();
            if (current instanceof JsWith) {
                tmpObject = current;
            } else {
                for (DeclarationScope scope = builder.getCurrentDeclarationFunction(); scope != null && tmpObject == null && scope.getParentScope() != null; scope = scope.getParentScope()) {
                    tmpObject = ((JsFunction)scope).getParameter(firstName);
                }
                if (tmpObject == null) {
                    tmpObject = builder.getGlobal();
                } else {
                    result = tmpObject;
                }
            }
        }
        int n = index = tmpObject instanceof ParameterObject ? 1 : 0;
        while (index < fqName.size()) {
            Identifier name = fqName.get(index);
            result = tmpObject.getProperty(name.getName());
            if (result == null) {
                result = new JsObjectImpl(tmpObject, name, name.getOffsetRange(), index < fqName.size() - 1 ? false : isLHS, tmpObject.getMimeType(), tmpObject.getSourceLabel());
                tmpObject.addProperty(name.getName(), result);
            }
            tmpObject = result;
            ++index;
        }
        return result;
    }

    public static boolean isGlobal(JsObject object) {
        return object != null && object.getJSKind() == JsElement.Kind.FILE;
    }

    public static boolean isDescendant(JsObject possibleDescendant, JsObject possibleAncestor) {
        JsObject parent;
        for (parent = possibleDescendant; parent != null && !parent.equals(possibleAncestor); parent = parent.getParent()) {
        }
        return parent != null;
    }

    public static JsObject findJsObject(Model model, int offset) {
        JsObject result = null;
        JsObject global = model.getGlobalObject();
        result = ModelUtils.findJsObject(global, offset);
        if (result == null) {
            result = global;
        }
        return result;
    }

    public static JsObject findJsObject(JsObject object, int offset) {
        HashSet<String> visited = new HashSet<String>();
        return ModelUtils.findJsObject(object, offset, visited);
    }

    public static JsObject findJsObject(JsObject object, int offset, Set<String> visited) {
        JsObjectImpl jsObject = (JsObjectImpl)object;
        visited.add(jsObject.getFullyQualifiedName());
        JsObject result = null;
        JsObject tmpObject = null;
        if (jsObject.containsOffset(offset)) {
            result = jsObject;
            for (JsObject property : jsObject.getProperties().values()) {
                JsElement.Kind kind = property.getJSKind();
                if (!(kind != JsElement.Kind.OBJECT && kind != JsElement.Kind.ANONYMOUS_OBJECT && kind != JsElement.Kind.OBJECT_LITERAL && kind != JsElement.Kind.FUNCTION && kind != JsElement.Kind.METHOD && kind != JsElement.Kind.CONSTRUCTOR && kind != JsElement.Kind.WITH_OBJECT || visited.contains(property.getFullyQualifiedName()))) {
                    tmpObject = ModelUtils.findJsObject(property, offset, visited);
                }
                if (tmpObject == null) continue;
                result = tmpObject;
                break;
            }
            if (object instanceof JsArray) {
                JsArray array = (JsArray)object;
                block1 : for (TypeUsage type : array.getTypesInArray()) {
                    int anonymOffset;
                    if (!type.getType().startsWith("@anonym;") || (anonymOffset = Integer.parseInt(type.getType().substring("@anonym;".length()))) <= 0) continue;
                    DeclarationScope scope = ModelUtils.getDeclarationScope(array);
                    for (JsObject property2 : ((JsObject)((Object)scope)).getProperties().values()) {
                        JsElement.Kind kind = property2.getJSKind();
                        if (kind == JsElement.Kind.ANONYMOUS_OBJECT && !visited.contains(property2.getFullyQualifiedName())) {
                            tmpObject = ModelUtils.findJsObject(property2, offset, visited);
                        }
                        if (tmpObject == null) continue;
                        result = tmpObject;
                        continue block1;
                    }
                }
            }
        }
        return result;
    }

    public static JsObject findJsObjectByName(JsObject global, String fqName) {
        JsObject result;
        JsObject property = result = global;
        StringTokenizer stringTokenizer = new StringTokenizer(fqName, ".");
        while (stringTokenizer.hasMoreTokens() && result != null) {
            String token = stringTokenizer.nextToken();
            property = result.getProperty(token);
            if (property == null) {
                JsObject jsObject = result instanceof JsFunction ? ((JsFunction)result).getParameter(token) : null;
                result = jsObject;
                if (result != null) continue;
                break;
            }
            result = property;
        }
        return result;
    }

    public static JsObject findJsObjectByName(Model model, String fqName) {
        return ModelUtils.findJsObjectByName(model.getGlobalObject(), fqName);
    }

    public static JsObject getGlobalObject(JsObject jsObject) {
        JsObject result = jsObject;
        while (result.getParent() != null) {
            result = result.getParent();
        }
        return result;
    }

    public static DeclarationScope getDeclarationScope(JsObject object) {
        assert (object != null);
        JsObject result = object;
        while (result.getParent() != null && !(result.getParent() instanceof DeclarationScope)) {
            result = result.getParent();
        }
        if (result.getParent() != null && result.getParent() instanceof DeclarationScope) {
            result = result.getParent();
        }
        if (!(result instanceof DeclarationScope)) {
            result = ModelUtils.getGlobalObject(object);
        }
        return (DeclarationScope)((Object)result);
    }

    public static DeclarationScope getDeclarationScope(Model model, int offset) {
        DeclarationScope result = null;
        JsObject global = model.getGlobalObject();
        result = ModelUtils.getDeclarationScope((DeclarationScope)((Object)global), offset);
        if (result == null) {
            result = (DeclarationScope)((Object)global);
        }
        return result;
    }

    public static DeclarationScope getDeclarationScope(DeclarationScope scope, int offset) {
        DeclarationScopeImpl dScope = (DeclarationScopeImpl)scope;
        DeclarationScope result = null;
        if (result == null && dScope.getOffsetRange().containsInclusive(offset)) {
            result = dScope;
            boolean deep = true;
            block0 : while (deep) {
                deep = false;
                for (DeclarationScope innerScope : result.getChildrenScopes()) {
                    if (!((DeclarationScopeImpl)innerScope).getOffsetRange().containsInclusive(offset)) continue;
                    result = innerScope;
                    deep = true;
                    continue block0;
                }
            }
        }
        return result;
    }

    public static OffsetRange documentOffsetRange(JsParserResult result, int start, int end) {
        int lStart = LexUtilities.getLexerOffset(result, start);
        int lEnd = LexUtilities.getLexerOffset(result, end);
        if (lStart == -1 || lEnd == -1) {
            return OffsetRange.NONE;
        }
        if (lEnd < lStart) {
            int length = lStart - lEnd;
            lEnd = lStart + length;
        }
        return new OffsetRange(lStart, lEnd);
    }

    public static Collection<? extends JsObject> getVariables(DeclarationScope inScope) {
        HashMap<String, JsObject> result = new HashMap<String, JsObject>();
        while (inScope != null) {
            for (JsObject object22 : ((JsObject)((Object)inScope)).getProperties().values()) {
                if (result.containsKey(object22.getName()) || !object22.getModifiers().contains((Object)Modifier.PRIVATE)) continue;
                result.put(object22.getName(), object22);
            }
            for (JsObject object : ((JsFunction)inScope).getParameters()) {
                if (result.containsKey(object.getName())) continue;
                result.put(object.getName(), object);
            }
            for (JsObject object2 : ((JsObject)((Object)inScope)).getProperties().values()) {
                if (result.containsKey(object2.getName())) continue;
                result.put(object2.getName(), object2);
            }
            if (inScope.getParentScope() != null && !result.containsKey(((JsObject)((Object)inScope)).getName())) {
                result.put(((JsObject)((Object)inScope)).getName(), (JsObject)((Object)inScope));
            }
            inScope = inScope.getParentScope();
        }
        return result.values();
    }

    public static Collection<? extends JsObject> getVariables(Model model, int offset) {
        DeclarationScope scope = ModelUtils.getDeclarationScope(model, offset);
        return ModelUtils.getVariables(scope);
    }

    public static JsObject getJsObjectByName(DeclarationScope inScope, String simpleName) {
        Collection<? extends JsObject> variables = ModelUtils.getVariables(inScope);
        for (JsObject jsObject : variables) {
            if (!simpleName.equals(jsObject.getName())) continue;
            return jsObject;
        }
        return null;
    }

    private static Collection<TypeUsage> tryResolveWindowProperty(Model model, JsIndex jsIndex, String name) {
        String fqn = null;
        for (IndexedElement indexedElement2 : jsIndex.getProperties("window")) {
            if (!indexedElement2.getName().equals(name)) continue;
            fqn = "window." + name;
            break;
        }
        if (fqn == null) {
            for (IndexedElement indexedElement2 : jsIndex.getProperties("Window.prototype")) {
                if (!indexedElement2.getName().equals(name)) continue;
                fqn = "Window.prototype." + name;
                break;
            }
        }
        if (fqn != null) {
            ArrayList<TypeUsage> fromAssignment = new ArrayList<TypeUsage>();
            ModelUtils.resolveAssignments(model, jsIndex, fqn, fromAssignment);
            if (fromAssignment.isEmpty()) {
                fromAssignment.add(new TypeUsageImpl(fqn));
            }
            return fromAssignment;
        }
        return null;
    }

    private static String getSemiType(TokenSequence<JsTokenId> ts, int offset) {
        String result = "UNKNOWN";
        ts.move(offset);
        if (!ts.moveNext()) {
            return result;
        }
        State state = State.INIT;
        while (ts.movePrevious()) {
            Token token = ts.token();
            if (CTX_DELIMITERS.contains((Object)token.id())) continue;
            switch (state) {
                case INIT: {
                    if (token.id() == JsTokenId.IDENTIFIER) {
                        // empty if block
                    } else {
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static Collection<TypeUsage> resolveSemiTypeOfExpression(ModelBuilder builder, Node expression) {
        Set result = new HashSet();
        SemiTypeResolverVisitor visitor = new SemiTypeResolverVisitor();
        if (expression != null) {
            result = visitor.getSemiTypes(expression);
        }
        if (builder.getCurrentWith() != null) {
            HashSet<TypeUsage> withResult = new HashSet<TypeUsage>();
            String withSemi = "@with;" + builder.getCurrentWith().getFullyQualifiedName();
            for (TypeUsage type : result) {
                if (!KNOWN_TYPES.contains(type.getType())) {
                    withResult.add(new TypeUsageImpl(withSemi + type.getType(), type.getOffset(), type.isResolved()));
                    continue;
                }
                withResult.add(type);
            }
            result = withResult;
        }
        return result;
    }

    public static Collection<TypeUsage> resolveTypeFromSemiType(JsObject object, TypeUsage type) {
        HashSet<TypeUsage> result = new HashSet<TypeUsage>();
        if (type.isResolved()) {
            result.add(type);
        } else if ("undefined".equals(type.getType())) {
            if (object.getJSKind() == JsElement.Kind.CONSTRUCTOR) {
                result.add(new TypeUsageImpl(object.getFullyQualifiedName(), type.getOffset(), true));
            } else {
                result.add(new TypeUsageImpl("undefined", type.getOffset(), true));
            }
        } else if (JsEmbeddingProvider.containsGeneratedIdentifier(type.getType())) {
            result.add(new TypeUsageImpl("undefined", type.getOffset(), true));
        } else if ("@this;".equals(type.getType())) {
            JsObject parent = ModelUtils.resolveThis(object);
            if (parent != null) {
                result.add(new TypeUsageImpl(parent.getFullyQualifiedName(), type.getOffset(), true));
            }
        } else if (type.getType().startsWith("@this;")) {
            JsObject parent = ModelUtils.resolveThis(object);
            if (parent != null) {
                Collection<TypeUsage> locally = ModelUtils.resolveSemiTypeChain(parent, type.getType().substring(6));
                if (locally.isEmpty()) {
                    result.add(new TypeUsageImpl(type.getType().replace("@this;", parent.getFullyQualifiedName()), type.getOffset(), false));
                } else {
                    TypeUsage localType;
                    if (locally.size() == 1 && (localType = locally.iterator().next()).isResolved()) {
                        JsFunctionImpl function;
                        JsObject rObject = ModelUtils.findJsObjectByName(ModelUtils.getGlobalObject(object), localType.getType());
                        JsFunction jsFunction = rObject instanceof JsFunctionImpl ? (JsFunctionImpl)rObject : (function = rObject instanceof JsFunctionReference ? ((JsFunctionReference)rObject).getOriginal() : null);
                        if (function != null && function.getParent() != null && object != null && function.getParent().equals(object.getParent()) && object.getDeclarationName() != null) {
                            object.getParent().addProperty(object.getName(), new JsFunctionReference(object.getParent(), object.getDeclarationName(), function, true, null));
                        }
                    }
                    result.addAll(locally);
                }
            }
        } else if (type.getType().startsWith("@new;")) {
            result.addAll(ModelUtils.resolveSemiTypeCallChain(object, type));
        } else if (type.getType().startsWith("@call;")) {
            result.addAll(ModelUtils.resolveSemiTypeCallChain(object, type));
        } else if (type.getType().startsWith("@anonym;")) {
            int start;
            JsObject byOffset;
            String offsetPart = type.getType().substring(8);
            String rest = "";
            int index = offsetPart.indexOf("@");
            if (index > -1) {
                rest = offsetPart.substring(index);
                offsetPart = offsetPart.substring(0, index);
            }
            if ((byOffset = ModelUtils.findJsObject(object, start = Integer.parseInt(offsetPart))) == null) {
                JsObject globalObject = ModelUtils.getGlobalObject(object);
                byOffset = ModelUtils.findJsObject(globalObject, start);
            }
            if (byOffset != null && byOffset.isAnonymous()) {
                if (rest.isEmpty()) {
                    result.add(new TypeUsageImpl(byOffset.getFullyQualifiedName(), byOffset.getOffset(), true));
                } else {
                    String newType = "@exp;" + byOffset.getFullyQualifiedName().replace(".", "@pro;");
                    newType = newType + rest;
                    result.add(new TypeUsageImpl(newType, byOffset.getOffset(), false));
                }
            }
        } else if (type.getType().startsWith("@var;")) {
            String name = type.getType().substring(5);
            JsFunction declarationScope = object instanceof DeclarationScope ? (JsFunction)object : (JsFunction)ModelUtils.getDeclarationScope(object);
            Collection<? extends JsObject> variables = ModelUtils.getVariables(declarationScope);
            if (declarationScope != null) {
                boolean resolved = false;
                for (JsObject variable : variables) {
                    String newVarType;
                    if (!variable.getName().equals(name)) continue;
                    if (!variable.getAssignments().isEmpty()) {
                        newVarType = "@exp;" + variable.getFullyQualifiedName().replace(".", "@pro;");
                        result.add(new TypeUsageImpl(newVarType, type.getOffset(), false));
                        resolved = true;
                        break;
                    }
                    if (variable.getJSKind() == JsElement.Kind.PARAMETER) continue;
                    if (variable.getJSKind().isFunction() && object.getAssignments().size() == 1 && object.getParent() != null && object.getDeclarationName() != null) {
                        JsObject oldProperty = object.getParent().getProperty(object.getName());
                        JsFunctionReference newProperty = new JsFunctionReference(object.getParent(), object.getDeclarationName(), (JsFunction)variable, true, oldProperty.getModifiers());
                        for (Occurrence occurrence : oldProperty.getOccurrences()) {
                            newProperty.addOccurrence(occurrence.getOffsetRange());
                        }
                        object.getParent().addProperty(object.getName(), newProperty);
                    } else {
                        newVarType = variable.getFullyQualifiedName();
                        result.add(new TypeUsageImpl(newVarType, type.getOffset(), false));
                    }
                    resolved = true;
                    break;
                }
                if (!resolved) {
                    Collection<? extends JsObject> parameters = declarationScope.getParameters();
                    boolean isParameter = false;
                    for (JsObject parameter : parameters) {
                        if (!name.equals(parameter.getName())) continue;
                        Collection<? extends TypeUsage> assignments = parameter.getAssignmentForOffset(parameter.getOffset());
                        result.addAll(assignments);
                        isParameter = true;
                        break;
                    }
                    if (!isParameter) {
                        result.add(new TypeUsageImpl(name, type.getOffset(), false));
                    }
                }
            }
        } else if (type.getType().startsWith("@param;")) {
            String functionName = type.getType().substring(7);
            int index = functionName.indexOf(":");
            if (index > 0) {
                JsObject param;
                String fqn = functionName.substring(0, index);
                JsObject globalObject = ModelUtils.getGlobalObject(object);
                JsObject function = ModelUtils.findJsObjectByName(globalObject, fqn);
                if (function instanceof JsFunction && (param = ((JsFunction)function).getParameter(functionName.substring(index + 1))) != null) {
                    result.addAll(param.getAssignments());
                }
            }
        } else {
            result.add(type);
        }
        return result;
    }

    private static JsObject resolveThis(JsObject object) {
        JsObject grandParent;
        JsObject parent = null;
        parent = object.getJSKind() == JsElement.Kind.CONSTRUCTOR ? object : (object.getParent() != null && object.getParent().getJSKind() != JsElement.Kind.FILE ? object.getParent() : object);
        if (parent != null && (parent.getJSKind() == JsElement.Kind.FUNCTION || parent.getJSKind() == JsElement.Kind.METHOD) && parent.getParent().getJSKind() != JsElement.Kind.FILE && (grandParent = parent.getParent()) != null && (grandParent.getJSKind() == JsElement.Kind.OBJECT_LITERAL || "prototype".equals(grandParent.getName())) && "prototype".equals((parent = grandParent).getName()) && parent.getParent() != null) {
            parent = parent.getParent();
        }
        while (parent != null && parent.getParent() != null && parent.getModifiers().contains((Object)Modifier.PROTECTED)) {
            parent = parent.getParent();
        }
        return parent;
    }

    private static Collection<TypeUsage> resolveSemiTypeCallChain(JsObject object, TypeUsage type) {
        HashSet<TypeUsage> result = new HashSet<TypeUsage>();
        DeclarationScope declarationScope = ModelUtils.getDeclarationScope(object);
        JsObject function = null;
        boolean calledNew = false;
        int index = -1;
        int dotIndex = -1;
        if (type.getType().startsWith("@call;")) {
            index = 6;
        } else if (type.getType().startsWith("@new;")) {
            index = 5;
            calledNew = true;
        }
        String name = type.getType().substring(index);
        if (declarationScope != null) {
            index = name.indexOf("@");
            if (index > -1) {
                name = name.substring(0, index);
            }
            Collection<? extends JsObject> variables = ModelUtils.getVariables(declarationScope);
            dotIndex = name.indexOf(46);
            String firstSpace = dotIndex == -1 ? name : name.substring(0, name.indexOf(46));
            for (JsObject variable : variables) {
                if (!variable.getName().equals(firstSpace)) continue;
                function = variable;
                break;
            }
        }
        if (dotIndex != -1 && function != null) {
            function = ModelUtils.findJsObjectByName(function, name.substring(dotIndex + 1));
        }
        if (function != null) {
            if (index == -1) {
                if (function instanceof JsFunction) {
                    if (calledNew) {
                        result.add(new TypeUsageImpl(function.getFullyQualifiedName(), type.getOffset(), true));
                    } else {
                        result.addAll(((JsFunction)function).getReturnTypes());
                    }
                } else if (calledNew) {
                    result.add(new TypeUsageImpl(function.getFullyQualifiedName(), type.getOffset(), true));
                } else {
                    result.add(type);
                }
            } else {
                result.add(new TypeUsageImpl(type.getType().replace(name, function.getFullyQualifiedName()), type.getOffset(), false));
            }
        } else {
            result.add(type);
        }
        return result;
    }

    private static Collection<TypeUsage> resolveSemiTypeChain(JsObject object, String chain) {
        String part;
        int index;
        HashSet<TypeUsage> result = new HashSet<TypeUsage>();
        if (chain.isEmpty()) {
            return result;
        }
        if ("prototype".equals(object.getName()) && (object = object.getParent()) == null) {
            return result;
        }
        String[] parts = chain.substring(1).split("@");
        JsObject resultObject = null;
        JsObject testObject = object;
        String kind = "";
        String[] arr$ = parts;
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$ && (index = (part = arr$[i$]).indexOf(";")) > 0; ++i$) {
            JsObject prototype;
            kind = part.substring(0, index);
            String name = part.substring(index + 1);
            resultObject = testObject.getProperty(name);
            if (resultObject == null && (prototype = testObject.getProperty("prototype")) != null) {
                resultObject = prototype.getProperty(name);
            }
            if (resultObject == null) break;
            testObject = resultObject;
        }
        if (resultObject != null) {
            if (resultObject instanceof JsFunction) {
                if ("call".endsWith(kind)) {
                    ModelUtils.addUniqueType(result, ((JsFunction)resultObject).getReturnTypes());
                } else {
                    ModelUtils.addUniqueType(result, new TypeUsageImpl(resultObject.getFullyQualifiedName(), -1, true));
                }
            } else {
                Collection<? extends TypeUsage> assignments = resultObject.getAssignments();
                if (assignments.isEmpty()) {
                    ModelUtils.addUniqueType(result, new TypeUsageImpl(resultObject.getFullyQualifiedName(), -1, true));
                } else {
                    ModelUtils.addUniqueType(result, resultObject.getAssignments());
                }
            }
        }
        return result;
    }

    public static Collection<TypeUsage> resolveTypeFromExpression(Model model, @NullAllowed JsIndex jsIndex, List<String> exp, int offset) {
        ArrayList<JsObject> localObjects = new ArrayList<JsObject>();
        ArrayList<JsObject> lastResolvedObjects = new ArrayList<JsObject>();
        ArrayList<TypeUsage> lastResolvedTypes = new ArrayList<TypeUsage>();
        block0 : for (int i = exp.size() - 1; i > -1; --i) {
            String name;
            String kind = exp.get(i);
            if ((name = exp.get(--i)).startsWith("@ano:")) {
                String[] parts = name.split(":");
                int anoOffset = Integer.parseInt(parts[1]);
                JsObject anonym = ModelUtils.findJsObject(model, anoOffset);
                lastResolvedObjects.add(anonym);
                continue;
            }
            if ("this".equals(name)) {
                JsObject thisObject;
                JsObject first = thisObject = ModelUtils.findJsObject(model, offset);
                while (thisObject != null && thisObject.getParent() != null && thisObject.getJSKind() != JsElement.Kind.CONSTRUCTOR && thisObject.getJSKind() != JsElement.Kind.ANONYMOUS_OBJECT && thisObject.getJSKind() != JsElement.Kind.OBJECT_LITERAL) {
                    thisObject = thisObject.getParent();
                }
                if ((thisObject == null || thisObject.getParent() == null) && first != null) {
                    thisObject = first;
                }
                if (thisObject != null) {
                    name = thisObject.getName();
                }
            }
            if (i == exp.size() - 2) {
                JsObject localObject = null;
                int index = name.lastIndexOf(46);
                Collection<? extends TypeUsage> typeFromWith = ModelUtils.getTypeFromWith(model, offset);
                if (!typeFromWith.isEmpty()) {
                    String firstNamePart = index == -1 ? name : name.substring(0, index);
                    for (TypeUsage type : typeFromWith) {
                        String sType = type.getType();
                        localObject = ModelUtils.findJsObjectByName(model, sType);
                        if (localObject == null || localObject.getProperty(firstNamePart) == null) continue;
                        name = localObject.getFullyQualifiedName() + "." + name;
                        break;
                    }
                }
                if (index > -1) {
                    localObject = ModelUtils.findJsObjectByName(model, name);
                    if (localObject != null) {
                        localObjects.add(localObject);
                    }
                } else {
                    Collection<TypeUsage> windowProperty;
                    for (JsObject object : model.getVariables(offset)) {
                        if (!object.getName().equals(name)) continue;
                        localObjects.add(object);
                        localObject = object;
                        break;
                    }
                    block4 : for (JsObject libGlobal : ModelExtender.getDefault().getExtendingGlobalObjects()) {
                        assert (libGlobal != null);
                        for (JsObject object2 : libGlobal.getProperties().values()) {
                            if (!object2.getName().equals(name)) continue;
                            lastResolvedTypes.add(new TypeUsageImpl(object2.getName(), -1, true));
                            continue block4;
                        }
                    }
                    if (jsIndex != null && (windowProperty = ModelUtils.tryResolveWindowProperty(model, jsIndex, name)) != null && !windowProperty.isEmpty()) {
                        lastResolvedTypes.addAll(windowProperty);
                    }
                }
                if (localObject == null || localObject.getJSKind() != JsElement.Kind.PARAMETER && (ModelUtils.isGlobal(localObject.getParent()) || localObject.getJSKind() != JsElement.Kind.VARIABLE)) {
                    ArrayList<TypeUsage> fromAssignments = new ArrayList<TypeUsage>();
                    if ("@pro".equals(kind) && jsIndex != null) {
                        ModelUtils.resolveAssignments(model, jsIndex, name, fromAssignments);
                    }
                    lastResolvedTypes.addAll(fromAssignments);
                    if (!typeFromWith.isEmpty()) {
                        for (TypeUsage typeUsage : typeFromWith) {
                            String sType = typeUsage.getType();
                            if (sType.startsWith("@exp;")) {
                                sType = sType.substring(5);
                                sType = sType.replace("@pro;", ".");
                            }
                            ModelUtils.resolveAssignments(model, jsIndex, (String)sType, fromAssignments);
                            for (TypeUsage typeUsage1 : fromAssignments) {
                                String localFqn;
                                String string = localFqn = localObject != null ? localObject.getFullyQualifiedName() : null;
                                if (localFqn != null && name.startsWith(localFqn) && name.length() > localFqn.length()) {
                                    lastResolvedTypes.add(new TypeUsageImpl(typeUsage1.getType() + kind + ";" + name.substring(localFqn.length() + 1), typeUsage.getOffset(), false));
                                    continue;
                                }
                                if (!typeUsage1.getType().equals(name)) {
                                    lastResolvedTypes.add(new TypeUsageImpl(typeUsage1.getType() + kind + ";" + name, typeUsage.getOffset(), false));
                                    continue;
                                }
                                lastResolvedTypes.add(typeUsage1);
                            }
                        }
                    }
                }
                if (localObjects.isEmpty()) continue;
                for (JsObject lObject : localObjects) {
                    if (lObject.getAssignmentForOffset(offset).isEmpty()) {
                        JsObject original;
                        boolean addAsType;
                        boolean bl = addAsType = lObject.getJSKind() == JsElement.Kind.OBJECT_LITERAL;
                        if (lObject instanceof JsObjectReference && (original = ((JsObjectReference)lObject).getOriginal()) != null) {
                            String string = name = original.getDeclarationName() != null ? original.getDeclarationName().getName() : original.getName();
                        }
                        if (addAsType) {
                            lastResolvedTypes.add(new TypeUsageImpl(name, -1, true));
                        }
                    }
                    if ("@mtd".equals(kind)) {
                        if (!lObject.getJSKind().isFunction()) continue;
                        lastResolvedTypes.addAll(((JsFunction)lObject).getReturnTypes());
                        continue;
                    }
                    if ("@arr".equals(kind) && lObject instanceof JsArray) {
                        lastResolvedTypes.addAll(((JsArray)lObject).getTypesInArray());
                        continue;
                    }
                    Collection<? extends TypeUsage> lastTypeAssignment = lObject.getAssignmentForOffset(offset);
                    lastResolvedObjects.add(lObject);
                    if (lastTypeAssignment.isEmpty()) continue;
                    ModelUtils.resolveAssignments(model, lObject, offset, lastResolvedObjects, lastResolvedTypes);
                    continue block0;
                }
                continue;
            }
            ArrayList<JsObject> newResolvedObjects = new ArrayList<JsObject>();
            ArrayList<TypeUsage> newResolvedTypes = new ArrayList<TypeUsage>();
            for (JsObject localObject : lastResolvedObjects) {
                JsObject property = localObject.getProperty(name);
                if (property == null) continue;
                if ("@mtd".equals(kind)) {
                    if (!property.getJSKind().isFunction()) continue;
                    Collection<? extends TypeUsage> resovledTypes = ((JsFunction)property).getReturnTypes();
                    newResolvedTypes.addAll(resovledTypes);
                    continue;
                }
                if ("@arr".equals(kind)) {
                    if (!(property instanceof JsArray)) continue;
                    newResolvedTypes.addAll(((JsArray)property).getTypesInArray());
                    continue;
                }
                Collection<? extends TypeUsage> lastTypeAssignment = property.getAssignmentForOffset(offset);
                if (lastTypeAssignment.isEmpty()) {
                    newResolvedObjects.add(property);
                    continue;
                }
                newResolvedTypes.addAll(lastTypeAssignment);
                if (property.getProperties().isEmpty()) continue;
                newResolvedObjects.add(property);
            }
            for (TypeUsage typeUsage : lastResolvedTypes) {
                if (jsIndex != null) {
                    void indexResults22;
                    boolean checkProperty;
                    ArrayList<Object> prototypeChain = new ArrayList<Object>();
                    String typeName = typeUsage.getType();
                    if (typeName.contains("@exp;")) {
                        typeName = typeName.substring(typeName.indexOf("@exp;") + "@exp;".length());
                    }
                    if (typeName.contains("@pro;")) {
                        typeName = typeName.replace("@pro;", ".");
                    }
                    prototypeChain.add(typeName);
                    prototypeChain.addAll(ModelUtils.findPrototypeChain((String)typeName, jsIndex));
                    Object indexResults22 = null;
                    Object propertyToCheck222 = null;
                    for (String fqn : prototypeChain) {
                        String propertyToCheck222 = fqn + "." + name;
                        Collection<? extends IndexResult> indexResults22 = jsIndex.findByFqn(propertyToCheck222, "flag", "return", "array", "assign");
                        if (indexResults22.isEmpty() && !fqn.endsWith(".prototype")) {
                            propertyToCheck222 = fqn + ".prototype." + name;
                            indexResults22 = jsIndex.findByFqn(propertyToCheck222, "flag", "return", "array", "assign");
                        }
                        if (!indexResults22.isEmpty()) break;
                        Object propertyToCheck222 = null;
                    }
                    boolean bl = checkProperty = (indexResults22 == null || indexResults22.isEmpty()) && !"@mtd".equals(kind);
                    if (indexResults22 != null) {
                        for (IndexResult indexResult : indexResults22) {
                            Collection<TypeUsage> resolvedTypes;
                            JsElement.Kind jsKind = IndexedElement.Flag.getJsKind(Integer.parseInt(indexResult.getValue("flag")));
                            if ("@mtd".equals(kind) && jsKind.isFunction()) {
                                resolvedTypes = IndexedElement.getReturnTypes(indexResult);
                                ModelUtils.addUniqueType(newResolvedTypes, resolvedTypes);
                                continue;
                            }
                            if ("@arr".equals(kind)) {
                                resolvedTypes = IndexedElement.getArrayTypes(indexResult);
                                ModelUtils.addUniqueType(newResolvedTypes, resolvedTypes);
                                continue;
                            }
                            checkProperty = true;
                        }
                    }
                    if (checkProperty) {
                        void propertyToCheck222;
                        void propertyFQN = propertyToCheck222 != null ? propertyToCheck222 : (String)typeName + "." + name;
                        ArrayList<TypeUsage> fromAssignment = new ArrayList<TypeUsage>();
                        ModelUtils.resolveAssignments(model, jsIndex, (String)propertyFQN, fromAssignment);
                        if (fromAssignment.isEmpty()) {
                            ModelUtils.addUniqueType(newResolvedTypes, new TypeUsageImpl((String)propertyFQN));
                        } else {
                            ModelUtils.addUniqueType(newResolvedTypes, fromAssignment);
                        }
                    }
                }
                block13 : for (JsObject libGlobal : ModelExtender.getDefault().getExtendingGlobalObjects()) {
                    for (JsObject object : libGlobal.getProperties().values()) {
                        if (!object.getName().equals(typeUsage.getType())) continue;
                        JsObject property = object.getProperty(name);
                        if (property == null) continue block13;
                        JsElement.Kind jsKind = property.getJSKind();
                        if ("@mtd".equals(kind) && jsKind.isFunction()) {
                            newResolvedTypes.addAll(((JsFunction)property).getReturnTypes());
                            continue block13;
                        }
                        newResolvedObjects.add(property);
                        continue block13;
                    }
                }
            }
            lastResolvedObjects = newResolvedObjects;
            lastResolvedTypes = newResolvedTypes;
        }
        HashMap<String, TypeUsage> resultTypes = new HashMap<String, TypeUsage>();
        for (TypeUsage typeUsage : lastResolvedTypes) {
            if (resultTypes.containsKey(typeUsage.getType())) continue;
            resultTypes.put(typeUsage.getType(), typeUsage);
        }
        for (JsObject jsObject : lastResolvedObjects) {
            String fqn;
            if (!jsObject.isDeclared() || resultTypes.containsKey(fqn = jsObject.getFullyQualifiedName())) continue;
            resultTypes.put(fqn, new TypeUsageImpl(fqn, offset));
        }
        return resultTypes.values();
    }

    public static List<String> expressionFromType(TypeUsage type) {
        String sexp = type.getType();
        if ((sexp.startsWith("@exp;") || sexp.startsWith("@new;") || sexp.startsWith("@arr;") || sexp.startsWith("@pro;") || sexp.startsWith("@call;") || sexp.startsWith("@with;")) && sexp.length() > 5) {
            int start = sexp.startsWith("@call;") || sexp.startsWith("@arr;") || sexp.startsWith("@with;") ? 1 : (sexp.charAt(5) == '@' ? 6 : 5);
            sexp = sexp.substring(start);
            ArrayList<String> nExp = new ArrayList<String>();
            String[] split = sexp.split("@");
            for (int i = split.length - 1; i > -1; --i) {
                nExp.add(split[i].substring(split[i].indexOf(59) + 1));
                if (split[i].startsWith("arr;")) {
                    nExp.add("@arr");
                    continue;
                }
                if (split[i].startsWith("call;")) {
                    nExp.add("@mtd");
                    continue;
                }
                if (split[i].startsWith("with;")) {
                    nExp.add("@with");
                    continue;
                }
                nExp.add("@pro");
            }
            return nExp;
        }
        return Collections.singletonList(type.getType());
    }

    public static Collection<TypeUsage> resolveTypes(Collection<? extends TypeUsage> unresolved, JsParserResult parserResult, boolean useIndex) {
        ArrayList types = new ArrayList<TypeUsage>(unresolved);
        if (types.size() == 1 && types.iterator().next().isResolved()) {
            return types;
        }
        HashSet<String> original = null;
        Model model = parserResult.getModel();
        FileObject fo = parserResult.getSnapshot().getSource().getFileObject();
        JsIndex jsIndex = useIndex ? JsIndex.get(fo) : null;
        int cycle = 0;
        boolean resolvedAll = false;
        while (!resolvedAll && cycle < 10) {
            ++cycle;
            resolvedAll = true;
            ArrayList<TypeUsage> resolved = new ArrayList<TypeUsage>();
            for (TypeUsage typeUsage : types) {
                if (!typeUsage.isResolved()) {
                    if (original == null) {
                        original = new HashSet<String>(unresolved.size());
                        for (TypeUsage t : unresolved) {
                            original.add(t.getType());
                        }
                    }
                    resolvedAll = false;
                    List<String> nExp = ModelUtils.expressionFromType(typeUsage);
                    if (nExp.size() > 1) {
                        ModelUtils.addUniqueType(resolved, original, ModelUtils.resolveTypeFromExpression(model, jsIndex, nExp, typeUsage.getOffset()));
                        continue;
                    }
                    ModelUtils.addUniqueType(resolved, new TypeUsageImpl(typeUsage.getType(), typeUsage.getOffset(), true));
                    continue;
                }
                ModelUtils.addUniqueType(resolved, typeUsage);
            }
            types.clear();
            types = new ArrayList(resolved);
        }
        return types;
    }

    private static void resolveAssignments(Model model, JsObject jsObject, int offset, List<JsObject> resolvedObjects, List<TypeUsage> resolvedTypes) {
        Collection<? extends TypeUsage> assignments = jsObject.getAssignmentForOffset(offset);
        for (TypeUsage typeName : assignments) {
            if (typeName.isResolved()) {
                resolvedTypes.add(typeName);
                continue;
            }
            String type = typeName.getType();
            if (type.startsWith("@with;")) {
                List<String> expression = ModelUtils.expressionFromType(typeName);
                Collection<? extends TypeUsage> typesFromWith = ModelUtils.getTypeFromWith(model, typeName.getOffset());
                expression.remove(expression.size() - 1);
                expression.remove(expression.size() - 1);
                StringBuilder sb = new StringBuilder();
                for (int i = expression.size() - 1; i > 0; --i) {
                    sb.append(expression.get(i--));
                    sb.append(";");
                    sb.append(expression.get(i));
                }
                for (TypeUsage typeWith : typesFromWith) {
                    resolvedTypes.add(new TypeUsageImpl("@exp;" + typeWith.getType() + sb.toString(), typeName.getOffset(), false));
                }
                resolvedTypes.add(new TypeUsageImpl(sb.toString(), typeName.getOffset(), false));
                continue;
            }
            JsObject byOffset = ModelUtils.findObjectForOffset(typeName.getType(), offset, model);
            if (byOffset != null) {
                if (jsObject.getName().equals(byOffset.getName())) continue;
                resolvedObjects.add(byOffset);
                ModelUtils.resolveAssignments(model, byOffset, offset, resolvedObjects, resolvedTypes);
                continue;
            }
            resolvedTypes.add(typeName);
        }
    }

    private static void resolveAssignments(Model model, JsIndex jsIndex, String fqn, List<TypeUsage> resolved) {
        HashSet<String> alreadyProcessed = new HashSet<String>();
        for (TypeUsage type : resolved) {
            alreadyProcessed.add(type.getType());
        }
        ModelUtils.resolveAssignments(model, jsIndex, fqn, resolved, alreadyProcessed);
    }

    private static void resolveAssignments(Model model, JsIndex jsIndex, String fqn, List<TypeUsage> resolved, Set<String> alreadyProcessed) {
        if (!alreadyProcessed.contains(fqn)) {
            alreadyProcessed.add(fqn);
            if (!fqn.startsWith("@")) {
                if (jsIndex != null) {
                    JsObject found;
                    Collection<? extends TypeUsage> assignments;
                    Collection<? extends IndexResult> indexResults = jsIndex.findByFqn(fqn, "assign");
                    boolean hasAssignments = false;
                    boolean isType = false;
                    block0 : for (IndexResult indexResult : indexResults) {
                        Collection<TypeUsage> assignments2 = IndexedElement.getAssignments(indexResult);
                        if (assignments2.isEmpty()) continue;
                        hasAssignments = true;
                        for (TypeUsage type2 : assignments2) {
                            if (resolved.size() > 10) {
                                resolved.clear();
                                continue block0;
                            }
                            if (alreadyProcessed.contains(type2.getType())) continue;
                            ModelUtils.resolveAssignments(model, jsIndex, type2.getType(), resolved, alreadyProcessed);
                        }
                    }
                    if (indexResults.isEmpty() && (found = ModelUtils.findJsObjectByName(model.getGlobalObject(), fqn)) != null && !(assignments = found.getAssignments()).isEmpty()) {
                        hasAssignments = true;
                        ArrayList<TypeUsage> toProcess = new ArrayList<TypeUsage>();
                        for (TypeUsage type : assignments) {
                            if (!type.isResolved()) {
                                for (TypeUsage resolvedType : ModelUtils.resolveTypeFromSemiType(found, type)) {
                                    toProcess.add(resolvedType);
                                }
                                continue;
                            }
                            toProcess.add(type);
                        }
                        for (TypeUsage type2 : toProcess) {
                            if (alreadyProcessed.contains(type2.getType())) continue;
                            ModelUtils.resolveAssignments(model, jsIndex, type2.getType(), resolved, alreadyProcessed);
                        }
                    }
                    Collection<IndexedElement> properties = jsIndex.getProperties(fqn);
                    for (IndexedElement property : properties) {
                        if (!property.getFQN().startsWith(fqn) || !property.isDeclared() && !"prototype".equals(property.getName())) continue;
                        isType = true;
                        break;
                    }
                    if (!hasAssignments || isType) {
                        ModelUtils.addUniqueType(resolved, new TypeUsageImpl(fqn, -1, true));
                    }
                }
            } else {
                ModelUtils.addUniqueType(resolved, new TypeUsageImpl(fqn, -1, false));
            }
        }
    }

    public static JsObject findObjectForOffset(String name, int offset, Model model) {
        for (JsObject object : model.getVariables(offset)) {
            if (!object.getName().equals(name)) continue;
            return object;
        }
        return null;
    }

    public static Collection<String> findPrototypeChain(String fqn, JsIndex jsIndex) {
        Collection<String> chain = ModelUtils.findPrototypeChain(fqn, jsIndex, new HashSet<String>());
        return chain;
    }

    private static Collection<String> findPrototypeChain(String fqn, JsIndex jsIndex, Set<String> alreadyCheck) {
        ArrayList<String> result = new ArrayList<String>();
        if (!alreadyCheck.contains(fqn)) {
            alreadyCheck.add(fqn);
            Collection<IndexedElement> properties = jsIndex.getProperties(fqn);
            for (IndexedElement property : properties) {
                if (!"prototype".equals(property.getName())) continue;
                Collection<? extends IndexResult> indexResults = jsIndex.findByFqn(property.getFQN(), "assign");
                for (IndexResult indexResult : indexResults) {
                    Collection<TypeUsage> assignments = IndexedElement.getAssignments(indexResult);
                    for (TypeUsage typeUsage2 : assignments) {
                        result.add(typeUsage2.getType());
                    }
                    for (TypeUsage typeUsage : assignments) {
                        result.addAll(ModelUtils.findPrototypeChain(typeUsage.getType(), jsIndex, alreadyCheck));
                    }
                }
            }
        }
        return result;
    }

    public static Collection<? extends TypeUsage> getTypeFromWith(Model model, int offset) {
        JsObject jsObject = ModelUtils.findJsObject(model, offset);
        while (jsObject != null && jsObject.isAnonymous() && jsObject.getJSKind() != JsElement.Kind.WITH_OBJECT) {
            jsObject = ModelUtils.findJsObject(model, jsObject.getOffset() - 1);
        }
        while (jsObject != null && jsObject.getJSKind() != JsElement.Kind.WITH_OBJECT) {
            jsObject = jsObject.getParent();
        }
        if (jsObject != null && jsObject.getJSKind() == JsElement.Kind.WITH_OBJECT) {
            ArrayList<TypeUsage> types = new ArrayList<TypeUsage>();
            JsWith wObject = (JsWith)jsObject;
            Collection<TypeUsage> withTypes = wObject.getTypes();
            types.addAll(withTypes);
            while (wObject.getOuterWith() != null) {
                wObject = wObject.getOuterWith();
                withTypes = wObject.getTypes();
                types.addAll(withTypes);
            }
            return types;
        }
        return Collections.EMPTY_LIST;
    }

    public static void addUniqueType(Collection<TypeUsage> where, Set<String> forbidden, TypeUsage type) {
        String typeName = type.getType();
        if (forbidden.contains(typeName)) {
            return;
        }
        for (TypeUsage utype : where) {
            if (!utype.getType().equals(typeName)) continue;
            return;
        }
        where.add(type);
    }

    public static void addUniqueType(Collection<TypeUsage> where, TypeUsage type) {
        ModelUtils.addUniqueType(where, Collections.<String>emptySet(), type);
    }

    public static void addUniqueType(Collection<TypeUsage> where, Set<String> forbidden, Collection<TypeUsage> what) {
        for (TypeUsage type : what) {
            ModelUtils.addUniqueType(where, forbidden, type);
        }
    }

    public static void addUniqueType(Collection<TypeUsage> where, Collection<TypeUsage> what) {
        ModelUtils.addUniqueType(where, Collections.<String>emptySet(), what);
    }

    public static void addDocTypesOccurence(JsObject jsObject, JsDocumentationHolder docHolder) {
        if (docHolder.getOccurencesMap().containsKey(jsObject.getName())) {
            for (OffsetRange offsetRange : docHolder.getOccurencesMap().get(jsObject.getName())) {
                ((JsObjectImpl)jsObject).addOccurrence(offsetRange);
            }
        }
    }

    public static String getDisplayName(String typeName) {
        String displayName = typeName;
        if (displayName.startsWith("@param;") || displayName.contains("With$") || displayName.contains("Anonym$")) {
            displayName = "";
        } else {
            if (displayName.contains("_L")) {
                displayName = ModelUtils.removeGeneratedFromFQN(displayName, "_L");
            }
            if (displayName.contains("Anonym$")) {
                displayName = ModelUtils.removeGeneratedFromFQN(displayName, "Anonym$");
            }
        }
        return displayName;
    }

    private static String removeGeneratedFromFQN(String fqn, String generated) {
        String[] parts = fqn.split("\\.");
        String part = parts[parts.length - 1];
        if (part.contains(generated)) {
            try {
                Integer.parseInt(part.substring(generated.length()));
                return "";
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; ++i) {
            part = parts[i];
            boolean add = true;
            if (part.startsWith(generated) || i == 0 && part.contains(generated)) {
                try {
                    Integer.parseInt(part.substring(part.indexOf(generated) + generated.length()));
                    add = false;
                }
                catch (NumberFormatException nfe) {
                    // empty catch block
                }
            }
            if (!add) continue;
            sb.append(part);
            if (i >= parts.length - 1) continue;
            sb.append(".");
        }
        return sb.toString();
    }

    public static boolean isKnownGLobalType(String type) {
        return knownGlobalObjects.contains(type);
    }

    public static List<String> resolveExpressionChain(Snapshot snapshot, int offset, boolean lookBefore) {
        TokenHierarchy th = snapshot.getTokenHierarchy();
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(th, offset);
        if (ts == null) {
            return Collections.emptyList();
        }
        ts.move(offset);
        if (ts.movePrevious() && (ts.moveNext() || ts.offset() + ts.token().length() == snapshot.getText().length())) {
            if (!lookBefore && ts.token().id() != JsTokenId.OPERATOR_DOT) {
                ts.movePrevious();
            }
            Token<? extends JsTokenId> token = lookBefore ? LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.EOL})) : ts.token();
            int parenBalancer = 0;
            int partType = 0;
            boolean wasLastDot = lookBefore;
            int offsetFirstRightParen = -1;
            ArrayList<String> exp = new ArrayList<String>();
            while (token.id() != JsTokenId.OPERATOR_SEMICOLON && token.id() != JsTokenId.BRACKET_RIGHT_CURLY && token.id() != JsTokenId.BRACKET_LEFT_CURLY && token.id() != JsTokenId.BRACKET_LEFT_PAREN && token.id() != JsTokenId.BLOCK_COMMENT && token.id() != JsTokenId.LINE_COMMENT && token.id() != JsTokenId.OPERATOR_ASSIGNMENT && token.id() != JsTokenId.OPERATOR_PLUS) {
                if (token.id() == JsTokenId.WHITESPACE) {
                    int helpOffset = ts.offset();
                    if (ts.movePrevious() && (token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.LINE_COMMENT, JsTokenId.EOL}))).id() != JsTokenId.BRACKET_RIGHT_PAREN && token.id() != JsTokenId.IDENTIFIER && token.id() != JsTokenId.OPERATOR_DOT) {
                        ts.move(helpOffset);
                        ts.moveNext();
                        token = ts.token();
                        break;
                    }
                }
                if (token.id() != JsTokenId.EOL) {
                    if (token.id() != JsTokenId.OPERATOR_DOT) {
                        if (token.id() == JsTokenId.BRACKET_RIGHT_PAREN) {
                            ++parenBalancer;
                            partType = 1;
                            if (offsetFirstRightParen == -1) {
                                offsetFirstRightParen = ts.offset();
                            }
                            while (parenBalancer > 0 && ts.movePrevious()) {
                                token = ts.token();
                                if (token.id() == JsTokenId.BRACKET_RIGHT_PAREN) {
                                    ++parenBalancer;
                                    continue;
                                }
                                if (token.id() != JsTokenId.BRACKET_LEFT_PAREN) continue;
                                --parenBalancer;
                            }
                        } else if (token.id() == JsTokenId.BRACKET_RIGHT_BRACKET) {
                            ++parenBalancer;
                            partType = 2;
                            while (parenBalancer > 0 && ts.movePrevious()) {
                                token = ts.token();
                                if (token.id() == JsTokenId.BRACKET_RIGHT_BRACKET) {
                                    ++parenBalancer;
                                    continue;
                                }
                                if (token.id() != JsTokenId.BRACKET_LEFT_BRACKET) continue;
                                --parenBalancer;
                            }
                        } else {
                            if (parenBalancer == 0 && "operator".equals(((JsTokenId)token.id()).primaryCategory())) {
                                return exp;
                            }
                            exp.add(token.text().toString());
                            switch (partType) {
                                case 0: {
                                    exp.add("@pro");
                                    break;
                                }
                                case 1: {
                                    exp.add("@mtd");
                                    offsetFirstRightParen = -1;
                                    break;
                                }
                                case 2: {
                                    exp.add("@arr");
                                    break;
                                }
                            }
                            partType = 0;
                            wasLastDot = false;
                        }
                    } else {
                        wasLastDot = true;
                    }
                } else if (!wasLastDot && ts.movePrevious() && (token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.LINE_COMMENT}))).id() != JsTokenId.OPERATOR_DOT) break;
                if (!ts.movePrevious()) break;
                token = ts.token();
            }
            if (token.id() == JsTokenId.WHITESPACE) {
                if (ts.movePrevious()) {
                    token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.EOL}));
                    if (token.id() == JsTokenId.KEYWORD_NEW && !exp.isEmpty()) {
                        exp.remove(exp.size() - 1);
                        exp.add("@pro");
                    } else if (!lookBefore && offsetFirstRightParen > -1) {
                        exp.addAll(ModelUtils.resolveExpressionChain(snapshot, offsetFirstRightParen - 1, true));
                    }
                }
            } else if (exp.isEmpty() && !lookBefore && offsetFirstRightParen > -1) {
                exp.addAll(ModelUtils.resolveExpressionChain(snapshot, offsetFirstRightParen - 1, true));
            } else if (wasLastDot && !lookBefore && token.id() == JsTokenId.BRACKET_RIGHT_CURLY) {
                int balancer = 1;
                while (balancer > 0 && ts.movePrevious()) {
                    token = ts.token();
                    if (token.id() == JsTokenId.BRACKET_RIGHT_CURLY) {
                        ++balancer;
                        continue;
                    }
                    if (token.id() != JsTokenId.BRACKET_LEFT_CURLY) continue;
                    --balancer;
                }
                exp.add("@ano:" + ts.offset());
                exp.add("@pro");
            }
            return exp;
        }
        return Collections.emptyList();
    }

    public static void moveProperty(JsObject newParent, JsObject property) {
        JsObject newProperty = newParent.getProperty(property.getName());
        if (property.getParent() != null) {
            property.getParent().getProperties().remove(property.getName());
        }
        if (newProperty == null) {
            ((JsObjectImpl)property).setParent(newParent);
            newParent.addProperty(property.getName(), property);
        } else {
            if (property.isDeclared() && !newProperty.isDeclared()) {
                JsObject tmpProperty = newProperty;
                newParent.addProperty(property.getName(), property);
                ((JsObjectImpl)property).setParent(newParent);
                newProperty = property;
                property = tmpProperty;
            }
            JsObjectImpl.moveOccurrenceOfProperties((JsObjectImpl)newProperty, property);
            for (Occurrence occurrence : property.getOccurrences()) {
                newProperty.addOccurrence(occurrence.getOffsetRange());
            }
            ArrayList<? extends JsObject> propertiesToMove = new ArrayList<JsObject>(property.getProperties().values());
            for (JsObject propOfProperty : propertiesToMove) {
                ModelUtils.moveProperty(newProperty, propOfProperty);
            }
        }
    }

    private static enum State {
        INIT;
        

        private State() {
        }
    }

}

