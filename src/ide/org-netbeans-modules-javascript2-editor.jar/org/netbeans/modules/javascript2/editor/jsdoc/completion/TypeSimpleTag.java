/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HtmlFormatter
 */
package org.netbeans.modules.javascript2.editor.jsdoc.completion;

import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;

public class TypeSimpleTag
extends AnnotationCompletionTag {
    public static final String TEMPLATE = " ${type}";

    public TypeSimpleTag(String name) {
        super(name, name + " ${type}");
    }

    @Override
    public void formatParameters(HtmlFormatter formatter) {
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("type");
        formatter.parameters(false);
        formatter.appendText("");
    }
}

