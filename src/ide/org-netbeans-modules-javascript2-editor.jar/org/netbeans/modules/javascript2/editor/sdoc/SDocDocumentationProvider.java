/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.sdoc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.javascript2.editor.sdoc.SDocAnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.sdoc.SDocDocumentationHolder;
import org.netbeans.modules.javascript2.editor.sdoc.SDocSyntaxProvider;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;
import org.netbeans.modules.parsing.api.Snapshot;

public class SDocDocumentationProvider
implements JsDocumentationProvider {
    private static Set<String> supportedTags;
    private static final List<AnnotationCompletionTagProvider> ANNOTATION_PROVIDERS;
    private static final SyntaxProvider SYNTAX_PROVIDER;

    @Override
    public JsDocumentationHolder createDocumentationHolder(Snapshot snapshot) {
        return new SDocDocumentationHolder(this, snapshot);
    }

    public synchronized Set getSupportedTags() {
        if (supportedTags == null) {
            supportedTags = new HashSet<String>(SDocElementType.values().length);
            for (SDocElementType type : SDocElementType.values()) {
                supportedTags.add(type.toString());
            }
            supportedTags.remove("unknown");
            supportedTags.remove("contextSensitive");
        }
        return supportedTags;
    }

    public List<AnnotationCompletionTagProvider> getAnnotationsProvider() {
        return ANNOTATION_PROVIDERS;
    }

    @Override
    public SyntaxProvider getSyntaxProvider() {
        return SYNTAX_PROVIDER;
    }

    static {
        ANNOTATION_PROVIDERS = Arrays.asList(new SDocAnnotationCompletionTagProvider("SDoc"));
        SYNTAX_PROVIDER = new SDocSyntaxProvider();
    }
}

