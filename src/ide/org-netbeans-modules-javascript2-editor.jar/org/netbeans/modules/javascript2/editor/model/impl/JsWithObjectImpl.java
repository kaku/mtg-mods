/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.JsWith;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;

public class JsWithObjectImpl
extends JsObjectImpl
implements JsWith {
    private final Collection<TypeUsage> withTypes;
    private final JsWith outerWith;
    private final Collection<JsWith> innerWith = new ArrayList<JsWith>();
    private final OffsetRange expressionRange;
    private final Set<JsObject> assignedIn = new HashSet<JsObject>();

    public JsWithObjectImpl(JsObject parent, String name, Collection<TypeUsage> withTypes, OffsetRange offsetRange, OffsetRange expressionRange, JsWith outer, String mimeType, String sourceLabel) {
        super(parent, name, false, offsetRange, EnumSet.of(Modifier.PUBLIC), mimeType, sourceLabel);
        this.withTypes = withTypes;
        this.outerWith = outer;
        if (this.outerWith != null) {
            ((JsWithObjectImpl)this.outerWith).addInnerWith(this);
        }
        this.expressionRange = expressionRange;
    }

    @Override
    public Collection<TypeUsage> getTypes() {
        return this.withTypes;
    }

    protected void addInnerWith(JsWith inner) {
        this.innerWith.add(inner);
    }

    public Collection<JsWith> getInnerWiths() {
        List result = this.innerWith.isEmpty() ? Collections.EMPTY_LIST : new ArrayList<JsWith>(this.innerWith);
        return result;
    }

    @Override
    public JsWith getOuterWith() {
        return this.outerWith;
    }

    @Override
    public void resolveTypes(JsDocumentationHolder jsDocHolder) {
        ArrayList<? extends JsObject> withProperties = new ArrayList<JsObject>(this.getProperties().values());
        for (JsObject withProperty : withProperties) {
            if (!this.resolveWith(this, withProperty)) continue;
            this.properties.remove(withProperty.getName());
        }
    }

    public Collection<JsObject> getObjectWithAssignment() {
        return this.assignedIn;
    }

    public void addObjectWithAssignment(JsObject object) {
        this.assignedIn.add(object);
    }

    private boolean resolveWith(JsWithObjectImpl withObject, JsObject property) {
        JsObject global = ModelUtils.getGlobalObject(withObject.getParent());
        for (TypeUsage typeUsage : withObject.getTypes()) {
            for (TypeUsage rType : ModelUtils.resolveTypeFromSemiType(withObject, typeUsage)) {
                JsObject fromType = ModelUtils.findJsObjectByName(global, rType.getType());
                if (fromType != null) {
                    JsObject propertyFromType = fromType.getProperty(property.getName());
                    if (propertyFromType == null && withObject.getOuterWith() == null) {
                        propertyFromType = global.getProperty(property.getName());
                    }
                    if (propertyFromType != null) {
                        this.moveOccurrenceOfObject((JsObjectImpl)propertyFromType, (JsObjectImpl)property);
                        this.moveFromWith((JsObjectImpl)propertyFromType, (JsObjectImpl)property);
                        return true;
                    }
                    JsWith outer = withObject.getOuterWith();
                    if (outer == null) continue;
                    return this.resolveWith((JsWithObjectImpl)outer, property);
                }
                JsWith outer = withObject.getOuterWith();
                if (outer == null) continue;
                return this.resolveWith((JsWithObjectImpl)outer, property);
            }
        }
        return false;
    }

    @Override
    public JsElement.Kind getJSKind() {
        return JsElement.Kind.WITH_OBJECT;
    }

    @Override
    public int getOffset() {
        return this.getOffsetRange().getStart();
    }

    @Override
    public boolean isAnonymous() {
        return true;
    }

    public OffsetRange getExpressionRange() {
        return this.expressionRange;
    }

    private void moveOccurrenceOfObject(JsObjectImpl original, JsObjectImpl copy) {
        for (Occurrence occurrence : copy.getOccurrences()) {
            original.addOccurrence(occurrence.getOffsetRange());
        }
        copy.clearOccurrences();
    }

    protected void moveFromWith(JsObjectImpl original, JsObjectImpl inWith) {
        if (original.equals(inWith)) {
            return;
        }
        if (!original.isDeclared() && inWith.isDeclared()) {
            this.moveOccurrenceOfObject(inWith, original);
            JsWithObjectImpl.moveOccurrenceOfProperties(inWith, original);
            inWith.setParent(original.getParent());
            original.getParent().addProperty(original.getName(), inWith);
            return;
        }
        Collection<JsObject> prototypeChains = JsWithObjectImpl.findPrototypeChain(original);
        ArrayList<? extends JsObject> propertiesCopy = new ArrayList<JsObject>(inWith.getProperties().values());
        for (JsObject withProperty : propertiesCopy) {
            if (!withProperty.isDeclared()) continue;
            boolean accessible = false;
            for (JsObject jsObject : prototypeChains) {
                JsObject originalProperty = jsObject.getProperty(withProperty.getName());
                if (originalProperty == null) continue;
                accessible = true;
                break;
            }
            if (accessible) continue;
            ((JsObjectImpl)withProperty).setParent(original);
            original.addProperty(withProperty.getName(), withProperty);
            inWith.properties.remove(withProperty.getName());
        }
        for (JsObject jsObject : prototypeChains) {
            for (JsObject origProperty : jsObject.getProperties().values()) {
                JsObjectImpl usedProperty;
                if (!origProperty.getModifiers().contains((Object)Modifier.PUBLIC) && !origProperty.getModifiers().contains((Object)Modifier.PROTECTED) || (usedProperty = (JsObjectImpl)inWith.getProperty(origProperty.getName())) == null) continue;
                ((JsObjectImpl)origProperty).addOccurrence(usedProperty.getDeclarationName().getOffsetRange());
                for (Occurrence occur : usedProperty.getOccurrences()) {
                    ((JsObjectImpl)origProperty).addOccurrence(occur.getOffsetRange());
                }
                usedProperty.clearOccurrences();
                if (origProperty.isDeclared() && usedProperty.isDeclared()) {
                    usedProperty.setDeclared(false);
                }
                this.moveFromWith((JsObjectImpl)origProperty, usedProperty);
            }
            JsObject prototype = jsObject.getProperty("prototype");
            if (prototype == null) continue;
            this.moveFromWith((JsObjectImpl)prototype, inWith);
        }
    }
}

