/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collection;
import java.util.Set;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;

public class JsFunctionReference
extends JsObjectReference
implements JsFunction {
    private final JsFunction original;

    public JsFunctionReference(JsObject parent, Identifier declarationName, JsFunction original, boolean isDeclared, Set<Modifier> modifiers) {
        super(parent, declarationName, original, isDeclared, modifiers);
        this.original = original;
    }

    @Override
    public JsFunction getOriginal() {
        return this.original;
    }

    @Override
    public Collection<? extends JsObject> getParameters() {
        return this.original.getParameters();
    }

    @Override
    public JsObject getParameter(String name) {
        return this.original.getParameter(name);
    }

    @Override
    public void addReturnType(TypeUsage type) {
        this.original.addReturnType(type);
    }

    @Override
    public Collection<? extends TypeUsage> getReturnTypes() {
        return this.original.getReturnTypes();
    }

    @Override
    public Collection<? extends DeclarationScope> getChildrenScopes() {
        return this.original.getChildrenScopes();
    }

    @Override
    public DeclarationScope getParentScope() {
        return this.original.getParentScope();
    }
}

