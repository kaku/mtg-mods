/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.codegen.Compiler
 *  jdk.nashorn.internal.codegen.CompilerConstants
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.parser.Parser
 *  jdk.nashorn.internal.runtime.Context
 *  jdk.nashorn.internal.runtime.ErrorManager
 *  jdk.nashorn.internal.runtime.Source
 *  jdk.nashorn.internal.runtime.options.Options
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.parser;

import jdk.nashorn.internal.codegen.Compiler;
import jdk.nashorn.internal.codegen.CompilerConstants;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.parser.Parser;
import jdk.nashorn.internal.runtime.Context;
import jdk.nashorn.internal.runtime.ErrorManager;
import jdk.nashorn.internal.runtime.Source;
import jdk.nashorn.internal.runtime.options.Options;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.parser.JsErrorManager;
import org.netbeans.modules.javascript2.editor.parser.SanitizingParser;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsParser
extends SanitizingParser {
    public JsParser() {
        super(JsTokenId.javascriptLanguage());
    }

    @Override
    protected String getDefaultScriptName() {
        return "javascript.js";
    }

    @Override
    protected FunctionNode parseSource(Snapshot snapshot, String name, String text, JsErrorManager errorManager) throws Exception {
        String parsableText = text;
        if (parsableText.startsWith("#!")) {
            StringBuilder sb = new StringBuilder(parsableText);
            int index = parsableText.indexOf("\n");
            if (index < 0) {
                index = parsableText.length();
            }
            sb.delete(0, index);
            for (int i = 0; i < index; ++i) {
                sb.insert(i, ' ');
            }
            parsableText = sb.toString();
        }
        Source source = new Source(name, parsableText);
        Options options = new Options("nashorn");
        options.process(new String[]{"--parse-only=true", "--empty-statements=true", "--debug-lines=false"});
        errorManager.setLimit(0);
        Context nashornContext = new Context(options, (ErrorManager)errorManager, JsParser.class.getClassLoader());
        Compiler compiler = Compiler.compiler((Source)source, (Context)nashornContext);
        Parser parser = new Parser(compiler);
        FunctionNode node = parser.parse(CompilerConstants.RUN_SCRIPT.tag());
        return node;
    }

    @Override
    protected String getMimeType() {
        return "text/javascript";
    }
}

