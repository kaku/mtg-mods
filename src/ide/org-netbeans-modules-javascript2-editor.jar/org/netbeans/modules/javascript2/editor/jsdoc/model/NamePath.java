/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

public class NamePath {
    private final String namePath;

    public NamePath(String namePath) {
        this.namePath = namePath;
    }

    public String getNamePath() {
        return this.namePath;
    }

    public String toString() {
        return this.namePath;
    }
}

