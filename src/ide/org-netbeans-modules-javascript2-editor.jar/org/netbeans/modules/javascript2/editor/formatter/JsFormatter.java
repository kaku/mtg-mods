/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.Formatter
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.formatter.CodeStyle;
import org.netbeans.modules.javascript2.editor.formatter.Defaults;
import org.netbeans.modules.javascript2.editor.formatter.FormatContext;
import org.netbeans.modules.javascript2.editor.formatter.FormatToken;
import org.netbeans.modules.javascript2.editor.formatter.FormatTokenStream;
import org.netbeans.modules.javascript2.editor.formatter.FormatVisitor;
import org.netbeans.modules.javascript2.editor.formatter.IndentContext;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.util.Exceptions;

public class JsFormatter
implements Formatter {
    static final Object CT_HANDLER_DOC_PROPERTY = "code-template-insert-handler";
    private static final Logger LOGGER = Logger.getLogger(JsFormatter.class.getName());
    private static final Pattern MOOTOOLS_COMMENT = Pattern.compile("/\\*\n---\\s*\n((.|\n)+)\n\\.\\.\\.\\s*\n\\*/");
    private static final boolean ELSE_IF_SINGLE_LINE = true;
    private final Language<JsTokenId> language;
    private final Defaults.Provider provider;
    private int lastOffsetDiff = 0;
    private final Set<FormatToken> processed = new HashSet<FormatToken>();

    public JsFormatter(Language<JsTokenId> language) {
        this.language = language;
        this.provider = Defaults.getInstance(language.mimeType());
    }

    public int hangingIndentSize() {
        return CodeStyle.get((Document)null, this.provider).getContinuationIndentSize();
    }

    public int indentSize() {
        return CodeStyle.get((Document)null, this.provider).getIndentSize();
    }

    public boolean needsParserResult() {
        return true;
    }

    public void reformat(final Context context, final ParserResult compilationInfo) {
        this.processed.clear();
        this.lastOffsetDiff = 0;
        final BaseDocument doc = (BaseDocument)context.document();
        final boolean templateEdit = doc.getProperty(CT_HANDLER_DOC_PROPERTY) != null;
        doc.runAtomic(new Runnable(){

            @Override
            public void run() {
                long startTime = System.nanoTime();
                FormatContext formatContext = new FormatContext(context, JsFormatter.this.provider, compilationInfo.getSnapshot(), JsFormatter.this.language);
                CodeStyle.Holder codeStyle = CodeStyle.get(formatContext).toHolder();
                TokenSequence ts = LexUtilities.getTokenSequence(compilationInfo.getSnapshot().getTokenHierarchy(), context.startOffset(), JsFormatter.this.language);
                if (ts == null) {
                    return;
                }
                FormatTokenStream tokenStream = FormatTokenStream.create(ts, 0, context.endOffset());
                LOGGER.log(Level.FINE, "Format token stream creation: {0} ms", (System.nanoTime() - startTime) / 1000000);
                startTime = System.nanoTime();
                FormatVisitor visitor = new FormatVisitor(tokenStream, ts, context.endOffset());
                FunctionNode root = ((JsParserResult)compilationInfo).getRoot();
                if (root != null) {
                    root.accept((NodeVisitor)visitor);
                } else {
                    LOGGER.log(Level.FINE, "Format visitor not executed; no root node");
                }
                LOGGER.log(Level.FINE, "Format visitor: {0} ms", (System.nanoTime() - startTime) / 1000000);
                startTime = System.nanoTime();
                int indentLevelSize = codeStyle.indentSize;
                int initialIndent = codeStyle.initialIndent;
                int continuationIndent = codeStyle.continuationIndentSize;
                List<FormatToken> tokens = tokenStream.getTokens();
                if (LOGGER.isLoggable(Level.FINE)) {
                    for (FormatToken token : tokens) {
                        LOGGER.log(Level.FINE, token.toString());
                    }
                }
                boolean started = false;
                boolean firstTokenFound = false;
                Stack continuations = new Stack();
                for (int i = 0; i < tokens.size(); ++i) {
                    int j;
                    FormatToken next;
                    FormatToken token = tokens.get(i);
                    if (!started && !token.isVirtual() && token.getOffset() >= context.startOffset()) {
                        started = true;
                    }
                    if (!codeStyle.expandTabsToSpaces && !token.isVirtual()) {
                        if (token.getId() == JsTokenId.WHITESPACE) {
                            CharSequence text = token.getText();
                            for (j = 0; j < text.length(); ++j) {
                                if (text.charAt(j) != '\t') continue;
                                formatContext.incTabCount();
                            }
                        } else if (token.getKind() == FormatToken.Kind.EOL) {
                            formatContext.resetTabCount();
                        }
                    }
                    if (JsFormatter.this.processed.remove(token)) continue;
                    if (!token.isVirtual()) {
                        boolean change = false;
                        if (formatContext.isPendingContinuation() && token.getKind() != FormatToken.Kind.EOL) {
                            change = JsFormatter.this.updateContinuationStart(formatContext, token, continuations, false);
                        }
                        if (!change) {
                            JsFormatter.this.updateContinuationEnd(formatContext, token, continuations);
                        }
                        if (!firstTokenFound) {
                            firstTokenFound = true;
                            formatContext.setCurrentLineStart(token.getOffset());
                        }
                        if (token.getKind() != FormatToken.Kind.WHITESPACE && token.getKind() != FormatToken.Kind.EOL) {
                            JsFormatter.this.lastOffsetDiff = formatContext.getOffsetDiff();
                        }
                        initialIndent = formatContext.getEmbeddingIndent(tokenStream, token) + codeStyle.initialIndent;
                    }
                    if (started && (token.getKind() == FormatToken.Kind.BLOCK_COMMENT || token.getKind() == FormatToken.Kind.DOC_COMMENT || token.getKind() == FormatToken.Kind.LINE_COMMENT)) {
                        try {
                            int indent = context.lineIndent(context.lineStartOffset(formatContext.getDocumentOffset(token.getOffset()) + formatContext.getOffsetDiff()));
                            JsFormatter.this.formatComment(token, formatContext, codeStyle, indent);
                        }
                        catch (BadLocationException ex) {
                            LOGGER.log(Level.INFO, null, ex);
                        }
                        continue;
                    }
                    if (started && token.getKind().isSpaceMarker()) {
                        JsFormatter.this.formatSpace(tokens, i, formatContext, codeStyle);
                        continue;
                    }
                    if (started && token.getKind().isLineWrapMarker()) {
                        JsFormatter.this.formatLineWrap(tokens, i, formatContext, codeStyle, initialIndent, continuationIndent, continuations);
                        continue;
                    }
                    if (token.getKind().isIndentationMarker()) {
                        JsFormatter.updateIndentationLevel(token, formatContext);
                        continue;
                    }
                    if (token.getKind() != FormatToken.Kind.SOURCE_START && token.getKind() != FormatToken.Kind.EOL) continue;
                    if (started && token.getKind() != FormatToken.Kind.SOURCE_START) {
                        FormatContext.LineWrap lastWrap;
                        int segmentLength;
                        FormatToken tokenBeforeEol = null;
                        for (j = i - 1; j >= 0 && (tokenBeforeEol = tokens.get(j)).isVirtual(); --j) {
                        }
                        if (tokenBeforeEol.getKind() != FormatToken.Kind.SOURCE_START && (segmentLength = tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length() - formatContext.getCurrentLineStart() + JsFormatter.this.lastOffsetDiff) >= codeStyle.rightMargin && (lastWrap = formatContext.getLastLineWrap()) != null) {
                            JsFormatter.this.wrapLine(formatContext, codeStyle, lastWrap, initialIndent, continuationIndent, continuations);
                        }
                    }
                    if (started) {
                        JsFormatter.removeTrailingSpaces(tokens, i, formatContext, token, templateEdit);
                    }
                    if (token.getKind() != FormatToken.Kind.SOURCE_START) {
                        formatContext.setCurrentLineStart(token.getOffset() + 1 + formatContext.getOffsetDiff());
                        formatContext.setLastLineWrap(null);
                    }
                    if ((next = FormatTokenStream.getNextNonVirtual(token)) != null && next.getKind() == FormatToken.Kind.LINE_COMMENT) continue;
                    FormatToken indentationStart = null;
                    FormatToken indentationEnd = null;
                    for (int j2 = i + 1; j2 < tokens.size(); ++j2) {
                        FormatToken nextToken = tokens.get(j2);
                        if (!nextToken.isVirtual()) {
                            if (nextToken.getOffset() >= context.startOffset()) {
                                started = true;
                            }
                            if (nextToken.getKind() != FormatToken.Kind.WHITESPACE) {
                                indentationEnd = nextToken;
                                if (indentationStart != null) break;
                                indentationStart = nextToken;
                                break;
                            }
                            if (indentationStart == null) {
                                indentationStart = nextToken;
                            }
                        } else {
                            JsFormatter.updateIndentationLevel(nextToken, formatContext);
                        }
                        JsFormatter.this.processed.add(nextToken);
                    }
                    if (indentationEnd == null || indentationEnd.getKind() == FormatToken.Kind.EOL && !templateEdit) continue;
                    int indentationSize = initialIndent + formatContext.getIndentationLevel() * indentLevelSize;
                    int continuationLevel = formatContext.getContinuationLevel();
                    if (JsFormatter.isContinuation(formatContext, token, false)) {
                        ++continuationLevel;
                        JsFormatter.this.updateContinuationStart(formatContext, token, continuations, true);
                    } else {
                        formatContext.setPendingContinuation(false);
                    }
                    indentationSize += continuationIndent * continuationLevel;
                    if (!started) continue;
                    formatContext.indentLine(indentationStart.getOffset(), indentationSize, JsFormatter.this.checkIndentation(doc, token, indentationEnd, formatContext, context, indentationSize), codeStyle);
                }
                LOGGER.log(Level.FINE, "Formatting changes: {0} ms", (System.nanoTime() - startTime) / 1000000);
            }
        });
    }

    private static void removeTrailingSpaces(List<FormatToken> tokens, int index, FormatContext formatContext, FormatToken limit, boolean templateEdit) {
        FormatToken nextToken;
        if (templateEdit && limit.getKind() == FormatToken.Kind.EOL) {
            return;
        }
        FormatToken start = null;
        for (int j = index - 1; j >= 0 && ((nextToken = tokens.get(j)).isVirtual() || nextToken.getKind() == FormatToken.Kind.WHITESPACE); --j) {
            start = tokens.get(j);
        }
        while (start != null && start != limit) {
            if (!start.isVirtual()) {
                formatContext.remove(start.getOffset(), start.getText().length());
            }
            start = start.next();
        }
    }

    private void wrapLine(FormatContext formatContext, CodeStyle.Holder codeStyle, FormatContext.LineWrap lastWrap, int initialIndent, int continuationIndent, Stack<FormatContext.ContinuationBlock> continuations) {
        formatContext.insertWithOffsetDiff(lastWrap.getToken().getOffset() + lastWrap.getToken().getText().length(), "\n", lastWrap.getOffsetDiff());
        formatContext.setCurrentLineStart(lastWrap.getToken().getOffset() + lastWrap.getToken().getText().length() + 1 + lastWrap.getOffsetDiff());
        int indentationSize = initialIndent + lastWrap.getIndentationLevel() * IndentUtils.indentLevelSize((Document)formatContext.getDocument());
        int continuationLevel = formatContext.getContinuationLevel();
        if (JsFormatter.isContinuation(formatContext, lastWrap.getToken(), true)) {
            ++continuationLevel;
            this.updateContinuationStart(formatContext, lastWrap.getToken(), continuations, true);
        } else {
            formatContext.setPendingContinuation(false);
        }
        formatContext.indentLineWithOffsetDiff(lastWrap.getToken().getOffset() + lastWrap.getToken().getText().length() + 1, indentationSize += continuationIndent * continuationLevel, Indentation.ALLOWED, lastWrap.getOffsetDiff(), codeStyle);
        formatContext.resetTabCount();
    }

    private void formatLineWrap(List<FormatToken> tokens, int index, FormatContext formatContext, CodeStyle.Holder codeStyle, int initialIndent, int continuationIndent, Stack<FormatContext.ContinuationBlock> continuations) {
        FormatContext.LineWrap lastWrap;
        FormatToken token = tokens.get(index);
        CodeStyle.WrapStyle style = JsFormatter.getLineWrap(token, formatContext, codeStyle);
        if (style == null) {
            return;
        }
        FormatToken tokenAfterEol = token.next();
        int startIndex = index;
        while (tokenAfterEol != null && tokenAfterEol.getKind() != FormatToken.Kind.EOL && tokenAfterEol.getKind() != FormatToken.Kind.TEXT) {
            tokenAfterEol = tokenAfterEol.next();
            ++startIndex;
        }
        FormatToken tokenBeforeEol = null;
        for (int j = startIndex - 1; j >= 0 && ((tokenBeforeEol = tokens.get(j)).isVirtual() || tokenBeforeEol.getKind() == FormatToken.Kind.WHITESPACE); --j) {
        }
        assert (tokenBeforeEol.getKind() != FormatToken.Kind.WHITESPACE && tokenBeforeEol.getKind() != FormatToken.Kind.EOL);
        if (style == CodeStyle.WrapStyle.WRAP_IF_LONG) {
            int segmentLength = tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length() - formatContext.getCurrentLineStart() + this.lastOffsetDiff;
            int tabCount = formatContext.getTabCount();
            if (!codeStyle.expandTabsToSpaces) {
                segmentLength += tabCount * codeStyle.tabSize - tabCount;
            }
            if (segmentLength >= codeStyle.rightMargin) {
                lastWrap = formatContext.getLastLineWrap();
                if (lastWrap != null && (tokenAfterEol.getKind() != FormatToken.Kind.EOL || segmentLength > codeStyle.rightMargin)) {
                    int offsetBeforeChanges = formatContext.getOffsetDiff();
                    this.wrapLine(formatContext, codeStyle, lastWrap, initialIndent, continuationIndent, continuations);
                    formatContext.setLastLineWrap(new FormatContext.LineWrap(tokenBeforeEol, this.lastOffsetDiff + (formatContext.getOffsetDiff() - offsetBeforeChanges), formatContext.getIndentationLevel(), formatContext.getContinuationLevel()));
                    return;
                }
            } else {
                formatContext.setLastLineWrap(new FormatContext.LineWrap(tokenBeforeEol, this.lastOffsetDiff, formatContext.getIndentationLevel(), formatContext.getContinuationLevel()));
                return;
            }
        }
        FormatToken extendedTokenAfterEol = tokenAfterEol;
        for (FormatToken current = tokenAfterEol; current != null && (current.getKind() == FormatToken.Kind.EOL || current.getKind() == FormatToken.Kind.WHITESPACE || current.isVirtual()); current = current.next()) {
            if (current == tokenAfterEol || current.getKind() != FormatToken.Kind.EOL) continue;
            extendedTokenAfterEol = current;
        }
        if (tokenAfterEol != null && (tokenAfterEol.getKind() != FormatToken.Kind.EOL || extendedTokenAfterEol != tokenAfterEol && !JsFormatter.isStatementWrap(token))) {
            this.moveForward(token, extendedTokenAfterEol, formatContext, true);
            if (style != CodeStyle.WrapStyle.WRAP_NEVER) {
                if (tokenAfterEol.getKind() != FormatToken.Kind.EOL) {
                    int segmentLength = tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length() - formatContext.getCurrentLineStart() + this.lastOffsetDiff;
                    if (segmentLength >= codeStyle.rightMargin && (lastWrap = formatContext.getLastLineWrap()) != null && tokenAfterEol.getKind() != FormatToken.Kind.EOL) {
                        this.wrapLine(formatContext, codeStyle, lastWrap, initialIndent, continuationIndent, continuations);
                    }
                    formatContext.insert(tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length(), "\n");
                    formatContext.setCurrentLineStart(tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length() + 1);
                    formatContext.setLastLineWrap(null);
                    int indentationSize = initialIndent + formatContext.getIndentationLevel() * IndentUtils.indentLevelSize((Document)formatContext.getDocument());
                    int continuationLevel = formatContext.getContinuationLevel();
                    if (JsFormatter.isContinuation(formatContext, tokenBeforeEol, true)) {
                        ++continuationLevel;
                        this.updateContinuationStart(formatContext, tokenBeforeEol, continuations, true);
                    } else {
                        formatContext.setPendingContinuation(false);
                    }
                    formatContext.indentLine(tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length(), indentationSize += continuationIndent * continuationLevel, Indentation.ALLOWED, codeStyle);
                }
                if (extendedTokenAfterEol != tokenAfterEol) {
                    if (extendedTokenAfterEol != null) {
                        formatContext.remove(tokenAfterEol.getOffset(), extendedTokenAfterEol.getOffset() - tokenAfterEol.getOffset());
                        this.processed.remove(extendedTokenAfterEol.previous());
                    } else {
                        FormatToken last;
                        for (last = tokens.get((int)(tokens.size() - 1)); last != null && last.isVirtual(); last = last.previous()) {
                        }
                        if (last != null) {
                            formatContext.remove(tokenAfterEol.getOffset(), last.getOffset() + last.getText().length() - tokenAfterEol.getOffset());
                        }
                    }
                }
            } else {
                int start = tokenBeforeEol.getOffset() + tokenBeforeEol.getText().length();
                FormatToken endToken = extendedTokenAfterEol;
                if (endToken == null) {
                    for (endToken = tokens.get((int)(tokens.size() - 1)); endToken != null && endToken.isVirtual(); endToken = endToken.previous()) {
                    }
                    if (endToken != null) {
                        formatContext.remove(start, endToken.getOffset() + endToken.getText().length() - start);
                    }
                } else if (endToken.getKind() != FormatToken.Kind.EOL) {
                    FormatToken spaceStartToken = tokenBeforeEol.next();
                    if (spaceStartToken == null) {
                        spaceStartToken = tokenBeforeEol;
                    }
                    if (JsFormatter.isSpace(spaceStartToken, formatContext, codeStyle, true, true)) {
                        formatContext.replace(start, endToken.getOffset() - start, " ");
                    } else {
                        formatContext.remove(start, endToken.getOffset() - start);
                    }
                } else if (tokenAfterEol != endToken) {
                    formatContext.remove(start, endToken.getOffset() - start);
                }
            }
        }
    }

    private void formatSpace(List<FormatToken> tokens, int index, FormatContext formatContext, CodeStyle.Holder codeStyle) {
        FormatToken current;
        FormatToken token = tokens.get(index);
        assert (token.isVirtual());
        CodeStyle.WrapStyle style = JsFormatter.getLineWrap(tokens, index, formatContext, codeStyle, true);
        if (style == CodeStyle.WrapStyle.WRAP_ALWAYS) {
            return;
        }
        FormatToken lastEol = null;
        FormatToken start = null;
        for (FormatToken current2 = token.previous(); current2 != null; current2 = current2.previous()) {
            if (current2.isVirtual()) continue;
            if (current2.getKind() != FormatToken.Kind.WHITESPACE && current2.getKind() != FormatToken.Kind.EOL) {
                start = current2;
                break;
            }
            if (lastEol != null || current2.getKind() != FormatToken.Kind.EOL) continue;
            lastEol = current2;
        }
        FormatToken end = null;
        for (current = token.next(); current != null; current = current.next()) {
            if (current.isVirtual()) continue;
            if (current.getKind() != FormatToken.Kind.WHITESPACE && current.getKind() != FormatToken.Kind.EOL) {
                end = current;
                break;
            }
            if (current.getKind() != FormatToken.Kind.EOL) continue;
            lastEol = current;
        }
        for (current = start; current != null && current != end; current = current.next()) {
            if (!current.isVirtual() || current.getKind().isIndentationMarker() || JsFormatter.getLineWrap(current, formatContext, codeStyle) == CodeStyle.WrapStyle.WRAP_IF_LONG) continue;
            this.processed.add(current);
        }
        if (start != null && end != null) {
            boolean remove = !JsFormatter.isSpace(token, formatContext, codeStyle, true, false);
            if ((start = FormatTokenStream.getNextNonVirtual(start)) != null) {
                if (start.getKind() != FormatToken.Kind.WHITESPACE && start.getKind() != FormatToken.Kind.EOL) {
                    assert (start == end);
                    if (!remove) {
                        formatContext.insert(start.getOffset(), " ");
                    }
                } else {
                    if (lastEol != null) {
                        end = lastEol;
                    }
                    if (remove || end.getKind() == FormatToken.Kind.EOL) {
                        formatContext.remove(start.getOffset(), end.getOffset() - start.getOffset());
                    } else {
                        formatContext.replace(start.getOffset(), end.getOffset() - start.getOffset(), " ");
                    }
                }
            }
        }
    }

    private void formatComment(FormatToken comment, FormatContext formatContext, CodeStyle.Holder codeStyle, int indent) {
        assert (comment.getKind() == FormatToken.Kind.BLOCK_COMMENT || comment.getKind() == FormatToken.Kind.DOC_COMMENT || comment.getKind() == FormatToken.Kind.LINE_COMMENT);
        if (comment.getKind() == FormatToken.Kind.LINE_COMMENT) {
            return;
        }
        String text = comment.getText().toString();
        if (!text.contains("\n")) {
            return;
        }
        if (comment.getKind() == FormatToken.Kind.BLOCK_COMMENT && MOOTOOLS_COMMENT.matcher(text).matches()) {
            return;
        }
        for (int i = 0; i < text.length(); ++i) {
            char single = text.charAt(i);
            if (single != '\n') continue;
            formatContext.indentLine(comment.getOffset() + i + 1, indent + 1, Indentation.ALLOWED, codeStyle);
        }
    }

    private boolean updateContinuationStart(FormatContext formatContext, FormatToken token, Stack<FormatContext.ContinuationBlock> continuations, boolean continuation) {
        FormatToken nextImportant;
        boolean change = false;
        FormatToken formatToken = nextImportant = continuation ? FormatTokenStream.getNextImportant(token) : token;
        if (nextImportant != null && nextImportant.getKind() == FormatToken.Kind.TEXT) {
            if (JsTokenId.BRACKET_LEFT_CURLY == nextImportant.getId()) {
                continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.CURLY, true));
                formatContext.incContinuationLevel();
                formatContext.setPendingContinuation(false);
                this.processed.add(nextImportant);
                change = true;
            } else if (JsTokenId.BRACKET_LEFT_BRACKET == nextImportant.getId()) {
                continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.BRACKET, true));
                formatContext.incContinuationLevel();
                formatContext.setPendingContinuation(false);
                this.processed.add(nextImportant);
                change = true;
            } else if (JsTokenId.BRACKET_LEFT_PAREN == nextImportant.getId()) {
                continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.PAREN, true));
                formatContext.incContinuationLevel();
                formatContext.setPendingContinuation(false);
                this.processed.add(nextImportant);
                change = true;
            } else if (JsTokenId.KEYWORD_FUNCTION == nextImportant.getId()) {
                FormatToken curly;
                for (curly = nextImportant; curly != null; curly = curly.next()) {
                    if (curly.isVirtual()) continue;
                    if (JsTokenId.BRACKET_RIGHT_CURLY == curly.getId()) {
                        curly = null;
                        break;
                    }
                    if (JsTokenId.BRACKET_LEFT_CURLY == curly.getId()) break;
                }
                if (curly != null) {
                    continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.CURLY, true));
                    formatContext.incContinuationLevel();
                    formatContext.setPendingContinuation(false);
                    this.processed.add(curly);
                    change = true;
                }
            } else if (continuation) {
                formatContext.setPendingContinuation(true);
            }
        }
        return change;
    }

    private void updateContinuationEnd(FormatContext formatContext, FormatToken token, Stack<FormatContext.ContinuationBlock> continuations) {
        FormatContext.ContinuationBlock block;
        if (token.isVirtual() || token.getKind() != FormatToken.Kind.TEXT) {
            return;
        }
        if (formatContext.isPendingContinuation() && (JsTokenId.BRACKET_RIGHT_CURLY == token.getId() || JsTokenId.BRACKET_RIGHT_BRACKET == token.getId() || JsTokenId.BRACKET_RIGHT_PAREN == token.getId())) {
            formatContext.setPendingContinuation(false);
        }
        if (continuations.isEmpty()) {
            return;
        }
        if (JsTokenId.BRACKET_LEFT_CURLY == token.getId()) {
            continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.CURLY, false));
        } else if (JsTokenId.BRACKET_LEFT_BRACKET == token.getId()) {
            continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.BRACKET, false));
        } else if (JsTokenId.BRACKET_LEFT_PAREN == token.getId()) {
            continuations.push(new FormatContext.ContinuationBlock(FormatContext.ContinuationBlock.Type.PAREN, false));
        } else if (JsTokenId.BRACKET_RIGHT_CURLY == token.getId()) {
            FormatContext.ContinuationBlock block2 = continuations.peek();
            if (block2.getType() == FormatContext.ContinuationBlock.Type.CURLY) {
                continuations.pop();
                if (block2.isChange()) {
                    formatContext.decContinuationLevel();
                }
            }
        } else if (JsTokenId.BRACKET_RIGHT_BRACKET == token.getId()) {
            FormatContext.ContinuationBlock block3 = continuations.peek();
            if (block3.getType() == FormatContext.ContinuationBlock.Type.BRACKET) {
                continuations.pop();
                if (block3.isChange()) {
                    formatContext.decContinuationLevel();
                }
            }
        } else if (JsTokenId.BRACKET_RIGHT_PAREN == token.getId() && (block = continuations.peek()).getType() == FormatContext.ContinuationBlock.Type.PAREN) {
            continuations.pop();
            if (block.isChange()) {
                formatContext.decContinuationLevel();
            }
        }
    }

    private static boolean isContinuation(FormatContext formatContext, FormatToken token, boolean noRealEol) {
        FormatToken previous;
        FormatToken next;
        assert (noRealEol || token.getKind() == FormatToken.Kind.SOURCE_START || token.getKind() == FormatToken.Kind.EOL);
        if (token.getKind() == FormatToken.Kind.SOURCE_START) {
            return false;
        }
        for (FormatToken current = next = token.next(); current != null && current.isVirtual(); current = current.next()) {
            if (current.getKind() != FormatToken.Kind.AFTER_STATEMENT && current.getKind() != FormatToken.Kind.AFTER_PROPERTY && current.getKind() != FormatToken.Kind.AFTER_ARRAY_LITERAL_ITEM && current.getKind() != FormatToken.Kind.AFTER_CASE && !current.getKind().isIndentationMarker()) continue;
            return false;
        }
        FormatToken nonVirtualNext = FormatTokenStream.getNextNonVirtual(next);
        if (nonVirtualNext != null) {
            if (JsTokenId.BRACKET_LEFT_CURLY == nonVirtualNext.getId()) {
                FormatToken previous2 = nonVirtualNext.previous();
                if (previous2 == null || previous2.getKind() != FormatToken.Kind.BEFORE_OBJECT) {
                    return false;
                }
            } else {
                if (JsTokenId.BRACKET_RIGHT_CURLY == nonVirtualNext.getId() || JsTokenId.BRACKET_RIGHT_BRACKET == nonVirtualNext.getId()) {
                    return false;
                }
                if (JsTokenId.OPERATOR_COMMA == nonVirtualNext.getId()) {
                    for (FormatToken virtualNext = nonVirtualNext.next(); virtualNext != null && virtualNext.isVirtual(); virtualNext = virtualNext.next()) {
                        if (virtualNext.getKind() != FormatToken.Kind.AFTER_PROPERTY) continue;
                        return false;
                    }
                }
            }
        }
        FormatToken result = null;
        FormatToken formatToken = previous = noRealEol ? token : token.previous();
        while (previous != null) {
            FormatToken.Kind kind = previous.getKind();
            if (kind == FormatToken.Kind.SOURCE_START || kind == FormatToken.Kind.TEXT || kind == FormatToken.Kind.AFTER_STATEMENT || kind == FormatToken.Kind.AFTER_PROPERTY || kind == FormatToken.Kind.AFTER_ARRAY_LITERAL_ITEM || kind == FormatToken.Kind.AFTER_CASE || kind.isIndentationMarker()) {
                result = previous;
                break;
            }
            previous = previous.previous();
        }
        if (result == null || result.getKind() == FormatToken.Kind.SOURCE_START || result.getKind() == FormatToken.Kind.AFTER_STATEMENT || result.getKind() == FormatToken.Kind.AFTER_PROPERTY || result.getKind() == FormatToken.Kind.AFTER_ARRAY_LITERAL_ITEM || result.getKind() == FormatToken.Kind.AFTER_CASE || result.getKind().isIndentationMarker()) {
            return false;
        }
        return JsTokenId.BRACKET_LEFT_CURLY != result.getId() && JsTokenId.BRACKET_RIGHT_CURLY != result.getId() && !formatContext.isGenerated(result);
    }

    private Indentation checkIndentation(BaseDocument doc, FormatToken token, FormatToken indentationEnd, FormatContext formatContext, Context context, int indentationSize) {
        assert (indentationEnd != null && !indentationEnd.isVirtual());
        assert (token.getKind() == FormatToken.Kind.EOL || token.getKind() == FormatToken.Kind.SOURCE_START);
        if (token.getKind() != FormatToken.Kind.SOURCE_START && formatContext.getDocumentOffset(token.getOffset()) >= 0 || context.startOffset() <= 0 && !formatContext.isEmbedded()) {
            if (formatContext.isGenerated(indentationEnd)) {
                return Indentation.FORBIDDEN;
            }
            return Indentation.ALLOWED;
        }
        if (formatContext.isEmbedded()) {
            return Indentation.FORBIDDEN;
        }
        try {
            int currentIndentation;
            int lineStartOffset = IndentUtils.lineStartOffset((Document)doc, (int)context.startOffset());
            if (JsFormatter.isWhitespace(doc.getText(lineStartOffset, context.startOffset() - lineStartOffset)) && (currentIndentation = IndentUtils.lineIndent((Document)doc, (int)lineStartOffset)) != indentationSize && lineStartOffset + indentationSize >= context.startOffset()) {
                return new Indentation(true, true);
            }
        }
        catch (BadLocationException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
        return Indentation.FORBIDDEN;
    }

    private static void updateIndentationLevel(FormatToken token, FormatContext formatContext) {
        switch (token.getKind()) {
            case ELSE_IF_INDENTATION_INC: {
                break;
            }
            case INDENTATION_INC: {
                formatContext.incIndentationLevel();
                break;
            }
            case ELSE_IF_INDENTATION_DEC: {
                break;
            }
            case INDENTATION_DEC: {
                formatContext.decIndentationLevel();
                break;
            }
        }
    }

    private static boolean isStatementWrap(FormatToken token) {
        return token.getKind() == FormatToken.Kind.AFTER_STATEMENT || token.getKind() == FormatToken.Kind.AFTER_BLOCK_START || token.getKind() == FormatToken.Kind.AFTER_CASE || token.getKind() == FormatToken.Kind.ELSE_IF_AFTER_BLOCK_START;
    }

    private static CodeStyle.WrapStyle getLineWrap(List<FormatToken> tokens, int index, FormatContext context, CodeStyle.Holder codeStyle, boolean skipWitespace) {
        FormatToken token = tokens.get(index);
        assert (token.isVirtual());
        for (FormatToken next = token; next != null && (next.isVirtual() || skipWitespace && next.getKind() == FormatToken.Kind.WHITESPACE); next = next.next()) {
            CodeStyle.WrapStyle style = JsFormatter.getLineWrap(next, context, codeStyle);
            if (style == null) continue;
            return style;
        }
        return null;
    }

    private static CodeStyle.WrapStyle getLineWrap(FormatToken token, FormatContext context, CodeStyle.Holder codeStyle) {
        switch (token.getKind()) {
            case AFTER_STATEMENT: {
                FormatToken check = token.previous();
                if (check != null && check.getKind() == FormatToken.Kind.BEFORE_DOT) {
                    return null;
                }
                check = token.next();
                if (check != null && check.getKind() == FormatToken.Kind.BEFORE_DOT) {
                    return null;
                }
                return codeStyle.wrapStatement;
            }
            case AFTER_BLOCK_START: {
                return CodeStyle.WrapStyle.WRAP_ALWAYS;
            }
            case AFTER_CASE: {
                return CodeStyle.WrapStyle.WRAP_ALWAYS;
            }
            case ELSE_IF_AFTER_BLOCK_START: {
                return CodeStyle.WrapStyle.WRAP_NEVER;
            }
            case AFTER_VAR_DECLARATION: {
                return codeStyle.wrapVariables;
            }
            case BEFORE_FUNCTION_DECLARATION_PARAMETER: {
                return codeStyle.wrapMethodParams;
            }
            case BEFORE_FUNCTION_CALL_ARGUMENT: {
                return codeStyle.wrapMethodCallArgs;
            }
            case AFTER_IF_START: {
                return codeStyle.wrapIfStatement;
            }
            case AFTER_ELSE_START: {
                return codeStyle.wrapIfStatement;
            }
            case AFTER_WHILE_START: {
                return codeStyle.wrapWhileStatement;
            }
            case AFTER_DO_START: {
                return codeStyle.wrapDoWhileStatement;
            }
            case AFTER_FOR_START: {
                return codeStyle.wrapForStatement;
            }
            case AFTER_WITH_START: {
                return codeStyle.wrapWithStatement;
            }
            case BEFORE_FOR_TEST: 
            case BEFORE_FOR_MODIFY: {
                return codeStyle.wrapFor;
            }
            case BEFORE_CHAIN_CALL_DOT: {
                if (codeStyle.wrapAfterDotInChainedMethodCalls) {
                    return null;
                }
                return codeStyle.wrapChainedMethodCalls;
            }
            case AFTER_CHAIN_CALL_DOT: {
                if (codeStyle.wrapAfterDotInChainedMethodCalls) {
                    return codeStyle.wrapChainedMethodCalls;
                }
                return null;
            }
            case AFTER_BINARY_OPERATOR_WRAP: {
                if (codeStyle.wrapAfterBinaryOps) {
                    return codeStyle.wrapBinaryOps;
                }
                return null;
            }
            case BEFORE_BINARY_OPERATOR_WRAP: {
                if (codeStyle.wrapAfterBinaryOps) {
                    return null;
                }
                return codeStyle.wrapBinaryOps;
            }
            case AFTER_ASSIGNMENT_OPERATOR_WRAP: {
                return codeStyle.wrapAssignOps;
            }
            case AFTER_TERNARY_OPERATOR_WRAP: {
                if (codeStyle.wrapAfterTernaryOps) {
                    return codeStyle.wrapTernaryOps;
                }
                return null;
            }
            case BEFORE_TERNARY_OPERATOR_WRAP: {
                if (codeStyle.wrapAfterTernaryOps) {
                    return null;
                }
                return codeStyle.wrapTernaryOps;
            }
            case AFTER_OBJECT_START: 
            case BEFORE_OBJECT_END: {
                if (JsFormatter.isEmptyObject(token)) {
                    return null;
                }
                return codeStyle.wrapObjects;
            }
            case AFTER_PROPERTY: {
                return codeStyle.wrapProperties;
            }
            case AFTER_ARRAY_LITERAL_START: 
            case BEFORE_ARRAY_LITERAL_END: {
                return codeStyle.wrapArrayInit;
            }
            case AFTER_ARRAY_LITERAL_ITEM: {
                return codeStyle.wrapArrayInitItems;
            }
        }
        return null;
    }

    private static boolean isSpace(FormatToken token, FormatContext context, CodeStyle.Holder codeStyle, boolean skipWitespace, boolean skipEol) {
        if (!(token.isVirtual() || skipWitespace && token.getKind() == FormatToken.Kind.WHITESPACE || skipEol && token.getKind() == FormatToken.Kind.EOL)) {
            return false;
        }
        boolean hasSpaceMarker = false;
        boolean hasSpace = false;
        for (FormatToken next = token; next != null && (next.isVirtual() || skipWitespace && next.getKind() == FormatToken.Kind.WHITESPACE || skipWitespace && next.getKind() == FormatToken.Kind.EOL); next = next.next()) {
            if (next.getKind() != FormatToken.Kind.WHITESPACE && next.getKind() != FormatToken.Kind.EOL) {
                if (JsFormatter.isSpace(next, context, codeStyle)) {
                    return true;
                }
                if (!next.getKind().isSpaceMarker()) continue;
                hasSpaceMarker = true;
                continue;
            }
            hasSpace = true;
        }
        return !hasSpaceMarker && hasSpace;
    }

    private static boolean isSpace(FormatToken token, FormatContext formatContext, CodeStyle.Holder codeStyle) {
        switch (token.getKind()) {
            case BEFORE_ASSIGNMENT_OPERATOR: {
                return codeStyle.spaceAroundAssignOps;
            }
            case AFTER_ASSIGNMENT_OPERATOR: {
                return codeStyle.spaceAroundAssignOps;
            }
            case BEFORE_PROPERTY_OPERATOR: {
                return codeStyle.spaceBeforeColon;
            }
            case AFTER_PROPERTY_OPERATOR: {
                return codeStyle.spaceAfterColon;
            }
            case BEFORE_BINARY_OPERATOR: {
                return codeStyle.spaceAroundBinaryOps;
            }
            case AFTER_BINARY_OPERATOR: {
                return codeStyle.spaceAroundBinaryOps;
            }
            case BEFORE_COMMA: {
                return codeStyle.spaceBeforeComma;
            }
            case AFTER_COMMA: {
                return codeStyle.spaceAfterComma;
            }
            case AFTER_IF_KEYWORD: {
                return codeStyle.spaceBeforeIfParen;
            }
            case AFTER_WHILE_KEYWORD: {
                return codeStyle.spaceBeforeWhileParen;
            }
            case AFTER_FOR_KEYWORD: {
                return codeStyle.spaceBeforeForParen;
            }
            case AFTER_WITH_KEYWORD: {
                return codeStyle.spaceBeforeWithParen;
            }
            case AFTER_SWITCH_KEYWORD: {
                return codeStyle.spaceBeforeSwitchParen;
            }
            case AFTER_CATCH_KEYWORD: {
                return codeStyle.spaceBeforeCatchParen;
            }
            case BEFORE_WHILE_KEYWORD: {
                return codeStyle.spaceBeforeWhile;
            }
            case BEFORE_ELSE_KEYWORD: {
                return codeStyle.spaceBeforeElse;
            }
            case BEFORE_CATCH_KEYWORD: {
                return codeStyle.spaceBeforeCatch;
            }
            case BEFORE_FINALLY_KEYWORD: {
                return codeStyle.spaceBeforeFinally;
            }
            case BEFORE_SEMICOLON: {
                return codeStyle.spaceBeforeSemi;
            }
            case AFTER_SEMICOLON: {
                return codeStyle.spaceAfterSemi;
            }
            case BEFORE_UNARY_OPERATOR: {
                return codeStyle.spaceAroundUnaryOps;
            }
            case AFTER_UNARY_OPERATOR: {
                return codeStyle.spaceAroundUnaryOps;
            }
            case BEFORE_TERNARY_OPERATOR: {
                return codeStyle.spaceAroundTernaryOps;
            }
            case AFTER_TERNARY_OPERATOR: {
                return codeStyle.spaceAroundTernaryOps;
            }
            case BEFORE_ANONYMOUS_FUNCTION_DECLARATION: {
                return codeStyle.spaceBeforeAnonMethodDeclParen;
            }
            case BEFORE_FUNCTION_DECLARATION: {
                return codeStyle.spaceBeforeMethodDeclParen;
            }
            case BEFORE_FUNCTION_CALL: {
                return codeStyle.spaceBeforeMethodCallParen;
            }
            case AFTER_FUNCTION_DECLARATION_PARENTHESIS: {
                return codeStyle.spaceWithinMethodDeclParens;
            }
            case BEFORE_FUNCTION_DECLARATION_PARENTHESIS: {
                return codeStyle.spaceWithinMethodDeclParens;
            }
            case AFTER_FUNCTION_CALL_PARENTHESIS: {
                return codeStyle.spaceWithinMethodCallParens;
            }
            case BEFORE_FUNCTION_CALL_PARENTHESIS: {
                return codeStyle.spaceWithinMethodCallParens;
            }
            case AFTER_IF_PARENTHESIS: {
                return codeStyle.spaceWithinIfParens;
            }
            case BEFORE_IF_PARENTHESIS: {
                return codeStyle.spaceWithinIfParens;
            }
            case AFTER_WHILE_PARENTHESIS: {
                return codeStyle.spaceWithinWhileParens;
            }
            case BEFORE_WHILE_PARENTHESIS: {
                return codeStyle.spaceWithinWhileParens;
            }
            case AFTER_FOR_PARENTHESIS: {
                return codeStyle.spaceWithinForParens;
            }
            case BEFORE_FOR_PARENTHESIS: {
                return codeStyle.spaceWithinForParens;
            }
            case AFTER_WITH_PARENTHESIS: {
                return codeStyle.spaceWithinWithParens;
            }
            case BEFORE_WITH_PARENTHESIS: {
                return codeStyle.spaceWithinWithParens;
            }
            case AFTER_SWITCH_PARENTHESIS: {
                return codeStyle.spaceWithinSwitchParens;
            }
            case BEFORE_SWITCH_PARENTHESIS: {
                return codeStyle.spaceWithinSwitchParens;
            }
            case AFTER_CATCH_PARENTHESIS: {
                return codeStyle.spaceWithinCatchParens;
            }
            case BEFORE_CATCH_PARENTHESIS: {
                return codeStyle.spaceWithinCatchParens;
            }
            case AFTER_LEFT_PARENTHESIS: {
                return codeStyle.spaceWithinParens;
            }
            case BEFORE_RIGHT_PARENTHESIS: {
                return codeStyle.spaceWithinParens;
            }
            case AFTER_LEFT_BRACE: {
                return codeStyle.spaceWithinBraces;
            }
            case BEFORE_RIGHT_BRACE: {
                return codeStyle.spaceWithinBraces;
            }
            case BEFORE_IF_BRACE: {
                return codeStyle.spaceBeforeIfLeftBrace;
            }
            case BEFORE_ELSE_BRACE: {
                return codeStyle.spaceBeforeElseLeftBrace;
            }
            case BEFORE_WHILE_BRACE: {
                return codeStyle.spaceBeforeWhileLeftBrace;
            }
            case BEFORE_FOR_BRACE: {
                return codeStyle.spaceBeforeForLeftBrace;
            }
            case BEFORE_DO_BRACE: {
                return codeStyle.spaceBeforeDoLeftBrace;
            }
            case BEFORE_TRY_BRACE: {
                return codeStyle.spaceBeforeTryLeftBrace;
            }
            case BEFORE_CATCH_BRACE: {
                return codeStyle.spaceBeforeCatchLeftBrace;
            }
            case BEFORE_FINALLY_BRACE: {
                return codeStyle.spaceBeforeFinallyLeftBrace;
            }
            case BEFORE_SWITCH_BRACE: {
                return codeStyle.spaceBeforeSwitchLeftBrace;
            }
            case BEFORE_WITH_BRACE: {
                return codeStyle.spaceBeforeWithLeftBrace;
            }
            case BEFORE_FUNCTION_DECLARATION_BRACE: {
                return codeStyle.spaceBeforeMethodDeclLeftBrace;
            }
            case AFTER_ARRAY_LITERAL_BRACKET: {
                return codeStyle.spaceWithinArrayBrackets;
            }
            case BEFORE_ARRAY_LITERAL_BRACKET: {
                return codeStyle.spaceWithinArrayBrackets;
            }
            case AFTER_NEW_KEYWORD: {
                return true;
            }
            case AFTER_VAR_KEYWORD: {
                return true;
            }
            case AFTER_TYPEOF_KEYWORD: {
                return true;
            }
            case BEFORE_DOT: 
            case AFTER_DOT: {
                return false;
            }
        }
        return false;
    }

    private static boolean isEmptyObject(FormatToken token) {
        if (token.getKind() == FormatToken.Kind.AFTER_OBJECT_START) {
            for (FormatToken current = token.next(); current != null && current.isVirtual(); current = current.next()) {
                if (current.getKind() != FormatToken.Kind.BEFORE_OBJECT_END) continue;
                return true;
            }
            return false;
        }
        if (token.getKind() == FormatToken.Kind.BEFORE_OBJECT_END) {
            for (FormatToken current = token.previous(); current != null && current.isVirtual(); current = current.previous()) {
                if (current.getKind() != FormatToken.Kind.AFTER_OBJECT_START) continue;
                return true;
            }
            return false;
        }
        return false;
    }

    private int getFormatStableStart(BaseDocument doc, Language<JsTokenId> language, int offset, int startOffset, boolean embedded) {
        TokenSequence<JsTokenId> ts = LexUtilities.getTokenSequence(TokenHierarchy.get((Document)doc), offset, language);
        if (ts == null) {
            return 0;
        }
        ts.move(startOffset);
        if (!ts.movePrevious()) {
            return 0;
        }
        int curlyBalance = 0;
        block7 : do {
            Token token = ts.token();
            JsTokenId id = (JsTokenId)token.id();
            switch (id) {
                case KEYWORD_FUNCTION: {
                    if (curlyBalance <= 0 || ts.offset() >= offset) continue block7;
                    return ts.offset();
                }
                case BRACKET_LEFT_CURLY: {
                    ++curlyBalance;
                    break;
                }
                case BRACKET_RIGHT_CURLY: {
                    --curlyBalance;
                    break;
                }
            }
        } while (ts.movePrevious());
        if (embedded && !ts.movePrevious()) {
            int sequenceBegin = ts.offset();
            try {
                int lineTextEnd = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)sequenceBegin);
                if (lineTextEnd == -1 || sequenceBegin > lineTextEnd) {
                    return Math.min(doc.getLength(), Utilities.getRowEnd((BaseDocument)doc, (int)sequenceBegin) + 1);
                }
            }
            catch (BadLocationException ex) {
                LOGGER.log(Level.INFO, null, ex);
            }
        }
        return ts.offset();
    }

    private boolean isContinuation(BaseDocument doc, int offset, int bracketBalance, boolean continued, int bracketBalanceDelta, IndentContext.BlockDescription block) throws BadLocationException {
        Token token;
        if ((offset = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)offset)) == -1) {
            return false;
        }
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getPositionedSequence((Document)doc, offset, this.language);
        Token token2 = token = ts != null ? ts.token() : null;
        if (ts != null && token != null) {
            int index = ts.index();
            JsTokenId previousId = null;
            if (ts.movePrevious()) {
                Token<? extends JsTokenId> previous = LexUtilities.findPreviousNonWsNonComment(ts);
                if (previous != null) {
                    previousId = (JsTokenId)previous.id();
                }
                ts.moveIndex(index);
                ts.moveNext();
            }
            JsTokenId id = (JsTokenId)token.id();
            boolean isContinuationOperator = LexUtilities.isBinaryOperator(id, previousId);
            if (ts.offset() == offset && token.length() > 1 && token.text().toString().startsWith("\\")) {
                isContinuationOperator = true;
            }
            if (id == JsTokenId.OPERATOR_COMMA) {
                isContinuationOperator = bracketBalance == 0 && (block == null || !block.isObject());
            } else if (id == JsTokenId.BRACKET_LEFT_PAREN) {
                isContinuationOperator = true;
            } else if (id == JsTokenId.BRACKET_LEFT_CURLY) {
                isContinuationOperator = bracketBalanceDelta >= 0 && continued;
            } else if (id == JsTokenId.OPERATOR_COLON) {
                TokenSequence<? extends JsTokenId> inner = LexUtilities.getPositionedSequence((Document)doc, ts.offset(), this.language);
                Token<? extends JsTokenId> foundToken = LexUtilities.findPreviousIncluding(inner, Arrays.asList(new JsTokenId[]{JsTokenId.KEYWORD_CASE, JsTokenId.KEYWORD_DEFAULT, JsTokenId.OPERATOR_COLON}));
                isContinuationOperator = foundToken == null || foundToken.id() != JsTokenId.KEYWORD_CASE && foundToken.id() != JsTokenId.KEYWORD_DEFAULT;
            } else {
                JsTokenId nextId = null;
                if (ts.moveNext()) {
                    Token<? extends JsTokenId> next = LexUtilities.findNextNonWsNonComment(ts);
                    if (next != null) {
                        nextId = (JsTokenId)next.id();
                    }
                    ts.moveIndex(index);
                    ts.moveNext();
                }
                if (nextId == JsTokenId.BRACKET_RIGHT_PAREN) {
                    isContinuationOperator = true;
                }
            }
            return isContinuationOperator;
        }
        return false;
    }

    public void reindent(final Context context) {
        Document document = context.document();
        int startOffset = context.startOffset();
        int endOffset = context.endOffset();
        final IndentContext indentContext = new IndentContext(context, this.provider);
        int indentationSize = IndentUtils.indentLevelSize((Document)document);
        int continuationIndent = CodeStyle.get(indentContext).getContinuationIndentSize();
        try {
            boolean indentOnly;
            Token<? extends JsTokenId> token;
            final BaseDocument doc = (BaseDocument)document;
            startOffset = Utilities.getRowStart((BaseDocument)doc, (int)startOffset);
            int endLineOffset = Utilities.getRowStart((BaseDocument)doc, (int)endOffset);
            boolean bl = indentOnly = !(startOffset != endLineOffset || endOffset != context.caretOffset() && startOffset != context.caretOffset() || !Utilities.isRowEmpty((BaseDocument)doc, (int)startOffset) && !Utilities.isRowWhite((BaseDocument)doc, (int)startOffset) && Utilities.getFirstNonWhiteFwd((BaseDocument)doc, (int)startOffset) != context.caretOffset());
            if (indentOnly && indentContext.isEmbedded() && (token = LexUtilities.getToken((Document)doc, startOffset, this.language)) == null) {
                return;
            }
            if (endOffset > doc.getLength()) {
                endOffset = doc.getLength();
            }
            final int lineStart = startOffset;
            int initialOffset = 0;
            int initialIndent = 0;
            if (startOffset > 0) {
                int prevOffset = Utilities.getRowStart((BaseDocument)doc, (int)(startOffset - 1));
                initialOffset = this.getFormatStableStart(doc, this.language, prevOffset, startOffset, indentContext.isEmbedded());
                initialIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)initialOffset);
            }
            boolean indentEmptyLines = startOffset != 0 || endOffset + 1 != doc.getLength();
            boolean includeEnd = endOffset == doc.getLength() || indentOnly;
            this.computeIndents(indentContext, initialIndent, indentationSize, continuationIndent, initialOffset, endOffset, indentEmptyLines, includeEnd, indentOnly);
            doc.runAtomic(new Runnable(){

                @Override
                public void run() {
                    try {
                        List<IndentContext.Indentation> indents = indentContext.getIndentations();
                        for (int i = indents.size() - 1; i >= 0; --i) {
                            IndentContext.Indentation indentation = indents.get(i);
                            int indent = indentation.getSize();
                            int lineBegin = indentation.getOffset();
                            if (lineBegin >= lineStart) {
                                int currentIndent;
                                if (lineBegin == lineStart && i > 0) {
                                    IndentContext.Indentation prevIndentation = indents.get(i - 1);
                                    int prevOffset = prevIndentation.getOffset();
                                    int prevIndent = prevIndentation.getSize();
                                    int actualPrevIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)prevOffset);
                                    if (actualPrevIndent != prevIndent && (indentOnly || !Utilities.isRowEmpty((BaseDocument)doc, (int)prevOffset) && !Utilities.isRowWhite((BaseDocument)doc, (int)prevOffset))) {
                                        indent = actualPrevIndent + (indent - prevIndent);
                                    }
                                }
                                if ((currentIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)lineBegin)) == indent || indent < 0) continue;
                                context.modifyIndent(lineBegin, indent);
                                continue;
                            }
                            break;
                        }
                    }
                    catch (BadLocationException ble) {
                        Exceptions.printStackTrace((Throwable)ble);
                    }
                }
            });
        }
        catch (BadLocationException ble) {
            LOGGER.log(Level.FINE, null, ble);
        }
    }

    private void computeIndents(IndentContext context, int initialIndent, int indentSize, int continuationIndent, int startOffset, int endOffset, boolean indentEmptyLines, boolean includeEnd, boolean indentOnly) {
        BaseDocument doc = context.getDocument();
        try {
            int offset = Utilities.getRowStart((BaseDocument)doc, (int)startOffset);
            int end = endOffset;
            int balance = 0;
            int bracketBalance = 0;
            boolean continued = false;
            int adjustedBlockCommentIndention = 0;
            boolean IN_CODE = false;
            boolean IN_LITERAL = true;
            int IN_BLOCK_COMMENT_START = 2;
            int IN_BLOCK_COMMENT_MIDDLE = 3;
            while (!includeEnd && offset < end || includeEnd && offset <= end) {
                int endIndents;
                int indent;
                int lineBegin;
                int hangingIndent;
                if (context.isEmbedded()) {
                    initialIndent = context.getEmbeddedIndent() + indentSize;
                }
                int lineType = 0;
                int pos = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
                TokenSequence<? extends JsTokenId> ts = null;
                if (pos != -1) {
                    ts = LexUtilities.getPositionedSequence((Document)doc, pos, false, this.language);
                    if (ts != null) {
                        JsTokenId id = (JsTokenId)ts.token().id();
                        int index = ts.index();
                        JsTokenId previousId = null;
                        if (ts.movePrevious()) {
                            Token<? extends JsTokenId> previous = LexUtilities.findPreviousNonWsNonComment(ts);
                            if (previous != null) {
                                previousId = (JsTokenId)previous.id();
                            }
                            ts.moveIndex(index);
                            ts.moveNext();
                        }
                        if (id == JsTokenId.BLOCK_COMMENT || id == JsTokenId.DOC_COMMENT) {
                            lineType = ts.offset() == pos ? 2 : 3;
                        } else if (LexUtilities.isBinaryOperator(id, previousId)) {
                            continued = true;
                        } else if (id == JsTokenId.STRING || id == JsTokenId.STRING_END || id == JsTokenId.REGEXP || id == JsTokenId.REGEXP_END) {
                            lineType = 1;
                        }
                    } else {
                        lineType = 1;
                    }
                }
                int n = hangingIndent = continued ? continuationIndent : 0;
                if (lineType == 1) {
                    indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                } else if (lineType == 3) {
                    indent = doc.getText(pos, 1).charAt(0) == '*' ? adjustedBlockCommentIndention + 1 : GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                } else if (!(indentOnly && offset >= context.getCaretLineStart() && offset <= context.getCaretLineEnd() || (endIndents = this.isEndIndent(context, offset)) <= 0)) {
                    indent = (balance - endIndents) * indentSize + hangingIndent + initialIndent;
                } else {
                    assert (lineType == 0 || lineType == 2);
                    indent = balance * indentSize + hangingIndent + initialIndent;
                    if (lineType == 2) {
                        adjustedBlockCommentIndention = indent;
                    }
                }
                if (indent < 0) {
                    indent = 0;
                }
                if ((lineBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset)) != -1 || indentEmptyLines) {
                    context.addIndentation(new IndentContext.Indentation(offset, indent, continued));
                }
                int endOfLine = Utilities.getRowEnd((BaseDocument)doc, (int)offset) + 1;
                if (lineBegin != -1) {
                    balance += this.getTokenBalance(context, ts, lineBegin, endOfLine, true, indentOnly);
                    int bracketDelta = this.getTokenBalance(context, ts, lineBegin, endOfLine, false, indentOnly);
                    continued = this.isContinuation(doc, offset, bracketBalance += bracketDelta, continued, bracketDelta, context.getBlocks().isEmpty() ? null : context.getBlocks().peek());
                }
                offset = endOfLine;
            }
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
    }

    private int getTokenBalance(IndentContext context, TokenSequence<? extends JsTokenId> ts, int begin, int end, boolean includeKeywords, boolean indentOnly) {
        Token token;
        TokenSequence<? extends JsTokenId> ets;
        int balance = 0;
        BaseDocument doc = context.getDocument();
        if (ts == null) {
            try {
                context.setEmbeddedIndent(Utilities.getRowIndent((BaseDocument)doc, (int)begin));
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            return 0;
        }
        ts.move(begin);
        if (!ts.moveNext()) {
            return 0;
        }
        int last = begin;
        while ((token = ts.token()) != null) {
            JsTokenId id = (JsTokenId)token.id();
            if (includeKeywords) {
                int delta = this.getTokenBalanceDelta(context, id, ts, indentOnly);
                balance += delta;
            } else {
                balance += this.getBracketBalanceDelta(id);
            }
            last = ts.offset() + token.length();
            if (ts.moveNext() && ts.offset() < end) continue;
        }
        if (context.isEmbedded() && last < end && (ets = LexUtilities.getNextJsTokenSequence((Document)doc, last + 1, end, this.language)) != null && ets.offset() > begin) {
            return balance + this.getTokenBalance(context, ets, ets.offset(), end, includeKeywords, indentOnly);
        }
        return balance;
    }

    private int getBracketBalanceDelta(JsTokenId id) {
        if (id == JsTokenId.BRACKET_LEFT_PAREN || id == JsTokenId.BRACKET_LEFT_BRACKET) {
            return 1;
        }
        if (id == JsTokenId.BRACKET_RIGHT_PAREN || id == JsTokenId.BRACKET_RIGHT_BRACKET) {
            return -1;
        }
        return 0;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private int getTokenBalanceDelta(IndentContext context, JsTokenId id, TokenSequence<? extends JsTokenId> ts, boolean indentOnly) {
        try {
            BaseDocument doc = context.getDocument();
            OffsetRange range = OffsetRange.NONE;
            if (id == JsTokenId.BRACKET_LEFT_BRACKET) {
                context.getBlocks().push(new IndentContext.BlockDescription(false, false, new OffsetRange(ts.offset(), ts.offset())));
                return 1;
            }
            if (id == JsTokenId.BRACKET_LEFT_CURLY) {
                boolean object = false;
                TokenSequence<? extends JsTokenId> inner = LexUtilities.getPositionedSequence((Document)doc, ts.offset(), this.language);
                if (inner != null) {
                    inner.movePrevious();
                    Token<? extends JsTokenId> token = LexUtilities.findPreviousNonWsNonComment(inner);
                    if (token.id() != JsTokenId.BRACKET_RIGHT_PAREN && token.id() != JsTokenId.KEYWORD_DO && token.id() != JsTokenId.KEYWORD_ELSE && token.id() != JsTokenId.KEYWORD_FINALLY) {
                        object = true;
                    }
                }
                context.getBlocks().push(new IndentContext.BlockDescription(false, object, new OffsetRange(ts.offset(), ts.offset())));
                return 1;
            }
            if (id == JsTokenId.KEYWORD_CASE || id == JsTokenId.KEYWORD_DEFAULT) {
                int index = ts.index();
                LexUtilities.findNextIncluding(ts, Collections.singletonList(JsTokenId.OPERATOR_COLON));
                Token<? extends JsTokenId> token = LexUtilities.findNextNonWsNonComment(ts);
                JsTokenId tokenId = (JsTokenId)token.id();
                if (tokenId == JsTokenId.KEYWORD_CASE || tokenId == JsTokenId.KEYWORD_DEFAULT) {
                    return 0;
                }
                if (tokenId == JsTokenId.BRACKET_RIGHT_CURLY) {
                    return -1;
                }
                LexUtilities.findNextIncluding(ts, Collections.singletonList(JsTokenId.EOL));
                LexUtilities.findNextNonWsNonComment(ts);
                if (ts.token().id() == JsTokenId.KEYWORD_CASE || ts.token().id() == JsTokenId.KEYWORD_DEFAULT || ts.token().id() == JsTokenId.BRACKET_LEFT_CURLY) {
                    return 0;
                }
                ts.moveIndex(index);
                ts.moveNext();
                return 1;
            }
            if (id == JsTokenId.BRACKET_RIGHT_BRACKET || id == JsTokenId.BRACKET_RIGHT_CURLY) {
                int delta = -1;
                if (context.getBlocks().empty()) {
                    return delta;
                }
                IndentContext.BlockDescription blockDescription = context.getBlocks().pop();
                IndentContext.BlockDescription lastPop = blockDescription;
                if (lastPop == null || lastPop.getRange().getStart() > doc.getLength() + 1 || Utilities.getLineOffset((BaseDocument)doc, (int)lastPop.getRange().getStart()) == Utilities.getLineOffset((BaseDocument)doc, (int)ts.offset())) return delta;
                int blocks = 0;
                while (!context.getBlocks().empty() && context.getBlocks().peek().isBraceless()) {
                    context.getBlocks().pop();
                    ++blocks;
                }
                delta -= blocks;
                return delta;
            }
            range = LexUtilities.getMultilineRange((Document)doc, ts);
            if (range != OffsetRange.NONE) {
                context.getBlocks().push(new IndentContext.BlockDescription(true, false, range));
                return 0;
            }
            if (id != JsTokenId.EOL) return 0;
            if (!indentOnly) {
                Token<? extends JsTokenId> prevToken;
                JsTokenId tokenId;
                TokenSequence<? extends JsTokenId> inner = LexUtilities.getPositionedSequence((Document)doc, ts.offset(), this.language);
                Token<? extends JsTokenId> nextToken = null;
                if (inner != null) {
                    nextToken = LexUtilities.findNextNonWsNonComment(inner);
                }
                JsTokenId jsTokenId = tokenId = nextToken == null ? null : (JsTokenId)nextToken.id();
                if (tokenId == JsTokenId.BRACKET_RIGHT_CURLY) {
                    OffsetRange offsetRange = LexUtilities.findBwd((Document)doc, inner, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY);
                    if (offsetRange != OffsetRange.NONE) {
                        inner.movePrevious();
                        if (LexUtilities.skipParenthesis(inner, true)) {
                            Token<? extends JsTokenId> token = inner.token();
                            token = LexUtilities.findPreviousNonWsNonComment(inner);
                            if (token.id() == JsTokenId.KEYWORD_SWITCH) {
                                return -1;
                            }
                        }
                    }
                } else if ((tokenId == JsTokenId.KEYWORD_CASE || tokenId == JsTokenId.KEYWORD_DEFAULT) && (prevToken = LexUtilities.findPreviousNonWsNonComment(inner = LexUtilities.getPositionedSequence((Document)doc, ts.offset(), this.language))).id() != JsTokenId.BRACKET_LEFT_CURLY) {
                    inner = LexUtilities.getPositionedSequence((Document)doc, ts.offset(), this.language);
                    LexUtilities.findPreviousIncluding(inner, Arrays.asList(new JsTokenId[]{JsTokenId.KEYWORD_CASE, JsTokenId.KEYWORD_DEFAULT}));
                    int offset = inner.offset();
                    inner = LexUtilities.getPositionedSequence((Document)doc, ts.offset(), this.language);
                    prevToken = LexUtilities.findPrevious(inner, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL}));
                    int beginLine = Utilities.getLineOffset((BaseDocument)doc, (int)offset);
                    int eolLine = Utilities.getLineOffset((BaseDocument)doc, (int)ts.offset());
                    if (prevToken.id() != JsTokenId.BLOCK_COMMENT && prevToken.id() != JsTokenId.DOC_COMMENT && prevToken.id() != JsTokenId.LINE_COMMENT) {
                        if (beginLine != eolLine) {
                            if (prevToken.id() != JsTokenId.BRACKET_RIGHT_CURLY) return -1;
                            OffsetRange offsetRange = LexUtilities.findBwd((Document)doc, inner, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY);
                            if (offsetRange != OffsetRange.NONE) {
                                inner.movePrevious();
                                Token<? extends JsTokenId> token = LexUtilities.findPreviousNonWsNonComment(inner);
                                if (token.id() != JsTokenId.OPERATOR_COLON) {
                                    return -1;
                                }
                            }
                        }
                    } else {
                        int commentLine = Utilities.getLineOffset((BaseDocument)doc, (int)inner.offset());
                        if (beginLine != eolLine && commentLine == beginLine) {
                            return -1;
                        }
                    }
                }
            }
            if (context.getBlocks().empty() || !context.getBlocks().peek().isBraceless()) return 0;
            OffsetRange stackOffset = context.getBlocks().peek().getRange();
            if (stackOffset.containsInclusive(ts.offset())) {
                int offsetLine;
                if (indentOnly) {
                    return 1;
                }
                int stackEndLine = Utilities.getLineOffset((BaseDocument)doc, (int)stackOffset.getEnd());
                if (stackEndLine != (offsetLine = Utilities.getLineOffset((BaseDocument)doc, (int)ts.offset()))) return 0;
                return 1;
            }
            int blocks = 0;
            while (!context.getBlocks().empty() && context.getBlocks().peek().isBraceless()) {
                ++blocks;
                context.getBlocks().pop();
            }
            return - blocks;
        }
        catch (BadLocationException ble) {
            LOGGER.log(Level.INFO, null, ble);
        }
        return 0;
    }

    private int isEndIndent(IndentContext context, int offset) throws BadLocationException {
        BaseDocument doc = context.getDocument();
        int lineBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (lineBegin != -1) {
            Token<? extends JsTokenId> token = this.getFirstToken(context, offset);
            if (token == null) {
                return 0;
            }
            TokenId id = token.id();
            if (id == JsTokenId.BRACKET_RIGHT_CURLY || id == JsTokenId.BRACKET_RIGHT_BRACKET) {
                int indents = 1;
                int lineEnd = Utilities.getRowEnd((BaseDocument)doc, (int)offset);
                int newOffset = offset;
                while (newOffset < lineEnd && token != null && ((newOffset += token.length()) >= doc.getLength() || (token = LexUtilities.getToken((Document)doc, newOffset, this.language)) == null || (id = token.id()) == JsTokenId.WHITESPACE)) {
                }
                return indents;
            }
        }
        return 0;
    }

    private Token<? extends JsTokenId> getFirstToken(IndentContext context, int offset) throws BadLocationException {
        BaseDocument doc = context.getDocument();
        int lineBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (lineBegin != -1) {
            if (context.isEmbedded()) {
                TokenSequence<JsTokenId> ts = LexUtilities.getTokenSequence(TokenHierarchy.get((Document)doc), lineBegin, this.language);
                if (ts != null) {
                    ts.moveNext();
                    Token token = ts.token();
                    while (token != null && token.id() == JsTokenId.WHITESPACE) {
                        if (!ts.moveNext()) {
                            return null;
                        }
                        token = ts.token();
                    }
                    return token;
                }
            } else {
                return LexUtilities.getToken((Document)doc, lineBegin, this.language);
            }
        }
        return null;
    }

    private void moveForward(FormatToken token, FormatToken limit, FormatContext formatContext, boolean allowComment) {
        for (FormatToken current = token; current != null && current != limit; current = current.next()) {
            assert (current.isVirtual() || current.getKind() == FormatToken.Kind.WHITESPACE || current.getKind() == FormatToken.Kind.EOL || allowComment && (current.getKind() == FormatToken.Kind.BLOCK_COMMENT || current.getKind() == FormatToken.Kind.LINE_COMMENT || current.getKind() == FormatToken.Kind.DOC_COMMENT));
            this.processed.add(current);
            JsFormatter.updateIndentationLevel(current, formatContext);
            if (current.getKind() != FormatToken.Kind.EOL) continue;
            formatContext.setCurrentLineStart(current.getOffset() + 1 + formatContext.getOffsetDiff());
            formatContext.setLastLineWrap(null);
        }
    }

    private static boolean isWhitespace(CharSequence charSequence) {
        for (int i = 0; i < charSequence.length(); ++i) {
            if (Character.isWhitespace(charSequence.charAt(i))) continue;
            return false;
        }
        return true;
    }

    static class Indentation {
        static final Indentation ALLOWED = new Indentation(true, false);
        static final Indentation FORBIDDEN = new Indentation(false, false);
        private final boolean allowed;
        private final boolean exceedLimits;

        public Indentation(boolean allowed, boolean exceedLimits) {
            this.allowed = allowed;
            this.exceedLimits = exceedLimits;
        }

        public boolean isAllowed() {
            return this.allowed;
        }

        public boolean isExceedLimits() {
            return this.exceedLimits;
        }
    }

}

