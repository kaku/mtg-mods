/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.AccessNode
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.FunctionNode$Kind
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.PropertyNode
 *  jdk.nashorn.internal.ir.VarNode
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import jdk.nashorn.internal.ir.AccessNode;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.PropertyNode;
import jdk.nashorn.internal.ir.VarNode;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.doc.api.JsDocumentationSupport;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.PathNodeVisitor;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public class JsDocumentationCompleter {
    protected static final RequestProcessor RP = new RequestProcessor("JavaScript Documentation Completer", 1);

    public static void generateCompleteComment(BaseDocument doc, int caretOffset, int indent) {
        DocumentationGenerator documentationGenerator = new DocumentationGenerator(doc, caretOffset, indent);
        RP.post((Runnable)documentationGenerator);
    }

    private static JsObject getWrapperScope(JsParserResult jsParserResult, JsObject jsObject, Node nearestNode, int offset) {
        JsObject result = null;
        if (jsObject instanceof JsFunctionImpl) {
            result = jsObject;
            for (DeclarationScope declarationScope : ((JsFunctionImpl)jsObject).getChildrenScopes()) {
                if (!(declarationScope instanceof JsFunctionImpl) || !((JsFunctionImpl)declarationScope).getOffsetRange(jsParserResult).containsInclusive(offset)) continue;
                result = JsDocumentationCompleter.getWrapperScope(jsParserResult, (JsFunctionImpl)declarationScope, nearestNode, offset);
            }
        }
        return result;
    }

    private static boolean isWrapperObject(JsParserResult jsParserResult, JsObject jsObject, Node nearestNode) {
        List<Identifier> nodeName = jsParserResult.getModel().getNodeName(nearestNode);
        if (nodeName == null || nodeName.isEmpty()) {
            return false;
        }
        return jsObject.getProperties().containsKey(nodeName.get(nodeName.size() - 1).getName());
    }

    public static String getFqnName(JsParserResult parserResult, Node node) {
        PathToNodeVisitor ptnv = new PathToNodeVisitor(node);
        FunctionNode root = parserResult.getRoot();
        root.accept((NodeVisitor)ptnv);
        StringBuilder fqn = new StringBuilder();
        for (Node currentNode : ptnv.getFinalPath()) {
            List<Identifier> name = parserResult.getModel().getNodeName(currentNode);
            if (name == null) continue;
            for (Identifier identifier : name) {
                fqn.append(".").append(identifier.getName());
            }
        }
        if (fqn.length() > 0) {
            return fqn.toString().substring(1);
        }
        return "";
    }

    private static void generateFieldComment(BaseDocument doc, int offset, int indent, JsParserResult jsParserResult, JsObject jsObject) throws BadLocationException {
        StringBuilder toAdd = new StringBuilder();
        SyntaxProvider syntaxProvider = JsDocumentationSupport.getSyntaxProvider(jsParserResult);
        Collection<? extends TypeUsage> assignments = jsObject.getAssignments();
        StringBuilder types = new StringBuilder();
        for (TypeUsage typeUsage : assignments) {
            if (typeUsage.getType().equals(jsObject.getName())) continue;
            types.append("|").append(typeUsage.getType());
        }
        String type = types.length() == 0 ? null : types.toString().substring(1);
        JsDocumentationCompleter.generateDocEntry(doc, toAdd, syntaxProvider.typeTagTemplate(), indent, null, type);
        doc.insertString(offset, toAdd.toString(), null);
    }

    private static void generateFunctionComment(BaseDocument doc, int offset, int indent, JsParserResult jsParserResult, JsObject jsObject) throws BadLocationException {
        StringBuilder toAdd = new StringBuilder();
        SyntaxProvider syntaxProvider = JsDocumentationSupport.getSyntaxProvider(jsParserResult);
        JsFunction function = (JsFunction)jsObject;
        JsDocumentationCompleter.addParameters(doc, toAdd, syntaxProvider, indent, function.getParameters());
        Collection<? extends TypeUsage> returnTypes = function.getReturnTypes();
        Collection<TypeUsage> types = ModelUtils.resolveTypes(returnTypes, jsParserResult, true);
        if (types.isEmpty()) {
            if (JsDocumentationCompleter.hasReturnClause(jsParserResult, jsObject)) {
                JsDocumentationCompleter.addReturns(doc, toAdd, syntaxProvider, indent, Collections.singleton(new TypeUsageImpl("unresolved")));
            }
        } else {
            JsDocumentationCompleter.addReturns(doc, toAdd, syntaxProvider, indent, types);
        }
        doc.insertString(offset, toAdd.toString(), null);
    }

    private static boolean hasReturnClause(JsParserResult jsParserResult, JsObject jsObject) {
        OffsetRange offsetRange = jsObject.getOffsetRange();
        TokenHierarchy tokenHierarchy = jsParserResult.getSnapshot().getTokenHierarchy();
        TokenSequence ts = tokenHierarchy.tokenSequence(JsTokenId.javascriptLanguage());
        if (ts == null) {
            return false;
        }
        ts.move(offsetRange.getStart());
        if (!ts.moveNext() || !ts.movePrevious()) {
            return false;
        }
        while (ts.moveNext() && ts.offset() <= offsetRange.getEnd()) {
            if (ts.token().id() != JsTokenId.KEYWORD_RETURN) continue;
            return true;
        }
        return false;
    }

    private static void addParameters(BaseDocument doc, StringBuilder toAdd, SyntaxProvider syntaxProvider, int indent, Collection<? extends JsObject> params) {
        for (JsObject jsObject : params) {
            JsDocumentationCompleter.generateDocEntry(doc, toAdd, syntaxProvider.paramTagTemplate(), indent, jsObject.getName(), null);
        }
    }

    private static void addReturns(BaseDocument doc, StringBuilder toAdd, SyntaxProvider syntaxProvider, int indent, Collection<? extends TypeUsage> returns) {
        StringBuilder sb = new StringBuilder();
        for (TypeUsage typeUsage : returns) {
            if (syntaxProvider.typesSeparator() == null) {
                sb.append(" ").append(typeUsage.getType());
                break;
            }
            sb.append(syntaxProvider.typesSeparator()).append(typeUsage.getType());
        }
        int separatorLength = syntaxProvider.typesSeparator() == null ? 1 : syntaxProvider.typesSeparator().length();
        String returnString = returns.isEmpty() ? "" : sb.toString().substring(separatorLength);
        JsDocumentationCompleter.generateDocEntry(doc, toAdd, syntaxProvider.returnTagTemplate(), indent, null, returnString);
    }

    private static void generateDocEntry(BaseDocument doc, StringBuilder toAdd, String template, int indent, String name, String type) {
        toAdd.append("\n");
        toAdd.append(IndentUtils.createIndentString((Document)doc, (int)indent));
        toAdd.append("* ");
        toAdd.append(JsDocumentationCompleter.getProcessedTemplate(template, name, type));
    }

    private static String getProcessedTemplate(String template, String name, String type) {
        String finalTag = template;
        if (name != null) {
            finalTag = finalTag.replace("[name]", name);
        }
        finalTag = type != null ? finalTag.replace("[type]", type) : finalTag.replace("[type]", "type");
        return finalTag;
    }

    private static boolean isField(JsObject jsObject) {
        JsElement.Kind kind = jsObject.getJSKind();
        return kind == JsElement.Kind.FIELD || kind == JsElement.Kind.VARIABLE || kind == JsElement.Kind.PROPERTY;
    }

    private static boolean isFunction(JsObject jsObject) {
        return jsObject.getJSKind().isFunction();
    }

    private static Node getNearestNode(JsParserResult parserResult, int offset) {
        FunctionNode root = parserResult.getRoot();
        NearestNodeVisitor offsetVisitor = new NearestNodeVisitor(offset);
        root.accept((NodeVisitor)offsetVisitor);
        return offsetVisitor.getNearestNode();
    }

    private static JsObject findJsObjectFunctionVariable(JsObject object, int offset) {
        JsObjectImpl jsObject = (JsObjectImpl)object;
        JsObject result = null;
        JsObject tmpObject = null;
        if (jsObject.getOffsetRange().containsInclusive(offset)) {
            result = jsObject;
            for (JsObject property : jsObject.getProperties().values()) {
                JsElement.Kind kind = property.getJSKind();
                if (kind == JsElement.Kind.OBJECT || kind == JsElement.Kind.FUNCTION || kind == JsElement.Kind.METHOD || kind == JsElement.Kind.CONSTRUCTOR || kind == JsElement.Kind.VARIABLE) {
                    tmpObject = JsDocumentationCompleter.findJsObjectFunctionVariable(property, offset);
                }
                if (tmpObject == null) continue;
                result = tmpObject;
                break;
            }
        }
        return result;
    }

    private static class PathToNodeVisitor
    extends PathNodeVisitor {
        private final Node finalNode;
        private List<? extends Node> finalPath;

        public PathToNodeVisitor(Node finalNode) {
            this.finalNode = finalNode;
        }

        @Override
        public void addToPath(Node node) {
            super.addToPath(node);
            if (node.equals((Object)this.finalNode)) {
                this.finalPath = new LinkedList<Node>(this.getPath());
            }
        }

        public List<? extends Node> getFinalPath() {
            return this.finalPath;
        }
    }

    private static class FarestIdentNodeVisitor
    extends PathNodeVisitor {
        private Node farestNode;
        private final StringBuilder farestPath = new StringBuilder();

        private FarestIdentNodeVisitor() {
        }

        @Override
        public Node enter(IdentNode identNode) {
            this.farestNode = identNode;
            this.farestPath.append(".").append(identNode.getName());
            return super.enter(identNode);
        }

        @Override
        public Node leave(IdentNode identNode) {
            this.farestNode = identNode;
            return super.leave(identNode);
        }

        public Node getFarestNode() {
            return this.farestNode;
        }

        public String getFarestFqn() {
            return this.farestPath.toString().substring(1);
        }
    }

    private static class NearestNodeVisitor
    extends PathNodeVisitor {
        private final int offset;
        private Node nearestNode = null;

        public NearestNodeVisitor(int offset) {
            this.offset = offset;
        }

        private void processNode(Node node) {
            if (this.offset < node.getStart() && (this.nearestNode == null || node.getStart() < this.nearestNode.getStart())) {
                this.nearestNode = node;
            }
        }

        public Node getNearestNode() {
            if (this.nearestNode instanceof AccessNode) {
                FarestIdentNodeVisitor farestNV = new FarestIdentNodeVisitor();
                this.nearestNode.accept((NodeVisitor)farestNV);
                return farestNV.getFarestNode();
            }
            return this.nearestNode;
        }

        @Override
        public Node enter(AccessNode accessNode) {
            this.processNode((Node)accessNode);
            return super.enter(accessNode);
        }

        @Override
        public Node enter(FunctionNode functionNode) {
            if (functionNode.getKind() != FunctionNode.Kind.SCRIPT) {
                this.processNode((Node)functionNode);
            }
            return super.enter(functionNode);
        }

        @Override
        public Node enter(PropertyNode propertyNode) {
            this.processNode((Node)propertyNode);
            return super.enter(propertyNode);
        }

        @Override
        public Node enter(VarNode varNode) {
            this.processNode((Node)varNode);
            return super.enter(varNode);
        }

        @Override
        public Node enter(BinaryNode binaryNode) {
            this.processNode((Node)binaryNode);
            return super.enter(binaryNode);
        }
    }

    private static class DocumentationGenerator
    implements Runnable {
        private final BaseDocument doc;
        private final int offset;
        private final int indent;

        public DocumentationGenerator(BaseDocument doc, int offset, int indent) {
            this.doc = doc;
            this.offset = offset;
            this.indent = indent;
        }

        @Override
        public void run() {
            try {
                ParserManager.parse(Collections.singleton(Source.create((Document)this.doc)), (UserTask)new UserTask(){

                    public void run(ResultIterator resultIterator) throws Exception {
                        ParserResult parserResult = (ParserResult)resultIterator.getParserResult(DocumentationGenerator.this.offset);
                        if (parserResult != null && parserResult instanceof JsParserResult) {
                            JsObject wrapperScope;
                            JsParserResult jsParserResult = (JsParserResult)parserResult;
                            if (jsParserResult.getRoot() == null) {
                                return;
                            }
                            int embeddedOffset = parserResult.getSnapshot().getEmbeddedOffset(DocumentationGenerator.this.offset);
                            Node nearestNode = JsDocumentationCompleter.getNearestNode(jsParserResult, embeddedOffset);
                            if (nearestNode == null) {
                                return;
                            }
                            int examinedOffset = nearestNode instanceof VarNode ? nearestNode.getStart() : nearestNode.getFinish();
                            int originalExaminedOffset = parserResult.getSnapshot().getOriginalOffset(examinedOffset);
                            JsObject jsObject = JsDocumentationCompleter.findJsObjectFunctionVariable(jsParserResult.getModel().getGlobalObject(), originalExaminedOffset);
                            assert (jsObject != null);
                            if (jsObject.getJSKind() == JsElement.Kind.FILE || JsDocumentationCompleter.isWrapperObject(jsParserResult, jsObject, nearestNode)) {
                                String fqn = JsDocumentationCompleter.getFqnName(jsParserResult, nearestNode);
                                jsObject = ModelUtils.findJsObjectByName(jsParserResult.getModel(), fqn);
                                if (jsObject == null) {
                                    jsObject = jsParserResult.getModel().getGlobalObject();
                                }
                            }
                            if ((wrapperScope = JsDocumentationCompleter.getWrapperScope(jsParserResult, jsObject, nearestNode, originalExaminedOffset)) != null) {
                                jsObject = nearestNode instanceof VarNode && wrapperScope instanceof JsFunction ? ModelUtils.getJsObjectByName((JsFunction)wrapperScope, ((VarNode)nearestNode).getName().getName()) : wrapperScope;
                            }
                            if (jsObject == null) {
                                return;
                            }
                            int originalStart = parserResult.getSnapshot().getOriginalOffset(jsObject.getOffsetRange().getStart());
                            if (originalStart != -1 && originalStart < DocumentationGenerator.this.offset) {
                                return;
                            }
                            if (JsDocumentationCompleter.isField(jsObject)) {
                                JsDocumentationCompleter.generateFieldComment(DocumentationGenerator.this.doc, DocumentationGenerator.this.offset, DocumentationGenerator.this.indent, jsParserResult, jsObject);
                            } else if (JsDocumentationCompleter.isFunction(jsObject)) {
                                JsDocumentationCompleter.generateFunctionComment(DocumentationGenerator.this.doc, DocumentationGenerator.this.offset, DocumentationGenerator.this.indent, jsParserResult, jsObject);
                            } else {
                                JsDocumentationCompleter.generateFieldComment(DocumentationGenerator.this.doc, DocumentationGenerator.this.offset, DocumentationGenerator.this.indent, jsParserResult, jsObject);
                            }
                        }
                    }
                });
            }
            catch (ParseException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }

    }

}

