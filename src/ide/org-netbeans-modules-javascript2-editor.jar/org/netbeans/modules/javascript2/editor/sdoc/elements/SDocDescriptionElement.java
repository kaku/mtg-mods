/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocBaseElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public class SDocDescriptionElement
extends SDocBaseElement {
    private final String description;

    private SDocDescriptionElement(SDocElementType type, String description) {
        super(type);
        this.description = description;
    }

    public static SDocDescriptionElement create(SDocElementType type, String description) {
        return new SDocDescriptionElement(type, description);
    }

    public String getDescription() {
        return this.description;
    }
}

