/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.AccessNode
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.Block
 *  jdk.nashorn.internal.ir.CallNode
 *  jdk.nashorn.internal.ir.CatchNode
 *  jdk.nashorn.internal.ir.ExecuteNode
 *  jdk.nashorn.internal.ir.ForNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.FunctionNode$Kind
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.IndexNode
 *  jdk.nashorn.internal.ir.LiteralNode
 *  jdk.nashorn.internal.ir.LiteralNode$ArrayLiteralNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.ObjectNode
 *  jdk.nashorn.internal.ir.PropertyNode
 *  jdk.nashorn.internal.ir.ReferenceNode
 *  jdk.nashorn.internal.ir.ReturnNode
 *  jdk.nashorn.internal.ir.TernaryNode
 *  jdk.nashorn.internal.ir.UnaryNode
 *  jdk.nashorn.internal.ir.VarNode
 *  jdk.nashorn.internal.ir.WithNode
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  jdk.nashorn.internal.parser.TokenType
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jdk.nashorn.internal.ir.AccessNode;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.Block;
import jdk.nashorn.internal.ir.CallNode;
import jdk.nashorn.internal.ir.CatchNode;
import jdk.nashorn.internal.ir.ExecuteNode;
import jdk.nashorn.internal.ir.ForNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.IndexNode;
import jdk.nashorn.internal.ir.LiteralNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.ObjectNode;
import jdk.nashorn.internal.ir.PropertyNode;
import jdk.nashorn.internal.ir.ReferenceNode;
import jdk.nashorn.internal.ir.ReturnNode;
import jdk.nashorn.internal.ir.TernaryNode;
import jdk.nashorn.internal.ir.UnaryNode;
import jdk.nashorn.internal.ir.VarNode;
import jdk.nashorn.internal.ir.WithNode;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import jdk.nashorn.internal.parser.TokenType;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsModifier;
import org.netbeans.modules.javascript2.editor.embedding.JsEmbeddingProvider;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsArray;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.JsWith;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.CatchBlockImpl;
import org.netbeans.modules.javascript2.editor.model.impl.DeclarationScopeImpl;
import org.netbeans.modules.javascript2.editor.model.impl.FunctionArgumentAccessor;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsArrayImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionReference;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsWithObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelBuilder;
import org.netbeans.modules.javascript2.editor.model.impl.ModelElementFactory;
import org.netbeans.modules.javascript2.editor.model.impl.ModelExtender;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.OccurrenceBuilder;
import org.netbeans.modules.javascript2.editor.model.impl.ParameterObject;
import org.netbeans.modules.javascript2.editor.model.impl.PathNodeVisitor;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionArgument;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionInterceptor;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class ModelVisitor
extends PathNodeVisitor {
    private final ModelBuilder modelBuilder;
    private final OccurrenceBuilder occurrenceBuilder;
    private final List<List<FunctionNode>> functionStack;
    private final JsParserResult parserResult;
    private final Stack<Collection<JsObjectImpl>> functionArgumentStack = new Stack();
    private Map<FunctionInterceptor, Collection<FunctionCall>> functionCalls = null;
    private final String scriptName;

    public ModelVisitor(JsParserResult parserResult, OccurrenceBuilder occurrenceBuilder) {
        FileObject fileObject = parserResult.getSnapshot().getSource().getFileObject();
        this.modelBuilder = new ModelBuilder(JsFunctionImpl.createGlobal(fileObject, Integer.MAX_VALUE, parserResult.getSnapshot().getMimeType()));
        this.occurrenceBuilder = occurrenceBuilder;
        this.functionStack = new ArrayList<List<FunctionNode>>();
        this.parserResult = parserResult;
        this.scriptName = fileObject != null ? fileObject.getName() : "";
    }

    public JsObject getGlobalObject() {
        return this.modelBuilder.getGlobal();
    }

    @Override
    public Node enter(AccessNode accessNode) {
        BinaryNode node;
        BinaryNode binaryNode = node = this.getPath().get(this.getPath().size() - 1) instanceof BinaryNode ? (BinaryNode)this.getPath().get(this.getPath().size() - 1) : null;
        if ((node == null || node.tokenType() != TokenType.ASSIGN) && accessNode.getBase() instanceof IdentNode && "this".equals(((IdentNode)accessNode.getBase()).getName())) {
            IdentNode iNode = accessNode.getProperty();
            JsObject current = this.modelBuilder.getCurrentDeclarationFunction();
            JsObject property = current.getProperty(iNode.getName());
            if (property == null && current.getParent() != null && (current.getParent().getJSKind() == JsElement.Kind.CONSTRUCTOR || current.getParent().getJSKind() == JsElement.Kind.OBJECT) && (property = (current = current.getParent()).getProperty(iNode.getName())) == null && "prototype".equals(current.getName())) {
                current = current.getParent();
                property = current.getProperty(iNode.getName());
            }
            if (property == null && current.getParent() == null) {
                property = this.modelBuilder.getGlobal().getProperty(iNode.getName());
            }
            if (property != null) {
                ((JsObjectImpl)property).addOccurrence(new OffsetRange(iNode.getStart(), iNode.getFinish()));
            }
        }
        return super.enter(accessNode);
    }

    @Override
    public Node leave(AccessNode accessNode) {
        this.createJsObject(accessNode, this.parserResult, this.modelBuilder);
        return super.leave(accessNode);
    }

    @Override
    public Node enter(BinaryNode binaryNode) {
        Node lhs = binaryNode.lhs();
        Node rhs = binaryNode.rhs();
        if (binaryNode.tokenType() == TokenType.ASSIGN && !(rhs instanceof ReferenceNode) && !(rhs instanceof ObjectNode) && (lhs instanceof AccessNode || lhs instanceof IdentNode || lhs instanceof IndexNode)) {
            JsFunctionImpl parent = this.modelBuilder.getCurrentDeclarationFunction();
            if (parent == null) {
                return super.enter(binaryNode);
            }
            String fieldName = null;
            if (lhs instanceof AccessNode) {
                AccessNode aNode = (AccessNode)lhs;
                JsObjectImpl property = null;
                List<Identifier> fqName = ModelVisitor.getName(aNode, this.parserResult);
                if (fqName != null && "this".equals(fqName.get(0).getName())) {
                    fieldName = aNode.getProperty().getName();
                    property = (JsObjectImpl)this.createJsObject(aNode, this.parserResult, this.modelBuilder);
                } else if (fqName != null && (property = ModelUtils.getJsObject(this.modelBuilder, fqName, true)).getParent().getJSKind().isFunction() && !property.getModifiers().contains((Object)Modifier.STATIC)) {
                    property.getModifiers().add(Modifier.STATIC);
                }
                if (property != null) {
                    Collection types;
                    IdentNode iNode;
                    String parameter = null;
                    JsFunctionImpl function = this.modelBuilder.getCurrentDeclarationFunction();
                    if (binaryNode.rhs() instanceof IdentNode && function.getParameter((iNode = (IdentNode)rhs).getName()) != null) {
                        parameter = "@param;" + function.getFullyQualifiedName() + ":" + iNode.getName();
                    }
                    if (parameter == null) {
                        types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, binaryNode.rhs());
                        ArrayList<TypeUsage> correctedTypes = new ArrayList<TypeUsage>(types.size());
                        for (TypeUsage type : types) {
                            String typeName = type.getType();
                            if (typeName.startsWith("@var;")) {
                                String varName = typeName.substring("@var;".length());
                                if (function.getParameter(varName) != null) {
                                    correctedTypes.add(new TypeUsageImpl("@param;" + function.getFullyQualifiedName() + ":" + varName, type.getOffset(), false));
                                    continue;
                                }
                                correctedTypes.add(type);
                                continue;
                            }
                            correctedTypes.add(type);
                        }
                        types = correctedTypes;
                    } else {
                        types = new ArrayList<TypeUsageImpl>();
                        types.add((TypeUsageImpl)new TypeUsageImpl(parameter, binaryNode.rhs().getStart(), false));
                    }
                    for (TypeUsage type : types) {
                        property.addAssignment(type, binaryNode.getStart() + 5);
                    }
                }
            } else {
                JsObject lObject = null;
                int assignmentOffset = lhs.getFinish();
                if (lhs instanceof IndexNode) {
                    IdentifierImpl newPropName;
                    LiteralNode lNode;
                    IndexNode iNode = (IndexNode)lhs;
                    if (iNode.getBase() instanceof IdentNode) {
                        lObject = this.processLhs(ModelElementFactory.create(this.parserResult, (IdentNode)iNode.getBase()), parent, false);
                        assignmentOffset = iNode.getFinish();
                    }
                    if (lObject != null && iNode.getIndex() instanceof LiteralNode && (lNode = (LiteralNode)iNode.getIndex()).isString() && (newPropName = ModelElementFactory.create(this.parserResult, lNode)) != null) {
                        if (lObject.getProperty(lNode.getString()) == null) {
                            JsObjectImpl newProperty = new JsObjectImpl(lObject, newPropName, newPropName.getOffsetRange(), true, this.parserResult.getSnapshot().getMimeType(), null);
                            lObject.addProperty(newPropName.getName(), newProperty);
                            assignmentOffset = lNode.getFinish();
                        }
                        lObject = this.processLhs(newPropName, lObject, true);
                    }
                } else if (lhs instanceof IdentNode) {
                    lObject = this.processLhs(ModelElementFactory.create(this.parserResult, (IdentNode)lhs), parent, true);
                }
                if (lObject != null) {
                    Collection<TypeUsage> types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, binaryNode.rhs());
                    if (lhs instanceof IndexNode && lObject instanceof JsArrayImpl) {
                        ((JsArrayImpl)lObject).addTypesInArray(types);
                    } else {
                        for (TypeUsage type : types) {
                            lObject.addAssignment(type, assignmentOffset);
                        }
                    }
                }
            }
            if (binaryNode.rhs() instanceof IdentNode) {
                if (fieldName == null) {
                    this.addOccurence((IdentNode)binaryNode.rhs(), false);
                } else {
                    this.addOccurrence((IdentNode)binaryNode.rhs(), fieldName);
                }
            }
        } else if (binaryNode.tokenType() != TokenType.ASSIGN || binaryNode.tokenType() == TokenType.ASSIGN && binaryNode.lhs() instanceof IndexNode) {
            if (binaryNode.lhs() instanceof IdentNode) {
                this.addOccurence((IdentNode)binaryNode.lhs(), binaryNode.tokenType() == TokenType.ASSIGN);
            }
            if (binaryNode.rhs() instanceof IdentNode) {
                this.addOccurence((IdentNode)binaryNode.rhs(), false);
            }
        }
        return super.enter(binaryNode);
    }

    @Override
    public Node enter(CallNode callNode) {
        this.functionArgumentStack.push(new ArrayList(3));
        if (callNode.getFunction() instanceof IdentNode) {
            IdentNode iNode = (IdentNode)callNode.getFunction();
            this.addOccurence(iNode, false, true);
        }
        for (Node argument : callNode.getArgs()) {
            if (!(argument instanceof IdentNode)) continue;
            this.addOccurence((IdentNode)argument, false);
        }
        return super.enter(callNode);
    }

    @Override
    public Node leave(CallNode callNode) {
        Collection<JsObjectImpl> functionArguments = this.functionArgumentStack.pop();
        Node function = callNode.getFunction();
        if (function instanceof AccessNode || function instanceof IdentNode) {
            List funcName;
            if (function instanceof AccessNode) {
                funcName = ModelVisitor.getName((AccessNode)function, this.parserResult);
            } else {
                funcName = new ArrayList<IdentifierImpl>();
                funcName.add((IdentifierImpl)new IdentifierImpl(((IdentNode)function).getName(), ((IdentNode)function).getStart()));
            }
            if (funcName != null) {
                StringBuilder sb = new StringBuilder();
                for (Identifier identifier : funcName) {
                    sb.append(identifier.getName());
                    sb.append(".");
                }
                if (this.functionCalls == null) {
                    this.functionCalls = new LinkedHashMap<FunctionInterceptor, Collection<FunctionCall>>();
                }
                String name = sb.substring(0, sb.length() - 1);
                FunctionInterceptor interceptorToUse = null;
                for (FunctionInterceptor interceptor : ModelExtender.getDefault().getFunctionInterceptors()) {
                    if (!interceptor.getNamePattern().matcher(name).matches()) continue;
                    interceptorToUse = interceptor;
                    break;
                }
                if (interceptorToUse != null) {
                    ArrayList<FunctionArgument> funcArg = new ArrayList<FunctionArgument>();
                    for (int i = 0; i < callNode.getArgs().size(); ++i) {
                        Node argument = (Node)callNode.getArgs().get(i);
                        this.createFunctionArgument(argument, i, functionArguments, funcArg);
                    }
                    Collection<FunctionCall> calls = this.functionCalls.get(interceptorToUse);
                    if (calls == null) {
                        calls = new ArrayList<FunctionCall>();
                        this.functionCalls.put(interceptorToUse, calls);
                    }
                    calls.add(new FunctionCall(name, this.modelBuilder.getCurrentDeclarationScope(), funcArg));
                }
            }
        }
        return super.leave(callNode);
    }

    private void createFunctionArgument(Node argument, int position, Collection<JsObjectImpl> functionArguments, Collection<FunctionArgument> result) {
        if (argument instanceof LiteralNode) {
            LiteralNode ln = (LiteralNode)argument;
            if (ln.isString()) {
                result.add(FunctionArgumentAccessor.getDefault().createForString(position, argument.getStart(), ln.getString()));
            } else if (ln instanceof LiteralNode.ArrayLiteralNode) {
                for (JsObjectImpl jsObject : functionArguments) {
                    if (jsObject.getOffset() != argument.getStart()) continue;
                    result.add(FunctionArgumentAccessor.getDefault().createForArray(position, jsObject.getOffset(), jsObject));
                    break;
                }
            }
        } else if (argument instanceof ObjectNode) {
            for (JsObjectImpl jsObject : functionArguments) {
                if (jsObject.getOffset() != argument.getStart()) continue;
                result.add(FunctionArgumentAccessor.getDefault().createForAnonymousObject(position, jsObject.getOffset(), jsObject));
                break;
            }
        } else if (argument instanceof AccessNode) {
            ArrayList<String> strFqn = new ArrayList<String>();
            if (this.fillName((AccessNode)argument, strFqn)) {
                result.add(FunctionArgumentAccessor.getDefault().createForReference(position, argument.getStart(), strFqn));
            } else {
                result.add(FunctionArgumentAccessor.getDefault().createForUnknown(position));
            }
        } else if (argument instanceof IndexNode) {
            ArrayList<String> strFqn = new ArrayList<String>();
            if (this.fillName((IndexNode)argument, strFqn)) {
                result.add(FunctionArgumentAccessor.getDefault().createForReference(position, argument.getStart(), strFqn));
            } else {
                result.add(FunctionArgumentAccessor.getDefault().createForUnknown(position));
            }
        } else if (argument instanceof IdentNode) {
            IdentNode in = (IdentNode)argument;
            String inName = in.getName();
            result.add(FunctionArgumentAccessor.getDefault().createForReference(position, argument.getStart(), Collections.singletonList(inName)));
        } else if (argument instanceof UnaryNode) {
            UnaryNode un = (UnaryNode)argument;
            if (un.tokenType() == TokenType.NEW) {
                CallNode constructor = (CallNode)un.rhs();
                this.createFunctionArgument(constructor.getFunction(), position, functionArguments, result);
            }
        } else if (argument instanceof ReferenceNode) {
            ReferenceNode reference = (ReferenceNode)argument;
            result.add(FunctionArgumentAccessor.getDefault().createForReference(position, argument.getStart(), Collections.singletonList(reference.getReference().getName())));
        } else {
            result.add(FunctionArgumentAccessor.getDefault().createForUnknown(position));
        }
    }

    @Override
    public Node enter(CatchNode catchNode) {
        IdentifierImpl exception = ModelElementFactory.create(this.parserResult, catchNode.getException());
        if (exception != null) {
            DeclarationScopeImpl inScope = this.modelBuilder.getCurrentDeclarationScope();
            CatchBlockImpl catchBlock = new CatchBlockImpl(inScope, exception, new OffsetRange(catchNode.getStart(), catchNode.getFinish()), this.parserResult.getSnapshot().getMimeType());
            inScope.addDeclaredScope(catchBlock);
            this.modelBuilder.setCurrentObject(catchBlock);
        }
        return super.enter(catchNode);
    }

    @Override
    public Node leave(CatchNode catchNode) {
        if (!JsEmbeddingProvider.containsGeneratedIdentifier(catchNode.getException().getName())) {
            this.modelBuilder.reset();
        }
        return super.leave(catchNode);
    }

    @Override
    public Node enter(IdentNode identNode) {
        Node previousVisited = this.getPath().get(this.getPath().size() - 1);
        if (!(previousVisited instanceof AccessNode || previousVisited instanceof VarNode || previousVisited instanceof BinaryNode || previousVisited instanceof PropertyNode || previousVisited instanceof CatchNode)) {
            this.addOccurence(identNode, false);
        }
        return super.enter(identNode);
    }

    @Override
    public Node leave(IndexNode indexNode) {
        if (indexNode.getIndex() instanceof LiteralNode) {
            IdentifierImpl parentName;
            IdentNode iNode;
            LiteralNode literal;
            Node base = indexNode.getBase();
            JsObjectImpl parent = null;
            if (base instanceof AccessNode) {
                parent = (JsObjectImpl)this.createJsObject((AccessNode)base, this.parserResult, this.modelBuilder);
            } else if (base instanceof IdentNode && !"this".equals((iNode = (IdentNode)base).getName()) && (parentName = ModelElementFactory.create(this.parserResult, iNode)) != null) {
                ArrayList<Identifier> fqName = new ArrayList<Identifier>();
                fqName.add(parentName);
                parent = ModelUtils.getJsObject(this.modelBuilder, fqName, false);
                parent.addOccurrence(parentName.getOffsetRange());
            }
            if (parent != null && indexNode.getIndex() instanceof LiteralNode && (literal = (LiteralNode)indexNode.getIndex()).isString()) {
                String index = literal.getPropertyName();
                JsObjectImpl property = (JsObjectImpl)parent.getProperty(index);
                if (property != null) {
                    property.addOccurrence(new OffsetRange(indexNode.getIndex().getStart(), indexNode.getIndex().getFinish()));
                } else {
                    IdentifierImpl name = ModelElementFactory.create(this.parserResult, (LiteralNode)indexNode.getIndex());
                    if (name != null) {
                        property = new JsObjectImpl(parent, name, name.getOffsetRange(), this.parserResult.getSnapshot().getMimeType(), null);
                        parent.addProperty(name.getName(), property);
                    }
                }
            }
        }
        return super.leave(indexNode);
    }

    @Override
    public Node enter(ForNode forNode) {
        if (forNode.getInit() instanceof IdentNode) {
            JsObject parent = this.modelBuilder.getCurrentObject();
            while (parent instanceof JsWith) {
                parent = parent.getParent();
            }
            IdentNode name = (IdentNode)forNode.getInit();
            JsObjectImpl variable = (JsObjectImpl)parent.getProperty(name.getName());
            if (variable != null) {
                Collection<TypeUsage> types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, forNode.getModify());
                Iterator<TypeUsage> i$ = types.iterator();
                while (i$.hasNext()) {
                    String newType;
                    int index;
                    TypeUsage type = i$.next();
                    if (type.getType().contains("@var;")) {
                        index = type.getType().lastIndexOf("@var;");
                        newType = type.getType().substring(0, index) + "@arr;" + type.getType().substring(index + "@var;".length());
                        type = new TypeUsageImpl(newType, type.getOffset(), false);
                    } else if (type.getType().contains("@pro;")) {
                        index = type.getType().lastIndexOf("@pro;");
                        newType = type.getType().substring(0, index) + "@arr;" + type.getType().substring(index + "@pro;".length());
                        type = new TypeUsageImpl(newType, type.getOffset(), false);
                    }
                    variable.addAssignment(type, forNode.getModify().getStart());
                }
            }
        }
        return super.enter(forNode);
    }

    @Override
    public Node enter(FunctionNode functionNode) {
        this.addToPath((Node)functionNode);
        ArrayList functions = new ArrayList(functionNode.getFunctions());
        List name = null;
        boolean isPrivate = false;
        boolean isStatic = false;
        boolean isPrivilage = false;
        int pathSize = this.getPath().size();
        if (pathSize > 1 && this.getPath().get(pathSize - 2) instanceof ReferenceNode) {
            List<FunctionNode> siblings = this.functionStack.get(this.functionStack.size() - 1);
            siblings.remove((Object)functionNode);
            if (pathSize > 3) {
                Node node = this.getPath().get(pathSize - 3);
                if (node instanceof PropertyNode) {
                    name = this.getName((PropertyNode)node);
                } else if (node instanceof BinaryNode) {
                    IdentNode iNode;
                    AccessNode aNode;
                    BinaryNode bNode = (BinaryNode)node;
                    if (bNode.lhs() instanceof AccessNode && (aNode = (AccessNode)bNode.lhs()).getBase() instanceof IdentNode && "this".equals((iNode = (IdentNode)aNode.getBase()).getName())) {
                        isPrivilage = true;
                    }
                    name = ModelVisitor.getName((BinaryNode)node, this.parserResult);
                } else if (node instanceof VarNode) {
                    name = ModelVisitor.getName((VarNode)node, this.parserResult);
                    boolean bl = isPrivate = this.functionStack.size() > 1;
                }
                if (name != null && !name.isEmpty() && !functionNode.isAnonymous()) {
                    String functionName;
                    JsObject originalFunction = null;
                    String string = functionName = functionNode.getIdent() != null ? functionNode.getIdent().getName() : functionNode.getName();
                    for (DeclarationScope currentScope = this.modelBuilder.getCurrentDeclarationScope(); originalFunction == null && currentScope != null; currentScope = currentScope.getParentScope()) {
                        originalFunction = ((JsObject)((Object)currentScope)).getProperty(functionName);
                    }
                    if (originalFunction != null) {
                        JsObjectImpl jsObject = ModelUtils.getJsObject(this.modelBuilder, name, true);
                        JsFunctionReference jsFunctionReference = new JsFunctionReference(jsObject.getParent(), jsObject.getDeclarationName(), (JsFunction)originalFunction, true, jsObject.getModifiers());
                        jsObject.getParent().addProperty(jsObject.getName(), jsFunctionReference);
                        return null;
                    }
                }
            }
        }
        JsObject previousUsage = null;
        if (name == null || name.isEmpty()) {
            name = new ArrayList<IdentifierImpl>(1);
            int start = functionNode.getIdent().getStart();
            int end = functionNode.getIdent().getFinish();
            if (end == 0) {
                end = this.parserResult.getSnapshot().getText().length();
            }
            if ((previousUsage = this.modelBuilder.getCurrentDeclarationScope().getProperty(functionNode.getIdent().getName())) != null && previousUsage.isDeclared() && previousUsage instanceof JsFunction) {
                return null;
            }
            String funcName = functionNode.isAnonymous() ? functionNode.getName() : functionNode.getIdent().getName();
            name.add((IdentifierImpl)new IdentifierImpl(funcName, new OffsetRange(start, end)));
            if (pathSize > 2 && this.getPath().get(pathSize - 2) instanceof FunctionNode) {
                isPrivate = true;
            }
        }
        this.functionStack.add(functions);
        JsFunctionImpl fncScope = this.modelBuilder.getCurrentDeclarationFunction();
        JsObject parent = null;
        if (functionNode.getKind() != FunctionNode.Kind.SCRIPT) {
            JsFunctionImpl scope = this.modelBuilder.getCurrentDeclarationFunction();
            boolean isAnonymous = false;
            if (this.getPreviousFromPath(2) instanceof ReferenceNode) {
                String methodName;
                Node node = this.getPreviousFromPath(3);
                if (node instanceof CallNode || node instanceof ExecuteNode || node instanceof LiteralNode.ArrayLiteralNode) {
                    isAnonymous = true;
                } else if (node instanceof AccessNode && this.getPreviousFromPath(4) instanceof CallNode && ("call".equals(methodName = ((AccessNode)node).getProperty().getName()) || "apply".equals(methodName))) {
                    isAnonymous = true;
                }
            }
            if (this.canBeSingletonPattern()) {
                parent = this.resolveThis(fncScope);
            }
            if ("this".equals(((Identifier)name.get(0)).getName())) {
                name.remove(0);
            }
            if (!name.isEmpty()) {
                fncScope = ModelElementFactory.create(this.parserResult, functionNode, name, this.modelBuilder, isAnonymous, parent);
                if (fncScope != null) {
                    Set<Modifier> modifiers = fncScope.getModifiers();
                    if (isPrivate || isPrivilage) {
                        modifiers.remove((Object)Modifier.PUBLIC);
                        if (isPrivate) {
                            modifiers.add(Modifier.PRIVATE);
                        } else {
                            modifiers.add(Modifier.PROTECTED);
                        }
                    }
                    if (isStatic) {
                        modifiers.add(Modifier.STATIC);
                    }
                    scope.addDeclaredScope(fncScope);
                    this.modelBuilder.setCurrentObject(fncScope);
                }
                if (previousUsage != null) {
                    for (Occurrence occurrence : previousUsage.getOccurrences()) {
                        fncScope.addOccurrence(occurrence.getOffsetRange());
                    }
                    ArrayList<? extends JsObject> propertiesCopy = new ArrayList<JsObject>(previousUsage.getProperties().values());
                    for (JsObject property : propertiesCopy) {
                        ModelUtils.moveProperty(fncScope, property);
                    }
                }
            }
        }
        for (FunctionNode cFunction : functionNode.getFunctions()) {
            if (!cFunction.isAnonymous()) continue;
            cFunction.setName(this.scriptName + cFunction.getIdent().getName());
        }
        if (fncScope != null) {
            Node lastNode;
            JsDocumentationHolder docHolder = this.parserResult.getDocumentationHolder();
            parent = this.canBeSingletonPattern() ? ((lastNode = this.getPreviousFromPath(1)) instanceof FunctionNode && !this.canBeSingletonPattern(1) ? fncScope : this.resolveThis(fncScope)) : fncScope;
            if (parent == null) {
                parent = fncScope;
            }
            for (VarNode varNode : functionNode.getDeclarations()) {
                IdentifierImpl varName = new IdentifierImpl(varNode.getName().getName(), new OffsetRange(varNode.getName().getStart(), varNode.getName().getFinish()));
                OffsetRange range = varNode.getInit() instanceof ObjectNode ? new OffsetRange(varNode.getName().getStart(), ((ObjectNode)varNode.getInit()).getFinish()) : varName.getOffsetRange();
                JsObject variable = this.handleArrayCreation(varNode.getInit(), parent, varName);
                if (variable == null) {
                    JsObjectImpl newObject = new JsObjectImpl(parent, varName, range, this.parserResult.getSnapshot().getMimeType(), null);
                    newObject.setDeclared(true);
                    if (functionNode.getKind() != FunctionNode.Kind.SCRIPT) {
                        newObject.getModifiers().remove((Object)Modifier.PUBLIC);
                        newObject.getModifiers().add(Modifier.PRIVATE);
                    }
                    variable = newObject;
                }
                variable.addOccurrence(varName.getOffsetRange());
                parent.addProperty(varName.getName(), variable);
                if (docHolder == null) continue;
                ((JsObjectImpl)variable).setDocumentation(docHolder.getDocumentation((Node)varNode));
                ((JsObjectImpl)variable).setDeprecated(docHolder.isDeprecated((Node)varNode));
            }
            ArrayList copy = new ArrayList(functions);
            for (FunctionNode fn : copy) {
                String functionName;
                if (fn.getIdent().getStart() >= fn.getIdent().getFinish() || (functionName = fn.getIdent().getName()).startsWith("get ") || functionName.startsWith("set ")) continue;
                fn.accept((NodeVisitor)this);
            }
            if (functionNode.getKind() != FunctionNode.Kind.SCRIPT && docHolder.isClass((Node)functionNode)) {
                fncScope.setJsKind(JsElement.Kind.CONSTRUCTOR);
            }
            for (Node node : functionNode.getStatements()) {
                node.accept((NodeVisitor)this);
            }
            fncScope.setDeprecated(docHolder.isDeprecated((Node)functionNode));
            List<Type> types = docHolder.getReturnType((Node)functionNode);
            if (types != null && !types.isEmpty()) {
                for (Type type : types) {
                    fncScope.addReturnType(new TypeUsageImpl(type.getType(), type.getOffset(), ModelUtils.isKnownGLobalType(type.getType())));
                }
            }
            if (fncScope.areReturnTypesEmpty()) {
                fncScope.addReturnType(new TypeUsageImpl("undefined", -1, false));
            }
            List<DocParameter> docParams = docHolder.getParameters((Node)functionNode);
            for (DocParameter docParameter : docParams) {
                JsObjectImpl param;
                String sParamName;
                Identifier paramName = docParameter.getParamName();
                if (paramName == null || (sParamName = paramName.getName()) == null || sParamName.isEmpty() || (param = (JsObjectImpl)fncScope.getParameter(sParamName)) == null) continue;
                for (Type type : docParameter.getParamTypes()) {
                    param.addAssignment(new TypeUsageImpl(type.getType(), type.getOffset(), true), param.getOffset());
                }
                this.addDocNameOccurence(param);
            }
            List<Type> extendTypes = docHolder.getExtends((Node)functionNode);
            if (!extendTypes.isEmpty()) {
                JsObject prototype = fncScope.getProperty("prototype");
                if (prototype == null) {
                    prototype = new JsObjectImpl((JsObject)fncScope, "prototype", true, OffsetRange.NONE, EnumSet.of(Modifier.PUBLIC), this.parserResult.getSnapshot().getMimeType(), null);
                    fncScope.addProperty("prototype", prototype);
                }
                for (Type type : extendTypes) {
                    prototype.addAssignment(new TypeUsageImpl(type.getType(), type.getOffset(), true), type.getOffset());
                }
            }
            this.setModifiersFromDoc(fncScope, docHolder.getModifiers((Node)functionNode));
            for (FunctionNode fn2 : functions) {
                if (fn2.getIdent().getStart() < fn2.getIdent().getFinish()) continue;
                fn2.accept((NodeVisitor)this);
            }
        }
        if (fncScope != null && functionNode.getKind() != FunctionNode.Kind.SCRIPT) {
            this.modelBuilder.reset();
        }
        this.functionStack.remove(this.functionStack.size() - 1);
        this.removeFromPathTheLast();
        return null;
    }

    private JsArray handleArrayCreation(Node initNode, JsObject parent, Identifier name) {
        CallNode cNode;
        UnaryNode uNode;
        if (initNode instanceof UnaryNode && parent != null && (uNode = (UnaryNode)initNode).tokenType() == TokenType.NEW && uNode.rhs() instanceof CallNode && (cNode = (CallNode)uNode.rhs()).getFunction() instanceof IdentNode && "Array".equals(((IdentNode)cNode.getFunction()).getName())) {
            ArrayList<TypeUsage> itemTypes = new ArrayList<TypeUsage>();
            for (Node node : cNode.getArgs()) {
                itemTypes.addAll(ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, node));
            }
            EnumSet<Modifier> modifiers = parent.getJSKind() != JsElement.Kind.FILE ? EnumSet.of(Modifier.PRIVATE) : EnumSet.of(Modifier.PUBLIC);
            JsArrayImpl result = new JsArrayImpl(parent, name, name.getOffsetRange(), true, modifiers, this.parserResult.getSnapshot().getMimeType(), null);
            result.addTypesInArray(itemTypes);
            return result;
        }
        return null;
    }

    @Override
    public Node enter(LiteralNode lNode) {
        Node lastVisited = this.getPreviousFromPath(1);
        if (lNode instanceof LiteralNode.ArrayLiteralNode) {
            JsArrayImpl array;
            LiteralNode.ArrayLiteralNode aNode = (LiteralNode.ArrayLiteralNode)lNode;
            List<Identifier> fqName = null;
            int pathSize = this.getPath().size();
            boolean isDeclaredInParent = false;
            boolean isPrivate = false;
            boolean treatAsAnonymous = false;
            JsObject parent = null;
            if (lastVisited instanceof TernaryNode && pathSize > 1) {
                lastVisited = this.getPath().get(pathSize - 2);
            }
            int pathIndex = 1;
            while (lastVisited instanceof BinaryNode && pathSize > pathIndex && ((BinaryNode)lastVisited).tokenType() != TokenType.ASSIGN) {
                lastVisited = this.getPath().get(pathSize - ++pathIndex);
            }
            if (lastVisited instanceof VarNode) {
                JsFunctionImpl declarationScope;
                fqName = ModelVisitor.getName((VarNode)lastVisited, this.parserResult);
                isDeclaredInParent = true;
                parent = declarationScope = this.modelBuilder.getCurrentDeclarationFunction();
                if (fqName.size() == 1 && !ModelUtils.isGlobal(declarationScope)) {
                    isPrivate = true;
                }
            } else if (lastVisited instanceof PropertyNode) {
                fqName = this.getName((PropertyNode)lastVisited);
                isDeclaredInParent = true;
            } else if (lastVisited instanceof BinaryNode) {
                Node index;
                BinaryNode binNode = (BinaryNode)lastVisited;
                if (!(!(binNode.lhs() instanceof IndexNode) || (index = ((IndexNode)binNode.lhs()).getIndex()) instanceof LiteralNode && ((LiteralNode)index).isString())) {
                    treatAsAnonymous = true;
                }
                if (!treatAsAnonymous) {
                    if (this.getPath().size() > 1) {
                        lastVisited = this.getPath().get(this.getPath().size() - pathIndex - 1);
                    }
                    fqName = ModelVisitor.getName(binNode, this.parserResult);
                    if (binNode.lhs() instanceof IdentNode || binNode.lhs() instanceof AccessNode && ((AccessNode)binNode.lhs()).getBase() instanceof IdentNode && ((IdentNode)((AccessNode)binNode.lhs()).getBase()).getName().equals("this")) {
                        if (lastVisited instanceof ExecuteNode && !fqName.get(0).getName().equals("this")) {
                            List<Identifier> objectName = fqName.size() > 1 ? fqName.subList(0, fqName.size() - 1) : fqName;
                            JsObjectImpl existingArray = ModelUtils.getJsObject(this.modelBuilder, objectName, false);
                            if (existingArray != null) {
                                existingArray.addOccurrence(fqName.get(fqName.size() - 1).getOffsetRange());
                                return super.enter(lNode);
                            }
                        } else {
                            isDeclaredInParent = true;
                            if (!(binNode.lhs() instanceof IdentNode)) {
                                parent = this.resolveThis(this.modelBuilder.getCurrentObject());
                            }
                        }
                    }
                }
            } else if (lastVisited instanceof CallNode || lastVisited instanceof LiteralNode.ArrayLiteralNode || lastVisited instanceof ReturnNode || lastVisited instanceof AccessNode) {
                treatAsAnonymous = true;
            }
            if (!isDeclaredInParent && lastVisited instanceof FunctionNode) {
                boolean bl = isDeclaredInParent = ((FunctionNode)lastVisited).getKind() == FunctionNode.Kind.SCRIPT;
            }
            if (!treatAsAnonymous) {
                if (fqName == null || fqName.isEmpty()) {
                    fqName = new ArrayList<Identifier>(1);
                    fqName.add(new IdentifierImpl("UNKNOWN", new OffsetRange(lNode.getStart(), lNode.getFinish())));
                }
                if ((array = ModelElementFactory.create(this.parserResult, aNode, fqName, this.modelBuilder, isDeclaredInParent, parent)) != null && isPrivate) {
                    array.getModifiers().remove((Object)Modifier.PUBLIC);
                    array.getModifiers().add(Modifier.PRIVATE);
                }
            } else {
                array = ModelElementFactory.createAnonymousObject(this.parserResult, aNode, this.modelBuilder);
            }
            if (array != null) {
                int aOffset = fqName == null ? lastVisited.getStart() : fqName.get(fqName.size() - 1).getOffsetRange().getEnd();
                array.addAssignment(ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, (Node)lNode), aOffset);
                for (Node item : aNode.getArray()) {
                    array.addTypesInArray(ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, item));
                }
                if (!this.functionArgumentStack.isEmpty()) {
                    this.functionArgumentStack.peek().add(array);
                }
            }
        }
        return super.enter(lNode);
    }

    @Override
    public Node enter(ObjectNode objectNode) {
        Node previousVisited = this.getPath().get(this.getPath().size() - 1);
        if (previousVisited instanceof CallNode || previousVisited instanceof LiteralNode.ArrayLiteralNode) {
            JsObjectImpl object = ModelElementFactory.createAnonymousObject(this.parserResult, objectNode, this.modelBuilder);
            this.modelBuilder.setCurrentObject(object);
            object.setJsKind(JsElement.Kind.OBJECT_LITERAL);
            if (!this.functionArgumentStack.isEmpty()) {
                this.functionArgumentStack.peek().add(object);
            }
            return super.enter(objectNode);
        }
        if (previousVisited instanceof ReturnNode || previousVisited instanceof BinaryNode && ((BinaryNode)previousVisited).tokenType() == TokenType.COMMARIGHT) {
            JsObjectImpl objectScope = ModelElementFactory.createAnonymousObject(this.parserResult, objectNode, this.modelBuilder);
            this.modelBuilder.setCurrentObject(objectScope);
            objectScope.setJsKind(JsElement.Kind.OBJECT_LITERAL);
        } else {
            JsObjectImpl objectScope;
            List<Identifier> fqName = null;
            int pathSize = this.getPath().size();
            boolean isDeclaredInParent = false;
            boolean isDeclaredThroughThis = false;
            boolean isPrivate = false;
            boolean treatAsAnonymous = false;
            Node lastVisited = this.getPath().get(pathSize - 1);
            VarNode varNode = null;
            if (lastVisited instanceof TernaryNode && pathSize > 1) {
                lastVisited = this.getPath().get(pathSize - 2);
            }
            int pathIndex = 1;
            while (lastVisited instanceof BinaryNode && pathSize > pathIndex && ((BinaryNode)lastVisited).tokenType() != TokenType.ASSIGN) {
                lastVisited = this.getPath().get(pathSize - ++pathIndex);
            }
            if (lastVisited instanceof VarNode) {
                fqName = ModelVisitor.getName((VarNode)lastVisited, this.parserResult);
                isDeclaredInParent = true;
                JsFunctionImpl declarationScope = this.modelBuilder.getCurrentDeclarationFunction();
                varNode = (VarNode)lastVisited;
                if (fqName.size() == 1 && !ModelUtils.isGlobal(declarationScope)) {
                    isPrivate = true;
                }
            } else if (lastVisited instanceof PropertyNode) {
                fqName = this.getName((PropertyNode)lastVisited);
                isDeclaredInParent = true;
            } else if (lastVisited instanceof ExecuteNode || lastVisited instanceof AccessNode) {
                treatAsAnonymous = true;
            } else if (lastVisited instanceof BinaryNode) {
                Node index;
                BinaryNode binNode = (BinaryNode)lastVisited;
                Node binLhs = binNode.lhs();
                if (!(!(binLhs instanceof IndexNode) || (index = ((IndexNode)binLhs).getIndex()) instanceof LiteralNode && ((LiteralNode)index).isString())) {
                    treatAsAnonymous = true;
                }
                if (!treatAsAnonymous) {
                    if (this.getPath().size() > 1 && (lastVisited = this.getPath().get(this.getPath().size() - pathIndex - 1)) instanceof VarNode) {
                        varNode = (VarNode)lastVisited;
                    }
                    fqName = ModelVisitor.getName(binNode, this.parserResult);
                    if (binLhs instanceof IdentNode || binLhs instanceof AccessNode && ((AccessNode)binLhs).getBase() instanceof IdentNode && ((IdentNode)((AccessNode)binLhs).getBase()).getName().equals("this")) {
                        boolean bl = isDeclaredInParent = binLhs instanceof IdentNode && varNode != null;
                        if (binLhs instanceof AccessNode) {
                            isDeclaredInParent = true;
                            isDeclaredThroughThis = true;
                        }
                    }
                }
            }
            if (!isDeclaredInParent && lastVisited instanceof FunctionNode) {
                boolean bl = isDeclaredInParent = ((FunctionNode)lastVisited).getKind() == FunctionNode.Kind.SCRIPT;
            }
            if (!treatAsAnonymous) {
                if (fqName == null || fqName.isEmpty()) {
                    fqName = new ArrayList<Identifier>(1);
                    fqName.add(new IdentifierImpl("UNKNOWN", new OffsetRange(objectNode.getStart(), objectNode.getFinish())));
                }
                if (varNode != null) {
                    objectScope = this.modelBuilder.getCurrentObject();
                } else {
                    Identifier name = fqName.get(fqName.size() - 1);
                    JsObject alreadyThere = null;
                    if (isDeclaredThroughThis) {
                        JsObject thisIs = this.resolveThis(this.modelBuilder.getCurrentObject());
                        alreadyThere = thisIs.getProperty(name.getName());
                    } else if (isDeclaredInParent) {
                        alreadyThere = lastVisited instanceof PropertyNode ? this.modelBuilder.getCurrentObject().getProperty(name.getName()) : ModelUtils.getJsObjectByName(this.modelBuilder.getCurrentDeclarationFunction(), name.getName());
                    } else {
                        if (fqName.size() == 1) {
                            Collection<? extends JsObject> variables = ModelUtils.getVariables(this.modelBuilder.getCurrentDeclarationScope());
                            for (JsObject variable : variables) {
                                if (!variable.getName().equals(name.getName())) continue;
                                alreadyThere = variable;
                                break;
                            }
                        }
                        if (alreadyThere == null) {
                            alreadyThere = ModelUtils.getJsObject(this.modelBuilder, fqName, true);
                        }
                    }
                    JsObjectImpl jsObjectImpl = objectScope = alreadyThere == null ? ModelElementFactory.create(this.parserResult, objectNode, fqName, this.modelBuilder, isDeclaredInParent) : (JsObjectImpl)alreadyThere;
                    if (alreadyThere != null) {
                        ((JsObjectImpl)alreadyThere).addOccurrence(name.getOffsetRange());
                    }
                }
                if (objectScope != null) {
                    objectScope.setJsKind(JsElement.Kind.OBJECT_LITERAL);
                    if (!objectScope.isDeclared()) {
                        objectScope.setDeclared(true);
                    }
                    this.modelBuilder.setCurrentObject(objectScope);
                    if (isPrivate) {
                        objectScope.getModifiers().remove((Object)Modifier.PUBLIC);
                        objectScope.getModifiers().add(Modifier.PRIVATE);
                    }
                }
            } else {
                objectScope = ModelElementFactory.createAnonymousObject(this.parserResult, objectNode, this.modelBuilder);
                this.modelBuilder.setCurrentObject(objectScope);
            }
        }
        return super.enter(objectNode);
    }

    @Override
    public Node leave(ObjectNode objectNode) {
        this.modelBuilder.reset();
        return super.leave(objectNode);
    }

    @Override
    public Node enter(PropertyNode propertyNode) {
        if ((propertyNode.getKey() instanceof IdentNode || propertyNode.getKey() instanceof LiteralNode) && !(propertyNode.getValue() instanceof ObjectNode)) {
            JsObjectImpl scope = this.modelBuilder.getCurrentObject();
            IdentifierImpl name = null;
            Node key = propertyNode.getKey();
            if (key instanceof IdentNode) {
                name = ModelElementFactory.create(this.parserResult, (IdentNode)key);
            } else if (key instanceof LiteralNode) {
                name = ModelElementFactory.create(this.parserResult, (LiteralNode)key);
            }
            if (name != null) {
                JsObjectImpl property = (JsObjectImpl)scope.getProperty(name.getName());
                if (property == null) {
                    property = ModelElementFactory.create(this.parserResult, propertyNode, name, this.modelBuilder, true);
                } else {
                    JsObjectImpl newProperty = ModelElementFactory.create(this.parserResult, propertyNode, name, this.modelBuilder, true);
                    if (newProperty != null) {
                        newProperty.addOccurrence(property.getDeclarationName().getOffsetRange());
                        for (Occurrence occurrence : property.getOccurrences()) {
                            newProperty.addOccurrence(occurrence.getOffsetRange());
                        }
                        property = newProperty;
                    }
                }
                if (property != null) {
                    if (propertyNode.getGetter() != null) {
                        FunctionNode getter = ((ReferenceNode)propertyNode.getGetter()).getReference();
                        property.addOccurrence(new OffsetRange(getter.getIdent().getStart(), getter.getIdent().getFinish()));
                    }
                    if (propertyNode.getSetter() != null) {
                        FunctionNode setter = ((ReferenceNode)propertyNode.getSetter()).getReference();
                        property.addOccurrence(new OffsetRange(setter.getIdent().getStart(), setter.getIdent().getFinish()));
                    }
                    scope.addProperty(name.getName(), property);
                    property.setDeclared(true);
                    Node value = propertyNode.getValue();
                    if (!(value instanceof CallNode)) {
                        Collection<TypeUsage> types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, value);
                        if (!types.isEmpty()) {
                            property.addAssignment(types, name.getOffsetRange().getStart());
                        }
                        if (value instanceof IdentNode) {
                            IdentNode iNode = (IdentNode)value;
                            if (!iNode.getPropertyName().equals(name.getName())) {
                                this.addOccurence((IdentNode)value, false);
                            } else if (this.modelBuilder.getCurrentObject().getParent() != null) {
                                this.occurrenceBuilder.addOccurrence(name.getName(), new OffsetRange(iNode.getStart(), iNode.getFinish()), this.modelBuilder.getCurrentDeclarationScope(), this.modelBuilder.getCurrentObject().getParent(), this.modelBuilder.getCurrentWith(), false, false);
                            }
                        }
                    }
                }
            }
        }
        return super.enter(propertyNode);
    }

    @Override
    public Node enter(ReferenceNode referenceNode) {
        FunctionNode reference = referenceNode.getReference();
        if (reference != null) {
            Node lastNode = this.getPreviousFromPath(1);
            if (!(lastNode instanceof VarNode) || reference.isAnonymous()) {
                this.addToPath((Node)referenceNode);
                reference.accept((NodeVisitor)this);
                this.removeFromPathTheLast();
            }
            return null;
        }
        return super.enter(referenceNode);
    }

    @Override
    public Node enter(ReturnNode returnNode) {
        Node expression = returnNode.getExpression();
        Collection<TypeUsage> types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, expression);
        if (expression == null) {
            types.add(new TypeUsageImpl("undefined", returnNode.getStart(), true));
        } else {
            if (expression instanceof IdentNode) {
                this.addOccurence((IdentNode)expression, false);
            }
            if (types.isEmpty()) {
                types.add(new TypeUsageImpl("unresolved", returnNode.getStart(), true));
            }
        }
        JsFunctionImpl function = this.modelBuilder.getCurrentDeclarationFunction();
        function.addReturnType(types);
        return super.enter(returnNode);
    }

    @Override
    public Node enter(TernaryNode ternaryNode) {
        if (ternaryNode.lhs() instanceof IdentNode) {
            this.addOccurence((IdentNode)ternaryNode.lhs(), false);
        }
        if (ternaryNode.rhs() instanceof IdentNode) {
            this.addOccurence((IdentNode)ternaryNode.rhs(), false);
        }
        if (ternaryNode.third() instanceof IdentNode) {
            this.addOccurence((IdentNode)ternaryNode.third(), false);
        }
        return super.enter(ternaryNode);
    }

    @Override
    public Node enter(UnaryNode unaryNode) {
        if (unaryNode.rhs() instanceof IdentNode) {
            this.addOccurence((IdentNode)unaryNode.rhs(), false);
        }
        return super.enter(unaryNode);
    }

    @Override
    public Node enter(VarNode varNode) {
        FunctionNode fnode;
        Node init = varNode.getInit();
        ReferenceNode rNode = null;
        if (init instanceof ReferenceNode) {
            rNode = (ReferenceNode)init;
        } else if (init instanceof BinaryNode) {
            BinaryNode bNode = (BinaryNode)init;
            while (bNode.rhs() instanceof BinaryNode) {
                bNode = (BinaryNode)bNode.rhs();
            }
            if (bNode.rhs() instanceof ReferenceNode) {
                rNode = (ReferenceNode)bNode.rhs();
            }
        }
        if (!(init instanceof ObjectNode) && rNode == null && !(init instanceof LiteralNode.ArrayLiteralNode)) {
            JsObject parent = this.modelBuilder.getCurrentObject();
            JsObject jsObject = parent = this.canBeSingletonPattern(1) ? this.resolveThis(parent) : parent;
            if (parent instanceof CatchBlockImpl) {
                parent = parent.getParent();
            }
            while (parent instanceof JsWith) {
                parent = parent.getParent();
            }
            JsObjectImpl variable = (JsObjectImpl)parent.getProperty(varNode.getName().getName());
            IdentifierImpl name = ModelElementFactory.create(this.parserResult, varNode.getName());
            if (name != null) {
                if (variable == null) {
                    variable = new JsObjectImpl(parent, name, name.getOffsetRange(), true, this.parserResult.getSnapshot().getMimeType(), null);
                    if (parent.getJSKind() != JsElement.Kind.FILE) {
                        variable.getModifiers().remove((Object)Modifier.PUBLIC);
                        variable.getModifiers().add(Modifier.PRIVATE);
                    }
                    parent.addProperty(name.getName(), variable);
                    variable.addOccurrence(name.getOffsetRange());
                } else if (!variable.isDeclared()) {
                    JsObjectImpl newVariable = new JsObjectImpl(parent, name, name.getOffsetRange(), true, this.parserResult.getSnapshot().getMimeType(), null);
                    newVariable.addOccurrence(name.getOffsetRange());
                    for (String propertyName : variable.getProperties().keySet()) {
                        JsObject property = variable.getProperty(propertyName);
                        if (property instanceof JsObjectImpl) {
                            ((JsObjectImpl)property).setParent(newVariable);
                        }
                        newVariable.addProperty(propertyName, property);
                    }
                    if (parent.getJSKind() != JsElement.Kind.FILE) {
                        newVariable.getModifiers().remove((Object)Modifier.PUBLIC);
                        newVariable.getModifiers().add(Modifier.PRIVATE);
                    }
                    for (TypeUsage type : variable.getAssignments()) {
                        newVariable.addAssignment(type, type.getOffset());
                    }
                    for (Occurrence occurrence : variable.getOccurrences()) {
                        newVariable.addOccurrence(occurrence.getOffsetRange());
                    }
                    parent.addProperty(name.getName(), newVariable);
                    variable = newVariable;
                }
                JsDocumentationHolder docHolder = this.parserResult.getDocumentationHolder();
                variable.setDeprecated(docHolder.isDeprecated((Node)varNode));
                variable.setDocumentation(docHolder.getDocumentation((Node)varNode));
                if (init instanceof IdentNode) {
                    this.addOccurrence((IdentNode)init, variable.getName());
                }
                this.modelBuilder.setCurrentObject(variable);
                Collection<TypeUsage> types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, init);
                if (this.modelBuilder.getCurrentWith() != null) {
                    ((JsWithObjectImpl)this.modelBuilder.getCurrentWith()).addObjectWithAssignment(variable);
                }
                for (TypeUsage type : types) {
                    variable.addAssignment(type, varNode.getName().getFinish());
                }
                List<Type> returnTypes = docHolder.getReturnType((Node)varNode);
                if (returnTypes != null && !returnTypes.isEmpty()) {
                    for (Type type2 : returnTypes) {
                        variable.addAssignment(new TypeUsageImpl(type2.getType(), type2.getOffset(), true), varNode.getName().getFinish());
                    }
                }
            }
        } else if (init instanceof ObjectNode) {
            JsFunctionImpl function = this.modelBuilder.getCurrentDeclarationFunction();
            IdentifierImpl name = ModelElementFactory.create(this.parserResult, varNode.getName());
            if (name != null) {
                JsObjectImpl variable = (JsObjectImpl)function.getProperty(name.getName());
                if (variable != null) {
                    variable.setDeclared(true);
                } else {
                    List<Identifier> fqName = ModelVisitor.getName(varNode, this.parserResult);
                    variable = ModelElementFactory.create(this.parserResult, (ObjectNode)varNode.getInit(), fqName, this.modelBuilder, true);
                }
                if (variable != null) {
                    variable.setJsKind(JsElement.Kind.OBJECT_LITERAL);
                    this.modelBuilder.setCurrentObject(variable);
                }
            }
        } else if (rNode != null && rNode.getReference() != null && rNode.getReference() instanceof FunctionNode && !(fnode = rNode.getReference()).isAnonymous()) {
            JsFunctionImpl function = this.modelBuilder.getCurrentDeclarationFunction();
            JsObject origFunction = function.getProperty(fnode.getName());
            IdentifierImpl name = ModelElementFactory.create(this.parserResult, varNode.getName());
            if (name != null && origFunction != null && origFunction instanceof JsFunction) {
                JsObjectImpl oldVariable = (JsObjectImpl)function.getProperty(name.getName());
                JsFunctionReference variable = new JsFunctionReference(function, name, (JsFunction)origFunction, true, oldVariable != null ? oldVariable.getModifiers() : null);
                function.addProperty(variable.getName(), variable);
            }
        }
        return super.enter(varNode);
    }

    @Override
    public Node leave(VarNode varNode) {
        if (!(varNode.getInit() instanceof ReferenceNode) && !(varNode.getInit() instanceof LiteralNode.ArrayLiteralNode) && ModelElementFactory.create(this.parserResult, varNode.getName()) != null) {
            this.modelBuilder.reset();
        }
        return super.leave(varNode);
    }

    @Override
    public Node enter(WithNode withNode) {
        JsObjectImpl currentObject = this.modelBuilder.getCurrentObject();
        Collection<TypeUsage> types = ModelUtils.resolveSemiTypeOfExpression(this.modelBuilder, withNode.getExpression());
        JsWithObjectImpl withObject = new JsWithObjectImpl(currentObject, this.modelBuilder.getUnigueNameForWithObject(), types, new OffsetRange(withNode.getStart(), withNode.getFinish()), new OffsetRange(withNode.getExpression().getStart(), withNode.getExpression().getFinish()), this.modelBuilder.getCurrentWith(), this.parserResult.getSnapshot().getMimeType(), null);
        currentObject.addProperty(withObject.getName(), withObject);
        this.modelBuilder.setCurrentObject(withObject);
        withNode.getBody().accept((NodeVisitor)this);
        this.modelBuilder.reset();
        return null;
    }

    public Map<FunctionInterceptor, Collection<FunctionCall>> getCallsForProcessing() {
        return this.functionCalls;
    }

    private boolean fillName(AccessNode node, List<String> result) {
        List<Identifier> fqn = ModelVisitor.getName(node, this.parserResult);
        if (fqn != null) {
            for (int i = fqn.size() - 1; i >= 0; --i) {
                result.add(0, fqn.get(i).getName());
            }
        }
        for (JsObject current = this.modelBuilder.getCurrentObject(); current != null && current.getDeclarationName() != null; current = current.getParent()) {
            if (current == this.modelBuilder.getGlobal()) continue;
            result.add(0, current.getDeclarationName().getName());
        }
        return true;
    }

    private boolean fillName(IndexNode node, List<String> result) {
        LiteralNode literal;
        Node index = node.getIndex();
        Node base = node.getBase();
        if (index instanceof LiteralNode && base instanceof AccessNode && (literal = (LiteralNode)index).isString()) {
            result.add(0, literal.getString());
            List<Identifier> fqn = ModelVisitor.getName((AccessNode)base, this.parserResult);
            for (int i = fqn.size() - 1; i >= 0; --i) {
                result.add(0, fqn.get(i).getName());
            }
            return true;
        }
        return false;
    }

    private List<Identifier> getName(PropertyNode propertyNode) {
        String fName;
        FunctionNode fNode;
        Node previousNode;
        ArrayList<Identifier> name = new ArrayList<Identifier>(1);
        if ((propertyNode.getGetter() != null || propertyNode.getSetter() != null) && (previousNode = this.getPreviousFromPath(1)) instanceof FunctionNode && ((fName = (fNode = (FunctionNode)previousNode).getIdent().getName()).startsWith("get ") || fName.startsWith("set "))) {
            name.add(new IdentifierImpl(fName, new OffsetRange(fNode.getIdent().getStart(), fNode.getIdent().getFinish())));
            return name;
        }
        return ModelVisitor.getName(propertyNode, this.parserResult);
    }

    private static List<Identifier> getName(PropertyNode propertyNode, JsParserResult parserResult) {
        ArrayList<Identifier> name = new ArrayList<Identifier>(1);
        if (propertyNode.getKey() instanceof IdentNode) {
            IdentNode ident = (IdentNode)propertyNode.getKey();
            name.add(new IdentifierImpl(ident.getName(), new OffsetRange(ident.getStart(), ident.getFinish())));
        } else if (propertyNode.getKey() instanceof LiteralNode) {
            LiteralNode lNode = (LiteralNode)propertyNode.getKey();
            name.add(new IdentifierImpl(lNode.getString(), new OffsetRange(lNode.getStart(), lNode.getFinish())));
        }
        return name;
    }

    private static List<Identifier> getName(VarNode varNode, JsParserResult parserResult) {
        ArrayList<Identifier> name = new ArrayList<Identifier>();
        name.add(new IdentifierImpl(varNode.getName().getName(), new OffsetRange(varNode.getName().getStart(), varNode.getName().getFinish())));
        return name;
    }

    private static List<Identifier> getName(BinaryNode binaryNode, JsParserResult parserResult) {
        ArrayList<Identifier> name = new ArrayList();
        Node lhs = binaryNode.lhs();
        if (lhs instanceof AccessNode) {
            name = ModelVisitor.getName((AccessNode)lhs, parserResult);
        } else if (lhs instanceof IdentNode) {
            IdentNode ident = (IdentNode)lhs;
            name.add(new IdentifierImpl(ident.getName(), new OffsetRange(ident.getStart(), ident.getFinish())));
        } else if (lhs instanceof IndexNode) {
            IndexNode indexNode = (IndexNode)lhs;
            if (indexNode.getBase() instanceof AccessNode) {
                List<Identifier> aName = ModelVisitor.getName((AccessNode)indexNode.getBase(), parserResult);
                if (aName != null) {
                    name.addAll(ModelVisitor.getName((AccessNode)indexNode.getBase(), parserResult));
                } else {
                    return null;
                }
            }
            if (indexNode.getIndex() instanceof LiteralNode) {
                LiteralNode lNode = (LiteralNode)indexNode.getIndex();
                name.add(new IdentifierImpl(lNode.getPropertyName(), new OffsetRange(lNode.getStart(), lNode.getFinish())));
            }
        }
        return name;
    }

    private static List<Identifier> getName(AccessNode aNode, JsParserResult parserResult) {
        ArrayList<Identifier> name = new ArrayList<Identifier>();
        name.add(new IdentifierImpl(aNode.getProperty().getName(), new OffsetRange(aNode.getProperty().getStart(), aNode.getProperty().getFinish())));
        Node base = aNode.getBase();
        while (base instanceof AccessNode || base instanceof CallNode || base instanceof IndexNode) {
            if (base instanceof CallNode) {
                CallNode cNode = (CallNode)base;
                base = cNode.getFunction();
            } else if (base instanceof IndexNode) {
                IndexNode iNode = (IndexNode)base;
                if (iNode.getIndex() instanceof LiteralNode) {
                    LiteralNode lNode = (LiteralNode)iNode.getIndex();
                    if (lNode.isString()) {
                        name.add(new IdentifierImpl(lNode.getPropertyName(), new OffsetRange(lNode.getStart(), lNode.getFinish())));
                    }
                } else {
                    return null;
                }
                base = iNode.getBase();
            }
            if (!(base instanceof AccessNode)) continue;
            AccessNode aaNode = (AccessNode)base;
            base = aaNode.getBase();
            name.add(new IdentifierImpl(aaNode.getProperty().getName(), new OffsetRange(aaNode.getProperty().getStart(), aaNode.getProperty().getFinish())));
        }
        if (base instanceof IdentNode) {
            if (name.size() > 0) {
                IdentNode ident = (IdentNode)base;
                name.add(new IdentifierImpl(ident.getName(), new OffsetRange(ident.getStart(), ident.getFinish())));
            }
            Collections.reverse(name);
            return name;
        }
        return null;
    }

    private JsObject createJsObject(AccessNode accessNode, JsParserResult parserResult, ModelBuilder modelBuilder) {
        List<Identifier> fqn = ModelVisitor.getName(accessNode, parserResult);
        if (fqn == null) {
            return null;
        }
        JsObject object = null;
        Identifier name = fqn.get(0);
        if (!"this".equals(fqn.get(0).getName())) {
            if (modelBuilder.getCurrentWith() == null) {
                JsObjectImpl global;
                Collection<? extends JsObject> variables = ModelUtils.getVariables(modelBuilder.getCurrentDeclarationFunction());
                for (JsObject variable : variables) {
                    if (!variable.getName().equals(name.getName())) continue;
                    if (variable instanceof ParameterObject || variable.getModifiers().contains((Object)Modifier.PRIVATE)) {
                        object = (JsObjectImpl)variable;
                        break;
                    }
                    DeclarationScope variableDS = ModelUtils.getDeclarationScope(variable);
                    if (variableDS.equals(modelBuilder.getCurrentDeclarationScope())) continue;
                    object = (JsObjectImpl)variable;
                    break;
                }
                if (object == null && (object = (JsObjectImpl)(global = modelBuilder.getGlobal()).getProperty(name.getName())) == null) {
                    object = new JsObjectImpl(global, name, name.getOffsetRange(), false, global.getMimeType(), global.getSourceLabel());
                    global.addProperty(name.getName(), object);
                }
            } else {
                JsWith withObject = modelBuilder.getCurrentWith();
                object = (JsObjectImpl)withObject.getProperty(name.getName());
                if (object == null) {
                    object = new JsObjectImpl(withObject, name, name.getOffsetRange(), false, parserResult.getSnapshot().getMimeType(), null);
                    withObject.addProperty(name.getName(), object);
                }
            }
            object.addOccurrence(name.getOffsetRange());
        } else {
            JsObject prototype;
            JsFunctionImpl current = modelBuilder.getCurrentDeclarationFunction();
            object = (JsObjectImpl)this.resolveThis(current);
            if (object != null && object.getProperty(fqn.get(1).getName()) == null && (prototype = object.getProperty("prototype")) != null && prototype.getProperty(fqn.get(1).getName()) != null) {
                object = prototype;
            }
        }
        if (object != null) {
            JsObjectImpl property = null;
            for (int i = 1; i < fqn.size(); ++i) {
                property = (JsObjectImpl)object.getProperty(fqn.get(i).getName());
                if (property == null) continue;
                object = property;
            }
            int pathSize = this.getPath().size();
            Node lastVisited = this.getPath().get(pathSize - 2);
            boolean onLeftSite = false;
            if (lastVisited instanceof BinaryNode) {
                BinaryNode bNode = (BinaryNode)lastVisited;
                boolean bl = onLeftSite = bNode.tokenType() == TokenType.ASSIGN && bNode.lhs().equals((Object)accessNode);
            }
            if (property != null) {
                OffsetRange range = new OffsetRange(accessNode.getProperty().getStart(), accessNode.getProperty().getFinish());
                if (onLeftSite && !property.isDeclared()) {
                    property.setDeclared(true);
                    property.setDeclarationName(new IdentifierImpl(property.getName(), range));
                }
                property.addOccurrence(range);
            } else {
                name = ModelElementFactory.create(parserResult, accessNode.getProperty());
                if (name != null) {
                    if (pathSize > 1 && this.getPath().get(pathSize - 2) instanceof CallNode) {
                        CallNode cNode = (CallNode)this.getPath().get(pathSize - 2);
                        if (!cNode.getArgs().contains((Object)accessNode)) {
                            property = ModelElementFactory.createVirtualFunction(parserResult, object, name, cNode.getArgs().size());
                        } else {
                            property = new JsObjectImpl(object, name, name.getOffsetRange(), onLeftSite, parserResult.getSnapshot().getMimeType(), null);
                            property.addOccurrence(name.getOffsetRange());
                        }
                    } else {
                        JsDocumentationHolder docHolder;
                        boolean setDocumentation = false;
                        if (this.isPriviliged(accessNode) && this.getPath().size() > 1 && (this.getPreviousFromPath(2) instanceof ExecuteNode || this.getPreviousFromPath(1) instanceof ExecuteNode)) {
                            onLeftSite = true;
                            setDocumentation = true;
                        }
                        property = new JsObjectImpl(object, name, name.getOffsetRange(), onLeftSite, parserResult.getSnapshot().getMimeType(), null);
                        property.addOccurrence(name.getOffsetRange());
                        if (setDocumentation && (docHolder = parserResult.getDocumentationHolder()) != null) {
                            property.setDocumentation(docHolder.getDocumentation((Node)accessNode));
                            property.setDeprecated(docHolder.isDeprecated((Node)accessNode));
                            List<Type> returnTypes = docHolder.getReturnType((Node)accessNode);
                            if (!returnTypes.isEmpty()) {
                                for (Type type : returnTypes) {
                                    property.addAssignment(new TypeUsageImpl(type.getType(), type.getOffset(), true), accessNode.getFinish());
                                }
                            }
                            this.setModifiersFromDoc(property, docHolder.getModifiers((Node)accessNode));
                        }
                    }
                    object.addProperty(name.getName(), property);
                    object = property;
                }
            }
        }
        return object;
    }

    public static List<Identifier> getNodeName(Node node, JsParserResult parserResult) {
        if (node instanceof AccessNode) {
            return ModelVisitor.getName((AccessNode)node, parserResult);
        }
        if (node instanceof BinaryNode) {
            return ModelVisitor.getName((BinaryNode)node, parserResult);
        }
        if (node instanceof VarNode) {
            return ModelVisitor.getName((VarNode)node, parserResult);
        }
        if (node instanceof PropertyNode) {
            return ModelVisitor.getName((PropertyNode)node, parserResult);
        }
        if (node instanceof FunctionNode) {
            if (((FunctionNode)node).getKind() == FunctionNode.Kind.SCRIPT) {
                return Collections.emptyList();
            }
            IdentNode ident = ((FunctionNode)node).getIdent();
            return Arrays.asList(new IdentifierImpl(ident.getName(), new OffsetRange(ident.getStart(), ident.getFinish())));
        }
        return Collections.emptyList();
    }

    private boolean isInPropertyNode() {
        boolean inFunction = false;
        for (int i = this.getPath().size() - 1; i > 0; --i) {
            Node node = this.getPath().get(i);
            if (node instanceof FunctionNode) {
                if (!inFunction) {
                    inFunction = true;
                    continue;
                }
                return false;
            }
            if (!(node instanceof PropertyNode)) continue;
            return true;
        }
        return false;
    }

    private void addOccurence(IdentNode iNode, boolean leftSite) {
        this.addOccurence(iNode, leftSite, false);
    }

    private void addOccurence(IdentNode iNode, boolean leftSite, boolean isFunction) {
        this.addOccurrence(iNode.getName(), new OffsetRange(iNode.getStart(), iNode.getFinish()), leftSite, isFunction);
    }

    private void addOccurrence(String name, OffsetRange range, boolean leftSite, boolean isFunction) {
        if ("this".equals(name)) {
            return;
        }
        this.occurrenceBuilder.addOccurrence(name, range, this.modelBuilder.getCurrentDeclarationScope(), this.modelBuilder.getCurrentObject(), this.modelBuilder.getCurrentWith(), isFunction, leftSite);
    }

    private void addOccurrence(IdentNode iNode, String name) {
        String valueName = iNode.getName();
        if (!name.equals(valueName)) {
            this.addOccurence(iNode, false);
        } else {
            DeclarationScopeImpl scope = this.modelBuilder.getCurrentDeclarationScope();
            JsObject parameter = null;
            JsFunction function = (JsFunction)((Object)scope);
            parameter = function.getParameter(iNode.getName());
            if (parameter != null) {
                parameter.addOccurrence(new OffsetRange(iNode.getStart(), iNode.getFinish()));
            } else {
                IdentifierImpl nameI;
                boolean found = false;
                Collection<? extends JsObject> variables = ModelUtils.getVariables(scope.getParentScope());
                for (JsObject jsObject : variables) {
                    if (!valueName.equals(jsObject.getName())) continue;
                    jsObject.addOccurrence(new OffsetRange(iNode.getStart(), iNode.getFinish()));
                    found = true;
                    break;
                }
                if (!found && (nameI = ModelElementFactory.create(this.parserResult, iNode)) != null) {
                    JsObjectImpl newObject = new JsObjectImpl(this.modelBuilder.getGlobal(), nameI, nameI.getOffsetRange(), false, this.parserResult.getSnapshot().getMimeType(), null);
                    newObject.addOccurrence(nameI.getOffsetRange());
                    this.modelBuilder.getGlobal().addProperty(nameI.getName(), newObject);
                }
            }
        }
    }

    private void addDocNameOccurence(JsObjectImpl jsObject) {
        JsDocumentationHolder holder = this.parserResult.getDocumentationHolder();
        JsComment comment = holder.getCommentForOffset(jsObject.getOffset(), holder.getCommentBlocks());
        if (comment != null) {
            for (DocParameter docParameter : comment.getParameters()) {
                Identifier paramName = docParameter.getParamName();
                String name = docParameter.getParamName() == null ? "" : docParameter.getParamName().getName();
                if (!name.equals(jsObject.getName())) continue;
                jsObject.addOccurrence(paramName.getOffsetRange());
            }
        }
    }

    private void addDocTypesOccurence(JsObjectImpl jsObject) {
        JsDocumentationHolder holder = this.parserResult.getDocumentationHolder();
        if (holder.getOccurencesMap().containsKey(jsObject.getName())) {
            for (OffsetRange offsetRange : holder.getOccurencesMap().get(jsObject.getName())) {
                jsObject.addOccurrence(offsetRange);
            }
        }
    }

    private Node getPreviousFromPath(int back) {
        int size = this.getPath().size();
        if (size >= back) {
            return this.getPath().get(size - back);
        }
        return null;
    }

    private JsObject processLhs(Identifier name, JsObject parent, boolean lastOnLeft) {
        JsObject lObject = null;
        if (name != null) {
            boolean hasGrandParent;
            if ("this".equals(name.getName())) {
                return null;
            }
            String newVarName = name.getName();
            boolean hasParent = parent.getProperty(newVarName) != null;
            boolean bl = hasGrandParent = parent.getJSKind() == JsElement.Kind.METHOD && parent.getParent().getProperty(newVarName) != null;
            if (!hasParent && !hasGrandParent && this.modelBuilder.getGlobal().getProperty(newVarName) == null) {
                this.addOccurrence(name.getName(), name.getOffsetRange(), lastOnLeft, false);
            } else {
                JsObject jsObject = hasParent ? parent.getProperty(newVarName) : (lObject = hasGrandParent ? parent.getParent().getProperty(newVarName) : null);
                if (lObject != null) {
                    ((JsObjectImpl)lObject).addOccurrence(name.getOffsetRange());
                } else {
                    this.addOccurrence(name.getName(), name.getOffsetRange(), lastOnLeft, false);
                }
            }
            lObject = (JsObjectImpl)parent.getProperty(newVarName);
            if (lObject == null) {
                Model model = this.parserResult.getModel();
                Collection<? extends JsObject> variables = model.getVariables(name.getOffsetRange().getStart());
                for (JsObject variable : variables) {
                    if (!variable.getName().equals(newVarName)) continue;
                    lObject = (JsObjectImpl)variable;
                    break;
                }
                if (lObject == null) {
                    JsObject where = this.modelBuilder.getCurrentWith() == null ? model.getGlobalObject() : this.modelBuilder.getCurrentWith();
                    lObject = new JsObjectImpl(where, name, name.getOffsetRange(), lastOnLeft, this.parserResult.getSnapshot().getMimeType(), null);
                    where.addProperty(name.getName(), lObject);
                }
            }
        }
        return lObject;
    }

    public JsObject resolveThis(JsObject where) {
        JsObject result;
        JsObject result2;
        JsElement.Kind whereKind = where.getJSKind();
        if (this.canBeSingletonPattern() && (result2 = this.resolveThisInSingletonPattern(where)) != null) {
            return result2;
        }
        if (whereKind == JsElement.Kind.FILE) {
            return where;
        }
        if (whereKind.isFunction() && where.getModifiers().contains((Object)Modifier.PRIVATE)) {
            return where;
        }
        JsObject parent = where.getParent();
        if (parent == null) {
            return where;
        }
        JsElement.Kind parentKind = parent.getJSKind();
        if (parentKind == JsElement.Kind.FILE && !where.isAnonymous()) {
            return where;
        }
        if ("prototype".equals(parent.getName())) {
            return where.getParent().getParent();
        }
        if (whereKind == JsElement.Kind.CONSTRUCTOR) {
            return where;
        }
        if (whereKind.isFunction() && !where.getModifiers().contains((Object)Modifier.PRIVATE) && !where.isAnonymous()) {
            return parent;
        }
        if (this.isInPropertyNode()) {
            return parent;
        }
        if (where.isAnonymous() && (result = this.resolveThisInSingletonPattern(where)) != null) {
            return result;
        }
        return where;
    }

    private JsObject resolveThisInSingletonPattern(JsObject where) {
        UnaryNode uNode;
        int pathIndex = 1;
        Node lastNode = this.getPreviousFromPath(1);
        if (lastNode instanceof FunctionNode && !this.canBeSingletonPattern(pathIndex)) {
            ++pathIndex;
        }
        while (pathIndex < this.getPath().size() && !(this.getPreviousFromPath(pathIndex) instanceof FunctionNode)) {
            ++pathIndex;
        }
        if (this.canBeSingletonPattern(pathIndex) && (uNode = (UnaryNode)this.getPreviousFromPath(pathIndex + 3)).tokenType() == TokenType.NEW) {
            JsObject parent;
            String name = null;
            boolean simpleName = true;
            if (this.getPreviousFromPath(pathIndex + 4) instanceof BinaryNode) {
                BinaryNode bNode = (BinaryNode)this.getPreviousFromPath(pathIndex + 4);
                if (bNode.tokenType() == TokenType.ASSIGN) {
                    if (bNode.lhs() instanceof AccessNode) {
                        List<Identifier> identifier = ModelVisitor.getName((AccessNode)bNode.lhs(), this.parserResult);
                        if (identifier != null) {
                            if (!identifier.isEmpty() && "this".equals(identifier.get(0).getName())) {
                                identifier.remove(0);
                            }
                            if (identifier.size() == 1) {
                                name = identifier.get(0).getName();
                            } else {
                                StringBuilder sb = new StringBuilder();
                                for (Identifier part : identifier) {
                                    sb.append(part.getName()).append('.');
                                }
                                name = sb.toString().substring(0, sb.length() - 1);
                                simpleName = false;
                            }
                        }
                    } else if (bNode.lhs() instanceof IdentNode) {
                        name = ((IdentNode)bNode.lhs()).getName();
                    }
                }
            } else if (this.getPreviousFromPath(pathIndex + 4) instanceof VarNode) {
                VarNode vNode = (VarNode)this.getPreviousFromPath(pathIndex + 4);
                name = vNode.getName().getName();
            }
            JsObject jsObject = parent = where.getParent() == null ? where : where.getParent();
            if (name != null) {
                if (simpleName) {
                    for (parent = where; parent != null && parent.getProperty(name) == null; parent = parent.getParent()) {
                    }
                    if (parent != null && parent.getProperty(name) != null) {
                        return parent.getProperty(name);
                    }
                } else {
                    JsObject property = ModelUtils.findJsObjectByName(ModelUtils.getGlobalObject(parent), name);
                    if (property != null) {
                        return property;
                    }
                }
            }
        }
        return null;
    }

    private boolean canBeSingletonPattern() {
        int pathIndex = 1;
        Node lastNode = this.getPreviousFromPath(1);
        if (lastNode instanceof FunctionNode && !this.canBeSingletonPattern(pathIndex)) {
            ++pathIndex;
        }
        while (pathIndex < this.getPath().size() && !(this.getPreviousFromPath(pathIndex) instanceof FunctionNode)) {
            ++pathIndex;
        }
        return this.canBeSingletonPattern(pathIndex);
    }

    private boolean canBeSingletonPattern(int pathIndex) {
        return this.getPath().size() > pathIndex + 4 && this.getPreviousFromPath(pathIndex) instanceof FunctionNode && this.getPreviousFromPath(pathIndex + 1) instanceof ReferenceNode && this.getPreviousFromPath(pathIndex + 2) instanceof CallNode && this.getPreviousFromPath(pathIndex + 3) instanceof UnaryNode && (this.getPreviousFromPath(pathIndex + 4) instanceof BinaryNode || this.getPreviousFromPath(pathIndex + 4) instanceof VarNode);
    }

    private boolean isPriviliged(AccessNode aNode) {
        Node node = aNode.getBase();
        while (node instanceof AccessNode) {
            node = ((AccessNode)node).getBase();
        }
        if (node instanceof IdentNode && "this".endsWith(((IdentNode)node).getName())) {
            return true;
        }
        return false;
    }

    private void setModifiersFromDoc(JsObject object, Set<JsModifier> modifiers) {
        if (modifiers != null && !modifiers.isEmpty()) {
            for (JsModifier jsModifier : modifiers) {
                switch (jsModifier) {
                    case PRIVATE: {
                        object.getModifiers().remove((Object)Modifier.PROTECTED);
                        object.getModifiers().remove((Object)Modifier.PUBLIC);
                        object.getModifiers().add(Modifier.PRIVATE);
                        break;
                    }
                    case PUBLIC: {
                        object.getModifiers().remove((Object)Modifier.PROTECTED);
                        object.getModifiers().remove((Object)Modifier.PRIVATE);
                        object.getModifiers().add(Modifier.PUBLIC);
                        break;
                    }
                    case STATIC: {
                        object.getModifiers().add(Modifier.STATIC);
                    }
                }
            }
        }
    }

    public static class FunctionCall {
        private final String name;
        private final DeclarationScope scope;
        private final Collection<FunctionArgument> arguments;

        public FunctionCall(String name, DeclarationScope scope, Collection<FunctionArgument> arguments) {
            this.name = name;
            this.scope = scope;
            this.arguments = arguments;
        }

        public String getName() {
            return this.name;
        }

        public DeclarationScope getScope() {
            return this.scope;
        }

        public Collection<FunctionArgument> getArguments() {
            return this.arguments;
        }
    }

}

