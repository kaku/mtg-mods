/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.modules.csl.api.EditorOptions
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor$MutableContext
 */
package org.netbeans.modules.javascript2.editor;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.csl.api.EditorOptions;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.javascript2.editor.TokenSequenceIterator;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationCompleter;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.spi.editor.typinghooks.TypedBreakInterceptor;

public class JsTypedBreakInterceptor
implements TypedBreakInterceptor {
    static final boolean CONTINUE_COMMENTS = Boolean.getBoolean("js.cont.comment");
    static boolean completeDocumentation = true;
    private static final Logger LOGGER = Logger.getLogger(JsTypedBreakInterceptor.class.getName());
    private final Language<JsTokenId> language;
    private final boolean comments;
    private final boolean multiLineLiterals;
    private CommentGenerator commentGenerator = null;

    public JsTypedBreakInterceptor(Language<JsTokenId> language, boolean comments, boolean multiLineLiterals) {
        this.language = language;
        this.comments = comments;
        this.multiLineLiterals = multiLineLiterals;
    }

    private boolean isInsertMatchingEnabled() {
        EditorOptions options = EditorOptions.get((String)this.language.mimeType());
        if (options != null) {
            return options.getMatchBrackets();
        }
        return true;
    }

    public void insert(TypedBreakInterceptor.MutableContext context) throws BadLocationException {
        boolean isComment;
        Token<? extends JsTokenId> prevToken;
        int indentSize;
        StringBuilder sb;
        int begin;
        BaseDocument doc = (BaseDocument)context.getDocument();
        TokenHierarchy tokenHierarchy = TokenHierarchy.get((Document)doc);
        int offset = context.getCaretOffset();
        int lineBegin = Utilities.getRowStart((BaseDocument)doc, (int)offset);
        int lineEnd = Utilities.getRowEnd((BaseDocument)doc, (int)offset);
        if (lineBegin == offset && lineEnd == offset) {
            return;
        }
        TokenSequence<JsTokenId> ts = LexUtilities.getTokenSequence(tokenHierarchy, offset, this.language);
        if (ts == null) {
            return;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return;
        }
        Token token = ts.token();
        JsTokenId id = (JsTokenId)token.id();
        if (!id.isError() && this.isInsertMatchingEnabled() && !this.isDocToken(id) && this.isAddRightBrace(doc, offset)) {
            int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
            int afterLastNonWhite = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)offset);
            sb = new StringBuilder();
            int carretOffset = 0;
            int curlyOffset = this.getUnbalancedCurlyOffset(doc, offset);
            if (offset > afterLastNonWhite) {
                sb.append("\n");
                sb.append(IndentUtils.createIndentString((Document)doc, (int)(indent + IndentUtils.indentLevelSize((Document)doc))));
                carretOffset = sb.length();
                sb.append("\n");
                if (curlyOffset >= 0) {
                    sb.append(IndentUtils.createIndentString((Document)doc, (int)this.getCurlyIndent(doc, curlyOffset)));
                } else {
                    sb.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                }
                sb.append("}");
            } else {
                boolean[] insert = new boolean[]{true};
                int end = this.getRowOrBlockEnd(doc, offset, insert);
                if (insert[0]) {
                    String restOfLine = doc.getText(offset, Math.min(end, Utilities.getRowEnd((BaseDocument)doc, (int)afterLastNonWhite)) - offset);
                    sb.append("\n");
                    sb.append(IndentUtils.createIndentString((Document)doc, (int)(indent + IndentUtils.indentLevelSize((Document)doc))));
                    carretOffset = sb.length();
                    sb.append(restOfLine);
                    sb.append("\n");
                    if (curlyOffset >= 0) {
                        sb.append(IndentUtils.createIndentString((Document)doc, (int)this.getCurlyIndent(doc, curlyOffset)));
                    } else {
                        sb.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                    }
                    sb.append("}");
                    doc.remove(offset, restOfLine.length());
                }
            }
            if (sb.length() > 0) {
                context.setText(sb.toString(), 0, carretOffset, new int[0]);
            }
            return;
        }
        if (id.isError()) {
            String text = token.text().toString();
            if (this.comments && text.startsWith("/*") && ts.offset() == Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset)) {
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb2 = new StringBuilder();
                sb2.append("\n");
                sb2.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                sb2.append(" * ");
                int carretOffset = sb2.length();
                sb2.append("\n");
                sb2.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                sb2.append(" */");
                if (text.startsWith("/**")) {
                    this.commentGenerator = new CommentGenerator(offset + carretOffset, indent + 1);
                }
                context.setText(sb2.toString(), 0, carretOffset, new int[0]);
                return;
            }
        }
        if (this.multiLineLiterals) {
            String str;
            if (id == JsTokenId.STRING || id == JsTokenId.STRING_END && offset < ts.offset() + ts.token().length()) {
                str = id != JsTokenId.STRING || offset > ts.offset() ? "\\n\\\n" : "\\\n";
                context.setText(str, -1, str.length(), new int[0]);
                return;
            }
            if (id == JsTokenId.REGEXP || id == JsTokenId.REGEXP_END && offset < ts.offset() + ts.token().length()) {
                str = id != JsTokenId.REGEXP || offset > ts.offset() ? "\\n\\\n" : "\\\n";
                context.setText(str, -1, str.length(), new int[0]);
                return;
            }
        }
        if ((id == JsTokenId.BRACKET_RIGHT_CURLY || id == JsTokenId.BRACKET_RIGHT_BRACKET) && offset > 0 && (prevToken = LexUtilities.getToken((Document)doc, offset - 1, this.language)) != null) {
            JsTokenId prevTokenId = (JsTokenId)prevToken.id();
            if (id == JsTokenId.BRACKET_RIGHT_CURLY && prevTokenId == JsTokenId.BRACKET_LEFT_CURLY || id == JsTokenId.BRACKET_RIGHT_BRACKET && prevTokenId == JsTokenId.BRACKET_LEFT_BRACKET) {
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb3 = new StringBuilder();
                sb3.append("\n");
                sb3.append(IndentUtils.createIndentString((Document)doc, (int)(indent + IndentUtils.indentLevelSize((Document)doc))));
                int carretOffset = sb3.length();
                sb3.append("\n");
                sb3.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                context.setText(sb3.toString(), 0, carretOffset, new int[0]);
                return;
            }
        }
        if (!this.comments) {
            return;
        }
        if (id == JsTokenId.WHITESPACE && (begin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset)) != -1 && offset < begin) {
            ts.move(begin);
            if (ts.moveNext() && (id = (JsTokenId)ts.token().id()) == JsTokenId.LINE_COMMENT) {
                offset = begin;
            }
        }
        if ((id == JsTokenId.BLOCK_COMMENT || id == JsTokenId.DOC_COMMENT) && offset > ts.offset() && offset < ts.offset() + ts.token().length()) {
            String line;
            boolean isBlockStart;
            int begin2 = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            int end = Utilities.getRowEnd((BaseDocument)doc, (int)offset) + 1;
            if (begin2 == -1) {
                begin2 = end;
            }
            boolean bl = isBlockStart = (line = doc.getText(begin2, end - begin2)).startsWith("/*") || begin2 != -1 && begin2 < ts.offset();
            if (isBlockStart || line.startsWith("*")) {
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb4 = new StringBuilder("\n");
                if (isBlockStart) {
                    ++indent;
                }
                int carretPosition = 0;
                sb4.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                if (isBlockStart) {
                    sb4.append("* ");
                    carretPosition = sb4.length();
                    TokenSequence<? extends JsDocumentationTokenId> jsDocTS = LexUtilities.getJsDocumentationTokenSequence(tokenHierarchy, offset);
                    if (jsDocTS != null && !JsTypedBreakInterceptor.hasCommentEnd(jsDocTS)) {
                        this.commentGenerator = new CommentGenerator(offset + carretPosition, indent);
                        sb4.append("\n").append(IndentUtils.createIndentString((Document)doc, (int)indent)).append("*/");
                    }
                } else {
                    char c;
                    sb4.append("*");
                    int afterStar = isBlockStart ? begin2 + 2 : begin2 + 1;
                    line = doc.getText(afterStar, Utilities.getRowEnd((BaseDocument)doc, (int)afterStar) - afterStar);
                    for (int i = 0; i < line.length() && ((c = line.charAt(i)) == ' ' || c == '\t'); ++i) {
                        sb4.append(c);
                    }
                    carretPosition = sb4.length();
                }
                if (offset == begin2 && offset > 0) {
                    context.setText(sb4.toString(), -1, sb4.length(), new int[0]);
                    return;
                }
                context.setText(sb4.toString(), -1, carretPosition, new int[0]);
                return;
            }
        }
        boolean bl = isComment = id == JsTokenId.LINE_COMMENT;
        if (id == JsTokenId.EOL && ts.movePrevious() && ts.token().id() == JsTokenId.LINE_COMMENT) {
            isComment = true;
        }
        if (isComment) {
            Token<? extends JsTokenId> firstToken;
            int rowEnd;
            int nextBegin;
            int prevBegin;
            Token<? extends JsTokenId> firstToken2;
            boolean continueComment = false;
            int begin3 = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
            boolean previousLineWasComment = false;
            boolean nextLineIsComment = false;
            int rowStart = Utilities.getRowStart((BaseDocument)doc, (int)offset);
            if (rowStart > 0 && (prevBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)(rowStart - 1))) != -1 && (firstToken2 = LexUtilities.getToken((Document)doc, prevBegin, this.language)) != null && firstToken2.id() == JsTokenId.LINE_COMMENT) {
                previousLineWasComment = true;
            }
            if ((rowEnd = Utilities.getRowEnd((BaseDocument)doc, (int)offset)) < doc.getLength() && (nextBegin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)(rowEnd + 1))) != -1 && (firstToken = LexUtilities.getToken((Document)doc, nextBegin, this.language)) != null && firstToken.id() == JsTokenId.LINE_COMMENT) {
                nextLineIsComment = true;
            }
            if (previousLineWasComment || nextLineIsComment || offset > ts.offset() && offset < ts.offset() + ts.token().length()) {
                Token<? extends JsTokenId> firstToken3;
                Token<? extends JsTokenId> firstToken4;
                int nextLine;
                int nextLineFirst;
                if (ts.offset() + token.length() > offset + 1) {
                    String trailing = doc.getText(offset, Utilities.getRowEnd((BaseDocument)doc, (int)offset) - offset);
                    if (trailing.trim().length() != 0) {
                        continueComment = true;
                    }
                } else if (CONTINUE_COMMENTS && (firstToken3 = LexUtilities.getToken((Document)doc, begin3, this.language)).id() == JsTokenId.LINE_COMMENT) {
                    continueComment = true;
                }
                if (!continueComment && (nextLine = Utilities.getRowEnd((BaseDocument)doc, (int)offset) + 1) < doc.getLength() && (nextLineFirst = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)nextLine)) != -1 && (firstToken4 = LexUtilities.getToken((Document)doc, nextLineFirst, this.language)) != null && firstToken4.id() == JsTokenId.LINE_COMMENT) {
                    continueComment = true;
                }
            }
            if (continueComment) {
                char c;
                int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                StringBuilder sb5 = new StringBuilder();
                if (offset != begin3 || offset <= 0) {
                    sb5.append("\n");
                }
                sb5.append(IndentUtils.createIndentString((Document)doc, (int)indent));
                sb5.append("//");
                int afterSlash = begin3 + 2;
                String line = doc.getText(afterSlash, Utilities.getRowEnd((BaseDocument)doc, (int)afterSlash) - afterSlash);
                for (int i = 0; i < line.length() && ((c = line.charAt(i)) == ' ' || c == '\t'); ++i) {
                    sb5.append(c);
                }
                if (offset == begin3 && offset > 0) {
                    int caretPosition = sb5.length();
                    sb5.append("\n");
                    context.setText(sb5.toString(), -1, caretPosition, new int[0]);
                    return;
                }
                context.setText(sb5.toString(), -1, sb5.length(), new int[0]);
                return;
            }
        }
        if ((indentSize = this.getNextLineIndentation(doc, offset)) > 0) {
            sb = new StringBuilder("\n");
            sb.append(IndentUtils.createIndentString((Document)doc, (int)indentSize));
            context.setText(sb.toString(), -1, sb.length(), new int[0]);
        }
    }

    public void afterInsert(TypedBreakInterceptor.Context context) throws BadLocationException {
        if (completeDocumentation && this.commentGenerator != null) {
            JsDocumentationCompleter.generateCompleteComment((BaseDocument)context.getDocument(), this.commentGenerator.getOffset(), this.commentGenerator.getIndent());
            this.commentGenerator = null;
        }
    }

    public boolean beforeInsert(TypedBreakInterceptor.Context context) throws BadLocationException {
        return false;
    }

    public void cancelled(TypedBreakInterceptor.Context context) {
    }

    private int getNextLineIndentation(BaseDocument doc, int offset) throws BadLocationException {
        int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
        int currentOffset = offset;
        while (currentOffset > 0) {
            if (!(Utilities.isRowEmpty((BaseDocument)doc, (int)currentOffset) || Utilities.isRowWhite((BaseDocument)doc, (int)currentOffset) || LexUtilities.isCommentOnlyLine(doc, currentOffset, this.language))) {
                indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)currentOffset);
                int parenBalance = LexUtilities.getLineBalance(doc, currentOffset, JsTokenId.BRACKET_LEFT_PAREN, JsTokenId.BRACKET_RIGHT_PAREN);
                if (parenBalance < 0) break;
                int curlyBalance = LexUtilities.getLineBalance(doc, currentOffset, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY);
                if (curlyBalance > 0) {
                    indent += IndentUtils.indentLevelSize((Document)doc);
                }
                return indent;
            }
            currentOffset = Utilities.getRowStart((BaseDocument)doc, (int)currentOffset) - 1;
        }
        return indent;
    }

    private boolean isAddRightBrace(BaseDocument doc, int caretOffset) throws BadLocationException {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence((Document)doc, caretOffset, this.language);
        if (ts == null) {
            return false;
        }
        ts.moveIndex(0);
        if (!ts.moveNext()) {
            return false;
        }
        int balance = 0;
        boolean balancedAfter = false;
        do {
            Token t;
            if ((t = ts.token()).id() == JsTokenId.BRACKET_LEFT_CURLY) {
                ++balance;
                continue;
            }
            if (t.id() != JsTokenId.BRACKET_RIGHT_CURLY) continue;
            --balance;
        } while (ts.offset() < caretOffset && ts.moveNext());
        TokenSequenceIterator tsi = new TokenSequenceIterator(TokenHierarchy.get((Document)doc).tokenSequenceList(ts.languagePath(), caretOffset, doc.getLength()), false);
        while (tsi.hasMore()) {
            TokenSequence sq = tsi.getSequence();
            Token t = sq.token();
            if (t.id() == JsTokenId.BRACKET_LEFT_CURLY) {
                ++balance;
            } else if (t.id() == JsTokenId.BRACKET_RIGHT_CURLY) {
                --balance;
            }
            if (balance != 0 || t.id() != JsTokenId.BRACKET_LEFT_CURLY && t.id() != JsTokenId.BRACKET_RIGHT_CURLY) continue;
            balancedAfter = true;
            break;
        }
        if (balance < 0) {
            return false;
        }
        int caretRowStartOffset = Utilities.getRowStart((BaseDocument)doc, (int)caretOffset);
        ts = LexUtilities.getPositionedSequence((Document)doc, caretOffset, this.language);
        if (ts == null) {
            return false;
        }
        if (ts.offset() == caretOffset && !ts.movePrevious()) {
            return false;
        }
        boolean first = true;
        do {
            if (ts.offset() < caretRowStartOffset) {
                return false;
            }
            JsTokenId id = (JsTokenId)ts.token().id();
            switch (id) {
                case WHITESPACE: 
                case LINE_COMMENT: {
                    break;
                }
                case BLOCK_COMMENT: 
                case DOC_COMMENT: {
                    if (!first || caretOffset <= ts.offset() || caretOffset >= ts.offset() + ts.token().length()) break;
                    return false;
                }
                case BRACKET_LEFT_CURLY: {
                    return !balancedAfter;
                }
                default: {
                    return false;
                }
            }
            first = false;
        } while (ts.movePrevious());
        return false;
    }

    private int getRowOrBlockEnd(BaseDocument doc, int caretOffset, boolean[] insert) throws BadLocationException {
        int rowEnd = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)caretOffset);
        if (rowEnd == -1 || caretOffset >= rowEnd) {
            return caretOffset;
        }
        ++rowEnd;
        int parenBalance = 0;
        int braceBalance = 0;
        int bracketBalance = 0;
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getPositionedSequence((Document)doc, caretOffset, this.language);
        if (ts == null) {
            return caretOffset;
        }
        while (ts.offset() < rowEnd) {
            JsTokenId id = (JsTokenId)ts.token().id();
            switch (id) {
                case OPERATOR_SEMICOLON: {
                    return ts.offset() + 1;
                }
                case OPERATOR_COMMA: {
                    return ts.offset();
                }
                case BRACKET_LEFT_PAREN: {
                    ++parenBalance;
                    break;
                }
                case BRACKET_RIGHT_PAREN: {
                    if (parenBalance-- != 0) break;
                    return ts.offset();
                }
                case BRACKET_LEFT_CURLY: {
                    ++braceBalance;
                    break;
                }
                case BRACKET_RIGHT_CURLY: {
                    if (braceBalance-- != 0) break;
                    return ts.offset();
                }
                case BRACKET_LEFT_BRACKET: {
                    ++bracketBalance;
                    break;
                }
                case BRACKET_RIGHT_BRACKET: {
                    if (bracketBalance-- != 0) break;
                    return ts.offset();
                }
            }
            if (ts.moveNext()) continue;
            if (caretOffset - ts.offset() != 1 || bracketBalance != 1 && parenBalance != 1 && braceBalance != 1) break;
            return caretOffset;
        }
        insert[0] = false;
        return rowEnd;
    }

    private int getUnbalancedCurlyOffset(BaseDocument doc, int offset) throws BadLocationException {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getPositionedSequence((Document)doc, offset, this.language);
        if (ts == null) {
            return -1;
        }
        int balance = 0;
        while (ts.movePrevious()) {
            Token t = ts.token();
            if (t.id() == JsTokenId.BRACKET_RIGHT_CURLY) {
                ++balance;
                continue;
            }
            if (t.id() != JsTokenId.BRACKET_LEFT_CURLY || --balance >= 0) continue;
            return ts.offset();
        }
        return -1;
    }

    private int getCurlyIndent(BaseDocument doc, int offset) {
        try {
            int lineStart = Utilities.getRowStart((BaseDocument)doc, (int)offset, (int)0);
            TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence((Document)doc, lineStart, this.language);
            int prevLineStart = -1;
            if (ts != null) {
                do {
                    ts.move(lineStart);
                    if (!ts.moveNext()) {
                        if (prevLineStart >= 0) {
                            return IndentUtils.lineIndent((Document)doc, (int)lineStart);
                        }
                        return GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                    }
                    Token<? extends JsTokenId> token = LexUtilities.findNextNonWsNonComment(ts);
                    Token<? extends JsTokenId> nextToken = LexUtilities.findNextNonWsNonComment(ts);
                    if (!LexUtilities.isBinaryOperator((JsTokenId)token.id(), (JsTokenId)nextToken.id())) {
                        ts.move(lineStart);
                        if (!ts.movePrevious()) {
                            return IndentUtils.lineIndent((Document)doc, (int)lineStart);
                        }
                        nextToken = token;
                        token = LexUtilities.findPreviousNonWsNonComment(ts);
                        if (!LexUtilities.isBinaryOperator((JsTokenId)token.id(), (JsTokenId)nextToken.id())) {
                            return IndentUtils.lineIndent((Document)doc, (int)lineStart);
                        }
                    }
                    prevLineStart = lineStart;
                } while ((lineStart = Utilities.getRowStart((BaseDocument)doc, (int)lineStart, (int)-1)) > 0);
                if (lineStart <= 0) {
                    return IndentUtils.lineIndent((Document)doc, (int)lineStart);
                }
            }
        }
        catch (BadLocationException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
        return GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
    }

    private boolean isDocToken(JsTokenId id) {
        return id == JsTokenId.BLOCK_COMMENT || id == JsTokenId.DOC_COMMENT;
    }

    private static boolean hasCommentEnd(TokenSequence ts) {
        while (ts.moveNext()) {
            Token token = ts.token();
            if (token.id() == JsDocumentationTokenId.COMMENT_END) {
                return true;
            }
            if (!CharSequenceUtilities.endsWith((CharSequence)token.text(), (CharSequence)"/") || !ts.moveNext()) continue;
            Token nextToken = ts.token();
            if (CharSequenceUtilities.textEquals((CharSequence)nextToken.text(), (CharSequence)"/")) {
                ts.movePrevious();
                continue;
            }
            if (nextToken.id() != JsDocumentationTokenId.ASTERISK) continue;
            return false;
        }
        return false;
    }

    private static class CommentGenerator {
        private final int offset;
        private final int indent;

        public CommentGenerator(int offset, int indent) {
            this.offset = offset;
            this.indent = indent;
        }

        public int getIndent() {
            return this.indent;
        }

        public int getOffset() {
            return this.offset;
        }
    }

    public static class JsonFactory
    implements TypedBreakInterceptor.Factory {
        public TypedBreakInterceptor createTypedBreakInterceptor(MimePath mimePath) {
            return new JsTypedBreakInterceptor(JsTokenId.jsonLanguage(), false, false);
        }
    }

    public static class JsFactory
    implements TypedBreakInterceptor.Factory {
        public TypedBreakInterceptor createTypedBreakInterceptor(MimePath mimePath) {
            return new JsTypedBreakInterceptor(JsTokenId.javascriptLanguage(), true, true);
        }
    }

}

