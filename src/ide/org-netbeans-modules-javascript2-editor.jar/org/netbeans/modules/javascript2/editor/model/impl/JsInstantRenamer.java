/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.InstantRenamer
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.OccurrencesSupport;
import org.netbeans.modules.javascript2.editor.navigation.OccurrencesFinderImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsInstantRenamer
implements InstantRenamer {
    public boolean isRenameAllowed(ParserResult info, int caretOffset, String[] explanationRetValue) {
        JsParserResult jsInfo = (JsParserResult)info;
        OccurrencesSupport os = jsInfo.getModel().getOccurrencesSupport();
        Occurrence occurrence = os.getOccurrence(jsInfo.getSnapshot().getEmbeddedOffset(caretOffset));
        return occurrence != null;
    }

    public Set<OffsetRange> getRenameRegions(ParserResult info, int caretOffset) {
        if (info instanceof JsParserResult) {
            JsParserResult pResult = (JsParserResult)info;
            Set<OffsetRange> findOccurrenceRanges = OccurrencesFinderImpl.findOccurrenceRanges(pResult, info.getSnapshot().getEmbeddedOffset(caretOffset));
            HashSet<OffsetRange> sourceRanges = new HashSet<OffsetRange>(findOccurrenceRanges.size());
            for (OffsetRange range : findOccurrenceRanges) {
                sourceRanges.add(LexUtilities.getLexerOffsets(pResult, range));
            }
            return sourceRanges;
        }
        return Collections.EMPTY_SET;
    }
}

