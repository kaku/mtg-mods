/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.AccessNode
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.Block
 *  jdk.nashorn.internal.ir.BreakNode
 *  jdk.nashorn.internal.ir.CallNode
 *  jdk.nashorn.internal.ir.CaseNode
 *  jdk.nashorn.internal.ir.CatchNode
 *  jdk.nashorn.internal.ir.ContinueNode
 *  jdk.nashorn.internal.ir.DoWhileNode
 *  jdk.nashorn.internal.ir.ExecuteNode
 *  jdk.nashorn.internal.ir.ForNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.IfNode
 *  jdk.nashorn.internal.ir.IndexNode
 *  jdk.nashorn.internal.ir.LabelNode
 *  jdk.nashorn.internal.ir.LineNumberNode
 *  jdk.nashorn.internal.ir.LiteralNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.ObjectNode
 *  jdk.nashorn.internal.ir.PropertyNode
 *  jdk.nashorn.internal.ir.ReferenceNode
 *  jdk.nashorn.internal.ir.ReturnNode
 *  jdk.nashorn.internal.ir.RuntimeNode
 *  jdk.nashorn.internal.ir.SwitchNode
 *  jdk.nashorn.internal.ir.TernaryNode
 *  jdk.nashorn.internal.ir.ThrowNode
 *  jdk.nashorn.internal.ir.TryNode
 *  jdk.nashorn.internal.ir.UnaryNode
 *  jdk.nashorn.internal.ir.VarNode
 *  jdk.nashorn.internal.ir.WhileNode
 *  jdk.nashorn.internal.ir.WithNode
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.List;
import jdk.nashorn.internal.ir.AccessNode;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.Block;
import jdk.nashorn.internal.ir.BreakNode;
import jdk.nashorn.internal.ir.CallNode;
import jdk.nashorn.internal.ir.CaseNode;
import jdk.nashorn.internal.ir.CatchNode;
import jdk.nashorn.internal.ir.ContinueNode;
import jdk.nashorn.internal.ir.DoWhileNode;
import jdk.nashorn.internal.ir.ExecuteNode;
import jdk.nashorn.internal.ir.ForNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.IfNode;
import jdk.nashorn.internal.ir.IndexNode;
import jdk.nashorn.internal.ir.LabelNode;
import jdk.nashorn.internal.ir.LineNumberNode;
import jdk.nashorn.internal.ir.LiteralNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.ObjectNode;
import jdk.nashorn.internal.ir.PropertyNode;
import jdk.nashorn.internal.ir.ReferenceNode;
import jdk.nashorn.internal.ir.ReturnNode;
import jdk.nashorn.internal.ir.RuntimeNode;
import jdk.nashorn.internal.ir.SwitchNode;
import jdk.nashorn.internal.ir.TernaryNode;
import jdk.nashorn.internal.ir.ThrowNode;
import jdk.nashorn.internal.ir.TryNode;
import jdk.nashorn.internal.ir.UnaryNode;
import jdk.nashorn.internal.ir.VarNode;
import jdk.nashorn.internal.ir.WhileNode;
import jdk.nashorn.internal.ir.WithNode;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;

public class PathNodeVisitor
extends NodeVisitor {
    private final List<Node> treePath = new ArrayList<Node>();

    public List<? extends Node> getPath() {
        return this.treePath;
    }

    public void addToPath(Node node) {
        this.treePath.add(node);
    }

    public void removeFromPathTheLast() {
        this.treePath.remove(this.treePath.size() - 1);
    }

    public Node enter(AccessNode accessNode) {
        this.addToPath((Node)accessNode);
        return super.enter(accessNode);
    }

    public Node leave(AccessNode accessNode) {
        this.removeFromPathTheLast();
        return super.leave(accessNode);
    }

    public Node enter(BinaryNode binaryNode) {
        this.addToPath((Node)binaryNode);
        return super.enter(binaryNode);
    }

    public Node leave(BinaryNode binaryNode) {
        this.removeFromPathTheLast();
        return super.leave(binaryNode);
    }

    public Node enter(Block block) {
        this.addToPath((Node)block);
        return super.enter(block);
    }

    public Node leave(Block block) {
        this.removeFromPathTheLast();
        return super.leave(block);
    }

    public Node enter(BreakNode breakNode) {
        this.addToPath((Node)breakNode);
        return super.enter(breakNode);
    }

    public Node leave(BreakNode breakNode) {
        this.removeFromPathTheLast();
        return super.leave(breakNode);
    }

    public Node enter(CallNode callNode) {
        this.addToPath((Node)callNode);
        return super.enter(callNode);
    }

    public Node leave(CallNode callNode) {
        this.removeFromPathTheLast();
        return super.leave(callNode);
    }

    public Node enter(CaseNode caseNode) {
        this.addToPath((Node)caseNode);
        return super.enter(caseNode);
    }

    public Node leave(CaseNode caseNode) {
        this.removeFromPathTheLast();
        return super.leave(caseNode);
    }

    public Node enter(CatchNode catchNode) {
        this.addToPath((Node)catchNode);
        return super.enter(catchNode);
    }

    public Node leave(CatchNode catchNode) {
        this.removeFromPathTheLast();
        return super.leave(catchNode);
    }

    public Node enter(ContinueNode continueNode) {
        this.addToPath((Node)continueNode);
        return super.enter(continueNode);
    }

    public Node leave(ContinueNode continueNode) {
        this.removeFromPathTheLast();
        return super.leave(continueNode);
    }

    public Node enter(DoWhileNode doWhileNode) {
        this.addToPath((Node)doWhileNode);
        return super.enter(doWhileNode);
    }

    public Node leave(DoWhileNode doWhileNode) {
        this.removeFromPathTheLast();
        return super.leave(doWhileNode);
    }

    public Node enter(ExecuteNode executeNode) {
        this.addToPath((Node)executeNode);
        return super.enter(executeNode);
    }

    public Node leave(ExecuteNode executeNode) {
        this.removeFromPathTheLast();
        return super.leave(executeNode);
    }

    public Node enter(ForNode forNode) {
        this.addToPath((Node)forNode);
        return super.enter(forNode);
    }

    public Node leave(ForNode forNode) {
        this.removeFromPathTheLast();
        return super.leave(forNode);
    }

    public Node enter(FunctionNode functionNode) {
        this.addToPath((Node)functionNode);
        return super.enter(functionNode);
    }

    public Node leave(FunctionNode functionNode) {
        this.removeFromPathTheLast();
        return super.leave(functionNode);
    }

    public Node enter(IdentNode identNode) {
        this.addToPath((Node)identNode);
        return super.enter(identNode);
    }

    public Node leave(IdentNode identNode) {
        this.removeFromPathTheLast();
        return super.leave(identNode);
    }

    public Node enter(IfNode ifNode) {
        this.addToPath((Node)ifNode);
        return super.enter(ifNode);
    }

    public Node leave(IfNode ifNode) {
        this.removeFromPathTheLast();
        return super.leave(ifNode);
    }

    public Node enter(IndexNode indexNode) {
        this.addToPath((Node)indexNode);
        return super.enter(indexNode);
    }

    public Node leave(IndexNode indexNode) {
        this.removeFromPathTheLast();
        return super.leave(indexNode);
    }

    public Node enter(LabelNode labeledNode) {
        this.addToPath((Node)labeledNode);
        return super.enter(labeledNode);
    }

    public Node leave(LabelNode labeledNode) {
        this.removeFromPathTheLast();
        return super.leave(labeledNode);
    }

    public Node enter(LineNumberNode lineNumberNode) {
        this.addToPath((Node)lineNumberNode);
        return super.enter(lineNumberNode);
    }

    public Node leave(LineNumberNode lineNumberNode) {
        this.removeFromPathTheLast();
        return super.leave(lineNumberNode);
    }

    public Node enter(LiteralNode literalNode) {
        this.addToPath((Node)literalNode);
        return super.enter(literalNode);
    }

    public Node leave(LiteralNode literalNode) {
        this.removeFromPathTheLast();
        return super.leave(literalNode);
    }

    public Node enter(ObjectNode objectNode) {
        this.addToPath((Node)objectNode);
        return super.enter(objectNode);
    }

    public Node leave(ObjectNode objectNode) {
        this.removeFromPathTheLast();
        return super.leave(objectNode);
    }

    public Node enter(PropertyNode propertyNode) {
        this.addToPath((Node)propertyNode);
        return super.enter(propertyNode);
    }

    public Node leave(PropertyNode propertyNode) {
        this.removeFromPathTheLast();
        return super.leave(propertyNode);
    }

    public Node enter(ReferenceNode referenceNode) {
        this.addToPath((Node)referenceNode);
        return super.enter(referenceNode);
    }

    public Node leave(ReferenceNode referenceNode) {
        this.removeFromPathTheLast();
        return super.leave(referenceNode);
    }

    public Node enter(ReturnNode returnNode) {
        this.addToPath((Node)returnNode);
        return super.enter(returnNode);
    }

    public Node leave(ReturnNode returnNode) {
        this.removeFromPathTheLast();
        return super.leave(returnNode);
    }

    public Node enter(RuntimeNode runtimeNode) {
        this.addToPath((Node)runtimeNode);
        return super.enter(runtimeNode);
    }

    public Node leave(RuntimeNode runtimeNode) {
        this.removeFromPathTheLast();
        return super.leave(runtimeNode);
    }

    public Node enter(SwitchNode switchNode) {
        this.addToPath((Node)switchNode);
        return super.enter(switchNode);
    }

    public Node leave(SwitchNode switchNode) {
        this.removeFromPathTheLast();
        return super.leave(switchNode);
    }

    public Node enter(TernaryNode ternaryNode) {
        this.addToPath((Node)ternaryNode);
        return super.enter(ternaryNode);
    }

    public Node leave(TernaryNode ternaryNode) {
        this.removeFromPathTheLast();
        return super.leave(ternaryNode);
    }

    public Node enter(ThrowNode throwNode) {
        this.addToPath((Node)throwNode);
        return super.enter(throwNode);
    }

    public Node leave(ThrowNode throwNode) {
        this.removeFromPathTheLast();
        return super.leave(throwNode);
    }

    public Node enter(TryNode tryNode) {
        this.addToPath((Node)tryNode);
        return super.enter(tryNode);
    }

    public Node leave(TryNode tryNode) {
        this.removeFromPathTheLast();
        return super.leave(tryNode);
    }

    public Node enter(UnaryNode unaryNode) {
        this.addToPath((Node)unaryNode);
        return super.enter(unaryNode);
    }

    public Node leave(UnaryNode unaryNode) {
        this.removeFromPathTheLast();
        return super.leave(unaryNode);
    }

    public Node enter(VarNode varNode) {
        this.addToPath((Node)varNode);
        return super.enter(varNode);
    }

    public Node leave(VarNode varNode) {
        this.removeFromPathTheLast();
        return super.leave(varNode);
    }

    public Node enter(WhileNode whileNode) {
        this.addToPath((Node)whileNode);
        return super.enter(whileNode);
    }

    public Node leave(WhileNode whileNode) {
        this.removeFromPathTheLast();
        return super.leave(whileNode);
    }

    public Node enter(WithNode withNode) {
        this.addToPath((Node)withNode);
        return super.enter(withNode);
    }

    public Node leave(WithNode withNode) {
        this.removeFromPathTheLast();
        return super.leave(withNode);
    }
}

