/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.spi;

public enum CompletionContext {
    NONE,
    EXPRESSION,
    OBJECT_PROPERTY,
    OBJECT_MEMBERS,
    OBJECT_PROPERTY_NAME,
    DOCUMENTATION,
    GLOBAL,
    STRING;
    

    private CompletionContext() {
    }
}

