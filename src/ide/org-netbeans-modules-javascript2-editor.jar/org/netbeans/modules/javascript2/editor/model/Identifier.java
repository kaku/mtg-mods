/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model;

import org.netbeans.modules.csl.api.OffsetRange;

public interface Identifier {
    public String getName();

    public OffsetRange getOffsetRange();
}

