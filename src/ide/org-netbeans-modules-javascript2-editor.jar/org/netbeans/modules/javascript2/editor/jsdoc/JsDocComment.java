/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.jsdoc;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsModifier;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocCommentType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.DeclarationElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.DescriptionElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.NamedParameterElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.UnnamedParameterElement;
import org.netbeans.modules.javascript2.editor.model.Type;

public class JsDocComment
extends JsComment {
    private final Map<JsDocElementType, List<JsDocElement>> tags = new EnumMap<JsDocElementType, List<JsDocElement>>(JsDocElementType.class);
    private final JsDocCommentType type;

    public JsDocComment(OffsetRange offsetRange, JsDocCommentType type, List<JsDocElement> elements) {
        super(offsetRange);
        this.type = type;
        this.initTags(elements);
    }

    public JsDocCommentType getType() {
        return this.type;
    }

    @Override
    public List<String> getSummary() {
        LinkedList<String> summaries = new LinkedList<String>();
        for (JsDocElement jsDocElement2 : this.getTagsForTypes(new JsDocElementType[]{JsDocElementType.DESCRIPTION, JsDocElementType.CONTEXT_SENSITIVE})) {
            summaries.add(((DescriptionElement)jsDocElement2).getDescription());
        }
        for (JsDocElement jsDocElement : this.getTagsForType(JsDocElementType.CLASS)) {
            summaries.add(((DescriptionElement)jsDocElement).getDescription());
        }
        return summaries;
    }

    @Override
    public List<String> getSyntax() {
        LinkedList<String> syntaxes = new LinkedList<String>();
        for (JsDocElement jsDocElement : this.getTagsForType(JsDocElementType.SYNTAX)) {
            syntaxes.add(((DescriptionElement)jsDocElement).getDescription());
        }
        return syntaxes;
    }

    @Override
    public DocParameter getReturnType() {
        Iterator<? extends JsDocElement> i$ = this.getTagsForTypes(new JsDocElementType[]{JsDocElementType.RETURN, JsDocElementType.RETURNS}).iterator();
        if (i$.hasNext()) {
            JsDocElement jsDocElement = i$.next();
            return (DocParameter)((Object)jsDocElement);
        }
        i$ = this.getTagsForType(JsDocElementType.TYPE).iterator();
        if (i$.hasNext()) {
            JsDocElement jsDocElement = i$.next();
            return UnnamedParameterElement.create(jsDocElement.getType(), Arrays.asList(((DeclarationElement)jsDocElement).getDeclaredType()), "");
        }
        return null;
    }

    @Override
    public List<DocParameter> getParameters() {
        LinkedList<DocParameter> params = new LinkedList<DocParameter>();
        for (JsDocElement jsDocElement : this.getTagsForTypes(new JsDocElementType[]{JsDocElementType.PARAM, JsDocElementType.ARGUMENT})) {
            params.add((NamedParameterElement)jsDocElement);
        }
        return params;
    }

    @Override
    public String getDeprecated() {
        List<? extends JsDocElement> deprecatedTags = this.getTagsForType(JsDocElementType.DEPRECATED);
        if (deprecatedTags.isEmpty()) {
            return null;
        }
        return ((DescriptionElement)deprecatedTags.get(0)).getDescription();
    }

    @Override
    public Set<JsModifier> getModifiers() {
        EnumSet<JsModifier> modifiers = EnumSet.noneOf(JsModifier.class);
        for (JsDocElement jsDocElement : this.getTagsForTypes(new JsDocElementType[]{JsDocElementType.PRIVATE, JsDocElementType.PUBLIC, JsDocElementType.STATIC})) {
            modifiers.add(JsModifier.fromString(jsDocElement.getType().toString().substring(1)));
        }
        return modifiers;
    }

    @Override
    public List<DocParameter> getThrows() {
        LinkedList<DocParameter> throwsEntries = new LinkedList<DocParameter>();
        for (JsDocElement exceptionTag : this.getTagsForType(JsDocElementType.THROWS)) {
            throwsEntries.add((UnnamedParameterElement)exceptionTag);
        }
        return throwsEntries;
    }

    @Override
    public List<Type> getExtends() {
        LinkedList<Type> extendsEntries = new LinkedList<Type>();
        for (JsDocElement extend : this.getTagsForTypes(new JsDocElementType[]{JsDocElementType.EXTENDS, JsDocElementType.AUGMENTS})) {
            DeclarationElement ident = (DeclarationElement)extend;
            extendsEntries.add(ident.getDeclaredType());
        }
        return extendsEntries;
    }

    @Override
    public List<String> getSee() {
        LinkedList<String> sees = new LinkedList<String>();
        for (JsDocElement extend : this.getTagsForType(JsDocElementType.SEE)) {
            DescriptionElement element = (DescriptionElement)extend;
            sees.add(element.getDescription());
        }
        return sees;
    }

    @Override
    public String getSince() {
        List<? extends JsDocElement> since = this.getTagsForType(JsDocElementType.SINCE);
        if (since.isEmpty()) {
            return null;
        }
        return ((DescriptionElement)since.get(0)).getDescription();
    }

    @Override
    public boolean isClass() {
        return !this.getTagsForTypes(new JsDocElementType[]{JsDocElementType.CLASS, JsDocElementType.CONSTRUCTOR, JsDocElementType.CONSTRUCTS}).isEmpty();
    }

    @Override
    public List<String> getExamples() {
        LinkedList<String> examples = new LinkedList<String>();
        for (JsDocElement extend : this.getTagsForType(JsDocElementType.EXAMPLE)) {
            DescriptionElement element = (DescriptionElement)extend;
            examples.add(element.getDescription());
        }
        return examples;
    }

    private void initTags(List<JsDocElement> elements) {
        for (JsDocElement jsDocElement : elements) {
            List<JsDocElement> list = this.tags.get((Object)jsDocElement.getType());
            if (list == null) {
                list = new LinkedList<JsDocElement>();
                this.tags.put(jsDocElement.getType(), list);
            }
            this.tags.get((Object)jsDocElement.getType()).add(jsDocElement);
        }
    }

    protected List<? extends JsDocElement> getTags() {
        LinkedList<JsDocElement> allTags = new LinkedList<JsDocElement>();
        for (List<JsDocElement> list : this.tags.values()) {
            allTags.addAll(list);
        }
        return allTags;
    }

    public List<? extends JsDocElement> getTagsForType(JsDocElementType type) {
        List tagsForType = this.tags.get((Object)type);
        return tagsForType == null ? Collections.emptyList() : tagsForType;
    }

    public List<? extends JsDocElement> getTagsForTypes(JsDocElementType[] types) {
        LinkedList<? extends JsDocElement> list = new LinkedList<JsDocElement>();
        for (JsDocElementType type : types) {
            list.addAll(this.getTagsForType(type));
        }
        return list;
    }
}

