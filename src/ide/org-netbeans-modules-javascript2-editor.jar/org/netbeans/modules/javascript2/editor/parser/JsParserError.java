/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.Error$Badging
 *  org.netbeans.modules.csl.api.Severity
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.parser;

import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.javascript2.editor.parser.JsErrorManager;
import org.openide.filesystems.FileObject;

public class JsParserError
implements Error.Badging {
    private final JsErrorManager.SimpleError error;
    private final FileObject file;
    private final boolean wholeLine;
    private final Severity severity;
    private final Object[] parameters;
    private final boolean showExplorerBadge;
    private final boolean showInEditor;

    public JsParserError(JsErrorManager.SimpleError error, FileObject file, Severity severity, Object[] parameters, boolean wholeLine, boolean showExplorerBadge, boolean showInEditor) {
        this.error = error;
        this.file = file;
        this.severity = severity;
        this.parameters = parameters != null ? (Object[])parameters.clone() : new Object[]{};
        this.wholeLine = wholeLine;
        this.showExplorerBadge = showExplorerBadge;
        this.showInEditor = showInEditor;
    }

    public String getDisplayName() {
        return this.error.getMessage();
    }

    public String getDescription() {
        return null;
    }

    public String getKey() {
        int position = this.error.getPosition();
        return "[" + position + "," + position + "]-" + this.error.getMessage();
    }

    public FileObject getFile() {
        return this.file;
    }

    public int getStartPosition() {
        return this.error.getPosition();
    }

    public int getEndPosition() {
        return this.error.getPosition();
    }

    public boolean isLineError() {
        return this.wholeLine;
    }

    public Severity getSeverity() {
        return this.severity;
    }

    public Object[] getParameters() {
        return this.parameters;
    }

    public boolean showExplorerBadge() {
        return this.showExplorerBadge;
    }

    public boolean showInEditor() {
        return this.showInEditor;
    }
}

