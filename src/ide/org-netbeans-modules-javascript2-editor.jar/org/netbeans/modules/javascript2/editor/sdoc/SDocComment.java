/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.sdoc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsModifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocDescriptionElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocIdentifierElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeDescribedElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeNamedElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeSimpleElement;

public class SDocComment
extends JsComment {
    private final Map<SDocElementType, List<SDocElement>> tags = new EnumMap<SDocElementType, List<SDocElement>>(SDocElementType.class);

    public SDocComment(OffsetRange offsetRange, List<SDocElement> elements) {
        super(offsetRange);
        this.initComment(elements);
    }

    @Override
    public List<String> getSummary() {
        LinkedList<String> summaries = new LinkedList<String>();
        for (SDocElement sDocElement : this.getTagsForTypes(new SDocElementType[]{SDocElementType.DESCRIPTION, SDocElementType.CLASS_DESCRIPTION, SDocElementType.PROJECT_DESCRIPTION})) {
            summaries.add(((SDocDescriptionElement)sDocElement).getDescription());
        }
        return summaries;
    }

    @Override
    public List<String> getSyntax() {
        return Collections.emptyList();
    }

    @Override
    public DocParameter getReturnType() {
        Iterator<? extends SDocElement> i$ = this.getTagsForTypes(new SDocElementType[]{SDocElementType.RETURN, SDocElementType.TYPE, SDocElementType.PROPERTY}).iterator();
        if (i$.hasNext()) {
            SDocElement sDocElement = i$.next();
            return (SDocTypeSimpleElement)sDocElement;
        }
        return null;
    }

    @Override
    public List<DocParameter> getParameters() {
        LinkedList<DocParameter> params = new LinkedList<DocParameter>();
        for (SDocElement jsDocElement : this.getTagsForType(SDocElementType.PARAM)) {
            params.add((SDocTypeNamedElement)jsDocElement);
        }
        return params;
    }

    @Override
    public String getDeprecated() {
        if (this.getTagsForType(SDocElementType.DEPRECATED).isEmpty()) {
            return null;
        }
        return "";
    }

    @Override
    public Set<JsModifier> getModifiers() {
        EnumSet<JsModifier> modifiers = EnumSet.noneOf(JsModifier.class);
        for (SDocElement jsDocElement : this.getTagsForType(SDocElementType.PRIVATE)) {
            modifiers.add(JsModifier.fromString(jsDocElement.getType().toString().substring(1)));
        }
        return modifiers;
    }

    @Override
    public List<DocParameter> getThrows() {
        LinkedList<DocParameter> throwsEntries = new LinkedList<DocParameter>();
        for (SDocElement exceptionTag : this.getTagsForType(SDocElementType.EXCEPTION)) {
            throwsEntries.add((SDocTypeDescribedElement)exceptionTag);
        }
        return throwsEntries;
    }

    @Override
    public List<Type> getExtends() {
        LinkedList<Type> extendsEntries = new LinkedList<Type>();
        for (SDocElement extend : this.getTagsForType(SDocElementType.INHERITS)) {
            SDocIdentifierElement ident = (SDocIdentifierElement)extend;
            extendsEntries.add(new TypeUsageImpl(ident.getIdentifier(), -1));
        }
        return extendsEntries;
    }

    @Override
    public List<String> getSee() {
        LinkedList<String> sees = new LinkedList<String>();
        for (SDocElement extend : this.getTagsForType(SDocElementType.SEE)) {
            SDocDescriptionElement element = (SDocDescriptionElement)extend;
            sees.add(element.getDescription());
        }
        return sees;
    }

    @Override
    public String getSince() {
        List<? extends SDocElement> since = this.getTagsForType(SDocElementType.SINCE);
        if (since.isEmpty()) {
            return null;
        }
        return ((SDocDescriptionElement)since.get(0)).getDescription();
    }

    @Override
    public boolean isClass() {
        return !this.getTagsForTypes(new SDocElementType[]{SDocElementType.CONSTRUCTOR}).isEmpty();
    }

    @Override
    public List<String> getExamples() {
        LinkedList<String> examples = new LinkedList<String>();
        for (SDocElement extend : this.getTagsForType(SDocElementType.EXAMPLE)) {
            SDocDescriptionElement element = (SDocDescriptionElement)extend;
            examples.add(element.getDescription());
        }
        return examples;
    }

    private void initComment(List<SDocElement> elements) {
        for (SDocElement element : elements) {
            List<SDocElement> list = this.tags.get((Object)element.getType());
            if (list == null) {
                list = new LinkedList<SDocElement>();
                this.tags.put(element.getType(), list);
            }
            this.tags.get((Object)element.getType()).add(element);
        }
    }

    protected List<? extends SDocElement> getTags() {
        ArrayList<SDocElement> allTags = new ArrayList<SDocElement>();
        for (List<SDocElement> list : this.tags.values()) {
            allTags.addAll(list);
        }
        return allTags;
    }

    public List<? extends SDocElement> getTagsForType(SDocElementType type) {
        List tagsForType = this.tags.get((Object)type);
        return tagsForType == null ? Collections.emptyList() : tagsForType;
    }

    public List<? extends SDocElement> getTagsForTypes(SDocElementType[] types) {
        LinkedList<? extends SDocElement> list = new LinkedList<SDocElement>();
        for (SDocElementType type : types) {
            list.addAll(this.getTagsForType(type));
        }
        return list;
    }
}

