/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexDocument
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.index;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.StringTokenizer;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.model.JsArray;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsElementImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;

public class IndexedElement
implements JsElement {
    private final JsElement.Kind jsKind;
    private final String fqn;
    private final boolean isAnonymous;
    private final boolean isPlatform;
    private final Collection<TypeUsage> assignments;
    public static final char ANONYMOUS_POSFIX = 'A';
    public static final char OBJECT_POSFIX = 'O';
    public static final char PARAMETER_POSTFIX = 'P';
    private final FileObject fileObject;
    private final String name;
    private final boolean isDeclared;
    private final Set<Modifier> modifiers;
    private final OffsetRange offsetRange;

    public IndexedElement(FileObject fileObject, String name, String fqn, boolean isDeclared, boolean isAnonymous, JsElement.Kind kind, OffsetRange offsetRange, Set<Modifier> modifiers, Collection<TypeUsage> assignments, boolean isPlatform) {
        this.jsKind = kind;
        this.fqn = fqn;
        this.isAnonymous = isAnonymous;
        this.assignments = assignments;
        this.isPlatform = isPlatform;
        this.fileObject = fileObject;
        this.name = name;
        this.isDeclared = isDeclared;
        this.modifiers = modifiers;
        this.offsetRange = offsetRange;
    }

    @Override
    public JsElement.Kind getJSKind() {
        return this.jsKind;
    }

    public String getFQN() {
        return this.fqn;
    }

    public boolean isAnonymous() {
        return this.isAnonymous;
    }

    public Collection<TypeUsage> getAssignments() {
        return this.assignments;
    }

    @Override
    public boolean isPlatform() {
        return this.isPlatform;
    }

    protected static IndexDocument createDocument(JsObject object, String fqn, IndexingSupport support, Indexable indexable) {
        IndexDocument elementDocument = support.createDocument(indexable);
        elementDocument.addPair("bn", object.getName(), true, true);
        elementDocument.addPair("fqn", fqn + (char)(object.isAnonymous() ? 65 : (object.getJSKind() == JsElement.Kind.PARAMETER ? 80 : 79)), true, true);
        elementDocument.addPair("offset", Integer.toString(object.getOffset()), true, true);
        elementDocument.addPair("flag", Integer.toString(Flag.getFlag(object)), false, true);
        StringBuilder sb = new StringBuilder();
        for (TypeUsage type2 : object.getAssignments()) {
            sb.append(type2.getType());
            sb.append(":");
            sb.append(type2.getOffset());
            sb.append(":");
            sb.append(type2.isResolved() ? "1" : "0");
            sb.append("|");
        }
        elementDocument.addPair("assign", sb.toString(), false, true);
        if (object.getJSKind().isFunction()) {
            sb = new StringBuilder();
            for (TypeUsage type : ((JsFunction)object).getReturnTypes()) {
                sb.append(type.getType());
                sb.append(",");
                sb.append(type.getOffset());
                sb.append(",");
                sb.append(type.isResolved() ? "1" : "0");
                sb.append("|");
            }
            elementDocument.addPair("return", sb.toString(), false, true);
            elementDocument.addPair("param", IndexedElement.codeParameters(((JsFunction)object).getParameters()), false, true);
        }
        if (object instanceof JsArray) {
            sb = new StringBuilder();
            for (TypeUsage type : ((JsArray)object).getTypesInArray()) {
                sb.append(type.getType());
                sb.append(",");
                sb.append(type.getOffset());
                sb.append(",");
                sb.append(type.isResolved() ? "1" : "0");
                sb.append("|");
            }
            elementDocument.addPair("array", sb.toString(), false, true);
        }
        return elementDocument;
    }

    protected static IndexDocument createDocumentForReference(JsObjectReference object, String fqn, IndexingSupport support, Indexable indexable) {
        IndexDocument elementDocument = support.createDocument(indexable);
        elementDocument.addPair("bn", object.getName(), true, true);
        elementDocument.addPair("fqn", fqn + (char)(object.isAnonymous() ? 65 : (object.getJSKind() == JsElement.Kind.PARAMETER ? 80 : 79)), true, true);
        elementDocument.addPair("offset", Integer.toString(object.getOffset()), true, true);
        elementDocument.addPair("flag", Integer.toString(Flag.getFlag(object)), false, true);
        StringBuilder sb = new StringBuilder();
        sb.append(object.getOriginal().getFullyQualifiedName());
        sb.append(":");
        sb.append(object.getOffset());
        sb.append(":");
        sb.append("1");
        elementDocument.addPair("assign", sb.toString(), false, true);
        if (object.getJSKind().isFunction()) {
            sb = new StringBuilder();
            for (TypeUsage type : ((JsFunction)((Object)object)).getReturnTypes()) {
                sb.append(type.getType());
                sb.append(",");
                sb.append(type.getOffset());
                sb.append(",");
                sb.append(type.isResolved() ? "1" : "0");
                sb.append("|");
            }
            elementDocument.addPair("return", sb.toString(), false, true);
            elementDocument.addPair("param", IndexedElement.codeParameters(((JsFunction)((Object)object)).getParameters()), false, true);
        }
        if (object instanceof JsArray) {
            sb = new StringBuilder();
            for (TypeUsage type : ((JsArray)((Object)object)).getTypesInArray()) {
                sb.append(type.getType());
                sb.append(",");
                sb.append(type.getOffset());
                sb.append(",");
                sb.append(type.isResolved() ? "1" : "0");
                sb.append("|");
            }
            elementDocument.addPair("array", sb.toString(), false, true);
        }
        return elementDocument;
    }

    public static IndexedElement create(IndexResult indexResult) {
        IndexedElement result;
        FileObject fo = indexResult.getFile();
        String name = indexResult.getValue("bn");
        String fqn = IndexedElement.getFQN(indexResult);
        int flag = Integer.parseInt(indexResult.getValue("flag"));
        boolean isDeclared = Flag.isDeclared(flag);
        boolean isAnonymous = Flag.isAnonymous(flag);
        JsElement.Kind kind = Flag.getJsKind(flag);
        Set<Modifier> modifiers = Flag.getModifiers(flag);
        int offset = Integer.parseInt(indexResult.getValue("offset"));
        Collection<TypeUsage> assignments = IndexedElement.getAssignments(indexResult);
        boolean isPlatform = Flag.isPlatform(flag);
        if (!kind.isFunction()) {
            result = new IndexedElement(fo, name, fqn, isDeclared, isAnonymous, kind, offset > -1 ? new OffsetRange(offset, offset + name.length()) : OffsetRange.NONE, modifiers, assignments, isPlatform);
        } else {
            Collection<TypeUsage> returnTypes = IndexedElement.getReturnTypes(indexResult);
            ArrayList<String> rTypes = new ArrayList<String>();
            for (TypeUsage type : returnTypes) {
                rTypes.add(type.getType());
            }
            String paramText = indexResult.getValue("param");
            LinkedHashMap<String, Collection<String>> params = IndexedElement.decodeParameters(paramText);
            result = new FunctionIndexedElement(fo, name, fqn, new OffsetRange(offset, offset + name.length()), flag, params, rTypes, assignments);
        }
        return result;
    }

    public static Collection<TypeUsage> getAssignments(IndexResult indexResult) {
        return IndexedElement.getAssignments(indexResult.getValue("assign"));
    }

    public static String getFQN(IndexResult indexResult) {
        String fqn = indexResult.getValue("fqn");
        fqn = fqn.substring(0, fqn.length() - 1);
        return fqn;
    }

    private static Collection<TypeUsage> getAssignments(String sAssignments) {
        ArrayList<TypeUsage> result = new ArrayList<TypeUsage>();
        if (sAssignments != null) {
            StringTokenizer st = new StringTokenizer(sAssignments, "|");
            while (st.hasMoreTokens()) {
                int offset;
                String token = st.nextToken();
                String[] parts = token.split(":");
                if (parts.length <= 2) continue;
                String type = parts[0];
                String sOffset = parts[1];
                try {
                    offset = Integer.parseInt(sOffset);
                }
                catch (NumberFormatException nfe) {
                    offset = -1;
                }
                boolean resolve = parts[2].equals("1");
                result.add(new TypeUsageImpl(type, offset, resolve));
            }
        }
        return result;
    }

    public static Collection<TypeUsage> getReturnTypes(IndexResult indexResult) {
        return IndexedElement.getTypes(indexResult, "return");
    }

    public static Collection<TypeUsage> getArrayTypes(IndexResult indexResult) {
        return IndexedElement.getTypes(indexResult, "array");
    }

    public static Collection<TypeUsage> getTypes(IndexResult indexResult, String field) {
        ArrayList<TypeUsage> result = new ArrayList<TypeUsage>();
        String text = indexResult.getValue(field);
        if (text != null) {
            StringTokenizer st = new StringTokenizer(text, "|");
            while (st.hasMoreTokens()) {
                int offset;
                String token = st.nextToken();
                String[] parts = token.split(",");
                if (parts.length <= 2) continue;
                try {
                    offset = Integer.parseInt(parts[1]);
                }
                catch (NumberFormatException nfe) {
                    offset = -1;
                }
                boolean resolve = parts[2].equals("1");
                result.add(new TypeUsageImpl(parts[0], offset, resolve));
            }
        }
        return result;
    }

    private static String codeParameters(Collection<? extends JsObject> params) {
        StringBuilder result = new StringBuilder();
        Iterator<? extends JsObject> it = params.iterator();
        while (it.hasNext()) {
            JsObject parametr = it.next();
            result.append(parametr.getName());
            result.append(":");
            Iterator<? extends TypeUsage> itType = parametr.getAssignmentForOffset(parametr.getOffset() + 1).iterator();
            while (itType.hasNext()) {
                TypeUsage type = itType.next();
                result.append(type.getType());
                if (!itType.hasNext()) continue;
                result.append("|");
            }
            if (!it.hasNext()) continue;
            result.append(',');
        }
        return result.toString();
    }

    private static LinkedHashMap<String, Collection<String>> decodeParameters(String paramsText) {
        LinkedHashMap<String, Collection<String>> parameters = new LinkedHashMap<String, Collection<String>>();
        StringTokenizer stringTokenizer = new StringTokenizer(paramsText, ",");
        while (stringTokenizer.hasMoreTokens()) {
            String paramName;
            String param = stringTokenizer.nextToken();
            int index = param.indexOf(58);
            ArrayList<String> types = new ArrayList<String>();
            if (index > 0) {
                paramName = param.substring(0, index);
                String typesText = param.substring(index + 1);
                StringTokenizer stParamType = new StringTokenizer(typesText, "|");
                while (stParamType.hasMoreTokens()) {
                    types.add(stParamType.nextToken());
                }
            } else {
                paramName = param;
            }
            parameters.put(paramName, types);
        }
        return parameters;
    }

    @Override
    public int getOffset() {
        return this.offsetRange.getStart();
    }

    @Override
    public OffsetRange getOffsetRange() {
        return this.offsetRange;
    }

    @Override
    public boolean isDeclared() {
        return this.isDeclared;
    }

    @Override
    public String getSourceLabel() {
        return null;
    }

    public FileObject getFileObject() {
        return this.fileObject;
    }

    public String getMimeType() {
        return "text/javascript";
    }

    public String getName() {
        return this.name;
    }

    public String getIn() {
        return null;
    }

    public ElementKind getKind() {
        return JsElementImpl.convertJsKindToElementKind(this.jsKind);
    }

    public Set<Modifier> getModifiers() {
        return this.modifiers;
    }

    public boolean signatureEquals(ElementHandle handle) {
        return false;
    }

    public OffsetRange getOffsetRange(ParserResult result) {
        return this.getOffsetRange();
    }

    public static class Flag {
        private static final int PRIVATE = 1;
        private static final int PUBLIC = 2;
        private static final int STATIC = 4;
        private static final int PRIVILAGE = 8;
        private static final int DEPRICATED = 16;
        private static final int GLOBAL = 32;
        private static final int DECLARED = 64;
        private static final int ANONYMOUS = 128;
        private static final int FILE = 256;
        private static final int PROPERTY = 512;
        private static final int VARIABLE = 1024;
        private static final int OBJECT = 2048;
        private static final int METHOD = 4096;
        private static final int FUNCTION = 8192;
        private static final int ANONYMOUS_OBJECT = 16384;
        private static final int CONSTRUCTOR = 32768;
        private static final int FIELD = 65536;
        private static final int PARAMETER = 131072;
        private static final int PROPERTY_GETTER = 262144;
        private static final int PROPERTY_SETTER = 524288;
        private static final int PLATFORM = 1048576;
        private static final int OBJECT_LITERAL = 2097152;

        public static int getFlag(JsObject object) {
            JsElement.Kind kind;
            int value = 0;
            Set modifiers = object.getModifiers();
            if (modifiers.contains((Object)Modifier.PRIVATE)) {
                value |= true;
            }
            if (modifiers.contains((Object)Modifier.PUBLIC)) {
                value |= 2;
            }
            if (modifiers.contains((Object)Modifier.STATIC)) {
                value |= 4;
            }
            if (modifiers.contains((Object)Modifier.PROTECTED)) {
                value |= 8;
            }
            if (modifiers.contains((Object)Modifier.DEPRECATED)) {
                value |= 16;
            }
            if (ModelUtils.isGlobal(object)) {
                value |= 32;
            }
            if (object.isDeclared()) {
                value |= 64;
            }
            if (object.isAnonymous()) {
                value |= 128;
            }
            if ((kind = object.getJSKind()) == JsElement.Kind.ANONYMOUS_OBJECT) {
                value |= 16384;
            }
            if (kind == JsElement.Kind.CONSTRUCTOR) {
                value |= 32768;
            }
            if (kind == JsElement.Kind.FIELD) {
                value |= 65536;
            }
            if (kind == JsElement.Kind.FILE) {
                value |= 256;
            }
            if (kind == JsElement.Kind.FUNCTION) {
                value |= 8192;
            }
            if (kind == JsElement.Kind.METHOD) {
                value |= 4096;
            }
            if (kind == JsElement.Kind.OBJECT) {
                value |= 2048;
            }
            if (kind == JsElement.Kind.PARAMETER) {
                value |= 131072;
            }
            if (kind == JsElement.Kind.PROPERTY) {
                value |= 512;
            }
            if (kind == JsElement.Kind.PROPERTY_GETTER) {
                value |= 262144;
            }
            if (kind == JsElement.Kind.PROPERTY_SETTER) {
                value |= 524288;
            }
            if (kind == JsElement.Kind.VARIABLE) {
                value |= 1024;
            }
            if (kind == JsElement.Kind.OBJECT_LITERAL) {
                value |= 2097152;
            }
            if (object.isPlatform()) {
                value |= 1048576;
            }
            return value;
        }

        public static Set<Modifier> getModifiers(int flag) {
            EnumSet<Modifier> result = EnumSet.noneOf(Modifier.class);
            if ((flag & 1) != 0) {
                result.add(Modifier.PRIVATE);
            }
            if ((flag & 2) != 0) {
                result.add(Modifier.PUBLIC);
            }
            if ((flag & 4) != 0) {
                result.add(Modifier.STATIC);
            }
            if ((flag & 8) != 0) {
                result.add(Modifier.PROTECTED);
            }
            if ((flag & 16) != 0) {
                result.add(Modifier.DEPRECATED);
            }
            return result;
        }

        public static boolean isGlobal(int flag) {
            return (flag & 32) != 0;
        }

        public static boolean isDeclared(int flag) {
            return (flag & 64) != 0;
        }

        public static boolean isAnonymous(int flag) {
            return (flag & 128) != 0;
        }

        public static boolean isPlatform(int flag) {
            return (flag & 1048576) != 0;
        }

        public static JsElement.Kind getJsKind(int flag) {
            JsElement.Kind result = JsElement.Kind.VARIABLE;
            if ((flag & 16384) != 0) {
                result = JsElement.Kind.ANONYMOUS_OBJECT;
            } else if ((flag & 32768) != 0) {
                result = JsElement.Kind.CONSTRUCTOR;
            } else if ((flag & 65536) != 0) {
                result = JsElement.Kind.FIELD;
            } else if ((flag & 256) != 0) {
                result = JsElement.Kind.FILE;
            } else if ((flag & 8192) != 0) {
                result = JsElement.Kind.FUNCTION;
            } else if ((flag & 4096) != 0) {
                result = JsElement.Kind.METHOD;
            } else if ((flag & 2048) != 0) {
                result = JsElement.Kind.OBJECT;
            } else if ((flag & 131072) != 0) {
                result = JsElement.Kind.PARAMETER;
            } else if ((flag & 512) != 0) {
                result = JsElement.Kind.PROPERTY;
            } else if ((flag & 262144) != 0) {
                result = JsElement.Kind.PROPERTY_GETTER;
            } else if ((flag & 524288) != 0) {
                result = JsElement.Kind.PROPERTY_SETTER;
            } else if ((flag & 1024) != 0) {
                result = JsElement.Kind.VARIABLE;
            } else if ((flag & 2097152) != 0) {
                result = JsElement.Kind.OBJECT_LITERAL;
            }
            return result;
        }
    }

    public static class FunctionIndexedElement
    extends IndexedElement {
        private final LinkedHashMap<String, Collection<String>> parameters;
        private final Collection<String> returnTypes;

        public FunctionIndexedElement(FileObject fileObject, String name, String fqn, OffsetRange offsetRange, int flag, LinkedHashMap<String, Collection<String>> parameters, Collection<String> returnTypes, Collection<TypeUsage> assignments) {
            super(fileObject, name, fqn, Flag.isDeclared(flag), Flag.isAnonymous(flag), Flag.getJsKind(flag), offsetRange, Flag.getModifiers(flag), assignments, Flag.isPlatform(flag));
            this.parameters = parameters;
            this.returnTypes = returnTypes;
        }

        public LinkedHashMap<String, Collection<String>> getParameters() {
            return this.parameters;
        }

        public Collection<String> getReturnTypes() {
            return this.returnTypes;
        }
    }

}

