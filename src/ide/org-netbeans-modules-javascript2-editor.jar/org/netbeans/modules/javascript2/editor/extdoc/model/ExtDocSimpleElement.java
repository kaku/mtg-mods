/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocBaseElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;

public class ExtDocSimpleElement
extends ExtDocBaseElement {
    private ExtDocSimpleElement(ExtDocElementType type) {
        super(type);
    }

    public static ExtDocSimpleElement create(ExtDocElementType type) {
        return new ExtDocSimpleElement(type);
    }
}

