/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

import java.util.List;
import java.util.Set;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.parsing.api.Snapshot;

public interface JsDocumentationProvider {
    public JsDocumentationHolder createDocumentationHolder(Snapshot var1);

    public Set<String> getSupportedTags();

    public List<? extends AnnotationCompletionTagProvider> getAnnotationsProvider();

    public SyntaxProvider getSyntaxProvider();
}

