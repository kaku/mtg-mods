/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;

public class JsObjectReference
extends JsObjectImpl {
    private final JsObject original;
    private final Set<Modifier> modifiers;

    public JsObjectReference(JsObject parent, Identifier declarationName, @NonNull JsObject original, boolean isDeclared, Set<Modifier> modifiers) {
        super(parent, declarationName, declarationName.getOffsetRange(), isDeclared, modifiers == null ? EnumSet.noneOf(Modifier.class) : modifiers, original.getMimeType(), original.getSourceLabel());
        this.original = original;
        this.modifiers = modifiers;
    }

    @Override
    public Map<String, ? extends JsObject> getProperties() {
        return this.original.getProperties();
    }

    @Override
    public void addProperty(String name, JsObject property) {
        this.original.addProperty(name, property);
    }

    @Override
    public JsObject getProperty(String name) {
        return this.original.getProperty(name);
    }

    @Override
    public boolean isAnonymous() {
        return false;
    }

    @Override
    public JsElement.Kind getJSKind() {
        JsElement.Kind kind = this.original.getJSKind();
        if (kind == JsElement.Kind.ANONYMOUS_OBJECT) {
            kind = JsElement.Kind.OBJECT_LITERAL;
        }
        return kind;
    }

    @Override
    public ElementKind getKind() {
        return this.original.getKind();
    }

    @Override
    public Set<Modifier> getModifiers() {
        if (this.modifiers != null) {
            return this.modifiers;
        }
        return this.original.getModifiers();
    }

    public JsObject getOriginal() {
        return this.original;
    }

    @Override
    public Collection<? extends TypeUsage> getAssignmentForOffset(int offset) {
        return this.original.getAssignmentForOffset(offset);
    }

    @Override
    public Collection<? extends TypeUsage> getAssignments() {
        return this.original.getAssignments();
    }

    @Override
    public void resolveTypes(JsDocumentationHolder docHolder) {
    }

    @Override
    public Documentation getDocumentation() {
        return this.original.getDocumentation();
    }
}

