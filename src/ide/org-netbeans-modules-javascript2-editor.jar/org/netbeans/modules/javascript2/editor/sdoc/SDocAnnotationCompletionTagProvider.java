/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc;

import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.sdoc.completion.DescriptionTag;
import org.netbeans.modules.javascript2.editor.sdoc.completion.TypeDescribedTag;
import org.netbeans.modules.javascript2.editor.sdoc.completion.TypeNamedTag;
import org.netbeans.modules.javascript2.editor.sdoc.completion.TypeSimpleTag;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public class SDocAnnotationCompletionTagProvider
extends AnnotationCompletionTagProvider {
    List<AnnotationCompletionTag> annotations = null;

    public SDocAnnotationCompletionTagProvider(String name) {
        super(name);
    }

    @Override
    public synchronized List<AnnotationCompletionTag> getAnnotations() {
        if (this.annotations == null) {
            this.initAnnotations();
        }
        return this.annotations;
    }

    private void initAnnotations() {
        this.annotations = new LinkedList<AnnotationCompletionTag>();
        block6 : for (SDocElementType type : SDocElementType.values()) {
            if (type == SDocElementType.UNKNOWN) continue;
            switch (type.getCategory()) {
                case DESCRIPTION: {
                    this.annotations.add(new DescriptionTag(type.toString()));
                    continue block6;
                }
                case IDENT: {
                    this.annotations.add(new TypeSimpleTag(type.toString()));
                    continue block6;
                }
                case TYPE_NAMED: {
                    this.annotations.add(new TypeNamedTag(type.toString()));
                    continue block6;
                }
                case TYPE_DESCRIBED: {
                    this.annotations.add(new TypeDescribedTag(type.toString()));
                    continue block6;
                }
                default: {
                    this.annotations.add(new AnnotationCompletionTag(type.toString(), type.toString()));
                }
            }
        }
    }

}

