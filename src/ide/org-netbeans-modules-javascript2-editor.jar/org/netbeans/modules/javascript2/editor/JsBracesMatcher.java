/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcher
 *  org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory
 *  org.netbeans.spi.editor.bracesmatching.MatcherContext
 *  org.netbeans.spi.editor.bracesmatching.support.BracesMatcherSupport
 */
package org.netbeans.modules.javascript2.editor;

import java.util.List;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.javascript2.editor.TokenSequenceIterator;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.bracesmatching.support.BracesMatcherSupport;

public class JsBracesMatcher
implements BracesMatcher {
    private static final char[] PAIRS = new char[]{'(', ')', '[', ']', '{', '}'};
    private static final JsTokenId[] PAIR_TOKEN_IDS = new JsTokenId[]{JsTokenId.BRACKET_LEFT_PAREN, JsTokenId.BRACKET_RIGHT_PAREN, JsTokenId.BRACKET_LEFT_BRACKET, JsTokenId.BRACKET_RIGHT_BRACKET, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY};
    private final MatcherContext context;
    private final Language<JsTokenId> language;
    private int originOffset;
    private char originChar;
    private char matchingChar;
    private boolean backward;
    private List<TokenSequence<?>> sequences;

    public JsBracesMatcher(MatcherContext context, Language<JsTokenId> language) {
        this.context = context;
        this.language = language;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] findOrigin() throws InterruptedException, BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            int[] origin = BracesMatcherSupport.findChar((Document)this.context.getDocument(), (int)this.context.getSearchOffset(), (int)this.context.getLimitOffset(), (char[])PAIRS);
            if (origin != null) {
                int[] seq;
                this.originOffset = origin[0];
                this.originChar = PAIRS[origin[1]];
                this.matchingChar = PAIRS[origin[1] + origin[2]];
                this.backward = origin[2] < 0;
                TokenHierarchy th = TokenHierarchy.get((Document)this.context.getDocument());
                this.sequences = JsBracesMatcher.getEmbeddedTokenSequences(th, this.originOffset, this.backward, this.language);
                if (!this.sequences.isEmpty()) {
                    seq = this.sequences.get(this.sequences.size() - 1);
                    seq.move(this.originOffset);
                    if (seq.moveNext() && (seq.token().id() == JsTokenId.BLOCK_COMMENT || seq.token().id() == JsTokenId.DOC_COMMENT || seq.token().id() == JsTokenId.LINE_COMMENT || seq.token().id() == JsTokenId.REGEXP || seq.token().id() == JsTokenId.STRING)) {
                        int[] arrn = null;
                        return arrn;
                    }
                }
                seq = new int[]{this.originOffset, this.originOffset + 1};
                return seq;
            }
            int[] th = null;
            return th;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] findMatches() throws InterruptedException, BadLocationException {
        ((AbstractDocument)this.context.getDocument()).readLock();
        try {
            TokenSequence seq;
            if (!this.sequences.isEmpty()) {
                seq = this.sequences.get(this.sequences.size() - 1);
                TokenHierarchy th = TokenHierarchy.get((Document)this.context.getDocument());
                List list = this.backward ? th.tokenSequenceList(seq.languagePath(), 0, this.originOffset) : th.tokenSequenceList(seq.languagePath(), this.originOffset + 1, this.context.getDocument().getLength());
                JsTokenId originId = this.getTokenId(this.originChar);
                JsTokenId lookingForId = this.getTokenId(this.matchingChar);
                int counter = 0;
                TokenSequenceIterator tsi = new TokenSequenceIterator(list, this.backward);
                while (tsi.hasMore()) {
                    TokenSequence sq = tsi.getSequence();
                    if (originId == sq.token().id()) {
                        ++counter;
                        continue;
                    }
                    if (lookingForId != sq.token().id()) continue;
                    if (counter == 0) {
                        int[] arrn = new int[]{sq.offset(), sq.offset() + sq.token().length()};
                        return arrn;
                    }
                    --counter;
                }
            }
            seq = null;
            return seq;
        }
        finally {
            ((AbstractDocument)this.context.getDocument()).readUnlock();
        }
    }

    public static List<TokenSequence<?>> getEmbeddedTokenSequences(TokenHierarchy<?> th, int offset, boolean backwardBias, Language<?> language) {
        TokenSequence seq;
        List sequences = th.embeddedTokenSequences(offset, backwardBias);
        for (int i = sequences.size() - 1; i >= 0 && (seq = (TokenSequence)sequences.get(i)).language() != language; --i) {
            sequences.remove(i);
        }
        return sequences;
    }

    private JsTokenId getTokenId(char ch) {
        for (int i = 0; i < PAIRS.length; ++i) {
            if (PAIRS[i] != ch) continue;
            return PAIR_TOKEN_IDS[i];
        }
        return null;
    }

    public static class JsonBracesMatcherFactory
    implements BracesMatcherFactory {
        public BracesMatcher createMatcher(MatcherContext context) {
            return new JsBracesMatcher(context, JsTokenId.jsonLanguage());
        }
    }

    public static class JsBracesMatcherFactory
    implements BracesMatcherFactory {
        public BracesMatcher createMatcher(MatcherContext context) {
            return new JsBracesMatcher(context, JsTokenId.javascriptLanguage());
        }
    }

}

