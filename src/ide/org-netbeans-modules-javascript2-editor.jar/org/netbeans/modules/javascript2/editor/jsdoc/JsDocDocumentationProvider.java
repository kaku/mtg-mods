/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.jsdoc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocAnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocDocumentationHolder;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocSyntaxProvider;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsDocDocumentationProvider
implements JsDocumentationProvider {
    private Set<String> supportedTags;
    private static final List<AnnotationCompletionTagProvider> ANNOTATION_PROVIDERS = Arrays.asList(new JsDocAnnotationCompletionTagProvider("JsDoc"));
    private static final SyntaxProvider SYNTAX_PROVIDER = new JsDocSyntaxProvider();

    @Override
    public JsDocumentationHolder createDocumentationHolder(Snapshot snapshot) {
        return new JsDocDocumentationHolder(this, snapshot);
    }

    public synchronized Set getSupportedTags() {
        if (this.supportedTags == null) {
            this.supportedTags = new HashSet<String>(JsDocElementType.values().length);
            for (JsDocElementType type : JsDocElementType.values()) {
                this.supportedTags.add(type.toString());
            }
            this.supportedTags.remove("unknown");
            this.supportedTags.remove("contextSensitive");
        }
        return this.supportedTags;
    }

    public List<AnnotationCompletionTagProvider> getAnnotationsProvider() {
        return ANNOTATION_PROVIDERS;
    }

    @Override
    public SyntaxProvider getSyntaxProvider() {
        return SYNTAX_PROVIDER;
    }
}

