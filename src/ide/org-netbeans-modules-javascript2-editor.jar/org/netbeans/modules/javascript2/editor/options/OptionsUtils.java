/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.lexer.Language
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.javascript2.editor.options;

import java.util.EventListener;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.openide.util.WeakListeners;

public final class OptionsUtils {
    private static final Map<Language<JsTokenId>, OptionsUtils> INSTANCES = new WeakHashMap<Language<JsTokenId>, OptionsUtils>();
    public static final String AUTO_COMPLETION_TYPE_RESOLUTION = "codeCompletionTypeResolution";
    public static final String AUTO_COMPLETION_SMART_QUOTES = "codeCompletionSmartQuotes";
    public static final String AUTO_STRING_CONCATINATION = "codeCompletionStringAutoConcatination";
    public static final String AUTO_COMPLETION_FULL = "autoCompletionFull";
    public static final String AUTO_COMPLETION_AFTER_DOT = "autoCompletionAfterDot";
    public static final String COMPETION_ITEM_SIGNATURE_WIDTH = "codeComletionItemDescriptionWith";
    public static final boolean AUTO_COMPLETION_TYPE_RESOLUTION_DEFAULT = false;
    public static final boolean AUTO_COMPLETION_SMART_QUOTES_DEFAULT = true;
    public static final boolean AUTO_STRING_CONCATINATION_DEFAULT = true;
    public static final boolean AUTO_COMPLETION_FULL_DEFAULT = false;
    public static final boolean AUTO_COMPLETION_AFTER_DOT_DEFAULT = true;
    public static final int COMPETION_ITEM_SIGNATURE_WIDTH_DEFAULT = 40;
    private final AtomicBoolean inited = new AtomicBoolean(false);
    private final PreferenceChangeListener preferencesTracker;
    private final String mimeType;
    private Preferences preferences;
    private Boolean autoCompletionTypeResolution;
    private Boolean autoCompletionSmartQuotes;
    private Boolean autoStringConcatination;
    private Boolean autoCompletionFull;
    private Boolean autoCompletionAfterDot;
    private Integer codeCompletionItemSignatureWidth;

    private OptionsUtils(Language<JsTokenId> language) {
        this.preferencesTracker = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String settingName;
                String string = settingName = evt == null ? null : evt.getKey();
                if (settingName == null || "codeCompletionTypeResolution".equals(settingName)) {
                    OptionsUtils.this.autoCompletionTypeResolution = OptionsUtils.this.preferences.getBoolean("codeCompletionTypeResolution", false);
                }
                if (settingName == null || "codeCompletionSmartQuotes".equals(settingName)) {
                    OptionsUtils.this.autoCompletionSmartQuotes = OptionsUtils.this.preferences.getBoolean("codeCompletionSmartQuotes", true);
                }
                if (settingName == null || "codeCompletionStringAutoConcatination".equals(settingName)) {
                    OptionsUtils.this.autoStringConcatination = OptionsUtils.this.preferences.getBoolean("codeCompletionStringAutoConcatination", true);
                }
                if (settingName == null || "autoCompletionFull".equals(settingName)) {
                    OptionsUtils.this.autoCompletionFull = OptionsUtils.this.preferences.getBoolean("autoCompletionFull", false);
                }
                if (settingName == null || "autoCompletionAfterDot".equals(settingName)) {
                    OptionsUtils.this.autoCompletionAfterDot = OptionsUtils.this.preferences.getBoolean("autoCompletionAfterDot", true);
                }
                if (settingName == null || "codeComletionItemDescriptionWith".equals(settingName)) {
                    OptionsUtils.this.codeCompletionItemSignatureWidth = OptionsUtils.this.preferences.getInt("codeComletionItemDescriptionWith", 40);
                }
            }
        };
        this.autoCompletionTypeResolution = null;
        this.autoCompletionSmartQuotes = null;
        this.autoStringConcatination = null;
        this.autoCompletionFull = null;
        this.autoCompletionAfterDot = null;
        this.codeCompletionItemSignatureWidth = null;
        this.mimeType = language.mimeType();
    }

    public static synchronized OptionsUtils forLanguage(Language<JsTokenId> language) {
        OptionsUtils instance = INSTANCES.get(language);
        if (instance == null) {
            instance = new OptionsUtils(language);
            INSTANCES.put(language, instance);
        }
        return instance;
    }

    public boolean autoCompletionTypeResolution() {
        this.lazyInit();
        assert (this.autoCompletionTypeResolution != null);
        return this.autoCompletionTypeResolution;
    }

    public boolean autoCompletionSmartQuotes() {
        this.lazyInit();
        assert (this.autoCompletionSmartQuotes != null);
        return this.autoCompletionSmartQuotes;
    }

    public boolean autoStringConcatination() {
        this.lazyInit();
        assert (this.autoStringConcatination != null);
        return this.autoStringConcatination;
    }

    public boolean autoCompletionFull() {
        this.lazyInit();
        assert (this.autoCompletionFull != null);
        return this.autoCompletionFull;
    }

    public int getCodeCompletionItemSignatureWidth() {
        this.lazyInit();
        assert (this.codeCompletionItemSignatureWidth != null);
        return this.codeCompletionItemSignatureWidth;
    }

    public boolean autoCompletionAfterDot() {
        this.lazyInit();
        assert (this.autoCompletionAfterDot != null);
        if (this.autoCompletionFull()) {
            return true;
        }
        return this.autoCompletionAfterDot;
    }

    private void lazyInit() {
        if (this.inited.compareAndSet(false, true)) {
            this.preferences = (Preferences)MimeLookup.getLookup((String)this.mimeType).lookup(Preferences.class);
            this.preferences.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.preferencesTracker, (Object)this.preferences));
            this.preferencesTracker.preferenceChange(null);
        }
    }

    public void setTestTypeResolution(boolean value) {
        this.autoCompletionTypeResolution = value;
    }

    public void setCodeCompletionItemSignatureWidth(int width) {
        this.codeCompletionItemSignatureWidth = width;
    }

}

