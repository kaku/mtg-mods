/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsConventionHint;

public class DuplicatePropertyName
extends JsConventionHint {
    public String getId() {
        return "jsduplicatepropertyname.hint";
    }

    public String getDescription() {
        return Bundle.DuplicatePropertyNameDescription();
    }

    public String getDisplayName() {
        return Bundle.DuplicatePropertyNameDisplayName();
    }
}

