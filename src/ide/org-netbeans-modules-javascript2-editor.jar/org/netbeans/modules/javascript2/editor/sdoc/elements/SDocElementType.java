/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

public enum SDocElementType {
    DESCRIPTION("description", Category.DESCRIPTION),
    UNKNOWN("unknown", Category.UNKNOWN),
    ALIAS("@alias", Category.IDENT),
    AUTHOR("@author", Category.DESCRIPTION),
    CLASS_DESCRIPTION("@classDescription", Category.DESCRIPTION),
    CONSTRUCTOR("@constructor", Category.SIMPLE),
    DEPRECATED("@deprecated", Category.SIMPLE),
    EXAMPLE("@example", Category.DESCRIPTION),
    EXCEPTION("@exception", Category.TYPE_DESCRIBED),
    ID("@id", Category.DESCRIPTION),
    INHERITS("@inherits", Category.IDENT),
    INTERNAL("@internal", Category.SIMPLE),
    MEMBER_OF("@memberOf", Category.IDENT),
    METHOD("@method", Category.SIMPLE),
    NAMESPACE("@namespace", Category.IDENT),
    PARAM("@param", Category.TYPE_NAMED),
    PRIVATE("@private", Category.SIMPLE),
    PROJECT_DESCRIPTION("@projectDescription", Category.DESCRIPTION),
    PROPERTY("@property", Category.TYPE_SIMPLE),
    RETURN("@return", Category.TYPE_DESCRIBED),
    SEE("@see", Category.DESCRIPTION),
    SINCE("@since", Category.DESCRIPTION),
    TYPE("@type", Category.TYPE_SIMPLE),
    VERSION("@version", Category.DESCRIPTION);
    
    private final String value;
    private final Category category;

    private SDocElementType(String textValue, Category category) {
        this.value = textValue;
        this.category = category;
    }

    public String toString() {
        return this.value;
    }

    public Category getCategory() {
        return this.category;
    }

    public static SDocElementType fromString(String value) {
        if (value != null) {
            for (SDocElementType type : SDocElementType.values()) {
                if (!value.equalsIgnoreCase(type.toString())) continue;
                return type;
            }
        }
        return UNKNOWN;
    }

    public static enum Category {
        DESCRIPTION,
        IDENT,
        SIMPLE,
        UNKNOWN,
        TYPE_SIMPLE,
        TYPE_NAMED,
        TYPE_DESCRIBED;
        

        private Category() {
        }
    }

}

