/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;

public interface DeclarationScope {
    public DeclarationScope getParentScope();

    public Collection<? extends DeclarationScope> getChildrenScopes();
}

