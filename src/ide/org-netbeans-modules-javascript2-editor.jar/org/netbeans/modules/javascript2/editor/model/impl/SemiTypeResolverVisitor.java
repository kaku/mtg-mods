/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.AccessNode
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.CallNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.IndexNode
 *  jdk.nashorn.internal.ir.LiteralNode
 *  jdk.nashorn.internal.ir.LiteralNode$ArrayLiteralNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.ObjectNode
 *  jdk.nashorn.internal.ir.ReferenceNode
 *  jdk.nashorn.internal.ir.TernaryNode
 *  jdk.nashorn.internal.ir.UnaryNode
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  jdk.nashorn.internal.parser.Lexer
 *  jdk.nashorn.internal.parser.Lexer$RegexToken
 *  jdk.nashorn.internal.parser.Token
 *  jdk.nashorn.internal.parser.TokenType
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.ir.AccessNode;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.CallNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.IndexNode;
import jdk.nashorn.internal.ir.LiteralNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.ObjectNode;
import jdk.nashorn.internal.ir.ReferenceNode;
import jdk.nashorn.internal.ir.TernaryNode;
import jdk.nashorn.internal.ir.UnaryNode;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import jdk.nashorn.internal.parser.Lexer;
import jdk.nashorn.internal.parser.Token;
import jdk.nashorn.internal.parser.TokenType;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.PathNodeVisitor;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;

public class SemiTypeResolverVisitor
extends PathNodeVisitor {
    private static final Logger LOGGER = Logger.getLogger(SemiTypeResolverVisitor.class.getName());
    public static final String ST_START_DELIMITER = "@";
    public static final String ST_THIS = "@this;";
    public static final String ST_VAR = "@var;";
    public static final String ST_EXP = "@exp;";
    public static final String ST_PRO = "@pro;";
    public static final String ST_CALL = "@call;";
    public static final String ST_NEW = "@new;";
    public static final String ST_ARR = "@arr;";
    public static final String ST_ANONYM = "@anonym;";
    public static final String ST_WITH = "@with;";
    private static final TypeUsage BOOLEAN_TYPE = new TypeUsageImpl("Boolean", -1, true);
    private static final TypeUsage STRING_TYPE = new TypeUsageImpl("String", -1, true);
    private static final TypeUsage NUMBER_TYPE = new TypeUsageImpl("Number", -1, true);
    private static final TypeUsage ARRAY_TYPE = new TypeUsageImpl("Array", -1, true);
    private static final TypeUsage REGEXP_TYPE = new TypeUsageImpl("RegExp", -1, true);
    private Map<String, TypeUsage> result;
    private List<String> exp;
    private int typeOffset;

    public Set<TypeUsage> getSemiTypes(Node expression) {
        this.exp = new ArrayList<String>();
        this.result = new HashMap<String, TypeUsage>();
        this.reset();
        expression.accept((NodeVisitor)this);
        this.add(this.exp, this.typeOffset == -1 ? expression.getStart() : this.typeOffset, false);
        return new HashSet<TypeUsage>(this.result.values());
    }

    private void reset() {
        this.exp.clear();
        this.typeOffset = -1;
    }

    private void add(List<String> exp, int offset, boolean resolved) {
        if (exp.isEmpty() || exp.size() == 1 && exp.get(0).startsWith("@") && !exp.get(0).startsWith("@anonym;") && !"@this;".equals(exp.get(0))) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        if (!exp.get(0).startsWith("@")) {
            if (exp.size() == 1) {
                sb.append("@var;");
            } else {
                sb.append("@exp;");
            }
        }
        for (String part : exp) {
            sb.append(part);
        }
        String type = sb.toString();
        if (!this.result.containsKey(type)) {
            this.result.put(type, new TypeUsageImpl(type, offset, resolved));
        }
    }

    private void add(TypeUsage type) {
        if (!this.result.containsKey(type.getType())) {
            this.result.put(type.getType(), type);
        }
    }

    @Override
    public Node leave(AccessNode accessNode) {
        this.exp.add(this.exp.size() - 1, "@pro;");
        return super.leave(accessNode);
    }

    @Override
    public Node enter(CallNode callNode) {
        this.addToPath((Node)callNode);
        callNode.getFunction().accept((NodeVisitor)this);
        if (this.exp.size() == 2 && "@new;".equals(this.exp.get(0))) {
            return null;
        }
        if (callNode.getFunction() instanceof AccessNode) {
            int size = this.exp.size();
            if (size > 1 && "@pro;".equals(this.exp.get(size - 2))) {
                this.exp.remove(size - 2);
            }
        } else if (callNode.getFunction() instanceof ReferenceNode) {
            FunctionNode function = ((ReferenceNode)callNode.getFunction()).getReference();
            String name = function.isAnonymous() ? function.getName() : function.getIdent().getName();
            this.add(new TypeUsageImpl("@call;" + name, function.getStart(), false));
            return null;
        }
        if (this.exp.isEmpty()) {
            this.exp.add("@call;");
        } else {
            this.exp.add(this.exp.size() - 1, "@call;");
        }
        return null;
    }

    @Override
    public Node leave(CallNode callNode) {
        int size;
        if (callNode.getFunction() instanceof AccessNode && (size = this.exp.size()) > 1 && "@pro;".equals(this.exp.get(size - 2))) {
            this.exp.remove(size - 2);
        }
        this.exp.add(this.exp.size() - 1, "@call;");
        return super.leave(callNode);
    }

    @Override
    public Node enter(UnaryNode unaryNode) {
        switch (Token.descType((long)unaryNode.getToken())) {
            case NEW: {
                this.exp.add("@new;");
                SimpleNameResolver snr = new SimpleNameResolver();
                this.exp.add(snr.getFQN(unaryNode.rhs()));
                this.typeOffset = snr.getTypeOffset();
                return null;
            }
            case NOT: {
                this.add(BOOLEAN_TYPE);
                return null;
            }
            case ADD: 
            case SUB: 
            case DECPREFIX: 
            case DECPOSTFIX: 
            case INCPREFIX: 
            case INCPOSTFIX: {
                this.add(NUMBER_TYPE);
                return null;
            }
        }
        return super.enter(unaryNode);
    }

    @Override
    public Node enter(IdentNode iNode) {
        String name = iNode.getPropertyName();
        if ("this".equals(name)) {
            this.exp.add("@this;");
        } else {
            if (this.getPath().isEmpty()) {
                this.exp.add("@var;");
            }
            this.exp.add(name);
        }
        return null;
    }

    @Override
    public Node enter(LiteralNode lNode) {
        Object value = lNode.getObject();
        if (value instanceof Boolean) {
            this.add(BOOLEAN_TYPE);
        } else if (value instanceof String) {
            this.add(STRING_TYPE);
        } else if (value instanceof Integer || value instanceof Float || value instanceof Double) {
            this.add(NUMBER_TYPE);
        } else if (lNode instanceof LiteralNode.ArrayLiteralNode) {
            this.add(ARRAY_TYPE);
        } else if (value instanceof Lexer.RegexToken) {
            this.add(REGEXP_TYPE);
        }
        return null;
    }

    @Override
    public Node enter(TernaryNode ternaryNode) {
        ternaryNode.rhs().accept((NodeVisitor)this);
        this.add(this.exp, ternaryNode.rhs().getStart(), false);
        this.reset();
        ternaryNode.third().accept((NodeVisitor)this);
        this.add(this.exp, ternaryNode.third().getStart(), false);
        this.reset();
        return null;
    }

    @Override
    public Node enter(ObjectNode objectNode) {
        int size = this.getPath().size();
        if (size > 0 && this.getPath().get(size - 1) instanceof AccessNode) {
            this.exp.add("@anonym;" + objectNode.getStart());
        } else {
            this.add(new TypeUsageImpl("@anonym;" + objectNode.getStart(), objectNode.getStart(), false));
        }
        return null;
    }

    @Override
    public Node enter(IndexNode indexNode) {
        this.addToPath((Node)indexNode);
        indexNode.getBase().accept((NodeVisitor)this);
        int size = this.exp.size();
        if (size > 1 && "@pro;".equals(this.exp.get(size - 2))) {
            this.exp.remove(size - 2);
        }
        if (this.exp.isEmpty()) {
            this.exp.add("@arr;");
        } else {
            LiteralNode lNode;
            boolean propertyAccess = false;
            if (indexNode.getIndex() instanceof LiteralNode && (lNode = (LiteralNode)indexNode.getIndex()).isString()) {
                this.exp.add("@pro;");
                this.exp.add(lNode.getPropertyName());
                propertyAccess = true;
            }
            if (!propertyAccess) {
                this.exp.add(this.exp.size() - 1, "@arr;");
            }
        }
        return null;
    }

    @Override
    public Node enter(BinaryNode binaryNode) {
        if (!binaryNode.isAssignment()) {
            if (this.isResultString(binaryNode)) {
                this.add(STRING_TYPE);
                return null;
            }
            if (this.isResultNumber(binaryNode)) {
                this.add(NUMBER_TYPE);
                return null;
            }
            TokenType tokenType = binaryNode.tokenType();
            if (tokenType == TokenType.EQ || tokenType == TokenType.EQ_STRICT || tokenType == TokenType.NE || tokenType == TokenType.NE_STRICT || tokenType == TokenType.GE || tokenType == TokenType.GT || tokenType == TokenType.LE || tokenType == TokenType.LT) {
                if (this.getPath().isEmpty()) {
                    this.add(BOOLEAN_TYPE);
                }
                return null;
            }
            binaryNode.lhs().accept((NodeVisitor)this);
            this.add(this.exp, binaryNode.lhs().getStart(), false);
            this.reset();
            binaryNode.rhs().accept((NodeVisitor)this);
            this.add(this.exp, binaryNode.rhs().getStart(), false);
            this.reset();
            return null;
        }
        return super.enter(binaryNode);
    }

    @Override
    public Node enter(ReferenceNode rNode) {
        this.add(new TypeUsageImpl("Function", rNode.getReference().getStart(), true));
        return null;
    }

    private boolean isResultString(BinaryNode binaryNode) {
        boolean bResult = false;
        TokenType tokenType = binaryNode.tokenType();
        Node lhs = binaryNode.lhs();
        Node rhs = binaryNode.rhs();
        if (tokenType == TokenType.ADD && (lhs instanceof LiteralNode && ((LiteralNode)lhs).isString() || rhs instanceof LiteralNode && ((LiteralNode)rhs).isString())) {
            bResult = true;
        } else if (lhs instanceof BinaryNode) {
            bResult = this.isResultString((BinaryNode)lhs);
        } else if (rhs instanceof BinaryNode) {
            bResult = this.isResultString((BinaryNode)rhs);
        }
        return bResult;
    }

    private boolean isResultNumber(BinaryNode binaryNode) {
        boolean bResult = false;
        TokenType tokenType = binaryNode.tokenType();
        Node lhs = binaryNode.lhs();
        Node rhs = binaryNode.rhs();
        if ((tokenType == TokenType.BIT_OR || tokenType == TokenType.BIT_AND) && (lhs instanceof LiteralNode && ((LiteralNode)lhs).isNumeric() || rhs instanceof LiteralNode && ((LiteralNode)rhs).isNumeric())) {
            bResult = true;
        } else if (lhs instanceof BinaryNode) {
            bResult = this.isResultNumber((BinaryNode)lhs);
        } else if (rhs instanceof BinaryNode) {
            bResult = this.isResultNumber((BinaryNode)rhs);
        }
        return bResult;
    }

    private static class SimpleNameResolver
    extends PathNodeVisitor {
        private List<String> exp = new ArrayList<String>();
        private int typeOffset = -1;

        private SimpleNameResolver() {
        }

        public String getFQN(Node expression) {
            this.exp.clear();
            expression.accept((NodeVisitor)this);
            StringBuilder sb = new StringBuilder();
            for (String part : this.exp) {
                sb.append(part);
                sb.append('.');
            }
            if (sb.length() == 0) {
                LOGGER.log(Level.FINE, "New operator withouth name: {0}", expression.toString());
                return null;
            }
            return sb.toString().substring(0, sb.length() - 1);
        }

        public int getTypeOffset() {
            return this.typeOffset;
        }

        @Override
        public Node enter(CallNode callNode) {
            callNode.getFunction().accept((NodeVisitor)this);
            return null;
        }

        @Override
        public Node enter(FunctionNode functionNode) {
            functionNode.getIdent().accept((NodeVisitor)this);
            return null;
        }

        @Override
        public Node enter(IndexNode indexNode) {
            indexNode.getBase().accept((NodeVisitor)this);
            return null;
        }

        @Override
        public Node enter(IdentNode identNode) {
            this.exp.add(identNode.getName());
            this.typeOffset = identNode.getStart();
            return super.enter(identNode);
        }

        @Override
        public Node enter(ReferenceNode referenceNode) {
            referenceNode.getReference().accept((NodeVisitor)this);
            return null;
        }
    }

}

