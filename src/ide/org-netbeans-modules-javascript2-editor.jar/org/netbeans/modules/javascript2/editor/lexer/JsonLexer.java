/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 *  org.netbeans.spi.lexer.TokenFactory
 */
package org.netbeans.modules.javascript2.editor.lexer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsonColoringLexer;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.netbeans.spi.lexer.TokenFactory;

public class JsonLexer
implements Lexer<JsTokenId> {
    private static final Logger LOGGER = Logger.getLogger(JsonLexer.class.getName());
    private final JsonColoringLexer scanner;
    private TokenFactory<JsTokenId> tokenFactory;

    private JsonLexer(LexerRestartInfo<JsTokenId> info) {
        this.scanner = new JsonColoringLexer(info);
        this.tokenFactory = info.tokenFactory();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static JsonLexer create(LexerRestartInfo<JsTokenId> info) {
        Class<JsonLexer> class_ = JsonLexer.class;
        synchronized (JsonLexer.class) {
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return new JsonLexer(info);
        }
    }

    public Token<JsTokenId> nextToken() {
        try {
            JsTokenId tokenId = this.scanner.nextToken();
            LOGGER.log(Level.FINEST, "Lexed token is {0}", (Object)tokenId);
            Token token = null;
            if (tokenId != null) {
                token = this.tokenFactory.createToken((TokenId)tokenId);
            }
            return token;
        }
        catch (IOException ex) {
            Logger.getLogger(JsonLexer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object state() {
        return this.scanner.getState();
    }

    public void release() {
    }
}

