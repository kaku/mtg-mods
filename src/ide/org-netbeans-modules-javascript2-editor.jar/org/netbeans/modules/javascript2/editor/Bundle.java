/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String JsCompletionItem_lbl_js_platform() {
        return NbBundle.getMessage(Bundle.class, (String)"JsCompletionItem.lbl.js.platform");
    }

    static String JsResolver() {
        return NbBundle.getMessage(Bundle.class, (String)"JsResolver");
    }

    static String JsonResolver() {
        return NbBundle.getMessage(Bundle.class, (String)"JsonResolver");
    }

    private void Bundle() {
    }
}

