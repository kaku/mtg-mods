/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.CodeCompletionContext
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.spi.ParserResult
 */
package org.netbeans.modules.javascript2.editor.spi;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import org.netbeans.modules.csl.api.CodeCompletionContext;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.spi.CompletionContext;

public interface CompletionProvider {
    public List<CompletionProposal> complete(CodeCompletionContext var1, CompletionContext var2, String var3);

    public String getHelpDocumentation(ParserResult var1, ElementHandle var2);

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE})
    public static @interface Registration {
        public int priority() default 100;
    }

}

