/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.awt.Image;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.JsCompletionItem;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationElement;
import org.netbeans.modules.javascript2.editor.doc.api.JsDocumentationSupport;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.util.ImageUtilities;

public class JsDocumentationCodeCompletion {
    private static final String TAG_PREFIX = "@";

    public static void complete(JsCompletionItem.CompletionRequest request, List<CompletionProposal> resultList) {
        int originalOffset = request.info.getSnapshot().getOriginalOffset(request.anchor);
        if (request.prefix != null && request.prefix.startsWith("@")) {
            JsDocumentationCodeCompletion.completeAnnotation(request, resultList, originalOffset);
        } else {
            CharSequence text = request.info.getSnapshot().getText();
            if ("@".charAt(0) == text.charAt(request.anchor - 1)) {
                request.prefix = request.prefix == null ? "@" : "@" + request.prefix;
                JsDocumentationCodeCompletion.completeAnnotation(request, resultList, originalOffset - 1);
            }
        }
    }

    private static void completeAnnotation(JsCompletionItem.CompletionRequest request, List<CompletionProposal> resultList, int anchor) {
        JsDocumentationProvider documentationProvider = JsDocumentationSupport.getDocumentationProvider(request.result);
        int orderingBase = 0;
        for (AnnotationCompletionTagProvider provider : documentationProvider.getAnnotationsProvider()) {
            ++orderingBase;
            for (AnnotationCompletionTag tag : provider.getAnnotations()) {
                if (!tag.getName().startsWith(request.prefix)) continue;
                resultList.add(new JsDocumentationCodeCompletionItem(anchor, tag, provider.getName(), orderingBase));
            }
        }
    }

    public static class JsDocumentationCodeCompletionItem
    implements CompletionProposal {
        private static final String ANNOTATION_ICON = "org/netbeans/modules/csl/source/resources/icons/annotation.png";
        private static final ImageIcon IMAGE_ICON = new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/csl/source/resources/icons/annotation.png"));
        private final AnnotationCompletionTag tag;
        private final int anchorOffset;
        private final JsDocumentationElement elem;
        private final String providerName;
        private final int priority;

        public JsDocumentationCodeCompletionItem(int anchorOffset, AnnotationCompletionTag tag, String providerName, int priority) {
            this.tag = tag;
            this.anchorOffset = anchorOffset;
            this.providerName = providerName;
            this.priority = priority;
            this.elem = new JsDocumentationElement(tag.getName(), tag.getDocumentation());
        }

        public int getAnchorOffset() {
            return this.anchorOffset;
        }

        public ElementHandle getElement() {
            return this.elem;
        }

        public String getName() {
            return this.tag.getName();
        }

        public String getInsertPrefix() {
            return this.getName();
        }

        public String getSortText() {
            return "" + this.priority + this.providerName + this.getName();
        }

        public int getSortPrioOverride() {
            return 0;
        }

        public String getLhsHtml(HtmlFormatter formatter) {
            formatter.name(this.getKind(), true);
            formatter.appendText(this.getName());
            formatter.name(this.getKind(), false);
            this.tag.formatParameters(formatter);
            return formatter.getText();
        }

        public String getRhsHtml(HtmlFormatter formatter) {
            return this.providerName;
        }

        public ElementKind getKind() {
            return this.elem.getKind();
        }

        public ImageIcon getIcon() {
            return IMAGE_ICON;
        }

        public Set<Modifier> getModifiers() {
            return Collections.emptySet();
        }

        public boolean isSmart() {
            return false;
        }

        public String getCustomInsertTemplate() {
            return this.tag.getInsertTemplate();
        }
    }

}

