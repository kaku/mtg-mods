/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import java.util.List;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeDescribedElement;

public class SDocTypeNamedElement
extends SDocTypeDescribedElement {
    private final Identifier typeName;
    private final boolean optional;

    private SDocTypeNamedElement(SDocElementType type, List<Type> declaredTypes, String description, Identifier typeName, boolean optional) {
        super(type, declaredTypes, description);
        this.typeName = typeName;
        this.optional = optional;
    }

    public static SDocTypeNamedElement create(SDocElementType type, List<Type> declaredTypes, String description, Identifier typeName) {
        return new SDocTypeNamedElement(type, declaredTypes, description, typeName, false);
    }

    public static SDocTypeNamedElement create(SDocElementType type, List<Type> declaredTypes, String description, Identifier typeName, boolean optional) {
        return new SDocTypeNamedElement(type, declaredTypes, description, typeName, optional);
    }

    public Identifier getTypeName() {
        return this.typeName;
    }

    @Override
    public Identifier getParamName() {
        return this.typeName;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOptional() {
        return this.optional;
    }
}

