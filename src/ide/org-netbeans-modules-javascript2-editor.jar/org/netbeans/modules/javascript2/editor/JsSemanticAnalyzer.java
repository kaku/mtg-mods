/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.ColoringAttributes
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.SemanticAnalyzer
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 */
package org.netbeans.modules.javascript2.editor;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;

public class JsSemanticAnalyzer
extends SemanticAnalyzer<JsParserResult> {
    public static final EnumSet<ColoringAttributes> UNUSED_OBJECT_SET = EnumSet.of(ColoringAttributes.UNUSED, ColoringAttributes.CLASS);
    public static final EnumSet<ColoringAttributes> UNUSED_METHOD_SET = EnumSet.of(ColoringAttributes.UNUSED, ColoringAttributes.METHOD);
    private boolean cancelled = false;
    private Map<OffsetRange, Set<ColoringAttributes>> semanticHighlights = null;
    private static final List<String> GLOBAL_TYPES = Arrays.asList("Array", "String", "Boolean", "Number", "undefined");

    public Map<OffsetRange, Set<ColoringAttributes>> getHighlights() {
        return this.semanticHighlights;
    }

    public void run(JsParserResult result, SchedulerEvent event) {
        this.resume();
        if (this.isCancelled()) {
            return;
        }
        Map highlights = new HashMap<OffsetRange, Set<ColoringAttributes>>(100);
        Model model = result.getModel();
        JsObject global = model.getGlobalObject();
        highlights = this.count(result, global, highlights);
        this.semanticHighlights = highlights != null && highlights.size() > 0 ? highlights : null;
    }

    private Map<OffsetRange, Set<ColoringAttributes>> count(JsParserResult result, JsObject parent, Map<OffsetRange, Set<ColoringAttributes>> highlights) {
        for (JsObject object : parent.getProperties().values()) {
            if (object.getDeclarationName() != null) {
                switch (object.getJSKind()) {
                    OffsetRange range;
                    case CONSTRUCTOR: 
                    case METHOD: 
                    case FUNCTION: {
                        if (object.isDeclared() && !object.isAnonymous() && !object.getDeclarationName().getOffsetRange().isEmpty()) {
                            EnumSet<ColoringAttributes> coloring = ColoringAttributes.METHOD_SET;
                            if (object.getModifiers().contains((Object)Modifier.PRIVATE)) {
                                OffsetRange orDeclaration;
                                OffsetRange orOccurrence;
                                if (object.getOccurrences().isEmpty()) {
                                    coloring = UNUSED_METHOD_SET;
                                } else if (object.getOccurrences().size() == 1 && (orDeclaration = object.getDeclarationName().getOffsetRange()).equals((Object)(orOccurrence = object.getOccurrences().get(0).getOffsetRange()))) {
                                    coloring = UNUSED_METHOD_SET;
                                }
                            }
                            this.addColoring(result, highlights, object.getDeclarationName().getOffsetRange(), coloring);
                        }
                        for (JsObject param : ((JsFunction)object).getParameters()) {
                            if (!(object instanceof JsObjectReference) || ((JsObjectReference)object).getOriginal().isAnonymous()) {
                                this.count(result, param, highlights);
                            }
                            if (this.hasSourceOccurences(result, param) || (range = LexUtilities.getLexerOffsets(result, param.getDeclarationName().getOffsetRange())).getStart() >= range.getEnd()) continue;
                            highlights.put(range, ColoringAttributes.UNUSED_SET);
                        }
                        break;
                    }
                    case PROPERTY_GETTER: 
                    case PROPERTY_SETTER: {
                        Token<? extends JsTokenId> token;
                        int offset = LexUtilities.getLexerOffset(result, object.getDeclarationName().getOffsetRange().getStart());
                        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(result.getSnapshot(), offset);
                        if (ts == null) break;
                        ts.move(offset);
                        if (ts.moveNext() && ts.movePrevious() && (token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.DOC_COMMENT}))).id() == JsTokenId.IDENTIFIER && token.length() == 3) {
                            highlights.put(new OffsetRange(ts.offset(), ts.offset() + token.length()), ColoringAttributes.METHOD_SET);
                        }
                        highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), ColoringAttributes.FIELD_SET);
                        break;
                    }
                    case OBJECT: 
                    case OBJECT_LITERAL: {
                        if ("UNKNOWN".equals(object.getName())) break;
                        if (parent.getParent() == null && !GLOBAL_TYPES.contains(object.getName())) {
                            this.addColoring(result, highlights, object.getDeclarationName().getOffsetRange(), ColoringAttributes.GLOBAL_SET);
                            for (Occurrence occurence : object.getOccurrences()) {
                                if (JsSemanticAnalyzer.isCommentOccurence(result, occurence)) continue;
                                this.addColoring(result, highlights, occurence.getOffsetRange(), ColoringAttributes.GLOBAL_SET);
                            }
                        } else {
                            if (!object.isDeclared() || "prototype".equals(object.getName()) || object.isAnonymous()) break;
                            if ((object.getOccurrences().isEmpty() || object.getOccurrences().size() == 1 && object.getOccurrences().get(0).getOffsetRange().equals((Object)object.getDeclarationName().getOffsetRange())) && object.getModifiers().contains((Object)Modifier.PRIVATE)) {
                                highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), UNUSED_OBJECT_SET);
                                break;
                            }
                            highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), ColoringAttributes.CLASS_SET);
                            TokenSequence<? extends JsTokenId> cts = LexUtilities.getJsTokenSequence(result.getSnapshot(), object.getDeclarationName().getOffsetRange().getStart());
                            for (Occurrence occurrence : object.getOccurrences()) {
                                cts.move(occurrence.getOffsetRange().getStart());
                                if (!cts.moveNext() || cts.token().id() != JsTokenId.STRING || occurrence.getOffsetRange().equals((Object)object.getDeclarationName().getOffsetRange())) continue;
                                highlights.put(LexUtilities.getLexerOffsets(result, occurrence.getOffsetRange()), ColoringAttributes.CLASS_SET);
                            }
                        }
                        break;
                    }
                    case PROPERTY: {
                        if (!object.isDeclared()) break;
                        highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), ColoringAttributes.FIELD_SET);
                        for (Occurrence occurence : object.getOccurrences()) {
                            if (JsSemanticAnalyzer.isCommentOccurence(result, occurence)) continue;
                            highlights.put(LexUtilities.getLexerOffsets(result, occurence.getOffsetRange()), ColoringAttributes.FIELD_SET);
                        }
                        break;
                    }
                    case FIELD: {
                        highlights.put(object.getDeclarationName().getOffsetRange(), ColoringAttributes.FIELD_SET);
                        for (Occurrence occurence : object.getOccurrences()) {
                            if (JsSemanticAnalyzer.isCommentOccurence(result, occurence)) continue;
                            highlights.put(LexUtilities.getLexerOffsets(result, occurence.getOffsetRange()), ColoringAttributes.FIELD_SET);
                        }
                        break;
                    }
                    case VARIABLE: {
                        if (parent.getParent() == null && !GLOBAL_TYPES.contains(object.getName())) {
                            highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), ColoringAttributes.GLOBAL_SET);
                            for (Occurrence occurence : object.getOccurrences()) {
                                if (JsSemanticAnalyzer.isCommentOccurence(result, occurence)) continue;
                                highlights.put(LexUtilities.getLexerOffsets(result, occurence.getOffsetRange()), ColoringAttributes.GLOBAL_SET);
                            }
                        } else {
                            if ((object.getOccurrences().isEmpty() || object.getOccurrences().size() == 1 && object.getOccurrences().get(0).getOffsetRange().equals((Object)object.getDeclarationName().getOffsetRange())) && !GLOBAL_TYPES.contains(object.getName())) {
                                range = object.getDeclarationName().getOffsetRange();
                                if (range.getStart() >= range.getEnd()) break;
                                highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), ColoringAttributes.UNUSED_SET);
                                break;
                            }
                            if (!(object instanceof JsObjectImpl) || "arguments".equals(object.getName()) || object.getOccurrences().size() > ((JsObjectImpl)object).getCountOfAssignments()) break;
                            if (object.getDeclarationName().getOffsetRange().getLength() > 0) {
                                highlights.put(LexUtilities.getLexerOffsets(result, object.getDeclarationName().getOffsetRange()), ColoringAttributes.UNUSED_SET);
                            }
                            for (Occurrence occurence : object.getOccurrences()) {
                                if (occurence.getOffsetRange().getLength() <= 0) continue;
                                highlights.put(LexUtilities.getLexerOffsets(result, occurence.getOffsetRange()), ColoringAttributes.UNUSED_SET);
                            }
                        }
                        break;
                    }
                }
            }
            if (this.isCancelled()) {
                highlights = null;
                break;
            }
            if (object instanceof JsObjectReference && ModelUtils.isDescendant(object, ((JsObjectReference)object).getOriginal())) continue;
            highlights = this.count(result, object, highlights);
        }
        return highlights;
    }

    private void addColoring(ParserResult result, Map<OffsetRange, Set<ColoringAttributes>> highlights, OffsetRange astRange, Set<ColoringAttributes> coloring) {
        int start = result.getSnapshot().getOriginalOffset(astRange.getStart());
        int end = result.getSnapshot().getOriginalOffset(astRange.getEnd());
        if (start > -1 && end > -1 && start < end) {
            OffsetRange range = start == astRange.getStart() ? astRange : new OffsetRange(start, end);
            highlights.put(range, coloring);
        }
    }

    public int getPriority() {
        return 0;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    public synchronized void cancel() {
        this.cancelled = true;
    }

    protected final synchronized boolean isCancelled() {
        return this.cancelled;
    }

    protected final synchronized void resume() {
        this.cancelled = false;
    }

    private boolean hasSourceOccurences(JsParserResult result, JsObject param) {
        if (param.getOccurrences().isEmpty()) {
            return false;
        }
        if (param.getOccurrences().size() == 1 && param.getOccurrences().get(0).getOffsetRange().equals((Object)param.getDeclarationName().getOffsetRange())) {
            return false;
        }
        int sourceOccurenceCount = 0;
        for (Occurrence occurrence : param.getOccurrences()) {
            if (!JsSemanticAnalyzer.isCommentOccurence(result, occurrence)) {
                ++sourceOccurenceCount;
            }
            if (sourceOccurenceCount <= 1) continue;
            return true;
        }
        return false;
    }

    private static boolean isCommentOccurence(JsParserResult result, Occurrence occurence) {
        for (JsComment comment : result.getDocumentationHolder().getCommentBlocks().values()) {
            if (!comment.getOffsetRange().containsInclusive(occurence.getOffsetRange().getStart())) continue;
            return true;
        }
        return false;
    }

}

