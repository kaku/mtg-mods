/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.Node
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jdk.nashorn.internal.ir.Node;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.parsing.api.Snapshot;

public final class JsDocumentationFallbackHolder
extends JsDocumentationHolder {
    public JsDocumentationFallbackHolder(JsDocumentationProvider provider, Snapshot snapshot) {
        super(provider, snapshot);
    }

    public List getReturnType(Node node) {
        return Collections.emptyList();
    }

    public List getParameters(Node node) {
        return Collections.emptyList();
    }

    @Override
    public Documentation getDocumentation(Node node) {
        return null;
    }

    @Override
    public boolean isDeprecated(Node node) {
        return false;
    }

    public Set getModifiers(Node node) {
        return Collections.emptySet();
    }

    public Map getCommentBlocks() {
        return Collections.emptyMap();
    }
}

