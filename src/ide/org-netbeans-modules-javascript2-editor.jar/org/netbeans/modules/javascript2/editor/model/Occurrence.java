/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.JsObject;

public interface Occurrence {
    public OffsetRange getOffsetRange();

    public Collection<? extends JsObject> getDeclarations();
}

