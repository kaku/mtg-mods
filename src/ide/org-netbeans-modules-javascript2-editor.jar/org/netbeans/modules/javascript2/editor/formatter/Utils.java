/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Utils {
    public static final int POSSIBLE_SCROLL_BAR_WIDTH = 30;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String loadPreviewText(InputStream is) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = r.readLine();
            while (line != null) {
                sb.append(line).append('\n');
                line = r.readLine();
            }
            line = sb.toString();
            return line;
        }
        finally {
            r.close();
        }
    }
}

