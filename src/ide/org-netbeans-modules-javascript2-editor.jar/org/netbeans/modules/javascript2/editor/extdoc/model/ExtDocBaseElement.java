/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;

public class ExtDocBaseElement
implements ExtDocElement {
    private final ExtDocElementType type;

    public ExtDocBaseElement(ExtDocElementType type) {
        this.type = type;
    }

    @Override
    public ExtDocElementType getType() {
        return this.type;
    }
}

