/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

public interface SyntaxProvider {
    public static final String TYPE_PLACEHOLDER = "[type]";
    public static final String NAME_PLACEHOLDER = "[name]";

    public String typesSeparator();

    public String paramTagTemplate();

    public String returnTagTemplate();

    public String typeTagTemplate();
}

