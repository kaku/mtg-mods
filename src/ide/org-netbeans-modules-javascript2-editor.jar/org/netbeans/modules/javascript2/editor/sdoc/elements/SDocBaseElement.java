/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public class SDocBaseElement
implements SDocElement {
    private final SDocElementType type;

    public SDocBaseElement(SDocElementType type) {
        this.type = type;
    }

    @Override
    public SDocElementType getType() {
        return this.type;
    }
}

