/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.editor.indent.spi.Context
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.javascript2.editor.formatter.Defaults;

public final class IndentContext {
    private final Context context;
    private final Defaults.Provider provider;
    private final boolean embedded;
    private final Stack<BlockDescription> blocks = new Stack();
    private final List<Indentation> indentations = new ArrayList<Indentation>();
    private int embeddedIndent;
    private final int caretLineStart;
    private final int caretLineEnd;

    public IndentContext(Context context, Defaults.Provider provider) {
        int lineStart;
        int lineEnd;
        this.context = context;
        this.provider = provider;
        this.embedded = !"text/javascript".equals(context.mimePath()) && !"text/x-json".equals(context.mimePath());
        try {
            lineStart = Utilities.getRowStart((BaseDocument)((BaseDocument)context.document()), (int)context.caretOffset());
        }
        catch (BadLocationException ex) {
            lineStart = context.caretOffset();
        }
        this.caretLineStart = lineStart;
        try {
            lineEnd = Utilities.getRowEnd((BaseDocument)((BaseDocument)context.document()), (int)context.caretOffset());
        }
        catch (BadLocationException ex) {
            lineEnd = context.caretOffset();
        }
        this.caretLineEnd = lineEnd;
    }

    public Defaults.Provider getDefaultsProvider() {
        return this.provider;
    }

    public BaseDocument getDocument() {
        return (BaseDocument)this.context.document();
    }

    public Context getContext() {
        return this.context;
    }

    public boolean isEmbedded() {
        return this.embedded;
    }

    public int getEmbeddedIndent() {
        return this.embeddedIndent;
    }

    public void setEmbeddedIndent(int embeddedIndent) {
        this.embeddedIndent = embeddedIndent;
    }

    public Stack<BlockDescription> getBlocks() {
        return this.blocks;
    }

    public void addIndentation(Indentation indentation) {
        this.indentations.add(indentation);
    }

    public List<Indentation> getIndentations() {
        return Collections.unmodifiableList(this.indentations);
    }

    public int getCaretLineStart() {
        return this.caretLineStart;
    }

    public int getCaretLineEnd() {
        return this.caretLineEnd;
    }

    public static final class BlockDescription {
        private final boolean braceless;
        private final boolean object;
        private final OffsetRange range;

        public BlockDescription(boolean braceless, boolean object, OffsetRange range) {
            assert (!object || !braceless);
            this.braceless = braceless;
            this.object = object;
            this.range = range;
        }

        public boolean isBraceless() {
            return this.braceless;
        }

        public boolean isObject() {
            return this.object;
        }

        public OffsetRange getRange() {
            return this.range;
        }
    }

    public static final class Indentation {
        private final int offset;
        private final int size;
        private final boolean continuation;

        public Indentation(int offset, int size, boolean continuation) {
            this.offset = offset;
            this.size = size;
            this.continuation = continuation;
        }

        public int getOffset() {
            return this.offset;
        }

        public int getSize() {
            return this.size;
        }

        public boolean isContinuation() {
            return this.continuation;
        }
    }

}

