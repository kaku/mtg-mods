/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.formatter.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import java.io.InputStream;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.netbeans.modules.javascript2.editor.formatter.FmtOptions;
import org.netbeans.modules.javascript2.editor.formatter.Utils;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtWrapping
extends JPanel
implements FocusListener {
    private JCheckBox afterBinaryOpsCheckBox;
    private JCheckBox afterDotCheckBox;
    private JCheckBox afterTernaryOpsCheckBox;
    private JComboBox arrayInitCombo;
    private JComboBox arrayInitItemsCombo;
    private JLabel arrayInitItemsLabel;
    private JLabel arrayInitLabel;
    private JComboBox assignOpsCombo;
    private JLabel assignOpsLabel;
    private JComboBox binaryOpsCombo;
    private JLabel binaryOpsLabel;
    private JComboBox chainedMethodCallsCombo;
    private JLabel chainedMethodCallsLabel;
    private JComboBox doWhileStatementCombo;
    private JLabel doWhileStatementLabel;
    private JComboBox forCombo;
    private JLabel forLabel;
    private JComboBox forStatementCombo;
    private JLabel forStatementLabel;
    private JComboBox ifStatementCombo;
    private JLabel ifStatementLabel;
    private JComboBox methodCallArgsCombo;
    private JLabel methodCallArgsLabel;
    private JComboBox methodParamsCombo;
    private JLabel methodParamsLabel;
    private JComboBox objectsCombo;
    private JLabel objectsLabel;
    private JPanel panel1;
    private JComboBox propertiesCombo;
    private JLabel propertiesLabel;
    private JScrollPane scrollPane;
    private JComboBox ternaryOpsCombo;
    private JLabel ternaryOpsLabel;
    private JComboBox variablesCombo;
    private JLabel variablesLabel;
    private JComboBox whileStatementComboBox;
    private JLabel whileStatementLabel;
    private JComboBox withStatementCombo;
    private JLabel withStatementLabel;

    public FmtWrapping() {
        this.initComponents();
        this.scrollPane.getViewport().setBackground(SystemColor.controlLtHighlight);
        this.variablesCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapVariables");
        this.variablesCombo.addFocusListener(this);
        this.methodParamsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapMethodParams");
        this.methodParamsCombo.addFocusListener(this);
        this.methodCallArgsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapMethodCallArgs");
        this.methodCallArgsCombo.addFocusListener(this);
        this.chainedMethodCallsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapChainedMethodCalls");
        this.chainedMethodCallsCombo.addFocusListener(this);
        this.afterDotCheckBox.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapAfterDotInChainedMethodCalls");
        this.afterDotCheckBox.addFocusListener(this);
        this.arrayInitCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapArrayInit");
        this.arrayInitCombo.addFocusListener(this);
        this.arrayInitItemsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapArrayInitItems");
        this.arrayInitItemsCombo.addFocusListener(this);
        this.forCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapFor");
        this.forCombo.addFocusListener(this);
        this.forStatementCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapForStatement");
        this.forStatementCombo.addFocusListener(this);
        this.ifStatementCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapIfStatement");
        this.ifStatementCombo.addFocusListener(this);
        this.whileStatementComboBox.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapWhileStatement");
        this.whileStatementComboBox.addFocusListener(this);
        this.doWhileStatementCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapDoWhileStatement");
        this.doWhileStatementCombo.addFocusListener(this);
        this.withStatementCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapWithStatement");
        this.withStatementCombo.addFocusListener(this);
        this.objectsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapObjects");
        this.objectsCombo.addFocusListener(this);
        this.propertiesCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapProperties");
        this.propertiesCombo.addFocusListener(this);
        this.binaryOpsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapBinaryOps");
        this.binaryOpsCombo.addFocusListener(this);
        this.afterBinaryOpsCheckBox.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapAfterBinaryOps");
        this.afterBinaryOpsCheckBox.addFocusListener(this);
        this.ternaryOpsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapTernaryOps");
        this.ternaryOpsCombo.addFocusListener(this);
        this.afterTernaryOpsCheckBox.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapAfterTernaryOps");
        this.afterTernaryOpsCheckBox.addFocusListener(this);
        this.assignOpsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapAssignOps");
        this.assignOpsCombo.addFocusListener(this);
        Dimension dimension = new Dimension((int)this.panel1.getPreferredSize().getWidth() + 30, (int)this.scrollPane.getMinimumSize().getHeight());
        this.scrollPane.setMinimumSize(dimension);
    }

    public static PreferencesCustomizer.Factory getController() {
        String preview = "";
        try {
            preview = Utils.loadPreviewText(FmtWrapping.class.getClassLoader().getResourceAsStream("org/netbeans/modules/javascript2/editor/formatter/ui/Wrapping.js"));
        }
        catch (IOException ex) {
            // empty catch block
        }
        return new FmtOptions.CategorySupport.Factory("text/javascript", "wrapping", FmtWrapping.class, preview, new String[0][]);
    }

    @Override
    public void focusGained(FocusEvent e) {
        this.scrollPane.getViewport().scrollRectToVisible(e.getComponent().getBounds());
    }

    @Override
    public void focusLost(FocusEvent e) {
    }

    private void initComponents() {
        this.scrollPane = new JScrollPane();
        this.panel1 = new JPanel();
        this.variablesLabel = new JLabel();
        this.variablesCombo = new JComboBox();
        this.methodParamsLabel = new JLabel();
        this.methodParamsCombo = new JComboBox();
        this.methodCallArgsLabel = new JLabel();
        this.methodCallArgsCombo = new JComboBox();
        this.chainedMethodCallsLabel = new JLabel();
        this.chainedMethodCallsCombo = new JComboBox();
        this.arrayInitLabel = new JLabel();
        this.arrayInitCombo = new JComboBox();
        this.forLabel = new JLabel();
        this.forCombo = new JComboBox();
        this.forStatementLabel = new JLabel();
        this.forStatementCombo = new JComboBox();
        this.ifStatementLabel = new JLabel();
        this.ifStatementCombo = new JComboBox();
        this.whileStatementLabel = new JLabel();
        this.whileStatementComboBox = new JComboBox();
        this.doWhileStatementLabel = new JLabel();
        this.doWhileStatementCombo = new JComboBox();
        this.binaryOpsLabel = new JLabel();
        this.binaryOpsCombo = new JComboBox();
        this.ternaryOpsLabel = new JLabel();
        this.ternaryOpsCombo = new JComboBox();
        this.assignOpsLabel = new JLabel();
        this.assignOpsCombo = new JComboBox();
        this.withStatementLabel = new JLabel();
        this.withStatementCombo = new JComboBox();
        this.afterBinaryOpsCheckBox = new JCheckBox();
        this.afterTernaryOpsCheckBox = new JCheckBox();
        this.objectsLabel = new JLabel();
        this.objectsCombo = new JComboBox();
        this.afterDotCheckBox = new JCheckBox();
        this.propertiesLabel = new JLabel();
        this.propertiesCombo = new JComboBox();
        this.arrayInitItemsLabel = new JLabel();
        this.arrayInitItemsCombo = new JComboBox();
        this.setName(NbBundle.getMessage(FmtWrapping.class, (String)"LBL_Wrapping"));
        this.setOpaque(false);
        this.setLayout(new BorderLayout());
        this.scrollPane.setMinimumSize(new Dimension(300, 200));
        this.panel1.setFocusCycleRoot(true);
        this.panel1.setFocusTraversalPolicy(new FocusTraversalPolicy(){

            @Override
            public Component getDefaultComponent(Container focusCycleRoot) {
                return FmtWrapping.this.variablesCombo;
            }

            @Override
            public Component getFirstComponent(Container focusCycleRoot) {
                return FmtWrapping.this.variablesCombo;
            }

            @Override
            public Component getLastComponent(Container focusCycleRoot) {
                return FmtWrapping.this.assignOpsCombo;
            }

            @Override
            public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
                if (aComponent == FmtWrapping.this.chainedMethodCallsCombo) {
                    return FmtWrapping.this.afterDotCheckBox;
                }
                if (aComponent == FmtWrapping.this.afterDotCheckBox) {
                    return FmtWrapping.this.arrayInitCombo;
                }
                if (aComponent == FmtWrapping.this.methodCallArgsCombo) {
                    return FmtWrapping.this.chainedMethodCallsCombo;
                }
                if (aComponent == FmtWrapping.this.methodParamsCombo) {
                    return FmtWrapping.this.methodCallArgsCombo;
                }
                if (aComponent == FmtWrapping.this.variablesCombo) {
                    return FmtWrapping.this.methodParamsCombo;
                }
                if (aComponent == FmtWrapping.this.doWhileStatementCombo) {
                    return FmtWrapping.this.withStatementCombo;
                }
                if (aComponent == FmtWrapping.this.withStatementCombo) {
                    return FmtWrapping.this.objectsCombo;
                }
                if (aComponent == FmtWrapping.this.objectsCombo) {
                    return FmtWrapping.this.propertiesCombo;
                }
                if (aComponent == FmtWrapping.this.propertiesCombo) {
                    return FmtWrapping.this.binaryOpsCombo;
                }
                if (aComponent == FmtWrapping.this.ternaryOpsCombo) {
                    return FmtWrapping.this.afterTernaryOpsCheckBox;
                }
                if (aComponent == FmtWrapping.this.afterTernaryOpsCheckBox) {
                    return FmtWrapping.this.assignOpsCombo;
                }
                if (aComponent == FmtWrapping.this.binaryOpsCombo) {
                    return FmtWrapping.this.afterBinaryOpsCheckBox;
                }
                if (aComponent == FmtWrapping.this.afterBinaryOpsCheckBox) {
                    return FmtWrapping.this.ternaryOpsCombo;
                }
                if (aComponent == FmtWrapping.this.whileStatementComboBox) {
                    return FmtWrapping.this.doWhileStatementCombo;
                }
                if (aComponent == FmtWrapping.this.forStatementCombo) {
                    return FmtWrapping.this.ifStatementCombo;
                }
                if (aComponent == FmtWrapping.this.ifStatementCombo) {
                    return FmtWrapping.this.whileStatementComboBox;
                }
                if (aComponent == FmtWrapping.this.arrayInitCombo) {
                    return FmtWrapping.this.arrayInitItemsCombo;
                }
                if (aComponent == FmtWrapping.this.arrayInitItemsCombo) {
                    return FmtWrapping.this.forCombo;
                }
                if (aComponent == FmtWrapping.this.forCombo) {
                    return FmtWrapping.this.forStatementCombo;
                }
                return FmtWrapping.this.variablesCombo;
            }

            @Override
            public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
                if (aComponent == FmtWrapping.this.afterDotCheckBox) {
                    return FmtWrapping.this.chainedMethodCallsCombo;
                }
                if (aComponent == FmtWrapping.this.arrayInitCombo) {
                    return FmtWrapping.this.afterDotCheckBox;
                }
                if (aComponent == FmtWrapping.this.chainedMethodCallsCombo) {
                    return FmtWrapping.this.methodCallArgsCombo;
                }
                if (aComponent == FmtWrapping.this.methodCallArgsCombo) {
                    return FmtWrapping.this.methodParamsCombo;
                }
                if (aComponent == FmtWrapping.this.methodParamsCombo) {
                    return FmtWrapping.this.variablesCombo;
                }
                if (aComponent == FmtWrapping.this.binaryOpsCombo) {
                    return FmtWrapping.this.propertiesCombo;
                }
                if (aComponent == FmtWrapping.this.propertiesCombo) {
                    return FmtWrapping.this.objectsCombo;
                }
                if (aComponent == FmtWrapping.this.objectsCombo) {
                    return FmtWrapping.this.withStatementCombo;
                }
                if (aComponent == FmtWrapping.this.assignOpsCombo) {
                    return FmtWrapping.this.afterTernaryOpsCheckBox;
                }
                if (aComponent == FmtWrapping.this.afterTernaryOpsCheckBox) {
                    return FmtWrapping.this.ternaryOpsCombo;
                }
                if (aComponent == FmtWrapping.this.afterBinaryOpsCheckBox) {
                    return FmtWrapping.this.binaryOpsCombo;
                }
                if (aComponent == FmtWrapping.this.ternaryOpsCombo) {
                    return FmtWrapping.this.afterBinaryOpsCheckBox;
                }
                if (aComponent == FmtWrapping.this.doWhileStatementCombo) {
                    return FmtWrapping.this.whileStatementComboBox;
                }
                if (aComponent == FmtWrapping.this.ifStatementCombo) {
                    return FmtWrapping.this.forStatementCombo;
                }
                if (aComponent == FmtWrapping.this.whileStatementComboBox) {
                    return FmtWrapping.this.ifStatementCombo;
                }
                if (aComponent == FmtWrapping.this.forCombo) {
                    return FmtWrapping.this.arrayInitItemsCombo;
                }
                if (aComponent == FmtWrapping.this.arrayInitItemsCombo) {
                    return FmtWrapping.this.arrayInitCombo;
                }
                if (aComponent == FmtWrapping.this.forStatementCombo) {
                    return FmtWrapping.this.forCombo;
                }
                return FmtWrapping.this.assignOpsCombo;
            }
        });
        this.variablesLabel.setLabelFor(this.variablesCombo);
        Mnemonics.setLocalizedText((JLabel)this.variablesLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_extendsImplementsList"));
        this.variablesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.methodParamsLabel.setLabelFor(this.methodParamsCombo);
        Mnemonics.setLocalizedText((JLabel)this.methodParamsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_methodParameters"));
        this.methodParamsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.methodCallArgsLabel.setLabelFor(this.methodCallArgsCombo);
        Mnemonics.setLocalizedText((JLabel)this.methodCallArgsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_methodCallArgs"));
        this.methodCallArgsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.chainedMethodCallsLabel.setLabelFor(this.chainedMethodCallsCombo);
        Mnemonics.setLocalizedText((JLabel)this.chainedMethodCallsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_chainedMethodCalls"));
        this.chainedMethodCallsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.arrayInitLabel.setLabelFor(this.arrayInitCombo);
        Mnemonics.setLocalizedText((JLabel)this.arrayInitLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_arrayInit"));
        this.arrayInitCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.forLabel.setLabelFor(this.forCombo);
        Mnemonics.setLocalizedText((JLabel)this.forLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_for"));
        this.forCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.forStatementLabel.setLabelFor(this.forStatementCombo);
        Mnemonics.setLocalizedText((JLabel)this.forStatementLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_forStatement"));
        this.forStatementCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.ifStatementLabel.setLabelFor(this.ifStatementCombo);
        Mnemonics.setLocalizedText((JLabel)this.ifStatementLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_ifStatement"));
        this.ifStatementCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.whileStatementLabel.setLabelFor(this.whileStatementComboBox);
        Mnemonics.setLocalizedText((JLabel)this.whileStatementLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_whileStatement"));
        this.whileStatementComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.doWhileStatementLabel.setLabelFor(this.doWhileStatementCombo);
        Mnemonics.setLocalizedText((JLabel)this.doWhileStatementLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_doWhileStatement"));
        this.doWhileStatementCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.binaryOpsLabel.setLabelFor(this.binaryOpsCombo);
        Mnemonics.setLocalizedText((JLabel)this.binaryOpsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_binaryOps"));
        this.binaryOpsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.ternaryOpsLabel.setLabelFor(this.ternaryOpsCombo);
        Mnemonics.setLocalizedText((JLabel)this.ternaryOpsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_ternaryOps"));
        this.ternaryOpsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.assignOpsLabel.setLabelFor(this.assignOpsCombo);
        Mnemonics.setLocalizedText((JLabel)this.assignOpsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_assignOps"));
        this.assignOpsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.withStatementLabel.setLabelFor(this.withStatementCombo);
        Mnemonics.setLocalizedText((JLabel)this.withStatementLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_withStatement"));
        this.withStatementCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        Mnemonics.setLocalizedText((AbstractButton)this.afterBinaryOpsCheckBox, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterBinaryOpsCheckBox.text"));
        this.afterBinaryOpsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this.afterTernaryOpsCheckBox, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterTernaryOpsCheckBox.text"));
        this.afterTernaryOpsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.objectsLabel.setLabelFor(this.objectsCombo);
        Mnemonics.setLocalizedText((JLabel)this.objectsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_objects"));
        this.objectsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        Mnemonics.setLocalizedText((AbstractButton)this.afterDotCheckBox, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterDotCheckBox.text"));
        this.afterDotCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.propertiesLabel.setLabelFor(this.propertiesCombo);
        Mnemonics.setLocalizedText((JLabel)this.propertiesLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesLabel.text"));
        this.propertiesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.arrayInitItemsLabel.setLabelFor(this.arrayInitItemsCombo);
        Mnemonics.setLocalizedText((JLabel)this.arrayInitItemsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitItemsLabel.text"));
        this.arrayInitItemsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GroupLayout panel1Layout = new GroupLayout(this.panel1);
        this.panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panel1Layout.createSequentialGroup().addContainerGap().addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panel1Layout.createSequentialGroup().addComponent(this.variablesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.variablesCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.methodParamsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.methodParamsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.methodCallArgsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.methodCallArgsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.chainedMethodCallsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.chainedMethodCallsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.arrayInitLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.arrayInitCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.arrayInitItemsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.arrayInitItemsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.forLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.forCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.forStatementLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.forStatementCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.ifStatementLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.ifStatementCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.whileStatementLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.whileStatementComboBox, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.doWhileStatementLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.doWhileStatementCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.withStatementLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.withStatementCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.objectsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.objectsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.propertiesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.propertiesCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.binaryOpsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.binaryOpsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.ternaryOpsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.ternaryOpsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.afterDotCheckBox).addComponent(this.afterBinaryOpsCheckBox).addComponent(this.afterTernaryOpsCheckBox)).addGap(0, 12, 32767)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.assignOpsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.assignOpsCombo, -2, -1, -2))).addContainerGap()));
        panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panel1Layout.createSequentialGroup().addContainerGap().addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.variablesLabel).addComponent(this.variablesCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.methodParamsLabel).addComponent(this.methodParamsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.methodCallArgsLabel).addComponent(this.methodCallArgsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.chainedMethodCallsLabel).addComponent(this.chainedMethodCallsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.afterDotCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.arrayInitLabel).addComponent(this.arrayInitCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.arrayInitItemsLabel).addComponent(this.arrayInitItemsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.forLabel).addComponent(this.forCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.forStatementLabel).addComponent(this.forStatementCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.ifStatementLabel).addComponent(this.ifStatementCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.whileStatementLabel).addComponent(this.whileStatementComboBox, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.doWhileStatementLabel).addComponent(this.doWhileStatementCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.withStatementLabel).addComponent(this.withStatementCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.objectsLabel).addComponent(this.objectsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.propertiesLabel).addComponent(this.propertiesCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.binaryOpsLabel).addComponent(this.binaryOpsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.afterBinaryOpsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.ternaryOpsLabel).addComponent(this.ternaryOpsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.afterTernaryOpsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.assignOpsLabel).addComponent(this.assignOpsCombo, -2, -1, -2)).addContainerGap(-1, 32767)));
        this.variablesLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.variablesLabel.AccessibleContext.accessibleName"));
        this.variablesLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.variablesLabel.AccessibleContext.accessibleDescription"));
        this.variablesCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.variablesCombo.AccessibleContext.accessibleName"));
        this.variablesCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.variablesCombo.AccessibleContext.accessibleDescription"));
        this.methodParamsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodParamsLabel.AccessibleContext.accessibleName"));
        this.methodParamsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodParamsLabel.AccessibleContext.accessibleDescription"));
        this.methodParamsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodParamsCombo.AccessibleContext.accessibleName"));
        this.methodParamsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodParamsCombo.AccessibleContext.accessibleDescription"));
        this.methodCallArgsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodCallArgsLabel.AccessibleContext.accessibleName"));
        this.methodCallArgsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodCallArgsLabel.AccessibleContext.accessibleDescription"));
        this.methodCallArgsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodCallArgsCombo.AccessibleContext.accessibleName"));
        this.methodCallArgsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.methodCallArgsCombo.AccessibleContext.accessibleDescription"));
        this.chainedMethodCallsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.chainedMethodCallsLabel.AccessibleContext.accessibleName"));
        this.chainedMethodCallsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.chainedMethodCallsLabel.AccessibleContext.accessibleDescription"));
        this.chainedMethodCallsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.chainedMethodCallsCombo.AccessibleContext.accessibleName"));
        this.chainedMethodCallsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.chainedMethodCallsCombo.AccessibleContext.accessibleDescription"));
        this.arrayInitLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitLabel.AccessibleContext.accessibleName"));
        this.arrayInitLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitLabel.AccessibleContext.accessibleDescription"));
        this.arrayInitCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitCombo.AccessibleContext.accessibleName"));
        this.arrayInitCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitCombo.AccessibleContext.accessibleDescription"));
        this.forLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forLabel.AccessibleContext.accessibleName"));
        this.forLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forLabel.AccessibleContext.accessibleDescription"));
        this.forCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forCombo.AccessibleContext.accessibleName"));
        this.forCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forCombo.AccessibleContext.accessibleDescription"));
        this.forStatementLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forStatementLabel.AccessibleContext.accessibleName"));
        this.forStatementLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forStatementLabel.AccessibleContext.accessibleDescription"));
        this.forStatementCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forStatementCombo.AccessibleContext.accessibleName"));
        this.forStatementCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.forStatementCombo.AccessibleContext.accessibleDescription"));
        this.ifStatementLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ifStatementLabel.AccessibleContext.accessibleName"));
        this.ifStatementLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ifStatementLabel.AccessibleContext.accessibleDescription"));
        this.ifStatementCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ifStatementCombo.AccessibleContext.accessibleName"));
        this.ifStatementCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ifStatementCombo.AccessibleContext.accessibleDescription"));
        this.whileStatementLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.whileStatementLabel.AccessibleContext.accessibleName"));
        this.whileStatementLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.whileStatementLabel.AccessibleContext.accessibleDescription"));
        this.whileStatementComboBox.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.whileStatementComboBox.AccessibleContext.accessibleName"));
        this.whileStatementComboBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.whileStatementComboBox.AccessibleContext.accessibleDescription"));
        this.doWhileStatementLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.doWhileStatementLabel.AccessibleContext.accessibleName"));
        this.doWhileStatementLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.doWhileStatementLabel.AccessibleContext.accessibleDescription"));
        this.doWhileStatementCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.doWhileStatementCombo.AccessibleContext.accessibleName"));
        this.doWhileStatementCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.doWhileStatementCombo.AccessibleContext.accessibleDescription"));
        this.binaryOpsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.binaryOpsLabel.AccessibleContext.accessibleName"));
        this.binaryOpsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.binaryOpsLabel.AccessibleContext.accessibleDescription"));
        this.binaryOpsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.binaryOpsCombo.AccessibleContext.accessibleName"));
        this.binaryOpsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.binaryOpsCombo.AccessibleContext.accessibleDescription"));
        this.ternaryOpsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ternaryOpsLabel.AccessibleContext.accessibleName"));
        this.ternaryOpsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ternaryOpsLabel.AccessibleContext.accessibleDescription"));
        this.ternaryOpsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ternaryOpsCombo.AccessibleContext.accessibleName"));
        this.ternaryOpsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.ternaryOpsCombo.AccessibleContext.accessibleDescription"));
        this.assignOpsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.assignOpsLabel.AccessibleContext.accessibleName"));
        this.assignOpsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.assignOpsLabel.AccessibleContext.accessibleDescription"));
        this.assignOpsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.assignOpsCombo.AccessibleContext.accessibleName"));
        this.assignOpsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.assignOpsCombo.AccessibleContext.accessibleDescription"));
        this.withStatementLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.withLabel.AccessibleContext.accessibleName"));
        this.withStatementLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.withLabel.AccessibleContext.accessibleDescription"));
        this.withStatementCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.withStatementCombo.AccessibleContext.accessibleName"));
        this.withStatementCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.withStatementCombo.AccessibleContext.accessibleDescription"));
        this.afterBinaryOpsCheckBox.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterBinaryOpsCheckBox.AccessibleContext.accessibleName"));
        this.afterBinaryOpsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterBinaryOpsCheckBox.AccessibleContext.accessibleDescription"));
        this.afterTernaryOpsCheckBox.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterTernaryOpsCheckBox.AccessibleContext.accessibleName"));
        this.afterTernaryOpsCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterTernaryOpsCheckBox.AccessibleContext.accessibleDescription"));
        this.objectsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsLabel.AccessibleContext.accessibleName"));
        this.objectsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsLabel.AccessibleContext.accessibleDescription"));
        this.objectsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsCombo.AccessibleContext.accessibleName"));
        this.objectsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsCombo.AccessibleContext.accessibleDescription"));
        this.afterDotCheckBox.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterDotCheckBox.AccessibleContext.accessibleName"));
        this.afterDotCheckBox.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.afterDotCheckBox.AccessibleContext.accessibleDescription"));
        this.propertiesLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesLabel.AccessibleContext.accessibleName"));
        this.propertiesLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesLabel.AccessibleContext.accessibleDescription"));
        this.propertiesCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesCombo.AccessibleContext.accessibleName"));
        this.propertiesCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesCombo.AccessibleContext.accessibleDescription"));
        this.arrayInitItemsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitItemsLabel.AccessibleContext.accessibleName"));
        this.arrayInitItemsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitItemsLabel.AccessibleContext.accessibleDescription"));
        this.arrayInitItemsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitItemsCombo.AccessibleContext.accessibleName"));
        this.arrayInitItemsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitItemsCombo.AccessibleContext.accessibleDescription"));
        this.scrollPane.setViewportView(this.panel1);
        this.panel1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.panel1.AccessibleContext.accessibleName"));
        this.panel1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.panel1.AccessibleContext.accessibleDescription"));
        this.add((Component)this.scrollPane, "Center");
        this.scrollPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.scrollPane.AccessibleContext.accessibleName"));
        this.scrollPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.scrollPane.AccessibleContext.accessibleDescription"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.AccessibleContext.accessibleDescription"));
    }

}

