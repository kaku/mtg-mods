/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.OffsetRange;

public interface JsElement
extends ElementHandle {
    public int getOffset();

    public OffsetRange getOffsetRange();

    public Kind getJSKind();

    public boolean isDeclared();

    public boolean isPlatform();

    public String getSourceLabel();

    public static enum Kind {
        FUNCTION(1),
        METHOD(2),
        CONSTRUCTOR(3),
        OBJECT(4),
        PROPERTY(5),
        VARIABLE(6),
        FIELD(7),
        FILE(8),
        PARAMETER(9),
        ANONYMOUS_OBJECT(10),
        PROPERTY_GETTER(11),
        PROPERTY_SETTER(12),
        OBJECT_LITERAL(13),
        CATCH_BLOCK(14),
        WITH_OBJECT(15);
        
        private final int id;
        private static final Map<Integer, Kind> LOOKUP;

        private Kind(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }

        public static Kind fromId(int id) {
            return LOOKUP.get(id);
        }

        public boolean isFunction() {
            return this == FUNCTION || this == METHOD || this == CONSTRUCTOR || this == PROPERTY_GETTER || this == PROPERTY_SETTER;
        }

        public boolean isPropertyGetterSetter() {
            return this == PROPERTY_GETTER || this == PROPERTY_SETTER;
        }

        static {
            LOOKUP = new HashMap<Integer, Kind>();
            for (Kind kind : EnumSet.allOf(Kind.class)) {
                LOOKUP.put(kind.getId(), kind);
            }
        }
    }

}

