/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;

public class TypeUsageImpl
implements TypeUsage {
    private final String type;
    private final int offset;
    private final boolean resolved;

    public TypeUsageImpl(String type, int offset, boolean resolved) {
        this.type = type;
        this.offset = offset;
        this.resolved = resolved;
    }

    public TypeUsageImpl(String type, int offset) {
        this(type, offset, false);
    }

    public TypeUsageImpl(String type) {
        this(type, -1, false);
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public int getOffset() {
        return this.offset;
    }

    @Override
    public boolean isResolved() {
        return this.resolved;
    }

    @Override
    public String getDisplayName() {
        return ModelUtils.getDisplayName(this.type);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        TypeUsageImpl other = (TypeUsageImpl)obj;
        if (this.type == null ? other.type != null : !this.type.equals(other.type)) {
            return false;
        }
        if (this.offset != other.offset) {
            return false;
        }
        if (this.resolved != other.resolved) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 83 * hash + this.offset;
        hash = 83 * hash + (this.resolved ? 1 : 0);
        return hash;
    }

    public String toString() {
        return "TypeUsageImpl{type=" + this.type + ", offset=" + this.offset + ", resolved=" + this.resolved + '}';
    }
}

