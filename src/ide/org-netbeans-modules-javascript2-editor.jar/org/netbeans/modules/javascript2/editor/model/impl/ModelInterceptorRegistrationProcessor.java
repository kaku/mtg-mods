/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import org.netbeans.modules.javascript2.editor.spi.model.ModelInterceptor;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedAnnotationTypes(value={"org.netbeans.modules.javascript2.editor.spi.model.ModelInterceptor.Registration"})
@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class ModelInterceptorRegistrationProcessor
extends LayerGeneratingProcessor {
    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        for (Element element : roundEnv.getElementsAnnotatedWith(ModelInterceptor.Registration.class)) {
            this.layer(new Element[]{element}).instanceFile("JavaScript/Model/ModelInterceptors", null, ModelInterceptor.class).position(element.getAnnotation(ModelInterceptor.Registration.class).priority()).write();
        }
        return true;
    }
}

