/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.EnumSet;
import java.util.Set;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.impl.JsArrayImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;

public class AnonymousObject
extends JsObjectImpl {
    public AnonymousObject(JsObject parent, String name, OffsetRange offsetRange, String mimeType, String sourceLabel) {
        super(parent, name, true, offsetRange, EnumSet.of(Modifier.PRIVATE), mimeType, sourceLabel);
    }

    @Override
    public JsElement.Kind getJSKind() {
        return JsElement.Kind.ANONYMOUS_OBJECT;
    }

    @Override
    public boolean isAnonymous() {
        return true;
    }

    @Override
    public int getOffset() {
        return this.getOffsetRange().getStart();
    }

    @Override
    public boolean hasExactName() {
        return false;
    }

    public static class AnonymousArray
    extends JsArrayImpl {
        public AnonymousArray(JsObject parent, String name, OffsetRange offsetRange, String mimeType, String sourceLabel) {
            super(parent, name, true, offsetRange, EnumSet.of(Modifier.PRIVATE), mimeType, sourceLabel);
        }

        @Override
        public JsElement.Kind getJSKind() {
            return JsElement.Kind.ANONYMOUS_OBJECT;
        }

        @Override
        public boolean isAnonymous() {
            return true;
        }

        @Override
        public int getOffset() {
            return this.getOffsetRange().getStart();
        }

        @Override
        public boolean hasExactName() {
            return false;
        }
    }

}

