/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.api;

public class FrameworksUtils {
    public static final String CATEGORY = "jsframeworks";
    protected static final String CATEGORY_LABEL = "JavaScript Frameworks";
    public static final String HTML5_CLIENT_PROJECT = "org.netbeans.modules.web.clientproject";
    public static final String PHP_PROJECT = "org-netbeans-modules-php-project";
    public static final String MAVEN_PROJECT = "org-netbeans-modules-maven";
}

