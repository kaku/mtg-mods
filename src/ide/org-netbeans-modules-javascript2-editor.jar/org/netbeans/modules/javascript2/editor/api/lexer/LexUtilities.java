/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.javascript2.editor.api.lexer;

import java.util.Arrays;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.util.Exceptions;

public final class LexUtilities {
    private LexUtilities() {
    }

    public static TokenSequence<? extends JsTokenId> getJsTokenSequence(Document doc, int offset) {
        TokenHierarchy th = TokenHierarchy.get((Document)doc);
        return LexUtilities.getTokenSequence(th, offset, JsTokenId.javascriptLanguage());
    }

    @CheckForNull
    public static TokenSequence<? extends JsTokenId> getTokenSequence(Snapshot snapshot, int offset, Language<JsTokenId> language) {
        TokenHierarchy th = snapshot.getTokenHierarchy();
        return LexUtilities.getTokenSequence(th, offset, language);
    }

    @CheckForNull
    public static TokenSequence<? extends JsTokenId> getTokenSequence(Document doc, int offset, Language<JsTokenId> language) {
        TokenHierarchy th = TokenHierarchy.get((Document)doc);
        return LexUtilities.getTokenSequence(th, offset, language);
    }

    @CheckForNull
    public static TokenSequence<? extends JsTokenId> getJsTokenSequence(Snapshot snapshot, int offset) {
        TokenHierarchy th = snapshot.getTokenHierarchy();
        return LexUtilities.getTokenSequence(th, offset, JsTokenId.javascriptLanguage());
    }

    @CheckForNull
    public static TokenSequence<? extends JsTokenId> getJsTokenSequence(TokenHierarchy<?> th, int offset) {
        return LexUtilities.getTokenSequence(th, offset, JsTokenId.javascriptLanguage());
    }

    public static TokenSequence<? extends JsDocumentationTokenId> getJsDocumentationTokenSequence(TokenHierarchy<?> th, int offset) {
        return LexUtilities.getTokenSequence(th, offset, JsDocumentationTokenId.language());
    }

    public static TokenSequence<? extends JsDocumentationTokenId> getJsDocumentationTokenSequence(Snapshot snapshot, int offset) {
        return LexUtilities.getTokenSequence(snapshot.getTokenHierarchy(), offset, JsDocumentationTokenId.language());
    }

    @CheckForNull
    public static <K> TokenSequence<? extends K> getTokenSequence(TokenHierarchy<?> th, int offset, Language<? extends K> language) {
        TokenSequence ts = th.tokenSequence(language);
        if (ts == null) {
            List list = th.embeddedTokenSequences(offset, true);
            for (TokenSequence t2 : list) {
                if (t2.language() != language) continue;
                ts = t2;
                break;
            }
            if (ts == null) {
                list = th.embeddedTokenSequences(offset, false);
                for (TokenSequence t2 : list) {
                    if (t2.language() != language) continue;
                    ts = t2;
                    break;
                }
            }
        }
        return ts;
    }

    public static TokenSequence<? extends JsTokenId> getNextJsTokenSequence(Document doc, int fromOffset, int max, Language<JsTokenId> language) {
        TokenHierarchy th = TokenHierarchy.get((Document)doc);
        TokenSequence ts = th.tokenSequence();
        ts.move(fromOffset);
        return LexUtilities.findNextJsTokenSequence(ts, fromOffset, max, language);
    }

    private static TokenSequence<? extends JsTokenId> findNextJsTokenSequence(TokenSequence<?> ts, int fromOffset, int max, Language<JsTokenId> language) {
        if (ts.language() == language) {
            if (!ts.moveNext()) {
                return null;
            }
            return ts;
        }
        while (ts.moveNext() && ts.offset() <= max) {
            int offset = ts.offset();
            TokenSequence ets = ts.embedded();
            if (ets == null) continue;
            ets.move(offset);
            TokenSequence<? extends JsTokenId> result = LexUtilities.findNextJsTokenSequence(ets, fromOffset, max, language);
            if (result == null) continue;
            return result;
        }
        return null;
    }

    public static OffsetRange findFwd(Document doc, TokenSequence<? extends JsTokenId> ts, TokenId up, TokenId down) {
        int balance = 0;
        while (ts.moveNext()) {
            Token token = ts.token();
            TokenId id = token.id();
            if (id == up) {
                ++balance;
                continue;
            }
            if (id != down) continue;
            if (balance == 0) {
                return new OffsetRange(ts.offset(), ts.offset() + token.length());
            }
            --balance;
        }
        return OffsetRange.NONE;
    }

    public static OffsetRange findBwd(Document doc, TokenSequence<? extends JsTokenId> ts, TokenId up, TokenId down) {
        int balance = 0;
        while (ts.movePrevious()) {
            Token token = ts.token();
            TokenId id = token.id();
            if (id == up) {
                if (balance == 0) {
                    return new OffsetRange(ts.offset(), ts.offset() + token.length());
                }
                ++balance;
                continue;
            }
            if (id != down) continue;
            --balance;
        }
        return OffsetRange.NONE;
    }

    public static Token<? extends JsTokenId> getToken(Document doc, int offset, Language<JsTokenId> language) {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getPositionedSequence(doc, offset, language);
        if (ts != null) {
            return ts.token();
        }
        return null;
    }

    public static char getTokenChar(Document doc, int offset, Language<JsTokenId> language) {
        String text;
        Token<? extends JsTokenId> token = LexUtilities.getToken(doc, offset, language);
        if (token != null && (text = token.text().toString()).length() > 0) {
            return text.charAt(0);
        }
        return '\u0000';
    }

    public static int getTokenBalance(Document doc, TokenId open, TokenId close, int offset, Language<JsTokenId> language) throws BadLocationException {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(doc, offset, language);
        if (ts == null) {
            return 0;
        }
        ts.moveIndex(0);
        if (!ts.moveNext()) {
            return 0;
        }
        int balance = 0;
        do {
            Token t;
            if ((t = ts.token()).id() == open) {
                ++balance;
                continue;
            }
            if (t.id() != close) continue;
            --balance;
        } while (ts.moveNext());
        return balance;
    }

    public static boolean isCommentOnlyLine(BaseDocument doc, int offset, Language<JsTokenId> language) throws BadLocationException {
        int begin = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
        if (begin == -1) {
            return false;
        }
        Token<? extends JsTokenId> token = LexUtilities.getToken((Document)doc, begin, language);
        if (token != null) {
            return token.id() == JsTokenId.LINE_COMMENT;
        }
        return false;
    }

    public static int getLineBalance(BaseDocument doc, int offset, TokenId up, TokenId down) {
        try {
            int begin = Utilities.getRowStart((BaseDocument)doc, (int)offset);
            int end = Utilities.getRowEnd((BaseDocument)doc, (int)offset);
            TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence((Document)doc, begin);
            if (ts == null) {
                return 0;
            }
            ts.move(begin);
            if (!ts.moveNext()) {
                return 0;
            }
            int balance = 0;
            do {
                Token token;
                TokenId id;
                if ((id = (token = ts.token()).id()) == up) {
                    ++balance;
                    continue;
                }
                if (id != down) continue;
                --balance;
            } while (ts.moveNext() && ts.offset() <= end);
            return balance;
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
            return 0;
        }
    }

    public static TokenSequence<? extends JsTokenId> getPositionedSequence(Document doc, int offset, Language<JsTokenId> language) {
        return LexUtilities.getPositionedSequence(doc, offset, true, language);
    }

    public static TokenSequence<? extends JsTokenId> getPositionedSequence(Snapshot snapshot, int offset, Language<JsTokenId> language) {
        return LexUtilities.getPositionedSequence(snapshot, offset, true, language);
    }

    public static TokenSequence<? extends JsTokenId> getJsPositionedSequence(Document doc, int offset) {
        return LexUtilities.getPositionedSequence(doc, offset, true, JsTokenId.javascriptLanguage());
    }

    public static TokenSequence<? extends JsTokenId> getJsPositionedSequence(Snapshot snapshot, int offset) {
        return LexUtilities.getPositionedSequence(snapshot, offset, true, JsTokenId.javascriptLanguage());
    }

    public static TokenSequence<? extends JsTokenId> getPositionedSequence(Document doc, int offset, boolean lookBack, Language<JsTokenId> language) {
        TokenHierarchy th = TokenHierarchy.get((Document)doc);
        return LexUtilities._getPosSeq(LexUtilities.getTokenSequence(th, offset, language), offset, lookBack);
    }

    private static TokenSequence<? extends JsTokenId> getPositionedSequence(Snapshot snapshot, int offset, boolean lookBack, Language<JsTokenId> language) {
        TokenHierarchy th = snapshot.getTokenHierarchy();
        return LexUtilities._getPosSeq(LexUtilities.getTokenSequence(th, offset, language), offset, lookBack);
    }

    private static <K> TokenSequence<? extends K> _getPosSeq(TokenSequence<? extends K> ts, int offset, boolean lookBack) {
        if (ts != null) {
            ts.move(offset);
            if (!lookBack && !ts.moveNext()) {
                return null;
            }
            if (lookBack && !ts.moveNext() && !ts.movePrevious()) {
                return null;
            }
            return ts;
        }
        return null;
    }

    public static int getLexerOffset(JsParserResult info, int astOffset) {
        return info.getSnapshot().getOriginalOffset(astOffset);
    }

    public static OffsetRange getLexerOffsets(JsParserResult info, OffsetRange astRange) {
        int rangeStart = astRange.getStart();
        int start = info.getSnapshot().getOriginalOffset(rangeStart);
        if (start == rangeStart) {
            return astRange;
        }
        if (start == -1) {
            return OffsetRange.NONE;
        }
        return new OffsetRange(start, start + astRange.getLength());
    }

    public static Token<? extends JsTokenId> findNext(TokenSequence<? extends JsTokenId> ts, List<JsTokenId> ignores) {
        if (ignores.contains((Object)ts.token().id())) {
            while (ts.moveNext() && ignores.contains((Object)ts.token().id())) {
            }
        }
        return ts.token();
    }

    public static Token<? extends JsTokenId> findPrevious(TokenSequence<? extends JsTokenId> ts, List<JsTokenId> ignores) {
        if (ignores.contains((Object)ts.token().id())) {
            while (ts.movePrevious() && ignores.contains((Object)ts.token().id())) {
            }
        }
        return ts.token();
    }

    public static Token<? extends JsTokenId> findNextToken(TokenSequence<? extends JsTokenId> ts, List<JsTokenId> lookfor) {
        if (!lookfor.contains((Object)ts.token().id())) {
            while (ts.moveNext() && !lookfor.contains((Object)ts.token().id())) {
            }
        }
        return ts.token();
    }

    public static Token<? extends JsTokenId> findPreviousToken(TokenSequence<? extends JsTokenId> ts, List<JsTokenId> lookfor) {
        if (!lookfor.contains((Object)ts.token().id())) {
            while (ts.movePrevious() && !lookfor.contains((Object)ts.token().id())) {
            }
        }
        return ts.token();
    }

    public static Token<? extends JsTokenId> findNextIncluding(TokenSequence<? extends JsTokenId> ts, List<JsTokenId> includes) {
        while (ts.moveNext() && !includes.contains((Object)ts.token().id())) {
        }
        return ts.token();
    }

    public static Token<? extends JsTokenId> findPreviousIncluding(TokenSequence<? extends JsTokenId> ts, List<JsTokenId> includes) {
        while (ts.movePrevious() && !includes.contains((Object)ts.token().id())) {
        }
        return ts.token();
    }

    public static Token<? extends JsTokenId> findNextNonWsNonComment(TokenSequence<? extends JsTokenId> ts) {
        return LexUtilities.findNext(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL, JsTokenId.LINE_COMMENT, JsTokenId.BLOCK_COMMENT, JsTokenId.DOC_COMMENT}));
    }

    public static Token<? extends JsTokenId> findPreviousNonWsNonComment(TokenSequence<? extends JsTokenId> ts) {
        return LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL, JsTokenId.LINE_COMMENT, JsTokenId.BLOCK_COMMENT, JsTokenId.DOC_COMMENT}));
    }

    public static OffsetRange getMultilineRange(Document doc, TokenSequence<? extends JsTokenId> ts) {
        int index = ts.index();
        OffsetRange offsetRange = LexUtilities.findMultilineRange(ts);
        ts.moveIndex(index);
        ts.moveNext();
        return offsetRange;
    }

    private static OffsetRange findMultilineRange(TokenSequence<? extends JsTokenId> ts) {
        int startOffset = ts.offset();
        JsTokenId id = (JsTokenId)ts.token().id();
        switch (id) {
            case KEYWORD_ELSE: {
                ts.moveNext();
                id = (JsTokenId)ts.token().id();
                break;
            }
            case KEYWORD_IF: 
            case KEYWORD_FOR: 
            case KEYWORD_WHILE: {
                ts.moveNext();
                if (!LexUtilities.skipParenthesis(ts, false)) {
                    return OffsetRange.NONE;
                }
                id = (JsTokenId)ts.token().id();
                break;
            }
            default: {
                return OffsetRange.NONE;
            }
        }
        boolean eolFound = false;
        int lastEolOffset = ts.offset();
        if (id == JsTokenId.WHITESPACE || id == JsTokenId.LINE_COMMENT || id == JsTokenId.BLOCK_COMMENT || id == JsTokenId.DOC_COMMENT || id == JsTokenId.EOL) {
            if (ts.token().id() == JsTokenId.EOL) {
                lastEolOffset = ts.offset();
                eolFound = true;
            }
            while (ts.moveNext() && (ts.token().id() == JsTokenId.WHITESPACE || ts.token().id() == JsTokenId.LINE_COMMENT || ts.token().id() == JsTokenId.EOL || ts.token().id() == JsTokenId.BLOCK_COMMENT || ts.token().id() == JsTokenId.DOC_COMMENT)) {
                if (ts.token().id() != JsTokenId.EOL) continue;
                lastEolOffset = ts.offset();
                eolFound = true;
            }
        }
        if (ts.token() == null || ts.token().id() != JsTokenId.BRACKET_LEFT_CURLY && eolFound) {
            return new OffsetRange(startOffset, lastEolOffset);
        }
        return OffsetRange.NONE;
    }

    public static boolean skipParenthesis(TokenSequence<? extends JsTokenId> ts, boolean back) {
        int balance = 0;
        Token token = ts.token();
        if (token == null) {
            return false;
        }
        TokenId id = token.id();
        if (id == JsTokenId.WHITESPACE || id == JsTokenId.EOL) {
            while ((back ? ts.movePrevious() : ts.moveNext()) && (ts.token().id() == JsTokenId.WHITESPACE || ts.token().id() == JsTokenId.EOL)) {
            }
        }
        if (ts.token().id() != (back ? JsTokenId.BRACKET_RIGHT_PAREN : JsTokenId.BRACKET_LEFT_PAREN)) {
            return false;
        }
        do {
            if ((id = (token = ts.token()).id()) == (back ? JsTokenId.BRACKET_RIGHT_PAREN : JsTokenId.BRACKET_LEFT_PAREN)) {
                ++balance;
            } else if (id == (back ? JsTokenId.BRACKET_LEFT_PAREN : JsTokenId.BRACKET_RIGHT_PAREN)) {
                if (balance == 0) {
                    return false;
                }
                if (balance == 1) {
                    if (back) {
                        ts.movePrevious();
                    } else {
                        ts.moveNext();
                    }
                    return true;
                }
                --balance;
            }
            if (back) {
                if (!ts.movePrevious()) break;
                continue;
            }
            if (!ts.moveNext()) break;
        } while (true);
        return false;
    }

    public static boolean isBinaryOperator(JsTokenId id, JsTokenId previous) {
        switch (id) {
            case OPERATOR_GREATER: 
            case OPERATOR_LOWER: 
            case OPERATOR_EQUALS: 
            case OPERATOR_EQUALS_EXACTLY: 
            case OPERATOR_LOWER_EQUALS: 
            case OPERATOR_GREATER_EQUALS: 
            case OPERATOR_NOT_EQUALS: 
            case OPERATOR_NOT_EQUALS_EXACTLY: 
            case OPERATOR_AND: 
            case OPERATOR_OR: 
            case OPERATOR_MULTIPLICATION: 
            case OPERATOR_DIVISION: 
            case OPERATOR_BITWISE_AND: 
            case OPERATOR_BITWISE_OR: 
            case OPERATOR_BITWISE_XOR: 
            case OPERATOR_MODULUS: 
            case OPERATOR_LEFT_SHIFT_ARITHMETIC: 
            case OPERATOR_RIGHT_SHIFT_ARITHMETIC: 
            case OPERATOR_RIGHT_SHIFT: 
            case OPERATOR_ASSIGNMENT: 
            case OPERATOR_PLUS_ASSIGNMENT: 
            case OPERATOR_MINUS_ASSIGNMENT: 
            case OPERATOR_MULTIPLICATION_ASSIGNMENT: 
            case OPERATOR_DIVISION_ASSIGNMENT: 
            case OPERATOR_BITWISE_AND_ASSIGNMENT: 
            case OPERATOR_BITWISE_OR_ASSIGNMENT: 
            case OPERATOR_BITWISE_XOR_ASSIGNMENT: 
            case OPERATOR_MODULUS_ASSIGNMENT: 
            case OPERATOR_LEFT_SHIFT_ARITHMETIC_ASSIGNMENT: 
            case OPERATOR_RIGHT_SHIFT_ARITHMETIC_ASSIGNMENT: 
            case OPERATOR_RIGHT_SHIFT_ASSIGNMENT: 
            case OPERATOR_DOT: {
                return true;
            }
            case OPERATOR_PLUS: 
            case OPERATOR_MINUS: {
                if (previous != null && (previous == JsTokenId.IDENTIFIER || previous == JsTokenId.NUMBER || previous == JsTokenId.REGEXP_END || previous == JsTokenId.STRING_END || previous == JsTokenId.BRACKET_RIGHT_BRACKET || previous == JsTokenId.BRACKET_RIGHT_CURLY || previous == JsTokenId.BRACKET_RIGHT_PAREN)) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

}

