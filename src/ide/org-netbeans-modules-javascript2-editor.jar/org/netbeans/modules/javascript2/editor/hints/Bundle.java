/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String ArrayTrailingCommaDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"ArrayTrailingCommaDescription");
    }

    static String ArrayTrailingCommaDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"ArrayTrailingCommaDisplayName");
    }

    static String AssignmentCondition() {
        return NbBundle.getMessage(Bundle.class, (String)"AssignmentCondition");
    }

    static String AssignmentInConditionDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"AssignmentInConditionDescription");
    }

    static String AssignmentInConditionDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"AssignmentInConditionDisplayName");
    }

    static String BetterConditionDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"BetterConditionDescription");
    }

    static String BetterConditionDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"BetterConditionDisplayName");
    }

    static String CTL_Hints_DisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_Hints_DisplayName");
    }

    static String CTL_Hints_ToolTip() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_Hints_ToolTip");
    }

    static String DuplicateName(Object name_of_the_duplicated_property) {
        return NbBundle.getMessage(Bundle.class, (String)"DuplicateName", (Object)name_of_the_duplicated_property);
    }

    static String DuplicatePropertyNameDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"DuplicatePropertyNameDescription");
    }

    static String DuplicatePropertyNameDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"DuplicatePropertyNameDisplayName");
    }

    static String ExpectedInstead(Object expected_char_or_string, Object usually_text_where_is_expected_the_first_parameter) {
        return NbBundle.getMessage(Bundle.class, (String)"ExpectedInstead", (Object)expected_char_or_string, (Object)usually_text_where_is_expected_the_first_parameter);
    }

    static String IncorrectDocumentationRuleDN() {
        return NbBundle.getMessage(Bundle.class, (String)"IncorrectDocumentationRuleDN");
    }

    static String IncorrectDocumentationRuleDesc() {
        return NbBundle.getMessage(Bundle.class, (String)"IncorrectDocumentationRuleDesc");
    }

    static String IncorrectDocumentationRuleDisplayDescription(Object parameter_name_which_is_incorectly_specified) {
        return NbBundle.getMessage(Bundle.class, (String)"IncorrectDocumentationRuleDisplayDescription", (Object)parameter_name_which_is_incorectly_specified);
    }

    static String JsConventionHintDesc() {
        return NbBundle.getMessage(Bundle.class, (String)"JsConventionHintDesc");
    }

    static String JsConventionHintDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"JsConventionHintDisplayName");
    }

    static String JsDocumentationHintDesc() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationHintDesc");
    }

    static String JsDocumentationHintDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationHintDisplayName");
    }

    static String JsErrorRule_displayName() {
        return NbBundle.getMessage(Bundle.class, (String)"JsErrorRule.displayName");
    }

    static String JsGlobalIsNotDefinedDN() {
        return NbBundle.getMessage(Bundle.class, (String)"JsGlobalIsNotDefinedDN");
    }

    static String JsGlobalIsNotDefinedDesc() {
        return NbBundle.getMessage(Bundle.class, (String)"JsGlobalIsNotDefinedDesc");
    }

    static String JsGlobalIsNotDefinedHintDesc(Object name_of_global_variable) {
        return NbBundle.getMessage(Bundle.class, (String)"JsGlobalIsNotDefinedHintDesc", (Object)name_of_global_variable);
    }

    static String JsSwitchRule_displayName() {
        return NbBundle.getMessage(Bundle.class, (String)"JsSwitchRule.displayName");
    }

    static String JsWeirdAssignmentDN() {
        return NbBundle.getMessage(Bundle.class, (String)"JsWeirdAssignmentDN");
    }

    static String JsWeirdAssignmentDesc() {
        return NbBundle.getMessage(Bundle.class, (String)"JsWeirdAssignmentDesc");
    }

    static String MSG_HINT_DISABLE_ERROR_CHECKS_FILE() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_HINT_DISABLE_ERROR_CHECKS_FILE");
    }

    static String MSG_HINT_DISABLE_ERROR_CHECKS_MIMETYPE(Object file_mime_type) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_HINT_DISABLE_ERROR_CHECKS_MIMETYPE", (Object)file_mime_type);
    }

    static String MSG_HINT_ENABLE_ERROR_CHECKS_FILE() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_HINT_ENABLE_ERROR_CHECKS_FILE");
    }

    static String MSG_HINT_ENABLE_ERROR_CHECKS_FILE_DESCR() {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_HINT_ENABLE_ERROR_CHECKS_FILE_DESCR");
    }

    static String MSG_HINT_ENABLE_ERROR_CHECKS_MIMETYPE(Object file_mime_type) {
        return NbBundle.getMessage(Bundle.class, (String)"MSG_HINT_ENABLE_ERROR_CHECKS_MIMETYPE", (Object)file_mime_type);
    }

    static String MissingSemicolon(Object char_where_is_expected_the_semicolon) {
        return NbBundle.getMessage(Bundle.class, (String)"MissingSemicolon", (Object)char_where_is_expected_the_semicolon);
    }

    static String MissingSemicolonDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"MissingSemicolonDescription");
    }

    static String MissingSemicolonDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"MissingSemicolonDisplayName");
    }

    static String ObjectTrailingCommaDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"ObjectTrailingCommaDescription");
    }

    static String ObjectTrailingCommaDisplayName() {
        return NbBundle.getMessage(Bundle.class, (String)"ObjectTrailingCommaDisplayName");
    }

    static String UndocumentedParameterRuleDN() {
        return NbBundle.getMessage(Bundle.class, (String)"UndocumentedParameterRuleDN");
    }

    static String UndocumentedParameterRuleDesc() {
        return NbBundle.getMessage(Bundle.class, (String)"UndocumentedParameterRuleDesc");
    }

    static String UndocumentedParameterRuleDisplayDescription(Object parameter_name_which_is_undocumented) {
        return NbBundle.getMessage(Bundle.class, (String)"UndocumentedParameterRuleDisplayDescription", (Object)parameter_name_which_is_undocumented);
    }

    static String UnexpectedArrayTrailing(Object the_eunexpected_token) {
        return NbBundle.getMessage(Bundle.class, (String)"UnexpectedArrayTrailing", (Object)the_eunexpected_token);
    }

    static String UnexpectedObjectTrailing(Object the_eunexpected_token) {
        return NbBundle.getMessage(Bundle.class, (String)"UnexpectedObjectTrailing", (Object)the_eunexpected_token);
    }

    private void Bundle() {
    }
}

