/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

public enum ExtDocElementType {
    DESCRIPTION("description", Category.DESCRIPTION),
    UNKNOWN("unknown", Category.UNKNOWN),
    CFG("@cfg", Category.TYPE_NAMED),
    CLASS("@class", Category.IDENT_DESCRIBED),
    CONSTRUCTOR("@constructor", Category.SIMPLE),
    EVENT("@event", Category.IDENT_DESCRIBED),
    EXTENDS("@extends", Category.IDENT_SIMPLE),
    HIDE("@hide", Category.SIMPLE),
    IGNORE("@ignore", Category.SIMPLE),
    LINK("@link", Category.IDENT_DESCRIBED),
    MEMBER("@member", Category.IDENT_SIMPLE),
    METHOD("@method", Category.SIMPLE),
    NAMESPACE("@namespace", Category.IDENT_SIMPLE),
    PARAM("@param", Category.TYPE_NAMED),
    PRIVATE("@private", Category.SIMPLE),
    PROPERTY("@property", Category.SIMPLE),
    RETURN("@return", Category.TYPE_DESCRIBED),
    SINGLETON("@singleton", Category.SIMPLE),
    STATIC("@static", Category.SIMPLE),
    TYPE("@type", Category.TYPE_SIMPLE);
    
    private final String value;
    private final Category category;

    private ExtDocElementType(String textValue, Category category) {
        this.value = textValue;
        this.category = category;
    }

    public String toString() {
        return this.value;
    }

    public Category getCategory() {
        return this.category;
    }

    public static ExtDocElementType fromString(String value) {
        if (value != null) {
            for (ExtDocElementType type : ExtDocElementType.values()) {
                if (!value.equalsIgnoreCase(type.toString())) continue;
                return type;
            }
        }
        return UNKNOWN;
    }

    public static enum Category {
        DESCRIPTION,
        IDENT_SIMPLE,
        IDENT_DESCRIBED,
        SIMPLE,
        UNKNOWN,
        TYPE_SIMPLE,
        TYPE_NAMED,
        TYPE_DESCRIBED;
        

        private Category() {
        }
    }

}

