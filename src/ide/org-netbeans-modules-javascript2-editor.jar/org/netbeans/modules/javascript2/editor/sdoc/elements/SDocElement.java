/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public interface SDocElement {
    public SDocElementType getType();
}

