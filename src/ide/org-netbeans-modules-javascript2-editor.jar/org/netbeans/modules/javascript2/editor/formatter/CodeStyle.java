/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.prefs.Preferences;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.netbeans.modules.javascript2.editor.formatter.Defaults;
import org.netbeans.modules.javascript2.editor.formatter.FormatContext;
import org.netbeans.modules.javascript2.editor.formatter.IndentContext;

public final class CodeStyle {
    private final Defaults.Provider provider;
    private final Preferences preferences;

    private CodeStyle(Defaults.Provider provider, Preferences preferences) {
        this.provider = provider;
        this.preferences = preferences;
    }

    public static CodeStyle get(Preferences prefs, Defaults.Provider provider) {
        return new CodeStyle(provider, prefs);
    }

    public static CodeStyle get(Document doc, Defaults.Provider provider) {
        return new CodeStyle(provider, CodeStylePreferences.get((Document)doc).getPreferences());
    }

    public static CodeStyle get(FormatContext context) {
        return CodeStyle.get(context.getDefaultsProvider(), (Document)context.getDocument(), context.isEmbedded());
    }

    public static CodeStyle get(IndentContext context) {
        return CodeStyle.get(context.getDefaultsProvider(), (Document)context.getDocument(), context.isEmbedded());
    }

    private static CodeStyle get(Defaults.Provider provider, Document doc, boolean embedded) {
        if (embedded) {
            return new CodeStyle(provider, CodeStylePreferences.get((Document)doc, (String)"text/javascript").getPreferences());
        }
        return new CodeStyle(provider, CodeStylePreferences.get((Document)doc).getPreferences());
    }

    public Holder toHolder() {
        return new Holder(this);
    }

    public boolean expandTabToSpaces() {
        return this.preferences.getBoolean("expand-tabs", this.provider.getDefaultAsBoolean("expand-tabs"));
    }

    public int getTabSize() {
        return this.preferences.getInt("tab-size", this.provider.getDefaultAsInt("tab-size"));
    }

    public int getIndentSize() {
        return this.preferences.getInt("indent-shift-width", this.provider.getDefaultAsInt("indent-shift-width"));
    }

    public int getContinuationIndentSize() {
        return this.preferences.getInt("continuationIndentSize", this.provider.getDefaultAsInt("continuationIndentSize"));
    }

    public int getItemsInArrayDeclarationIndentSize() {
        return this.preferences.getInt("itemsInArrayDeclarationIndentSize", this.provider.getDefaultAsInt("itemsInArrayDeclarationIndentSize"));
    }

    public int getInitialIndent() {
        return this.preferences.getInt("init.indent", this.provider.getDefaultAsInt("init.indent"));
    }

    public boolean reformatComments() {
        return this.preferences.getBoolean("reformatComments", this.provider.getDefaultAsBoolean("reformatComments"));
    }

    public boolean indentHtml() {
        return this.preferences.getBoolean("indentHtml", this.provider.getDefaultAsBoolean("indentHtml"));
    }

    public int getRightMargin() {
        return this.preferences.getInt("text-limit-width", this.provider.getDefaultAsInt("text-limit-width"));
    }

    public BracePlacement getClassDeclBracePlacement() {
        String placement = this.preferences.get("classDeclBracePlacement", this.provider.getDefaultAsString("classDeclBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getMethodDeclBracePlacement() {
        String placement = this.preferences.get("methodDeclBracePlacement", this.provider.getDefaultAsString("methodDeclBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getIfBracePlacement() {
        String placement = this.preferences.get("ifBracePlacement", this.provider.getDefaultAsString("ifBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getForBracePlacement() {
        String placement = this.preferences.get("forBracePlacement", this.provider.getDefaultAsString("forBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getWhileBracePlacement() {
        String placement = this.preferences.get("whileBracePlacement", this.provider.getDefaultAsString("whileBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getSwitchBracePlacement() {
        String placement = this.preferences.get("switchBracePlacement", this.provider.getDefaultAsString("switchBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getCatchBracePlacement() {
        String placement = this.preferences.get("catchBracePlacement", this.provider.getDefaultAsString("catchBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getUseTraitBodyBracePlacement() {
        String placement = this.preferences.get("useTraitBodyBracePlacement", this.provider.getDefaultAsString("useTraitBodyBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public BracePlacement getOtherBracePlacement() {
        String placement = this.preferences.get("otherBracePlacement", this.provider.getDefaultAsString("otherBracePlacement"));
        return BracePlacement.valueOf(placement);
    }

    public boolean spaceBeforeWhile() {
        return this.preferences.getBoolean("spaceBeforeWhile", this.provider.getDefaultAsBoolean("spaceBeforeWhile"));
    }

    public boolean spaceBeforeElse() {
        return this.preferences.getBoolean("spaceBeforeElse", this.provider.getDefaultAsBoolean("spaceBeforeElse"));
    }

    public boolean spaceBeforeCatch() {
        return this.preferences.getBoolean("spaceBeforeCatch", this.provider.getDefaultAsBoolean("spaceBeforeCatch"));
    }

    public boolean spaceBeforeFinally() {
        return this.preferences.getBoolean("spaceBeforeFinally", this.provider.getDefaultAsBoolean("spaceBeforeFinally"));
    }

    public boolean spaceBeforeMethodDeclParen() {
        return this.preferences.getBoolean("spaceBeforeMethodDeclParen", this.provider.getDefaultAsBoolean("spaceBeforeMethodDeclParen"));
    }

    public boolean spaceBeforeAnonMethodDeclParen() {
        return this.preferences.getBoolean("spaceBeforeAnonMethodDeclParen", this.provider.getDefaultAsBoolean("spaceBeforeAnonMethodDeclParen"));
    }

    public boolean spaceBeforeMethodCallParen() {
        return this.preferences.getBoolean("spaceBeforeMethodCallParen", this.provider.getDefaultAsBoolean("spaceBeforeMethodCallParen"));
    }

    public boolean spaceBeforeIfParen() {
        return this.preferences.getBoolean("spaceBeforeIfParen", this.provider.getDefaultAsBoolean("spaceBeforeIfParen"));
    }

    public boolean spaceBeforeForParen() {
        return this.preferences.getBoolean("spaceBeforeForParen", this.provider.getDefaultAsBoolean("spaceBeforeForParen"));
    }

    public boolean spaceBeforeWhileParen() {
        return this.preferences.getBoolean("spaceBeforeWhileParen", this.provider.getDefaultAsBoolean("spaceBeforeWhileParen"));
    }

    public boolean spaceBeforeCatchParen() {
        return this.preferences.getBoolean("spaceBeforeCatchParen", this.provider.getDefaultAsBoolean("spaceBeforeCatchParen"));
    }

    public boolean spaceBeforeSwitchParen() {
        return this.preferences.getBoolean("spaceBeforeSwitchParen", this.provider.getDefaultAsBoolean("spaceBeforeSwitchParen"));
    }

    public boolean spaceBeforeWithParen() {
        return this.preferences.getBoolean("spaceBeforeWithParen", this.provider.getDefaultAsBoolean("spaceBeforeWithParen"));
    }

    public boolean spaceAroundUnaryOps() {
        return this.preferences.getBoolean("spaceAroundUnaryOps", this.provider.getDefaultAsBoolean("spaceAroundUnaryOps"));
    }

    public boolean spaceAroundBinaryOps() {
        return this.preferences.getBoolean("spaceAroundBinaryOps", this.provider.getDefaultAsBoolean("spaceAroundBinaryOps"));
    }

    public boolean spaceAroundStringConcatOps() {
        return this.preferences.getBoolean("spaceAroundStringConcatOps", this.provider.getDefaultAsBoolean("spaceAroundStringConcatOps"));
    }

    public boolean spaceAroundTernaryOps() {
        return this.preferences.getBoolean("spaceAroundTernaryOps", this.provider.getDefaultAsBoolean("spaceAroundTernaryOps"));
    }

    public boolean spaceAroundKeyValueOps() {
        return this.preferences.getBoolean("spaceAroundKeyValueOps", this.provider.getDefaultAsBoolean("spaceAroundKeyValueOps"));
    }

    public boolean spaceAroundAssignOps() {
        return this.preferences.getBoolean("spaceAroundAssignOps", this.provider.getDefaultAsBoolean("spaceAroundAssignOps"));
    }

    public boolean spaceAroundObjectOps() {
        return this.preferences.getBoolean("spaceAroundObjectOps", this.provider.getDefaultAsBoolean("spaceAroundObjectOps"));
    }

    public boolean spaceBeforeClassDeclLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeClassDeclLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeClassDeclLeftBrace"));
    }

    public boolean spaceBeforeMethodDeclLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeMethodDeclLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeMethodDeclLeftBrace"));
    }

    public boolean spaceBeforeIfLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeIfLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeIfLeftBrace"));
    }

    public boolean spaceBeforeElseLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeElseLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeElseLeftBrace"));
    }

    public boolean spaceBeforeWhileLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeWhileLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeWhileLeftBrace"));
    }

    public boolean spaceBeforeForLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeForLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeForLeftBrace"));
    }

    public boolean spaceBeforeDoLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeDoLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeDoLeftBrace"));
    }

    public boolean spaceBeforeSwitchLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeSwitchLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeSwitchLeftBrace"));
    }

    public boolean spaceBeforeTryLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeTryLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeTryLeftBrace"));
    }

    public boolean spaceBeforeCatchLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeCatchLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeCatchLeftBrace"));
    }

    public boolean spaceBeforeFinallyLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeFinallyLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeFinallyLeftBrace"));
    }

    public boolean spaceBeforeWithLeftBrace() {
        return this.preferences.getBoolean("spaceBeforeWithLeftBrace", this.provider.getDefaultAsBoolean("spaceBeforeWithLeftBrace"));
    }

    public boolean spaceWithinParens() {
        return this.preferences.getBoolean("spaceWithinParens", this.provider.getDefaultAsBoolean("spaceWithinParens"));
    }

    public boolean spaceWithinMethodDeclParens() {
        return this.preferences.getBoolean("spaceWithinMethodDeclParens", this.provider.getDefaultAsBoolean("spaceWithinMethodDeclParens"));
    }

    public boolean spaceWithinMethodCallParens() {
        return this.preferences.getBoolean("spaceWithinMethodCallParens", this.provider.getDefaultAsBoolean("spaceWithinMethodCallParens"));
    }

    public boolean spaceWithinIfParens() {
        return this.preferences.getBoolean("spaceWithinIfParens", this.provider.getDefaultAsBoolean("spaceWithinIfParens"));
    }

    public boolean spaceWithinForParens() {
        return this.preferences.getBoolean("spaceWithinForParens", this.provider.getDefaultAsBoolean("spaceWithinForParens"));
    }

    public boolean spaceWithinWhileParens() {
        return this.preferences.getBoolean("spaceWithinWhileParens", this.provider.getDefaultAsBoolean("spaceWithinWhileParens"));
    }

    public boolean spaceWithinSwitchParens() {
        return this.preferences.getBoolean("spaceWithinSwitchParens", this.provider.getDefaultAsBoolean("spaceWithinSwitchParens"));
    }

    public boolean spaceWithinCatchParens() {
        return this.preferences.getBoolean("spaceWithinCatchParens", this.provider.getDefaultAsBoolean("spaceWithinCatchParens"));
    }

    public boolean spaceWithinWithParens() {
        return this.preferences.getBoolean("spaceWithinWithParens", this.provider.getDefaultAsBoolean("spaceWithinWithParens"));
    }

    public boolean spaceWithinTypeCastParens() {
        return this.preferences.getBoolean("spaceWithinTypeCastParens", this.provider.getDefaultAsBoolean("spaceWithinTypeCastParens"));
    }

    public boolean spaceWithinArrayDeclParens() {
        return this.preferences.getBoolean("spaceWithinArrayDeclParens", this.provider.getDefaultAsBoolean("spaceWithinArrayDeclParens"));
    }

    public boolean spaceWithinBraces() {
        return this.preferences.getBoolean("spaceWithinBraces", this.provider.getDefaultAsBoolean("spaceWithinBraces"));
    }

    public boolean spaceWithinArrayBrackets() {
        return this.preferences.getBoolean("spaceWithinArrayBrackets", this.provider.getDefaultAsBoolean("spaceWithinArrayBrackets"));
    }

    public boolean spaceBeforeComma() {
        return this.preferences.getBoolean("spaceBeforeComma", this.provider.getDefaultAsBoolean("spaceBeforeComma"));
    }

    public boolean spaceAfterComma() {
        return this.preferences.getBoolean("spaceAfterComma", this.provider.getDefaultAsBoolean("spaceAfterComma"));
    }

    public boolean spaceBeforeSemi() {
        return this.preferences.getBoolean("spaceBeforeSemi", this.provider.getDefaultAsBoolean("spaceBeforeSemi"));
    }

    public boolean spaceAfterSemi() {
        return this.preferences.getBoolean("spaceAfterSemi", this.provider.getDefaultAsBoolean("spaceAfterSemi"));
    }

    public boolean spaceBeforeColon() {
        return this.preferences.getBoolean("spaceBeforeColon", this.provider.getDefaultAsBoolean("spaceBeforeColon"));
    }

    public boolean spaceAfterColon() {
        return this.preferences.getBoolean("spaceAfterColon", this.provider.getDefaultAsBoolean("spaceAfterColon"));
    }

    public WrapStyle wrapStatement() {
        String wrap = this.preferences.get("wrapStatement", this.provider.getDefaultAsString("wrapStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapVariables() {
        String wrap = this.preferences.get("wrapVariables", this.provider.getDefaultAsString("wrapVariables"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapMethodParams() {
        String wrap = this.preferences.get("wrapMethodParams", this.provider.getDefaultAsString("wrapMethodParams"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapMethodCallArgs() {
        String wrap = this.preferences.get("wrapMethodCallArgs", this.provider.getDefaultAsString("wrapMethodCallArgs"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapChainedMethodCalls() {
        String wrap = this.preferences.get("wrapChainedMethodCalls", this.provider.getDefaultAsString("wrapChainedMethodCalls"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterDotInChainedMethodCalls() {
        return this.preferences.getBoolean("wrapAfterDotInChainedMethodCalls", this.provider.getDefaultAsBoolean("wrapAfterDotInChainedMethodCalls"));
    }

    public WrapStyle wrapArrayInit() {
        String wrap = this.preferences.get("wrapArrayInit", this.provider.getDefaultAsString("wrapArrayInit"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapArrayInitItems() {
        String wrap = this.preferences.get("wrapArrayInitItems", this.provider.getDefaultAsString("wrapArrayInitItems"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapFor() {
        String wrap = this.preferences.get("wrapFor", this.provider.getDefaultAsString("wrapFor"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapForStatement() {
        String wrap = this.preferences.get("wrapForStatement", this.provider.getDefaultAsString("wrapForStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapIfStatement() {
        String wrap = this.preferences.get("wrapIfStatement", this.provider.getDefaultAsString("wrapIfStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapWhileStatement() {
        String wrap = this.preferences.get("wrapWhileStatement", this.provider.getDefaultAsString("wrapWhileStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapDoWhileStatement() {
        String wrap = this.preferences.get("wrapDoWhileStatement", this.provider.getDefaultAsString("wrapDoWhileStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapWithStatement() {
        String wrap = this.preferences.get("wrapWithStatement", this.provider.getDefaultAsString("wrapWithStatement"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapBinaryOps() {
        String wrap = this.preferences.get("wrapBinaryOps", this.provider.getDefaultAsString("wrapBinaryOps"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterBinaryOps() {
        return this.preferences.getBoolean("wrapAfterBinaryOps", this.provider.getDefaultAsBoolean("wrapAfterBinaryOps"));
    }

    public WrapStyle wrapTernaryOps() {
        String wrap = this.preferences.get("wrapTernaryOps", this.provider.getDefaultAsString("wrapTernaryOps"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapAssignOps() {
        String wrap = this.preferences.get("wrapAssignOps", this.provider.getDefaultAsString("wrapAssignOps"));
        return WrapStyle.valueOf(wrap);
    }

    public boolean wrapAfterTernaryOps() {
        return this.preferences.getBoolean("wrapAfterTernaryOps", this.provider.getDefaultAsBoolean("wrapAfterTernaryOps"));
    }

    public boolean wrapBlockBrace() {
        return this.preferences.getBoolean("wrapBlockBraces", this.provider.getDefaultAsBoolean("wrapBlockBraces"));
    }

    public boolean wrapStatementsOnTheSameLine() {
        return this.preferences.getBoolean("wrapStateMentsOnTheLine", this.provider.getDefaultAsBoolean("wrapStateMentsOnTheLine"));
    }

    public WrapStyle wrapObjects() {
        String wrap = this.preferences.get("wrapObjects", this.provider.getDefaultAsString("wrapObjects"));
        return WrapStyle.valueOf(wrap);
    }

    public WrapStyle wrapProperties() {
        String wrap = this.preferences.get("wrapProperties", this.provider.getDefaultAsString("wrapProperties"));
        return WrapStyle.valueOf(wrap);
    }

    static final class Holder {
        final boolean expandTabsToSpaces;
        final int tabSize;
        final int indentSize;
        final int continuationIndentSize;
        final int itemsInArrayDeclarationIndentSize;
        final int initialIndent;
        final boolean reformatComments;
        final boolean indentHtml;
        final int rightMargin;
        final BracePlacement classDeclBracePlacement;
        final BracePlacement methodDeclBracePlacement;
        final BracePlacement ifBracePlacement;
        final BracePlacement forBracePlacement;
        final BracePlacement whileBracePlacement;
        final BracePlacement switchBracePlacement;
        final BracePlacement catchBracePlacement;
        final BracePlacement useTraitBodyBracePlacement;
        final BracePlacement otherBracePlacement;
        final boolean spaceBeforeWhile;
        final boolean spaceBeforeElse;
        final boolean spaceBeforeCatch;
        final boolean spaceBeforeFinally;
        final boolean spaceBeforeAnonMethodDeclParen;
        final boolean spaceBeforeMethodDeclParen;
        final boolean spaceBeforeMethodCallParen;
        final boolean spaceBeforeIfParen;
        final boolean spaceBeforeForParen;
        final boolean spaceBeforeWhileParen;
        final boolean spaceBeforeCatchParen;
        final boolean spaceBeforeSwitchParen;
        final boolean spaceBeforeWithParen;
        final boolean spaceAroundUnaryOps;
        final boolean spaceAroundBinaryOps;
        final boolean spaceAroundStringConcatOps;
        final boolean spaceAroundTernaryOps;
        final boolean spaceAroundKeyValueOps;
        final boolean spaceAroundAssignOps;
        final boolean spaceAroundObjectOps;
        final boolean spaceBeforeClassDeclLeftBrace;
        final boolean spaceBeforeMethodDeclLeftBrace;
        final boolean spaceBeforeIfLeftBrace;
        final boolean spaceBeforeElseLeftBrace;
        final boolean spaceBeforeWhileLeftBrace;
        final boolean spaceBeforeForLeftBrace;
        final boolean spaceBeforeDoLeftBrace;
        final boolean spaceBeforeSwitchLeftBrace;
        final boolean spaceBeforeTryLeftBrace;
        final boolean spaceBeforeCatchLeftBrace;
        final boolean spaceBeforeFinallyLeftBrace;
        final boolean spaceBeforeWithLeftBrace;
        final boolean spaceWithinParens;
        final boolean spaceWithinMethodDeclParens;
        final boolean spaceWithinMethodCallParens;
        final boolean spaceWithinIfParens;
        final boolean spaceWithinForParens;
        final boolean spaceWithinWhileParens;
        final boolean spaceWithinSwitchParens;
        final boolean spaceWithinCatchParens;
        final boolean spaceWithinWithParens;
        final boolean spaceWithinTypeCastParens;
        final boolean spaceWithinArrayDeclParens;
        final boolean spaceWithinBraces;
        final boolean spaceWithinArrayBrackets;
        final boolean spaceBeforeComma;
        final boolean spaceAfterComma;
        final boolean spaceBeforeSemi;
        final boolean spaceAfterSemi;
        final boolean spaceBeforeColon;
        final boolean spaceAfterColon;
        final WrapStyle wrapStatement;
        final WrapStyle wrapVariables;
        final WrapStyle wrapMethodParams;
        final WrapStyle wrapMethodCallArgs;
        final WrapStyle wrapChainedMethodCalls;
        final boolean wrapAfterDotInChainedMethodCalls;
        final WrapStyle wrapArrayInit;
        final WrapStyle wrapArrayInitItems;
        final WrapStyle wrapFor;
        final WrapStyle wrapForStatement;
        final WrapStyle wrapIfStatement;
        final WrapStyle wrapWhileStatement;
        final WrapStyle wrapDoWhileStatement;
        final WrapStyle wrapWithStatement;
        final WrapStyle wrapBinaryOps;
        final boolean wrapAfterBinaryOps;
        final WrapStyle wrapTernaryOps;
        final WrapStyle wrapAssignOps;
        final boolean wrapAfterTernaryOps;
        final boolean wrapBlockBrace;
        final boolean wrapStatementsOnTheSameLine;
        final WrapStyle wrapObjects;
        final WrapStyle wrapProperties;

        private Holder(CodeStyle style) {
            this.expandTabsToSpaces = style.expandTabToSpaces();
            this.tabSize = style.getTabSize();
            this.indentSize = style.getIndentSize();
            this.continuationIndentSize = style.getContinuationIndentSize();
            this.itemsInArrayDeclarationIndentSize = style.getItemsInArrayDeclarationIndentSize();
            this.initialIndent = style.getInitialIndent();
            this.reformatComments = style.reformatComments();
            this.indentHtml = style.indentHtml();
            this.rightMargin = style.getRightMargin();
            this.classDeclBracePlacement = style.getClassDeclBracePlacement();
            this.methodDeclBracePlacement = style.getMethodDeclBracePlacement();
            this.ifBracePlacement = style.getIfBracePlacement();
            this.forBracePlacement = style.getForBracePlacement();
            this.whileBracePlacement = style.getWhileBracePlacement();
            this.switchBracePlacement = style.getSwitchBracePlacement();
            this.catchBracePlacement = style.getCatchBracePlacement();
            this.useTraitBodyBracePlacement = style.getUseTraitBodyBracePlacement();
            this.otherBracePlacement = style.getOtherBracePlacement();
            this.spaceBeforeWhile = style.spaceBeforeWhile();
            this.spaceBeforeElse = style.spaceBeforeElse();
            this.spaceBeforeCatch = style.spaceBeforeCatch();
            this.spaceBeforeFinally = style.spaceBeforeFinally();
            this.spaceBeforeAnonMethodDeclParen = style.spaceBeforeAnonMethodDeclParen();
            this.spaceBeforeMethodDeclParen = style.spaceBeforeMethodDeclParen();
            this.spaceBeforeMethodCallParen = style.spaceBeforeMethodCallParen();
            this.spaceBeforeIfParen = style.spaceBeforeIfParen();
            this.spaceBeforeForParen = style.spaceBeforeForParen();
            this.spaceBeforeWhileParen = style.spaceBeforeWhileParen();
            this.spaceBeforeCatchParen = style.spaceBeforeCatchParen();
            this.spaceBeforeSwitchParen = style.spaceBeforeSwitchParen();
            this.spaceBeforeWithParen = style.spaceBeforeWithParen();
            this.spaceAroundUnaryOps = style.spaceAroundUnaryOps();
            this.spaceAroundBinaryOps = style.spaceAroundBinaryOps();
            this.spaceAroundStringConcatOps = style.spaceAroundStringConcatOps();
            this.spaceAroundTernaryOps = style.spaceAroundTernaryOps();
            this.spaceAroundKeyValueOps = style.spaceAroundKeyValueOps();
            this.spaceAroundAssignOps = style.spaceAroundAssignOps();
            this.spaceAroundObjectOps = style.spaceAroundObjectOps();
            this.spaceBeforeClassDeclLeftBrace = style.spaceBeforeClassDeclLeftBrace();
            this.spaceBeforeMethodDeclLeftBrace = style.spaceBeforeMethodDeclLeftBrace();
            this.spaceBeforeIfLeftBrace = style.spaceBeforeIfLeftBrace();
            this.spaceBeforeElseLeftBrace = style.spaceBeforeElseLeftBrace();
            this.spaceBeforeWhileLeftBrace = style.spaceBeforeWhileLeftBrace();
            this.spaceBeforeForLeftBrace = style.spaceBeforeForLeftBrace();
            this.spaceBeforeDoLeftBrace = style.spaceBeforeDoLeftBrace();
            this.spaceBeforeSwitchLeftBrace = style.spaceBeforeSwitchLeftBrace();
            this.spaceBeforeTryLeftBrace = style.spaceBeforeTryLeftBrace();
            this.spaceBeforeCatchLeftBrace = style.spaceBeforeCatchLeftBrace();
            this.spaceBeforeFinallyLeftBrace = style.spaceBeforeFinallyLeftBrace();
            this.spaceBeforeWithLeftBrace = style.spaceBeforeWithLeftBrace();
            this.spaceWithinParens = style.spaceWithinParens();
            this.spaceWithinMethodDeclParens = style.spaceWithinMethodDeclParens();
            this.spaceWithinMethodCallParens = style.spaceWithinMethodCallParens();
            this.spaceWithinIfParens = style.spaceWithinIfParens();
            this.spaceWithinForParens = style.spaceWithinForParens();
            this.spaceWithinWhileParens = style.spaceWithinWhileParens();
            this.spaceWithinSwitchParens = style.spaceWithinSwitchParens();
            this.spaceWithinCatchParens = style.spaceWithinCatchParens();
            this.spaceWithinWithParens = style.spaceWithinWithParens();
            this.spaceWithinTypeCastParens = style.spaceWithinTypeCastParens();
            this.spaceWithinArrayDeclParens = style.spaceWithinArrayDeclParens();
            this.spaceWithinBraces = style.spaceWithinBraces();
            this.spaceWithinArrayBrackets = style.spaceWithinArrayBrackets();
            this.spaceBeforeComma = style.spaceBeforeComma();
            this.spaceAfterComma = style.spaceAfterComma();
            this.spaceBeforeSemi = style.spaceBeforeSemi();
            this.spaceAfterSemi = style.spaceAfterSemi();
            this.spaceBeforeColon = style.spaceBeforeColon();
            this.spaceAfterColon = style.spaceAfterColon();
            this.wrapStatement = style.wrapStatement();
            this.wrapVariables = style.wrapVariables();
            this.wrapMethodParams = style.wrapMethodParams();
            this.wrapMethodCallArgs = style.wrapMethodCallArgs();
            this.wrapChainedMethodCalls = style.wrapChainedMethodCalls();
            this.wrapAfterDotInChainedMethodCalls = style.wrapAfterDotInChainedMethodCalls();
            this.wrapArrayInit = style.wrapArrayInit();
            this.wrapArrayInitItems = style.wrapArrayInitItems();
            this.wrapFor = style.wrapFor();
            this.wrapForStatement = style.wrapForStatement();
            this.wrapIfStatement = style.wrapIfStatement();
            this.wrapWhileStatement = style.wrapWhileStatement();
            this.wrapDoWhileStatement = style.wrapDoWhileStatement();
            this.wrapWithStatement = style.wrapWithStatement();
            this.wrapBinaryOps = style.wrapBinaryOps();
            this.wrapAfterBinaryOps = style.wrapAfterBinaryOps();
            this.wrapTernaryOps = style.wrapTernaryOps();
            this.wrapAssignOps = style.wrapAssignOps();
            this.wrapAfterTernaryOps = style.wrapAfterTernaryOps();
            this.wrapBlockBrace = style.wrapBlockBrace();
            this.wrapStatementsOnTheSameLine = style.wrapStatementsOnTheSameLine();
            this.wrapObjects = style.wrapObjects();
            this.wrapProperties = style.wrapProperties();
        }
    }

    public static enum WrapStyle {
        WRAP_ALWAYS,
        WRAP_IF_LONG,
        WRAP_NEVER;
        

        private WrapStyle() {
        }
    }

    public static enum BracePlacement {
        SAME_LINE,
        NEW_LINE,
        NEW_LINE_INDENTED,
        PRESERVE_EXISTING;
        

        private BracePlacement() {
        }
    }

}

