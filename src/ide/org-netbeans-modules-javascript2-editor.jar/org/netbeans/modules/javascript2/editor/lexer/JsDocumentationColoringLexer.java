/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.modules.javascript2.editor.lexer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;

@SuppressWarnings(value={"SF_SWITCH_FALLTHROUGH", "URF_UNREAD_FIELD", "DLS_DEAD_LOCAL_STORE", "DM_DEFAULT_ENCODING"})
public final class JsDocumentationColoringLexer {
    public static final int YYEOF = -1;
    private static final int ZZ_BUFFERSIZE = 16384;
    public static final int AT = 6;
    public static final int STRING = 8;
    public static final int YYINITIAL = 0;
    public static final int STRINGEND = 10;
    public static final int DOCBLOCK_START = 4;
    public static final int DOCBLOCK = 2;
    private static final int[] ZZ_LEXSTATE = new int[]{0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5};
    private static final String ZZ_CMAP_PACKED = "\t\u0000\u0001\u000b\u0001\u0001\u0002\u000b\u0001\b\u0012\u0000\u0001\u000b\u0001\u0000\u0001\t\u0007\u0000\u0001\f\u0001\u0000\u0001\u000e\u0002\u0000\u0001\u0003\n\u0007\u0002\u0000\u0001\u0002\u0001\u0000\u0001\u0006\u0001\u0000\u0001\r\u001a\u0004\u0001\u0011\u0001\n\u0001\u0012\u0003\u0000\u001a\u0004\u0001\u000f\u0001\u0005\u0001\u0010\"\u0000\u0001\u000b\t\u0000\u0001\u0004\n\u0000\u0001\u0004\u0004\u0000\u0001\u0004\u0005\u0000\u0017\u0004\u0001\u0000\u001f\u0004\u0001\u0000\u013f\u0004\u0019\u0000r\u0004\u0004\u0000\f\u0004\u000e\u0000\u0005\u0004\t\u0000\u0001\u0004\u008b\u0000\u0001\u0004\u000b\u0000\u0001\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0014\u0004\u0001\u0000,\u0004\u0001\u0000&\u0004\u0001\u0000\u0005\u0004\u0004\u0000\u0082\u0004\b\u0000E\u0004\u0001\u0000&\u0004\u0002\u0000\u0002\u0004\u0006\u0000\u0010\u0004!\u0000&\u0004\u0002\u0000\u0001\u0004\u0007\u0000'\u0004H\u0000\u001b\u0004\u0005\u0000\u0003\u0004.\u0000\u001a\u0004\u0005\u0000\u000b\u0004\u0015\u0000\n\u0007\u0004\u0000\u0002\u0004\u0001\u0000c\u0004\u0001\u0000\u0001\u0004\u000f\u0000\u0002\u0004\u0007\u0000\u0002\u0004\n\u0007\u0003\u0004\u0002\u0000\u0001\u0004\u0010\u0000\u0001\u0004\u0001\u0000\u001e\u0004\u001d\u0000\u0003\u00040\u0000&\u0004\u000b\u0000\u0001\u0004\u0152\u00006\u0004\u0003\u0000\u0001\u0004\u0012\u0000\u0001\u0004\u0007\u0000\n\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0002\u0000\u0002\u0004\u0002\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0003\u0000\u0004\u0004\u0003\u0000\u0001\u0004\u001e\u0000\u0002\u0004\u0001\u0000\u0003\u0004\u0004\u0000\n\u0007\u0002\u0004\u0013\u0000\u0006\u0004\u0004\u0000\u0002\u0004\u0002\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0002\u0004\u001f\u0000\u0004\u0004\u0001\u0000\u0001\u0004\u0007\u0000\n\u0007\u0002\u0000\u0003\u0004\u0010\u0000\t\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0005\u0004\u0003\u0000\u0001\u0004\u0012\u0000\u0001\u0004\u000f\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0002\u0000\u0002\u0004\u0002\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0005\u0004\u0003\u0000\u0001\u0004\u001e\u0000\u0002\u0004\u0001\u0000\u0003\u0004\u0004\u0000\n\u0007\u0001\u0000\u0001\u0004\u0011\u0000\u0001\u0004\u0001\u0000\u0006\u0004\u0003\u0000\u0003\u0004\u0001\u0000\u0004\u0004\u0003\u0000\u0002\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0002\u0004\u0003\u0000\u0002\u0004\u0003\u0000\u0003\u0004\u0003\u0000\b\u0004\u0001\u0000\u0003\u0004-\u0000\t\u0007\u0015\u0000\b\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0017\u0004\u0001\u0000\n\u0004\u0001\u0000\u0005\u0004&\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0017\u0004\u0001\u0000\n\u0004\u0001\u0000\u0005\u0004\u0003\u0000\u0001\u0004 \u0000\u0001\u0004\u0001\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0017\u0004\u0001\u0000\u0010\u0004&\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\u0012\u0004\u0003\u0000\u0018\u0004\u0001\u0000\t\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0007\u0004:\u00000\u0004\u0001\u0000\u0002\u0004\f\u0000\u0007\u0004\t\u0000\n\u0007'\u0000\u0002\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0002\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0001\u0004\u0006\u0000\u0004\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0002\u0004\u0001\u0000\u0004\u0004\u0001\u0000\u0002\u0004\t\u0000\u0001\u0004\u0002\u0000\u0005\u0004\u0001\u0000\u0001\u0004\t\u0000\n\u0007\u0002\u0000\u0002\u0004\"\u0000\u0001\u0004\u001f\u0000\n\u0007\u0016\u0000\b\u0004\u0001\u0000\"\u0004\u001d\u0000\u0004\u0004t\u0000\"\u0004\u0001\u0000\u0005\u0004\u0001\u0000\u0002\u0004\u0015\u0000\n\u0007\u0006\u0000\u0006\u0004J\u0000&\u0004\n\u0000)\u0004\u0007\u0000Z\u0004\u0005\u0000D\u0004\u0005\u0000R\u0004\u0006\u0000\u0007\u0004\u0001\u0000?\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000'\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u001f\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0017\u0004\u0001\u0000\u001f\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000'\u0004\u0001\u0000\u0013\u0004\u000e\u0000\t\u0007.\u0000U\u0004\f\u0000\u026c\u0004\u0002\u0000\b\u0004\n\u0000\u001a\u0004\u0005\u0000K\u0004\u0015\u0000\r\u0004\u0001\u0000\u0004\u0004\u000e\u0000\u0012\u0004\u000e\u0000\u0012\u0004\u000e\u0000\r\u0004\u0001\u0000\u0003\u0004\u000f\u00004\u0004#\u0000\u0001\u0004\u0004\u0000\u0001\u0004\u0003\u0000\n\u0007&\u0000\n\u0007\u0006\u0000X\u0004\b\u0000)\u0004W\u0000\u001d\u0004)\u0000\n\u0007\u001e\u0004\u0002\u0000\u0005\u0004\u038b\u0000l\u0004\u0094\u0000\u009c\u0004\u0004\u0000Z\u0004\u0006\u0000\u0016\u0004\u0002\u0000\u0006\u0004\u0002\u0000&\u0004\u0002\u0000\u0006\u0004\u0002\u0000\b\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u001f\u0004\u0002\u00005\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0003\u0000\u0003\u0004\u0001\u0000\u0007\u0004\u0003\u0000\u0004\u0004\u0002\u0000\u0006\u0004\u0004\u0000\r\u0004\u0005\u0000\u0003\u0004\u0001\u0000\u0007\u0004t\u0000\u0001\u0004\r\u0000\u0001\u0004\u0082\u0000\u0001\u0004\u0004\u0000\u0001\u0004\u0002\u0000\n\u0004\u0001\u0000\u0001\u0004\u0003\u0000\u0005\u0004\u0006\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0007\u0004\u0003\u0000\u0003\u0004\u0005\u0000\u0005\u0004\u0ebb\u0000\u0002\u0004*\u0000\u0005\u0004\u0005\u0000\u0002\u0004\u0004\u0000V\u0004\u0006\u0000\u0003\u0004\u0001\u0000Z\u0004\u0001\u0000\u0004\u0004\u0005\u0000(\u0004\u0004\u0000^\u0004\u0011\u0000\u0018\u00048\u0000\u0010\u0004\u0200\u0000\u19b6\u0004J\u0000\u51a6\u0004Z\u0000\u048d\u0004\u0773\u0000\u2ba4\u0004\u215c\u0000\u012e\u0004\u0002\u0000;\u0004\u0095\u0000\u0007\u0004\f\u0000\u0005\u0004\u0005\u0000\u0001\u0004\u0001\u0000\n\u0004\u0001\u0000\r\u0004\u0001\u0000\u0005\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0002\u0004\u0001\u0000l\u0004!\u0000\u016b\u0004\u0012\u0000@\u0004\u0002\u00006\u0004(\u0000\f\u0004t\u0000\u0005\u0004\u0001\u0000\u0087\u0004\u0013\u0000\n\u0007\u0007\u0000\u001a\u0004\u0006\u0000\u001a\u0004\u000b\u0000Y\u0004\u0003\u0000\u0006\u0004\u0002\u0000\u0006\u0004\u0002\u0000\u0006\u0004\u0002\u0000\u0003\u0004#\u0000";
    private static final char[] ZZ_CMAP = JsDocumentationColoringLexer.zzUnpackCMap("\t\u0000\u0001\u000b\u0001\u0001\u0002\u000b\u0001\b\u0012\u0000\u0001\u000b\u0001\u0000\u0001\t\u0007\u0000\u0001\f\u0001\u0000\u0001\u000e\u0002\u0000\u0001\u0003\n\u0007\u0002\u0000\u0001\u0002\u0001\u0000\u0001\u0006\u0001\u0000\u0001\r\u001a\u0004\u0001\u0011\u0001\n\u0001\u0012\u0003\u0000\u001a\u0004\u0001\u000f\u0001\u0005\u0001\u0010\"\u0000\u0001\u000b\t\u0000\u0001\u0004\n\u0000\u0001\u0004\u0004\u0000\u0001\u0004\u0005\u0000\u0017\u0004\u0001\u0000\u001f\u0004\u0001\u0000\u013f\u0004\u0019\u0000r\u0004\u0004\u0000\f\u0004\u000e\u0000\u0005\u0004\t\u0000\u0001\u0004\u008b\u0000\u0001\u0004\u000b\u0000\u0001\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0014\u0004\u0001\u0000,\u0004\u0001\u0000&\u0004\u0001\u0000\u0005\u0004\u0004\u0000\u0082\u0004\b\u0000E\u0004\u0001\u0000&\u0004\u0002\u0000\u0002\u0004\u0006\u0000\u0010\u0004!\u0000&\u0004\u0002\u0000\u0001\u0004\u0007\u0000'\u0004H\u0000\u001b\u0004\u0005\u0000\u0003\u0004.\u0000\u001a\u0004\u0005\u0000\u000b\u0004\u0015\u0000\n\u0007\u0004\u0000\u0002\u0004\u0001\u0000c\u0004\u0001\u0000\u0001\u0004\u000f\u0000\u0002\u0004\u0007\u0000\u0002\u0004\n\u0007\u0003\u0004\u0002\u0000\u0001\u0004\u0010\u0000\u0001\u0004\u0001\u0000\u001e\u0004\u001d\u0000\u0003\u00040\u0000&\u0004\u000b\u0000\u0001\u0004\u0152\u00006\u0004\u0003\u0000\u0001\u0004\u0012\u0000\u0001\u0004\u0007\u0000\n\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0002\u0000\u0002\u0004\u0002\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0003\u0000\u0004\u0004\u0003\u0000\u0001\u0004\u001e\u0000\u0002\u0004\u0001\u0000\u0003\u0004\u0004\u0000\n\u0007\u0002\u0004\u0013\u0000\u0006\u0004\u0004\u0000\u0002\u0004\u0002\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0002\u0004\u001f\u0000\u0004\u0004\u0001\u0000\u0001\u0004\u0007\u0000\n\u0007\u0002\u0000\u0003\u0004\u0010\u0000\t\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0005\u0004\u0003\u0000\u0001\u0004\u0012\u0000\u0001\u0004\u000f\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0002\u0000\u0002\u0004\u0002\u0000\u0016\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0005\u0004\u0003\u0000\u0001\u0004\u001e\u0000\u0002\u0004\u0001\u0000\u0003\u0004\u0004\u0000\n\u0007\u0001\u0000\u0001\u0004\u0011\u0000\u0001\u0004\u0001\u0000\u0006\u0004\u0003\u0000\u0003\u0004\u0001\u0000\u0004\u0004\u0003\u0000\u0002\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0002\u0004\u0003\u0000\u0002\u0004\u0003\u0000\u0003\u0004\u0003\u0000\b\u0004\u0001\u0000\u0003\u0004-\u0000\t\u0007\u0015\u0000\b\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0017\u0004\u0001\u0000\n\u0004\u0001\u0000\u0005\u0004&\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0017\u0004\u0001\u0000\n\u0004\u0001\u0000\u0005\u0004\u0003\u0000\u0001\u0004 \u0000\u0001\u0004\u0001\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\b\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0017\u0004\u0001\u0000\u0010\u0004&\u0000\u0002\u0004\u0004\u0000\n\u0007\u0015\u0000\u0012\u0004\u0003\u0000\u0018\u0004\u0001\u0000\t\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0007\u0004:\u00000\u0004\u0001\u0000\u0002\u0004\f\u0000\u0007\u0004\t\u0000\n\u0007'\u0000\u0002\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0002\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0001\u0004\u0006\u0000\u0004\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0002\u0000\u0002\u0004\u0001\u0000\u0004\u0004\u0001\u0000\u0002\u0004\t\u0000\u0001\u0004\u0002\u0000\u0005\u0004\u0001\u0000\u0001\u0004\t\u0000\n\u0007\u0002\u0000\u0002\u0004\"\u0000\u0001\u0004\u001f\u0000\n\u0007\u0016\u0000\b\u0004\u0001\u0000\"\u0004\u001d\u0000\u0004\u0004t\u0000\"\u0004\u0001\u0000\u0005\u0004\u0001\u0000\u0002\u0004\u0015\u0000\n\u0007\u0006\u0000\u0006\u0004J\u0000&\u0004\n\u0000)\u0004\u0007\u0000Z\u0004\u0005\u0000D\u0004\u0005\u0000R\u0004\u0006\u0000\u0007\u0004\u0001\u0000?\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000'\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u001f\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0017\u0004\u0001\u0000\u001f\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0002\u0000\u0007\u0004\u0001\u0000'\u0004\u0001\u0000\u0013\u0004\u000e\u0000\t\u0007.\u0000U\u0004\f\u0000\u026c\u0004\u0002\u0000\b\u0004\n\u0000\u001a\u0004\u0005\u0000K\u0004\u0015\u0000\r\u0004\u0001\u0000\u0004\u0004\u000e\u0000\u0012\u0004\u000e\u0000\u0012\u0004\u000e\u0000\r\u0004\u0001\u0000\u0003\u0004\u000f\u00004\u0004#\u0000\u0001\u0004\u0004\u0000\u0001\u0004\u0003\u0000\n\u0007&\u0000\n\u0007\u0006\u0000X\u0004\b\u0000)\u0004W\u0000\u001d\u0004)\u0000\n\u0007\u001e\u0004\u0002\u0000\u0005\u0004\u038b\u0000l\u0004\u0094\u0000\u009c\u0004\u0004\u0000Z\u0004\u0006\u0000\u0016\u0004\u0002\u0000\u0006\u0004\u0002\u0000&\u0004\u0002\u0000\u0006\u0004\u0002\u0000\b\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u001f\u0004\u0002\u00005\u0004\u0001\u0000\u0007\u0004\u0001\u0000\u0001\u0004\u0003\u0000\u0003\u0004\u0001\u0000\u0007\u0004\u0003\u0000\u0004\u0004\u0002\u0000\u0006\u0004\u0004\u0000\r\u0004\u0005\u0000\u0003\u0004\u0001\u0000\u0007\u0004t\u0000\u0001\u0004\r\u0000\u0001\u0004\u0082\u0000\u0001\u0004\u0004\u0000\u0001\u0004\u0002\u0000\n\u0004\u0001\u0000\u0001\u0004\u0003\u0000\u0005\u0004\u0006\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0004\u0004\u0001\u0000\u0003\u0004\u0001\u0000\u0007\u0004\u0003\u0000\u0003\u0004\u0005\u0000\u0005\u0004\u0ebb\u0000\u0002\u0004*\u0000\u0005\u0004\u0005\u0000\u0002\u0004\u0004\u0000V\u0004\u0006\u0000\u0003\u0004\u0001\u0000Z\u0004\u0001\u0000\u0004\u0004\u0005\u0000(\u0004\u0004\u0000^\u0004\u0011\u0000\u0018\u00048\u0000\u0010\u0004\u0200\u0000\u19b6\u0004J\u0000\u51a6\u0004Z\u0000\u048d\u0004\u0773\u0000\u2ba4\u0004\u215c\u0000\u012e\u0004\u0002\u0000;\u0004\u0095\u0000\u0007\u0004\f\u0000\u0005\u0004\u0005\u0000\u0001\u0004\u0001\u0000\n\u0004\u0001\u0000\r\u0004\u0001\u0000\u0005\u0004\u0001\u0000\u0001\u0004\u0001\u0000\u0002\u0004\u0001\u0000\u0002\u0004\u0001\u0000l\u0004!\u0000\u016b\u0004\u0012\u0000@\u0004\u0002\u00006\u0004(\u0000\f\u0004t\u0000\u0005\u0004\u0001\u0000\u0087\u0004\u0013\u0000\n\u0007\u0007\u0000\u001a\u0004\u0006\u0000\u001a\u0004\u000b\u0000Y\u0004\u0003\u0000\u0006\u0004\u0002\u0000\u0006\u0004\u0002\u0000\u0006\u0004\u0002\u0000\u0003\u0004#\u0000");
    private static final int[] ZZ_ACTION = JsDocumentationColoringLexer.zzUnpackAction();
    private static final String ZZ_ACTION_PACKED_0 = "\u0006\u0000\u0003\u0001\u0001\u0000\u0001\u0002\u0001\u0003\u0001\u0002\u0001\u0004\u0001\u0005\u0001\u0006\u0001\u0007\u0001\b\u0001\t\u0001\n\u0001\u000b\u0001\f\u0001\r\u0001\u000e\u0002\u000f\u0001\u0001\u0002\u0010\u0001\u0011\u0001\u0000\u0001\u0012\u0001\u0013\u0001\u0014\u0001\u0000\u0003\u0015\u0001\u0000\u0001\u0016\u0001\u0000\u0001\u0001\u0001\u0017";
    private static final int[] ZZ_ROWMAP = JsDocumentationColoringLexer.zzUnpackRowMap();
    private static final String ZZ_ROWMAP_PACKED_0 = "\u0000\u0000\u0000\u0013\u0000&\u00009\u0000L\u0000_\u0000r\u0000\u0085\u0000\u0098\u0000\u00ab\u0000r\u0000\u00be\u0000\u00d1\u0000r\u0000\u00e4\u0000\u0098\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000\u00f7\u0000\u010a\u0000r\u0000\u011d\u0000r\u0000\u0130\u0000r\u0000r\u0000r\u0000\u0098\u0000r\u0000\u0143\u0000\u0156\u0000\u0169\u0000\u00f7\u0000\u017c\u0000\u018f\u0000r";
    private static final int[] ZZ_TRANS = JsDocumentationColoringLexer.zzUnpackTrans();
    private static final String ZZ_TRANS_PACKED_0 = "\u0003\u0007\u0001\b\b\u0007\u0001\t\u0006\u0007\u0001\n\u0001\u000b\u0001\f\u0005\n\u0001\r\u0001\u000e\u0001\n\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0001\u0013\u0001\u0014\u0001\u0015\u0001\u0016\f\u0017\u0001\u0018\u0006\u0017\r\u0019\u0001\u001a\u0005\u0019\u0001\u001b\u0001\u001c\u0006\u001b\u0001\u001d\u0001\u001e\u0001\u001f\b\u001b\t\u0000\u0001 (\u0000\u0001!\t\u0000\u0001\"\b\u0000\u0001#\u0006\u0000\u0001\n\u0002$\u0005\n\u0001%\u0001$\u0001\n\u0001&\u0007$\u0003\u0000\u0002'\u000f\u0000\u0001\u000b\u001c\u0000\u0001\u000f\u000b\u0000\u0001(\u0002\u0000\u0001(\u000b\u0000\u0001\u001b\u0001\u0000\u0006\u001b\u0002\u0000\u0001)\b\u001b\u0001\u0000\u0001\u001c\u0011\u0000\u0001\u0007\u0001\u001b\u0006\u0007\u0001*\n\u0007\u0001\u0000\u0001$\u001c\u0000\u0001&\u0007\u0000\u0001'\u0002\u0000\u0002'\u0001\u0000\u0001+\u0001'\u0001\u0000\u0003'\u0001\u0000\u0006'\u0001\u0000\u0001\u001b\u0006\u0000\u0001*\n\u0000\b\u001b\u0002\u0000\u0001)\b\u001b";
    private static final int ZZ_UNKNOWN_ERROR = 0;
    private static final int ZZ_NO_MATCH = 1;
    private static final int ZZ_PUSHBACK_2BIG = 2;
    private static final String[] ZZ_ERROR_MSG = new String[]{"Unkown internal scanner error", "Error: could not match input", "Error: pushback value was too large"};
    private static final int[] ZZ_ATTRIBUTE = JsDocumentationColoringLexer.zzUnpackAttribute();
    private static final String ZZ_ATTRIBUTE_PACKED_0 = "\u0006\u0000\u0001\t\u0002\u0001\u0001\u0000\u0001\t\u0002\u0001\u0001\t\u0002\u0001\t\t\u0002\u0001\u0001\t\u0001\u0001\u0001\t\u0001\u0000\u0003\t\u0001\u0000\u0001\t\u0002\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0001\t";
    private Reader zzReader;
    private int zzState;
    private int zzLexicalState = 0;
    private char[] zzBuffer = new char[16384];
    private int zzMarkedPos;
    private int zzCurrentPos;
    private int zzStartRead;
    private int zzEndRead;
    private int yyline;
    private int yychar;
    private int yycolumn;
    private boolean zzAtBOL = true;
    private boolean zzAtEOF;
    private boolean zzEOFDone;
    private LexerInput input;

    private static int[] zzUnpackAction() {
        int[] result = new int[43];
        int offset = 0;
        offset = JsDocumentationColoringLexer.zzUnpackAction("\u0006\u0000\u0003\u0001\u0001\u0000\u0001\u0002\u0001\u0003\u0001\u0002\u0001\u0004\u0001\u0005\u0001\u0006\u0001\u0007\u0001\b\u0001\t\u0001\n\u0001\u000b\u0001\f\u0001\r\u0001\u000e\u0002\u000f\u0001\u0001\u0002\u0010\u0001\u0011\u0001\u0000\u0001\u0012\u0001\u0013\u0001\u0014\u0001\u0000\u0003\u0015\u0001\u0000\u0001\u0016\u0001\u0000\u0001\u0001\u0001\u0017", offset, result);
        return result;
    }

    private static int zzUnpackAction(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do {
                result[j++] = value;
            } while (--count > 0);
        }
        return j;
    }

    private static int[] zzUnpackRowMap() {
        int[] result = new int[43];
        int offset = 0;
        offset = JsDocumentationColoringLexer.zzUnpackRowMap("\u0000\u0000\u0000\u0013\u0000&\u00009\u0000L\u0000_\u0000r\u0000\u0085\u0000\u0098\u0000\u00ab\u0000r\u0000\u00be\u0000\u00d1\u0000r\u0000\u00e4\u0000\u0098\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000r\u0000\u00f7\u0000\u010a\u0000r\u0000\u011d\u0000r\u0000\u0130\u0000r\u0000r\u0000r\u0000\u0098\u0000r\u0000\u0143\u0000\u0156\u0000\u0169\u0000\u00f7\u0000\u017c\u0000\u018f\u0000r", offset, result);
        return result;
    }

    private static int zzUnpackRowMap(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int high = packed.charAt(i++) << 16;
            result[j++] = high | packed.charAt(i++);
        }
        return j;
    }

    private static int[] zzUnpackTrans() {
        int[] result = new int[418];
        int offset = 0;
        offset = JsDocumentationColoringLexer.zzUnpackTrans("\u0003\u0007\u0001\b\b\u0007\u0001\t\u0006\u0007\u0001\n\u0001\u000b\u0001\f\u0005\n\u0001\r\u0001\u000e\u0001\n\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0001\u0013\u0001\u0014\u0001\u0015\u0001\u0016\f\u0017\u0001\u0018\u0006\u0017\r\u0019\u0001\u001a\u0005\u0019\u0001\u001b\u0001\u001c\u0006\u001b\u0001\u001d\u0001\u001e\u0001\u001f\b\u001b\t\u0000\u0001 (\u0000\u0001!\t\u0000\u0001\"\b\u0000\u0001#\u0006\u0000\u0001\n\u0002$\u0005\n\u0001%\u0001$\u0001\n\u0001&\u0007$\u0003\u0000\u0002'\u000f\u0000\u0001\u000b\u001c\u0000\u0001\u000f\u000b\u0000\u0001(\u0002\u0000\u0001(\u000b\u0000\u0001\u001b\u0001\u0000\u0006\u001b\u0002\u0000\u0001)\b\u001b\u0001\u0000\u0001\u001c\u0011\u0000\u0001\u0007\u0001\u001b\u0006\u0007\u0001*\n\u0007\u0001\u0000\u0001$\u001c\u0000\u0001&\u0007\u0000\u0001'\u0002\u0000\u0002'\u0001\u0000\u0001+\u0001'\u0001\u0000\u0003'\u0001\u0000\u0006'\u0001\u0000\u0001\u001b\u0006\u0000\u0001*\n\u0000\b\u001b\u0002\u0000\u0001)\b\u001b", offset, result);
        return result;
    }

    private static int zzUnpackTrans(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            int value = packed.charAt(i++);
            do {
                result[j++] = --value;
            } while (--count > 0);
        }
        return j;
    }

    private static int[] zzUnpackAttribute() {
        int[] result = new int[43];
        int offset = 0;
        offset = JsDocumentationColoringLexer.zzUnpackAttribute("\u0006\u0000\u0001\t\u0002\u0001\u0001\u0000\u0001\t\u0002\u0001\u0001\t\u0002\u0001\t\t\u0002\u0001\u0001\t\u0001\u0001\u0001\t\u0001\u0000\u0003\t\u0001\u0000\u0001\t\u0002\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0001\t", offset, result);
        return result;
    }

    private static int zzUnpackAttribute(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do {
                result[j++] = value;
            } while (--count > 0);
        }
        return j;
    }

    public JsDocumentationColoringLexer(LexerRestartInfo info) {
        this.input = info.input();
        if (info.state() != null) {
            this.setState((LexerState)info.state());
        } else {
            this.zzLexicalState = 0;
            this.zzState = 0;
        }
    }

    public LexerState getState() {
        if (this.zzState == 0 && this.zzLexicalState == 0) {
            return null;
        }
        return new LexerState(this.zzState, this.zzLexicalState);
    }

    public void setState(LexerState state) {
        this.zzState = state.zzState;
        this.zzLexicalState = state.zzLexicalState;
    }

    public JsDocumentationTokenId nextToken() throws IOException {
        JsDocumentationTokenId token = this.yylex();
        return token;
    }

    public JsDocumentationColoringLexer(Reader in) {
        this.zzReader = in;
    }

    public JsDocumentationColoringLexer(InputStream in) {
        this(new InputStreamReader(in));
    }

    private static char[] zzUnpackCMap(String packed) {
        char[] map = new char[65536];
        int i = 0;
        int j = 0;
        while (i < 1236) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do {
                map[j++] = value;
            } while (--count > 0);
        }
        return map;
    }

    private boolean zzRefill() throws IOException {
        int numRead;
        if (this.zzStartRead > 0) {
            System.arraycopy(this.zzBuffer, this.zzStartRead, this.zzBuffer, 0, this.zzEndRead - this.zzStartRead);
            this.zzEndRead -= this.zzStartRead;
            this.zzCurrentPos -= this.zzStartRead;
            this.zzMarkedPos -= this.zzStartRead;
            this.zzStartRead = 0;
        }
        if (this.zzCurrentPos >= this.zzBuffer.length) {
            char[] newBuffer = new char[this.zzCurrentPos * 2];
            System.arraycopy(this.zzBuffer, 0, newBuffer, 0, this.zzBuffer.length);
            this.zzBuffer = newBuffer;
        }
        if ((numRead = this.zzReader.read(this.zzBuffer, this.zzEndRead, this.zzBuffer.length - this.zzEndRead)) > 0) {
            this.zzEndRead += numRead;
            return false;
        }
        if (numRead == 0) {
            int c = this.zzReader.read();
            if (c == -1) {
                return true;
            }
            this.zzBuffer[this.zzEndRead++] = (char)c;
            return false;
        }
        return true;
    }

    public final void yyclose() throws IOException {
        this.zzAtEOF = true;
        this.zzEndRead = this.zzStartRead;
        if (this.zzReader != null) {
            this.zzReader.close();
        }
    }

    public final void yyreset(Reader reader) {
        this.zzReader = reader;
        this.zzAtBOL = true;
        this.zzAtEOF = false;
        this.zzEOFDone = false;
        this.zzStartRead = 0;
        this.zzEndRead = 0;
        this.zzMarkedPos = 0;
        this.zzCurrentPos = 0;
        this.yycolumn = 0;
        this.yychar = 0;
        this.yyline = 0;
        this.zzLexicalState = 0;
    }

    public final int yystate() {
        return this.zzLexicalState;
    }

    public final void yybegin(int newState) {
        this.zzLexicalState = newState;
    }

    public final String yytext() {
        return this.input.readText().toString();
    }

    public final char yycharat(int pos) {
        return this.input.readText().charAt(pos);
    }

    public final int yylength() {
        return this.input.readLength();
    }

    private void zzScanError(int errorCode) {
        String message;
        try {
            message = ZZ_ERROR_MSG[errorCode];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            message = ZZ_ERROR_MSG[0];
        }
        throw new Error(message);
    }

    public void yypushback(int number) {
        if (number > this.yylength()) {
            this.zzScanError(2);
        }
        this.input.backup(number);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public JsDocumentationTokenId yylex() throws IOException {
        zzCMapL = JsDocumentationColoringLexer.ZZ_CMAP;
        zzTransL = JsDocumentationColoringLexer.ZZ_TRANS;
        zzRowMapL = JsDocumentationColoringLexer.ZZ_ROWMAP;
        zzAttrL = JsDocumentationColoringLexer.ZZ_ATTRIBUTE;
        block47 : do {
            zzMarkedPosL = this.zzMarkedPos;
            this.yychar += zzMarkedPosL - this.zzStartRead;
            zzAction = -1;
            tokenLength = 0;
            this.zzState = JsDocumentationColoringLexer.ZZ_LEXSTATE[this.zzLexicalState];
            do lbl-1000: // 3 sources:
            {
                if ((zzInput = this.input.read()) == -1) {
                    zzInput = -1;
                    break;
                }
                zzNext = zzTransL[zzRowMapL[this.zzState] + zzCMapL[zzInput]];
                if (zzNext == -1) break;
                this.zzState = zzNext;
                zzAttributes = zzAttrL[this.zzState];
                if ((zzAttributes & 1) != 1) ** GOTO lbl-1000
                zzAction = this.zzState;
                tokenLength = this.input.readLength();
            } while ((zzAttributes & 8) != 8);
            if (zzInput != -1) {
                this.input.backup(this.input.readLength() - tokenLength);
            }
            switch (zzAction < 0 ? zzAction : JsDocumentationColoringLexer.ZZ_ACTION[zzAction]) {
                case 6: {
                    return JsDocumentationTokenId.ASTERISK;
                }
                case 24: {
                    continue block47;
                }
                case 9: {
                    return JsDocumentationTokenId.BRACKET_LEFT_CURLY;
                }
                case 25: {
                    continue block47;
                }
                case 14: {
                    this.yybegin(2);
                    return JsDocumentationTokenId.COMMENT_DOC_START;
                }
                case 26: {
                    continue block47;
                }
                case 12: {
                    return JsDocumentationTokenId.BRACKET_RIGHT_BRACKET;
                }
                case 27: {
                    continue block47;
                }
                case 11: {
                    return JsDocumentationTokenId.BRACKET_LEFT_BRACKET;
                }
                case 28: {
                    continue block47;
                }
                case 7: {
                    this.yybegin(6);
                    this.yypushback(1);
                }
                case 29: {
                    continue block47;
                }
                case 20: {
                    return JsDocumentationTokenId.COMMENT_END;
                }
                case 30: {
                    continue block47;
                }
                case 18: {
                    this.yybegin(2);
                    return JsDocumentationTokenId.STRING_END;
                }
                case 31: {
                    continue block47;
                }
                case 10: {
                    return JsDocumentationTokenId.BRACKET_RIGHT_CURLY;
                }
                case 32: {
                    continue block47;
                }
                case 21: {
                    this.yypushback(1);
                    return JsDocumentationTokenId.OTHER;
                }
                case 33: {
                    continue block47;
                }
                case 5: {
                    return JsDocumentationTokenId.WHITESPACE;
                }
                case 34: {
                    continue block47;
                }
                case 19: {
                    this.yybegin(4);
                }
                case 35: {
                    continue block47;
                }
                case 13: {
                    this.yypushback(1);
                    this.yybegin(2);
                    return JsDocumentationTokenId.COMMENT_BLOCK_START;
                }
                case 36: {
                    continue block47;
                }
                case 16: {
                    this.yypushback(1);
                    this.yybegin(2);
                    if (tokenLength - 1 > 0) {
                        return JsDocumentationTokenId.UNKNOWN;
                    }
                }
                case 37: {
                    continue block47;
                }
                case 8: {
                    return JsDocumentationTokenId.COMMA;
                }
                case 38: {
                    continue block47;
                }
                case 4: {
                    this.yybegin(8);
                    return JsDocumentationTokenId.STRING_BEGIN;
                }
                case 39: {
                    continue block47;
                }
                case 3: {
                    return JsDocumentationTokenId.OTHER;
                }
                case 40: {
                    continue block47;
                }
                case 2: {
                    return JsDocumentationTokenId.EOL;
                }
                case 41: {
                    continue block47;
                }
                case 17: {
                    this.yypushback(1);
                    this.yybegin(10);
                    if (tokenLength - 1 > 0) {
                        return JsDocumentationTokenId.STRING;
                    }
                }
                case 42: {
                    continue block47;
                }
                case 22: {
                    this.yybegin(2);
                    return JsDocumentationTokenId.KEYWORD;
                }
                case 43: {
                    continue block47;
                }
                case 15: {
                    this.yybegin(2);
                    return JsDocumentationTokenId.AT;
                }
                case 44: {
                    continue block47;
                }
                case 23: {
                    return JsDocumentationTokenId.HTML;
                }
                case 45: {
                    continue block47;
                }
                case 1: 
                case 46: {
                    continue block47;
                }
            }
            if (zzInput == -1 && this.zzStartRead == this.zzCurrentPos) {
                this.zzAtEOF = true;
                if (this.input.readLength() <= 0) return null;
                this.input.backup(1);
                return JsDocumentationTokenId.UNKNOWN;
            }
            this.zzScanError(1);
        } while (true);
    }

    public static final class LexerState {
        final int zzState;
        final int zzLexicalState;

        LexerState(int zzState, int zzLexicalState) {
            this.zzState = zzState;
            this.zzLexicalState = zzLexicalState;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            LexerState other = (LexerState)obj;
            if (this.zzState != other.zzState) {
                return false;
            }
            if (this.zzLexicalState != other.zzLexicalState) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 5;
            hash = 29 * hash + this.zzState;
            hash = 29 * hash + this.zzLexicalState;
            return hash;
        }

        public String toString() {
            return "LexerState{zzState=" + this.zzState + ", zzLexicalState=" + this.zzLexicalState + '}';
        }
    }

}

