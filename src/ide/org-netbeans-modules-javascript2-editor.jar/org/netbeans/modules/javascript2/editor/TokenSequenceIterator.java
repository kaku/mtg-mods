/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.javascript2.editor;

import java.util.List;
import org.netbeans.api.lexer.TokenSequence;

final class TokenSequenceIterator {
    private final List<TokenSequence<?>> list;
    private final boolean backward;
    private int index;

    public TokenSequenceIterator(List<TokenSequence<?>> list, boolean backward) {
        this.list = list;
        this.backward = backward;
        this.index = -1;
    }

    public boolean hasMore() {
        return this.backward ? this.hasPrevious() : this.hasNext();
    }

    public TokenSequence<?> getSequence() {
        assert (this.index >= 0 && this.index < this.list.size());
        return this.list.get(this.index);
    }

    private boolean hasPrevious() {
        boolean anotherSeq = false;
        if (this.index == -1) {
            this.index = this.list.size() - 1;
            anotherSeq = true;
        }
        while (this.index >= 0) {
            TokenSequence seq = this.list.get(this.index);
            if (anotherSeq) {
                seq.moveEnd();
            }
            if (seq.movePrevious()) {
                return true;
            }
            anotherSeq = true;
            --this.index;
        }
        return false;
    }

    private boolean hasNext() {
        boolean anotherSeq = false;
        if (this.index == -1) {
            this.index = 0;
            anotherSeq = true;
        }
        while (this.index < this.list.size()) {
            TokenSequence seq = this.list.get(this.index);
            if (anotherSeq) {
                seq.moveStart();
            }
            if (seq.moveNext()) {
                return true;
            }
            anotherSeq = true;
            ++this.index;
        }
        return false;
    }
}

