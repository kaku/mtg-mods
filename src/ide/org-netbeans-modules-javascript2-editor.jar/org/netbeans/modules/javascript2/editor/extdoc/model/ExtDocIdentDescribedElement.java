/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocIdentSimpleElement;

public class ExtDocIdentDescribedElement
extends ExtDocIdentSimpleElement {
    private final String description;

    private ExtDocIdentDescribedElement(ExtDocElementType type, String identifier, String description) {
        super(type, identifier);
        this.description = description;
    }

    public static ExtDocIdentDescribedElement create(ExtDocElementType type, String identifier, String description) {
        return new ExtDocIdentDescribedElement(type, identifier, description);
    }

    public String getDescription() {
        return this.description;
    }
}

