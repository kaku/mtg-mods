/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.jsdoc;

import java.util.Map;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocComment;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocParser;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsDocDocumentationHolder
extends JsDocumentationHolder {
    private Map<Integer, JsDocComment> blocks = null;

    public JsDocDocumentationHolder(JsDocumentationProvider provider, Snapshot snapshot) {
        super(provider, snapshot);
    }

    public synchronized Map getCommentBlocks() {
        if (this.blocks == null) {
            this.blocks = JsDocParser.parse(this.getSnapshot());
        }
        return this.blocks;
    }
}

