/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.extdoc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.javascript2.editor.extdoc.ExtDocAnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.extdoc.ExtDocDocumentationHolder;
import org.netbeans.modules.javascript2.editor.extdoc.ExtDocSyntaxProvider;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.parsing.api.Snapshot;

public class ExtDocDocumentationProvider
implements JsDocumentationProvider {
    private static Set<String> supportedTags;
    private static final List<AnnotationCompletionTagProvider> ANNOTATION_PROVIDERS;
    private static final SyntaxProvider SYNTAX_PROVIDER;

    @Override
    public JsDocumentationHolder createDocumentationHolder(Snapshot snapshot) {
        return new ExtDocDocumentationHolder(this, snapshot);
    }

    public synchronized Set getSupportedTags() {
        if (supportedTags == null) {
            supportedTags = new HashSet<String>(ExtDocElementType.values().length);
            for (ExtDocElementType type : ExtDocElementType.values()) {
                supportedTags.add(type.toString());
            }
            supportedTags.remove("unknown");
            supportedTags.remove("description");
        }
        return supportedTags;
    }

    public List<AnnotationCompletionTagProvider> getAnnotationsProvider() {
        return ANNOTATION_PROVIDERS;
    }

    @Override
    public SyntaxProvider getSyntaxProvider() {
        return SYNTAX_PROVIDER;
    }

    static {
        ANNOTATION_PROVIDERS = Arrays.asList(new ExtDocAnnotationCompletionTagProvider("ExtDoc"));
        SYNTAX_PROVIDER = new ExtDocSyntaxProvider();
    }
}

