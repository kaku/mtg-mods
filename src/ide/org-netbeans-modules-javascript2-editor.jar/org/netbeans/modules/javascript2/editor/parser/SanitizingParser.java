/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.FunctionNode
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.Task
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.parser;

import java.util.Arrays;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import jdk.nashorn.internal.ir.FunctionNode;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.parser.JsErrorManager;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;

public abstract class SanitizingParser
extends Parser {
    private static final Logger LOGGER = Logger.getLogger(SanitizingParser.class.getName());
    private static final boolean PARSE_BIG_FILES = Boolean.getBoolean("nb.js.parse.big.files");
    private static final long MAX_FILE_SIZE_TO_PARSE = Integer.getInteger("nb.js.big.file.size", 1048576).intValue();
    private static final long MAX_MINIMIZE_FILE_SIZE_TO_PARSE = Integer.getInteger("nb.js.big.minimize.file.size", 349525).intValue();
    private final Language<JsTokenId> language;
    private JsParserResult lastResult = null;

    public SanitizingParser(Language<JsTokenId> language) {
        this.language = language;
    }

    public final void parse(Snapshot snapshot, Task task, SourceModificationEvent event) throws ParseException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, snapshot.getText().toString());
        }
        try {
            JsErrorManager errorManager = new JsErrorManager(snapshot, this.language);
            this.lastResult = this.parseSource(snapshot, event, Sanitize.NONE, errorManager);
            this.lastResult.setErrors(errorManager.getErrors());
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, "Exception during parsing", ex);
            this.lastResult = new JsParserResult(snapshot, null);
        }
    }

    protected abstract String getDefaultScriptName();

    protected abstract FunctionNode parseSource(Snapshot var1, String var2, String var3, JsErrorManager var4) throws Exception;

    protected abstract String getMimeType();

    private JsParserResult parseSource(Snapshot snapshot, SourceModificationEvent event, Sanitize sanitizing, JsErrorManager errorManager) throws Exception {
        FileObject fo = snapshot.getSource().getFileObject();
        long startTime = System.nanoTime();
        String scriptName = fo != null ? snapshot.getSource().getFileObject().getNameExt() : this.getDefaultScriptName();
        if (!this.isParsable(snapshot)) {
            return new JsParserResult(snapshot, null);
        }
        int caretOffset = GsfUtilities.getLastKnownCaretOffset((Snapshot)snapshot, (EventObject)event);
        JsParserResult result = this.parseContext(new Context(scriptName, snapshot, caretOffset), sanitizing, errorManager);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Parsing took: {0} ms; source: {1}", new Object[]{(System.nanoTime() - startTime) / 1000000, scriptName});
        }
        return result;
    }

    private boolean isParsable(Snapshot snapshot) {
        FileObject fo = snapshot.getSource().getFileObject();
        boolean isEmbeded = !this.getMimeType().equals(snapshot.getMimePath().getPath());
        CharSequence text = snapshot.getText();
        String scriptName = fo != null ? snapshot.getSource().getFileObject().getNameExt() : this.getDefaultScriptName();
        Long size = fo != null && !isEmbeded ? fo.getSize() : (long)text.length();
        if (!PARSE_BIG_FILES) {
            if (size > MAX_FILE_SIZE_TO_PARSE) {
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "The file {0} was not parsed because the size is too big.", scriptName);
                }
                return false;
            }
            if (size > MAX_MINIMIZE_FILE_SIZE_TO_PARSE) {
                boolean isMinified = false;
                TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(snapshot, 0, this.language);
                if (ts != null) {
                    int countedLines;
                    int offset = 0;
                    int countChars = 0;
                    for (countedLines = 0; !isMinified && ts.moveNext() && countedLines < 5; ++countedLines) {
                        LexUtilities.findNext(ts, Arrays.asList(new JsTokenId[]{JsTokenId.BLOCK_COMMENT, JsTokenId.LINE_COMMENT, JsTokenId.EOL}));
                        offset = ts.offset();
                        LexUtilities.findNextToken(ts, Arrays.asList(new JsTokenId[]{JsTokenId.EOL}));
                        countChars += ts.offset() - offset;
                    }
                    if (countedLines > 0 && countChars / countedLines > 200) {
                        if (LOGGER.isLoggable(Level.FINE)) {
                            LOGGER.log(Level.FINE, "The file {0} was not parsed because the is minimize and size is big.", scriptName);
                        }
                        return false;
                    }
                }
            }
        }
        return true;
    }

    JsParserResult parseContext(Context context, Sanitize sanitizing, JsErrorManager errorManager) throws Exception {
        return this.parseContext(context, sanitizing, errorManager, true);
    }

    private JsParserResult parseContext(Context context, Sanitize sanitizing, JsErrorManager errorManager, boolean copyErrors) throws Exception {
        boolean sanitized = false;
        if (sanitizing != Sanitize.NONE && sanitizing != Sanitize.NEVER) {
            boolean ok = this.sanitizeSource(context, sanitizing, errorManager);
            if (ok) {
                sanitized = true;
                assert (context.getSanitizedSource() != null);
            } else {
                return this.parseContext(context, sanitizing.next(), errorManager, false);
            }
        }
        JsErrorManager current = new JsErrorManager(context.getSnapshot(), this.language);
        FunctionNode node = this.parseSource(context.getSnapshot(), context.getName(), context.getSource(), current);
        if (copyErrors) {
            errorManager.fillErrors(current);
        }
        if (sanitizing != Sanitize.NEVER) {
            if (!sanitized) {
                if (current.getMissingCurlyError() != null) {
                    return this.parseContext(context, Sanitize.MISSING_CURLY, errorManager, false);
                }
                if (current.getMissingSemicolonError() != null) {
                    return this.parseContext(context, Sanitize.MISSING_SEMICOLON, errorManager, false);
                }
            }
            if (node == null || !current.isEmpty()) {
                return this.parseContext(context, sanitizing.next(), errorManager, false);
            }
        }
        return new JsParserResult(context.getSnapshot(), node);
    }

    private boolean sanitizeSource(Context context, Sanitize sanitizing, JsErrorManager errorManager) {
        if (sanitizing == Sanitize.MISSING_CURLY) {
            Error error = errorManager.getMissingCurlyError();
            if (error != null) {
                int offset = error.getStartPosition();
                return this.sanitizeBrackets(sanitizing, context, offset, '{', '}');
            }
        } else if (sanitizing == Sanitize.MISSING_SEMICOLON) {
            Error error = errorManager.getMissingSemicolonError();
            if (error != null) {
                String source = context.getOriginalSource();
                boolean ok = false;
                StringBuilder builder = new StringBuilder(source);
                if (error.getStartPosition() >= source.length()) {
                    builder.append(';');
                    ok = true;
                } else {
                    int replaceOffset = error.getStartPosition();
                    if (replaceOffset >= 0 && Character.isWhitespace(replaceOffset)) {
                        builder.delete(replaceOffset, replaceOffset + 1);
                        builder.insert(replaceOffset, ';');
                        ok = true;
                    }
                }
                if (ok) {
                    context.setSanitizedSource(builder.toString());
                    context.setSanitization(sanitizing);
                    return true;
                }
            }
        } else if (sanitizing == Sanitize.SYNTAX_ERROR_CURRENT) {
            List<? extends Error> errors = errorManager.getErrors();
            if (!errors.isEmpty()) {
                Error error = errors.get(0);
                int offset = error.getStartPosition();
                TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(context.getSnapshot(), 0, this.language);
                if (ts != null) {
                    int start;
                    ts.move(offset);
                    if (ts.moveNext() && (start = ts.offset()) >= 0 && ts.moveNext()) {
                        int end = ts.offset();
                        StringBuilder builder = new StringBuilder(context.getOriginalSource());
                        SanitizingParser.erase(builder, start, end);
                        context.setSanitizedSource(builder.toString());
                        context.setSanitization(sanitizing);
                        return true;
                    }
                }
            }
        } else if (sanitizing == Sanitize.SYNTAX_ERROR_PREVIOUS) {
            List<? extends Error> errors = errorManager.getErrors();
            if (!errors.isEmpty()) {
                Error error = errors.get(0);
                int offset = error.getStartPosition();
                return this.sanitizePrevious(sanitizing, context, offset, new TokenCondition(){

                    @Override
                    public boolean found(JsTokenId id) {
                        return id != JsTokenId.WHITESPACE && id != JsTokenId.EOL && id != JsTokenId.DOC_COMMENT && id != JsTokenId.LINE_COMMENT && id != JsTokenId.BLOCK_COMMENT;
                    }
                });
            }
        } else if (sanitizing == Sanitize.MISSING_PAREN) {
            List<? extends Error> errors = errorManager.getErrors();
            if (!errors.isEmpty()) {
                Error error = errors.get(0);
                int offset = error.getStartPosition();
                return this.sanitizeBrackets(sanitizing, context, offset, '(', ')');
            }
        } else if (sanitizing == Sanitize.ERROR_DOT) {
            List<? extends Error> errors = errorManager.getErrors();
            if (!errors.isEmpty()) {
                Error error = errors.get(0);
                int offset = error.getStartPosition();
                return this.sanitizePrevious(sanitizing, context, offset, new TokenCondition(){

                    @Override
                    public boolean found(JsTokenId id) {
                        return id == JsTokenId.OPERATOR_DOT;
                    }
                });
            }
        } else if (sanitizing == Sanitize.ERROR_LINE) {
            List<? extends Error> errors = errorManager.getErrors();
            if (!errors.isEmpty()) {
                Error error = errors.get(0);
                int offset = error.getStartPosition();
                return this.sanitizeLine(sanitizing, context, offset);
            }
        } else {
            if (sanitizing == Sanitize.EDITED_LINE) {
                int offset = context.getCaretOffset();
                return this.sanitizeLine(sanitizing, context, offset);
            }
            if (sanitizing == Sanitize.PREVIOUS_LINES) {
                StringBuilder result = new StringBuilder(context.getOriginalSource());
                for (Error error : errorManager.getErrors()) {
                    TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(context.getSnapshot(), error.getStartPosition(), this.language);
                    if (ts != null) {
                        ts.move(error.getStartPosition());
                        if (ts.movePrevious()) {
                            LexUtilities.findPreviousIncluding(ts, Collections.singletonList(JsTokenId.EOL));
                            if (this.sanitizeLine(context.getOriginalSource(), result, ts.offset())) continue;
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                context.setSanitizedSource(result.toString());
                context.setSanitization(sanitizing);
                return true;
            }
        }
        return false;
    }

    private boolean sanitizePrevious(Sanitize sanitizing, Context context, int offset, TokenCondition condition) {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(context.getSnapshot(), 0, this.language);
        if (ts != null) {
            ts.move(offset);
            int start = -1;
            while (ts.movePrevious()) {
                if (!condition.found((JsTokenId)ts.token().id())) continue;
                start = ts.offset();
                break;
            }
            if (start >= 0) {
                int end = offset;
                if (ts.moveNext()) {
                    end = ts.offset();
                }
                StringBuilder builder = new StringBuilder(context.getOriginalSource());
                SanitizingParser.erase(builder, start, end);
                context.setSanitizedSource(builder.toString());
                context.setSanitization(sanitizing);
                return true;
            }
        }
        return false;
    }

    private boolean sanitizeLine(Sanitize sanitizing, Context context, int offset) {
        if (offset > -1) {
            StringBuilder builder;
            String source = context.getOriginalSource();
            if (!this.sanitizeLine(source, builder = new StringBuilder(source), offset)) {
                return false;
            }
            context.setSanitizedSource(builder.toString());
            context.setSanitization(sanitizing);
            return true;
        }
        return false;
    }

    private boolean sanitizeLine(String source, StringBuilder result, int offset) {
        if (offset > -1 && !source.isEmpty()) {
            int start = offset > 0 ? offset - 1 : offset;
            int end = start + 1;
            boolean incPosition = false;
            char c = source.charAt(start);
            while (start > 0 && c != '\n' && c != '\r' && c != '{' && c != '}') {
                c = source.charAt(--start);
                if (start <= 0) {
                    incPosition = false;
                    continue;
                }
                incPosition = true;
            }
            if (incPosition) {
                ++start;
            }
            boolean decPosition = false;
            if (end < source.length()) {
                c = source.charAt(end);
                while (end < source.length() && c != '\n' && c != '\r' && c != '{' && c != '}') {
                    c = source.charAt(end++);
                    if (end >= source.length()) {
                        decPosition = false;
                        continue;
                    }
                    decPosition = true;
                }
            }
            if (decPosition) {
                --end;
            }
            SanitizingParser.erase(result, start, end);
            return true;
        }
        return false;
    }

    private boolean sanitizeBrackets(Sanitize sanitizing, Context context, int offset, char left, char right) {
        char current;
        String source = context.getOriginalSource();
        int balance = 0;
        for (int i = 0; i < source.length(); ++i) {
            current = source.charAt(i);
            if (current == left) {
                ++balance;
                continue;
            }
            if (current != right) continue;
            --balance;
        }
        if (balance != 0) {
            StringBuilder builder = new StringBuilder(source);
            if (balance < 0) {
                int index;
                while (balance < 0 && (index = builder.lastIndexOf(Character.toString(right))) >= 0) {
                    SanitizingParser.erase(builder, index, index + 1);
                    ++balance;
                }
            } else if (balance > 0) {
                if (offset >= source.length()) {
                    while (balance > 0) {
                        builder.append(right);
                        --balance;
                    }
                } else {
                    while (balance > 0 && offset - balance >= 0) {
                        current = source.charAt(offset - balance);
                        if (Character.isWhitespace(current)) {
                            builder.replace(offset - balance, offset - balance + 1, Character.toString(right));
                            --balance;
                            continue;
                        }
                        return false;
                    }
                    if (balance > 0) {
                        return false;
                    }
                }
            }
            context.setSanitizedSource(builder.toString());
            context.setSanitization(sanitizing);
            return true;
        }
        return false;
    }

    public final Parser.Result getResult(Task task) throws ParseException {
        return this.lastResult;
    }

    public final void addChangeListener(ChangeListener changeListener) {
        LOGGER.log(Level.FINE, "Adding changeListener: {0}", changeListener);
    }

    public final void removeChangeListener(ChangeListener changeListener) {
        LOGGER.log(Level.FINE, "Removing changeListener: {0}", changeListener);
    }

    private static void erase(StringBuilder builder, int start, int end) {
        builder.delete(start, end);
        for (int i = start; i < end; ++i) {
            builder.insert(i, ' ');
        }
    }

    private static abstract class TokenCondition {
        private TokenCondition() {
        }

        public abstract boolean found(JsTokenId var1);
    }

    public static enum Sanitize {
        NEVER{

            @Override
            public Sanitize next() {
                return NEVER;
            }
        }
        ,
        NONE{

            @Override
            public Sanitize next() {
                return MISSING_CURLY;
            }
        }
        ,
        MISSING_CURLY{

            @Override
            public Sanitize next() {
                return MISSING_SEMICOLON;
            }
        }
        ,
        MISSING_SEMICOLON{

            @Override
            public Sanitize next() {
                return SYNTAX_ERROR_CURRENT;
            }
        }
        ,
        SYNTAX_ERROR_CURRENT{

            @Override
            public Sanitize next() {
                return SYNTAX_ERROR_PREVIOUS;
            }
        }
        ,
        SYNTAX_ERROR_PREVIOUS{

            @Override
            public Sanitize next() {
                return MISSING_PAREN;
            }
        }
        ,
        MISSING_PAREN{

            @Override
            public Sanitize next() {
                return EDITED_DOT;
            }
        }
        ,
        EDITED_DOT{

            @Override
            public Sanitize next() {
                return ERROR_DOT;
            }
        }
        ,
        ERROR_DOT{

            @Override
            public Sanitize next() {
                return ERROR_LINE;
            }
        }
        ,
        ERROR_LINE{

            @Override
            public Sanitize next() {
                return EDITED_LINE;
            }
        }
        ,
        EDITED_LINE{

            @Override
            public Sanitize next() {
                return PREVIOUS_LINES;
            }
        }
        ,
        PREVIOUS_LINES{

            @Override
            public Sanitize next() {
                return NEVER;
            }
        };
        

        private Sanitize() {
        }

        public abstract Sanitize next();

    }

    static class Context {
        private final String name;
        private final Snapshot snapshot;
        private final int caretOffset;
        private String source;
        private String sanitizedSource;
        private Sanitize sanitization;

        public Context(String name, Snapshot snapshot, int caretOffset) {
            this.name = name;
            this.snapshot = snapshot;
            this.caretOffset = caretOffset;
        }

        public String getName() {
            return this.name;
        }

        public Snapshot getSnapshot() {
            return this.snapshot;
        }

        public int getCaretOffset() {
            return this.snapshot.getEmbeddedOffset(this.caretOffset);
        }

        public String getSource() {
            if (this.sanitizedSource != null) {
                return this.sanitizedSource;
            }
            return this.getOriginalSource();
        }

        public String getOriginalSource() {
            if (this.source == null) {
                this.source = this.snapshot.getText().toString();
            }
            return this.source;
        }

        public String getSanitizedSource() {
            return this.sanitizedSource;
        }

        public void setSanitizedSource(String sanitizedSource) {
            this.sanitizedSource = sanitizedSource;
        }

        public Sanitize getSanitization() {
            return this.sanitization;
        }

        public void setSanitization(Sanitize sanitization) {
            this.sanitization = sanitization;
        }
    }

}

