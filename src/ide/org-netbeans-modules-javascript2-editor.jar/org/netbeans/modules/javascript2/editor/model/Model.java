/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.model.Bundle;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.JsWith;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.OccurrencesSupport;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.AnonymousObject;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsWithObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelElementFactoryAccessor;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.ModelVisitor;
import org.netbeans.modules.javascript2.editor.model.impl.OccurrenceBuilder;
import org.netbeans.modules.javascript2.editor.model.impl.ParameterObject;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionArgument;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionInterceptor;
import org.netbeans.modules.javascript2.editor.spi.model.ModelElementFactory;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public final class Model {
    private static final Logger LOGGER = Logger.getLogger(OccurrencesSupport.class.getName());
    private static final Comparator<Map.Entry<String, ? extends JsObject>> PROPERTIES_COMPARATOR = new Comparator<Map.Entry<String, ? extends JsObject>>(){

        @Override
        public int compare(Map.Entry<String, ? extends JsObject> o1, Map.Entry<String, ? extends JsObject> o2) {
            return o1.getKey().compareTo(o2.getKey());
        }
    };
    private static final Comparator<TypeUsage> RETURN_TYPES_COMPARATOR = new Comparator<TypeUsage>(){

        @Override
        public int compare(TypeUsage o1, TypeUsage o2) {
            return o1.getType().compareTo(o2.getType());
        }
    };
    private static final Pattern OBJECT_PATTERN = Pattern.compile("(FUNCTION|OBJECT) (\\S+) \\[ANONYMOUS: (true|false), DECLARED: (true|false)( - (\\S+))?(, MODIFIERS: ((PUBLIC|STATIC|PROTECTED|PRIVATE|DEPRECATED|ABSTRACT)(, (PUBLIC|STATIC|PROTECTED|PRIVATE|DEPRECATED|ABSTRACT))*))?, (FUNCTION|METHOD|CONSTRUCTOR|OBJECT|PROPERTY|VARIABLE|FIELD|FILE|PARAMETER|ANONYMOUS_OBJECT|PROPERTY_GETTER|PROPERTY_SETTER|OBJECT_LITERAL|CATCH_BLOCK)\\]");
    private static final Pattern RETURN_TYPE_PATTERN = Pattern.compile("(\\S+), RESOLVED: (true|false)");
    private final JsParserResult parserResult;
    private final OccurrencesSupport occurrencesSupport;
    private final OccurrenceBuilder occurrenceBuilder;
    private ModelVisitor visitor;
    private boolean resolveWithObjects;

    Model(JsParserResult parserResult) {
        this.parserResult = parserResult;
        this.occurrencesSupport = new OccurrencesSupport(this);
        this.occurrenceBuilder = new OccurrenceBuilder(parserResult);
        this.resolveWithObjects = false;
    }

    private synchronized ModelVisitor getModelVisitor() {
        boolean resolveWindowProperties = false;
        if (this.visitor == null) {
            long start = System.currentTimeMillis();
            this.visitor = new ModelVisitor(this.parserResult, this.occurrenceBuilder);
            FunctionNode root = this.parserResult.getRoot();
            if (root != null) {
                root.accept((NodeVisitor)this.visitor);
            }
            long startResolve = System.currentTimeMillis();
            this.occurrenceBuilder.processOccurrences(this.visitor.getGlobalObject());
            this.resolveLocalTypes(this.visitor.getGlobalObject(), this.parserResult.getDocumentationHolder());
            ModelElementFactory elementFactory = ModelElementFactoryAccessor.getDefault().createModelElementFactory();
            long startCallingME = System.currentTimeMillis();
            Map<FunctionInterceptor, Collection<ModelVisitor.FunctionCall>> calls = this.visitor.getCallsForProcessing();
            if (calls != null && !calls.isEmpty()) {
                for (Map.Entry<FunctionInterceptor, Collection<ModelVisitor.FunctionCall>> entry : calls.entrySet()) {
                    Collection<ModelVisitor.FunctionCall> fncCalls = entry.getValue();
                    if (fncCalls == null || fncCalls.isEmpty()) continue;
                    for (ModelVisitor.FunctionCall call : fncCalls) {
                        entry.getKey().intercept(call.getName(), this.visitor.getGlobalObject(), call.getScope(), elementFactory, call.getArguments());
                    }
                }
            }
            resolveWindowProperties = !this.resolveWithObjects;
            long end = System.currentTimeMillis();
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.fine(MessageFormat.format("Building model took {0}ms. Resolving types took {1}ms. Extending model took {2}", end - start, startCallingME - startResolve, end - startCallingME));
            }
        } else if (this.resolveWithObjects) {
            this.resolveWithObjects = false;
            resolveWindowProperties = true;
            JsIndex jsIndex = JsIndex.get(this.parserResult.getSnapshot().getSource().getFileObject());
            this.processWithObjectIn(this.visitor.getGlobalObject(), jsIndex);
        }
        if (resolveWindowProperties) {
            this.processWindowsProperties(this.visitor.getGlobalObject());
        }
        return this.visitor;
    }

    private void processWithObjectIn(JsObject where, JsIndex jsIndex) {
        if (where.getProperties().isEmpty()) {
            return;
        }
        ArrayList<? extends JsObject> properties = new ArrayList<JsObject>(where.getProperties().values());
        for (JsObject property : properties) {
            if (property instanceof JsWith) {
                this.processWithObject((JsWith)property, jsIndex, null);
                continue;
            }
            this.processWithObjectIn(property, jsIndex);
        }
    }

    private void processWithObject(JsWith with, JsIndex jsIndex, List<String> outerExpression) {
        Collection<TypeUsage> withTypes = with.getTypes();
        withTypes.clear();
        Collection resolveTypeFromExpression = new ArrayList<TypeUsage>();
        int offset = ((JsWithObjectImpl)with).getExpressionRange().getEnd();
        List<String> ech = ModelUtils.resolveExpressionChain(this.parserResult.getSnapshot(), offset, false);
        ArrayList<String> originalExp = new ArrayList<String>(ech);
        JsObject fromType = null;
        if (outerExpression == null) {
            outerExpression = ech;
            resolveTypeFromExpression.addAll(ModelUtils.resolveTypeFromExpression(this, jsIndex, ech, offset));
            resolveTypeFromExpression = ModelUtils.resolveTypes(resolveTypeFromExpression, this.parserResult, true);
            withTypes.addAll(resolveTypeFromExpression);
        } else {
            ech.addAll(outerExpression);
            boolean resolved = false;
            resolveTypeFromExpression.addAll(ModelUtils.resolveTypeFromExpression(this, jsIndex, ech, offset));
            resolveTypeFromExpression = ModelUtils.resolveTypes(resolveTypeFromExpression, this.parserResult, true);
            for (TypeUsage type2 : resolveTypeFromExpression) {
                fromType = ModelUtils.findJsObjectByName(this.visitor.getGlobalObject(), type2.getType());
                if (fromType == null) continue;
                resolved = true;
                outerExpression = ech;
                withTypes.add(type2);
                break;
            }
            if (!resolved) {
                resolveTypeFromExpression.clear();
                resolveTypeFromExpression.addAll(ModelUtils.resolveTypeFromExpression(this, jsIndex, originalExp, offset));
                resolveTypeFromExpression = ModelUtils.resolveTypes(resolveTypeFromExpression, this.parserResult, true);
                for (TypeUsage type : resolveTypeFromExpression) {
                    fromType = ModelUtils.findJsObjectByName(this.visitor.getGlobalObject(), type.getType());
                    if (fromType == null) continue;
                    resolved = true;
                    outerExpression = originalExp;
                    withTypes.add(type);
                    break;
                }
            }
        }
        for (JsWith innerWith : with.getInnerWiths()) {
            this.processWithObject(innerWith, jsIndex, outerExpression);
        }
        for (TypeUsage type : resolveTypeFromExpression) {
            fromType = ModelUtils.findJsObjectByName(this.visitor.getGlobalObject(), type.getType());
            if (fromType != null) {
                this.processWithExpressionOccurrences(fromType, ((JsWithObjectImpl)with).getExpressionRange(), originalExp);
                Collection<TypeUsage> assignments = ModelUtils.resolveTypes(fromType.getAssignments(), this.parserResult, true);
                for (TypeUsage assignment : assignments) {
                    Collection<IndexedElement> properties = jsIndex.getProperties(assignment.getType());
                    for (IndexedElement indexedElement : properties) {
                        JsObject jsWithProperty = with.getProperty(indexedElement.getName());
                        if (jsWithProperty == null) continue;
                        this.moveProperty(fromType, jsWithProperty);
                    }
                }
                for (JsObject fromTypeProperty : fromType.getProperties().values()) {
                    JsObject jsWithProperty2 = with.getProperty(fromTypeProperty.getName());
                    if (jsWithProperty2 == null) continue;
                    this.moveProperty(fromType, jsWithProperty2);
                }
                continue;
            }
            Collection<IndexedElement> properties = jsIndex.getProperties(type.getType());
            if (properties.isEmpty()) continue;
            StringBuilder fqn = new StringBuilder();
            for (int i = outerExpression.size() - 1; i > -1; --i) {
                fqn.append(outerExpression.get(--i));
                fqn.append('.');
            }
            if (fqn.length() <= 0) continue;
            DeclarationScope ds = ModelUtils.getDeclarationScope(with);
            JsObject fromExpression = ModelUtils.findJsObjectByName((JsObject)((Object)ds), fqn.toString());
            if (fromExpression == null) {
                int position = ((JsWithObjectImpl)with).getExpressionRange().getStart();
                JsObject parent = this.visitor.getGlobalObject();
                StringTokenizer stringTokenizer = new StringTokenizer(type.getType(), ".");
                while (stringTokenizer.hasMoreTokens()) {
                    String name = stringTokenizer.nextToken();
                    JsObject newObject = parent.getProperty(name);
                    if (newObject == null) {
                        newObject = new JsObjectImpl(parent, new IdentifierImpl(name, position), new OffsetRange(position, position + name.length()), false, null, null);
                        parent.addProperty(name, newObject);
                    }
                    position = position + name.length() + 1;
                    parent = newObject;
                }
                fromExpression = parent;
            }
            if (fromExpression == null) continue;
            for (IndexedElement indexedElement : properties) {
                JsObject jsWithProperty = with.getProperty(indexedElement.getName());
                if (jsWithProperty == null) continue;
                this.moveProperty(fromExpression, jsWithProperty);
            }
            this.processWithExpressionOccurrences(fromExpression, ((JsWithObjectImpl)with).getExpressionRange(), originalExp);
        }
        boolean hasOuter = with.getOuterWith() != null;
        Collection<? extends JsObject> variables = ModelUtils.getVariables(ModelUtils.getDeclarationScope(with));
        ArrayList<? extends JsObject> withProperties = new ArrayList<JsObject>(with.getProperties().values());
        for (JsObject jsWithProperty : withProperties) {
            if (jsWithProperty instanceof JsWith) continue;
            String name = jsWithProperty.getName();
            boolean moved = false;
            if (hasOuter) {
                this.moveProperty(with.getOuterWith(), jsWithProperty);
                moved = true;
            } else {
                for (JsObject variable : variables) {
                    if (variable.getParent() == null || !variable.getName().equals(name)) continue;
                    this.moveProperty(variable.getParent(), jsWithProperty);
                    moved = true;
                    break;
                }
            }
            if (moved) continue;
            this.moveProperty(this.visitor.getGlobalObject(), jsWithProperty);
        }
    }

    private void processWithExpressionOccurrences(JsObject jsObject, OffsetRange expRange, List<String> expression) {
        TokenSequence<? extends JsTokenId> ts;
        JsObject parent = jsObject.getParent();
        boolean isThis = false;
        if (expression.size() > 1 && expression.get(expression.size() - 2).equals("this")) {
            parent = ModelUtils.findJsObject(this, expRange.getStart());
            if (parent instanceof JsWith) {
                parent = parent.getParent();
            }
            parent = this.visitor.resolveThis(parent);
            isThis = true;
        }
        if ((ts = LexUtilities.getJsTokenSequence(this.parserResult.getSnapshot(), expRange.getEnd())) == null) {
            return;
        }
        if (isThis) {
            ts.move(expRange.getStart());
        } else {
            ts.move(expRange.getEnd());
        }
        if (isThis && !ts.moveNext()) {
            return;
        }
        if (!isThis && !ts.movePrevious()) {
            return;
        }
        Token token = ts.token();
        if (isThis) {
            for (int i = expression.size() - 4; i > -1; --i) {
                String name = expression.get(i--);
                while ((token.id() != JsTokenId.IDENTIFIER || token.id() != JsTokenId.IDENTIFIER || !token.text().toString().equals(name)) && ts.offset() < expRange.getEnd() && ts.moveNext()) {
                    token = ts.token();
                }
                if (parent == null || token.id() != JsTokenId.IDENTIFIER || !token.text().toString().equals(name)) continue;
                JsObject property = parent.getProperty(name);
                if (property != null) {
                    property.addOccurrence(new OffsetRange(ts.offset(), ts.offset() + name.length()));
                }
                parent = property;
            }
        } else {
            for (int i = 0; i < expression.size() - 1; ++i) {
                String name = expression.get(i++);
                while ((token.id() != JsTokenId.IDENTIFIER || token.id() != JsTokenId.IDENTIFIER || !token.text().toString().equals(name)) && ts.offset() > expRange.getStart() && ts.movePrevious()) {
                    token = ts.token();
                }
                if (parent == null || token.id() != JsTokenId.IDENTIFIER || !token.text().toString().equals(name)) continue;
                JsObject property = parent.getProperty(name);
                if (property != null) {
                    property.addOccurrence(new OffsetRange(ts.offset(), ts.offset() + name.length()));
                }
                parent = parent.getParent();
            }
        }
    }

    private void moveProperty(JsObject newParent, JsObject property) {
        JsObject newProperty = newParent.getProperty(property.getName());
        if (property.getParent() != null) {
            property.getParent().getProperties().remove(property.getName());
        }
        if (newProperty == null) {
            ((JsObjectImpl)property).setParent(newParent);
            newParent.addProperty(property.getName(), property);
        } else {
            if (property.isDeclared() && !newProperty.isDeclared()) {
                JsObject tmpProperty = newProperty;
                newParent.addProperty(property.getName(), property);
                ((JsObjectImpl)property).setParent(newParent);
                newProperty = property;
                property = tmpProperty;
            }
            JsObjectImpl.moveOccurrenceOfProperties((JsObjectImpl)newProperty, property);
            for (Occurrence occurrence : property.getOccurrences()) {
                newProperty.addOccurrence(occurrence.getOffsetRange());
            }
            ArrayList<? extends JsObject> propertiesToMove = new ArrayList<JsObject>(property.getProperties().values());
            for (JsObject propOfProperty : propertiesToMove) {
                this.moveProperty(newProperty, propOfProperty);
            }
            this.resolveLocalTypes(newProperty, this.parserResult.getDocumentationHolder());
        }
    }

    private void processWindowsProperties(JsObject globalObject) {
        JsObject window = globalObject.getProperty("window");
        if (window != null) {
            for (JsObject winProp : window.getProperties().values()) {
                JsObject globalVar = globalObject.getProperty(winProp.getName());
                if (globalVar == null) continue;
                JsObjectImpl.moveOccurrence((JsObjectImpl)globalVar, winProp);
                JsObjectImpl.moveOccurrenceOfProperties((JsObjectImpl)winProp, globalVar);
            }
        }
    }

    public JsObject getGlobalObject() {
        return this.getModelVisitor().getGlobalObject();
    }

    public synchronized void resolve() {
        if (this.visitor == null) {
            this.getModelVisitor();
        }
        if (this.resolveWithObjects) {
            this.getModelVisitor();
        }
    }

    public OccurrencesSupport getOccurrencesSupport() {
        return this.occurrencesSupport;
    }

    public Collection<? extends JsObject> getVariables(int offset) {
        ArrayList<JsObject> result = new ArrayList<JsObject>();
        for (DeclarationScope scope = ModelUtils.getDeclarationScope((Model)this, (int)offset); scope != null; scope = scope.getParentScope()) {
            for (JsObject object2 : ((JsObject)((Object)scope)).getProperties().values()) {
                if (object2.isAnonymous()) continue;
                result.add(object2);
            }
            for (JsObject object : ((JsFunction)scope).getParameters()) {
                result.add(object);
            }
        }
        return result;
    }

    private void resolveLocalTypes(JsObject object, JsDocumentationHolder docHolder) {
        if (object instanceof JsFunctionImpl) {
            ((JsFunctionImpl)object).resolveTypes(docHolder);
        } else {
            ((JsObjectImpl)object).resolveTypes(docHolder);
            if (object instanceof JsWith) {
                this.resolveWithObjects = true;
            }
        }
        ArrayList<? extends JsObject> copy = new ArrayList<JsObject>(object.getProperties().values());
        ArrayList<String> namesBefore = new ArrayList<String>(object.getProperties().keySet());
        Collections.reverse(copy);
        for (JsObject property : copy) {
            this.resolveLocalTypes(property, docHolder);
        }
        ArrayList<String> namesAfter = new ArrayList<String>(object.getProperties().keySet());
        for (String propertyName : namesAfter) {
            if (namesBefore.contains(propertyName)) continue;
            this.resolveLocalTypes(object.getProperty(propertyName), docHolder);
        }
    }

    public List<Identifier> getNodeName(Node node) {
        return ModelVisitor.getNodeName(node, this.parserResult);
    }

    public void writeModel(Printer printer) {
        Model.writeObject(printer, this.getGlobalObject(), null);
    }

    public void writeModel(Printer printer, boolean resolve) {
        Model.writeObject(printer, this.getGlobalObject(), resolve ? this.parserResult : null);
    }

    public void writeObject(Printer printer, JsObject object, boolean resolve) {
        Model.writeObject(printer, object, resolve ? this.parserResult : null);
    }

    public static void writeObject(Printer printer, JsObject object, @NullAllowed JsParserResult parseResult) {
        StringBuilder sb = new StringBuilder();
        Model.writeObject(printer, object, parseResult, sb, "", new HashSet<JsObject>());
        String rest = sb.toString();
        if (!rest.isEmpty()) {
            printer.println(rest);
        }
    }

    public static Collection<JsObject> readModel(BufferedReader reader, JsObject parent, @NullAllowed String sourceLabel, URL defaultDocUrl) throws IOException {
        String line = null;
        StringBuilder pushback = new StringBuilder();
        ArrayList<JsObject> ret = new ArrayList<JsObject>();
        while (pushback.length() > 0 || (line = reader.readLine()) != null) {
            if (pushback.length() > 0) {
                line = pushback.toString();
                pushback.setLength(0);
            }
            if (line.trim().isEmpty()) continue;
            ret.add(Model.readObject(parent, line, 0, reader, pushback, false, sourceLabel, defaultDocUrl == null ? null : Documentation.create((String)Bundle.LBL_DefaultDocContentForURL(), (URL)defaultDocUrl)));
        }
        return ret;
    }

    private static JsObject readObject(JsObject parent, String firstLine, int indent, BufferedReader reader, StringBuilder pushback, boolean parameter, String sourceLabel, Documentation defaultDoc) throws IOException {
        JsObject object = Model.readObject(parent, firstLine, parameter, sourceLabel);
        ParsingState state = null;
        String line = null;
        StringBuilder innerPushback = new StringBuilder();
        block6 : while (innerPushback.length() > 0 || (line = reader.readLine()) != null) {
            if (innerPushback.length() > 0) {
                line = innerPushback.toString();
                innerPushback.setLength(0);
            }
            if (line.length() < indent || !line.substring(0, indent).trim().isEmpty()) {
                pushback.append(line);
                break;
            }
            if ("# DOCUMENTATION URL".equals(line.trim())) {
                state = ParsingState.DOCUMETATION_URL;
                continue;
            }
            if ("# RETURN TYPES".equals(line.trim())) {
                state = ParsingState.RETURN;
                continue;
            }
            if ("# PARAMETERS".equals(line.trim())) {
                state = ParsingState.PARAMETER;
                continue;
            }
            if ("# PROPERTIES".equals(line.trim())) {
                state = ParsingState.PROPERTY;
                continue;
            }
            if ("# SEPARATOR".equals(line.trim())) break;
            if (state == null) {
                pushback.append(line);
                break;
            }
            switch (state) {
                case DOCUMETATION_URL: {
                    ((JsObjectImpl)object).setDocumentation(Documentation.create((String)Bundle.LBL_DefaultDocContentForURL(), (URL)new URL(line.trim())));
                    continue block6;
                }
                case RETURN: {
                    Matcher matcher = RETURN_TYPE_PATTERN.matcher(line.trim());
                    if (!matcher.matches()) {
                        throw new IOException("Unexpected line: " + line);
                    }
                    ((JsFunctionImpl)object).addReturnType(new TypeUsageImpl(matcher.group(1), -1, Boolean.parseBoolean(matcher.group(2))));
                    continue block6;
                }
                case PARAMETER: {
                    JsObject parameterObject = Model.readObject(object, line.trim(), indent + 8, reader, innerPushback, true, sourceLabel, null);
                    ((JsFunctionImpl)object).addParameter(parameterObject);
                    continue block6;
                }
                case PROPERTY: {
                    int index = line.indexOf(58);
                    assert (index > 0 && index < line.length());
                    String name = line.substring(0, index);
                    String value = line.substring(index + 1);
                    int newIndent = name.length();
                    name = name.trim();
                    JsObject property = Model.readObject(object, value.trim(), newIndent, reader, innerPushback, false, sourceLabel, defaultDoc);
                    object.addProperty(name, property);
                    continue block6;
                }
            }
            throw new IOException("Unexpected line: " + line);
        }
        if (defaultDoc != null && object.getDocumentation() == null) {
            ((JsObjectImpl)object).setDocumentation(defaultDoc);
        }
        return object;
    }

    private static JsObject readObject(JsObject parent, String line, boolean parameter, String sourceLabel) throws IOException {
        JsObjectImpl ret;
        Matcher m = OBJECT_PATTERN.matcher(line);
        if (!m.matches()) {
            throw new IOException("Malformed line: " + line);
        }
        boolean function = "FUNCTION".equals(m.group(1));
        String name = m.group(2);
        boolean anonymous = Boolean.valueOf(m.group(3));
        boolean declared = Boolean.valueOf(m.group(4));
        String strModifiers = m.group(8);
        JsElement.Kind kind = JsElement.Kind.valueOf(m.group(12));
        EnumSet<Modifier> modifiers = EnumSet.noneOf(Modifier.class);
        if (modifiers != null) {
            String[] parts;
            for (String part : parts = strModifiers.split(", ")) {
                modifiers.add(Modifier.valueOf((String)part));
            }
        }
        if (parameter) {
            ret = new ParameterObject(parent, new IdentifierImpl(name, OffsetRange.NONE), null, sourceLabel);
        } else if (function) {
            JsFunctionImpl functionImpl = new JsFunctionImpl(null, parent, new IdentifierImpl(name, OffsetRange.NONE), Collections.<Identifier>emptyList(), OffsetRange.NONE, null, sourceLabel);
            functionImpl.setAnonymous(anonymous);
            ret = functionImpl;
        } else {
            ret = anonymous ? new AnonymousObject(parent, name, OffsetRange.NONE, null, sourceLabel) : new JsObjectImpl(parent, new IdentifierImpl(name, OffsetRange.NONE), OffsetRange.NONE, null, sourceLabel);
        }
        ret.setJsKind(kind);
        ret.setDeclared(declared);
        ret.getModifiers().clear();
        for (Modifier modifier : modifiers) {
            ret.addModifier(modifier);
        }
        ret.getProperties().clear();
        return ret;
    }

    private static void writeObject(Printer printer, JsObject jsObject, JsParserResult parseResult, StringBuilder sb, String ident, Set<JsObject> path) {
        if (jsObject instanceof JsFunction) {
            sb.append("FUNCTION ");
        } else {
            sb.append("OBJECT ");
        }
        sb.append(jsObject.getName());
        sb.append(" [");
        sb.append("ANONYMOUS: ");
        sb.append(jsObject.isAnonymous());
        sb.append(", DECLARED: ");
        sb.append(jsObject.isDeclared());
        if (jsObject.getDeclarationName() != null) {
            sb.append(" - ").append(jsObject.getDeclarationName().getName());
        }
        if (!jsObject.getModifiers().isEmpty()) {
            sb.append(", MODIFIERS: ");
            for (Modifier m : jsObject.getModifiers()) {
                sb.append(m.toString());
                sb.append(", ");
            }
            sb.setLength(sb.length() - 2);
        }
        sb.append(", ");
        sb.append((Object)jsObject.getJSKind());
        sb.append("]");
        path.add(jsObject);
        if (jsObject instanceof JsFunction) {
            JsFunction function = (JsFunction)jsObject;
            if (!function.getReturnTypes().isEmpty()) {
                Model.newLine(printer, sb, ident);
                sb.append("# RETURN TYPES");
                Collection<? extends TypeUsage> ret = function.getReturnTypes();
                if (parseResult != null) {
                    ret = ModelUtils.resolveTypes(ret, parseResult, true);
                }
                ArrayList<? extends TypeUsage> returnTypes = new ArrayList<TypeUsage>(ret);
                Collections.sort(returnTypes, RETURN_TYPES_COMPARATOR);
                for (TypeUsage type : returnTypes) {
                    Model.newLine(printer, sb, ident);
                    sb.append(type.getType());
                    sb.append(", RESOLVED: ");
                    sb.append(type.isResolved());
                }
            }
            if (!function.getParameters().isEmpty()) {
                Model.newLine(printer, sb, ident);
                sb.append("# PARAMETERS");
                for (JsObject param : function.getParameters()) {
                    Model.newLine(printer, sb, ident);
                    if (path.contains(param)) {
                        sb.append("CYCLE ").append(param.getFullyQualifiedName());
                        continue;
                    }
                    Model.writeObject(printer, param, parseResult, sb, ident + "        ", path);
                }
            }
        }
        int length = 0;
        for (String str : jsObject.getProperties().keySet()) {
            if (str.length() <= length) continue;
            length = str.length();
        }
        StringBuilder identBuilder = new StringBuilder(ident);
        identBuilder.append(' ');
        boolean i = false;
        while (++i < length) {
            identBuilder.append(' ');
        }
        ArrayList<Map.Entry<String, ? extends JsObject>> entries = new ArrayList<Map.Entry<String, ? extends JsObject>>(jsObject.getProperties().entrySet());
        if (!entries.isEmpty()) {
            Model.newLine(printer, sb, ident);
            sb.append("# PROPERTIES");
            Collections.sort(entries, PROPERTIES_COMPARATOR);
            for (Map.Entry entry : entries) {
                Model.newLine(printer, sb, ident);
                sb.append((String)entry.getKey());
                for (int i2 = ((String)entry.getKey()).length(); i2 < length; ++i2) {
                    sb.append(' ');
                }
                sb.append(" : ");
                if (path.contains(entry.getValue())) {
                    sb.append("CYCLE ").append(((JsObject)entry.getValue()).getFullyQualifiedName());
                    continue;
                }
                Model.writeObject(printer, (JsObject)entry.getValue(), parseResult, sb, identBuilder.toString(), path);
            }
        }
        path.remove(jsObject);
    }

    private static void newLine(Printer printer, StringBuilder sb, String ident) {
        printer.println(sb.toString());
        sb.setLength(0);
        sb.append(ident);
    }

    public static interface Printer {
        public void println(String var1);
    }

    private static enum ParsingState {
        DOCUMETATION_URL,
        RETURN,
        PARAMETER,
        PROPERTY;
        

        private ParsingState() {
        }
    }

}

