/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import java.util.List;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocTypeDescribedElement;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public class ExtDocTypeNamedElement
extends ExtDocTypeDescribedElement {
    private final Identifier typeName;
    private final boolean optional;
    private final String defaultValue;

    private ExtDocTypeNamedElement(ExtDocElementType type, List<Type> declaredTypes, String description, Identifier typeName, boolean optional, String defaultValue) {
        super(type, declaredTypes, description);
        this.typeName = typeName;
        this.optional = optional;
        this.defaultValue = defaultValue;
    }

    public static ExtDocTypeNamedElement create(ExtDocElementType type, List<Type> declaredTypes, String description, Identifier typeName) {
        return new ExtDocTypeNamedElement(type, declaredTypes, description, typeName, false, "");
    }

    public static ExtDocTypeNamedElement create(ExtDocElementType type, List<Type> declaredTypes, String description, Identifier typeName, boolean optional) {
        return new ExtDocTypeNamedElement(type, declaredTypes, description, typeName, optional, "");
    }

    public static ExtDocTypeNamedElement create(ExtDocElementType type, List<Type> declaredTypes, String description, Identifier typeName, boolean optional, String defaultValue) {
        return new ExtDocTypeNamedElement(type, declaredTypes, description, typeName, optional, defaultValue);
    }

    public Identifier getTypeName() {
        return this.typeName;
    }

    @Override
    public Identifier getParamName() {
        return this.typeName;
    }

    @Override
    public String getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public boolean isOptional() {
        return this.optional;
    }
}

