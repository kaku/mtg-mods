/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.Block
 *  jdk.nashorn.internal.ir.DoWhileNode
 *  jdk.nashorn.internal.ir.ExecuteNode
 *  jdk.nashorn.internal.ir.ForNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.IfNode
 *  jdk.nashorn.internal.ir.LiteralNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.ObjectNode
 *  jdk.nashorn.internal.ir.ReturnNode
 *  jdk.nashorn.internal.ir.ThrowNode
 *  jdk.nashorn.internal.ir.VarNode
 *  jdk.nashorn.internal.ir.WhileNode
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  jdk.nashorn.internal.parser.TokenType
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$AstRule
 *  org.netbeans.modules.csl.api.Rule$UserConfigurableRule
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.Block;
import jdk.nashorn.internal.ir.DoWhileNode;
import jdk.nashorn.internal.ir.ExecuteNode;
import jdk.nashorn.internal.ir.ForNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IfNode;
import jdk.nashorn.internal.ir.LiteralNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.ObjectNode;
import jdk.nashorn.internal.ir.ReturnNode;
import jdk.nashorn.internal.ir.ThrowNode;
import jdk.nashorn.internal.ir.VarNode;
import jdk.nashorn.internal.ir.WhileNode;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import jdk.nashorn.internal.parser.TokenType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.embedding.JsEmbeddingProvider;
import org.netbeans.modules.javascript2.editor.hints.ArrayTrailingComma;
import org.netbeans.modules.javascript2.editor.hints.AssignmentInCondition;
import org.netbeans.modules.javascript2.editor.hints.BetterConditionHint;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.DuplicatePropertyName;
import org.netbeans.modules.javascript2.editor.hints.JsAstRule;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;
import org.netbeans.modules.javascript2.editor.hints.MissingSemicolonHint;
import org.netbeans.modules.javascript2.editor.hints.ObjectTrailingComma;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.PathNodeVisitor;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class JsConventionRule
extends JsAstRule {
    @Override
    void computeHints(JsHintsProvider.JsRuleContext context, List<Hint> hints, int offset, HintsProvider.HintsManager manager) {
        Map allHints = manager.getHints();
        List conventionHints = (List)allHints.get("jsconvention.option.hints");
        Rule.AstRule betterConditionRule = null;
        Rule.AstRule missingSemicolon = null;
        Rule.AstRule duplicatePropertyName = null;
        Rule.AstRule assignmentInCondition = null;
        Rule.AstRule objectTrailingComma = null;
        Rule.AstRule arrayTrailingComma = null;
        if (conventionHints != null) {
            for (Rule.AstRule astRule : conventionHints) {
                if (!manager.isEnabled((Rule.UserConfigurableRule)astRule)) continue;
                if (astRule instanceof BetterConditionHint) {
                    betterConditionRule = astRule;
                    continue;
                }
                if (astRule instanceof MissingSemicolonHint) {
                    missingSemicolon = astRule;
                    continue;
                }
                if (astRule instanceof DuplicatePropertyName) {
                    duplicatePropertyName = astRule;
                    continue;
                }
                if (astRule instanceof AssignmentInCondition) {
                    assignmentInCondition = astRule;
                    continue;
                }
                if (astRule instanceof ObjectTrailingComma) {
                    objectTrailingComma = astRule;
                    continue;
                }
                if (!(astRule instanceof ArrayTrailingComma)) continue;
                arrayTrailingComma = astRule;
            }
        }
        ConventionVisitor conventionVisitor = new ConventionVisitor((Rule)betterConditionRule, (Rule)missingSemicolon, (Rule)duplicatePropertyName, (Rule)assignmentInCondition, (Rule)objectTrailingComma, (Rule)arrayTrailingComma);
        conventionVisitor.process(context, hints);
    }

    public Set<?> getKinds() {
        return Collections.singleton("js.other.hints");
    }

    public String getId() {
        return "jsconvention.hint";
    }

    public String getDescription() {
        return Bundle.JsConventionHintDesc();
    }

    public String getDisplayName() {
        return Bundle.JsConventionHintDisplayName();
    }

    private static class ConventionVisitor
    extends PathNodeVisitor {
        private final Rule betterConditionRule;
        private final Rule missingSemicolon;
        private final Rule duplicatePropertyName;
        private final Rule assignmentInCondition;
        private final Rule objectTrailingComma;
        private final Rule arrayTrailingComma;
        private List<Hint> hints;
        private JsHintsProvider.JsRuleContext context;

        public ConventionVisitor(Rule betterCondition, Rule missingSemicolon, Rule duplicatePropertyName, Rule assignmentInCondition, Rule objectTrailingComma, Rule arrayTrailingComma) {
            this.betterConditionRule = betterCondition;
            this.missingSemicolon = missingSemicolon;
            this.duplicatePropertyName = duplicatePropertyName;
            this.assignmentInCondition = assignmentInCondition;
            this.objectTrailingComma = objectTrailingComma;
            this.arrayTrailingComma = arrayTrailingComma;
        }

        public void process(JsHintsProvider.JsRuleContext context, List<Hint> hints) {
            this.hints = hints;
            this.context = context;
            FunctionNode root = context.getJsParserResult().getRoot();
            if (root != null) {
                context.getJsParserResult().getRoot().accept((NodeVisitor)this);
            }
        }

        private void checkSemicolon(int offset) {
            if (this.missingSemicolon == null) {
                return;
            }
            int fileOffset = this.context.parserResult.getSnapshot().getOriginalOffset(offset);
            if (fileOffset == -1) {
                return;
            }
            TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(this.context.parserResult.getSnapshot(), offset);
            if (ts == null) {
                return;
            }
            ts.move(offset - 1);
            if (ts.moveNext() && ts.token().id() == JsTokenId.OPERATOR_SEMICOLON) {
                return;
            }
            ts.move(offset);
            if (ts.movePrevious() && ts.moveNext()) {
                Token<? extends JsTokenId> previous;
                Token<? extends JsTokenId> next;
                JsTokenId id = (JsTokenId)ts.token().id();
                if (id == JsTokenId.STRING_END && ts.moveNext()) {
                    id = (JsTokenId)ts.token().id();
                }
                if (id == JsTokenId.EOL) {
                    int position = ts.offset();
                    Token<? extends JsTokenId> next2 = LexUtilities.findNext(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL, JsTokenId.BLOCK_COMMENT, JsTokenId.LINE_COMMENT}));
                    id = (JsTokenId)next2.id();
                    if (id != JsTokenId.OPERATOR_SEMICOLON && id != JsTokenId.OPERATOR_COMMA && ts.movePrevious()) {
                        ts.move(position);
                        ts.moveNext();
                        id = (JsTokenId)ts.token().id();
                    }
                }
                if ((id == JsTokenId.EOL || id == JsTokenId.BRACKET_RIGHT_CURLY) && ts.movePrevious()) {
                    id = (JsTokenId)ts.token().id();
                }
                if ((id == JsTokenId.BLOCK_COMMENT || id == JsTokenId.LINE_COMMENT || id == JsTokenId.WHITESPACE) && (id = (JsTokenId)(next = LexUtilities.findNext(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL, JsTokenId.BLOCK_COMMENT, JsTokenId.LINE_COMMENT}))).id()) == JsTokenId.IDENTIFIER) {
                    ts.movePrevious();
                }
                if (id != JsTokenId.OPERATOR_SEMICOLON && id != JsTokenId.OPERATOR_COMMA && (id = (JsTokenId)(previous = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.EOL, JsTokenId.LINE_COMMENT}))).id()) != JsTokenId.OPERATOR_SEMICOLON && id != JsTokenId.OPERATOR_COMMA && !JsEmbeddingProvider.isGeneratedIdentifier(previous.text().toString()) && (fileOffset = this.context.parserResult.getSnapshot().getOriginalOffset(ts.offset())) >= 0) {
                    this.hints.add(new Hint(this.missingSemicolon, Bundle.MissingSemicolon(ts.token().text().toString()), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), new OffsetRange(fileOffset, fileOffset + ts.token().length()), null, 500));
                }
            } else if (!ts.moveNext() && ts.movePrevious() && ts.moveNext()) {
                fileOffset = this.context.parserResult.getSnapshot().getOriginalOffset(ts.offset());
                this.hints.add(new Hint(this.missingSemicolon, Bundle.MissingSemicolon(ts.token().text().toString()), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), new OffsetRange(fileOffset, fileOffset + ts.token().length()), null, 500));
            }
        }

        private void checkAssignmentInCondition(Node condition) {
            if (this.assignmentInCondition == null) {
                return;
            }
            if (condition instanceof BinaryNode) {
                BinaryNode binaryNode = (BinaryNode)condition;
                if (binaryNode.isAssignment()) {
                    this.hints.add(new Hint(this.assignmentInCondition, Bundle.AssignmentCondition(), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), ModelUtils.documentOffsetRange(this.context.getJsParserResult(), condition.getStart(), condition.getFinish()), null, 500));
                }
                if (binaryNode.lhs() instanceof BinaryNode) {
                    this.checkAssignmentInCondition(binaryNode.lhs());
                }
                if (binaryNode.rhs() instanceof BinaryNode) {
                    this.checkAssignmentInCondition(binaryNode.rhs());
                }
            }
        }

        private void checkCondition(BinaryNode binaryNode) {
            if (this.betterConditionRule == null) {
                return;
            }
            String message = null;
            switch (binaryNode.tokenType()) {
                case EQ: {
                    message = Bundle.ExpectedInstead("===", "==");
                    break;
                }
                case NE: {
                    message = Bundle.ExpectedInstead("!==", "!=");
                    break;
                }
            }
            if (message != null) {
                this.hints.add(new Hint(this.betterConditionRule, message, this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), ModelUtils.documentOffsetRange(this.context.getJsParserResult(), binaryNode.getStart(), binaryNode.getFinish()), null, 500));
            }
        }

        private void checkDuplicateLabels(ObjectNode objectNode) {
            if (this.duplicatePropertyName == null) {
                return;
            }
            int startOffset = this.context.parserResult.getSnapshot().getOriginalOffset(objectNode.getStart());
            int endOffset = this.context.parserResult.getSnapshot().getOriginalOffset(objectNode.getFinish());
            if (startOffset == -1 || endOffset == -1) {
                return;
            }
            TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(this.context.parserResult.getSnapshot(), objectNode.getStart());
            if (ts == null) {
                return;
            }
            ts.move(objectNode.getStart());
            State state = State.BEFORE_COLON;
            int curlyBalance = 0;
            int parenBalance = 0;
            int bracketBalance = 0;
            boolean isGetterSetter = false;
            if (ts.movePrevious() && ts.moveNext()) {
                HashSet<String> names = new HashSet<String>();
                block7 : while (ts.moveNext() && ts.offset() < objectNode.getFinish()) {
                    JsTokenId id = (JsTokenId)ts.token().id();
                    switch (state) {
                        case BEFORE_COLON: {
                            if (id == JsTokenId.IDENTIFIER || id == JsTokenId.STRING) {
                                int docOffset;
                                String name = ts.token().text().toString();
                                if (this.context.getJsParserResult().isEmbedded() && JsEmbeddingProvider.isGeneratedIdentifier(name)) continue block7;
                                if ("set".equals(name) || "get".equals(name)) {
                                    isGetterSetter = true;
                                    break;
                                }
                                if (names.add(name) || isGetterSetter || (docOffset = this.context.parserResult.getSnapshot().getOriginalOffset(ts.offset())) < 0) continue block7;
                                this.hints.add(new Hint(this.duplicatePropertyName, Bundle.DuplicateName(name), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), new OffsetRange(docOffset, docOffset + ts.token().length()), null, 500));
                                break;
                            }
                            if (id == JsTokenId.OPERATOR_COLON) {
                                state = State.AFTER_COLON;
                                break;
                            }
                            if (id != JsTokenId.BRACKET_LEFT_CURLY) break;
                            state = State.AFTER_CURLY;
                            isGetterSetter = false;
                            break;
                        }
                        case AFTER_COLON: {
                            if (id == JsTokenId.OPERATOR_COMMA) {
                                state = State.BEFORE_COLON;
                                break;
                            }
                            if (id == JsTokenId.BRACKET_LEFT_CURLY) {
                                state = State.AFTER_CURLY;
                                break;
                            }
                            if (id == JsTokenId.BRACKET_LEFT_PAREN) {
                                state = State.AFTER_PAREN;
                                break;
                            }
                            if (id != JsTokenId.BRACKET_LEFT_BRACKET) break;
                            state = State.AFTER_BRACKET;
                            break;
                        }
                        case AFTER_CURLY: {
                            if (id == JsTokenId.BRACKET_LEFT_CURLY) {
                                ++curlyBalance;
                                break;
                            }
                            if (id != JsTokenId.BRACKET_RIGHT_CURLY) break;
                            if (curlyBalance == 0) {
                                state = State.AFTER_COLON;
                                break;
                            }
                            --curlyBalance;
                            break;
                        }
                        case AFTER_PAREN: {
                            if (id == JsTokenId.BRACKET_LEFT_PAREN) {
                                ++parenBalance;
                                break;
                            }
                            if (id != JsTokenId.BRACKET_RIGHT_PAREN) break;
                            if (parenBalance == 0) {
                                state = State.AFTER_COLON;
                                break;
                            }
                            --parenBalance;
                            break;
                        }
                        case AFTER_BRACKET: {
                            if (id == JsTokenId.BRACKET_LEFT_BRACKET) {
                                ++bracketBalance;
                                break;
                            }
                            if (id != JsTokenId.BRACKET_RIGHT_BRACKET) break;
                            if (bracketBalance == 0) {
                                state = State.AFTER_COLON;
                                break;
                            }
                            --bracketBalance;
                        }
                    }
                }
            }
        }

        @Override
        public Node enter(DoWhileNode doWhileNode) {
            this.checkAssignmentInCondition(doWhileNode.getTest());
            return super.enter(doWhileNode);
        }

        @Override
        public Node enter(ForNode forNode) {
            this.checkAssignmentInCondition(forNode.getTest());
            return super.enter(forNode);
        }

        @Override
        public Node enter(IfNode ifNode) {
            this.checkAssignmentInCondition(ifNode.getTest());
            return super.enter(ifNode);
        }

        @Override
        public Node enter(WhileNode whileNode) {
            this.checkAssignmentInCondition(whileNode.getTest());
            return super.enter(whileNode);
        }

        @Override
        public Node enter(ExecuteNode executeNode) {
            if (!(executeNode.getExpression() instanceof Block)) {
                this.checkSemicolon(executeNode.getFinish());
            }
            return super.enter(executeNode);
        }

        @Override
        public Node enter(ThrowNode throwNode) {
            this.checkSemicolon(throwNode.getExpression().getFinish());
            return super.enter(throwNode);
        }

        @Override
        public Node enter(ObjectNode objectNode) {
            int offset;
            this.checkDuplicateLabels(objectNode);
            if (this.objectTrailingComma != null && (offset = this.context.parserResult.getSnapshot().getOriginalOffset(objectNode.getFinish())) > -1) {
                TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(this.context.parserResult.getSnapshot(), objectNode.getFinish());
                if (ts == null) {
                    return super.enter(objectNode);
                }
                ts.move(objectNode.getFinish());
                if (ts.movePrevious() && ts.moveNext() && ts.movePrevious()) {
                    LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.EOL, JsTokenId.WHITESPACE, JsTokenId.BRACKET_RIGHT_CURLY, JsTokenId.LINE_COMMENT, JsTokenId.BLOCK_COMMENT, JsTokenId.DOC_COMMENT}));
                    if (ts.token().id() == JsTokenId.OPERATOR_COMMA && (offset = this.context.parserResult.getSnapshot().getOriginalOffset(ts.offset())) >= 0) {
                        this.hints.add(new Hint(this.objectTrailingComma, Bundle.UnexpectedObjectTrailing(ts.token().text().toString()), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), new OffsetRange(offset, offset + ts.token().length()), null, 500));
                    }
                }
            }
            return super.enter(objectNode);
        }

        @Override
        public Node enter(LiteralNode literalNode) {
            int offset;
            if (this.arrayTrailingComma != null && literalNode.getValue() instanceof Node[] && (offset = this.context.parserResult.getSnapshot().getOriginalOffset(literalNode.getFinish())) > -1) {
                TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(this.context.parserResult.getSnapshot(), literalNode.getFinish());
                if (ts == null) {
                    return super.enter(literalNode);
                }
                ts.move(literalNode.getFinish());
                if (ts.movePrevious() && ts.moveNext() && ts.movePrevious()) {
                    LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.EOL, JsTokenId.WHITESPACE, JsTokenId.BRACKET_RIGHT_BRACKET, JsTokenId.LINE_COMMENT, JsTokenId.BLOCK_COMMENT, JsTokenId.DOC_COMMENT}));
                    if (ts.token().id() == JsTokenId.OPERATOR_COMMA && (offset = this.context.parserResult.getSnapshot().getOriginalOffset(ts.offset())) >= 0) {
                        this.hints.add(new Hint(this.arrayTrailingComma, Bundle.UnexpectedArrayTrailing(ts.token().text().toString()), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), new OffsetRange(offset, offset + ts.token().length()), null, 500));
                    }
                }
            }
            return super.enter(literalNode);
        }

        @Override
        public Node enter(VarNode varNode) {
            boolean check = true;
            Node previous = this.getPath().get(this.getPath().size() - 1);
            if (previous instanceof Block) {
                Block block = (Block)previous;
                if (block.getStatements().size() == 2 && block.getStatements().get(1) instanceof ForNode) {
                    check = false;
                }
            } else if (previous instanceof ForNode) {
                check = false;
            }
            if (check) {
                this.checkSemicolon(varNode.getFinish());
            }
            return super.enter(varNode);
        }

        @Override
        public Node enter(ReturnNode returnNode) {
            this.checkSemicolon(returnNode.getFinish());
            return super.enter(returnNode);
        }

        @Override
        public Node enter(BinaryNode binaryNode) {
            this.checkCondition(binaryNode);
            return super.enter(binaryNode);
        }

        private static enum State {
            BEFORE_COLON,
            AFTER_COLON,
            AFTER_CURLY,
            AFTER_PAREN,
            AFTER_BRACKET;
            

            private State() {
            }
        }

    }

}

