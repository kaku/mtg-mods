/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HtmlFormatter
 */
package org.netbeans.modules.javascript2.editor.jsdoc.completion;

import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;

public class AssingTag
extends AnnotationCompletionTag {
    public static final String TEMPLATE = " ${otherName} as ${thisName}";

    public AssingTag(String name) {
        super(name, name + " ${otherName} as ${thisName}");
    }

    @Override
    public void formatParameters(HtmlFormatter formatter) {
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("otherName");
        formatter.parameters(false);
        formatter.appendText(" as ");
        formatter.parameters(true);
        formatter.appendText("thisName");
        formatter.parameters(false);
    }
}

