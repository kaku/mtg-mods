/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.model;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String LBL_DefaultDocContentForURL() {
        return NbBundle.getMessage(Bundle.class, (String)"LBL_DefaultDocContentForURL");
    }

    private void Bundle() {
    }
}

