/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import java.util.List;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocTypeSimpleElement;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public class ExtDocTypeDescribedElement
extends ExtDocTypeSimpleElement {
    protected final String typeDescription;

    protected ExtDocTypeDescribedElement(ExtDocElementType type, List<Type> declaredTypes, String description) {
        super(type, declaredTypes);
        this.typeDescription = description;
    }

    public static ExtDocTypeDescribedElement create(ExtDocElementType type, List<Type> declaredTypes, String description) {
        return new ExtDocTypeDescribedElement(type, declaredTypes, description);
    }

    public String getTypeDescription() {
        return this.typeDescription;
    }

    @Override
    public Identifier getParamName() {
        return null;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOptional() {
        return false;
    }

    @Override
    public String getParamDescription() {
        return this.typeDescription;
    }

    @Override
    public List<Type> getParamTypes() {
        return this.declaredTypes;
    }
}

