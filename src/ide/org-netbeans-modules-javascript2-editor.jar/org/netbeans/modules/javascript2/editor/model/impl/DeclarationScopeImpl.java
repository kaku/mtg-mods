/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;

public class DeclarationScopeImpl
extends JsObjectImpl
implements DeclarationScope {
    private final DeclarationScope parentScope;
    private final List<DeclarationScope> childrenScopes;

    public DeclarationScopeImpl(DeclarationScope inScope, JsObject inObject, Identifier name, OffsetRange offsetRange, String mimeType, String sourceLabel) {
        super(inObject, name, offsetRange, mimeType, sourceLabel);
        this.parentScope = inScope;
        this.childrenScopes = new ArrayList<DeclarationScope>();
    }

    @Override
    public DeclarationScope getParentScope() {
        return this.parentScope;
    }

    @Override
    public Collection<? extends DeclarationScope> getChildrenScopes() {
        return this.childrenScopes;
    }

    protected void addDeclaredScope(DeclarationScope scope) {
        this.childrenScopes.add(scope);
    }

    private static class With {
        private final OffsetRange range;
        private final Collection<? extends TypeUsage> types;

        public With(OffsetRange range, Collection<? extends TypeUsage> types) {
            this.range = range;
            this.types = types;
        }

        public OffsetRange getRange() {
            return this.range;
        }

        public Collection<? extends TypeUsage> getTypes() {
            return this.types;
        }
    }

}

