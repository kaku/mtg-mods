/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsAstRule;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.openide.filesystems.FileObject;

public class GlobalIsNotDefined
extends JsAstRule {
    private static final List<String> KNOWN_GLOBAL_OBJECTS = Arrays.asList("window", "document", "console", "clearInterval", "clearTimeout", "event", "frames", "history", "Image", "location", "name", "navigator", "Option", "parent", "screen", "setInterval", "setTimeout", "XMLHttpRequest", "JSON", "Date", "undefined", "Math", "$", "jQuery", "Array", "Object", "Boolean", "null", "Number", "RegExp", "String", "undefined", "unresolved");

    @Override
    void computeHints(JsHintsProvider.JsRuleContext context, List<Hint> hints, int offset, HintsProvider.HintsManager manager) throws BadLocationException {
        JsObject globalObject = context.getJsParserResult().getModel().getGlobalObject();
        Collection<? extends JsObject> variables = ModelUtils.getVariables((DeclarationScope)((Object)globalObject));
        FileObject fo = context.parserResult.getSnapshot().getSource().getFileObject();
        JsIndex jsIndex = JsIndex.get(fo);
        for (JsObject variable : variables) {
            if (variable.isDeclared() || KNOWN_GLOBAL_OBJECTS.contains(variable.getName()) || variable.getJSKind() != JsElement.Kind.VARIABLE && variable.getJSKind() != JsElement.Kind.OBJECT) continue;
            String varName = variable.getName();
            Collection<? extends IndexResult> findByFqn = jsIndex.findByFqn("window." + varName, "bn");
            if (!findByFqn.isEmpty()) continue;
            if (variable.getOccurrences().isEmpty()) {
                this.addHint(context, hints, offset, varName, variable.getOffsetRange());
                continue;
            }
            for (Occurrence occurrence : variable.getOccurrences()) {
                this.addHint(context, hints, offset, varName, occurrence.getOffsetRange());
            }
        }
    }

    private void addHint(JsHintsProvider.JsRuleContext context, List<Hint> hints, int offset, String name, OffsetRange range) throws BadLocationException {
        boolean add = false;
        if (offset > -1) {
            Document document = context.getJsParserResult().getSnapshot().getSource().getDocument(false);
            if (document != null && document instanceof BaseDocument) {
                int lineOffsetRange;
                BaseDocument baseDocument = (BaseDocument)document;
                int lineOffset = Utilities.getLineOffset((BaseDocument)baseDocument, (int)offset);
                add = lineOffset == (lineOffsetRange = Utilities.getLineOffset((BaseDocument)baseDocument, (int)range.getStart()));
            }
        } else {
            add = true;
        }
        if (add) {
            hints.add(new Hint((Rule)this, Bundle.JsGlobalIsNotDefinedHintDesc(name), context.getJsParserResult().getSnapshot().getSource().getFileObject(), ModelUtils.documentOffsetRange(context.getJsParserResult(), range.getStart(), range.getEnd()), null, 500));
        }
    }

    public Set<?> getKinds() {
        return Collections.singleton("js.other.hints");
    }

    public String getId() {
        return "jsglobalisnotdefined.hint";
    }

    public String getDescription() {
        return Bundle.JsGlobalIsNotDefinedDesc();
    }

    public String getDisplayName() {
        return Bundle.JsGlobalIsNotDefinedDN();
    }

    @Override
    public HintSeverity getDefaultSeverity() {
        return HintSeverity.CURRENT_LINE_WARNING;
    }

    @Override
    public boolean getDefaultEnabled() {
        return true;
    }
}

