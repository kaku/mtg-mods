/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc;

import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;

public class ExtDocSyntaxProvider
implements SyntaxProvider {
    @Override
    public String typesSeparator() {
        return "/";
    }

    @Override
    public String paramTagTemplate() {
        return "@param {[type]} [name]";
    }

    @Override
    public String returnTagTemplate() {
        return "@return {[type]}";
    }

    @Override
    public String typeTagTemplate() {
        return "@type {[type]}";
    }
}

