/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsFunctionDocumentationRule;

public class UndocumentedParameterRule
extends JsFunctionDocumentationRule {
    @Override
    public String getId() {
        return "UndocumentedParameterRule.hint";
    }

    @Override
    public String getDescription() {
        return Bundle.UndocumentedParameterRuleDesc();
    }

    @Override
    public String getDisplayName() {
        return Bundle.UndocumentedParameterRuleDN();
    }
}

