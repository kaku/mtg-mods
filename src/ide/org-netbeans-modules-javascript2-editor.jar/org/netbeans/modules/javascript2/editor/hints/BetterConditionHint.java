/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsConventionHint;

public class BetterConditionHint
extends JsConventionHint {
    public String getDisplayName() {
        return Bundle.BetterConditionDisplayName();
    }

    public String getId() {
        return "jsbettercondition.hint";
    }

    public String getDescription() {
        return Bundle.BetterConditionDescription();
    }
}

