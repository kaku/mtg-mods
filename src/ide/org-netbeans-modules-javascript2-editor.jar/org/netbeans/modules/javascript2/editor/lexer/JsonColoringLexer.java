/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.spi.lexer.LexerInput
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.modules.javascript2.editor.lexer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;

@SuppressWarnings(value={"URF_UNREAD_FIELD", "DLS_DEAD_LOCAL_STORE", "DM_DEFAULT_ENCODING"})
public final class JsonColoringLexer {
    public static final int YYEOF = -1;
    private static final int ZZ_BUFFERSIZE = 16384;
    public static final int STRING = 2;
    public static final int ERROR = 6;
    public static final int YYINITIAL = 0;
    public static final int STRINGEND = 4;
    private static final int[] ZZ_LEXSTATE = new int[]{0, 0, 1, 1, 2, 2, 3, 3};
    private static final String ZZ_CMAP_PACKED = "\t\u0000\u0001\u0003\u0001\u0002\u0002\u0003\u0001\u0001\u0012\u0000\u0001\u0003\u0001\u0000\u0001\u001a\b\u0000\u0001\b\u0001\u0017\u0001\u0019\u0001\u0006\u0001\u0000\u0004\u001b\u0004\u0004\u0002\u0005\u0001\u0018\n\u0000\u0001\u0007\u0015\u0000\u0001\u0015\u0001\t\u0001\u0016\u0003\u0000\u0001\u000f\u0003\u0000\u0001\r\u0001\u000e\u0005\u0000\u0001\u0010\u0001\u0000\u0001\u0012\u0003\u0000\u0001\u000b\u0001\u0011\u0001\n\u0001\f\u0005\u0000\u0001\u0013\u0001\u0000\u0001\u0014\"\u0000\u0001\u0003\uff5f\u0000";
    private static final char[] ZZ_CMAP = JsonColoringLexer.zzUnpackCMap("\t\u0000\u0001\u0003\u0001\u0002\u0002\u0003\u0001\u0001\u0012\u0000\u0001\u0003\u0001\u0000\u0001\u001a\b\u0000\u0001\b\u0001\u0017\u0001\u0019\u0001\u0006\u0001\u0000\u0004\u001b\u0004\u0004\u0002\u0005\u0001\u0018\n\u0000\u0001\u0007\u0015\u0000\u0001\u0015\u0001\t\u0001\u0016\u0003\u0000\u0001\u000f\u0003\u0000\u0001\r\u0001\u000e\u0005\u0000\u0001\u0010\u0001\u0000\u0001\u0012\u0003\u0000\u0001\u000b\u0001\u0011\u0001\n\u0001\f\u0005\u0000\u0001\u0013\u0001\u0000\u0001\u0014\"\u0000\u0001\u0003\uff5f\u0000");
    private static final int[] ZZ_ACTION = JsonColoringLexer.zzUnpackAction();
    private static final String ZZ_ACTION_PACKED_0 = "\u0004\u0000\u0001\u0001\u0002\u0002\u0001\u0003\u0001\u0004\u0003\u0001\u0001\u0005\u0001\u0006\u0001\u0007\u0001\b\u0001\t\u0001\n\u0001\u000b\u0001\f\u0001\r\u0002\u000e\u0001\u0001\u0001\u000f\u0001\u0010\u0001\u0001\u0001\u000e\u0001\u0004\u0004\u0000\u0003\r\u0001\u0000\u0001\u0004\u0004\u0000\u0001\u0011\u0001\u0000\u0001\u0012\u0001\u0013";
    private static final int[] ZZ_ROWMAP = JsonColoringLexer.zzUnpackRowMap();
    private static final String ZZ_ROWMAP_PACKED_0 = "\u0000\u0000\u0000\u001c\u00008\u0000T\u0000p\u0000\u008c\u0000p\u0000\u00a8\u0000\u00c4\u0000\u00e0\u0000\u00fc\u0000\u0118\u0000p\u0000p\u0000p\u0000p\u0000p\u0000p\u0000p\u0000p\u0000\u0134\u0000\u0150\u0000p\u0000\u016c\u0000p\u0000p\u0000\u0188\u0000\u0188\u0000\u01a4\u0000\u01c0\u0000\u01dc\u0000\u01f8\u0000\u0214\u0000p\u0000\u0230\u0000\u024c\u0000\u0188\u0000\u0268\u0000\u0268\u0000\u0284\u0000\u02a0\u0000\u02bc\u0000p\u0000\u02d8\u0000p\u0000p";
    private static final int[] ZZ_TRANS = JsonColoringLexer.zzUnpackTrans();
    private static final String ZZ_TRANS_PACKED_0 = "\u0001\u0005\u0001\u0006\u0001\u0007\u0001\b\u0002\t\u0004\u0005\u0001\n\u0003\u0005\u0001\u000b\u0003\u0005\u0001\f\u0001\r\u0001\u000e\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0001\u0013\u0001\u0014\u0001\t\u0001\u0015\u0001\u0016\u0001\u0017\u0006\u0015\u0001\u0018\u0010\u0015\u0001\u0019\u0001\u0015\u001a\u0005\u0001\u001a\u0001\u0005\u0001\u001b\u0001\u001c\u0001\u0017\u0019\u001b\u001e\u0000\u0001\u0007\u001c\u0000\u0001\b\u001c\u0000\u0002\t\u0001\u001d\u0001\u001e\u0005\u0000\u0001\u001e\r\u0000\u0001\t\u000b\u0000\u0001\u001f\u001f\u0000\u0001 \u0018\u0000\u0001!\u000f\u0000\u0001\u0015\u0002\u0000\u0006\u0015\u0001\u0000\u0010\u0015\u0001\u0000\u0001\u0015\u0002\u0000\u0001\u0017\u0019\u0000\u0002\"\u0001\u0000\u0001\"\u0001#\u0016\"\u0001$\u0001%\u0001\u001c\u0001\u0017\u0019%\u0004\u0000\u0002\u001d\u0001\u0000\u0001\u001e\u0005\u0000\u0001\u001e\r\u0000\u0001\u001d\u0004\u0000\u0002&\u0002\u0000\u0001'\u0010\u0000\u0001'\u0001\u0000\u0001&\f\u0000\u0001(\u001f\u0000\u0001)\u001b\u0000\u0001*\u000f\u0000\u0001\"\u0016\u0000\u0001\"\u0004\u0000\u0001#\u0016\u0000\u0001#\u0004\u0000\u0002&\u0015\u0000\u0001&\r\u0000\u0001+\u001f\u0000\u0001,\u001a\u0000\u0001-\u0018\u0000\u0001.\u000e\u0000";
    private static final int ZZ_UNKNOWN_ERROR = 0;
    private static final int ZZ_NO_MATCH = 1;
    private static final int ZZ_PUSHBACK_2BIG = 2;
    private static final String[] ZZ_ERROR_MSG = new String[]{"Unkown internal scanner error", "Error: could not match input", "Error: pushback value was too large"};
    private static final int[] ZZ_ATTRIBUTE = JsonColoringLexer.zzUnpackAttribute();
    private static final String ZZ_ATTRIBUTE_PACKED_0 = "\u0004\u0000\u0001\t\u0001\u0001\u0001\t\u0005\u0001\b\t\u0002\u0001\u0001\t\u0001\u0001\u0002\t\u0003\u0001\u0004\u0000\u0001\t\u0002\u0001\u0001\u0000\u0001\u0001\u0004\u0000\u0001\t\u0001\u0000\u0002\t";
    private Reader zzReader;
    private int zzState;
    private int zzLexicalState = 0;
    private char[] zzBuffer = new char[16384];
    private int zzMarkedPos;
    private int zzCurrentPos;
    private int zzStartRead;
    private int zzEndRead;
    private int yyline;
    private int yychar;
    private int yycolumn;
    private boolean zzAtBOL = true;
    private boolean zzAtEOF;
    private boolean zzEOFDone;
    private LexerInput input;
    private boolean embedded;

    private static int[] zzUnpackAction() {
        int[] result = new int[46];
        int offset = 0;
        offset = JsonColoringLexer.zzUnpackAction("\u0004\u0000\u0001\u0001\u0002\u0002\u0001\u0003\u0001\u0004\u0003\u0001\u0001\u0005\u0001\u0006\u0001\u0007\u0001\b\u0001\t\u0001\n\u0001\u000b\u0001\f\u0001\r\u0002\u000e\u0001\u0001\u0001\u000f\u0001\u0010\u0001\u0001\u0001\u000e\u0001\u0004\u0004\u0000\u0003\r\u0001\u0000\u0001\u0004\u0004\u0000\u0001\u0011\u0001\u0000\u0001\u0012\u0001\u0013", offset, result);
        return result;
    }

    private static int zzUnpackAction(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do {
                result[j++] = value;
            } while (--count > 0);
        }
        return j;
    }

    private static int[] zzUnpackRowMap() {
        int[] result = new int[46];
        int offset = 0;
        offset = JsonColoringLexer.zzUnpackRowMap("\u0000\u0000\u0000\u001c\u00008\u0000T\u0000p\u0000\u008c\u0000p\u0000\u00a8\u0000\u00c4\u0000\u00e0\u0000\u00fc\u0000\u0118\u0000p\u0000p\u0000p\u0000p\u0000p\u0000p\u0000p\u0000p\u0000\u0134\u0000\u0150\u0000p\u0000\u016c\u0000p\u0000p\u0000\u0188\u0000\u0188\u0000\u01a4\u0000\u01c0\u0000\u01dc\u0000\u01f8\u0000\u0214\u0000p\u0000\u0230\u0000\u024c\u0000\u0188\u0000\u0268\u0000\u0268\u0000\u0284\u0000\u02a0\u0000\u02bc\u0000p\u0000\u02d8\u0000p\u0000p", offset, result);
        return result;
    }

    private static int zzUnpackRowMap(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int high = packed.charAt(i++) << 16;
            result[j++] = high | packed.charAt(i++);
        }
        return j;
    }

    private static int[] zzUnpackTrans() {
        int[] result = new int[756];
        int offset = 0;
        offset = JsonColoringLexer.zzUnpackTrans("\u0001\u0005\u0001\u0006\u0001\u0007\u0001\b\u0002\t\u0004\u0005\u0001\n\u0003\u0005\u0001\u000b\u0003\u0005\u0001\f\u0001\r\u0001\u000e\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0001\u0013\u0001\u0014\u0001\t\u0001\u0015\u0001\u0016\u0001\u0017\u0006\u0015\u0001\u0018\u0010\u0015\u0001\u0019\u0001\u0015\u001a\u0005\u0001\u001a\u0001\u0005\u0001\u001b\u0001\u001c\u0001\u0017\u0019\u001b\u001e\u0000\u0001\u0007\u001c\u0000\u0001\b\u001c\u0000\u0002\t\u0001\u001d\u0001\u001e\u0005\u0000\u0001\u001e\r\u0000\u0001\t\u000b\u0000\u0001\u001f\u001f\u0000\u0001 \u0018\u0000\u0001!\u000f\u0000\u0001\u0015\u0002\u0000\u0006\u0015\u0001\u0000\u0010\u0015\u0001\u0000\u0001\u0015\u0002\u0000\u0001\u0017\u0019\u0000\u0002\"\u0001\u0000\u0001\"\u0001#\u0016\"\u0001$\u0001%\u0001\u001c\u0001\u0017\u0019%\u0004\u0000\u0002\u001d\u0001\u0000\u0001\u001e\u0005\u0000\u0001\u001e\r\u0000\u0001\u001d\u0004\u0000\u0002&\u0002\u0000\u0001'\u0010\u0000\u0001'\u0001\u0000\u0001&\f\u0000\u0001(\u001f\u0000\u0001)\u001b\u0000\u0001*\u000f\u0000\u0001\"\u0016\u0000\u0001\"\u0004\u0000\u0001#\u0016\u0000\u0001#\u0004\u0000\u0002&\u0015\u0000\u0001&\r\u0000\u0001+\u001f\u0000\u0001,\u001a\u0000\u0001-\u0018\u0000\u0001.\u000e\u0000", offset, result);
        return result;
    }

    private static int zzUnpackTrans(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            int value = packed.charAt(i++);
            do {
                result[j++] = --value;
            } while (--count > 0);
        }
        return j;
    }

    private static int[] zzUnpackAttribute() {
        int[] result = new int[46];
        int offset = 0;
        offset = JsonColoringLexer.zzUnpackAttribute("\u0004\u0000\u0001\t\u0001\u0001\u0001\t\u0005\u0001\b\t\u0002\u0001\u0001\t\u0001\u0001\u0002\t\u0003\u0001\u0004\u0000\u0001\t\u0002\u0001\u0001\u0000\u0001\u0001\u0004\u0000\u0001\t\u0001\u0000\u0002\t", offset, result);
        return result;
    }

    private static int zzUnpackAttribute(String packed, int offset, int[] result) {
        int i = 0;
        int j = offset;
        int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do {
                result[j++] = value;
            } while (--count > 0);
        }
        return j;
    }

    public JsonColoringLexer(LexerRestartInfo info) {
        this.input = info.input();
        boolean bl = this.embedded = !"text/x-json".equals(info.languagePath().mimePath());
        if (info.state() != null) {
            this.setState((LexerState)info.state());
        } else {
            this.zzLexicalState = 0;
            this.zzState = 0;
        }
    }

    public LexerState getState() {
        if (this.zzState == 0 && this.zzLexicalState == 0) {
            return null;
        }
        return new LexerState(this.zzState, this.zzLexicalState);
    }

    public void setState(LexerState state) {
        this.zzState = state.zzState;
        this.zzLexicalState = state.zzLexicalState;
    }

    public JsTokenId nextToken() throws IOException {
        return this.yylex();
    }

    private JsTokenId getErrorToken() {
        if (this.embedded) {
            return JsTokenId.UNKNOWN;
        }
        return JsTokenId.ERROR;
    }

    public JsonColoringLexer(Reader in) {
        this.zzReader = in;
    }

    public JsonColoringLexer(InputStream in) {
        this(new InputStreamReader(in));
    }

    private static char[] zzUnpackCMap(String packed) {
        char[] map = new char[65536];
        int i = 0;
        int j = 0;
        while (i < 92) {
            int count = packed.charAt(i++);
            char value = packed.charAt(i++);
            do {
                map[j++] = value;
            } while (--count > 0);
        }
        return map;
    }

    private boolean zzRefill() throws IOException {
        int numRead;
        if (this.zzStartRead > 0) {
            System.arraycopy(this.zzBuffer, this.zzStartRead, this.zzBuffer, 0, this.zzEndRead - this.zzStartRead);
            this.zzEndRead -= this.zzStartRead;
            this.zzCurrentPos -= this.zzStartRead;
            this.zzMarkedPos -= this.zzStartRead;
            this.zzStartRead = 0;
        }
        if (this.zzCurrentPos >= this.zzBuffer.length) {
            char[] newBuffer = new char[this.zzCurrentPos * 2];
            System.arraycopy(this.zzBuffer, 0, newBuffer, 0, this.zzBuffer.length);
            this.zzBuffer = newBuffer;
        }
        if ((numRead = this.zzReader.read(this.zzBuffer, this.zzEndRead, this.zzBuffer.length - this.zzEndRead)) > 0) {
            this.zzEndRead += numRead;
            return false;
        }
        if (numRead == 0) {
            int c = this.zzReader.read();
            if (c == -1) {
                return true;
            }
            this.zzBuffer[this.zzEndRead++] = (char)c;
            return false;
        }
        return true;
    }

    public final void yyclose() throws IOException {
        this.zzAtEOF = true;
        this.zzEndRead = this.zzStartRead;
        if (this.zzReader != null) {
            this.zzReader.close();
        }
    }

    public final void yyreset(Reader reader) {
        this.zzReader = reader;
        this.zzAtBOL = true;
        this.zzAtEOF = false;
        this.zzEOFDone = false;
        this.zzStartRead = 0;
        this.zzEndRead = 0;
        this.zzMarkedPos = 0;
        this.zzCurrentPos = 0;
        this.yycolumn = 0;
        this.yychar = 0;
        this.yyline = 0;
        this.zzLexicalState = 0;
    }

    public final int yystate() {
        return this.zzLexicalState;
    }

    public final void yybegin(int newState) {
        this.zzLexicalState = newState;
    }

    public final String yytext() {
        return this.input.readText().toString();
    }

    public final char yycharat(int pos) {
        return this.input.readText().charAt(pos);
    }

    public final int yylength() {
        return this.input.readLength();
    }

    private void zzScanError(int errorCode) {
        String message;
        try {
            message = ZZ_ERROR_MSG[errorCode];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            message = ZZ_ERROR_MSG[0];
        }
        throw new Error(message);
    }

    public void yypushback(int number) {
        if (number > this.yylength()) {
            this.zzScanError(2);
        }
        this.input.backup(number);
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public JsTokenId yylex() throws IOException {
        zzEndReadL = this.zzEndRead;
        zzBufferL = this.zzBuffer;
        zzCMapL = JsonColoringLexer.ZZ_CMAP;
        zzTransL = JsonColoringLexer.ZZ_TRANS;
        zzRowMapL = JsonColoringLexer.ZZ_ROWMAP;
        zzAttrL = JsonColoringLexer.ZZ_ATTRIBUTE;
        block39 : do {
            zzMarkedPosL = this.zzMarkedPos;
            this.yychar += zzMarkedPosL - this.zzStartRead;
            zzAction = -1;
            tokenLength = 0;
            this.zzState = JsonColoringLexer.ZZ_LEXSTATE[this.zzLexicalState];
            do lbl-1000: // 3 sources:
            {
                if ((zzInput = this.input.read()) == -1) {
                    zzInput = -1;
                    break;
                }
                zzNext = zzTransL[zzRowMapL[this.zzState] + zzCMapL[zzInput]];
                if (zzNext == -1) break;
                this.zzState = zzNext;
                zzAttributes = zzAttrL[this.zzState];
                if ((zzAttributes & 1) != 1) ** GOTO lbl-1000
                zzAction = this.zzState;
                tokenLength = this.input.readLength();
            } while ((zzAttributes & 8) != 8);
            if (zzInput != -1) {
                this.input.backup(this.input.readLength() - tokenLength);
            }
            switch (zzAction < 0 ? zzAction : JsonColoringLexer.ZZ_ACTION[zzAction]) {
                case 11: {
                    return JsTokenId.OPERATOR_MINUS;
                }
                case 20: {
                    continue block39;
                }
                case 7: {
                    return JsTokenId.BRACKET_LEFT_BRACKET;
                }
                case 21: {
                    continue block39;
                }
                case 4: {
                    return JsTokenId.NUMBER;
                }
                case 22: {
                    continue block39;
                }
                case 18: {
                    return JsTokenId.KEYWORD_NULL;
                }
                case 23: {
                    continue block39;
                }
                case 10: {
                    return JsTokenId.OPERATOR_COLON;
                }
                case 24: {
                    continue block39;
                }
                case 8: {
                    return JsTokenId.BRACKET_RIGHT_BRACKET;
                }
                case 25: {
                    continue block39;
                }
                case 15: {
                    this.yypushback(1);
                    this.yybegin(4);
                    if (tokenLength - 1 > 0) {
                        return JsTokenId.STRING;
                    }
                }
                case 26: {
                    continue block39;
                }
                case 16: {
                    this.yybegin(0);
                    return JsTokenId.STRING_END;
                }
                case 27: {
                    continue block39;
                }
                case 12: {
                    this.yybegin(2);
                    return JsTokenId.STRING_BEGIN;
                }
                case 28: {
                    continue block39;
                }
                case 5: {
                    return JsTokenId.BRACKET_LEFT_CURLY;
                }
                case 29: {
                    continue block39;
                }
                case 14: {
                    this.yypushback(1);
                    this.yybegin(0);
                    if (tokenLength - 1 > 0) {
                        return this.getErrorToken();
                    }
                }
                case 30: {
                    continue block39;
                }
                case 3: {
                    return JsTokenId.WHITESPACE;
                }
                case 31: {
                    continue block39;
                }
                case 19: {
                    return JsTokenId.KEYWORD_FALSE;
                }
                case 32: {
                    continue block39;
                }
                case 9: {
                    return JsTokenId.OPERATOR_COMMA;
                }
                case 33: {
                    continue block39;
                }
                case 2: {
                    return JsTokenId.EOL;
                }
                case 34: {
                    continue block39;
                }
                case 1: {
                    return this.getErrorToken();
                }
                case 35: {
                    continue block39;
                }
                case 6: {
                    return JsTokenId.BRACKET_RIGHT_CURLY;
                }
                case 36: {
                    continue block39;
                }
                case 17: {
                    return JsTokenId.KEYWORD_TRUE;
                }
                case 37: {
                    continue block39;
                }
                case 13: 
                case 38: {
                    continue block39;
                }
            }
            if (zzInput == -1 && this.zzStartRead == this.zzCurrentPos) {
                this.zzAtEOF = true;
                if (this.input.readLength() <= 0) return null;
                this.input.backup(1);
                return this.getErrorToken();
            }
            this.zzScanError(1);
        } while (true);
    }

    public static final class LexerState {
        final int zzState;
        final int zzLexicalState;

        LexerState(int zzState, int zzLexicalState) {
            this.zzState = zzState;
            this.zzLexicalState = zzLexicalState;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            LexerState other = (LexerState)obj;
            if (this.zzState != other.zzState) {
                return false;
            }
            if (this.zzLexicalState != other.zzLexicalState) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 5;
            hash = 29 * hash + this.zzState;
            hash = 29 * hash + this.zzLexicalState;
            return hash;
        }

        public String toString() {
            return "LexerState{zzState=" + this.zzState + ", zzLexicalState=" + this.zzLexicalState + '}';
        }
    }

}

