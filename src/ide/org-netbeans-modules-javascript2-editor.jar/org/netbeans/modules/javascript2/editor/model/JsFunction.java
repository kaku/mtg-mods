/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;

public interface JsFunction
extends JsObject,
DeclarationScope {
    public Collection<? extends JsObject> getParameters();

    public JsObject getParameter(String var1);

    public void addReturnType(TypeUsage var1);

    public Collection<? extends TypeUsage> getReturnTypes();
}

