/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.spi.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsArray;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsArrayReference;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionReference;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelElementFactoryAccessor;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.openide.filesystems.FileObject;

public final class ModelElementFactory {
    private ModelElementFactory() {
    }

    public JsFunction newGlobalObject(FileObject fileObject, int length) {
        return JsFunctionImpl.createGlobal(fileObject, length, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public JsObject loadGlobalObject(FileObject fileObject, int length, String sourceLabel, URL defaultDocURL) throws IOException {
        InputStream is = fileObject.getInputStream();
        try {
            JsObject jsObject = this.loadGlobalObject(is, sourceLabel, defaultDocURL);
            return jsObject;
        }
        finally {
            is.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public JsObject loadGlobalObject(InputStream is, String sourceLabel, URL defaultDocURL) throws IOException {
        JsFunction global = this.newGlobalObject(null, Integer.MAX_VALUE);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        try {
            for (JsObject object : Model.readModel(reader, global, sourceLabel, defaultDocURL)) {
                this.putGlobalProperty(global, object);
            }
            JsFunction i$ = global;
            return i$;
        }
        finally {
            reader.close();
        }
    }

    public JsObject putGlobalProperty(JsFunction global, JsObject property) {
        JsObject wrapped2;
        JsObject wrapped2;
        if (property.getParent() != global) {
            throw new IllegalArgumentException("Property is not child of global");
        }
        if (property instanceof JsFunction) {
            GlobalFunction real = new GlobalFunction((JsFunction)property);
            real.setParentScope(global);
            real.setParent(global);
            wrapped2 = real;
        } else {
            GlobalObject real = new GlobalObject(property);
            real.setParent(global);
            wrapped2 = real;
        }
        global.addProperty(wrapped2.getName(), wrapped2);
        return wrapped2;
    }

    public JsObject newObject(JsObject parent, String name, OffsetRange offsetRange, boolean isDeclared) {
        return new JsObjectImpl(parent, new IdentifierImpl(name, offsetRange), offsetRange, isDeclared, null, null);
    }

    public JsFunction newFunction(DeclarationScope scope, JsObject parent, String name, Collection<String> params) {
        ArrayList<Identifier> realParams = new ArrayList<Identifier>();
        for (String param : params) {
            realParams.add(new IdentifierImpl(param, OffsetRange.NONE));
        }
        return new JsFunctionImpl(scope, parent, new IdentifierImpl(name, OffsetRange.NONE), realParams, OffsetRange.NONE, null, null);
    }

    public JsFunction newFunction(DeclarationScope scope, JsObject parent, Identifier name, List<Identifier> params, OffsetRange range) {
        return new JsFunctionImpl(scope, parent, name, params, range, null, null);
    }

    public JsObject newReference(JsObject parent, String name, OffsetRange offsetRange, JsObject original, boolean isDeclared, @NullAllowed Set<Modifier> modifiers) {
        if (original instanceof JsFunction) {
            return new JsFunctionReference(parent, new IdentifierImpl(name, offsetRange), (JsFunction)original, isDeclared, modifiers);
        }
        if (original instanceof JsArray) {
            return new JsArrayReference(parent, new IdentifierImpl(name, offsetRange), (JsArray)original, isDeclared, modifiers);
        }
        return new JsObjectReference(parent, new IdentifierImpl(name, offsetRange), original, isDeclared, modifiers);
    }

    public JsObject newReference(String name, JsObject original, boolean isDeclared) {
        if (original instanceof JsFunction) {
            return new OriginalParentFunctionReference(new IdentifierImpl(name, OffsetRange.NONE), (JsFunction)original, isDeclared);
        }
        if (original instanceof JsArray) {
            return new OriginalParentArrayReference(new IdentifierImpl(name, OffsetRange.NONE), (JsArray)original, isDeclared);
        }
        return new OriginalParentObjectReference(new IdentifierImpl(name, OffsetRange.NONE), original, isDeclared);
    }

    public TypeUsage newType(String name, int offset, boolean resolved) {
        return new TypeUsageImpl(name, offset, resolved);
    }

    static {
        ModelElementFactoryAccessor.setDefault(new ModelElementFactoryAccessor(){

            @Override
            public ModelElementFactory createModelElementFactory() {
                return new ModelElementFactory();
            }
        });
    }

    private static class GlobalFunction
    implements JsFunction {
        private final JsFunction delegate;
        private DeclarationScope inScope;
        private JsObject parent;

        public GlobalFunction(JsFunction delegate) {
            this.delegate = delegate;
            this.inScope = delegate.getParentScope();
            this.parent = delegate.getParent();
        }

        @Override
        public DeclarationScope getParentScope() {
            return this.inScope;
        }

        protected void setParentScope(DeclarationScope inScope) {
            this.inScope = inScope;
        }

        @Override
        public JsObject getParent() {
            return this.parent;
        }

        public void setParent(JsObject parent) {
            this.parent = parent;
        }

        @Override
        public JsObject getProperty(String name) {
            return this.delegate.getProperty(name);
        }

        @Override
        public Collection<? extends DeclarationScope> getChildrenScopes() {
            return this.delegate.getChildrenScopes();
        }

        @Override
        public Collection<? extends JsObject> getParameters() {
            return this.delegate.getParameters();
        }

        @Override
        public JsObject getParameter(String name) {
            return this.delegate.getParameter(name);
        }

        @Override
        public void addReturnType(TypeUsage type) {
            this.delegate.addReturnType(type);
        }

        @Override
        public Collection<? extends TypeUsage> getReturnTypes() {
            return this.delegate.getReturnTypes();
        }

        @Override
        public Identifier getDeclarationName() {
            return this.delegate.getDeclarationName();
        }

        @Override
        public Map<String, ? extends JsObject> getProperties() {
            return this.delegate.getProperties();
        }

        @Override
        public void addProperty(String name, JsObject property) {
            this.delegate.addProperty(name, property);
        }

        @Override
        public List<Occurrence> getOccurrences() {
            return this.delegate.getOccurrences();
        }

        @Override
        public void addOccurrence(OffsetRange offsetRange) {
            this.delegate.addOccurrence(offsetRange);
        }

        @Override
        public String getFullyQualifiedName() {
            return this.delegate.getFullyQualifiedName();
        }

        @Override
        public Collection<? extends TypeUsage> getAssignmentForOffset(int offset) {
            return this.delegate.getAssignmentForOffset(offset);
        }

        @Override
        public Collection<? extends TypeUsage> getAssignments() {
            return this.delegate.getAssignments();
        }

        @Override
        public void addAssignment(TypeUsage typeName, int offset) {
            this.delegate.addAssignment(typeName, offset);
        }

        @Override
        public boolean isAnonymous() {
            return this.delegate.isAnonymous();
        }

        @Override
        public boolean isDeprecated() {
            return this.delegate.isDeprecated();
        }

        @Override
        public boolean hasExactName() {
            return this.delegate.hasExactName();
        }

        @Override
        public Documentation getDocumentation() {
            return this.delegate.getDocumentation();
        }

        @Override
        public int getOffset() {
            return this.delegate.getOffset();
        }

        @Override
        public OffsetRange getOffsetRange() {
            return this.delegate.getOffsetRange();
        }

        @Override
        public JsElement.Kind getJSKind() {
            return this.delegate.getJSKind();
        }

        @Override
        public boolean isDeclared() {
            return this.delegate.isDeclared();
        }

        @Override
        public String getSourceLabel() {
            return this.delegate.getSourceLabel();
        }

        @Override
        public boolean isPlatform() {
            return this.delegate.isPlatform();
        }

        public FileObject getFileObject() {
            return this.delegate.getFileObject();
        }

        public String getMimeType() {
            return this.delegate.getMimeType();
        }

        public String getName() {
            return this.delegate.getName();
        }

        public String getIn() {
            return this.delegate.getIn();
        }

        public ElementKind getKind() {
            return this.delegate.getKind();
        }

        public Set<Modifier> getModifiers() {
            return this.delegate.getModifiers();
        }

        public boolean signatureEquals(ElementHandle handle) {
            return this.delegate.signatureEquals(handle);
        }

        public OffsetRange getOffsetRange(ParserResult result) {
            return this.delegate.getOffsetRange(result);
        }

        @Override
        public boolean containsOffset(int offset) {
            return this.delegate.containsOffset(offset);
        }
    }

    private static class GlobalObject
    implements JsObject {
        private final JsObject delegate;
        private JsObject parent;

        public GlobalObject(JsObject delegate) {
            this.delegate = delegate;
            this.parent = delegate.getParent();
        }

        @Override
        public JsObject getParent() {
            return this.parent;
        }

        public void setParent(JsObject parent) {
            this.parent = parent;
        }

        @Override
        public Identifier getDeclarationName() {
            return this.delegate.getDeclarationName();
        }

        @Override
        public Map<String, ? extends JsObject> getProperties() {
            return this.delegate.getProperties();
        }

        @Override
        public void addProperty(String name, JsObject property) {
            this.delegate.addProperty(name, property);
        }

        @Override
        public JsObject getProperty(String name) {
            return this.delegate.getProperty(name);
        }

        @Override
        public List<Occurrence> getOccurrences() {
            return this.delegate.getOccurrences();
        }

        @Override
        public void addOccurrence(OffsetRange offsetRange) {
            this.delegate.addOccurrence(offsetRange);
        }

        @Override
        public String getFullyQualifiedName() {
            return this.delegate.getFullyQualifiedName();
        }

        @Override
        public Collection<? extends TypeUsage> getAssignmentForOffset(int offset) {
            return this.delegate.getAssignmentForOffset(offset);
        }

        @Override
        public Collection<? extends TypeUsage> getAssignments() {
            return this.delegate.getAssignments();
        }

        @Override
        public void addAssignment(TypeUsage typeName, int offset) {
            this.delegate.addAssignment(typeName, offset);
        }

        @Override
        public boolean isAnonymous() {
            return this.delegate.isAnonymous();
        }

        @Override
        public boolean isDeprecated() {
            return this.delegate.isDeprecated();
        }

        @Override
        public boolean hasExactName() {
            return this.delegate.hasExactName();
        }

        @Override
        public Documentation getDocumentation() {
            return this.delegate.getDocumentation();
        }

        @Override
        public int getOffset() {
            return this.delegate.getOffset();
        }

        @Override
        public OffsetRange getOffsetRange() {
            return this.delegate.getOffsetRange();
        }

        @Override
        public JsElement.Kind getJSKind() {
            return this.delegate.getJSKind();
        }

        @Override
        public boolean isDeclared() {
            return this.delegate.isDeclared();
        }

        @Override
        public String getSourceLabel() {
            return this.delegate.getSourceLabel();
        }

        @Override
        public boolean isPlatform() {
            return this.delegate.isPlatform();
        }

        public FileObject getFileObject() {
            return this.delegate.getFileObject();
        }

        public String getMimeType() {
            return this.delegate.getMimeType();
        }

        public String getName() {
            return this.delegate.getName();
        }

        public String getIn() {
            return this.delegate.getIn();
        }

        public ElementKind getKind() {
            return this.delegate.getKind();
        }

        public Set<Modifier> getModifiers() {
            return this.delegate.getModifiers();
        }

        public boolean signatureEquals(ElementHandle handle) {
            return this.delegate.signatureEquals(handle);
        }

        public OffsetRange getOffsetRange(ParserResult result) {
            return this.delegate.getOffsetRange(result);
        }

        @Override
        public boolean containsOffset(int offset) {
            return this.delegate.containsOffset(offset);
        }
    }

    private static class OriginalParentObjectReference
    extends JsObjectReference {
        public OriginalParentObjectReference(Identifier declarationName, JsObject original, boolean isDeclared) {
            super(original.getParent(), declarationName, original, isDeclared, null);
        }

        @Override
        public JsObject getParent() {
            return this.getOriginal().getParent();
        }
    }

    private static class OriginalParentArrayReference
    extends JsArrayReference {
        public OriginalParentArrayReference(Identifier declarationName, JsArray original, boolean isDeclared) {
            super(original.getParent(), declarationName, original, isDeclared, null);
        }

        @Override
        public JsObject getParent() {
            return this.getOriginal().getParent();
        }
    }

    private static class OriginalParentFunctionReference
    extends JsFunctionReference {
        public OriginalParentFunctionReference(Identifier declarationName, JsFunction original, boolean isDeclared) {
            super(original.getParent(), declarationName, original, isDeclared, null);
        }

        @Override
        public JsObject getParent() {
            return this.getOriginal().getParent();
        }
    }

}

