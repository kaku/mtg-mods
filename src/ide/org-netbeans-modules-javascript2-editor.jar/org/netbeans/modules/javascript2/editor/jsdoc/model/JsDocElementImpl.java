/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;

public abstract class JsDocElementImpl
implements JsDocElement {
    private final JsDocElementType type;

    public JsDocElementImpl(JsDocElementType type) {
        this.type = type;
    }

    @Override
    public JsDocElementType getType() {
        return this.type;
    }
}

