/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collection;
import java.util.Set;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsArray;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;

public class JsArrayReference
extends JsObjectReference
implements JsArray {
    private final JsArray original;

    public JsArrayReference(JsObject parent, Identifier declarationName, JsArray original, boolean isDeclared, Set<Modifier> modifiers) {
        super(parent, declarationName, original, isDeclared, modifiers);
        this.original = original;
    }

    @Override
    public JsArray getOriginal() {
        return this.original;
    }

    @Override
    public Collection<? extends TypeUsage> getTypesInArray() {
        return this.original.getTypesInArray();
    }
}

