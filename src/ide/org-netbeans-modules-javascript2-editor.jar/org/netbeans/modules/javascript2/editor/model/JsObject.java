/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;

public interface JsObject
extends JsElement {
    public Identifier getDeclarationName();

    public Map<String, ? extends JsObject> getProperties();

    public void addProperty(String var1, JsObject var2);

    public JsObject getProperty(String var1);

    public JsObject getParent();

    public List<Occurrence> getOccurrences();

    public void addOccurrence(OffsetRange var1);

    public String getFullyQualifiedName();

    public Collection<? extends TypeUsage> getAssignmentForOffset(int var1);

    public Collection<? extends TypeUsage> getAssignments();

    public void addAssignment(TypeUsage var1, int var2);

    public boolean isAnonymous();

    public boolean isDeprecated();

    public boolean hasExactName();

    public Documentation getDocumentation();

    public boolean containsOffset(int var1);
}

