/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementImpl;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;

public class DescriptionElement
extends JsDocElementImpl {
    private final String description;

    private DescriptionElement(JsDocElementType type, String description) {
        super(type);
        this.description = description;
    }

    public static DescriptionElement create(JsDocElementType type, String description) {
        return new DescriptionElement(type, description);
    }

    public String getDescription() {
        return this.description;
    }
}

