/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.extdoc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsModifier;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocDescriptionElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocIdentSimpleElement;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;

public class ExtDocComment
extends JsComment {
    private final Map<ExtDocElementType, List<ExtDocElement>> tags = new EnumMap<ExtDocElementType, List<ExtDocElement>>(ExtDocElementType.class);

    public ExtDocComment(OffsetRange offsetRange, List<ExtDocElement> elements) {
        super(offsetRange);
        this.initComment(elements);
    }

    @Override
    public List<String> getSummary() {
        LinkedList<String> summaries = new LinkedList<String>();
        for (ExtDocElement sDocElement : this.getTagsForType(ExtDocElementType.DESCRIPTION)) {
            summaries.add(((ExtDocDescriptionElement)sDocElement).getDescription());
        }
        return summaries;
    }

    @Override
    public List<String> getSyntax() {
        return Collections.emptyList();
    }

    @Override
    public DocParameter getReturnType() {
        Iterator<? extends ExtDocElement> i$ = this.getTagsForTypes(new ExtDocElementType[]{ExtDocElementType.TYPE, ExtDocElementType.RETURN}).iterator();
        if (i$.hasNext()) {
            ExtDocElement sDocElement = i$.next();
            return (DocParameter)((Object)sDocElement);
        }
        return null;
    }

    @Override
    public List<DocParameter> getParameters() {
        LinkedList<DocParameter> params = new LinkedList<DocParameter>();
        for (ExtDocElement extDocElement : this.getTagsForTypes(new ExtDocElementType[]{ExtDocElementType.CFG, ExtDocElementType.PARAM})) {
            params.add((DocParameter)((Object)extDocElement));
        }
        return params;
    }

    @Override
    public String getDeprecated() {
        return null;
    }

    @Override
    public Set<JsModifier> getModifiers() {
        EnumSet<JsModifier> modifiers = EnumSet.noneOf(JsModifier.class);
        for (ExtDocElement extDocElement : this.getTagsForTypes(new ExtDocElementType[]{ExtDocElementType.PRIVATE, ExtDocElementType.STATIC})) {
            modifiers.add(JsModifier.fromString(extDocElement.getType().toString().substring(1)));
        }
        return modifiers;
    }

    @Override
    public List<DocParameter> getThrows() {
        return Collections.emptyList();
    }

    @Override
    public List<Type> getExtends() {
        LinkedList<Type> extendsList = new LinkedList<Type>();
        for (ExtDocElement extDocElement : this.getTagsForType(ExtDocElementType.EXTENDS)) {
            ExtDocIdentSimpleElement ident = (ExtDocIdentSimpleElement)extDocElement;
            extendsList.add(new TypeUsageImpl(ident.getIdentifier(), -1));
        }
        return extendsList;
    }

    @Override
    public List<String> getSee() {
        return Collections.emptyList();
    }

    @Override
    public String getSince() {
        return null;
    }

    @Override
    public boolean isClass() {
        return !this.getTagsForTypes(new ExtDocElementType[]{ExtDocElementType.CLASS, ExtDocElementType.CONSTRUCTOR}).isEmpty();
    }

    @Override
    public List<String> getExamples() {
        return Collections.emptyList();
    }

    private void initComment(List<ExtDocElement> elements) {
        for (ExtDocElement element : elements) {
            List<ExtDocElement> list = this.tags.get((Object)element.getType());
            if (list == null) {
                list = new LinkedList<ExtDocElement>();
                this.tags.put(element.getType(), list);
            }
            this.tags.get((Object)element.getType()).add(element);
        }
    }

    protected List<? extends ExtDocElement> getTags() {
        ArrayList<ExtDocElement> allTags = new ArrayList<ExtDocElement>();
        for (List<ExtDocElement> list : this.tags.values()) {
            allTags.addAll(list);
        }
        return allTags;
    }

    public List<? extends ExtDocElement> getTagsForType(ExtDocElementType type) {
        List tagsForType = this.tags.get((Object)type);
        return tagsForType == null ? Collections.emptyList() : tagsForType;
    }

    public List<? extends ExtDocElement> getTagsForTypes(ExtDocElementType[] types) {
        LinkedList<? extends ExtDocElement> list = new LinkedList<ExtDocElement>();
        for (ExtDocElementType type : types) {
            list.addAll(this.getTagsForType(type));
        }
        return list;
    }
}

