/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HtmlFormatter
 */
package org.netbeans.modules.javascript2.editor.extdoc.completion;

import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;

public class IdentDescribedTag
extends AnnotationCompletionTag {
    public static final String TEMPLATE = " ${identifier} ${description}";

    public IdentDescribedTag(String name) {
        super(name, name + " ${identifier} ${description}");
    }

    @Override
    public void formatParameters(HtmlFormatter formatter) {
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("identifier");
        formatter.parameters(false);
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("description");
        formatter.parameters(false);
    }
}

