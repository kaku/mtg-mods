/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HtmlFormatter
 */
package org.netbeans.modules.javascript2.editor.sdoc.completion;

import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;

public class IdentifierTag
extends AnnotationCompletionTag {
    public static final String TEMPLATE = " ${identifier}";

    public IdentifierTag(String name) {
        super(name, name + " ${identifier}");
    }

    @Override
    public void formatParameters(HtmlFormatter formatter) {
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("identifier");
        formatter.parameters(false);
    }
}

