/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsConventionHint;

public class ObjectTrailingComma
extends JsConventionHint {
    public String getId() {
        return "jsobjecttrailingcomma.hint";
    }

    public String getDescription() {
        return Bundle.ObjectTrailingCommaDescription();
    }

    public String getDisplayName() {
        return Bundle.ObjectTrailingCommaDisplayName();
    }
}

