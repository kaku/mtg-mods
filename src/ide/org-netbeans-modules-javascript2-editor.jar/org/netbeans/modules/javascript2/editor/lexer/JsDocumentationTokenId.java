/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.modules.javascript2.editor.lexer;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationLexer;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public enum JsDocumentationTokenId implements TokenId
{
    COMMENT_DOC_START(null, "COMMENT"),
    COMMENT_BLOCK_START(null, "COMMENT"),
    COMMENT_END(null, "COMMENT"),
    AT("@", "COMMENT"),
    ASTERISK("*", "COMMENT"),
    BRACKET_LEFT_BRACKET("[", "COMMENT"),
    BRACKET_RIGHT_BRACKET("]", "COMMENT"),
    BRACKET_LEFT_CURLY("{", "COMMENT"),
    BRACKET_RIGHT_CURLY("}", "COMMENT"),
    COMMA(",", "COMMENT"),
    EOL(null, "COMMENT"),
    HTML(null, "COMMENT_HTML"),
    WHITESPACE(null, "COMMENT"),
    KEYWORD(null, "COMMENT_KEYWORD"),
    UNKNOWN(null, "COMMENT"),
    OTHER(null, "COMMENT"),
    STRING(null, "COMMENT"),
    STRING_BEGIN(null, "COMMENT"),
    STRING_END(null, "COMMENT");
    
    public static final String MIME_TYPE = "text/javascript-doc";
    private final String fixedText;
    private final String primaryCategory;
    protected static final Language<JsDocumentationTokenId> LANGUAGE;

    private JsDocumentationTokenId(String fixedText, String primaryCategory) {
        this.fixedText = fixedText;
        this.primaryCategory = primaryCategory;
    }

    public String fixedText() {
        return this.fixedText;
    }

    public String primaryCategory() {
        return this.primaryCategory;
    }

    public static Language<JsDocumentationTokenId> language() {
        return LANGUAGE;
    }

    static {
        LANGUAGE = new LanguageHierarchy<JsDocumentationTokenId>(){

            protected String mimeType() {
                return "text/javascript-doc";
            }

            protected Collection<JsDocumentationTokenId> createTokenIds() {
                return EnumSet.allOf(JsDocumentationTokenId.class);
            }

            protected Map<String, Collection<JsDocumentationTokenId>> createTokenCategories() {
                HashMap<String, Collection<JsDocumentationTokenId>> cats = new HashMap<String, Collection<JsDocumentationTokenId>>();
                return cats;
            }

            protected Lexer<JsDocumentationTokenId> createLexer(LexerRestartInfo<JsDocumentationTokenId> info) {
                return JsDocumentationLexer.create(info);
            }

            protected LanguageEmbedding<?> embedding(Token<JsDocumentationTokenId> token, LanguagePath languagePath, InputAttributes inputAttributes) {
                return null;
            }
        }.language();
    }

}

