/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.extdoc;

import java.util.Map;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.extdoc.ExtDocComment;
import org.netbeans.modules.javascript2.editor.extdoc.ExtDocParser;
import org.netbeans.modules.parsing.api.Snapshot;

public class ExtDocDocumentationHolder
extends JsDocumentationHolder {
    private final Map<Integer, ExtDocComment> blocks;

    public ExtDocDocumentationHolder(JsDocumentationProvider provider, Snapshot snapshot) {
        super(provider, snapshot);
        this.blocks = ExtDocParser.parse(snapshot);
    }

    public Map getCommentBlocks() {
        return this.blocks;
    }
}

