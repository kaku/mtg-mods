/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.doc.api;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationFallbackSyntaxProvider;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationResolver;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;

public final class JsDocumentationSupport {
    public static final String DOCUMENTATION_PROVIDER_PATH = "javascript/doc/providers";
    private static Map<JsParserResult, WeakReference<JsDocumentationHolder>> providers = new WeakHashMap<JsParserResult, WeakReference<JsDocumentationHolder>>();

    private JsDocumentationSupport() {
    }

    @NonNull
    public static synchronized JsDocumentationHolder getDocumentationHolder(JsParserResult result) {
        if (!providers.containsKey((Object)result)) {
            JsDocumentationHolder holder = JsDocumentationSupport.createDocumentationHolder(result);
            providers.put(result, new WeakReference<JsDocumentationHolder>(holder));
            return holder;
        }
        JsDocumentationHolder holder = providers.get((Object)result).get();
        if (holder == null) {
            holder = JsDocumentationSupport.createDocumentationHolder(result);
            providers.put(result, new WeakReference<JsDocumentationHolder>(holder));
        }
        return holder;
    }

    @NonNull
    public static JsDocumentationProvider getDocumentationProvider(JsParserResult result) {
        return JsDocumentationResolver.getDefault().getDocumentationProvider(result.getSnapshot());
    }

    @NonNull
    public static SyntaxProvider getSyntaxProvider(JsParserResult parserResult) {
        SyntaxProvider syntaxProvider = parserResult.getDocumentationHolder().getProvider().getSyntaxProvider();
        return syntaxProvider != null ? syntaxProvider : new JsDocumentationFallbackSyntaxProvider();
    }

    @CheckForNull
    public static JsComment getCommentForOffset(JsParserResult result, int offset) {
        JsDocumentationHolder holder = JsDocumentationSupport.getDocumentationHolder(result);
        return holder.getCommentForOffset(offset, holder.getCommentBlocks());
    }

    private static JsDocumentationHolder createDocumentationHolder(JsParserResult result) {
        JsDocumentationProvider provider = JsDocumentationSupport.getDocumentationProvider(result);
        return provider.createDocumentationHolder(result.getSnapshot());
    }
}

