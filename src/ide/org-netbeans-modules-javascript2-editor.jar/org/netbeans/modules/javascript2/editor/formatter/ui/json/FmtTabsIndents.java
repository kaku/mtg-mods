/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.formatter.ui.json;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.io.IOException;
import java.io.InputStream;
import javax.accessibility.AccessibleContext;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.netbeans.modules.javascript2.editor.formatter.FmtOptions;
import org.netbeans.modules.javascript2.editor.formatter.Utils;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtTabsIndents
extends JPanel {
    private JTextField continuationIndentSizeField;
    private JLabel continuationIndentSizeLabel;
    private JLabel initialIndentLabel;
    private JTextField initialIndentSizeField;

    public FmtTabsIndents() {
        this.initComponents();
        this.continuationIndentSizeField.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "continuationIndentSize");
        this.initialIndentSizeField.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "init.indent");
    }

    public static PreferencesCustomizer.Factory getController() {
        String preview = "";
        try {
            preview = Utils.loadPreviewText(FmtTabsIndents.class.getClassLoader().getResourceAsStream("org/netbeans/modules/javascript2/editor/formatter/ui/json/TabsIndents.json"));
        }
        catch (IOException ex) {
            // empty catch block
        }
        return new FmtOptions.CategorySupport.Factory("text/x-json", "tabs-and-indents", FmtTabsIndents.class, preview, {"text-limit-width", "30"}, {"init.indent", "0"});
    }

    private void initComponents() {
        this.continuationIndentSizeLabel = new JLabel();
        this.continuationIndentSizeField = new JTextField();
        this.initialIndentLabel = new JLabel();
        this.initialIndentSizeField = new JTextField();
        this.setName(NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_TabsAndIndents"));
        this.setOpaque(false);
        this.continuationIndentSizeLabel.setLabelFor(this.continuationIndentSizeField);
        Mnemonics.setLocalizedText((JLabel)this.continuationIndentSizeLabel, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"LBL_ContinuationIndentSize"));
        this.initialIndentLabel.setLabelFor(this.initialIndentSizeField);
        Mnemonics.setLocalizedText((JLabel)this.initialIndentLabel, (String)NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.initialIndentLabel.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.continuationIndentSizeLabel, -1, 205, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.continuationIndentSizeField, -2, 36, -2)).addGroup(layout.createSequentialGroup().addComponent(this.initialIndentLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.initialIndentSizeField, -2, 36, -2)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(12, 12, 12).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.initialIndentLabel).addComponent(this.initialIndentSizeField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.continuationIndentSizeLabel).addComponent(this.continuationIndentSizeField, -2, -1, -2)).addContainerGap(-1, 32767)));
        this.continuationIndentSizeLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.continuationIndentSizeLabel.AccessibleContext.accessibleName"));
        this.continuationIndentSizeLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.continuationIndentSizeLabel.AccessibleContext.accessibleDescription"));
        this.continuationIndentSizeField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.continuationIndentSizeField.AccessibleContext.accessibleName"));
        this.continuationIndentSizeField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.continuationIndentSizeField.AccessibleContext.accessibleDescription"));
        this.initialIndentLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.initialIndentLabel.AccessibleContext.accessibleName"));
        this.initialIndentLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.initialIndentLabel.AccessibleContext.accessibleDescription"));
        this.initialIndentSizeField.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.initialIndentSizeField.AccessibleContext.accessibleName"));
        this.initialIndentSizeField.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.initialIndentSizeField.AccessibleContext.accessibleDescription"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtTabsIndents.class, (String)"FmtTabsIndents.AccessibleContext.accessibleDescription"));
    }
}

