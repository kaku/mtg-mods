/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectInformation
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.IndexSearcher
 *  org.netbeans.modules.csl.api.IndexSearcher$Descriptor
 *  org.netbeans.modules.csl.api.IndexSearcher$Helper
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.modules.parsing.spi.indexing.PathRecognizer
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.javascript2.editor.navigation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.swing.Icon;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.parsing.spi.indexing.PathRecognizer;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;

public class JsIndexSearcher
implements IndexSearcher {
    public Set<? extends IndexSearcher.Descriptor> getTypes(Project project, String textForQuery, QuerySupport.Kind searchType, IndexSearcher.Helper helper) {
        HashSet<JsDescriptor> result = new HashSet<JsDescriptor>();
        JsIndex index = this.getIndex(project);
        if (index != null) {
            Collection<? extends IndexResult> indexResults = index.query("bn", textForQuery, searchType, JsIndex.TERMS_BASIC_INFO);
            for (IndexResult indexResult : indexResults) {
                IndexedElement element = IndexedElement.create(indexResult);
                if (element.getJSKind() != JsElement.Kind.CONSTRUCTOR && element.getJSKind() != JsElement.Kind.OBJECT_LITERAL) continue;
                result.add(new JsDescriptor(helper, element));
            }
        }
        return result;
    }

    public Set<? extends IndexSearcher.Descriptor> getSymbols(Project project, String textForQuery, QuerySupport.Kind searchType, IndexSearcher.Helper helper) {
        HashSet<JsDescriptor> result = new HashSet<JsDescriptor>();
        JsIndex index = this.getIndex(project);
        if (index != null) {
            Collection<? extends IndexResult> indexResults = index.query("bn", textForQuery, searchType, JsIndex.TERMS_BASIC_INFO);
            for (IndexResult indexResult : indexResults) {
                result.add(new JsDescriptor(helper, IndexedElement.create(indexResult)));
            }
        }
        return result;
    }

    private JsIndex getIndex(Project project) {
        HashSet sourceIds = new HashSet();
        HashSet libraryIds = new HashSet();
        Collection lookupAll = Lookup.getDefault().lookupAll(PathRecognizer.class);
        for (PathRecognizer pathRecognizer : lookupAll) {
            Set library;
            Set source = pathRecognizer.getSourcePathIds();
            if (source != null) {
                sourceIds.addAll(source);
            }
            if ((library = pathRecognizer.getLibraryPathIds()) == null) continue;
            libraryIds.addAll(library);
        }
        Collection findRoots = QuerySupport.findRoots((Project)project, sourceIds, libraryIds, Collections.emptySet());
        return JsIndex.get(findRoots);
    }

    private static class JsDescriptor
    extends IndexSearcher.Descriptor {
        private final IndexSearcher.Helper helper;
        private final IndexedElement element;
        private String projectName;
        private Icon projectIcon;

        public JsDescriptor(IndexSearcher.Helper helper, IndexedElement element) {
            this.helper = helper;
            this.element = element;
            this.projectName = null;
        }

        public ElementHandle getElement() {
            return this.element;
        }

        public String getSimpleName() {
            return this.element.getName();
        }

        public String getOuterName() {
            return null;
        }

        public String getTypeName() {
            return this.element.getName();
        }

        public String getContextName() {
            StringBuilder sb = new StringBuilder();
            FileObject file = this.getFileObject();
            if (file != null) {
                sb.append(FileUtil.getFileDisplayName((FileObject)file));
            }
            if (sb.length() > 0) {
                return sb.toString();
            }
            return null;
        }

        public Icon getIcon() {
            return this.helper.getIcon((ElementHandle)this.element);
        }

        public String getProjectName() {
            if (this.projectName == null) {
                this.initProjectInfo();
            }
            return this.projectName;
        }

        public Icon getProjectIcon() {
            if (this.projectName == null) {
                this.initProjectInfo();
            }
            return this.projectIcon;
        }

        public FileObject getFileObject() {
            return this.element.getFileObject();
        }

        public int getOffset() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void open() {
            FileObject fileObject = this.element.getFileObject();
            if (fileObject != null) {
                GsfUtilities.open((FileObject)fileObject, (int)this.element.getOffset(), (String)this.element.getName());
            }
        }

        private void initProjectInfo() {
            Project p;
            FileObject fo = this.element.getFileObject();
            if (fo != null && (p = FileOwnerQuery.getOwner((FileObject)fo)) != null) {
                ProjectInformation pi = ProjectUtils.getInformation((Project)p);
                this.projectName = pi.getDisplayName();
                this.projectIcon = pi.getIcon();
            }
            if (this.projectName == null) {
                this.projectName = "";
            }
        }
    }

}

