/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;

public interface JsDocElement {
    public JsDocElementType getType();

    public static enum Category {
        ASSIGN,
        DECLARATION,
        DESCRIPTION,
        LINK,
        NAMED_PARAMETER,
        SIMPLE,
        UNKNOWN,
        UNNAMED_PARAMETER;
        

        private Category() {
        }
    }

}

