/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import java.util.List;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.ParameterElement;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;

public class NamedParameterElement
extends ParameterElement
implements DocParameter {
    private final Identifier paramName;
    private final boolean optional;
    private final String defaultValue;

    private NamedParameterElement(JsDocElementType type, Identifier paramName, List<Type> paramTypes, String paramDescription, boolean optional, String defaultValue) {
        super(type, paramTypes, paramDescription);
        this.paramName = paramName;
        this.optional = optional;
        this.defaultValue = defaultValue;
    }

    public static NamedParameterElement create(JsDocElementType type, Identifier paramName, List<Type> paramTypes, String paramDescription, boolean optional, String defaultValue) {
        return new NamedParameterElement(type, paramName, paramTypes, paramDescription, optional, defaultValue);
    }

    public static NamedParameterElement create(JsDocElementType type, Identifier paramName, List<Type> paramTypes, String paramDescription, boolean optional) {
        return new NamedParameterElement(type, paramName, paramTypes, paramDescription, optional, null);
    }

    public static NamedParameterElement create(JsDocElementType type, Identifier paramName, List<Type> paramTypes, String paramDescription) {
        return new NamedParameterElement(type, paramName, paramTypes, paramDescription, false, null);
    }

    public static NamedParameterElement createWithNameDiagnostics(JsDocElementType type, Identifier paramName, List<Type> paramTypes, String paramDescription) {
        int nameStartOffset = paramName.getOffsetRange().getStart();
        String name = paramName.getName();
        boolean optional = name.matches("\\[.*\\]");
        String defaultValue = null;
        if (optional) {
            ++nameStartOffset;
            int indexOfEqual = (name = name.substring(1, name.length() - 1)).indexOf("=");
            if (indexOfEqual != -1) {
                defaultValue = name.substring(indexOfEqual + 1);
                name = name.substring(0, indexOfEqual);
            }
        }
        return new NamedParameterElement(type, new IdentifierImpl(name, nameStartOffset), paramTypes, paramDescription, optional, defaultValue);
    }

    @Override
    public Identifier getParamName() {
        return this.paramName;
    }

    @Override
    public String getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public boolean isOptional() {
        return this.optional;
    }
}

