/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.modules.javascript2.editor;

import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.modules.javascript2.editor.classpath.ClassPathProviderImpl;
import org.openide.modules.ModuleInstall;

public class ModuleInstaller
extends ModuleInstall {
    public void restored() {
        ClassPathProviderImpl.getBootClassPath();
    }

    public void uninstalled() {
        GlobalPathRegistry.getDefault().unregister("classpath/javascript-boot", new ClassPath[]{ClassPathProviderImpl.getBootClassPath()});
    }
}

