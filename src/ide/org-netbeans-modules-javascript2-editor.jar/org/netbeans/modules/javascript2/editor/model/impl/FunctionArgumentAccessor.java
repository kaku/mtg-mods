/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.List;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionArgument;

public abstract class FunctionArgumentAccessor {
    private static volatile FunctionArgumentAccessor DEFAULT;

    public static FunctionArgumentAccessor getDefault() {
        block3 : {
            FunctionArgumentAccessor a = DEFAULT;
            if (a != null) {
                return a;
            }
            Class<FunctionArgument> c = FunctionArgument.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError(ex);
            }
        }
        return DEFAULT;
    }

    public static void setDefault(FunctionArgumentAccessor accessor) {
        if (DEFAULT != null) {
            throw new IllegalStateException();
        }
        DEFAULT = accessor;
    }

    public abstract FunctionArgument createForAnonymousObject(int var1, int var2, JsObject var3);

    public abstract FunctionArgument createForString(int var1, int var2, String var3);

    public abstract FunctionArgument createForReference(int var1, int var2, List<String> var3);

    public abstract FunctionArgument createForArray(int var1, int var2, JsObject var3);

    public abstract FunctionArgument createForUnknown(int var1);
}

