/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  jdk.nashorn.internal.parser.TokenType
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import jdk.nashorn.internal.parser.TokenType;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsAstRule;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.PathNodeVisitor;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class WeirdAssignment
extends JsAstRule {
    @Override
    void computeHints(JsHintsProvider.JsRuleContext context, List<Hint> hints, int offset, HintsProvider.HintsManager manager) {
        WeirdVisitor conventionVisitor = new WeirdVisitor((Rule)this);
        conventionVisitor.process(context, hints);
    }

    public Set<?> getKinds() {
        return Collections.singleton("js.other.hints");
    }

    public String getId() {
        return "jsweirdassignment.hint";
    }

    public String getDescription() {
        return Bundle.JsWeirdAssignmentDesc();
    }

    public String getDisplayName() {
        return Bundle.JsWeirdAssignmentDN();
    }

    private static class WeirdVisitor
    extends PathNodeVisitor {
        private List<Hint> hints;
        private JsHintsProvider.JsRuleContext context;
        private final Rule rule;

        public WeirdVisitor(Rule rule) {
            this.rule = rule;
        }

        public void process(JsHintsProvider.JsRuleContext context, List<Hint> hints) {
            this.hints = hints;
            this.context = context;
            FunctionNode root = context.getJsParserResult().getRoot();
            if (root != null) {
                context.getJsParserResult().getRoot().accept((NodeVisitor)this);
            }
        }

        @Override
        public Node enter(BinaryNode binaryNode) {
            if (binaryNode.tokenType() == TokenType.ASSIGN && binaryNode.lhs().toString().equals(binaryNode.rhs().toString())) {
                this.hints.add(new Hint(this.rule, Bundle.JsWeirdAssignmentDN(), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), ModelUtils.documentOffsetRange(this.context.getJsParserResult(), binaryNode.getStart(), binaryNode.getFinish()), null, 500));
            }
            return super.enter(binaryNode);
        }
    }

}

