/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.spi.model;

import java.util.List;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.impl.FunctionArgumentAccessor;

public final class FunctionArgument {
    private final Kind kind;
    private final int order;
    private final int offset;
    private final Object value;

    private FunctionArgument(Kind kind, int order, int offset, Object value) {
        this.kind = kind;
        this.order = order;
        this.offset = offset;
        this.value = value;
    }

    public Kind getKind() {
        return this.kind;
    }

    public int getOrder() {
        return this.order;
    }

    public int getOffset() {
        return this.offset;
    }

    public Object getValue() {
        return this.value;
    }

    static {
        FunctionArgumentAccessor.setDefault(new FunctionArgumentAccessor(){

            @Override
            public FunctionArgument createForAnonymousObject(int order, int offset, JsObject value) {
                return new FunctionArgument(Kind.ANONYMOUS_OBJECT, order, offset, value);
            }

            @Override
            public FunctionArgument createForArray(int order, int offset, JsObject value) {
                return new FunctionArgument(Kind.ARRAY, order, offset, value);
            }

            @Override
            public FunctionArgument createForString(int order, int offset, String value) {
                return new FunctionArgument(Kind.STRING, order, offset, value);
            }

            @Override
            public FunctionArgument createForReference(int order, int offset, List<String> value) {
                return new FunctionArgument(Kind.REFERENCE, order, offset, value);
            }

            @Override
            public FunctionArgument createForUnknown(int order) {
                return new FunctionArgument(Kind.UNKNOWN, order, -1, null);
            }
        });
    }

    public static enum Kind {
        STRING,
        REFERENCE,
        ANONYMOUS_OBJECT,
        ARRAY,
        UNKNOWN;
        

        private Kind() {
        }
    }

}

