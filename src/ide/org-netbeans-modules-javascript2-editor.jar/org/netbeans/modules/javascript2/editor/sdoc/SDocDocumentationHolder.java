/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.sdoc;

import java.util.Map;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.sdoc.SDocComment;
import org.netbeans.modules.javascript2.editor.sdoc.SDocParser;
import org.netbeans.modules.parsing.api.Snapshot;

public class SDocDocumentationHolder
extends JsDocumentationHolder {
    private final Map<Integer, SDocComment> blocks;

    public SDocDocumentationHolder(JsDocumentationProvider provider, Snapshot snapshot) {
        super(provider, snapshot);
        this.blocks = SDocParser.parse(snapshot);
    }

    public Map getCommentBlocks() {
        return this.blocks;
    }
}

