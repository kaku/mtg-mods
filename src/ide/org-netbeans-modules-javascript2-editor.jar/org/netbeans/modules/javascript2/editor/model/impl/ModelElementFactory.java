/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.LiteralNode
 *  jdk.nashorn.internal.ir.LiteralNode$ArrayLiteralNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.ObjectNode
 *  jdk.nashorn.internal.ir.PropertyNode
 *  jdk.nashorn.internal.parser.Token
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.LiteralNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.ObjectNode;
import jdk.nashorn.internal.ir.PropertyNode;
import jdk.nashorn.internal.parser.Token;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.embedding.JsEmbeddingProvider;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.impl.AnonymousObject;
import org.netbeans.modules.javascript2.editor.model.impl.DeclarationScopeImpl;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsArrayImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelBuilder;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

class ModelElementFactory {
    ModelElementFactory() {
    }

    @CheckForNull
    static JsFunctionImpl create(JsParserResult parserResult, FunctionNode functionNode, List<Identifier> fqName, ModelBuilder modelBuilder, boolean isAnnonymous, JsObject parent) {
        JsFunctionImpl result;
        JsDocumentationHolder docHolder;
        if (JsEmbeddingProvider.containsGeneratedIdentifier(fqName.get(fqName.size() - 1).getName())) {
            return null;
        }
        JsObjectImpl inObject = modelBuilder.getCurrentObject();
        JsObjectImpl globalObject = modelBuilder.getGlobal();
        JsObject parentObject = parent;
        if (parent == null) {
            if (isAnnonymous) {
                JsObjectImpl decScope;
                for (decScope = modelBuilder.getCurrentDeclarationScope(); decScope != null && decScope.isAnonymous(); decScope = (DeclarationScopeImpl)decScope.getParentScope()) {
                }
                parentObject = decScope == null ? globalObject : decScope;
            } else {
                parentObject = inObject;
            }
            while (parentObject.getParent() != null && parentObject.getModifiers().contains((Object)Modifier.PROTECTED)) {
                parentObject = parentObject.getParent();
            }
        }
        int start = Token.descPosition((long)functionNode.getFirstToken());
        int end = Token.descPosition((long)functionNode.getLastToken()) + Token.descLength((long)functionNode.getLastToken());
        ArrayList<Identifier> parameters = new ArrayList<Identifier>(functionNode.getParameters().size());
        for (IdentNode node : functionNode.getParameters()) {
            IdentifierImpl param = ModelElementFactory.create(parserResult, node);
            if (param == null) continue;
            parameters.add(param);
        }
        if (fqName.size() > 1) {
            List<Identifier> objectName = fqName.subList(0, fqName.size() - 1);
            parentObject = isAnnonymous ? globalObject : ModelUtils.getJsObject(modelBuilder, objectName, false);
            result = new JsFunctionImpl(modelBuilder.getCurrentDeclarationFunction(), parentObject, fqName.get(fqName.size() - 1), parameters, new OffsetRange(start, end), parserResult.getSnapshot().getMimeType(), null);
            if (parentObject instanceof JsFunction && !"prototype".equals(parentObject.getName())) {
                result.addModifier(Modifier.STATIC);
            }
        } else {
            result = new JsFunctionImpl(modelBuilder.getCurrentDeclarationFunction(), parentObject, fqName.get(fqName.size() - 1), parameters, new OffsetRange(start, end), parserResult.getSnapshot().getMimeType(), null);
        }
        String propertyName = result.getDeclarationName().getName();
        if (parentObject == null) {
            parentObject = globalObject;
        }
        JsObject property = parentObject.getProperty(propertyName);
        parentObject.addProperty(result.getDeclarationName().getName(), result);
        if (property != null) {
            if (property.getDeclarationName() != null) {
                result.addOccurrence(property.getDeclarationName().getOffsetRange());
            }
            for (Occurrence occurrence : property.getOccurrences()) {
                result.addOccurrence(occurrence.getOffsetRange());
            }
        }
        if ((docHolder = parserResult.getDocumentationHolder()) != null) {
            result.setDocumentation(docHolder.getDocumentation((Node)functionNode));
        }
        result.setAnonymous(isAnnonymous);
        return result;
    }

    @NonNull
    static JsFunctionImpl createVirtualFunction(JsParserResult parserResult, JsObject parentObject, Identifier name, int paramCount) {
        ArrayList<Identifier> params = new ArrayList<Identifier>(paramCount);
        if (paramCount == 1) {
            params.add(new IdentifierImpl("param", OffsetRange.NONE));
        } else {
            for (int i = 0; i < paramCount; ++i) {
                params.add(new IdentifierImpl("param" + (i + 1), OffsetRange.NONE));
            }
        }
        JsFunctionImpl virtual = new JsFunctionImpl(parserResult.getSnapshot().getSource().getFileObject(), parentObject, name, params, parserResult.getSnapshot().getMimeType(), null);
        if (virtual.hasExactName()) {
            virtual.addOccurrence(name.getOffsetRange());
        }
        return virtual;
    }

    @CheckForNull
    static IdentifierImpl create(JsParserResult parserResult, IdentNode node) {
        return ModelElementFactory.create(parserResult, node.getName(), node.getStart(), node.getFinish());
    }

    @CheckForNull
    static IdentifierImpl create(JsParserResult parserResult, LiteralNode node) {
        return ModelElementFactory.create(parserResult, node.getString(), node.getStart(), node.getFinish());
    }

    @CheckForNull
    static IdentifierImpl create(JsParserResult parserResult, String name, int start, int end) {
        if (JsEmbeddingProvider.containsGeneratedIdentifier(name)) {
            return null;
        }
        return new IdentifierImpl(name, new OffsetRange(start, end));
    }

    @CheckForNull
    static JsObjectImpl create(JsParserResult parserResult, ObjectNode objectNode, List<Identifier> fqName, ModelBuilder modelBuilder, boolean belongsToParent) {
        JsDocumentationHolder docHolder;
        if (JsEmbeddingProvider.containsGeneratedIdentifier(fqName.get(fqName.size() - 1).getName())) {
            return null;
        }
        JsObjectImpl scope = modelBuilder.getCurrentObject();
        JsObject parent = scope;
        JsObject result = null;
        Identifier name = fqName.get(fqName.size() - 1);
        if (!belongsToParent) {
            List<Identifier> objectName = fqName.size() > 1 ? fqName.subList(0, fqName.size() - 1) : fqName;
            parent = ModelUtils.getJsObject(modelBuilder, objectName, false);
            if (parent != null) {
                parent = parent.getParent();
            }
            if (parent == null) {
                parent = modelBuilder.getGlobal();
            }
        }
        result = parent.getProperty(name.getName());
        JsObjectImpl newObject = new JsObjectImpl(parent, name, new OffsetRange(objectNode.getStart(), objectNode.getFinish()), parserResult.getSnapshot().getMimeType(), null);
        newObject.setDeclared(true);
        if (result != null) {
            for (String propertyName : result.getProperties().keySet()) {
                newObject.addProperty(propertyName, result.getProperty(propertyName));
            }
        }
        if ((docHolder = parserResult.getDocumentationHolder()) != null) {
            newObject.setDeprecated(docHolder.isDeprecated((Node)objectNode));
            newObject.setDocumentation(docHolder.getDocumentation((Node)objectNode));
        }
        parent.addProperty(name.getName(), newObject);
        if (newObject.hasExactName()) {
            newObject.addOccurrence(newObject.getDeclarationName().getOffsetRange());
        }
        return newObject;
    }

    @CheckForNull
    static JsArrayImpl create(JsParserResult parserResult, LiteralNode.ArrayLiteralNode aNode, List<Identifier> fqName, ModelBuilder modelBuilder, boolean belongsToParent, JsObject suggestedParent) {
        JsDocumentationHolder docHolder;
        if (JsEmbeddingProvider.containsGeneratedIdentifier(fqName.get(fqName.size() - 1).getName())) {
            return null;
        }
        JsObject parent = suggestedParent != null ? suggestedParent : modelBuilder.getCurrentObject();
        JsObject result = null;
        Identifier name = fqName.get(fqName.size() - 1);
        if (!belongsToParent) {
            List<Identifier> objectName = fqName.size() > 1 ? fqName.subList(0, fqName.size() - 1) : fqName;
            parent = ModelUtils.getJsObject(modelBuilder, objectName, false);
        }
        result = parent.getProperty(name.getName());
        JsArrayImpl newObject = new JsArrayImpl(parent, name, new OffsetRange(aNode.getStart(), aNode.getFinish()), parserResult.getSnapshot().getMimeType(), null);
        newObject.setDeclared(true);
        if (result != null) {
            for (String propertyName : result.getProperties().keySet()) {
                newObject.addProperty(propertyName, result.getProperty(propertyName));
            }
            for (Occurrence occurence : result.getOccurrences()) {
                newObject.addOccurrence(occurence.getOffsetRange());
            }
        }
        if ((docHolder = parserResult.getDocumentationHolder()) != null) {
            newObject.setDeprecated(docHolder.isDeprecated((Node)aNode));
            newObject.setDocumentation(docHolder.getDocumentation((Node)aNode));
        }
        parent.addProperty(name.getName(), newObject);
        if (newObject.hasExactName()) {
            newObject.addOccurrence(newObject.getDeclarationName().getOffsetRange());
        }
        return newObject;
    }

    @NonNull
    static JsObjectImpl createAnonymousObject(JsParserResult parserResult, ObjectNode objectNode, ModelBuilder modelBuilder) {
        String name = modelBuilder.getUnigueNameForAnonymObject(parserResult);
        AnonymousObject result = new AnonymousObject(modelBuilder.getCurrentDeclarationFunction(), name, new OffsetRange(objectNode.getStart(), objectNode.getFinish()), parserResult.getSnapshot().getMimeType(), null);
        modelBuilder.getCurrentDeclarationFunction().addProperty(name, result);
        JsDocumentationHolder docHolder = parserResult.getDocumentationHolder();
        if (docHolder != null) {
            result.setDocumentation(docHolder.getDocumentation((Node)objectNode));
            result.setDeprecated(docHolder.isDeprecated((Node)objectNode));
        }
        return result;
    }

    @NonNull
    static JsArrayImpl createAnonymousObject(JsParserResult parserResult, LiteralNode.ArrayLiteralNode aNode, ModelBuilder modelBuilder) {
        String name = modelBuilder.getUnigueNameForAnonymObject(parserResult);
        AnonymousObject.AnonymousArray result = new AnonymousObject.AnonymousArray(modelBuilder.getCurrentDeclarationFunction(), name, new OffsetRange(aNode.getStart(), aNode.getFinish()), parserResult.getSnapshot().getMimeType(), null);
        modelBuilder.getCurrentDeclarationFunction().addProperty(name, result);
        JsDocumentationHolder docHolder = parserResult.getDocumentationHolder();
        if (docHolder != null) {
            result.setDocumentation(docHolder.getDocumentation((Node)aNode));
            result.setDeprecated(docHolder.isDeprecated((Node)aNode));
        }
        return result;
    }

    @CheckForNull
    static JsObjectImpl create(JsParserResult parserResult, PropertyNode propertyNode, Identifier name, ModelBuilder modelBuilder, boolean belongsToParent) {
        if (JsEmbeddingProvider.containsGeneratedIdentifier(name.getName())) {
            return null;
        }
        JsObjectImpl scope = modelBuilder.getCurrentObject();
        JsObjectImpl property = new JsObjectImpl(scope, name, name.getOffsetRange(), parserResult.getSnapshot().getMimeType(), null);
        JsDocumentationHolder docHolder = parserResult.getDocumentationHolder();
        property.setDocumentation(docHolder.getDocumentation((Node)propertyNode));
        property.setDeprecated(docHolder.isDeprecated((Node)propertyNode));
        if (property.hasExactName()) {
            property.addOccurrence(property.getDeclarationName().getOffsetRange());
        }
        return property;
    }
}

