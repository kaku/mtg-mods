/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocBaseElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public class ExtDocTypeSimpleElement
extends ExtDocBaseElement
implements DocParameter {
    protected final List<Type> declaredTypes;

    protected ExtDocTypeSimpleElement(ExtDocElementType type, List<Type> declaredTypes) {
        super(type);
        this.declaredTypes = declaredTypes;
    }

    public static ExtDocTypeSimpleElement create(ExtDocElementType type, List<Type> declaredTypes) {
        return new ExtDocTypeSimpleElement(type, declaredTypes);
    }

    public List<Type> getDeclaredTypes() {
        return this.declaredTypes;
    }

    @Override
    public Identifier getParamName() {
        return null;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOptional() {
        return false;
    }

    @Override
    public String getParamDescription() {
        return "";
    }

    @Override
    public List<Type> getParamTypes() {
        return this.declaredTypes;
    }
}

