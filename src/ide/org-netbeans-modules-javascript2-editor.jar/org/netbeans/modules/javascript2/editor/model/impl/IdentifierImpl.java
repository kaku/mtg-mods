/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.Identifier;

public class IdentifierImpl
implements Identifier {
    private String name;
    private OffsetRange offsetRange;

    public IdentifierImpl(String name, OffsetRange offsetRange) {
        this.name = name;
        this.offsetRange = offsetRange;
    }

    public IdentifierImpl(String name, int startOffset) {
        this(name, startOffset >= 0 ? new OffsetRange(startOffset, startOffset + name.length()) : OffsetRange.NONE);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public OffsetRange getOffsetRange() {
        return this.offsetRange;
    }

    public String toString() {
        return "IdentifierImpl{name=" + this.name + ", offsetRange=" + (Object)this.offsetRange + '}';
    }

    public int hashCode() {
        int hash = 3;
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        IdentifierImpl other = (IdentifierImpl)obj;
        if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
            return false;
        }
        if (!(this.offsetRange == other.offsetRange || this.offsetRange != null && this.offsetRange.equals((Object)other.offsetRange))) {
            return false;
        }
        return true;
    }
}

