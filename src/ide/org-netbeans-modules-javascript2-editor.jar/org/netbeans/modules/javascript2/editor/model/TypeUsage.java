/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model;

import org.netbeans.modules.javascript2.editor.model.Type;

public interface TypeUsage
extends Type {
    public boolean isResolved();
}

