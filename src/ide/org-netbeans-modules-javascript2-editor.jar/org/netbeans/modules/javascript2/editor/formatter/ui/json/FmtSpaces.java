/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.formatter.ui.json;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.netbeans.modules.javascript2.editor.formatter.Defaults;
import org.netbeans.modules.javascript2.editor.formatter.FmtOptions;
import org.netbeans.modules.javascript2.editor.formatter.Utils;
import org.netbeans.modules.javascript2.editor.formatter.ui.json.FmtTabsIndents;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.util.NbBundle;

public class FmtSpaces
extends JPanel
implements TreeCellRenderer,
MouseListener,
KeyListener {
    private SpacesCategorySupport scs;
    private DefaultTreeModel model;
    private DefaultTreeCellRenderer dr = new DefaultTreeCellRenderer();
    private JCheckBox renderer = new JCheckBox();
    private JTree cfgTree;
    private JScrollPane jScrollPane1;

    private FmtSpaces() {
        this.initComponents();
        this.model = this.createModel();
        this.cfgTree.setModel(this.model);
        this.cfgTree.setRootVisible(false);
        this.cfgTree.setShowsRootHandles(true);
        this.cfgTree.setCellRenderer(this);
        this.cfgTree.setEditable(false);
        this.cfgTree.addMouseListener(this);
        this.cfgTree.addKeyListener(this);
        this.dr.setIcon(null);
        this.dr.setOpenIcon(null);
        this.dr.setClosedIcon(null);
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)this.model.getRoot();
        for (int i = root.getChildCount(); i >= 0; --i) {
            this.cfgTree.expandRow(i);
        }
        Dimension dimension = new Dimension((int)this.cfgTree.getPreferredSize().getWidth() + 30, (int)this.jScrollPane1.getMinimumSize().getHeight());
        this.jScrollPane1.setMinimumSize(dimension);
    }

    public static PreferencesCustomizer.Factory getController() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                String preview = "";
                try {
                    preview = Utils.loadPreviewText(FmtTabsIndents.class.getClassLoader().getResourceAsStream("org/netbeans/modules/javascript2/editor/formatter/ui/json/Spaces.json"));
                }
                catch (IOException ex) {
                    // empty catch block
                }
                return new SpacesCategorySupport(Defaults.getInstance("text/x-json"), preferences, new FmtSpaces(), preview);
            }
        };
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.cfgTree = new JTree();
        this.setName(NbBundle.getMessage(FmtSpaces.class, (String)"LBL_Spaces"));
        this.setOpaque(false);
        this.setLayout(new GridBagLayout());
        this.cfgTree.setRootVisible(false);
        this.jScrollPane1.setViewportView(this.cfgTree);
        this.cfgTree.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtSpaces.class, (String)"FmtSpaces.cfgTree.AccessibleContext.accessibleName"));
        this.cfgTree.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtSpaces.class, (String)"FmtSpaces.cfgTree.AccessibleContext.accessibleDescription"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jScrollPane1, gridBagConstraints);
        this.jScrollPane1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtSpaces.class, (String)"FmtSpaces.jScrollPane1.AccessibleContext.accessibleName"));
        this.jScrollPane1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtSpaces.class, (String)"FmtSpaces.jScrollPane1.AccessibleContext.accessibleDescription"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtSpaces.class, (String)"FmtSpaces.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtSpaces.class, (String)"FmtSpaces.AccessibleContext.accessibleDescription"));
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        this.renderer.setBackground(selected ? this.dr.getBackgroundSelectionColor() : this.dr.getBackgroundNonSelectionColor());
        this.renderer.setForeground(selected ? this.dr.getTextSelectionColor() : this.dr.getTextNonSelectionColor());
        this.renderer.setEnabled(true);
        Object data = ((DefaultMutableTreeNode)value).getUserObject();
        if (data instanceof Item) {
            Item item = (Item)data;
            if (((DefaultMutableTreeNode)value).getAllowsChildren()) {
                Component c = this.dr.getTreeCellRendererComponent(tree, value, leaf, expanded, leaf, row, hasFocus);
                return c;
            }
        } else {
            Component c = this.dr.getTreeCellRendererComponent(tree, value, leaf, expanded, leaf, row, hasFocus);
            return c;
        }
        this.renderer.setText(item.displayName);
        this.renderer.setSelected(item.value);
        return this.renderer;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Rectangle r;
        Point p = e.getPoint();
        TreePath path = this.cfgTree.getPathForLocation(e.getPoint().x, e.getPoint().y);
        if (path != null && (r = this.cfgTree.getPathBounds(path)) != null && r.contains(p)) {
            this.toggle(path);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        TreePath path;
        JTree tree;
        if ((e.getKeyCode() == 32 || e.getKeyCode() == 10) && e.getSource() instanceof JTree && this.toggle(path = (tree = (JTree)e.getSource()).getSelectionPath())) {
            e.consume();
        }
    }

    private DefaultTreeModel createModel() {
        Item[] categories = new Item[]{new Item("AroundOperators", new Item("spaceAroundUnaryOps", new Item[0])), new Item("WithinParentheses", new Item("spaceWithinBraces", new Item[0]), new Item("spaceWithinArrayBrackets", new Item[0])), new Item("Other", new Item("spaceBeforeComma", new Item[0]), new Item("spaceAfterComma", new Item[0]), new Item("spaceBeforeColon", new Item[0]), new Item("spaceAfterColon", new Item[0]))};
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("root", true);
        DefaultTreeModel dtm = new DefaultTreeModel(root);
        for (Item item : categories) {
            DefaultMutableTreeNode cn = new DefaultMutableTreeNode(item, true);
            root.add(cn);
            for (Item si : item.items) {
                DefaultMutableTreeNode in = new DefaultMutableTreeNode(si, false);
                cn.add(in);
            }
        }
        return dtm;
    }

    private boolean toggle(TreePath treePath) {
        if (treePath == null) {
            return false;
        }
        Object o = ((DefaultMutableTreeNode)treePath.getLastPathComponent()).getUserObject();
        DefaultTreeModel dtm = (DefaultTreeModel)this.cfgTree.getModel();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)treePath.getLastPathComponent();
        if (o instanceof Item) {
            Item item = (Item)o;
            if (node.getAllowsChildren()) {
                return false;
            }
            item.value = !item.value;
            dtm.nodeChanged(node);
            dtm.nodeChanged(node.getParent());
            this.scs.notifyChanged();
        }
        return false;
    }

    private static final class SpacesCategorySupport
    extends FmtOptions.CategorySupport {
        public SpacesCategorySupport(Defaults.Provider provider, Preferences preferences, FmtSpaces panel, String preview) {
            super("text/x-json", provider, preferences, "spaces", panel, preview, new String[0][]);
            panel.scs = this;
        }

        @Override
        protected void addListeners() {
        }

        @Override
        protected void loadFrom(Preferences preferences) {
            for (Item item : this.getAllItems()) {
                boolean df = this.provider.getDefaultAsBoolean(item.id);
                item.value = preferences.getBoolean(item.id, df);
            }
        }

        @Override
        protected void storeTo(Preferences preferences) {
            for (Item item : this.getAllItems()) {
                boolean df = this.provider.getDefaultAsBoolean(item.id);
                if (df == item.value) {
                    preferences.remove(item.id);
                    continue;
                }
                preferences.putBoolean(item.id, item.value);
            }
        }

        private List<Item> getAllItems() {
            LinkedList<Item> result = new LinkedList<Item>();
            DefaultMutableTreeNode root = (DefaultMutableTreeNode)((FmtSpaces)this.panel).model.getRoot();
            Enumeration children = root.depthFirstEnumeration();
            while (children.hasMoreElements()) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)children.nextElement();
                Object o = node.getUserObject();
                if (!(o instanceof Item)) continue;
                Item item = (Item)o;
                if (item.items != null && item.items.length != 0) continue;
                result.add(item);
            }
            return result;
        }
    }

    private static class Item {
        String id;
        String displayName;
        boolean value;
        Item[] items;

        public /* varargs */ Item(String id, Item ... items) {
            this.id = id;
            this.items = items;
            this.displayName = NbBundle.getMessage(FmtSpaces.class, (String)("LBL_" + id));
        }

        public String toString() {
            return this.displayName;
        }
    }

}

