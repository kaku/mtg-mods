/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc;

import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.jsdoc.completion.AssingTag;
import org.netbeans.modules.javascript2.editor.jsdoc.completion.DescriptionTag;
import org.netbeans.modules.javascript2.editor.jsdoc.completion.LinkTag;
import org.netbeans.modules.javascript2.editor.jsdoc.completion.TypeDescribedTag;
import org.netbeans.modules.javascript2.editor.jsdoc.completion.TypeNamedTag;
import org.netbeans.modules.javascript2.editor.jsdoc.completion.TypeSimpleTag;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;

public class JsDocAnnotationCompletionTagProvider
extends AnnotationCompletionTagProvider {
    List<AnnotationCompletionTag> annotations = null;

    public JsDocAnnotationCompletionTagProvider(String name) {
        super(name);
    }

    @Override
    public synchronized List<AnnotationCompletionTag> getAnnotations() {
        if (this.annotations == null) {
            this.initAnnotations();
        }
        return this.annotations;
    }

    private void initAnnotations() {
        this.annotations = new LinkedList<AnnotationCompletionTag>();
        block8 : for (JsDocElementType type : JsDocElementType.values()) {
            if (type == JsDocElementType.UNKNOWN || type == JsDocElementType.CONTEXT_SENSITIVE) continue;
            switch (type.getCategory()) {
                case ASSIGN: {
                    this.annotations.add(new AssingTag(type.toString()));
                    continue block8;
                }
                case DECLARATION: {
                    this.annotations.add(new TypeSimpleTag(type.toString()));
                    continue block8;
                }
                case DESCRIPTION: {
                    this.annotations.add(new DescriptionTag(type.toString()));
                    continue block8;
                }
                case LINK: {
                    this.annotations.add(new LinkTag(type.toString()));
                    continue block8;
                }
                case NAMED_PARAMETER: {
                    this.annotations.add(new TypeNamedTag(type.toString()));
                    continue block8;
                }
                case UNNAMED_PARAMETER: {
                    this.annotations.add(new TypeDescribedTag(type.toString()));
                    continue block8;
                }
                default: {
                    this.annotations.add(new AnnotationCompletionTag(type.toString(), type.toString()));
                }
            }
        }
    }

}

