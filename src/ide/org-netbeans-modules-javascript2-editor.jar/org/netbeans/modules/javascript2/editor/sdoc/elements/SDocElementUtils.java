/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocDescriptionElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocIdentifierElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocSimpleElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeDescribedElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeNamedElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeSimpleElement;

public class SDocElementUtils {
    public static SDocElement createElementForType(SDocElementType elementType, String elementText, int elementTextStartOffset) {
        switch (elementType.getCategory()) {
            case DESCRIPTION: {
                return SDocDescriptionElement.create(elementType, elementText);
            }
            case IDENT: {
                return SDocIdentifierElement.create(elementType, elementText);
            }
            case SIMPLE: {
                return SDocSimpleElement.create(elementType);
            }
            case TYPE_SIMPLE: {
                TypeInformation simpleInfo = SDocElementUtils.parseTypeInformation(elementType, elementText, elementTextStartOffset);
                return SDocTypeSimpleElement.create(elementType, simpleInfo.getType());
            }
            case TYPE_DESCRIBED: {
                TypeInformation descInfo = SDocElementUtils.parseTypeInformation(elementType, elementText, elementTextStartOffset);
                return SDocTypeDescribedElement.create(elementType, descInfo.getType(), descInfo.getDescription());
            }
            case TYPE_NAMED: {
                TypeInformation namedInfo = SDocElementUtils.parseTypeInformation(elementType, elementText, elementTextStartOffset);
                return SDocTypeNamedElement.create(elementType, namedInfo.getType(), namedInfo.getDescription(), namedInfo.getName(), namedInfo.isOptional());
            }
        }
        return SDocDescriptionElement.create(elementType, elementText);
    }

    public static List<Type> parseTypes(String textToParse, int offset) {
        String[] typesArray;
        LinkedList<Type> types = new LinkedList<Type>();
        textToParse = SDocElementUtils.removeCurlyBraces(textToParse);
        for (String string : typesArray = textToParse.split("[,]")) {
            types.add(new TypeUsageImpl(string.trim(), offset + textToParse.indexOf(string.trim())));
        }
        return types;
    }

    private static TypeInformation parseTypeInformation(SDocElementType elementType, String elementText, int descStartOffset) {
        int process;
        TypeInformation typeInformation = new TypeInformation();
        String[] parts = elementText.split("[\\s]+");
        if (parts.length > process) {
            if (parts[0].startsWith("{")) {
                int typeOffset = descStartOffset + 1;
                int rparIndex = parts[0].indexOf("}");
                if (rparIndex == -1) {
                    StringBuilder sb = new StringBuilder();
                    for (process = 0; process < parts.length; ++process) {
                        sb.append(parts[process]);
                        if (parts[process].indexOf("}") != -1) break;
                        sb.append(" ");
                    }
                    typeInformation.setType(SDocElementUtils.parseTypes(sb.toString(), typeOffset));
                } else {
                    typeInformation.setType(SDocElementUtils.parseTypes(parts[0], typeOffset));
                }
                ++process;
            }
            if (parts.length > process && elementType.getCategory() == SDocElementType.Category.TYPE_NAMED) {
                int nameOffset = descStartOffset + elementText.indexOf(parts[process]);
                SDocElementUtils.parseAndStoreTypeDetails(typeInformation, nameOffset, parts[process].trim());
                ++process;
            }
            StringBuilder sb = new StringBuilder();
            while (process < parts.length) {
                sb.append(parts[process]).append(" ");
                ++process;
            }
            typeInformation.setDescription(sb.toString().trim());
        }
        return typeInformation;
    }

    private static void parseAndStoreTypeDetails(TypeInformation typeInfo, int nameOffset, String nameText) {
        boolean optional = nameText.matches("\\[.*\\]");
        if (optional) {
            ++nameOffset;
            nameText = nameText.substring(1, nameText.length() - 1);
        }
        typeInfo.setOptional(optional);
        typeInfo.setName(new IdentifierImpl(nameText, nameOffset));
    }

    private static String removeCurlyBraces(String textToParse) {
        return textToParse.replaceAll("[{}]+", "");
    }

    private static class TypeInformation {
        private List<Type> type = Collections.emptyList();
        private String description = "";
        private Identifier name = null;
        private boolean optional = false;

        private TypeInformation() {
        }

        public void setName(Identifier name) {
            this.name = name;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setType(List<Type> type) {
            this.type = type;
        }

        public void setOptional(boolean optional) {
            this.optional = optional;
        }

        public Identifier getName() {
            return this.name;
        }

        public String getDescription() {
            return this.description;
        }

        public List<Type> getType() {
            return this.type;
        }

        public boolean isOptional() {
            return this.optional;
        }
    }

}

