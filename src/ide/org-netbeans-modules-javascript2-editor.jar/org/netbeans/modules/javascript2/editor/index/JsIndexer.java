/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.Context
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer
 *  org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory
 *  org.netbeans.modules.parsing.spi.indexing.Indexable
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexDocument
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.javascript2.editor.index;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Parameters;

public class JsIndexer
extends EmbeddingIndexer {
    private static final Logger LOG = Logger.getLogger(JsIndexer.class.getName());

    protected void index(Indexable indexable, Parser.Result result, Context context) {
        IndexingSupport support;
        LOG.log(Level.FINE, "Indexing: {0}, fullPath: {1}", new Object[]{indexable.getRelativePath(), result.getSnapshot().getSource().getFileObject().getPath()});
        if (!(result instanceof JsParserResult)) {
            return;
        }
        if (!context.checkForEditorModifications()) {
            JsIndex.changeInIndex();
        }
        JsParserResult parserResult = (JsParserResult)result;
        Model model = parserResult.getModel(true);
        try {
            support = IndexingSupport.getInstance((Context)context);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return;
        }
        support.removeDocuments(indexable);
        JsObject globalObject = model.getGlobalObject();
        for (JsObject object : globalObject.getProperties().values()) {
            if (object.getParent() == null) continue;
            this.storeObject(object, object.getName(), support, indexable);
        }
        IndexDocument document = support.createDocument(indexable);
        for (JsObject object2 : globalObject.getProperties().values()) {
            if (object2.getParent() == null) continue;
            this.storeUsages(object2, object2.getName(), document);
        }
        support.addDocument(document);
    }

    private void storeObject(JsObject object, String fqn, IndexingSupport support, Indexable indexable) {
        if (!this.isInvisibleFunction(object)) {
            if (object.isDeclared() || "prototype".equals(object.getName())) {
                IndexDocument document = IndexedElement.createDocument(object, fqn, support, indexable);
                support.addDocument(document);
            }
            if (!(object instanceof JsObjectReference) || !ModelUtils.isDescendant(object, ((JsObjectReference)object).getOriginal())) {
                for (JsObject property : object.getProperties().values()) {
                    if (!(property instanceof JsObjectReference) || ((JsObjectReference)property).getOriginal().isAnonymous()) {
                        this.storeObject(property, fqn + '.' + property.getName(), support, indexable);
                        continue;
                    }
                    IndexDocument document = IndexedElement.createDocumentForReference((JsObjectReference)property, fqn + '.' + property.getName(), support, indexable);
                    support.addDocument(document);
                }
                if (object instanceof JsFunction) {
                    for (JsObject parameter : ((JsFunction)object).getParameters()) {
                        this.storeObject(parameter, fqn + '.' + parameter.getName(), support, indexable);
                    }
                }
            }
        }
    }

    private boolean isInvisibleFunction(JsObject object) {
        if (object.getJSKind().isFunction() && (object.isAnonymous() || object.getModifiers().contains((Object)Modifier.PRIVATE))) {
            Collection<? extends TypeUsage> returnTypes;
            JsObject parent = object.getParent();
            if (parent != null && parent.getJSKind() == JsElement.Kind.FILE) {
                return false;
            }
            if (parent != null && parent instanceof JsFunction) {
                returnTypes = ((JsFunction)parent).getReturnTypes();
                String fqn = object.getFullyQualifiedName();
                for (TypeUsage returnType : returnTypes) {
                    if (!returnType.getType().equals(fqn)) continue;
                    return false;
                }
            }
            if ((returnTypes = ((JsFunction)object).getReturnTypes()).size() == 1 && returnTypes.iterator().next().getType().equals("undefined")) {
                return true;
            }
        }
        return false;
    }

    private void storeUsages(JsObject object, String name, IndexDocument document) {
        StringBuilder sb = new StringBuilder();
        sb.append(object.getName());
        for (JsObject property2 : object.getProperties().values()) {
            if (!this.storeUsage(property2)) continue;
            sb.append(':');
            sb.append(property2.getName()).append('#');
            if (property2.getJSKind().isFunction()) {
                sb.append('F');
                continue;
            }
            sb.append('P');
        }
        document.addPair("usage", sb.toString(), true, true);
        if (object instanceof JsFunction) {
            for (JsObject parameter : ((JsFunction)object).getParameters()) {
                this.storeUsages(parameter, parameter.getName(), document);
            }
        }
        for (JsObject property : object.getProperties().values()) {
            if (!this.storeUsage(property) || property instanceof JsObjectReference && !((JsObjectReference)property).getOriginal().isAnonymous()) continue;
            this.storeUsages(property, property.getName(), document);
        }
    }

    private boolean storeUsage(JsObject object) {
        boolean result = true;
        if ("arguments".equals(object.getName()) || object.getJSKind() == JsElement.Kind.ANONYMOUS_OBJECT || object.getModifiers().contains((Object)Modifier.PRIVATE)) {
            result = false;
        }
        return result;
    }

    public static final class Factory
    extends EmbeddingIndexerFactory {
        public static final String NAME = "js";
        public static final int VERSION = 13;
        private static final int PRIORITY = 100;
        private static final ThreadLocal<Collection<Runnable>> postScanTasks = new ThreadLocal();

        public EmbeddingIndexer createIndexer(Indexable indexable, Snapshot snapshot) {
            if (this.isIndexable(indexable, snapshot)) {
                return new JsIndexer();
            }
            return null;
        }

        public String getIndexerName() {
            return "js";
        }

        public int getIndexVersion() {
            return 13;
        }

        private boolean isIndexable(Indexable indexable, Snapshot snapshot) {
            return "text/javascript".equals(snapshot.getMimeType());
        }

        public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
            try {
                IndexingSupport is = IndexingSupport.getInstance((Context)context);
                for (Indexable i : deleted) {
                    is.removeDocuments(i);
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
        }

        public void rootsRemoved(Iterable<? extends URL> removedRoots) {
        }

        public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
            try {
                IndexingSupport is = IndexingSupport.getInstance((Context)context);
                for (Indexable i : dirty) {
                    is.markDirtyDocuments(i);
                }
            }
            catch (IOException ioe) {
                LOG.log(Level.WARNING, null, ioe);
            }
        }

        public boolean scanStarted(Context context) {
            postScanTasks.set(new LinkedList());
            return super.scanStarted(context);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void scanFinished(Context context) {
            try {
                for (Runnable task : postScanTasks.get()) {
                    task.run();
                }
            }
            finally {
                postScanTasks.remove();
                super.scanFinished(context);
            }
        }

        public static boolean isScannerThread() {
            return postScanTasks.get() != null;
        }

        public static void addPostScanTask(@NonNull Runnable task) {
            Parameters.notNull((CharSequence)"task", (Object)task);
            Collection<Runnable> tasks = postScanTasks.get();
            if (tasks == null) {
                throw new IllegalStateException("JsIndexer.postScanTask can be called only from scanner thread.");
            }
            tasks.add(task);
        }

        public int getPriority() {
            return 100;
        }
    }

}

