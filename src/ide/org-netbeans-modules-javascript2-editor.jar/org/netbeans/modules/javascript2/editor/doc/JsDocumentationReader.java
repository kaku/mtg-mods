/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsDocumentationReader {
    public static Set<String> getAllTags(Snapshot snapshot) {
        HashSet<String> tags = new HashSet<String>();
        tags.addAll(JsDocumentationReader.getCommentTags(snapshot.getText()));
        return tags;
    }

    protected static Set<String> getCommentTags(CharSequence commentText) {
        HashSet<String> tags = new HashSet<String>();
        String comment = commentText.toString();
        Pattern pattern = Pattern.compile("[@][a-zA-Z]+");
        Matcher matcher = pattern.matcher(comment);
        while (matcher.find()) {
            tags.add(matcher.group());
        }
        return tags;
    }
}

