/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.index;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.QuerySupportFactory;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;

public class JsIndex {
    public static final String FIELD_BASE_NAME = "bn";
    public static final String FIELD_FQ_NAME = "fqn";
    public static final String FIELD_OFFSET = "offset";
    public static final String FIELD_ASSIGNMENTS = "assign";
    public static final String FIELD_RETURN_TYPES = "return";
    public static final String FIELD_PARAMETERS = "param";
    public static final String FIELD_FLAG = "flag";
    public static final String FIELD_ARRAY_TYPES = "array";
    public static final String FIELD_USAGE = "usage";
    private static final String PROPERTIES_PATTERN = "\\.[^\\.]*[^P]";
    @SuppressWarnings(value={"MS_MUTABLE_ARRAY"})
    public static final String[] TERMS_BASIC_INFO = new String[]{"bn", "fqn", "offset", "return", "param", "flag", "assign", "array"};
    private static final Logger LOG = Logger.getLogger(JsIndex.class.getName());
    private static final ReentrantReadWriteLock LOCK = new ReentrantReadWriteLock();
    private static final Lock READ_LOCK = LOCK.readLock();
    private static final Lock WRITE_LOCK = LOCK.writeLock();
    private static final WeakHashMap<FileObject, JsIndex> INDEX_CACHE = new WeakHashMap();
    private static final int MAX_ENTRIES_CACHE_INDEX_RESULT = 2000;
    private static final int MAX_CACHE_VALUE_SIZE = 1000000;
    private static final int AVERAGE_BASIC_INFO_SIZE = 60;
    private static final Map<CacheKey, SoftReference<CacheValue>> CACHE_INDEX_RESULT_SMALL = new LinkedHashMap<CacheKey, SoftReference<CacheValue>>(2001, 0.75f, true){

        @Override
        public boolean removeEldestEntry(Map.Entry eldest) {
            return this.size() > 2000;
        }
    };
    private static final Map<CacheKey, SoftReference<CacheValue>> CACHE_INDEX_RESULT_LARGE = new LinkedHashMap<CacheKey, SoftReference<CacheValue>>(501, 0.75f, true){

        @Override
        public boolean removeEldestEntry(Map.Entry eldest) {
            return this.size() > 500;
        }
    };
    private static final AtomicBoolean INDEX_CHANGED = new AtomicBoolean(true);
    private static final Map<StatsKey, StatsValue> QUERY_STATS = new HashMap<StatsKey, StatsValue>();
    private static int cacheHit;
    private static int cacheMiss;
    private final QuerySupport querySupport;
    private final boolean updateCache;
    private final int MAX_FIND_PROPERTIES_RECURSION = 15;

    private JsIndex(QuerySupport querySupport, boolean updateCache) {
        this.querySupport = querySupport;
        this.updateCache = updateCache;
    }

    public static JsIndex get(Collection<FileObject> roots) {
        LOG.log(Level.FINE, "JsIndex for roots: {0}", roots);
        return new JsIndex(QuerySupportFactory.get(roots), false);
    }

    public static void changeInIndex() {
        INDEX_CHANGED.set(true);
    }

    public static JsIndex get(FileObject fo) {
        JsIndex index = INDEX_CACHE.get((Object)fo);
        if (index == null) {
            LOG.log(Level.FINE, "Creating JsIndex for FileObject: {0}", (Object)fo);
            index = new JsIndex(QuerySupportFactory.get(fo), true);
            INDEX_CACHE.put(fo, index);
        }
        return index;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public /* varargs */ Collection<? extends IndexResult> query(String fieldName, String fieldValue, QuerySupport.Kind kind, String ... fieldsToLoad) {
        if (this.querySupport == null) {
            return Collections.emptySet();
        }
        try {
            CacheValue value;
            CacheKey key;
            if (INDEX_CHANGED.get()) {
                WRITE_LOCK.lock();
                try {
                    CACHE_INDEX_RESULT_SMALL.clear();
                    CACHE_INDEX_RESULT_LARGE.clear();
                    LOG.log(Level.FINEST, "Cache cleared");
                }
                finally {
                    WRITE_LOCK.unlock();
                }
                INDEX_CHANGED.set(false);
            }
            if ((value = JsIndex.getCachedValue(key = new CacheKey(this, fieldName, fieldValue, kind), fieldsToLoad)) != null) {
                JsIndex.logStats(value.getResult(), true, fieldsToLoad);
                return value.getResult();
            }
            Collection result = this.querySupport.query(fieldName, fieldValue, kind, fieldsToLoad);
            if (!this.updateCache) {
                JsIndex.logStats(result, false, fieldsToLoad);
                return result;
            }
            WRITE_LOCK.lock();
            try {
                value = JsIndex.getCachedValue(key, fieldsToLoad);
                if (value != null) {
                    JsIndex.logStats(value.getResult(), false, fieldsToLoad);
                    Collection<? extends IndexResult> collection = value.getResult();
                    return collection;
                }
                value = new CacheValue(fieldsToLoad, result);
                if (result.size() * 60 < 1000000) {
                    CACHE_INDEX_RESULT_SMALL.put(key, new SoftReference<CacheValue>(value));
                } else {
                    CACHE_INDEX_RESULT_LARGE.put(key, new SoftReference<CacheValue>(value));
                }
                JsIndex.logStats(result, false, fieldsToLoad);
                Collection<? extends IndexResult> collection = value.getResult();
                return collection;
            }
            finally {
                WRITE_LOCK.unlock();
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return Collections.emptySet();
        }
    }

    public Collection<IndexedElement> getGlobalVar(String prefix) {
        prefix = prefix == null ? "" : prefix;
        ArrayList<IndexedElement> globals = new ArrayList<IndexedElement>();
        long start = System.currentTimeMillis();
        String indexPrefix = this.escapeRegExp(prefix) + "[^\\.]*[" + 'O' + "]";
        Collection<? extends IndexResult> globalObjects = this.query("fqn", indexPrefix, QuerySupport.Kind.REGEXP, TERMS_BASIC_INFO);
        for (IndexResult indexResult : globalObjects) {
            IndexedElement indexedElement = IndexedElement.create(indexResult);
            globals.add(indexedElement);
        }
        long end = System.currentTimeMillis();
        LOG.log(Level.FINE, "Obtaining globals from the index took: {0}", end - start);
        return globals;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static /* varargs */ CacheValue getCachedValue(CacheKey key, String ... fieldsToLoad) {
        READ_LOCK.lock();
        try {
            CacheValue value = null;
            SoftReference<CacheValue> currentReference = CACHE_INDEX_RESULT_SMALL.get(key);
            if (currentReference != null) {
                value = currentReference.get();
            }
            if (value == null || !value.contains(fieldsToLoad)) {
                currentReference = CACHE_INDEX_RESULT_LARGE.get(key);
                if (currentReference != null) {
                    value = currentReference.get();
                }
                if (value == null || !value.contains(fieldsToLoad)) {
                    CacheValue cacheValue = null;
                    return cacheValue;
                }
                CacheValue cacheValue = value;
                return cacheValue;
            }
            CacheValue cacheValue = value;
            return cacheValue;
        }
        finally {
            READ_LOCK.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static /* varargs */ void logStats(Collection<? extends IndexResult> result, boolean hit, String ... fieldsToLoad) {
        if (!LOG.isLoggable(Level.FINEST)) {
            return;
        }
        int size = 0;
        for (String field : fieldsToLoad) {
            for (IndexResult r : result) {
                String val = r.getValue(field);
                size += val == null ? 0 : val.length();
            }
        }
        Map<StatsKey, StatsValue> arr$ = QUERY_STATS;
        synchronized (arr$) {
            if (hit) {
                ++cacheHit;
            } else {
                ++cacheMiss;
            }
            StatsKey statsKey = new StatsKey(fieldsToLoad);
            StatsValue statsValue = QUERY_STATS.get(statsKey);
            if (statsValue == null) {
                QUERY_STATS.put(statsKey, new StatsValue(1, result.size(), size));
            } else {
                QUERY_STATS.put(statsKey, new StatsValue(statsValue.getRequests() + 1, statsValue.getCount() + result.size(), statsValue.getSize() + (long)size));
            }
            if ((cacheHit + cacheMiss) % 500 == 0) {
                LOG.log(Level.FINEST, "Cache hit: " + cacheHit + ", Cache miss: " + cacheMiss + ", Ratio: " + cacheHit / cacheMiss);
                for (Map.Entry<StatsKey, StatsValue> entry : QUERY_STATS.entrySet()) {
                    LOG.log(Level.FINEST, entry.getKey() + ": " + entry.getValue());
                }
            }
        }
    }

    private static Collection<IndexedElement> getElementsByPrefix(String prefix, Collection<IndexedElement> items) {
        ArrayList<IndexedElement> result = new ArrayList<IndexedElement>();
        for (IndexedElement indexedElement : items) {
            if (!indexedElement.getName().startsWith(prefix)) continue;
            result.add(indexedElement);
        }
        return result;
    }

    public Collection<IndexedElement> getPropertiesWithPrefix(String fqn, String prexif) {
        return JsIndex.getElementsByPrefix(prexif, this.getProperties(fqn));
    }

    public Collection<IndexedElement> getProperties(String fqn) {
        return this.getProperties(fqn, 0, new ArrayList<String>());
    }

    public Collection<IndexedElement> getUsagesFromExpression(List<String> expChain) {
        if (expChain == null || expChain.isEmpty()) {
            return Collections.emptyList();
        }
        String searchText = expChain.get(0) + ':';
        Collection<? extends IndexResult> results = this.query("usage", searchText, QuerySupport.Kind.PREFIX, "usage");
        ArrayList<IndexedElement> usages = new ArrayList<IndexedElement>();
        HashSet<String> alreadyUsed = new HashSet<String>();
        for (IndexResult indexResult : results) {
            String[] fields = indexResult.getValues("usage");
            FileObject fo = indexResult.getFile();
            for (String field : fields) {
                String[] parts;
                if (!field.startsWith(searchText)) continue;
                for (String property : parts = field.split(":")) {
                    String[] split = property.split("#");
                    if (split.length != 2 || alreadyUsed.contains(split[0])) continue;
                    alreadyUsed.add(split[0]);
                    IndexedElement element = new IndexedElement(fo, split[0], split[0], false, false, split[1].equals("F") ? JsElement.Kind.FUNCTION : JsElement.Kind.OBJECT, OffsetRange.NONE, Collections.singleton(Modifier.PUBLIC), Collections.EMPTY_LIST, false);
                    usages.add(element);
                }
            }
        }
        return usages;
    }

    private Collection<IndexedElement> getProperties(String fqn, int deepLevel, Collection<String> resolvedTypes) {
        if (deepLevel > 15) {
            return Collections.EMPTY_LIST;
        }
        ArrayList<IndexedElement> result = new ArrayList<IndexedElement>();
        if (!resolvedTypes.contains(fqn)) {
            resolvedTypes.add(fqn);
            ++deepLevel;
            Collection<? extends IndexResult> results = this.findByFqn(fqn, "assign");
            for (IndexResult indexResult : results) {
                TypeUsage type;
                Collection<TypeUsage> assignments = IndexedElement.getAssignments(indexResult);
                if (assignments.isEmpty() || resolvedTypes.contains((type = assignments.iterator().next()).getType())) continue;
                result.addAll(this.getProperties(type.getType(), deepLevel, resolvedTypes));
            }
            Hashtable<String, Collection<? extends IndexResult>> fqnToResults = new Hashtable<String, Collection<? extends IndexResult>>();
            fqnToResults.put(fqn, this.query("fqn", fqn + ".", QuerySupport.Kind.PREFIX, TERMS_BASIC_INFO));
            if (fqn.indexOf(46) == -1) {
                Collection<? extends IndexResult> tmpResults = this.query("bn", fqn, QuerySupport.Kind.EXACT, "fqn");
                for (IndexResult indexResult2 : tmpResults) {
                    String value = IndexedElement.getFQN(indexResult2);
                    fqnToResults.put(value, this.query("fqn", value + ".", QuerySupport.Kind.PREFIX, TERMS_BASIC_INFO));
                }
            }
            for (Map.Entry entry : fqnToResults.entrySet()) {
                String fqnKey = (String)entry.getKey();
                Collection properties = (Collection)entry.getValue();
                for (IndexResult indexResult3 : properties) {
                    IndexedElement property;
                    String value = indexResult3.getValue("fqn");
                    if (value.isEmpty() || value.charAt(value.length() - 1) == 'P' || (value = value.substring(fqnKey.length())).lastIndexOf(46) != 0 || (property = IndexedElement.create(indexResult3)).getModifiers().contains((Object)Modifier.PRIVATE)) continue;
                    result.add(property);
                }
            }
        }
        return result;
    }

    public /* varargs */ Collection<? extends IndexResult> findByFqn(String fqn, String ... fields) {
        ArrayList<Object> results = new ArrayList<Object>();
        results.addAll(this.query("fqn", fqn + 'A', QuerySupport.Kind.EXACT, fields));
        results.addAll(this.query("fqn", fqn + 'O', QuerySupport.Kind.EXACT, fields));
        results.addAll(this.query("fqn", fqn + 'P', QuerySupport.Kind.EXACT, fields));
        return results;
    }

    private String escapeRegExp(String text) {
        return Pattern.quote(text);
    }

    private static class StatsValue {
        private final int requests;
        private final int count;
        private final long size;

        public StatsValue(int requests, int count, long size) {
            this.requests = requests;
            this.count = count;
            this.size = size;
        }

        public int getRequests() {
            return this.requests;
        }

        public int getCount() {
            return this.count;
        }

        public long getSize() {
            return this.size;
        }

        public String toString() {
            return "StatsValue{requests=" + this.requests + ", average=" + (this.count != 0 ? this.size / (long)this.count : 0) + ", count=" + this.count + ", size=" + this.size + '}';
        }
    }

    private static class StatsKey {
        private final String[] fields;

        public StatsKey(String[] fields) {
            this.fields = (String[])fields.clone();
            Arrays.sort(this.fields);
        }

        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + Arrays.deepHashCode(this.fields);
            return hash;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            StatsKey other = (StatsKey)obj;
            if (!Arrays.deepEquals(this.fields, other.fields)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return Arrays.deepToString(this.fields);
        }
    }

    private static class CacheValue {
        private final Set<String> fields;
        private final Collection<? extends IndexResult> result;

        public CacheValue(String[] fields, Collection<? extends IndexResult> result) {
            this.fields = new HashSet<String>(Arrays.asList(fields));
            this.result = result;
        }

        public Collection<? extends IndexResult> getResult() {
            return this.result;
        }

        public /* varargs */ boolean contains(String ... fieldsToLoad) {
            return this.fields.containsAll(Arrays.asList(fieldsToLoad));
        }
    }

    private static class CacheKey {
        private final JsIndex index;
        private final String fieldName;
        private final String fieldValue;
        private final QuerySupport.Kind kind;

        public CacheKey(JsIndex index, String fieldName, String fieldValue, QuerySupport.Kind kind) {
            this.index = index;
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            this.kind = kind;
        }

        public int hashCode() {
            int hash = 3;
            hash = 41 * hash + (this.index != null ? this.index.hashCode() : 0);
            hash = 41 * hash + (this.fieldName != null ? this.fieldName.hashCode() : 0);
            hash = 41 * hash + (this.fieldValue != null ? this.fieldValue.hashCode() : 0);
            hash = 41 * hash + (this.kind != null ? this.kind.hashCode() : 0);
            return hash;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            CacheKey other = (CacheKey)obj;
            if (!(this.index == other.index || this.index != null && this.index.equals(other.index))) {
                return false;
            }
            if (this.fieldName == null ? other.fieldName != null : !this.fieldName.equals(other.fieldName)) {
                return false;
            }
            if (this.fieldValue == null ? other.fieldValue != null : !this.fieldValue.equals(other.fieldValue)) {
                return false;
            }
            if (this.kind != other.kind) {
                return false;
            }
            return true;
        }

        public String toString() {
            return "CacheKey{index=" + this.index + ", fieldName=" + this.fieldName + ", fieldValue=" + this.fieldValue + ", kind=" + (Object)this.kind + '}';
        }
    }

}

