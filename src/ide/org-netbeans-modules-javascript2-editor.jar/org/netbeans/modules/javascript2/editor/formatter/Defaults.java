/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.javascript2.editor.formatter.CodeStyle;
import org.netbeans.modules.javascript2.editor.formatter.FmtOptions;

public final class Defaults {
    private static final Map<String, String> JSON_SPECIAL_VALUES = new HashMap<String, String>();

    public static Provider getInstance(String mimeType) {
        if ("text/javascript".equals(mimeType)) {
            return new FmtOptions.BasicDefaultsProvider();
        }
        if ("text/x-json".equals(mimeType)) {
            FmtOptions.BasicDefaultsProvider basic = new FmtOptions.BasicDefaultsProvider();
            return new ProxyDefaultsProvider(basic, JSON_SPECIAL_VALUES);
        }
        throw new IllegalStateException("Unsupported mime type " + mimeType);
    }

    static {
        JSON_SPECIAL_VALUES.put("wrapObjects", CodeStyle.WrapStyle.WRAP_ALWAYS.name());
        JSON_SPECIAL_VALUES.put("wrapProperties", CodeStyle.WrapStyle.WRAP_ALWAYS.name());
    }

    public static class ProxyDefaultsProvider
    implements Provider {
        private final Provider provider;
        private final Map<String, String> defaults;

        public ProxyDefaultsProvider(Provider provider, Map<String, String> defaults) {
            this.provider = provider;
            this.defaults = defaults;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public int getDefaultAsInt(String key) {
            Map<String, String> map = this.defaults;
            synchronized (map) {
                if (this.defaults.containsKey(key)) {
                    return Integer.parseInt(this.defaults.get(key));
                }
            }
            return this.provider.getDefaultAsInt(key);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public boolean getDefaultAsBoolean(String key) {
            Map<String, String> map = this.defaults;
            synchronized (map) {
                if (this.defaults.containsKey(key)) {
                    return Boolean.parseBoolean(this.defaults.get(key));
                }
            }
            return this.provider.getDefaultAsBoolean(key);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public String getDefaultAsString(String key) {
            Map<String, String> map = this.defaults;
            synchronized (map) {
                if (this.defaults.containsKey(key)) {
                    return this.defaults.get(key);
                }
            }
            return this.provider.getDefaultAsString(key);
        }
    }

    public static interface Provider {
        public int getDefaultAsInt(String var1);

        public boolean getDefaultAsBoolean(String var1);

        public String getDefaultAsString(String var1);
    }

}

