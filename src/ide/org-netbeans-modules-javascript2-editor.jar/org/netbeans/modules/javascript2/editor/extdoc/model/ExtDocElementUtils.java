/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocDescriptionElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocIdentDescribedElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocIdentSimpleElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocSimpleElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocTypeDescribedElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocTypeNamedElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocTypeSimpleElement;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;

public class ExtDocElementUtils {
    public static ExtDocElement createElementForType(ExtDocElementType elementType, String elementText, int elementTextStartOffset) {
        switch (elementType.getCategory()) {
            case DESCRIPTION: {
                return ExtDocDescriptionElement.create(elementType, elementText);
            }
            case IDENT_SIMPLE: {
                return ExtDocIdentSimpleElement.create(elementType, elementText);
            }
            case IDENT_DESCRIBED: {
                String[] identAttributes = ExtDocElementUtils.parseIdentAttributes(elementText);
                return ExtDocIdentDescribedElement.create(elementType, identAttributes[0], identAttributes[1]);
            }
            case SIMPLE: {
                return ExtDocSimpleElement.create(elementType);
            }
            case TYPE_SIMPLE: {
                TypeInformation simpleInfo = ExtDocElementUtils.parseTypeInformation(elementType, elementText, elementTextStartOffset);
                return ExtDocTypeSimpleElement.create(elementType, simpleInfo.getType());
            }
            case TYPE_DESCRIBED: {
                TypeInformation descInfo = ExtDocElementUtils.parseTypeInformation(elementType, elementText, elementTextStartOffset);
                return ExtDocTypeDescribedElement.create(elementType, descInfo.getType(), descInfo.getDescription());
            }
            case TYPE_NAMED: {
                TypeInformation namedInfo = ExtDocElementUtils.parseTypeInformation(elementType, elementText, elementTextStartOffset);
                return ExtDocTypeNamedElement.create(elementType, namedInfo.getType(), namedInfo.getDescription(), namedInfo.getName(), namedInfo.isOptional(), namedInfo.getDefaultValue());
            }
        }
        return ExtDocDescriptionElement.create(elementType, elementText);
    }

    public static String[] parseIdentAttributes(String elementText) {
        String[] parts = elementText.split("[\\s]+");
        if (parts.length == 1) {
            return new String[]{parts[0], ""};
        }
        return new String[]{parts[0], elementText.substring(parts[0].length()).trim()};
    }

    public static List<Type> parseTypes(String textToParse, int offset) {
        String[] typesArray;
        LinkedList<Type> types = new LinkedList<Type>();
        for (String string : typesArray = textToParse.split("[/]")) {
            types.add(new TypeUsageImpl(string, offset + textToParse.indexOf(string)));
        }
        return types;
    }

    private static TypeInformation parseTypeInformation(ExtDocElementType elementType, String elementText, int descStartOffset) {
        TypeInformation typeInformation = new TypeInformation();
        int process = 0;
        String[] parts = elementText.split("[\\s]+");
        if (parts.length > process) {
            if (parts[0].startsWith("{")) {
                int typeOffset = descStartOffset + 1;
                int rparIndex = parts[0].indexOf("}");
                if (rparIndex == -1) {
                    typeInformation.setType(ExtDocElementUtils.parseTypes(parts[0].trim(), typeOffset));
                } else {
                    typeInformation.setType(ExtDocElementUtils.parseTypes(parts[0].substring(1, rparIndex), typeOffset));
                }
                ++process;
            }
            if (parts.length > process && elementType.getCategory() == ExtDocElementType.Category.TYPE_NAMED) {
                StringBuilder name = new StringBuilder();
                int nameOffset = descStartOffset + elementText.indexOf(parts[process]);
                name.append(parts[process].trim());
                ++process;
                if (name.toString().contains("\"") || name.toString().contains("'")) {
                    process = ExtDocElementUtils.getOptionalParamNameWithString(name, process, parts);
                }
                ExtDocElementUtils.parseAndStoreTypeDetails(typeInformation, nameOffset, name.toString().trim());
            }
            StringBuilder description = new StringBuilder();
            while (process < parts.length) {
                description.append(parts[process]).append(" ");
                ++process;
            }
            typeInformation.setDescription(description.toString().trim());
        }
        return typeInformation;
    }

    private static int getOptionalParamNameWithString(StringBuilder name, int currentOffset, String[] parts) {
        String nameString = name.toString();
        if (nameString.indexOf("\"") != -1 && nameString.indexOf("\"") == nameString.lastIndexOf("\"") || nameString.indexOf("'") != -1 && nameString.indexOf("'") == nameString.lastIndexOf("'")) {
            boolean endOfString = false;
            while (currentOffset < parts.length && !endOfString) {
                name.append(" ").append(parts[currentOffset]);
                if (parts[currentOffset].contains("\"") || parts[currentOffset].contains("'")) {
                    endOfString = true;
                }
                ++currentOffset;
            }
        }
        return currentOffset;
    }

    private static void parseAndStoreTypeDetails(TypeInformation typeInfo, int nameOffset, String nameText) {
        boolean optional = nameText.matches("\\[.*\\]");
        if (optional) {
            ++nameOffset;
            int indexOfEquals = (nameText = nameText.substring(1, nameText.length() - 1)).indexOf("=");
            if (indexOfEquals != -1) {
                typeInfo.setDefaultValue(nameText.substring(indexOfEquals + 1));
                nameText = nameText.substring(0, indexOfEquals);
            }
        }
        typeInfo.setOptional(optional);
        typeInfo.setName(new IdentifierImpl(nameText, nameOffset));
    }

    private static class TypeInformation {
        private List<Type> type = Collections.emptyList();
        private String description = "";
        private String defaultValue = null;
        private Identifier name = null;
        private boolean optional = false;

        private TypeInformation() {
        }

        public void setName(Identifier name) {
            this.name = name;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setType(List<Type> type) {
            this.type = type;
        }

        public void setOptional(boolean optional) {
            this.optional = optional;
        }

        public String getDefaultValue() {
            return this.defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public Identifier getName() {
            return this.name;
        }

        public String getDescription() {
            return this.description;
        }

        public List<Type> getType() {
            return this.type;
        }

        public boolean isOptional() {
            return this.optional;
        }
    }

}

