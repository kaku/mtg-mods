/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import java.util.List;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocTypeSimpleElement;

public class SDocTypeDescribedElement
extends SDocTypeSimpleElement {
    protected final String typeDescription;

    protected SDocTypeDescribedElement(SDocElementType type, List<Type> declaredTypes, String description) {
        super(type, declaredTypes);
        this.typeDescription = description;
    }

    public static SDocTypeDescribedElement create(SDocElementType type, List<Type> declaredTypes, String description) {
        return new SDocTypeDescribedElement(type, declaredTypes, description);
    }

    public String getTypeDescription() {
        return this.typeDescription;
    }

    @Override
    public Identifier getParamName() {
        return null;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOptional() {
        return false;
    }

    @Override
    public String getParamDescription() {
        return this.typeDescription;
    }

    @Override
    public List<Type> getParamTypes() {
        return this.declaredTypes;
    }
}

