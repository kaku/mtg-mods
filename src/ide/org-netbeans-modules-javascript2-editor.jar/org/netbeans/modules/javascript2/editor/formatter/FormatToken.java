/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.javascript2.editor.formatter;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;

public final class FormatToken {
    private final Kind kind;
    private final int offset;
    private final CharSequence text;
    private final JsTokenId id;
    private FormatToken next;
    private FormatToken previous;

    private FormatToken(Kind kind, int offset, CharSequence text, JsTokenId id) {
        this.kind = kind;
        this.offset = offset;
        this.text = text;
        this.id = id;
    }

    public static FormatToken forText(int offset, CharSequence text, JsTokenId id) {
        assert (text != null);
        assert (offset >= 0);
        return new FormatToken(Kind.TEXT, offset, text, id);
    }

    public static FormatToken forFormat(Kind kind) {
        return new FormatToken(kind, -1, null, null);
    }

    public static FormatToken forAny(Kind kind, int offset, CharSequence text, JsTokenId id) {
        assert (text != null);
        assert (offset >= 0);
        return new FormatToken(kind, offset, text, id);
    }

    @NonNull
    public Kind getKind() {
        return this.kind;
    }

    @CheckForNull
    public JsTokenId getId() {
        return this.id;
    }

    @NonNull
    public CharSequence getText() {
        assert (!this.isVirtual());
        return this.text != null ? this.text : "";
    }

    public int getOffset() {
        return this.offset;
    }

    @CheckForNull
    public FormatToken next() {
        return this.next;
    }

    @CheckForNull
    public FormatToken previous() {
        return this.previous;
    }

    public boolean isVirtual() {
        return this.offset < 0;
    }

    public String toString() {
        return "FormatToken{kind=" + (Object)((Object)this.kind) + ", offset=" + this.offset + ", text=" + this.text + ", id=" + (Object)((Object)this.id) + '}';
    }

    void setNext(FormatToken next) {
        this.next = next;
    }

    void setPrevious(FormatToken previous) {
        this.previous = previous;
    }

    public static enum Kind {
        SOURCE_START{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        TEXT{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        WHITESPACE{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        EOL{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        LINE_COMMENT{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        DOC_COMMENT{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BLOCK_COMMENT{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        INDENTATION_INC{

            @Override
            public boolean isIndentationMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        ELSE_IF_INDENTATION_INC{

            @Override
            public boolean isIndentationMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        INDENTATION_DEC{

            @Override
            public boolean isIndentationMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        ELSE_IF_INDENTATION_DEC{

            @Override
            public boolean isIndentationMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_STATEMENT{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_CASE{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_BLOCK_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        ELSE_IF_AFTER_BLOCK_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_VAR_DECLARATION{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_FUNCTION_DECLARATION_PARAMETER{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_FUNCTION_CALL_ARGUMENT{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_IF_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_ELSE_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_WHILE_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_FOR_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_WITH_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_DO_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_FOR_TEST{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_FOR_MODIFY{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_CHAIN_CALL_DOT{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_CHAIN_CALL_DOT{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_BINARY_OPERATOR_WRAP{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_BINARY_OPERATOR_WRAP{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_ASSIGNMENT_OPERATOR_WRAP{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_TERNARY_OPERATOR_WRAP{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_TERNARY_OPERATOR_WRAP{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_ARRAY_LITERAL_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_ARRAY_LITERAL_END{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_ARRAY_LITERAL_ITEM{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_OBJECT_START{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_OBJECT_END{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        AFTER_PROPERTY{

            @Override
            public boolean isLineWrapMarker() {
                return true;
            }

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_OBJECT{

            @Override
            public boolean isSpaceMarker() {
                return false;
            }
        }
        ,
        BEFORE_BINARY_OPERATOR,
        AFTER_BINARY_OPERATOR,
        BEFORE_ASSIGNMENT_OPERATOR,
        AFTER_ASSIGNMENT_OPERATOR,
        BEFORE_PROPERTY_OPERATOR,
        AFTER_PROPERTY_OPERATOR,
        BEFORE_COMMA,
        AFTER_COMMA,
        BEFORE_DOT,
        AFTER_DOT,
        AFTER_IF_KEYWORD,
        AFTER_WHILE_KEYWORD,
        AFTER_FOR_KEYWORD,
        AFTER_WITH_KEYWORD,
        AFTER_SWITCH_KEYWORD,
        AFTER_CATCH_KEYWORD,
        AFTER_VAR_KEYWORD,
        AFTER_NEW_KEYWORD,
        AFTER_TYPEOF_KEYWORD,
        BEFORE_WHILE_KEYWORD,
        BEFORE_ELSE_KEYWORD,
        BEFORE_CATCH_KEYWORD,
        BEFORE_FINALLY_KEYWORD,
        BEFORE_SEMICOLON,
        AFTER_SEMICOLON,
        BEFORE_UNARY_OPERATOR,
        AFTER_UNARY_OPERATOR,
        BEFORE_TERNARY_OPERATOR,
        AFTER_TERNARY_OPERATOR,
        BEFORE_ANONYMOUS_FUNCTION_DECLARATION,
        BEFORE_FUNCTION_DECLARATION,
        BEFORE_FUNCTION_CALL,
        AFTER_FUNCTION_DECLARATION_PARENTHESIS,
        BEFORE_FUNCTION_DECLARATION_PARENTHESIS,
        AFTER_FUNCTION_CALL_PARENTHESIS,
        BEFORE_FUNCTION_CALL_PARENTHESIS,
        BEFORE_IF_PARENTHESIS,
        AFTER_IF_PARENTHESIS,
        BEFORE_WHILE_PARENTHESIS,
        AFTER_WHILE_PARENTHESIS,
        BEFORE_FOR_PARENTHESIS,
        AFTER_FOR_PARENTHESIS,
        BEFORE_WITH_PARENTHESIS,
        AFTER_WITH_PARENTHESIS,
        BEFORE_SWITCH_PARENTHESIS,
        AFTER_SWITCH_PARENTHESIS,
        BEFORE_CATCH_PARENTHESIS,
        AFTER_CATCH_PARENTHESIS,
        BEFORE_RIGHT_PARENTHESIS,
        AFTER_LEFT_PARENTHESIS,
        BEFORE_RIGHT_BRACE,
        AFTER_LEFT_BRACE,
        BEFORE_FUNCTION_DECLARATION_BRACE,
        BEFORE_IF_BRACE,
        BEFORE_ELSE_BRACE,
        BEFORE_WHILE_BRACE,
        BEFORE_FOR_BRACE,
        BEFORE_DO_BRACE,
        BEFORE_SWITCH_BRACE,
        BEFORE_TRY_BRACE,
        BEFORE_CATCH_BRACE,
        BEFORE_FINALLY_BRACE,
        BEFORE_WITH_BRACE,
        AFTER_ARRAY_LITERAL_BRACKET,
        BEFORE_ARRAY_LITERAL_BRACKET;
        

        private Kind() {
        }

        public boolean isLineWrapMarker() {
            return false;
        }

        public boolean isIndentationMarker() {
            return false;
        }

        public boolean isSpaceMarker() {
            return true;
        }

    }

}

