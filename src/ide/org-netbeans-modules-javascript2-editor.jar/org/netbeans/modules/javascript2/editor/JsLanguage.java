/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.core.spi.multiview.text.MultiViewEditorElement
 *  org.netbeans.modules.csl.api.CodeCompletionHandler
 *  org.netbeans.modules.csl.api.DeclarationFinder
 *  org.netbeans.modules.csl.api.Formatter
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.IndexSearcher
 *  org.netbeans.modules.csl.api.InstantRenamer
 *  org.netbeans.modules.csl.api.OccurrencesFinder
 *  org.netbeans.modules.csl.api.SemanticAnalyzer
 *  org.netbeans.modules.csl.api.StructureScanner
 *  org.netbeans.modules.csl.spi.DefaultLanguageConfig
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.javascript2.editor;

import org.netbeans.api.lexer.Language;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.IndexSearcher;
import org.netbeans.modules.csl.api.InstantRenamer;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.SemanticAnalyzer;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.javascript2.editor.JsCodeCompletion;
import org.netbeans.modules.javascript2.editor.JsSemanticAnalyzer;
import org.netbeans.modules.javascript2.editor.JsStructureScanner;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.classpath.ClassPathProviderImpl;
import org.netbeans.modules.javascript2.editor.formatter.JsFormatter;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;
import org.netbeans.modules.javascript2.editor.model.impl.JsInstantRenamer;
import org.netbeans.modules.javascript2.editor.navigation.DeclarationFinderImpl;
import org.netbeans.modules.javascript2.editor.navigation.JsIndexSearcher;
import org.netbeans.modules.javascript2.editor.navigation.OccurrencesFinderImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParser;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Lookup;

public class JsLanguage
extends DefaultLanguageConfig {
    public static MultiViewEditorElement createMultiViewEditorElement(Lookup context) {
        return new MultiViewEditorElement(context);
    }

    public Language getLexerLanguage() {
        ClassPathProviderImpl.registerJsClassPathIfNeeded();
        return JsTokenId.javascriptLanguage();
    }

    public String getDisplayName() {
        return "JavaScript";
    }

    public Parser getParser() {
        return new JsParser();
    }

    public boolean hasStructureScanner() {
        return true;
    }

    public boolean hasHintsProvider() {
        return true;
    }

    public HintsProvider getHintsProvider() {
        return new JsHintsProvider();
    }

    public StructureScanner getStructureScanner() {
        return new JsStructureScanner(JsTokenId.javascriptLanguage());
    }

    public SemanticAnalyzer getSemanticAnalyzer() {
        return new JsSemanticAnalyzer();
    }

    public DeclarationFinder getDeclarationFinder() {
        return new DeclarationFinderImpl(JsTokenId.javascriptLanguage());
    }

    public boolean hasOccurrencesFinder() {
        return true;
    }

    public OccurrencesFinder getOccurrencesFinder() {
        return new OccurrencesFinderImpl();
    }

    public CodeCompletionHandler getCompletionHandler() {
        return new JsCodeCompletion();
    }

    public String getLineCommentPrefix() {
        return "//";
    }

    public Formatter getFormatter() {
        return new JsFormatter(JsTokenId.javascriptLanguage());
    }

    public boolean hasFormatter() {
        return true;
    }

    public InstantRenamer getInstantRenamer() {
        return new JsInstantRenamer();
    }

    public boolean isIdentifierChar(char c) {
        return super.isIdentifierChar(c) || c == '@';
    }

    public IndexSearcher getIndexSearcher() {
        return new JsIndexSearcher();
    }
}

