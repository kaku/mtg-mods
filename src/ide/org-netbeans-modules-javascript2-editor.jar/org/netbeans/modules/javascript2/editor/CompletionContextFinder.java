/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.spi.CompletionContext;
import org.netbeans.modules.parsing.api.Snapshot;

public class CompletionContextFinder {
    private static final List<JsTokenId> WHITESPACES_TOKENS = Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL});
    private static final List<JsTokenId> CHANGE_CONTEXT_TOKENS = Arrays.asList(new JsTokenId[]{JsTokenId.OPERATOR_SEMICOLON, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY});
    private static final List<Object[]> OBJECT_PROPERTY_TOKENCHAINS = Arrays.asList({JsTokenId.OPERATOR_DOT}, {JsTokenId.OPERATOR_DOT, JsTokenId.IDENTIFIER});
    private static final List<Object[]> OBJECT_THIS_TOKENCHAINS = Arrays.asList({JsTokenId.KEYWORD_THIS, JsTokenId.OPERATOR_DOT}, {JsTokenId.KEYWORD_THIS, JsTokenId.OPERATOR_DOT, JsTokenId.IDENTIFIER});

    @NonNull
    static CompletionContext findCompletionContext(ParserResult info, int offset) {
        TokenHierarchy th = info.getSnapshot().getTokenHierarchy();
        if (th == null) {
            return CompletionContext.NONE;
        }
        TokenSequence ts = th.tokenSequence(JsTokenId.javascriptLanguage());
        if (ts == null) {
            return CompletionContext.NONE;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return CompletionContext.NONE;
        }
        Token token = ts.token();
        JsTokenId tokenId = (JsTokenId)token.id();
        if (tokenId == JsTokenId.OPERATOR_DOT && ts.moveNext()) {
            ts.movePrevious();
            ts.movePrevious();
            token = ts.token();
            tokenId = (JsTokenId)token.id();
        }
        if (tokenId == JsTokenId.STRING || tokenId == JsTokenId.STRING_END) {
            return CompletionContext.STRING;
        }
        if (CompletionContextFinder.acceptTokenChains(ts, OBJECT_THIS_TOKENCHAINS, true)) {
            return CompletionContext.OBJECT_MEMBERS;
        }
        if (CompletionContextFinder.acceptTokenChains(ts, OBJECT_PROPERTY_TOKENCHAINS, tokenId != JsTokenId.OPERATOR_DOT)) {
            return CompletionContext.OBJECT_PROPERTY;
        }
        ts.move(offset);
        if (ts.moveNext() && CompletionContextFinder.isPropertyNameContext(ts)) {
            return CompletionContext.OBJECT_PROPERTY_NAME;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return CompletionContext.GLOBAL;
        }
        token = ts.token();
        tokenId = (JsTokenId)token.id();
        if (tokenId == JsTokenId.EOL && ts.movePrevious()) {
            token = ts.token();
            tokenId = (JsTokenId)token.id();
        }
        if (tokenId == JsTokenId.IDENTIFIER || WHITESPACES_TOKENS.contains((Object)tokenId)) {
            if (!ts.movePrevious()) {
                return CompletionContext.GLOBAL;
            }
            token = LexUtilities.findPrevious(ts, WHITESPACES_TOKENS);
        }
        if (CHANGE_CONTEXT_TOKENS.contains((Object)token.id()) || WHITESPACES_TOKENS.contains((Object)token.id()) && !ts.movePrevious()) {
            return CompletionContext.GLOBAL;
        }
        if (tokenId == JsTokenId.DOC_COMMENT) {
            return CompletionContext.DOCUMENTATION;
        }
        return CompletionContext.EXPRESSION;
    }

    private static boolean isPropertyNameContext(TokenSequence<JsTokenId> ts) {
        Token<? extends JsTokenId> token = null;
        JsTokenId tokenId = (JsTokenId)ts.token().id();
        if (tokenId == JsTokenId.OPERATOR_COMMA) {
            ts.movePrevious();
        }
        List<JsTokenId> listIds = Arrays.asList(new JsTokenId[]{JsTokenId.OPERATOR_COMMA, JsTokenId.OPERATOR_COLON, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.OPERATOR_SEMICOLON});
        token = LexUtilities.findPreviousToken(ts, listIds);
        tokenId = (JsTokenId)token.id();
        boolean commaFirst = false;
        if (tokenId == JsTokenId.OPERATOR_COMMA && ts.movePrevious()) {
            ArrayList<JsTokenId> checkParentList = new ArrayList<JsTokenId>(listIds);
            List<JsTokenId> parentList = Arrays.asList(new JsTokenId[]{JsTokenId.BRACKET_LEFT_PAREN, JsTokenId.BRACKET_RIGHT_PAREN, JsTokenId.BRACKET_RIGHT_CURLY});
            checkParentList.addAll(parentList);
            token = LexUtilities.findPreviousToken(ts, checkParentList);
            tokenId = (JsTokenId)token.id();
            commaFirst = true;
            if (tokenId == JsTokenId.BRACKET_RIGHT_PAREN || tokenId == JsTokenId.BRACKET_RIGHT_CURLY) {
                CompletionContextFinder.balanceBracketBack(ts);
                token = ts.token();
                tokenId = (JsTokenId)token.id();
            } else {
                if (tokenId == JsTokenId.BRACKET_LEFT_PAREN) {
                    return false;
                }
                if (tokenId == JsTokenId.OPERATOR_COLON) {
                    return true;
                }
            }
        }
        if (tokenId == JsTokenId.BRACKET_LEFT_CURLY && ts.movePrevious()) {
            List<JsTokenId> emptyIds = Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.EOL, JsTokenId.BLOCK_COMMENT});
            token = LexUtilities.findPrevious(ts, emptyIds);
            tokenId = (JsTokenId)token.id();
            if (tokenId == JsTokenId.BRACKET_LEFT_PAREN || tokenId == JsTokenId.OPERATOR_COMMA || tokenId == JsTokenId.OPERATOR_EQUALS || tokenId == JsTokenId.OPERATOR_COLON) {
                return true;
            }
            if (tokenId == JsTokenId.BRACKET_RIGHT_PAREN) {
                CompletionContextFinder.balanceBracketBack(ts);
                token = ts.token();
                tokenId = (JsTokenId)token.id();
                if (tokenId == JsTokenId.BRACKET_LEFT_PAREN && ts.movePrevious() && (tokenId = (JsTokenId)(token = LexUtilities.findPrevious(ts, emptyIds)).id()) == JsTokenId.KEYWORD_FUNCTION && ts.movePrevious() && (tokenId = (JsTokenId)(token = LexUtilities.findPrevious(ts, emptyIds)).id()) == JsTokenId.OPERATOR_COLON) {
                    return commaFirst;
                }
            }
        }
        return false;
    }

    private static boolean acceptTokenChains(TokenSequence tokenSequence, List<Object[]> tokenIdChains, boolean movePrevious) {
        for (Object[] tokenIDChain : tokenIdChains) {
            if (!CompletionContextFinder.acceptTokenChain(tokenSequence, tokenIDChain, movePrevious)) continue;
            return true;
        }
        return false;
    }

    private static boolean acceptTokenChain(TokenSequence tokenSequence, Object[] tokenIdChain, boolean movePrevious) {
        int orgTokenSequencePos = tokenSequence.offset();
        boolean accept = true;
        boolean moreTokens = movePrevious ? tokenSequence.movePrevious() : true;
        for (int i = tokenIdChain.length - 1; i >= 0; --i) {
            Object tokenID = tokenIdChain[i];
            if (!moreTokens) {
                accept = false;
                break;
            }
            if (tokenID instanceof JsTokenId) {
                if (tokenSequence.token().id() == tokenID) {
                    moreTokens = tokenSequence.movePrevious();
                    continue;
                }
                accept = false;
                break;
            }
            assert (false);
        }
        tokenSequence.move(orgTokenSequencePos);
        tokenSequence.moveNext();
        return accept;
    }

    private static void balanceBracketBack(TokenSequence<JsTokenId> ts) {
        List<JsTokenId> lookingFor;
        Token<? extends JsTokenId> token = ts.token();
        JsTokenId tokenId = (JsTokenId)ts.token().id();
        JsTokenId tokenIdOriginal = (JsTokenId)ts.token().id();
        if (tokenId == JsTokenId.BRACKET_RIGHT_CURLY) {
            lookingFor = Arrays.asList(new JsTokenId[]{JsTokenId.BRACKET_LEFT_CURLY});
        } else if (tokenId == JsTokenId.BRACKET_RIGHT_PAREN) {
            lookingFor = Arrays.asList(new JsTokenId[]{JsTokenId.BRACKET_LEFT_PAREN});
        } else {
            return;
        }
        int balance = -1;
        while (balance != 0 && ts.movePrevious()) {
            token = LexUtilities.findPreviousToken(ts, lookingFor);
            tokenId = (JsTokenId)token.id();
            if (lookingFor.contains((Object)tokenIdOriginal)) {
                --balance;
                continue;
            }
            if (!lookingFor.contains((Object)tokenId)) continue;
            ++balance;
        }
    }
}

