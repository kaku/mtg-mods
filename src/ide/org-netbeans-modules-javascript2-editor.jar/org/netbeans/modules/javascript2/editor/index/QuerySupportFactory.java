/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.javascript2.editor.index;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class QuerySupportFactory {
    public static QuerySupport get(Collection<FileObject> roots) {
        try {
            return QuerySupport.forRoots((String)"js", (int)13, (FileObject[])roots.toArray((T[])new FileObject[roots.size()]));
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return null;
        }
    }

    public static QuerySupport get(FileObject source) {
        return QuerySupportFactory.get(QuerySupport.findRoots((FileObject)source, (Collection)null, (Collection)null, Collections.emptySet()));
    }
}

