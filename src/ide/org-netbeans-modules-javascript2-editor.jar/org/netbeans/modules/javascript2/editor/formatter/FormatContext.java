/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.spi.Context
 *  org.netbeans.modules.editor.indent.spi.Context$Region
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.embedding.JsEmbeddingProvider;
import org.netbeans.modules.javascript2.editor.formatter.CodeStyle;
import org.netbeans.modules.javascript2.editor.formatter.Defaults;
import org.netbeans.modules.javascript2.editor.formatter.FormatToken;
import org.netbeans.modules.javascript2.editor.formatter.FormatTokenStream;
import org.netbeans.modules.javascript2.editor.formatter.JsFormatter;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;

public final class FormatContext {
    private static final Logger LOGGER = Logger.getLogger(FormatContext.class.getName());
    private static final Pattern SAFE_DELETE_PATTERN = Pattern.compile("\\s*");
    private final Context context;
    private final Snapshot snapshot;
    private final Language<JsTokenId> languange;
    private final Defaults.Provider provider;
    private final int initialStart;
    private final int initialEnd;
    private final List<Region> regions;
    private final boolean embedded;
    private LineWrap lastLineWrap;
    private int indentationLevel;
    private int continuationLevel;
    private int offsetDiff;
    private int currentLineStart;
    private boolean pendingContinuation;
    private int tabCount;

    public FormatContext(Context context, Defaults.Provider provider, Snapshot snapshot, Language<JsTokenId> language) {
        this.context = context;
        this.snapshot = snapshot;
        this.languange = language;
        this.provider = provider;
        this.initialStart = context.startOffset();
        this.initialEnd = context.endOffset();
        this.regions = new ArrayList<Region>(context.indentRegions().size());
        for (Context.Region region2 : context.indentRegions()) {
            this.regions.add(new Region(region2));
        }
        this.dumpRegions();
        this.embedded = JsParserResult.isEmbedded(snapshot);
        if (this.embedded) {
            for (Region region : this.regions) {
                int endOffset = region.getOriginalEnd();
                try {
                    int embeddedOffset;
                    Token token;
                    int lineOffset = context.lineStartOffset(endOffset);
                    TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(snapshot, region.getOriginalStart(), language);
                    if (ts == null || (embeddedOffset = snapshot.getEmbeddedOffset(lineOffset)) < 0) continue;
                    ts.move(embeddedOffset);
                    if (!ts.moveNext() || (token = ts.token()).id() != JsTokenId.WHITESPACE || lineOffset + token.length() != endOffset) continue;
                    region.setOriginalEnd(lineOffset);
                }
                catch (BadLocationException ex) {
                    LOGGER.log(Level.INFO, null, ex);
                }
            }
            LOGGER.log(Level.FINE, "Tuned regions");
            this.dumpRegions();
        }
    }

    public Defaults.Provider getDefaultsProvider() {
        return this.provider;
    }

    public void setLastLineWrap(LineWrap lineWrap) {
        this.lastLineWrap = lineWrap;
    }

    public LineWrap getLastLineWrap() {
        return this.lastLineWrap;
    }

    public int getCurrentLineStart() {
        return this.currentLineStart;
    }

    public void setCurrentLineStart(int currentLineStart) {
        this.currentLineStart = currentLineStart;
    }

    public int getIndentationLevel() {
        return this.indentationLevel;
    }

    public void incIndentationLevel() {
        ++this.indentationLevel;
    }

    public void decIndentationLevel() {
        --this.indentationLevel;
    }

    public int getContinuationLevel() {
        return this.continuationLevel;
    }

    public void incContinuationLevel() {
        ++this.continuationLevel;
    }

    public void decContinuationLevel() {
        --this.continuationLevel;
    }

    public boolean isPendingContinuation() {
        return this.pendingContinuation;
    }

    public void setPendingContinuation(boolean pendingContinuation) {
        this.pendingContinuation = pendingContinuation;
    }

    public void incTabCount() {
        ++this.tabCount;
    }

    public void resetTabCount() {
        this.tabCount = 0;
    }

    public int getTabCount() {
        return this.tabCount;
    }

    public int getOffsetDiff() {
        return this.offsetDiff;
    }

    private void setOffsetDiff(int offsetDiff) {
        this.offsetDiff = offsetDiff;
    }

    private void dumpRegions() {
        if (!LOGGER.isLoggable(Level.FINE)) {
            return;
        }
        for (Region region : this.regions) {
            try {
                LOGGER.log(Level.FINE, "" + region.getOriginalStart() + ":" + region.getOriginalEnd() + ":" + this.getDocument().getText(region.getOriginalStart(), region.getOriginalEnd() - region.getOriginalStart()));
            }
            catch (BadLocationException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
        }
    }

    public int getDocumentOffset(int offset) {
        return this.getDocumentOffset(offset, true);
    }

    private int getDocumentOffset(int offset, boolean check) {
        if (!this.embedded) {
            if (!check || offset >= this.initialStart && offset < this.initialEnd) {
                return offset;
            }
            return -1;
        }
        int docOffset = this.snapshot.getOriginalOffset(offset);
        if (docOffset < 0) {
            return -1;
        }
        for (Region region : this.regions) {
            if (docOffset < region.getOriginalStart() || docOffset >= region.getOriginalEnd()) continue;
            return docOffset;
        }
        return -1;
    }

    public boolean isEmbedded() {
        return this.embedded;
    }

    public int getEmbeddingIndent(FormatTokenStream stream, FormatToken token) {
        if (!this.embedded) {
            return 0;
        }
        int docOffset = this.snapshot.getOriginalOffset(token.getOffset());
        if (docOffset < 0) {
            return 0;
        }
        Region start = null;
        int i = 0;
        for (Region region : this.regions) {
            if (docOffset >= region.getOriginalStart() && docOffset < region.getOriginalEnd()) {
                start = region;
                break;
            }
            ++i;
        }
        if (start != null && start.getInitialIndentation() < 0) {
            try {
                boolean nonEmpty = false;
                FormatToken startToken = stream.getCoveringToken(this.snapshot.getEmbeddedOffset(start.getOriginalStart()));
                if (startToken != null) {
                    for (FormatToken previous = startToken.previous(); previous != null; previous = previous.previous()) {
                        if (!previous.isVirtual()) {
                            if (this.getDocumentOffset(previous.getOffset()) >= 0) {
                                nonEmpty = startToken == FormatTokenStream.getNextNonVirtual(previous);
                                break;
                            }
                            nonEmpty = JsEmbeddingProvider.isGeneratedIdentifier(previous.getText().toString());
                        }
                        if (nonEmpty) break;
                    }
                }
                if (nonEmpty && i > 0) {
                    Region regionWithIndentation = null;
                    for (int j = i - 1; j >= 0; --j) {
                        if (this.regions.get(j).getInitialIndentation() < 0) continue;
                        regionWithIndentation = this.regions.get(j);
                        break;
                    }
                    if (regionWithIndentation != null) {
                        start.setInitialIndentation(regionWithIndentation.getInitialIndentation());
                    } else {
                        start.setInitialIndentation(0);
                    }
                } else if (start.getOriginalStart() <= 0) {
                    start.setInitialIndentation(0);
                } else {
                    start.setInitialIndentation(this.context.lineIndent(this.context.lineStartOffset(start.getContextRegion().getStartOffset())) + IndentUtils.indentLevelSize((Document)this.getDocument()));
                }
            }
            catch (BadLocationException ex) {
                LOGGER.log(Level.INFO, null, ex);
            }
        }
        return start != null ? start.getInitialIndentation() : 0;
    }

    public boolean isGenerated(FormatToken token) {
        return this.embedded && JsEmbeddingProvider.isGeneratedIdentifier(token.getText().toString());
    }

    public BaseDocument getDocument() {
        return (BaseDocument)this.context.document();
    }

    public void indentLine(int voffset, int indentationSize, JsFormatter.Indentation indentationCheck, CodeStyle.Holder codeStyle) {
        this.indentLineWithOffsetDiff(voffset, indentationSize, indentationCheck, this.offsetDiff, codeStyle);
    }

    public void indentLineWithOffsetDiff(int voffset, int indentationSize, JsFormatter.Indentation indentationCheck, int realOffsetDiff, CodeStyle.Holder codeStyle) {
        if (!indentationCheck.isAllowed()) {
            return;
        }
        int offset = this.getDocumentOffset(voffset, !indentationCheck.isExceedLimits());
        if (offset < 0) {
            return;
        }
        try {
            int diff = FormatContext.setLineIndentation(this.getDocument(), offset + realOffsetDiff, indentationSize, codeStyle);
            this.setOffsetDiff(this.offsetDiff + diff);
        }
        catch (BadLocationException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
    }

    public void insert(int voffset, String newString) {
        this.insertWithOffsetDiff(voffset, newString, this.offsetDiff);
    }

    public void insertWithOffsetDiff(int voffset, String newString, int realOffsetDiff) {
        int offset = this.getDocumentOffset(voffset);
        if (offset < 0) {
            return;
        }
        BaseDocument doc = this.getDocument();
        try {
            doc.insertString(offset + realOffsetDiff, newString, null);
            this.setOffsetDiff(this.offsetDiff + newString.length());
        }
        catch (BadLocationException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
    }

    public void replace(int voffset, String oldString, String newString) {
        if (oldString.equals(newString)) {
            return;
        }
        this.replace(voffset, oldString.length(), newString);
    }

    public void replace(int voffset, int vlength, String newString) {
        int offset = this.getDocumentOffset(voffset);
        if (offset < 0) {
            return;
        }
        int length = this.computeLength(voffset, vlength);
        if (length <= 0) {
            this.insert(voffset, newString);
            return;
        }
        BaseDocument doc = this.getDocument();
        try {
            String oldText = doc.getText(offset + this.offsetDiff, length);
            if (newString.equals(oldText)) {
                return;
            }
            if (SAFE_DELETE_PATTERN.matcher(oldText).matches()) {
                doc.remove(offset + this.offsetDiff, length);
                doc.insertString(offset + this.offsetDiff, newString, null);
                this.setOffsetDiff(this.offsetDiff + (newString.length() - length));
            } else {
                LOGGER.log(Level.WARNING, "Tried to remove non empty text: {0}", doc.getText(offset + this.offsetDiff, length));
            }
        }
        catch (BadLocationException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
    }

    public void remove(int voffset, int vlength) {
        int offset = this.getDocumentOffset(voffset);
        if (offset < 0) {
            return;
        }
        int length = this.computeLength(voffset, vlength);
        if (length <= 0) {
            return;
        }
        BaseDocument doc = this.getDocument();
        try {
            if (SAFE_DELETE_PATTERN.matcher(doc.getText(offset + this.offsetDiff, length)).matches()) {
                doc.remove(offset + this.offsetDiff, length);
                this.setOffsetDiff(this.offsetDiff - length);
            } else {
                LOGGER.log(Level.WARNING, "Tried to remove non empty text: {0}", doc.getText(offset + this.offsetDiff, length));
            }
        }
        catch (BadLocationException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
    }

    private int computeLength(int voffset, int length) {
        if (!this.embedded) {
            return length;
        }
        for (int i = 0; i < length; ++i) {
            if (this.getDocumentOffset(voffset + i) >= 0) continue;
            return i;
        }
        return length;
    }

    private static int setLineIndentation(BaseDocument doc, int lineOffset, int newIndent, CodeStyle.Holder codeStyle) throws BadLocationException {
        char ch;
        int i;
        int oldIndentEndOffset;
        int lineStartOffset = Utilities.getRowStart((BaseDocument)doc, (int)lineOffset);
        int indent = 0;
        int tabSize = -1;
        CharSequence docText = DocumentUtilities.getText((Document)doc);
        for (oldIndentEndOffset = lineStartOffset; oldIndentEndOffset < docText.length() && (ch = docText.charAt(oldIndentEndOffset)) != '\n'; ++oldIndentEndOffset) {
            if (ch == '\t') {
                if (tabSize == -1) {
                    tabSize = codeStyle.tabSize;
                }
                indent = (indent + tabSize) / tabSize * tabSize;
                continue;
            }
            if (!Character.isWhitespace(ch)) break;
            ++indent;
        }
        String newIndentString = IndentUtils.createIndentString((int)newIndent, (boolean)codeStyle.expandTabsToSpaces, (int)codeStyle.tabSize);
        int offset = lineStartOffset;
        boolean different = false;
        for (i = 0; i < newIndentString.length() && lineStartOffset + i < oldIndentEndOffset; ++i) {
            if (newIndentString.charAt(i) == docText.charAt(lineStartOffset + i)) continue;
            offset = lineStartOffset + i;
            newIndentString = newIndentString.substring(i);
            different = true;
            break;
        }
        if (!different) {
            offset = lineStartOffset + i;
            newIndentString = newIndentString.substring(i);
        }
        if (offset < oldIndentEndOffset) {
            doc.remove(offset, oldIndentEndOffset - offset);
        }
        if (newIndentString.length() > 0) {
            doc.insertString(offset, newIndentString, null);
        }
        return newIndentString.length() - (oldIndentEndOffset - offset);
    }

    private static class Region {
        private final Context.Region contextRegion;
        private final int originalStart;
        private int originalEnd;
        private int initialIndentation = -1;

        public Region(Context.Region contextRegion) {
            this.contextRegion = contextRegion;
            this.originalStart = contextRegion.getStartOffset();
            this.originalEnd = contextRegion.getEndOffset();
        }

        public Context.Region getContextRegion() {
            return this.contextRegion;
        }

        public int getOriginalStart() {
            return this.originalStart;
        }

        public int getOriginalEnd() {
            return this.originalEnd;
        }

        public void setOriginalEnd(int originalEnd) {
            this.originalEnd = originalEnd;
        }

        public int getInitialIndentation() {
            return this.initialIndentation;
        }

        public void setInitialIndentation(int initialIndentation) {
            this.initialIndentation = initialIndentation;
        }
    }

    public static class ContinuationBlock {
        private final Type type;
        private final boolean change;

        public ContinuationBlock(Type type, boolean change) {
            this.type = type;
            this.change = change;
        }

        public Type getType() {
            return this.type;
        }

        public boolean isChange() {
            return this.change;
        }

        public static enum Type {
            CURLY,
            BRACKET,
            PAREN;
            

            private Type() {
            }
        }

    }

    public static class LineWrap {
        private final FormatToken token;
        private final int offsetDiff;
        private final int indentationLevel;
        private final int continuationLevel;

        public LineWrap(FormatToken token, int offsetDiff, int indentationLevel, int continuationLevel) {
            this.token = token;
            this.offsetDiff = offsetDiff;
            this.indentationLevel = indentationLevel;
            this.continuationLevel = continuationLevel;
        }

        public FormatToken getToken() {
            return this.token;
        }

        public int getOffsetDiff() {
            return this.offsetDiff;
        }

        public int getIndentationLevel() {
            return this.indentationLevel;
        }

        public int getContinuationLevel() {
            return this.continuationLevel;
        }
    }

}

