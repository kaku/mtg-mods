/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.Bundle;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public class JsDocumentationPrinter {
    private static final String WRAPPER_HEADER = "h3";
    private static final String WRAPPER_SUBHEADER = "h4";
    private static final String TABLE_BEGIN = "<table style=\"margin-left: 10px;\">\n";
    private static final String PARAGRAPH_BEGIN = "<p style=\"margin: 0px 14px 0px 14px;\">";
    private static final String OPTIONAL_PARAMETER = "[optional]";
    private static final String OPTIONAL_PARAMETER_DEFAULT = "[optional, default=%s]";

    private JsDocumentationPrinter() {
    }

    public static String printDocumentation(JsComment jsComment) {
        StringBuilder sb = new StringBuilder();
        sb.append(JsDocumentationPrinter.printSyntax(jsComment));
        sb.append(JsDocumentationPrinter.printDeprecated(jsComment));
        sb.append(JsDocumentationPrinter.printSummary(jsComment));
        sb.append(JsDocumentationPrinter.printParameters(jsComment));
        sb.append(JsDocumentationPrinter.printReturns(jsComment));
        sb.append(JsDocumentationPrinter.printExtends(jsComment));
        sb.append(JsDocumentationPrinter.printThrows(jsComment));
        sb.append(JsDocumentationPrinter.printExamples(jsComment));
        sb.append(JsDocumentationPrinter.printSince(jsComment));
        sb.append(JsDocumentationPrinter.printSee(jsComment));
        return sb.toString();
    }

    public static String printParameterDocumentation(DocParameter docParameter) {
        StringBuilder sb = new StringBuilder();
        sb.append(JsDocumentationPrinter.renderHeader("h3", Bundle.JsDocumentationPrinter_title_type()));
        sb.append(JsDocumentationPrinter.renderSingleValueFromTypes(docParameter.getParamTypes()));
        sb.append(JsDocumentationPrinter.renderHeader("h3", Bundle.JsDocumentationPrinter_title_description()));
        sb.append(JsDocumentationPrinter.renderSingleValue(docParameter.getParamDescription()));
        return sb.toString();
    }

    private static String printDeprecated(JsComment jsComment) {
        if (jsComment.getDeprecated() == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("<p style=\"margin: 0px 5px 0px 5px;\"><b>");
        sb.append(Bundle.JsDocumentationPrinter_title_deprecated()).append(".</b>\n");
        if (!jsComment.getDeprecated().isEmpty()) {
            sb.append(" <i>").append(jsComment.getDeprecated()).append("</i>\n");
        }
        sb.append("</p>\n");
        return sb.toString();
    }

    private static String printSyntax(JsComment jsComment) {
        List<String> syntax = jsComment.getSyntax();
        if (!syntax.isEmpty()) {
            StringBuilder sb = new StringBuilder("<p style=\"background-color: #C7C7C7; width: 100%; padding: 3px; margin: 10 5 3 5;\">\n");
            for (String descElement : syntax) {
                sb.append(descElement).append("<br>\n");
            }
            sb.append("</p>\n");
            return sb.toString();
        }
        return "";
    }

    private static String printSummary(JsComment jsComment) {
        List<String> summary = jsComment.getSummary();
        if (!summary.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (String descElement : summary) {
                sb.append("<p style=\"margin: 5px 5px 0px 5px;\">\n");
                sb.append(descElement).append("\n");
                sb.append("</p>\n");
            }
            return sb.toString();
        }
        return "";
    }

    private static String printParameters(JsComment jsComment) {
        List<DocParameter> parameters = jsComment.getParameters();
        if (!parameters.isEmpty()) {
            StringBuilder sb = new StringBuilder(JsDocumentationPrinter.renderHeader("h3", Bundle.JsDocumentationPrinter_title_parameters()));
            sb.append("<table style=\"margin-left: 10px;\">\n");
            for (DocParameter docParam : parameters) {
                String paramName = docParam.getParamName() == null ? "" : docParam.getParamName().getName();
                sb.append("<tr>\n");
                sb.append("<td valign=\"top\" style=\"margin-right:5px;\">").append(JsDocumentationPrinter.getStringFromTypes(docParam.getParamTypes())).append("</td>\n");
                sb.append("<td valign=\"top\" style=\"margin-right:5px;\"><b>").append(paramName).append("</b></td>\n");
                String description = docParam.getParamDescription();
                if (docParam.isOptional()) {
                    description = docParam.getDefaultValue() == null || docParam.getDefaultValue().isEmpty() ? "[optional]<br>" + description : String.format("[optional, default=%s]", docParam.getDefaultValue()) + "<br>" + description;
                }
                sb.append("<td>").append(description).append("</td>\n");
                sb.append("</tr>\n");
            }
            sb.append("</table>\n");
            return sb.toString();
        }
        return "";
    }

    private static String printReturns(JsComment jsComment) {
        DocParameter returns = jsComment.getReturnType();
        if (returns != null) {
            StringBuilder sb = new StringBuilder(JsDocumentationPrinter.renderHeader("h3", Bundle.JsDocumentationPrinter_title_returns()));
            sb.append("<table style=\"margin-left: 10px;\">\n");
            if (!returns.getParamTypes().isEmpty()) {
                sb.append("<tr>\n");
                sb.append("<td valign=\"top\" style=\"margin-right:5px;\"><b>");
                sb.append(Bundle.JsDocumentationPrinter_title_type());
                sb.append(":</b></td>\n");
                sb.append("<td valign=\"top\">").append(JsDocumentationPrinter.getStringFromTypes(returns.getParamTypes())).append("</td>\n");
                sb.append("</tr>\n");
            }
            if (!returns.getParamDescription().isEmpty()) {
                sb.append("<tr>\n");
                sb.append("<td valign=\"top\" style=\"margin-right:5px;\"><b>");
                sb.append(Bundle.JsDocumentationPrinter_title_description());
                sb.append(":</b></td>\n");
                sb.append("<td valign=\"top\">").append(returns.getParamDescription()).append("</td>\n");
                sb.append("</tr>\n");
            }
            sb.append("</table>\n");
            return sb.toString();
        }
        return "";
    }

    private static String printThrows(JsComment jsComment) {
        List<DocParameter> throwsList = jsComment.getThrows();
        if (!throwsList.isEmpty()) {
            StringBuilder sb = new StringBuilder(JsDocumentationPrinter.renderHeader("h4", Bundle.JsDocumentationPrinter_title_throws()));
            sb.append("<table style=\"margin-left: 10px;\">\n");
            for (DocParameter throwClause : throwsList) {
                sb.append("<tr>\n");
                if (!throwClause.getParamTypes().isEmpty()) {
                    sb.append("<td valign=\"top\">").append(JsDocumentationPrinter.getStringFromTypes(throwClause.getParamTypes())).append("</td>\n");
                } else {
                    sb.append("<td></td>\n");
                }
                if (!throwClause.getParamDescription().isEmpty()) {
                    sb.append("<td valign=\"top\">").append(throwClause.getParamDescription()).append("</td>\n");
                } else {
                    sb.append("<td></td>\n");
                }
                sb.append("</tr>\n");
            }
            sb.append("</table>\n");
            return sb.toString();
        }
        return "";
    }

    private static String printExtends(JsComment jsComment) {
        List<Type> extendsList = jsComment.getExtends();
        if (!extendsList.isEmpty()) {
            return JsDocumentationPrinter.renderHeader("h4", Bundle.JsDocumentationPrinter_title_extends()) + JsDocumentationPrinter.renderSingleValueFromTypes(extendsList);
        }
        return "";
    }

    private static String printSince(JsComment jsComment) {
        String since = jsComment.getSince();
        if (since != null && !since.isEmpty()) {
            return JsDocumentationPrinter.renderHeader("h4", Bundle.JsDocumentationPrinter_title_since()) + JsDocumentationPrinter.renderSingleValue(since);
        }
        return "";
    }

    private static String printExamples(JsComment jsComment) {
        List<String> examples = jsComment.getExamples();
        if (!examples.isEmpty()) {
            return JsDocumentationPrinter.renderHeader("h4", Bundle.JsDocumentationPrinter_title_examples()) + JsDocumentationPrinter.renderLines(examples);
        }
        return "";
    }

    private static String printSee(JsComment jsComment) {
        List<String> sees = jsComment.getSee();
        if (!sees.isEmpty()) {
            return JsDocumentationPrinter.renderHeader("h4", Bundle.JsDocumentationPrinter_title_see()) + JsDocumentationPrinter.renderLines(sees);
        }
        return "";
    }

    private static String renderHeader(String headerType, String header) {
        StringBuilder sb = new StringBuilder("<").append(headerType).append(" style=\"margin: 10px 0px 5px 0px\">");
        sb.append(header).append(":");
        sb.append("</").append(headerType).append(">\n");
        return sb.toString();
    }

    private static String renderLines(List<String> lines) {
        StringBuilder sb = new StringBuilder();
        sb.append("<table style=\"margin-left: 10px;\">\n");
        for (String line : lines) {
            sb.append("<tr>\n");
            sb.append("<td valign=\"top\">").append(line).append("</td>\n");
            sb.append("</tr>\n");
        }
        sb.append("</table>\n");
        return sb.toString();
    }

    private static String renderSingleValue(String value) {
        StringBuilder sb = new StringBuilder();
        sb.append("<p style=\"margin: 0px 14px 0px 14px;\">").append(value).append("</p>\n");
        return sb.toString();
    }

    private static String renderSingleValueFromStrings(List<String> values) {
        StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (String author : values) {
            sb.append(delimiter).append(author);
            delimiter = ", ";
        }
        return JsDocumentationPrinter.renderSingleValue(sb.toString());
    }

    private static String renderSingleValueFromTypes(List<Type> types) {
        LinkedList<String> values = new LinkedList<String>();
        for (Type type : types) {
            values.add(type.getType());
        }
        return JsDocumentationPrinter.renderSingleValueFromStrings(values);
    }

    private static String getStringFromTypes(List<? extends Type> types) {
        StringBuilder sb = new StringBuilder();
        String delimiter = "";
        for (Type type : types) {
            sb.append(delimiter).append(type.getType());
            delimiter = " | ";
        }
        return sb.toString();
    }
}

