/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.netbeans.modules.options.editor.spi.PreviewProvider
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.javascript2.editor.formatter.CodeStyle;
import org.netbeans.modules.javascript2.editor.formatter.Defaults;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.editor.spi.PreviewProvider;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class FmtOptions {
    private static final Logger LOGGER = Logger.getLogger(FmtOptions.class.getName());
    public static final String expandTabToSpaces = "expand-tabs";
    public static final String tabSize = "tab-size";
    public static final String spacesPerTab = "spaces-per-tab";
    public static final String indentSize = "indent-shift-width";
    public static final String continuationIndentSize = "continuationIndentSize";
    public static final String itemsInArrayDeclarationIndentSize = "itemsInArrayDeclarationIndentSize";
    public static final String reformatComments = "reformatComments";
    public static final String indentHtml = "indentHtml";
    public static final String rightMargin = "text-limit-width";
    public static final String initialIndent = "init.indent";
    public static final String classDeclBracePlacement = "classDeclBracePlacement";
    public static final String methodDeclBracePlacement = "methodDeclBracePlacement";
    public static final String ifBracePlacement = "ifBracePlacement";
    public static final String forBracePlacement = "forBracePlacement";
    public static final String whileBracePlacement = "whileBracePlacement";
    public static final String switchBracePlacement = "switchBracePlacement";
    public static final String catchBracePlacement = "catchBracePlacement";
    public static final String useTraitBodyBracePlacement = "useTraitBodyBracePlacement";
    public static final String otherBracePlacement = "otherBracePlacement";
    public static final String blankLinesBeforeNamespace = "blankLinesBeforeNamespace";
    public static final String blankLinesAfterNamespace = "blankLinesAfterNamespace";
    public static final String blankLinesBeforeUse = "blankLinesBeforeUse";
    public static final String blankLinesBeforeUseTrait = "blankLinesBeforeUseTrait";
    public static final String blankLinesAfterUse = "blankLinesAfterUse";
    public static final String blankLinesBeforeClass = "blankLinesBeforeClass";
    public static final String blankLinesBeforeClassEnd = "blankLinesBeforeClassEnd";
    public static final String blankLinesAfterClass = "blankLinesAfterClass";
    public static final String blankLinesAfterClassHeader = "blankLinesAfterClassHeader";
    public static final String blankLinesBeforeFields = "blankLinesBeforeField";
    public static final String blankLinesBetweenFields = "blankLinesBetweenField";
    public static final String blankLinesAfterFields = "blankLinesAfterField";
    public static final String blankLinesGroupFieldsWithoutDoc = "blankLinesGroupFieldsWithoutDoc";
    public static final String blankLinesBeforeFunction = "blankLinesBeforeFunction";
    public static final String blankLinesAfterFunction = "blankLinesAfterFunction";
    public static final String blankLinesBeforeFunctionEnd = "blankLinesBeforeFunctionEnd";
    public static final String blankLinesAfterOpenPHPTag = "blankLinesAfterOpenPHPTag";
    public static final String blankLinesAfterOpenPHPTagInHTML = "blankLinesAfterOpenPHPTagInHTML";
    public static final String blankLinesBeforeClosePHPTag = "blankLinesBeforeClosePHPTag";
    public static final String spaceBeforeWhile = "spaceBeforeWhile";
    public static final String spaceBeforeElse = "spaceBeforeElse";
    public static final String spaceBeforeCatch = "spaceBeforeCatch";
    public static final String spaceBeforeFinally = "spaceBeforeFinally";
    public static final String spaceBeforeAnonMethodDeclParen = "spaceBeforeAnonMethodDeclParen";
    public static final String spaceBeforeMethodDeclParen = "spaceBeforeMethodDeclParen";
    public static final String spaceBeforeMethodCallParen = "spaceBeforeMethodCallParen";
    public static final String spaceBeforeIfParen = "spaceBeforeIfParen";
    public static final String spaceBeforeForParen = "spaceBeforeForParen";
    public static final String spaceBeforeWhileParen = "spaceBeforeWhileParen";
    public static final String spaceBeforeCatchParen = "spaceBeforeCatchParen";
    public static final String spaceBeforeSwitchParen = "spaceBeforeSwitchParen";
    public static final String spaceBeforeWithParen = "spaceBeforeWithParen";
    public static final String spaceAroundUnaryOps = "spaceAroundUnaryOps";
    public static final String spaceAroundBinaryOps = "spaceAroundBinaryOps";
    public static final String spaceAroundTernaryOps = "spaceAroundTernaryOps";
    public static final String spaceAroundStringConcatOps = "spaceAroundStringConcatOps";
    public static final String spaceAroundAssignOps = "spaceAroundAssignOps";
    public static final String spaceAroundKeyValueOps = "spaceAroundKeyValueOps";
    public static final String spaceAroundObjectOps = "spaceAroundObjectOps";
    public static final String spaceBeforeClassDeclLeftBrace = "spaceBeforeClassDeclLeftBrace";
    public static final String spaceBeforeMethodDeclLeftBrace = "spaceBeforeMethodDeclLeftBrace";
    public static final String spaceBeforeIfLeftBrace = "spaceBeforeIfLeftBrace";
    public static final String spaceBeforeElseLeftBrace = "spaceBeforeElseLeftBrace";
    public static final String spaceBeforeWhileLeftBrace = "spaceBeforeWhileLeftBrace";
    public static final String spaceBeforeForLeftBrace = "spaceBeforeForLeftBrace";
    public static final String spaceBeforeDoLeftBrace = "spaceBeforeDoLeftBrace";
    public static final String spaceBeforeSwitchLeftBrace = "spaceBeforeSwitchLeftBrace";
    public static final String spaceBeforeTryLeftBrace = "spaceBeforeTryLeftBrace";
    public static final String spaceBeforeCatchLeftBrace = "spaceBeforeCatchLeftBrace";
    public static final String spaceBeforeFinallyLeftBrace = "spaceBeforeFinallyLeftBrace";
    public static final String spaceBeforeWithLeftBrace = "spaceBeforeWithLeftBrace";
    public static final String spaceWithinParens = "spaceWithinParens";
    public static final String spaceWithinArrayDeclParens = "spaceWithinArrayDeclParens";
    public static final String spaceWithinMethodDeclParens = "spaceWithinMethodDeclParens";
    public static final String spaceWithinMethodCallParens = "spaceWithinMethodCallParens";
    public static final String spaceWithinIfParens = "spaceWithinIfParens";
    public static final String spaceWithinForParens = "spaceWithinForParens";
    public static final String spaceWithinWhileParens = "spaceWithinWhileParens";
    public static final String spaceWithinSwitchParens = "spaceWithinSwitchParens";
    public static final String spaceWithinCatchParens = "spaceWithinCatchParens";
    public static final String spaceWithinWithParens = "spaceWithinWithParens";
    public static final String spaceWithinTypeCastParens = "spaceWithinTypeCastParens";
    public static final String spaceWithinBraces = "spaceWithinBraces";
    public static final String spaceWithinArrayBrackets = "spaceWithinArrayBrackets";
    public static final String spaceBeforeComma = "spaceBeforeComma";
    public static final String spaceAfterComma = "spaceAfterComma";
    public static final String spaceBeforeSemi = "spaceBeforeSemi";
    public static final String spaceAfterSemi = "spaceAfterSemi";
    public static final String spaceBeforeColon = "spaceBeforeColon";
    public static final String spaceAfterColon = "spaceAfterColon";
    public static final String placeElseOnNewLine = "placeElseOnNewLine";
    public static final String placeWhileOnNewLine = "placeWhileOnNewLine";
    public static final String placeCatchOnNewLine = "placeCatchOnNewLine";
    public static final String placeNewLineAfterModifiers = "placeNewLineAfterModifiers";
    public static final String alignMultilineMethodParams = "alignMultilineMethodParams";
    public static final String alignMultilineCallArgs = "alignMultilineCallArgs";
    public static final String alignMultilineImplements = "alignMultilineImplements";
    public static final String alignMultilineParenthesized = "alignMultilineParenthesized";
    public static final String alignMultilineBinaryOp = "alignMultilineBinaryOp";
    public static final String alignMultilineTernaryOp = "alignMultilineTernaryOp";
    public static final String alignMultilineAssignment = "alignMultilineAssignment";
    public static final String alignMultilineFor = "alignMultilineFor";
    public static final String alignMultilineArrayInit = "alignMultilineArrayInit";
    public static final String groupAlignmentAssignment = "groupAlignmentAssignment";
    public static final String groupAlignmentArrayInit = "groupAlignmentArrayInit";
    public static final String wrapStatement = "wrapStatement";
    public static final String wrapVariables = "wrapVariables";
    public static final String wrapMethodParams = "wrapMethodParams";
    public static final String wrapMethodCallArgs = "wrapMethodCallArgs";
    public static final String wrapChainedMethodCalls = "wrapChainedMethodCalls";
    public static final String wrapAfterDotInChainedMethodCalls = "wrapAfterDotInChainedMethodCalls";
    public static final String wrapArrayInit = "wrapArrayInit";
    public static final String wrapArrayInitItems = "wrapArrayInitItems";
    public static final String wrapFor = "wrapFor";
    public static final String wrapForStatement = "wrapForStatement";
    public static final String wrapIfStatement = "wrapIfStatement";
    public static final String wrapWhileStatement = "wrapWhileStatement";
    public static final String wrapDoWhileStatement = "wrapDoWhileStatement";
    public static final String wrapWithStatement = "wrapWithStatement";
    public static final String wrapBinaryOps = "wrapBinaryOps";
    public static final String wrapAfterBinaryOps = "wrapAfterBinaryOps";
    public static final String wrapTernaryOps = "wrapTernaryOps";
    public static final String wrapAfterTernaryOps = "wrapAfterTernaryOps";
    public static final String wrapAssignOps = "wrapAssignOps";
    public static final String wrapBlockBraces = "wrapBlockBraces";
    public static final String wrapStatementsOnTheLine = "wrapStateMentsOnTheLine";
    public static final String wrapObjects = "wrapObjects";
    public static final String wrapProperties = "wrapProperties";
    public static final String preferFullyQualifiedNames = "preferFullyQualifiedNames";
    public static final String preferMultipleUseStatementsCombined = "preferMultipleUseStatementsCombined";
    public static final String startUseWithNamespaceSeparator = "startUseWithNamespaceSeparator";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    public static final String OBRACE_NEWLINE = CodeStyle.BracePlacement.NEW_LINE.name();
    public static final String OBRACE_SAMELINE = CodeStyle.BracePlacement.SAME_LINE.name();
    public static final String OBRACE_PRESERVE = CodeStyle.BracePlacement.PRESERVE_EXISTING.name();
    public static final String OBRACE_NEWLINE_INDENTED = CodeStyle.BracePlacement.NEW_LINE_INDENTED.name();
    public static final String WRAP_ALWAYS = CodeStyle.WrapStyle.WRAP_ALWAYS.name();
    public static final String WRAP_IF_LONG = CodeStyle.WrapStyle.WRAP_IF_LONG.name();
    public static final String WRAP_NEVER = CodeStyle.WrapStyle.WRAP_NEVER.name();
    private static Map<String, String> defaults;

    private FmtOptions() {
    }

    private static int getDefaultAsInt(String key) {
        return Integer.parseInt(defaults.get(key));
    }

    private static boolean getDefaultAsBoolean(String key) {
        return Boolean.parseBoolean(defaults.get(key));
    }

    private static String getDefaultAsString(String key) {
        return defaults.get(key);
    }

    private static void createDefaults() {
        String[][] defaultValues = new String[][]{{"expand-tabs", "true"}, {"tab-size", "8"}, {"indent-shift-width", "4"}, {"continuationIndentSize", "8"}, {"itemsInArrayDeclarationIndentSize", "4"}, {"reformatComments", "false"}, {"indentHtml", "true"}, {"text-limit-width", "80"}, {"init.indent", "0"}, {"classDeclBracePlacement", OBRACE_SAMELINE}, {"methodDeclBracePlacement", OBRACE_SAMELINE}, {"ifBracePlacement", OBRACE_SAMELINE}, {"forBracePlacement", OBRACE_SAMELINE}, {"whileBracePlacement", OBRACE_SAMELINE}, {"switchBracePlacement", OBRACE_SAMELINE}, {"catchBracePlacement", OBRACE_SAMELINE}, {"useTraitBodyBracePlacement", OBRACE_SAMELINE}, {"otherBracePlacement", OBRACE_SAMELINE}, {"blankLinesBeforeNamespace", "1"}, {"blankLinesAfterNamespace", "1"}, {"blankLinesBeforeUse", "1"}, {"blankLinesBeforeUseTrait", "1"}, {"blankLinesAfterUse", "1"}, {"blankLinesBeforeClass", "1"}, {"blankLinesAfterClass", "1"}, {"blankLinesAfterClassHeader", "0"}, {"blankLinesBeforeClassEnd", "0"}, {"blankLinesBeforeField", "1"}, {"blankLinesGroupFieldsWithoutDoc", "true"}, {"blankLinesBetweenField", "1"}, {"blankLinesAfterField", "1"}, {"blankLinesBeforeFunction", "1"}, {"blankLinesAfterFunction", "1"}, {"blankLinesBeforeFunctionEnd", "0"}, {"blankLinesAfterOpenPHPTag", "1"}, {"blankLinesAfterOpenPHPTagInHTML", "0"}, {"blankLinesBeforeClosePHPTag", "0"}, {"spaceBeforeWhile", "true"}, {"spaceBeforeElse", "true"}, {"spaceBeforeCatch", "true"}, {"spaceBeforeFinally", "true"}, {"spaceBeforeAnonMethodDeclParen", "true"}, {"spaceBeforeMethodDeclParen", "false"}, {"spaceBeforeMethodCallParen", "false"}, {"spaceBeforeIfParen", "true"}, {"spaceBeforeForParen", "true"}, {"spaceBeforeWhileParen", "true"}, {"spaceBeforeCatchParen", "true"}, {"spaceBeforeSwitchParen", "true"}, {"spaceBeforeWithParen", "true"}, {"spaceAroundUnaryOps", "false"}, {"spaceAroundBinaryOps", "true"}, {"spaceAroundTernaryOps", "true"}, {"spaceAroundStringConcatOps", "true"}, {"spaceAroundKeyValueOps", "true"}, {"spaceAroundAssignOps", "true"}, {"spaceAroundObjectOps", "false"}, {"spaceBeforeClassDeclLeftBrace", "true"}, {"spaceBeforeMethodDeclLeftBrace", "true"}, {"spaceBeforeIfLeftBrace", "true"}, {"spaceBeforeElseLeftBrace", "true"}, {"spaceBeforeWhileLeftBrace", "true"}, {"spaceBeforeForLeftBrace", "true"}, {"spaceBeforeDoLeftBrace", "true"}, {"spaceBeforeSwitchLeftBrace", "true"}, {"spaceBeforeTryLeftBrace", "true"}, {"spaceBeforeCatchLeftBrace", "true"}, {"spaceBeforeFinallyLeftBrace", "true"}, {"spaceBeforeWithLeftBrace", "true"}, {"spaceWithinParens", "false"}, {"spaceWithinArrayDeclParens", "false"}, {"spaceWithinMethodDeclParens", "false"}, {"spaceWithinMethodCallParens", "false"}, {"spaceWithinIfParens", "false"}, {"spaceWithinForParens", "false"}, {"spaceWithinWhileParens", "false"}, {"spaceWithinSwitchParens", "false"}, {"spaceWithinCatchParens", "false"}, {"spaceWithinWithParens", "false"}, {"spaceWithinTypeCastParens", "false"}, {"spaceWithinBraces", "false"}, {"spaceWithinArrayBrackets", "false"}, {"spaceBeforeComma", "false"}, {"spaceAfterComma", "true"}, {"spaceBeforeSemi", "false"}, {"spaceAfterSemi", "true"}, {"spaceBeforeColon", "false"}, {"spaceAfterColon", "true"}, {"alignMultilineMethodParams", "false"}, {"alignMultilineCallArgs", "false"}, {"alignMultilineImplements", "false"}, {"alignMultilineParenthesized", "false"}, {"alignMultilineBinaryOp", "false"}, {"alignMultilineTernaryOp", "false"}, {"alignMultilineAssignment", "false"}, {"alignMultilineFor", "false"}, {"alignMultilineArrayInit", "false"}, {"placeElseOnNewLine", "false"}, {"placeWhileOnNewLine", "false"}, {"placeCatchOnNewLine", "false"}, {"placeNewLineAfterModifiers", "false"}, {"groupAlignmentArrayInit", "false"}, {"groupAlignmentAssignment", "false"}, {"wrapStatement", WRAP_ALWAYS}, {"wrapVariables", WRAP_NEVER}, {"wrapMethodParams", WRAP_NEVER}, {"wrapMethodCallArgs", WRAP_NEVER}, {"wrapChainedMethodCalls", WRAP_NEVER}, {"wrapAfterDotInChainedMethodCalls", "true"}, {"wrapArrayInit", WRAP_NEVER}, {"wrapArrayInitItems", WRAP_NEVER}, {"wrapFor", WRAP_NEVER}, {"wrapForStatement", WRAP_ALWAYS}, {"wrapIfStatement", WRAP_ALWAYS}, {"wrapWhileStatement", WRAP_ALWAYS}, {"wrapDoWhileStatement", WRAP_ALWAYS}, {"wrapWithStatement", WRAP_ALWAYS}, {"wrapBinaryOps", WRAP_NEVER}, {"wrapAfterBinaryOps", "false"}, {"wrapTernaryOps", WRAP_NEVER}, {"wrapAfterTernaryOps", "false"}, {"wrapAssignOps", WRAP_NEVER}, {"wrapBlockBraces", "true"}, {"wrapStateMentsOnTheLine", "true"}, {"wrapObjects", WRAP_NEVER}, {"wrapProperties", WRAP_NEVER}, {"preferFullyQualifiedNames", "false"}, {"preferMultipleUseStatementsCombined", "false"}, {"startUseWithNamespaceSeparator", "false"}};
        defaults = new HashMap<String, String>();
        for (String[] strings : defaultValues) {
            defaults.put(strings[0], strings[1]);
        }
    }

    public static boolean isInteger(String optionID) {
        String value = defaults.get(optionID);
        try {
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException numberFormatException) {
            return false;
        }
    }

    static {
        FmtOptions.createDefaults();
    }

    public static class BasicDefaultsProvider
    implements Defaults.Provider {
        @Override
        public int getDefaultAsInt(String key) {
            return FmtOptions.getDefaultAsInt(key);
        }

        @Override
        public boolean getDefaultAsBoolean(String key) {
            return FmtOptions.getDefaultAsBoolean(key);
        }

        @Override
        public String getDefaultAsString(String key) {
            return FmtOptions.getDefaultAsString(key);
        }
    }

    public static final class ProxyPreferences
    extends AbstractPreferences {
        private final Preferences[] delegates;

        public /* varargs */ ProxyPreferences(Preferences ... delegates) {
            super(null, "");
            this.delegates = delegates;
        }

        @Override
        protected void putSpi(String key, String value) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String getSpi(String key) {
            for (Preferences p : this.delegates) {
                String value = p.get(key, null);
                if (value == null) continue;
                return value;
            }
            return null;
        }

        @Override
        protected void removeSpi(String key) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            HashSet<String> keys = new HashSet<String>();
            for (Preferences p : this.delegates) {
                keys.addAll(Arrays.asList(p.keys()));
            }
            return keys.toArray(new String[keys.size()]);
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public static class PreviewPreferences
    extends AbstractPreferences {
        private Map<String, Object> map = new HashMap<String, Object>();

        public PreviewPreferences() {
            super(null, "");
        }

        @Override
        protected void putSpi(String key, String value) {
            this.map.put(key, value);
        }

        @Override
        protected String getSpi(String key) {
            return (String)this.map.get(key);
        }

        @Override
        protected void removeSpi(String key) {
            this.map.remove(key);
        }

        @Override
        protected void removeNodeSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected String[] keysSpi() throws BackingStoreException {
            String[] array = new String[this.map.keySet().size()];
            return this.map.keySet().toArray(array);
        }

        @Override
        protected String[] childrenNamesSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected AbstractPreferences childSpi(String name) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void syncSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected void flushSpi() throws BackingStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public static class CategorySupport
    implements ActionListener,
    DocumentListener,
    PreviewProvider,
    PreferencesCustomizer {
        public static final String OPTION_ID = "org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID";
        private static final int LOAD = 0;
        private static final int STORE = 1;
        private static final int ADD_LISTENERS = 2;
        private static final ComboItem[] bracePlacement = new ComboItem[]{new ComboItem(FmtOptions.OBRACE_NEWLINE, "LBL_bp_NEWLINE"), new ComboItem(FmtOptions.OBRACE_NEWLINE_INDENTED, "LBL_bp_NEWLINE_INDENTED"), new ComboItem(FmtOptions.OBRACE_SAMELINE, "LBL_bp_SAMELINE"), new ComboItem(FmtOptions.OBRACE_PRESERVE, "LBL_bp_PRESERVE")};
        private static final ComboItem[] wrap = new ComboItem[]{new ComboItem(CodeStyle.WrapStyle.WRAP_ALWAYS.name(), "LBL_wrp_WRAP_ALWAYS"), new ComboItem(CodeStyle.WrapStyle.WRAP_IF_LONG.name(), "LBL_wrp_WRAP_IF_LONG"), new ComboItem(CodeStyle.WrapStyle.WRAP_NEVER.name(), "LBL_wrp_WRAP_NEVER")};
        private final String previewText;
        private final String id;
        protected final JPanel panel;
        private final List<JComponent> components = new LinkedList<JComponent>();
        private JEditorPane previewPane;
        protected final Defaults.Provider provider;
        private final Preferences preferences;
        private final Preferences previewPrefs;
        private final String mimeType;

        protected /* varargs */ CategorySupport(String mimeType, Defaults.Provider provider, Preferences preferences, String id, JPanel panel, String previewText, String[] ... forcedOptions) {
            this.mimeType = mimeType;
            this.provider = provider;
            this.preferences = preferences;
            this.id = id;
            this.panel = panel;
            this.previewText = previewText != null ? previewText : NbBundle.getMessage(FmtOptions.class, (String)"SAMPLE_Default");
            this.scan(panel, this.components);
            PreviewPreferences forcedPrefs = new PreviewPreferences();
            for (String[] option : forcedOptions) {
                forcedPrefs.put(option[0], option[1]);
            }
            this.previewPrefs = new ProxyPreferences(preferences, forcedPrefs);
            this.loadFrom(preferences);
            this.addListeners();
        }

        protected void addListeners() {
            this.scan(2, null);
        }

        protected void loadFrom(Preferences preferences) {
            this.scan(0, preferences);
        }

        protected void storeTo(Preferences p) {
            this.scan(1, p);
        }

        public void notifyChanged() {
            this.storeTo(this.preferences);
            this.refreshPreview();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.notifyChanged();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.notifyChanged();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.notifyChanged();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.notifyChanged();
        }

        public JComponent getPreviewComponent() {
            if (this.previewPane == null) {
                this.previewPane = new JEditorPane();
                this.previewPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtOptions.class, (String)"AN_Preview"));
                this.previewPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtOptions.class, (String)"AD_Preview"));
                this.previewPane.setEditorKit(CloneableEditorSupport.getEditorKit((String)this.mimeType));
                this.previewPane.setEditable(false);
            }
            return this.previewPane;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void refreshPreview() {
            JEditorPane pane = (JEditorPane)this.getPreviewComponent();
            try {
                int rm = this.previewPrefs.getInt("text-limit-width", this.provider.getDefaultAsInt("text-limit-width"));
                pane.putClientProperty("TextLimitLine", rm);
            }
            catch (NumberFormatException e) {
                // empty catch block
            }
            Rectangle visibleRectangle = pane.getVisibleRect();
            pane.setText(this.previewText);
            pane.setIgnoreRepaint(true);
            final Document doc = pane.getDocument();
            if (doc instanceof BaseDocument) {
                final Reformat reformat = Reformat.get((Document)doc);
                reformat.lock();
                try {
                    ((BaseDocument)doc).runAtomic(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                reformat.reformat(0, doc.getLength());
                            }
                            catch (BadLocationException ble) {
                                LOGGER.log(Level.WARNING, null, ble);
                            }
                        }
                    });
                }
                finally {
                    reformat.unlock();
                }
            }
            LOGGER.warning(String.format("Can't format %s; it's not BaseDocument.", doc));
            pane.setIgnoreRepaint(false);
            pane.scrollRectToVisible(visibleRectangle);
            pane.repaint(100);
        }

        public JComponent getComponent() {
            return this.panel;
        }

        public String getDisplayName() {
            return this.panel.getName();
        }

        public String getId() {
            return this.id;
        }

        public HelpCtx getHelpCtx() {
            return null;
        }

        private void performOperation(int operation, JComponent jc, String optionID, Preferences p) {
            switch (operation) {
                case 0: {
                    this.loadData(jc, optionID, p);
                    break;
                }
                case 1: {
                    this.storeData(jc, optionID, p);
                    break;
                }
                case 2: {
                    this.addListener(jc);
                    break;
                }
                default: {
                    LOGGER.log(Level.WARNING, "Unknown operation value {0}", operation);
                }
            }
        }

        private void scan(int what, Preferences p) {
            for (JComponent jc : this.components) {
                Object o = jc.getClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID");
                if (o instanceof String) {
                    this.performOperation(what, jc, (String)o, p);
                    continue;
                }
                if (!(o instanceof String[])) continue;
                for (String oid : (String[])o) {
                    this.performOperation(what, jc, oid, p);
                }
            }
        }

        private void scan(Container container, List<JComponent> components) {
            for (Component c : container.getComponents()) {
                JComponent jc;
                Object o;
                if (c instanceof JComponent && ((o = (jc = (JComponent)c).getClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID")) instanceof String || o instanceof String[])) {
                    components.add(jc);
                }
                if (!(c instanceof Container)) continue;
                this.scan((Container)c, components);
            }
        }

        private void loadData(JComponent jc, String optionID, Preferences node) {
            if (jc instanceof JTextField) {
                JTextField field = (JTextField)jc;
                field.setText(node.get(optionID, this.provider.getDefaultAsString(optionID)));
            } else if (jc instanceof JCheckBox) {
                JCheckBox checkBox = (JCheckBox)jc;
                boolean df = this.provider.getDefaultAsBoolean(optionID);
                checkBox.setSelected(node.getBoolean(optionID, df));
            } else if (jc instanceof JComboBox) {
                JComboBox cb = (JComboBox)jc;
                String value = node.get(optionID, this.provider.getDefaultAsString(optionID));
                ComboBoxModel model = this.createModel(value);
                cb.setModel(model);
                ComboItem item = CategorySupport.whichItem(value, model);
                cb.setSelectedItem(item);
            }
        }

        private void storeData(JComponent jc, String optionID, Preferences node) {
            if (jc instanceof JTextField) {
                JTextField field = (JTextField)jc;
                String text = field.getText();
                if (FmtOptions.isInteger(optionID)) {
                    try {
                        Integer.parseInt(text);
                    }
                    catch (NumberFormatException e) {
                        return;
                    }
                }
                if (!optionID.equals("tab-size") && !optionID.equals("spaces-per-tab") && !optionID.equals("indent-shift-width") && this.provider.getDefaultAsString(optionID).equals(text)) {
                    node.remove(optionID);
                } else {
                    node.put(optionID, text);
                }
            } else if (jc instanceof JCheckBox) {
                JCheckBox checkBox = (JCheckBox)jc;
                if (!optionID.equals("expand-tabs") && this.provider.getDefaultAsBoolean(optionID) == checkBox.isSelected()) {
                    node.remove(optionID);
                } else {
                    node.putBoolean(optionID, checkBox.isSelected());
                }
            } else if (jc instanceof JComboBox) {
                String value;
                JComboBox cb = (JComboBox)jc;
                ComboItem comboItem = (ComboItem)cb.getSelectedItem();
                String string = value = comboItem == null ? this.provider.getDefaultAsString(optionID) : comboItem.value;
                if (this.provider.getDefaultAsString(optionID).equals(value)) {
                    node.remove(optionID);
                } else {
                    node.put(optionID, value);
                }
            }
        }

        private void addListener(JComponent jc) {
            if (jc instanceof JTextField) {
                JTextField field = (JTextField)jc;
                field.addActionListener(this);
                field.getDocument().addDocumentListener(this);
            } else if (jc instanceof JCheckBox) {
                JCheckBox checkBox = (JCheckBox)jc;
                checkBox.addActionListener(this);
            } else if (jc instanceof JComboBox) {
                JComboBox cb = (JComboBox)jc;
                cb.addActionListener(this);
            }
        }

        private ComboBoxModel createModel(String value) {
            for (ComboItem comboItem2 : bracePlacement) {
                if (!value.equals(comboItem2.value)) continue;
                return new DefaultComboBoxModel<ComboItem>(bracePlacement);
            }
            for (ComboItem comboItem2 : wrap) {
                if (!value.equals(comboItem2.value)) continue;
                return new DefaultComboBoxModel<ComboItem>(wrap);
            }
            return null;
        }

        private static ComboItem whichItem(String value, ComboBoxModel model) {
            for (int i = 0; i < model.getSize(); ++i) {
                ComboItem item = (ComboItem)model.getElementAt(i);
                if (!value.equals(item.value)) continue;
                return item;
            }
            return null;
        }

        private static class ComboItem {
            String value;
            String displayName;

            public ComboItem(String value, String key) {
                this.value = value;
                this.displayName = NbBundle.getMessage(FmtOptions.class, (String)key);
            }

            public String toString() {
                return this.displayName;
            }
        }

        public static final class Factory
        implements PreferencesCustomizer.Factory {
            private final String mimeType;
            private final Defaults.Provider provider;
            private final String id;
            private final Class<? extends JPanel> panelClass;
            private final String previewText;
            private final String[][] forcedOptions;

            public /* varargs */ Factory(String mimeType, String id, Class<? extends JPanel> panelClass, String previewText, String[] ... forcedOptions) {
                this.mimeType = mimeType;
                this.provider = Defaults.getInstance(mimeType);
                this.id = id;
                this.panelClass = panelClass;
                this.previewText = previewText;
                this.forcedOptions = forcedOptions;
            }

            public PreferencesCustomizer create(Preferences preferences) {
                try {
                    return new CategorySupport(this.mimeType, this.provider, preferences, this.id, this.panelClass.newInstance(), this.previewText, this.forcedOptions);
                }
                catch (Exception e) {
                    LOGGER.log(Level.WARNING, "Exception during creating formatter customiezer", e);
                    return null;
                }
            }
        }

    }

}

