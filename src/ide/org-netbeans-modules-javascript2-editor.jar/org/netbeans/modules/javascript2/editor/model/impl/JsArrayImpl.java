/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsArray;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;

public class JsArrayImpl
extends JsObjectImpl
implements JsArray {
    private List<TypeUsage> typesInArray = new ArrayList<TypeUsage>();

    public JsArrayImpl(JsObject parent, Identifier name, OffsetRange offsetRange, String mimeType, String sourceLabel) {
        super(parent, name, offsetRange, mimeType, sourceLabel);
    }

    public JsArrayImpl(JsObject parent, String name, boolean isDeclared, OffsetRange offsetRange, Set<Modifier> modifiers, String mimeType, String sourceLabel) {
        super(parent, name, isDeclared, offsetRange, modifiers, mimeType, sourceLabel);
    }

    public JsArrayImpl(JsObject parent, Identifier name, OffsetRange offsetRange, boolean isDeclared, Set<Modifier> modifiers, String mimeType, String sourceLabel) {
        super(parent, name, offsetRange, isDeclared, modifiers, mimeType, sourceLabel);
    }

    @Override
    public Collection<? extends TypeUsage> getTypesInArray() {
        ArrayList<TypeUsage> values = new ArrayList<TypeUsage>();
        for (TypeUsage type : this.typesInArray) {
            values.add(type);
        }
        return Collections.unmodifiableCollection(values);
    }

    public void addTypeInArray(TypeUsage type) {
        boolean isHere = false;
        for (TypeUsage typeUsage : this.typesInArray) {
            if (!typeUsage.getType().equals(type.getType())) continue;
            isHere = true;
            break;
        }
        if (!isHere) {
            this.typesInArray.add(type);
        }
    }

    public void addTypesInArray(Collection<TypeUsage> types) {
        for (TypeUsage type : types) {
            this.addTypeInArray(type);
        }
    }

    @Override
    public void resolveTypes(JsDocumentationHolder jsDocHolder) {
        super.resolveTypes(jsDocHolder);
        HashSet<String> nameTypesInArray = new HashSet<String>();
        ArrayList<TypeUsage> resolved = new ArrayList<TypeUsage>();
        Collection<? extends TypeUsage> typesIA = this.getTypesInArray();
        for (TypeUsage type2 : typesIA) {
            if (type2.getType().equals("unresolved") && typesIA.size() > 1) continue;
            if (!type2.isResolved()) {
                Iterator<TypeUsage> i$ = ModelUtils.resolveTypeFromSemiType(this, type2).iterator();
                while (i$.hasNext()) {
                    TypeUsage rType = i$.next();
                    if (nameTypesInArray.contains(rType.getType())) continue;
                    if ("@this;".equals(type2.getType())) {
                        rType = new TypeUsageImpl(rType.getType(), -1, rType.isResolved());
                    }
                    resolved.add(rType);
                    nameTypesInArray.add(rType.getType());
                }
                continue;
            }
            if (nameTypesInArray.contains(type2.getType())) continue;
            resolved.add(type2);
            nameTypesInArray.add(type2.getType());
        }
        for (TypeUsage type : resolved) {
            if (type.getOffset() <= 0) continue;
            JsObject jsObject = ModelUtils.findJsObjectByName(this, type.getType());
            if (jsObject == null) {
                JsObject global = ModelUtils.getGlobalObject(this);
                jsObject = ModelUtils.findJsObjectByName(global, type.getType());
            }
            if (jsObject == null) continue;
            int index = type.getType().lastIndexOf(46);
            int typeLength = index > -1 ? type.getType().length() - index - 1 : type.getType().length();
            ((JsObjectImpl)jsObject).addOccurrence(new OffsetRange(type.getOffset(), type.getOffset() + typeLength));
        }
        this.typesInArray.clear();
        this.typesInArray.addAll(resolved);
    }
}

