/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.EditorOptions
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor$Factory
 */
package org.netbeans.modules.javascript2.editor;

import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.EditorOptions;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.options.OptionsUtils;
import org.netbeans.spi.editor.typinghooks.DeletedTextInterceptor;

public class JsDeletedTextInterceptor
implements DeletedTextInterceptor {
    private final Language<JsTokenId> language;
    private final boolean singleQuote;
    private final boolean comments;

    public JsDeletedTextInterceptor(Language<JsTokenId> language, boolean singleQuote, boolean comments) {
        this.language = language;
        this.singleQuote = singleQuote;
        this.comments = comments;
    }

    private boolean isInsertMatchingEnabled() {
        EditorOptions options = EditorOptions.get((String)this.language.mimeType());
        if (options != null) {
            return options.getMatchBrackets();
        }
        return true;
    }

    private boolean isSmartQuotingEnabled() {
        return OptionsUtils.forLanguage(this.language).autoCompletionSmartQuotes();
    }

    public void afterRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
    }

    public boolean beforeRemove(DeletedTextInterceptor.Context context) throws BadLocationException {
        return false;
    }

    public void cancelled(DeletedTextInterceptor.Context context) {
    }

    public void remove(DeletedTextInterceptor.Context context) throws BadLocationException {
        BaseDocument doc = (BaseDocument)context.getDocument();
        int dotPos = context.getOffset() - 1;
        char ch = context.getText().charAt(0);
        JTextComponent target = context.getComponent();
        switch (ch) {
            case ' ': {
                TokenSequence<? extends JsTokenId> ts;
                if (!this.comments || (ts = LexUtilities.getPositionedSequence((Document)doc, dotPos, this.language)) == null || ts.token().id() != JsTokenId.LINE_COMMENT || ts.offset() != dotPos - 2) break;
                doc.remove(dotPos - 2, 2);
                target.getCaret().setDot(dotPos - 2);
                return;
            }
            case '(': 
            case '[': 
            case '{': {
                char tokenAtDot;
                if (!this.isInsertMatchingEnabled() || !((tokenAtDot = LexUtilities.getTokenChar((Document)doc, dotPos, this.language)) == ']' && LexUtilities.getTokenBalance((Document)doc, JsTokenId.BRACKET_LEFT_BRACKET, JsTokenId.BRACKET_RIGHT_BRACKET, dotPos, this.language) != 0 || tokenAtDot == ')' && LexUtilities.getTokenBalance((Document)doc, JsTokenId.BRACKET_LEFT_PAREN, JsTokenId.BRACKET_RIGHT_PAREN, dotPos, this.language) != 0) && (tokenAtDot != '}' || LexUtilities.getTokenBalance((Document)doc, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY, dotPos, this.language) == 0)) break;
                doc.remove(dotPos, 1);
                break;
            }
            case '/': {
                TokenSequence<? extends JsTokenId> ts;
                if (this.comments && (ts = LexUtilities.getPositionedSequence((Document)doc, dotPos, this.language)) != null && ts.token().id() == JsTokenId.REGEXP_BEGIN && ts.offset() == dotPos - 1) {
                    doc.remove(dotPos - 1, 1);
                    target.getCaret().setDot(dotPos - 1);
                    return;
                }
            }
            case '\'': {
                if (!this.singleQuote) break;
            }
            case '\"': {
                if (!this.isSmartQuotingEnabled()) break;
                char[] match = doc.getChars(dotPos, 1);
                if (match != null && match[0] == ch) {
                    doc.remove(dotPos, 1);
                    break;
                } else {
                    break;
                }
            }
        }
    }

    public static class JsonFactory
    implements DeletedTextInterceptor.Factory {
        public DeletedTextInterceptor createDeletedTextInterceptor(MimePath mimePath) {
            return new JsDeletedTextInterceptor(JsTokenId.jsonLanguage(), false, false);
        }
    }

    public static class JsFactory
    implements DeletedTextInterceptor.Factory {
        public DeletedTextInterceptor createDeletedTextInterceptor(MimePath mimePath) {
            return new JsDeletedTextInterceptor(JsTokenId.javascriptLanguage(), true, true);
        }
    }

}

