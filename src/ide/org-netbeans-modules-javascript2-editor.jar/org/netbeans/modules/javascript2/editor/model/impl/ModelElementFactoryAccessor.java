/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import org.netbeans.modules.javascript2.editor.spi.model.ModelElementFactory;

public abstract class ModelElementFactoryAccessor {
    private static volatile ModelElementFactoryAccessor DEFAULT;

    public static ModelElementFactoryAccessor getDefault() {
        block3 : {
            ModelElementFactoryAccessor a = DEFAULT;
            if (a != null) {
                return a;
            }
            Class<ModelElementFactory> c = ModelElementFactory.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block3;
                throw new AssertionError(ex);
            }
        }
        return DEFAULT;
    }

    public static void setDefault(ModelElementFactoryAccessor accessor) {
        if (DEFAULT != null) {
            throw new IllegalStateException();
        }
        DEFAULT = accessor;
    }

    public abstract ModelElementFactory createModelElementFactory();
}

