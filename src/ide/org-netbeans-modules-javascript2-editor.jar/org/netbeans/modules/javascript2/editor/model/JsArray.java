/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;

public interface JsArray
extends JsObject {
    public Collection<? extends TypeUsage> getTypesInArray();
}

