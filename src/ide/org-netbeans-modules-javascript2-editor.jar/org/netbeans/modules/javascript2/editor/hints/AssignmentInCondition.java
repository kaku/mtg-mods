/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsConventionHint;

public class AssignmentInCondition
extends JsConventionHint {
    public String getId() {
        return "jsassignmentincondition.hint";
    }

    public String getDescription() {
        return Bundle.AssignmentInConditionDescription();
    }

    public String getDisplayName() {
        return Bundle.AssignmentInConditionDisplayName();
    }
}

