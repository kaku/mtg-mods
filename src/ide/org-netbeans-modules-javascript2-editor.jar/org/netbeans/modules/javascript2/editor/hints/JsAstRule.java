/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$AstRule
 *  org.netbeans.modules.csl.api.RuleContext
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import javax.swing.text.BadLocationException;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;

public abstract class JsAstRule
implements Rule.AstRule {
    public static final String JS_OTHER_HINTS = "js.other.hints";

    abstract void computeHints(JsHintsProvider.JsRuleContext var1, List<Hint> var2, int var3, HintsProvider.HintsManager var4) throws BadLocationException;

    public boolean getDefaultEnabled() {
        return true;
    }

    public JComponent getCustomizer(Preferences node) {
        return null;
    }

    public boolean appliesTo(RuleContext context) {
        return true;
    }

    public boolean showInTasklist() {
        return false;
    }

    public HintSeverity getDefaultSeverity() {
        return HintSeverity.WARNING;
    }
}

