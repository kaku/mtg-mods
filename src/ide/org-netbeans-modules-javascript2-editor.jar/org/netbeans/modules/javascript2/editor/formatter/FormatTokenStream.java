/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.formatter.FormatToken;

public final class FormatTokenStream
implements Iterable<FormatToken> {
    private final TreeMap<Integer, FormatToken> tokenPosition = new TreeMap();
    private FormatToken firstToken;
    private FormatToken lastToken;

    private FormatTokenStream() {
    }

    public static FormatTokenStream create(TokenSequence<? extends JsTokenId> ts, int start, int end) {
        FormatTokenStream ret = new FormatTokenStream();
        int diff = ts.move(start);
        if (diff <= 0) {
            ts.movePrevious();
        }
        ret.addToken(FormatToken.forFormat(FormatToken.Kind.SOURCE_START));
        block25 : while (ts.moveNext() && ts.offset() < end) {
            Token token = ts.token();
            JsTokenId id = (JsTokenId)token.id();
            switch (id) {
                case EOL: {
                    ret.addToken(FormatToken.forAny(FormatToken.Kind.EOL, ts.offset(), token.text(), id));
                    continue block25;
                }
                case WHITESPACE: {
                    ret.addToken(FormatToken.forAny(FormatToken.Kind.WHITESPACE, ts.offset(), token.text(), id));
                    continue block25;
                }
                case BLOCK_COMMENT: {
                    ret.addToken(FormatToken.forAny(FormatToken.Kind.BLOCK_COMMENT, ts.offset(), token.text(), id));
                    continue block25;
                }
                case DOC_COMMENT: {
                    ret.addToken(FormatToken.forAny(FormatToken.Kind.DOC_COMMENT, ts.offset(), token.text(), id));
                    continue block25;
                }
                case LINE_COMMENT: {
                    ret.addToken(FormatToken.forAny(FormatToken.Kind.LINE_COMMENT, ts.offset(), token.text(), id));
                    continue block25;
                }
                case OPERATOR_GREATER: 
                case OPERATOR_LOWER: 
                case OPERATOR_EQUALS: 
                case OPERATOR_EQUALS_EXACTLY: 
                case OPERATOR_LOWER_EQUALS: 
                case OPERATOR_GREATER_EQUALS: 
                case OPERATOR_NOT_EQUALS: 
                case OPERATOR_NOT_EQUALS_EXACTLY: 
                case OPERATOR_AND: 
                case OPERATOR_OR: 
                case OPERATOR_MULTIPLICATION: 
                case OPERATOR_DIVISION: 
                case OPERATOR_BITWISE_AND: 
                case OPERATOR_BITWISE_OR: 
                case OPERATOR_BITWISE_XOR: 
                case OPERATOR_MODULUS: 
                case OPERATOR_LEFT_SHIFT_ARITHMETIC: 
                case OPERATOR_RIGHT_SHIFT_ARITHMETIC: 
                case OPERATOR_RIGHT_SHIFT: 
                case OPERATOR_PLUS: 
                case OPERATOR_MINUS: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_BINARY_OPERATOR_WRAP));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_BINARY_OPERATOR));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_BINARY_OPERATOR));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_BINARY_OPERATOR_WRAP));
                    continue block25;
                }
                case OPERATOR_ASSIGNMENT: 
                case OPERATOR_PLUS_ASSIGNMENT: 
                case OPERATOR_MINUS_ASSIGNMENT: 
                case OPERATOR_MULTIPLICATION_ASSIGNMENT: 
                case OPERATOR_DIVISION_ASSIGNMENT: 
                case OPERATOR_BITWISE_AND_ASSIGNMENT: 
                case OPERATOR_BITWISE_OR_ASSIGNMENT: 
                case OPERATOR_BITWISE_XOR_ASSIGNMENT: 
                case OPERATOR_MODULUS_ASSIGNMENT: 
                case OPERATOR_LEFT_SHIFT_ARITHMETIC_ASSIGNMENT: 
                case OPERATOR_RIGHT_SHIFT_ARITHMETIC_ASSIGNMENT: 
                case OPERATOR_RIGHT_SHIFT_ASSIGNMENT: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_ASSIGNMENT_OPERATOR));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_ASSIGNMENT_OPERATOR));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_ASSIGNMENT_OPERATOR_WRAP));
                    continue block25;
                }
                case OPERATOR_COMMA: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_COMMA));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_COMMA));
                    continue block25;
                }
                case OPERATOR_DOT: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_DOT));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_DOT));
                    continue block25;
                }
                case KEYWORD_IF: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_IF_KEYWORD));
                    continue block25;
                }
                case KEYWORD_WHILE: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_WHILE_KEYWORD));
                    continue block25;
                }
                case KEYWORD_FOR: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_FOR_KEYWORD));
                    continue block25;
                }
                case KEYWORD_WITH: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_WITH_KEYWORD));
                    continue block25;
                }
                case KEYWORD_SWITCH: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_SWITCH_KEYWORD));
                    continue block25;
                }
                case KEYWORD_CATCH: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_CATCH_KEYWORD));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_CATCH_KEYWORD));
                    continue block25;
                }
                case KEYWORD_ELSE: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_ELSE_KEYWORD));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    continue block25;
                }
                case KEYWORD_FINALLY: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_FINALLY_KEYWORD));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    continue block25;
                }
                case KEYWORD_VAR: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_VAR_KEYWORD));
                    continue block25;
                }
                case KEYWORD_NEW: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_NEW_KEYWORD));
                    continue block25;
                }
                case KEYWORD_TYPEOF: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_TYPEOF_KEYWORD));
                    continue block25;
                }
                case OPERATOR_SEMICOLON: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_SEMICOLON));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_SEMICOLON));
                    continue block25;
                }
                case BRACKET_LEFT_PAREN: {
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.AFTER_LEFT_PARENTHESIS));
                    continue block25;
                }
                case BRACKET_RIGHT_PAREN: {
                    ret.addToken(FormatToken.forFormat(FormatToken.Kind.BEFORE_RIGHT_PARENTHESIS));
                    ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
                    continue block25;
                }
            }
            ret.addToken(FormatToken.forText(ts.offset(), token.text(), id));
        }
        return ret;
    }

    @CheckForNull
    public static FormatToken getNextNonVirtual(FormatToken token) {
        FormatToken current;
        for (current = token.next(); current != null && current.isVirtual(); current = current.next()) {
        }
        return current;
    }

    @CheckForNull
    public static FormatToken getNextImportant(FormatToken token) {
        FormatToken current;
        for (current = token.next(); current != null && (current.isVirtual() || current.getKind() == FormatToken.Kind.WHITESPACE || current.getKind() == FormatToken.Kind.EOL || current.getKind() == FormatToken.Kind.DOC_COMMENT || current.getKind() == FormatToken.Kind.BLOCK_COMMENT || current.getKind() == FormatToken.Kind.LINE_COMMENT); current = current.next()) {
        }
        return current;
    }

    public FormatToken getToken(int offset) {
        return this.tokenPosition.get(offset);
    }

    public FormatToken getCoveringToken(int offset) {
        FormatToken token;
        Map.Entry<Integer, FormatToken> entry = this.tokenPosition.floorEntry(offset);
        if (entry != null && !(token = entry.getValue()).isVirtual()) {
            if (token.getOffset() == offset) {
                return token;
            }
            int endPos = token.getOffset() + token.getText().length();
            if (offset >= token.getOffset() && offset < endPos) {
                return token;
            }
        }
        return null;
    }

    @Override
    public Iterator<FormatToken> iterator() {
        return new FormatTokenIterator();
    }

    public List<FormatToken> getTokens() {
        ArrayList<FormatToken> tokens = new ArrayList<FormatToken>((int)((double)this.tokenPosition.size() * 1.5));
        for (FormatToken token : this) {
            tokens.add(token);
        }
        return tokens;
    }

    public void addToken(FormatToken token) {
        if (this.firstToken == null) {
            this.firstToken = token;
            this.lastToken = token;
        } else {
            this.lastToken.setNext(token);
            token.setPrevious(this.lastToken);
            this.lastToken = token;
        }
        if (token.getOffset() >= 0) {
            this.tokenPosition.put(token.getOffset(), token);
        }
    }

    public void removeToken(FormatToken token) {
        assert (token.isVirtual());
        FormatToken previous = token.previous();
        FormatToken next = token.next();
        token.setNext(null);
        token.setPrevious(null);
        if (token.getOffset() >= 0) {
            this.tokenPosition.remove(token.getOffset());
        }
        if (previous == null) {
            assert (this.firstToken == token);
            this.firstToken = next;
            next.setPrevious(null);
        }
        if (next == null) {
            assert (this.lastToken == token);
            this.lastToken = previous;
            previous.setNext(null);
        }
        if (previous == null || next == null) {
            return;
        }
        previous.setNext(next);
        next.setPrevious(previous);
    }

    private class FormatTokenIterator
    implements Iterator<FormatToken> {
        private FormatToken current;

        private FormatTokenIterator() {
            this.current = FormatTokenStream.this.firstToken;
        }

        @Override
        public boolean hasNext() {
            return this.current != null;
        }

        @Override
        public FormatToken next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            FormatToken ret = this.current;
            this.current = this.current.next();
            return ret;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove operation not supported.");
        }
    }

}

