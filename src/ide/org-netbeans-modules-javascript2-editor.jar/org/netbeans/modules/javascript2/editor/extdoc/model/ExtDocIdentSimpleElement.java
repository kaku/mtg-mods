/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocBaseElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;

public class ExtDocIdentSimpleElement
extends ExtDocBaseElement {
    protected final String identifier;

    protected ExtDocIdentSimpleElement(ExtDocElementType type, String identifier) {
        super(type);
        this.identifier = identifier;
    }

    public static ExtDocIdentSimpleElement create(ExtDocElementType type, String identifier) {
        return new ExtDocIdentSimpleElement(type, identifier);
    }

    public String getIdentifier() {
        return this.identifier;
    }
}

