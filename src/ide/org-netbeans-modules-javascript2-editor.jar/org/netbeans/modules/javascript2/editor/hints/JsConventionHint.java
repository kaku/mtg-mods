/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$AstRule
 *  org.netbeans.modules.csl.api.RuleContext
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.Collections;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;

public abstract class JsConventionHint
implements Rule.AstRule {
    public static final String JSCONVENTION_OPTION_HINTS = "jsconvention.option.hints";

    public boolean appliesTo(RuleContext context) {
        if (context instanceof JsHintsProvider.JsRuleContext) {
            return true;
        }
        return false;
    }

    public boolean showInTasklist() {
        return true;
    }

    public HintSeverity getDefaultSeverity() {
        return HintSeverity.WARNING;
    }

    public boolean getDefaultEnabled() {
        return true;
    }

    public JComponent getCustomizer(Preferences node) {
        return null;
    }

    public Set<?> getKinds() {
        return Collections.singleton("jsconvention.option.hints");
    }
}

