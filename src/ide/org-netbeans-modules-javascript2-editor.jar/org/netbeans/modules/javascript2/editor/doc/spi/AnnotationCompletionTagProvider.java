/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

import java.util.List;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;
import org.openide.util.Parameters;

public abstract class AnnotationCompletionTagProvider {
    private final String name;

    public AnnotationCompletionTagProvider(@NonNull String name) {
        Parameters.notNull((CharSequence)"name", (Object)name);
        this.name = name;
    }

    public final String getName() {
        return this.name;
    }

    public abstract List<AnnotationCompletionTag> getAnnotations();
}

