/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.core.spi.multiview.text.MultiViewEditorElement
 *  org.netbeans.modules.csl.api.DeclarationFinder
 *  org.netbeans.modules.csl.api.Formatter
 *  org.netbeans.modules.csl.api.StructureScanner
 *  org.netbeans.modules.csl.spi.DefaultLanguageConfig
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.javascript2.editor;

import org.netbeans.api.lexer.Language;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.Formatter;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.javascript2.editor.JsStructureScanner;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.classpath.ClassPathProviderImpl;
import org.netbeans.modules.javascript2.editor.formatter.JsFormatter;
import org.netbeans.modules.javascript2.editor.navigation.DeclarationFinderImpl;
import org.netbeans.modules.javascript2.editor.parser.JsonParser;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.util.Lookup;

public class JsonLanguage
extends DefaultLanguageConfig {
    public static MultiViewEditorElement createMultiViewEditorElement(Lookup context) {
        return new MultiViewEditorElement(context);
    }

    public JsonLanguage() {
        ClassPathProviderImpl.registerJsClassPathIfNeeded();
    }

    public Language getLexerLanguage() {
        return JsTokenId.jsonLanguage();
    }

    public String getDisplayName() {
        return "JSON";
    }

    public Parser getParser() {
        return new JsonParser();
    }

    public boolean hasStructureScanner() {
        return true;
    }

    public StructureScanner getStructureScanner() {
        return new JsStructureScanner(JsTokenId.jsonLanguage());
    }

    public DeclarationFinder getDeclarationFinder() {
        return new DeclarationFinderImpl(JsTokenId.jsonLanguage());
    }

    public Formatter getFormatter() {
        return new JsFormatter(JsTokenId.jsonLanguage());
    }

    public boolean hasFormatter() {
        return true;
    }
}

