/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.spi.model;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;
import java.util.regex.Pattern;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionArgument;
import org.netbeans.modules.javascript2.editor.spi.model.ModelElementFactory;

public interface FunctionInterceptor {
    public Pattern getNamePattern();

    public void intercept(String var1, JsObject var2, DeclarationScope var3, ModelElementFactory var4, Collection<FunctionArgument> var5);

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE})
    public static @interface Registration {
        public int priority() default 100;
    }

}

