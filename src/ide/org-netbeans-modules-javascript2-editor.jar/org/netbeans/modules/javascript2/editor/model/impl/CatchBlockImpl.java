/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.DeclarationScopeImpl;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ParameterObject;

public class CatchBlockImpl
extends DeclarationScopeImpl
implements JsFunction {
    private final List<JsObject> parameters = new ArrayList<JsObject>();

    public CatchBlockImpl(DeclarationScope inFunction, Identifier exception, OffsetRange range, String mimeType) {
        super(inFunction, (JsObject)((Object)inFunction), new IdentifierImpl(CatchBlockImpl.getBlockName((JsObject)((Object)inFunction)), OffsetRange.NONE), range, mimeType, null);
        ParameterObject param = new ParameterObject(this, exception, mimeType, null);
        this.parameters.add(param);
        ((JsObjectImpl)((Object)inFunction)).addProperty(this.getName(), this);
        param.addOccurrence(exception.getOffsetRange());
    }

    private static String getBlockName(JsObject parent) {
        int index = 1;
        while (parent.getProperty("catch_" + index) != null) {
            ++index;
        }
        return "catch_" + index;
    }

    @Override
    public Collection<? extends JsObject> getParameters() {
        return new ArrayList<JsObject>(this.parameters);
    }

    @Override
    public JsObject getParameter(String name) {
        for (JsObject param : this.parameters) {
            if (!name.equals(param.getName())) continue;
            return param;
        }
        return null;
    }

    @Override
    public void addReturnType(TypeUsage type) {
    }

    @Override
    public Collection<? extends TypeUsage> getReturnTypes() {
        return Collections.emptyList();
    }

    @Override
    public JsElement.Kind getJSKind() {
        return JsElement.Kind.CATCH_BLOCK;
    }
}

