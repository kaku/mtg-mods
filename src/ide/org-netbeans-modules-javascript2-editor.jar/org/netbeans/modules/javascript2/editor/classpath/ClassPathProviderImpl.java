/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.java.classpath.ClassPath
 *  org.netbeans.api.java.classpath.GlobalPathRegistry
 *  org.netbeans.spi.java.classpath.ClassPathProvider
 *  org.netbeans.spi.java.classpath.support.ClassPathSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.InstalledFileLocator
 *  org.openide.util.RequestProcessor
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.javascript2.editor.classpath;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.modules.javascript2.editor.index.JsIndexer;
import org.netbeans.spi.java.classpath.ClassPathProvider;
import org.netbeans.spi.java.classpath.support.ClassPathSupport;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.InstalledFileLocator;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class ClassPathProviderImpl
implements ClassPathProvider {
    private static final Logger LOG = Logger.getLogger(ClassPathProviderImpl.class.getName());
    protected static final RequestProcessor RP = new RequestProcessor(ClassPathProviderImpl.class);
    public static final String BOOT_CP = "classpath/javascript-boot";
    public static final AtomicBoolean JS_CLASSPATH_REGISTERED = new AtomicBoolean(false);
    private static ClassPath cachedBootClassPath;
    private static List<FileObject> roots;
    private static final StubsBundle[] STUBS_BUNDLES;

    public ClassPath findClassPath(FileObject file, String type) {
        if (type.equals("classpath/javascript-boot")) {
            return ClassPathProviderImpl.getBootClassPath();
        }
        return null;
    }

    public static synchronized ClassPath getBootClassPath() {
        if (cachedBootClassPath == null) {
            List<FileObject> stubs = ClassPathProviderImpl.getJsStubs();
            cachedBootClassPath = ClassPathSupport.createClassPath((FileObject[])stubs.toArray((T[])new FileObject[stubs.size()]));
        }
        return cachedBootClassPath;
    }

    public static synchronized List<FileObject> getJsStubs() {
        if (roots == null) {
            ArrayList<FileObject> result = new ArrayList<FileObject>(STUBS_BUNDLES.length);
            for (StubsBundle bundle : STUBS_BUNDLES) {
                File stubFile;
                block7 : {
                    stubFile = InstalledFileLocator.getDefault().locate("jsstubs/" + bundle.getNameOfDocumented(), "org.netbeans.modules.javascript2.editor", false);
                    if (stubFile == null || !stubFile.exists()) {
                        stubFile = InstalledFileLocator.getDefault().locate("jsstubs/" + bundle.getNameOfPruned(), "org.netbeans.modules.javascript2.editor", false);
                    }
                    if (stubFile == null) {
                        try {
                            File moduleJar = Utilities.toFile((URI)ClassPathProviderImpl.class.getProtectionDomain().getCodeSource().getLocation().toURI());
                            stubFile = new File(moduleJar.getParentFile().getParentFile(), "jsstubs/" + bundle.getNameOfPruned());
                        }
                        catch (URISyntaxException x) {
                            if ($assertionsDisabled) break block7;
                            throw new AssertionError(x);
                        }
                    }
                }
                if (!stubFile.isFile() || !stubFile.exists()) {
                    LOG.log(Level.WARNING, "JavaScript stubs file was not found: {0}", stubFile.getAbsolutePath());
                    continue;
                }
                result.add(FileUtil.getArchiveRoot((FileObject)FileUtil.toFileObject((File)stubFile)));
            }
            roots = result;
        }
        return Collections.unmodifiableList(roots);
    }

    public static void registerJsClassPathIfNeeded() {
        Runnable action = new Runnable(){

            @Override
            public void run() {
                ClassPathProviderImpl.registerJsClassPathIfNeededImpl();
            }
        };
        if (JsIndexer.Factory.isScannerThread()) {
            JsIndexer.Factory.addPostScanTask(action);
        } else {
            action.run();
        }
    }

    private static void registerJsClassPathIfNeededImpl() {
        if (JS_CLASSPATH_REGISTERED.compareAndSet(false, true)) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    ClassPath cp = ClassPathProviderImpl.getBootClassPath();
                    if (cp != null) {
                        GlobalPathRegistry.getDefault().register("classpath/javascript-boot", new ClassPath[]{cp});
                    }
                }
            });
        }
    }

    static {
        STUBS_BUNDLES = new StubsBundle[]{new StubsBundle("corestubs.zip", "corestubs-doc.zip"), new StubsBundle("domstubs.zip", "domstubs.zip"), new StubsBundle("reststubs.zip", "reststubs-doc.zip")};
    }

    private static class StubsBundle {
        private final String nameOfPruned;
        private final String nameOfDocumented;

        public StubsBundle(String nameOfPruned, String nameOfDocumented) {
            this.nameOfPruned = nameOfPruned;
            this.nameOfDocumented = nameOfDocumented;
        }

        public String getNameOfPruned() {
            return this.nameOfPruned;
        }

        public String getNameOfDocumented() {
            return this.nameOfDocumented;
        }
    }

}

