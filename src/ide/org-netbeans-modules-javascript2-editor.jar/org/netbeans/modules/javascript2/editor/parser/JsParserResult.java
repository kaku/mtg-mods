/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.FunctionNode
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.parser;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.ir.FunctionNode;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.doc.api.JsDocumentationSupport;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.ModelFactory;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsParserResult
extends ParserResult {
    private static final Logger LOGGER = Logger.getLogger(JsParserResult.class.getName());
    private final FunctionNode root;
    private final boolean embedded;
    private List<? extends Error> errors;
    private Model model;
    private JsDocumentationHolder docHolder;

    public JsParserResult(@NonNull Snapshot snapshot, @NullAllowed FunctionNode root) {
        super(snapshot);
        this.root = root;
        this.errors = Collections.emptyList();
        this.model = null;
        this.docHolder = null;
        this.embedded = JsParserResult.isEmbedded(snapshot);
    }

    public static boolean isEmbedded(@NonNull Snapshot snapshot) {
        return !"text/javascript".equals(snapshot.getMimePath().getPath()) && !"text/x-json".equals(snapshot.getMimePath().getPath());
    }

    public List<? extends Error> getDiagnostics() {
        return this.errors;
    }

    protected void invalidate() {
    }

    public FunctionNode getRoot() {
        return this.root;
    }

    public void setErrors(List<? extends Error> errors) {
        this.errors = errors;
    }

    public Model getModel() {
        return this.getModel(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Model getModel(boolean forceCreate) {
        JsParserResult jsParserResult = this;
        synchronized (jsParserResult) {
            if (this.model == null || forceCreate) {
                this.model = ModelFactory.getModel(this);
                if (LOGGER.isLoggable(Level.FINEST)) {
                    this.model.writeModel(new Model.Printer(){

                        @Override
                        public void println(String str) {
                            LOGGER.log(Level.FINEST, str);
                        }
                    });
                }
            }
            return this.model;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public JsDocumentationHolder getDocumentationHolder() {
        JsParserResult jsParserResult = this;
        synchronized (jsParserResult) {
            if (this.docHolder == null) {
                this.docHolder = JsDocumentationSupport.getDocumentationHolder(this);
            }
            return this.docHolder;
        }
    }

    public boolean isEmbedded() {
        return this.embedded;
    }

}

