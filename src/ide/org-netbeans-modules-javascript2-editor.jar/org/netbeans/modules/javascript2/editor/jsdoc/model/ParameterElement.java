/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import java.util.List;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementImpl;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.model.Type;

public abstract class ParameterElement
extends JsDocElementImpl {
    private final List<Type> paramTypes;
    private final String paramDescription;

    public ParameterElement(JsDocElementType type, List<Type> paramTypes, String paramDescription) {
        super(type);
        this.paramTypes = paramTypes;
        this.paramDescription = paramDescription;
    }

    public String getParamDescription() {
        return this.paramDescription;
    }

    public List<Type> getParamTypes() {
        return this.paramTypes;
    }
}

