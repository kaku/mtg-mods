/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;

public class JsKeyWords {
    protected static final Map<String, CompletionType> KEYWORDS = new HashMap<String, CompletionType>();

    static {
        KEYWORDS.put(JsTokenId.KEYWORD_BREAK.fixedText(), CompletionType.ENDS_WITH_SEMICOLON);
        KEYWORDS.put(JsTokenId.KEYWORD_CASE.fixedText(), CompletionType.ENDS_WITH_COLON);
        KEYWORDS.put(JsTokenId.KEYWORD_CATCH.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_CONTINUE.fixedText(), CompletionType.ENDS_WITH_SEMICOLON);
        KEYWORDS.put(JsTokenId.KEYWORD_DEBUGGER.fixedText(), CompletionType.ENDS_WITH_SEMICOLON);
        KEYWORDS.put(JsTokenId.KEYWORD_DEFAULT.fixedText(), CompletionType.ENDS_WITH_COLON);
        KEYWORDS.put(JsTokenId.KEYWORD_DELETE.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_DO.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_ELSE.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_FALSE.fixedText(), CompletionType.SIMPLE);
        KEYWORDS.put(JsTokenId.KEYWORD_FINALLY.fixedText(), CompletionType.ENDS_WITH_CURLY_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_FOR.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_FUNCTION.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_IF.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_IN.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_INSTANCEOF.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_NEW.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_RETURN.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_SWITCH.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_THIS.fixedText(), CompletionType.ENDS_WITH_DOT);
        KEYWORDS.put(JsTokenId.KEYWORD_THROW.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_TRUE.fixedText(), CompletionType.SIMPLE);
        KEYWORDS.put(JsTokenId.KEYWORD_TRY.fixedText(), CompletionType.ENDS_WITH_CURLY_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_TYPEOF.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_VAR.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_VOID.fixedText(), CompletionType.ENDS_WITH_SPACE);
        KEYWORDS.put(JsTokenId.KEYWORD_WHILE.fixedText(), CompletionType.CURSOR_INSIDE_BRACKETS);
        KEYWORDS.put(JsTokenId.KEYWORD_WITH.fixedText(), CompletionType.ENDS_WITH_SPACE);
    }

    protected static enum CompletionType {
        SIMPLE,
        CURSOR_INSIDE_BRACKETS,
        ENDS_WITH_CURLY_BRACKETS,
        ENDS_WITH_SPACE,
        ENDS_WITH_SEMICOLON,
        ENDS_WITH_COLON,
        ENDS_WITH_DOT;
        

        private CompletionType() {
        }
    }

}

