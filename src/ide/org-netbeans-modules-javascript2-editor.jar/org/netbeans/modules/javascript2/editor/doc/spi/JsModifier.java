/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

public enum JsModifier {
    PRIVATE("private"),
    PUBLIC("public"),
    STATIC("static");
    
    private final String value;

    private JsModifier(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public static JsModifier fromString(String value) {
        for (JsModifier modifier : JsModifier.values()) {
            if (!value.equalsIgnoreCase(modifier.toString())) continue;
            return modifier;
        }
        return null;
    }
}

