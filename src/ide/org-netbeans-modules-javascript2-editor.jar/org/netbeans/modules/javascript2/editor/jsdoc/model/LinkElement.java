/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementImpl;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.NamePath;

public class LinkElement
extends JsDocElementImpl {
    private final NamePath linkedPath;

    private LinkElement(JsDocElementType type, NamePath linkedPath) {
        super(type);
        this.linkedPath = linkedPath;
    }

    public static LinkElement create(JsDocElementType type, NamePath linkedPath) {
        return new LinkElement(type, linkedPath);
    }

    public NamePath getLinkedPath() {
        return this.linkedPath;
    }
}

