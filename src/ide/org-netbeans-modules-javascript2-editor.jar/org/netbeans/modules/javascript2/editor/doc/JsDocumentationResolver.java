/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationFallbackProvider;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationReader;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public class JsDocumentationResolver {
    private static final Logger LOG = Logger.getLogger(JsDocumentationResolver.class.getName());
    private static JsDocumentationResolver instance;
    private static final List<? extends JsDocumentationProvider> PROVIDERS;

    public static synchronized JsDocumentationResolver getDefault() {
        if (instance == null) {
            instance = new JsDocumentationResolver();
        }
        return instance;
    }

    public JsDocumentationProvider getDocumentationProvider(Snapshot snapshot) {
        return this.findBestMatchingProvider(snapshot);
    }

    private JsDocumentationProvider findBestMatchingProvider(Snapshot snapshot) {
        Set<String> allTags = JsDocumentationReader.getAllTags(snapshot);
        float max = -1.0f;
        JsDocumentationProvider bestProvider = null;
        for (JsDocumentationProvider jsDocumentationProvider : PROVIDERS) {
            float coverage = this.countTagsCoverageRation(allTags, jsDocumentationProvider);
            if ((double)coverage == 1.0) {
                return jsDocumentationProvider;
            }
            if (coverage <= max) continue;
            max = coverage;
            bestProvider = jsDocumentationProvider;
        }
        return bestProvider != null ? bestProvider : new JsDocumentationFallbackProvider();
    }

    private float countTagsCoverageRation(Set<String> tags, JsDocumentationProvider provider) {
        HashSet<String> unsupportedTags = new HashSet<String>(tags);
        unsupportedTags.removeAll(provider.getSupportedTags());
        if (unsupportedTags.isEmpty()) {
            return 1.0f;
        }
        float coverage = 1.0f - 1.0f / (float)tags.size() * (float)unsupportedTags.size();
        return coverage;
    }

    static {
        PROVIDERS = new ArrayList<JsDocumentationProvider>(Lookups.forPath((String)"javascript/doc/providers").lookupResult(JsDocumentationProvider.class).allInstances());
    }
}

