/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.openide.util.Parameters;

public class AnnotationCompletionTag {
    private final String name;
    private final String insertTemplate;
    private final String documentation;

    public AnnotationCompletionTag(@NonNull String name, @NonNull String insertTemplate) {
        this(name, insertTemplate, null);
    }

    public AnnotationCompletionTag(@NonNull String name, @NonNull String insertTemplate, @NullAllowed String documentation) {
        Parameters.notEmpty((CharSequence)"name", (CharSequence)name);
        Parameters.notEmpty((CharSequence)"insertTemplate", (CharSequence)insertTemplate);
        this.name = name;
        this.insertTemplate = insertTemplate;
        this.documentation = documentation;
    }

    public final String getName() {
        return this.name;
    }

    public final String getInsertTemplate() {
        return this.insertTemplate;
    }

    @CheckForNull
    public final String getDocumentation() {
        return this.documentation;
    }

    public void formatParameters(HtmlFormatter formatter) {
    }

    public boolean equals(@NullAllowed Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        AnnotationCompletionTag other = (AnnotationCompletionTag)obj;
        if (this.name == null ? other.name != null : !this.name.equals(other.name)) {
            return false;
        }
        if (this.insertTemplate == null ? other.insertTemplate != null : !this.insertTemplate.equals(other.insertTemplate)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 29 * hash + (this.insertTemplate != null ? this.insertTemplate.hashCode() : 0);
        return hash;
    }
}

