/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc;

public enum JsDocCommentType {
    DOC_COMMON("common"),
    DOC_NO_CODE_START("noCodeStart"),
    DOC_NO_CODE_END("noCodeEnd"),
    DOC_SHARED_TAG_START("sharedTagStart"),
    DOC_SHARED_TAG_END("sharedTagEnd");
    
    private final String value;

    private JsDocCommentType(String textValue) {
        this.value = textValue;
    }

    public String toString() {
        return this.value;
    }
}

