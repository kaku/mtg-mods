/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.modules.parsing.api.Embedding
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.EmbeddingProvider
 *  org.netbeans.modules.parsing.spi.SchedulerTask
 *  org.netbeans.modules.parsing.spi.TaskFactory
 */
package org.netbeans.modules.javascript2.editor.embedding;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.MatchResult;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.modules.parsing.api.Embedding;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.EmbeddingProvider;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;

public final class JsEmbeddingProvider
extends EmbeddingProvider {
    private static final int PRIORITY = 0;
    public static final String NETBEANS_IMPORT_FILE = "__netbeans_import__";
    private static final Logger LOG = Logger.getLogger(JsEmbeddingProvider.class.getName());
    private static final Map<String, Translator> translators = new HashMap<String, Translator>();
    private static final String GENERATED_IDENTIFIER = "__UNKNOWN__";
    private static final String T_INLINE_HTML = "T_INLINE_HTML";
    private final String sourceMimeType;
    private final Translator translator;

    public List<Embedding> getEmbeddings(Snapshot snapshot) {
        if (this.sourceMimeType.equals(snapshot.getMimeType())) {
            List<Embedding> embeddings = this.translator.translate(snapshot);
            if (embeddings.isEmpty()) {
                return Collections.emptyList();
            }
            return Collections.singletonList(Embedding.create(embeddings));
        }
        LOG.warning("Unexpected snapshot type: '" + snapshot.getMimeType() + "'; expecting '" + this.sourceMimeType + "'");
        return Collections.emptyList();
    }

    public int getPriority() {
        return 0;
    }

    public void cancel() {
    }

    public static boolean isGeneratedIdentifier(String ident) {
        return "__UNKNOWN__".equals(ident) || ident.contains("__netbeans_import__");
    }

    public static boolean containsGeneratedIdentifier(String ident) {
        return ident.contains("__UNKNOWN__") || ident.contains("__netbeans_import__");
    }

    private JsEmbeddingProvider(String sourceMimeType, Translator translator) {
        this.sourceMimeType = sourceMimeType;
        this.translator = translator;
    }

    protected static List<EmbeddingPosition> extractJsEmbeddings(String text, int sourceStart) {
        char c;
        int lineEnd;
        int start;
        LinkedList<EmbeddingPosition> embeddings = new LinkedList<EmbeddingPosition>();
        for (start = 0; start < text.length() && Character.isWhitespace(c = text.charAt(start)); ++start) {
        }
        if (start < text.length() && text.startsWith("<!--", start) && JsEmbeddingProvider.isHtmlCommentStartToSkip(text, start, lineEnd = text.indexOf(10, start))) {
            if (start > 0) {
                embeddings.add(new EmbeddingPosition(sourceStart, start));
            }
            sourceStart += ++lineEnd;
            text = text.substring(lineEnd);
        }
        Scanner scanner = new Scanner(text).useDelimiter("(<!--).*(-->)");
        while (scanner.hasNext()) {
            scanner.next();
            MatchResult match = scanner.match();
            embeddings.add(new EmbeddingPosition(sourceStart + match.start(), match.group().length()));
        }
        return embeddings;
    }

    private static boolean isHtmlCommentStartToSkip(String text, int start, int lineEnd) {
        if (lineEnd != -1) {
            if (text.startsWith("<!--//-->", start)) {
                return true;
            }
            return text.indexOf("-->", start) == -1 || lineEnd < text.indexOf("-->", start);
        }
        return false;
    }

    protected static final class EmbeddingPosition {
        private final int offset;
        private final int length;

        public EmbeddingPosition(int offset, int length) {
            this.offset = offset;
            this.length = length;
        }

        public int getLength() {
            return this.length;
        }

        public int getOffset() {
            return this.offset;
        }
    }

    private static final class JsAnalyzerState {
        int inlined_javascript_pieces = 0;
        boolean in_javascript = false;
        boolean in_inlined_javascript = false;
        boolean opening_quotation_stripped = false;
        Token<?> lastInlinedJavascriptToken = null;
        Embedding lastInlinedJavscriptEmbedding = null;

        private JsAnalyzerState() {
        }
    }

    protected static interface Translator {
        public List<Embedding> translate(Snapshot var1);
    }

    public static final class Factory
    extends TaskFactory {
        public Collection<? extends SchedulerTask> create(Snapshot snapshot) {
            if (snapshot.getMimeType().equals("text/html") && snapshot.getMimePath().size() > 1) {
                return null;
            }
            Translator t = (Translator)translators.get(snapshot.getMimeType());
            if (t != null) {
                return Collections.singleton(new JsEmbeddingProvider(snapshot.getMimeType(), t));
            }
            return Collections.emptyList();
        }
    }

}

