/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsConventionHint;

public class MissingSemicolonHint
extends JsConventionHint {
    public String getId() {
        return "jsmissingsemicolon.hint";
    }

    public String getDescription() {
        return Bundle.MissingSemicolonDisplayName();
    }

    public String getDisplayName() {
        return Bundle.MissingSemicolonDisplayName();
    }
}

