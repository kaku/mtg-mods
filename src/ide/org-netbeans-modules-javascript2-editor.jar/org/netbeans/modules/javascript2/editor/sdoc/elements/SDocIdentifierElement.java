/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocBaseElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public class SDocIdentifierElement
extends SDocBaseElement {
    private final String identifier;

    private SDocIdentifierElement(SDocElementType type, String identifier) {
        super(type);
        this.identifier = identifier;
    }

    public static SDocIdentifierElement create(SDocElementType type, String identifier) {
        return new SDocIdentifierElement(type, identifier);
    }

    public String getIdentifier() {
        return this.identifier;
    }
}

