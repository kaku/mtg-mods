/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;

public interface JsWith
extends JsObject {
    public Collection<TypeUsage> getTypes();

    public Collection<? extends JsWith> getInnerWiths();

    public JsWith getOuterWith();
}

