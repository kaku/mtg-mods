/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.DeclarationScopeImpl;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.ParameterObject;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.openide.filesystems.FileObject;

public class JsFunctionImpl
extends DeclarationScopeImpl
implements JsFunction {
    private final HashMap<String, JsObject> parametersByName;
    private final List<JsObject> parameters;
    private final Set<TypeUsage> returnTypes;
    private boolean isAnonymous;
    private boolean areReturnTypesResolved = false;

    public JsFunctionImpl(DeclarationScope scope, JsObject parentObject, Identifier name, List<Identifier> parameters, OffsetRange offsetRange, String mimeType, String sourceLabel) {
        super(scope, parentObject, name, offsetRange, mimeType, sourceLabel);
        this.parametersByName = new HashMap(parameters.size());
        this.parameters = new ArrayList<JsObject>(parameters.size());
        for (Identifier identifier : parameters) {
            ParameterObject parameter = new ParameterObject(this, identifier, mimeType, sourceLabel);
            this.addParameter(parameter);
        }
        this.isAnonymous = false;
        this.returnTypes = new HashSet<TypeUsage>();
        this.setDeclared(true);
        if (parentObject != null) {
            JsObjectImpl arguments = new JsObjectImpl((JsObject)this, new IdentifierImpl("arguments", new OffsetRange(name.getOffsetRange().getStart(), name.getOffsetRange().getStart())), name.getOffsetRange(), false, EnumSet.of(Modifier.PRIVATE), mimeType, sourceLabel);
            arguments.addAssignment(new TypeUsageImpl("Arguments", this.getOffset(), true), this.getOffset());
            this.addProperty(arguments.getName(), arguments);
        }
    }

    protected JsFunctionImpl(FileObject file, JsObject parentObject, Identifier name, List<Identifier> parameters, String mimeType, String sourceLabel) {
        this(null, parentObject, name, parameters, name.getOffsetRange(), mimeType, sourceLabel);
        this.setFileObject(file);
        this.setDeclared(false);
    }

    private JsFunctionImpl(FileObject file, Identifier name, String mimeType, String sourceLabel) {
        this(null, null, name, Collections.EMPTY_LIST, name.getOffsetRange(), mimeType, sourceLabel);
        this.setFileObject(file);
    }

    public static JsFunctionImpl createGlobal(FileObject fileObject, int length, String mimeType) {
        String name = fileObject != null ? fileObject.getName() : "VirtualSource";
        IdentifierImpl ident = new IdentifierImpl(name, new OffsetRange(0, length));
        return new JsFunctionImpl(fileObject, ident, mimeType, null);
    }

    @Override
    public final Collection<? extends JsObject> getParameters() {
        return this.parameters;
    }

    public final void addParameter(JsObject object) {
        assert (object.getParent() == this);
        this.parametersByName.put(object.getName(), object);
        this.parameters.add(object);
    }

    @Override
    public JsElement.Kind getJSKind() {
        if (this.kind != null) {
            return this.kind;
        }
        if (this.getParent() == null) {
            return JsElement.Kind.FILE;
        }
        if (this.getName().startsWith("get ")) {
            return JsElement.Kind.PROPERTY_GETTER;
        }
        if (this.getName().startsWith("set ")) {
            return JsElement.Kind.PROPERTY_SETTER;
        }
        if (this.getParent() != null && this.getParent() instanceof JsFunction) {
            JsObject prototype = null;
            for (JsObject property : this.getProperties().values()) {
                if (property.isDeclared() && (property.getModifiers().contains((Object)Modifier.PROTECTED) || property.getModifiers().contains((Object)Modifier.PUBLIC) && !property.getModifiers().contains((Object)Modifier.STATIC)) && !this.isAnonymous() && !property.isAnonymous() && !"prototype".equals(this.getParent().getName())) {
                    return JsElement.Kind.CONSTRUCTOR;
                }
                if (!"prototype".equals(property.getName())) continue;
                prototype = property;
            }
            if (prototype != null && !prototype.getProperties().isEmpty()) {
                return JsElement.Kind.CONSTRUCTOR;
            }
        }
        JsElement.Kind result = JsElement.Kind.FUNCTION;
        if (this.getParent().getJSKind() != JsElement.Kind.FILE) {
            result = JsElement.Kind.METHOD;
        }
        return result;
    }

    @Override
    public boolean isAnonymous() {
        return this.isAnonymous;
    }

    public void setAnonymous(boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    @Override
    public JsObject getParameter(String name) {
        JsObject result = this.parametersByName.get(name);
        return result;
    }

    @Override
    public Collection<? extends TypeUsage> getReturnTypes() {
        if (this.areReturnTypesResolved) {
            return Collections.EMPTY_LIST;
        }
        HashSet<TypeUsage> returns = new HashSet<TypeUsage>();
        HashSet<String> nameReturnTypes = new HashSet<String>();
        this.areReturnTypesResolved = true;
        for (TypeUsage type : this.returnTypes) {
            if (type.isResolved()) {
                if (nameReturnTypes.contains(type.getType())) continue;
                returns.add(type);
                nameReturnTypes.add(type.getType());
                continue;
            }
            if (type.getType().startsWith("@")) {
                String typeName = type.getType();
                if (typeName.endsWith(this.getName()) && typeName.startsWith("@call")) continue;
                Collection<TypeUsage> resolved = ModelUtils.resolveTypeFromSemiType(this, type);
                for (TypeUsage typeResolved : resolved) {
                    if (nameReturnTypes.contains(type.getType())) continue;
                    returns.add(typeResolved);
                    nameReturnTypes.add(typeResolved.getType());
                }
                continue;
            }
            JsObject jsObject = ModelUtils.getJsObjectByName(this, type.getType());
            if (jsObject == null) {
                JsObject global = ModelUtils.getGlobalObject(this);
                jsObject = ModelUtils.findJsObjectByName(global, type.getType());
            }
            if (jsObject != null) {
                Collection<TypeUsage> resolveAssignments = this.resolveAssignments(jsObject, type.getOffset());
                for (TypeUsage typeResolved : resolveAssignments) {
                    if (nameReturnTypes.contains(typeResolved.getType())) continue;
                    returns.add(typeResolved);
                    nameReturnTypes.add(typeResolved.getType());
                }
                continue;
            }
            returns.add(type);
            nameReturnTypes.add(type.getType());
        }
        this.areReturnTypesResolved = false;
        return returns;
    }

    @Override
    public void addReturnType(TypeUsage type) {
        boolean isThere = false;
        for (TypeUsage typeUsage : this.returnTypes) {
            if (!type.getType().equals(typeUsage.getType())) continue;
            isThere = true;
        }
        if (!isThere) {
            this.returnTypes.add(type);
        }
    }

    public void addReturnType(Collection<TypeUsage> types) {
        for (TypeUsage typeUsage : types) {
            this.addReturnType(typeUsage);
        }
    }

    public boolean areReturnTypesEmpty() {
        return this.returnTypes.isEmpty();
    }

    @Override
    public void resolveTypes(JsDocumentationHolder docHolder) {
        super.resolveTypes(docHolder);
        HashSet<String> nameReturnTypes = new HashSet<String>();
        ArrayList<TypeUsage> resolved = new ArrayList<TypeUsage>();
        for (TypeUsage type2 : this.returnTypes) {
            if (type2.getType().equals("unresolved") && this.returnTypes.size() > 1) continue;
            if (!type2.isResolved()) {
                Iterator<TypeUsage> i$ = ModelUtils.resolveTypeFromSemiType(this, type2).iterator();
                while (i$.hasNext()) {
                    TypeUsage rType = i$.next();
                    if (nameReturnTypes.contains(rType.getType())) continue;
                    if ("@this;".equals(type2.getType())) {
                        rType = new TypeUsageImpl(rType.getType(), -1, rType.isResolved());
                    }
                    resolved.add(rType);
                    nameReturnTypes.add(rType.getType());
                }
                continue;
            }
            if (nameReturnTypes.contains(type2.getType())) continue;
            resolved.add(type2);
            nameReturnTypes.add(type2.getType());
        }
        for (TypeUsage type : resolved) {
            if (type.getOffset() <= 0) continue;
            JsObject jsObject = ModelUtils.findJsObjectByName(this, type.getType());
            if (jsObject == null) {
                JsObject global = ModelUtils.getGlobalObject(this);
                jsObject = ModelUtils.findJsObjectByName(global, type.getType());
            }
            if (jsObject == null) continue;
            int index = type.getType().lastIndexOf(46);
            int typeLength = index > -1 ? type.getType().length() - index - 1 : type.getType().length();
            ((JsObjectImpl)jsObject).addOccurrence(new OffsetRange(type.getOffset(), jsObject.isAnonymous() ? type.getOffset() : type.getOffset() + typeLength));
        }
        this.returnTypes.clear();
        this.returnTypes.addAll(resolved);
        for (JsObject param : this.parameters) {
            Collection<? extends TypeUsage> types = param.getAssignmentForOffset(param.getDeclarationName().getOffsetRange().getStart());
            for (TypeUsage type3 : types) {
                JsObject jsObject = ModelUtils.getJsObjectByName(this, type3.getType());
                if (jsObject == null) continue;
                ModelUtils.addDocTypesOccurence(jsObject, docHolder);
                JsFunctionImpl.moveOccurrenceOfProperties((JsObjectImpl)jsObject, param);
            }
            ArrayList<? extends JsObject> paramProperties = new ArrayList<JsObject>(param.getProperties().values());
            for (JsObject paramProperty : paramProperties) {
                ((JsObjectImpl)paramProperty).resolveTypes(docHolder);
            }
        }
    }
}

