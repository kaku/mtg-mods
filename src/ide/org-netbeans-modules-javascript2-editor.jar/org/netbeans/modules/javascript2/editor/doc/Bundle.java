/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.doc;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String JsDocumentationPrinter_title_deprecated() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.deprecated");
    }

    static String JsDocumentationPrinter_title_description() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.description");
    }

    static String JsDocumentationPrinter_title_examples() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.examples");
    }

    static String JsDocumentationPrinter_title_extends() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.extends");
    }

    static String JsDocumentationPrinter_title_parameters() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.parameters");
    }

    static String JsDocumentationPrinter_title_returns() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.returns");
    }

    static String JsDocumentationPrinter_title_see() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.see");
    }

    static String JsDocumentationPrinter_title_since() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.since");
    }

    static String JsDocumentationPrinter_title_throws() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.throws");
    }

    static String JsDocumentationPrinter_title_type() {
        return NbBundle.getMessage(Bundle.class, (String)"JsDocumentationPrinter.title.type");
    }

    private void Bundle() {
    }
}

