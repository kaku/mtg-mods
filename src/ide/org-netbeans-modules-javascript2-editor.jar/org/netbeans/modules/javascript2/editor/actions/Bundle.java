/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.actions;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String complete_line() {
        return NbBundle.getMessage(Bundle.class, (String)"complete-line");
    }

    static String complete_line_newline() {
        return NbBundle.getMessage(Bundle.class, (String)"complete-line-newline");
    }

    private void Bundle() {
    }
}

