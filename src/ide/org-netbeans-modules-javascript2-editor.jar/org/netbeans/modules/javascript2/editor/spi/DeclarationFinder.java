/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.modules.csl.api.DeclarationFinder
 */
package org.netbeans.modules.javascript2.editor.spi;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.netbeans.api.annotations.common.SuppressWarnings;

@SuppressWarnings(value={"NM_SAME_SIMPLE_NAME_AS_INTERFACE"}, justification="Own SPI mathcing and extending the original one")
public interface DeclarationFinder
extends org.netbeans.modules.csl.api.DeclarationFinder {

    @Retention(value=RetentionPolicy.SOURCE)
    @Target(value={ElementType.TYPE})
    public static @interface Registration {
        public int priority() default 100;
    }

}

