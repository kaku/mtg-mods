/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HtmlFormatter
 */
package org.netbeans.modules.javascript2.editor.jsdoc.completion;

import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;

public class LinkTag
extends AnnotationCompletionTag {
    public static final String TEMPLATE = " ${namePath}";

    public LinkTag(String name) {
        super(name, name + " ${namePath}");
    }

    @Override
    public void formatParameters(HtmlFormatter formatter) {
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("namePath");
        formatter.parameters(false);
    }
}

