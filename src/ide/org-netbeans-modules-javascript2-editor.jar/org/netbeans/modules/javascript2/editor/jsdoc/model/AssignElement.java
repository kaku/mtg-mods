/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementImpl;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.NamePath;

public class AssignElement
extends JsDocElementImpl {
    private final NamePath otherMemberName;
    private final NamePath thisMemberName;

    private AssignElement(JsDocElementType type, NamePath otherMemberName, NamePath thisMemberName) {
        super(type);
        this.otherMemberName = otherMemberName;
        this.thisMemberName = thisMemberName;
    }

    public static AssignElement create(JsDocElementType type, NamePath otherMemberName, NamePath thisMemberName) {
        return new AssignElement(type, otherMemberName, thisMemberName);
    }

    public NamePath getOtherMemberName() {
        return this.otherMemberName;
    }

    public NamePath getThisMemberName() {
        return this.thisMemberName;
    }
}

