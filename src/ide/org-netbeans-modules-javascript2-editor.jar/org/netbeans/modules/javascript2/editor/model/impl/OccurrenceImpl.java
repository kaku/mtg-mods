/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Occurrence;

public class OccurrenceImpl
implements Occurrence {
    private final OffsetRange offsetRange;
    private final List<JsObject> declarations;

    public OccurrenceImpl(OffsetRange offsetRange, JsObject declaration) {
        this.offsetRange = offsetRange;
        this.declarations = new ArrayList<JsObject>(1);
        this.declarations.add(declaration);
    }

    @Override
    public OffsetRange getOffsetRange() {
        return this.offsetRange;
    }

    @Override
    public Collection<? extends JsObject> getDeclarations() {
        return this.declarations;
    }

    public int hashCode() {
        return this.offsetRange.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        OccurrenceImpl other = (OccurrenceImpl)obj;
        if (!(this.offsetRange == other.offsetRange || this.offsetRange != null && this.offsetRange.equals((Object)other.offsetRange))) {
            return false;
        }
        return true;
    }
}

