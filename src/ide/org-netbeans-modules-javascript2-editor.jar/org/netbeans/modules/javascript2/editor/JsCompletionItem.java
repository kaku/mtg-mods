/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.javascript2.editor;

import java.awt.Image;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.Bundle;
import org.netbeans.modules.javascript2.editor.JsKeyWords;
import org.netbeans.modules.javascript2.editor.Utils;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.options.OptionsUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;

public class JsCompletionItem
implements CompletionProposal {
    protected final CompletionRequest request;
    private final ElementHandle element;
    private static ImageIcon priviligedIcon = null;

    JsCompletionItem(ElementHandle element, CompletionRequest request) {
        this.element = element;
        this.request = request;
    }

    public int getAnchorOffset() {
        return LexUtilities.getLexerOffset((JsParserResult)this.request.info, this.request.anchor);
    }

    public ElementHandle getElement() {
        return this.element;
    }

    public String getName() {
        return this.element.getName();
    }

    public String getInsertPrefix() {
        return this.element.getName();
    }

    public String getSortText() {
        StringBuilder sb = new StringBuilder();
        if (this.element != null) {
            FileObject sourceFo = this.request.result.getSnapshot().getSource().getFileObject();
            FileObject elementFo = this.element.getFileObject();
            if (elementFo != null && sourceFo != null && sourceFo.equals((Object)elementFo)) {
                sb.append("1");
            } else if (OffsetRange.NONE.equals((Object)this.element.getOffsetRange((ParserResult)this.request.result))) {
                sb.append("8");
            } else {
                sb.append("9");
            }
        }
        sb.append(this.getName());
        return sb.toString();
    }

    protected boolean isDeprecated() {
        return this.element.getModifiers().contains((Object)Modifier.DEPRECATED);
    }

    public String getLhsHtml(HtmlFormatter formatter) {
        this.formatName(formatter);
        return formatter.getText();
    }

    protected void formatName(HtmlFormatter formatter) {
        if (this.isDeprecated()) {
            formatter.deprecated(true);
            formatter.appendText(this.getName());
            formatter.deprecated(false);
        } else {
            formatter.appendText(this.getName());
        }
    }

    public String getRhsHtml(HtmlFormatter formatter) {
        String location = null;
        if (this.element instanceof JsElement) {
            JsElement jsElement = (JsElement)this.element;
            if (jsElement.isPlatform()) {
                location = Bundle.JsCompletionItem_lbl_js_platform();
            } else if (jsElement.getSourceLabel() != null) {
                location = jsElement.getSourceLabel();
            }
        }
        if (location == null) {
            location = this.getFileNameURL();
        }
        if (location == null) {
            return null;
        }
        formatter.reset();
        boolean isgues = OffsetRange.NONE.equals((Object)this.element.getOffsetRange((ParserResult)this.request.result));
        if (isgues) {
            formatter.appendHtml("<font color=#999999>");
        }
        formatter.appendText(location);
        if (isgues) {
            formatter.appendHtml("</font>");
        }
        return formatter.getText();
    }

    public ElementKind getKind() {
        return this.element.getKind();
    }

    public ImageIcon getIcon() {
        return null;
    }

    public Set<Modifier> getModifiers() {
        Set emptyModifiers = Collections.emptySet();
        ElementHandle handle = this.getElement();
        return handle != null ? handle.getModifiers() : emptyModifiers;
    }

    public boolean isSmart() {
        return false;
    }

    public int getSortPrioOverride() {
        int order = 100;
        if (this.element != null) {
            if (((JsElement)this.element).isPlatform()) {
                order = 0;
            }
            if (OffsetRange.NONE.equals((Object)this.element.getOffsetRange((ParserResult)this.request.result))) {
                order = 120;
            }
        }
        return order;
    }

    public String getCustomInsertTemplate() {
        return null;
    }

    @CheckForNull
    public final String getFileNameURL() {
        ElementHandle elem = this.getElement();
        if (elem == null) {
            return null;
        }
        FileObject fo = elem.getFileObject();
        if (fo != null) {
            return fo.getNameExt();
        }
        return this.getName();
    }

    public static class Factory {
        public static void create(Map<String, List<JsElement>> items, CompletionRequest request, List<CompletionProposal> result) {
            HashMap<String, Set> resolvedTypes = new HashMap<String, Set>();
            for (Map.Entry<String, List<JsElement>> entry : items.entrySet()) {
                HashMap<String, JsCompletionItem> signatures = new HashMap<String, JsCompletionItem>();
                block5 : for (JsElement element : entry.getValue()) {
                    JsCompletionItem item;
                    String signature;
                    switch (element.getJSKind()) {
                        Set resolvedType;
                        case CONSTRUCTOR: 
                        case FUNCTION: 
                        case METHOD: {
                            HashSet<String> returnTypes = new HashSet<String>();
                            LinkedHashMap<String, Set<String>> allParameters = new LinkedHashMap<String, Set<String>>();
                            if (element instanceof JsFunction) {
                                Collection<TypeUsage> resolveTypes = ModelUtils.resolveTypes(((JsFunction)element).getReturnTypes(), (JsParserResult)request.info, OptionsUtils.forLanguage(JsTokenId.javascriptLanguage()).autoCompletionTypeResolution());
                                returnTypes.addAll(Utils.getDisplayNames(resolveTypes));
                                for (JsObject jsObject : ((JsFunction)element).getParameters()) {
                                    HashSet paramTypes = new HashSet();
                                    for (TypeUsage type : jsObject.getAssignmentForOffset(jsObject.getOffset() + 1)) {
                                        resolvedType = (Set)resolvedTypes.get(type.getType());
                                        if (resolvedType == null) {
                                            resolvedType = new HashSet(1);
                                            String displayName = type.getDisplayName();
                                            if (!displayName.isEmpty()) {
                                                resolvedType.add(displayName);
                                            }
                                            resolvedTypes.put(type.getType(), resolvedType);
                                        }
                                        paramTypes.addAll(resolvedType);
                                    }
                                    allParameters.put(jsObject.getName(), paramTypes);
                                }
                            } else if (element instanceof IndexedElement.FunctionIndexedElement) {
                                HashSet<TypeUsageImpl> returnTypeUsages = new HashSet<TypeUsageImpl>();
                                for (String type : ((IndexedElement.FunctionIndexedElement)element).getReturnTypes()) {
                                    returnTypeUsages.add(new TypeUsageImpl(type, -1, false));
                                }
                                Collection<TypeUsage> resolveTypes = ModelUtils.resolveTypes(returnTypeUsages, (JsParserResult)request.info, OptionsUtils.forLanguage(JsTokenId.javascriptLanguage()).autoCompletionTypeResolution());
                                returnTypes.addAll(Utils.getDisplayNames(resolveTypes));
                                LinkedHashMap<String, Collection<String>> parameters = ((IndexedElement.FunctionIndexedElement)element).getParameters();
                                for (Map.Entry<String, Collection<String>> paramEntry : parameters.entrySet()) {
                                    HashSet paramTypes = new HashSet();
                                    for (String type2 : paramEntry.getValue()) {
                                        HashSet<String> resolvedType2 = (HashSet<String>)resolvedTypes.get(type2);
                                        if (resolvedType2 == null) {
                                            resolvedType2 = new HashSet<String>(1);
                                            String displayName = ModelUtils.getDisplayName(type2);
                                            if (!displayName.isEmpty()) {
                                                resolvedType2.add(displayName);
                                            }
                                            resolvedTypes.put(type2, resolvedType2);
                                        }
                                        paramTypes.addAll(resolvedType2);
                                    }
                                    allParameters.put(paramEntry.getKey(), paramTypes);
                                }
                            }
                            if (signatures.containsKey(signature = Factory.createFnSignature(entry.getKey(), allParameters, returnTypes))) continue block5;
                            JsFunctionCompletionItem item2 = new JsFunctionCompletionItem(element, request, returnTypes, allParameters);
                            signatures.put(signature, item2);
                            continue block5;
                        }
                        case PARAMETER: 
                        case PROPERTY: 
                        case PROPERTY_GETTER: 
                        case PROPERTY_SETTER: 
                        case FIELD: 
                        case VARIABLE: {
                            void assignment22;
                            HashSet<String> typesToDisplay = new HashSet<String>();
                            Object assignment22 = null;
                            if (element instanceof JsObject) {
                                JsObject jsObject = (JsObject)element;
                                Collection<? extends TypeUsage> assignment22 = jsObject.getAssignmentForOffset(request.anchor);
                            } else if (element instanceof IndexedElement) {
                                IndexedElement iElement = (IndexedElement)element;
                                Collection<TypeUsage> assignment22 = iElement.getAssignments();
                            }
                            if (assignment22 != null && !assignment22.isEmpty()) {
                                HashSet<TypeUsage> toResolve = new HashSet<TypeUsage>();
                                for (TypeUsage type : assignment22) {
                                    if (type.isResolved()) {
                                        typesToDisplay.add(type.getDisplayName());
                                        continue;
                                    }
                                    resolvedType = (HashSet)resolvedTypes.get(type.getType());
                                    if (resolvedType == null) {
                                        toResolve.clear();
                                        toResolve.add(type);
                                        resolvedType = new HashSet(1);
                                        Collection<TypeUsage> resolved = ModelUtils.resolveTypes(toResolve, request.result, OptionsUtils.forLanguage(JsTokenId.javascriptLanguage()).autoCompletionTypeResolution());
                                        for (TypeUsage rType : resolved) {
                                            String displayName = rType.getDisplayName();
                                            if (displayName.isEmpty()) continue;
                                            resolvedType.add(displayName);
                                        }
                                        resolvedTypes.put(type.getType(), resolvedType);
                                    }
                                    typesToDisplay.addAll(resolvedType);
                                }
                            }
                            if (signatures.containsKey(signature = element.getName() + ":" + Factory.createTypeSignature(typesToDisplay))) continue block5;
                            item = new JsPropertyCompletionItem(element, request, typesToDisplay);
                            signatures.put(signature, item);
                            continue block5;
                        }
                    }
                    signature = element.getName();
                    if (signatures.containsKey(signature)) continue;
                    item = new JsCompletionItem(element, request);
                    signatures.put(signature, item);
                }
                for (JsCompletionItem item : signatures.values()) {
                    result.add(item);
                }
            }
        }

        private static String createFnSignature(String name, HashMap<String, Set<String>> params, Set<String> returnTypes) {
            StringBuilder sb = new StringBuilder();
            sb.append(name).append('(');
            for (Map.Entry<String, Set<String>> entry : params.entrySet()) {
                sb.append(entry.getKey()).append(':');
                sb.append(Factory.createTypeSignature(entry.getValue()));
                sb.append(',');
            }
            sb.append(')');
            sb.append(Factory.createTypeSignature(returnTypes));
            return sb.toString();
        }

        private static String createTypeSignature(Set<String> types) {
            StringBuilder sb = new StringBuilder();
            for (String name : types) {
                sb.append(name).append('|');
            }
            return sb.toString();
        }
    }

    public static class JsPropertyCompletionItem
    extends JsCompletionItem {
        private final Set<String> resolvedTypes;

        JsPropertyCompletionItem(ElementHandle element, CompletionRequest request, Set<String> resolvedTypes) {
            super(element, request);
            this.resolvedTypes = resolvedTypes != null ? resolvedTypes : Collections.EMPTY_SET;
        }

        @Override
        public String getLhsHtml(HtmlFormatter formatter) {
            this.formatName(formatter);
            if (!this.resolvedTypes.isEmpty()) {
                formatter.type(true);
                formatter.appendText(": ");
                Iterator<String> it = this.resolvedTypes.iterator();
                while (it.hasNext()) {
                    formatter.appendText(it.next());
                    if (!it.hasNext()) continue;
                    formatter.appendText("|");
                }
                formatter.type(false);
            }
            return formatter.getText();
        }
    }

    static class KeywordItem
    extends JsCompletionItem {
        private static ImageIcon keywordIcon = null;
        private String keyword = null;

        public KeywordItem(String keyword, CompletionRequest request) {
            super(null, request);
            this.keyword = keyword;
        }

        @Override
        public String getName() {
            return this.keyword;
        }

        @Override
        public String getLhsHtml(HtmlFormatter formatter) {
            formatter.name(this.getKind(), true);
            formatter.appendText(this.getName());
            formatter.name(this.getKind(), false);
            return formatter.getText();
        }

        @Override
        public ElementKind getKind() {
            return ElementKind.KEYWORD;
        }

        @Override
        public String getRhsHtml(HtmlFormatter formatter) {
            return null;
        }

        @Override
        public ImageIcon getIcon() {
            if (keywordIcon == null) {
                keywordIcon = new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/javascript2/editor/resources/javascript.png"));
            }
            return keywordIcon;
        }

        @Override
        public String getInsertPrefix() {
            return this.getName();
        }

        @Override
        public String getCustomInsertTemplate() {
            StringBuilder builder = new StringBuilder();
            JsKeyWords.CompletionType type = JsKeyWords.KEYWORDS.get(this.getName());
            if (type == null) {
                return this.getName();
            }
            switch (type) {
                case SIMPLE: {
                    builder.append(this.getName());
                    break;
                }
                case ENDS_WITH_SPACE: {
                    builder.append(this.getName());
                    builder.append(" ${cursor}");
                    break;
                }
                case CURSOR_INSIDE_BRACKETS: {
                    builder.append(this.getName());
                    builder.append("(${cursor})");
                    break;
                }
                case ENDS_WITH_CURLY_BRACKETS: {
                    builder.append(this.getName());
                    builder.append(" {${cursor}}");
                    break;
                }
                case ENDS_WITH_SEMICOLON: {
                    builder.append(this.getName());
                    CharSequence text = this.request.info.getSnapshot().getText();
                    int index = this.request.anchor + this.request.prefix.length();
                    if (index != text.length() && ';' == text.charAt(index)) break;
                    builder.append(";");
                    break;
                }
                case ENDS_WITH_COLON: {
                    builder.append(this.getName());
                    builder.append(" ${cursor}:");
                    break;
                }
                case ENDS_WITH_DOT: {
                    builder.append(this.getName());
                    builder.append(".${cursor}");
                    break;
                }
                default: {
                    assert (false);
                    break;
                }
            }
            return builder.toString();
        }

        @Override
        public int getSortPrioOverride() {
            return 110;
        }
    }

    public static class JsFunctionCompletionItem
    extends JsCompletionItem {
        private final Set<String> returnTypes;
        private final Map<String, Set<String>> parametersTypes;

        JsFunctionCompletionItem(ElementHandle element, CompletionRequest request, Set<String> resolvedReturnTypes, Map<String, Set<String>> parametersTypes) {
            super(element, request);
            this.returnTypes = resolvedReturnTypes != null ? resolvedReturnTypes : Collections.EMPTY_SET;
            this.parametersTypes = parametersTypes != null ? parametersTypes : Collections.EMPTY_MAP;
        }

        @Override
        public String getLhsHtml(HtmlFormatter formatter) {
            formatter.setMaxLength(OptionsUtils.forLanguage(JsTokenId.javascriptLanguage()).getCodeCompletionItemSignatureWidth());
            formatter.emphasis(true);
            this.formatName(formatter);
            formatter.emphasis(false);
            formatter.appendText("(");
            this.appendParamsStr(formatter);
            formatter.appendText(")");
            this.appendReturnTypes(formatter);
            return formatter.getText();
        }

        private void appendParamsStr(HtmlFormatter formatter) {
            Iterator<Map.Entry<String, Set<String>>> it = this.parametersTypes.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Set<String>> entry = it.next();
                formatter.parameters(true);
                formatter.appendText(entry.getKey());
                formatter.parameters(false);
                Collection types = entry.getValue();
                if (!types.isEmpty()) {
                    formatter.type(true);
                    formatter.appendText(": ");
                    Iterator itTypes = types.iterator();
                    while (itTypes.hasNext()) {
                        formatter.appendText((String)itTypes.next());
                        if (!itTypes.hasNext()) continue;
                        formatter.appendText("|");
                    }
                    formatter.type(false);
                }
                if (!it.hasNext()) continue;
                formatter.appendText(", ");
            }
        }

        private void appendReturnTypes(HtmlFormatter formatter) {
            if (!this.returnTypes.isEmpty()) {
                formatter.appendText(": ");
                formatter.type(true);
                Iterator<String> it = this.returnTypes.iterator();
                while (it.hasNext()) {
                    formatter.appendText(it.next());
                    if (!it.hasNext()) continue;
                    formatter.appendText("|");
                }
                formatter.type(false);
            }
        }

        @Override
        public String getCustomInsertTemplate() {
            StringBuilder template = new StringBuilder();
            template.append(this.getName());
            template.append("(${cursor})");
            return template.toString();
        }

        @Override
        public ImageIcon getIcon() {
            if (this.getModifiers().contains((Object)Modifier.PROTECTED)) {
                if (priviligedIcon == null) {
                    priviligedIcon = new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/javascript2/editor/resources/methodPriviliged.png"));
                }
                return priviligedIcon;
            }
            return super.getIcon();
        }
    }

    public static class CompletionRequest {
        public int anchor;
        public JsParserResult result;
        public ParserResult info;
        public String prefix;
    }

}

