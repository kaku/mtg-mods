/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor$MutableContext
 */
package org.netbeans.modules.javascript2.editor;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.spi.editor.typinghooks.CamelCaseInterceptor;

public class JsCamelCaseInterceptor
implements CamelCaseInterceptor {
    public boolean beforeChange(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
        return false;
    }

    public void change(final CamelCaseInterceptor.MutableContext context) throws BadLocationException {
        final Document doc = context.getDocument();
        final int offset = context.getOffset();
        final boolean reverse = context.isBackward();
        doc.render(new Runnable(){

            @Override
            public void run() {
                int nextOffset = JsCamelCaseInterceptor.getWordOffset(doc, offset, reverse);
                context.setNextWordOffset(nextOffset);
            }
        });
    }

    public void afterChange(CamelCaseInterceptor.MutableContext context) throws BadLocationException {
    }

    public void cancelled(CamelCaseInterceptor.MutableContext context) {
    }

    protected static int getWordOffset(Document doc, int offset, boolean reverse) {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(doc, offset);
        if (ts == null) {
            return -1;
        }
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return -1;
        }
        if (reverse && ts.offset() == offset && !ts.movePrevious()) {
            return -1;
        }
        Token token = ts.token();
        if (token.id() == JsTokenId.WHITESPACE && (reverse && ts.offset() < offset || !reverse && ts.offset() > offset)) {
            return ts.offset();
        }
        if (token.id() == JsTokenId.IDENTIFIER) {
            String image = token.text().toString();
            int imageLength = image.length();
            int offsetInImage = offset - ts.offset();
            if (reverse) {
                return JsCamelCaseInterceptor.getPreviousIdentifierWordOffset(ts, image, imageLength, offsetInImage);
            }
            return JsCamelCaseInterceptor.getNextIdentifierWordOffset(ts, image, imageLength, offsetInImage);
        }
        return -1;
    }

    private static int getPreviousIdentifierWordOffset(TokenSequence<? extends JsTokenId> ts, String image, int imageLength, int offsetInImage) {
        if (--offsetInImage < 0) {
            return -1;
        }
        if (offsetInImage < imageLength && Character.isUpperCase(image.charAt(offsetInImage))) {
            for (int i = offsetInImage - 1; i >= 0; --i) {
                char charAtI = image.charAt(i);
                if (Character.isUpperCase(charAtI)) continue;
                return ts.offset() + i + 1;
            }
            return ts.offset();
        }
        for (int i = offsetInImage - 1; i >= 0; --i) {
            char charAtI = image.charAt(i);
            if (!Character.isUpperCase(charAtI)) continue;
            for (int j = i; j >= 0; --j) {
                char charAtJ = image.charAt(j);
                if (Character.isUpperCase(charAtJ)) continue;
                return ts.offset() + j + 1;
            }
            return ts.offset();
        }
        return ts.offset();
    }

    private static int getNextIdentifierWordOffset(TokenSequence<? extends JsTokenId> ts, String image, int imageLength, int offsetInImage) {
        char charAtI;
        int i;
        int start = offsetInImage + 1;
        if (offsetInImage < 0 || offsetInImage >= image.length()) {
            return -1;
        }
        if (Character.isUpperCase(image.charAt(offsetInImage))) {
            for (i = start; i < imageLength && Character.isUpperCase(charAtI = image.charAt(i)); ++i) {
                ++start;
            }
        }
        for (i = start; i < imageLength; ++i) {
            charAtI = image.charAt(i);
            if (!Character.isUpperCase(charAtI)) continue;
            return ts.offset() + i;
        }
        return -1;
    }

    public static class Factory
    implements CamelCaseInterceptor.Factory {
        public CamelCaseInterceptor createCamelCaseInterceptor(MimePath mimePath) {
            return new JsCamelCaseInterceptor();
        }
    }

}

