/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.javascript2.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.modules.javascript2.editor.spi.CompletionProvider;
import org.netbeans.modules.javascript2.editor.spi.DeclarationFinder;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public final class EditorExtender {
    public static final String COMPLETION_PROVIDERS_PATH = "JavaScript/Editor/CompletionInterceptors";
    public static final String DECLARATION_FINDERS_PATH = "JavaScript/Editor/DeclarationFinderInterceptors";
    private static final Lookup.Result<CompletionProvider> COMPLETION_PROVIDERS = Lookups.forPath((String)"JavaScript/Editor/CompletionInterceptors").lookupResult(CompletionProvider.class);
    private static final Lookup.Result<DeclarationFinder> DECLARATION_FINDERS = Lookups.forPath((String)"JavaScript/Editor/DeclarationFinderInterceptors").lookupResult(DeclarationFinder.class);
    private static EditorExtender instance;

    private EditorExtender() {
    }

    public static synchronized EditorExtender getDefault() {
        if (instance == null) {
            instance = new EditorExtender();
        }
        return instance;
    }

    public List<CompletionProvider> getCompletionProviders() {
        return new ArrayList<CompletionProvider>(COMPLETION_PROVIDERS.allInstances());
    }

    public List<DeclarationFinder> getDeclarationFinders() {
        return new ArrayList<DeclarationFinder>(DECLARATION_FINDERS.allInstances());
    }
}

