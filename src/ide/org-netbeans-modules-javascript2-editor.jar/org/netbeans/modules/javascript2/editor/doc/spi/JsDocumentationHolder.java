/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.Node
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jdk.nashorn.internal.ir.Node;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.doc.DocumentationUtils;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationPrinter;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsModifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.parsing.api.Snapshot;

public abstract class JsDocumentationHolder {
    private final Snapshot snapshot;
    private final JsDocumentationProvider provider;
    private Map<String, List<OffsetRange>> occurencesMap = null;

    public JsDocumentationHolder(JsDocumentationProvider provider, Snapshot snapshot) {
        this.provider = provider;
        this.snapshot = snapshot;
    }

    public abstract Map<Integer, ? extends JsComment> getCommentBlocks();

    public final Map<String, List<OffsetRange>> getOccurencesMap() {
        if (this.occurencesMap == null) {
            this.occurencesMap = new HashMap<String, List<OffsetRange>>();
            for (Map.Entry<Integer, ? extends JsComment> entry : this.getCommentBlocks().entrySet()) {
                JsComment comment = entry.getValue();
                for (DocParameter docParameter : comment.getParameters()) {
                    for (Type type : docParameter.getParamTypes()) {
                        this.insertIntoOccurencesMap(type);
                    }
                }
                DocParameter returnType = comment.getReturnType();
                if (returnType == null) continue;
                for (Type type : returnType.getParamTypes()) {
                    this.insertIntoOccurencesMap(type);
                }
            }
        }
        return this.occurencesMap;
    }

    private void insertIntoOccurencesMap(Type type) {
        if (type.getType().trim().isEmpty()) {
            return;
        }
        if (!this.occurencesMap.containsKey(type.getType())) {
            this.occurencesMap.put(type.getType(), new LinkedList<E>());
        }
        this.occurencesMap.get(type.getType()).add(DocumentationUtils.getOffsetRange(type));
    }

    public JsDocumentationProvider getProvider() {
        return this.provider;
    }

    public List<Type> getReturnType(Node node) {
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null && comment.getReturnType() != null) {
            return comment.getReturnType().getParamTypes();
        }
        return Collections.emptyList();
    }

    public List<DocParameter> getParameters(Node node) {
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null) {
            return comment.getParameters();
        }
        return Collections.emptyList();
    }

    public Documentation getDocumentation(Node node) {
        String content;
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null && !(content = JsDocumentationPrinter.printDocumentation(comment)).isEmpty()) {
            return Documentation.create((String)content);
        }
        return null;
    }

    public boolean isDeprecated(Node node) {
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null) {
            return comment.getDeprecated() != null;
        }
        return false;
    }

    public boolean isClass(Node node) {
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null) {
            return comment.isClass();
        }
        return false;
    }

    public List<Type> getExtends(Node node) {
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null) {
            return comment.getExtends();
        }
        return Collections.emptyList();
    }

    public Set<JsModifier> getModifiers(Node node) {
        JsComment comment = this.getCommentForOffset(node.getStart(), this.getCommentBlocks());
        if (comment != null) {
            return comment.getModifiers();
        }
        return Collections.emptySet();
    }

    public boolean isWhitespaceToken(Token<? extends JsTokenId> token) {
        return token.id() == JsTokenId.EOL || token.id() == JsTokenId.WHITESPACE || token.id() == JsTokenId.BLOCK_COMMENT || token.id() == JsTokenId.DOC_COMMENT || token.id() == JsTokenId.LINE_COMMENT;
    }

    public JsComment getCommentForOffset(int offset, Map<Integer, ? extends JsComment> comments) {
        int endOffset = this.getEndOffsetOfAssociatedComment(offset);
        if (endOffset > 0) {
            return comments.get(endOffset);
        }
        return null;
    }

    public Snapshot getSnapshot() {
        return this.snapshot;
    }

    private int getEndOffsetOfAssociatedComment(int offset) {
        TokenHierarchy tokenHierarchy = this.snapshot.getTokenHierarchy();
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(tokenHierarchy, offset);
        if (ts != null) {
            ts.move(offset);
            ts.moveNext();
            while (ts.movePrevious() && ts.token().id() != JsTokenId.DOC_COMMENT && ts.token().id() != JsTokenId.BLOCK_COMMENT && ts.token().id() != JsTokenId.BRACKET_RIGHT_CURLY && ts.token().id() != JsTokenId.BRACKET_LEFT_CURLY && ts.token().id() != JsTokenId.OPERATOR_SEMICOLON) {
            }
            if (ts.token() != null && ts.token().id() == JsTokenId.DOC_COMMENT) {
                return ts.token().offset(tokenHierarchy) + ts.token().length();
            }
            while (ts.movePrevious()) {
                if (ts.token().id() == JsTokenId.DOC_COMMENT) {
                    return ts.token().offset(tokenHierarchy) + ts.token().length();
                }
                if (this.isWhitespaceToken(ts.token())) continue;
                return -1;
            }
        }
        return -1;
    }
}

