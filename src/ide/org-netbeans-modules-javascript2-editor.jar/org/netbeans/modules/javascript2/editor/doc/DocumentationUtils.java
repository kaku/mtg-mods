/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.doc;

import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.Type;

public class DocumentationUtils {
    public static OffsetRange getOffsetRange(Type type) {
        int startOffset = type.getOffset();
        return new OffsetRange(startOffset, startOffset + type.getType().length());
    }
}

