/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.jsdoc.model.AssignElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.DeclarationElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.DescriptionElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.LinkElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.NamePath;
import org.netbeans.modules.javascript2.editor.jsdoc.model.NamedParameterElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.ParameterElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.SimpleElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.UnnamedParameterElement;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;

public class JsDocElementUtils {
    private static final String LOWERCASED_NUMBER = "Number".toLowerCase();
    private static final String LOWERCASED_STRING = "String".toLowerCase();
    private static final String LOWERCASED_BOOLEAN = "Boolean".toLowerCase();
    public static final int LIMIT_SPACES_IN_TYPE = 10;

    public static JsDocElement createElementForType(JsDocElementType type, String tagDescription, int descStartOffset) {
        switch (type.getCategory()) {
            case ASSIGN: {
                String[] values = tagDescription.split("(\\s)*as(\\s)*");
                return AssignElement.create(type, values.length > 0 ? new NamePath(values[0].trim()) : null, values.length > 1 ? new NamePath(values[1].trim()) : null);
            }
            case DECLARATION: {
                return JsDocElementUtils.createDeclarationElement(type, tagDescription, descStartOffset);
            }
            case DESCRIPTION: {
                return DescriptionElement.create(type, tagDescription);
            }
            case LINK: {
                return LinkElement.create(type, new NamePath(tagDescription));
            }
            case NAMED_PARAMETER: {
                return JsDocElementUtils.createParameterElement(type, tagDescription, descStartOffset);
            }
            case SIMPLE: {
                return SimpleElement.create(type);
            }
            case UNNAMED_PARAMETER: {
                return JsDocElementUtils.createParameterElement(type, tagDescription, descStartOffset);
            }
        }
        return DescriptionElement.create(type, tagDescription);
    }

    public static List<Type> parseTypes(String textToParse, int offset) {
        String[] typesArray;
        LinkedList<Type> types = new LinkedList<Type>();
        for (String string : typesArray = textToParse.split("[|]")) {
            if (string.trim().isEmpty()) continue;
            types.add(JsDocElementUtils.createTypeUsage(string, offset + textToParse.indexOf(string)));
        }
        return types;
    }

    private static DeclarationElement createDeclarationElement(JsDocElementType elementType, String elementText, int descStartOffset) {
        String type = elementText;
        int typeOffset = descStartOffset + (elementText.indexOf("{") == -1 ? 0 : elementText.indexOf("{") + 1);
        if (typeOffset > 0 && elementText.endsWith("}")) {
            type = type.substring(1, type.length() - 1);
        }
        return DeclarationElement.create(elementType, JsDocElementUtils.createTypeUsage(type, typeOffset));
    }

    private static TypeUsage createTypeUsage(String type, int offset) {
        if (LOWERCASED_STRING.equals(type)) {
            return new TypeUsageImpl("String", offset);
        }
        if (LOWERCASED_NUMBER.equals(type)) {
            return new TypeUsageImpl("Number", offset);
        }
        if (LOWERCASED_BOOLEAN.equals(type)) {
            return new TypeUsageImpl("Boolean", offset);
        }
        return new TypeUsageImpl(type, offset);
    }

    private static ParameterElement createParameterElement(JsDocElementType elementType, String elementText, int descStartOffset) {
        int typeOffset = -1;
        int nameOffset = -1;
        String types = "";
        String desc = "";
        StringBuilder name = new StringBuilder();
        int process = 0;
        String[] parts = elementText.split("[\\s]+");
        if (parts.length > process) {
            if (parts[0].startsWith("{")) {
                typeOffset = descStartOffset + 1;
                int rparIndex = parts[0].indexOf("}");
                if (rparIndex == -1) {
                    int actualProcessed = process;
                    StringBuilder typesSB = new StringBuilder(parts[0].substring(1));
                    while (actualProcessed < parts.length - 1 && actualProcessed < 10) {
                        if ((rparIndex = parts[++actualProcessed].indexOf("}")) != -1) {
                            typesSB.append(parts[actualProcessed].substring(0, rparIndex));
                            process = actualProcessed;
                            types = typesSB.toString();
                            break;
                        }
                        typesSB.append(parts[actualProcessed]);
                    }
                    if (types.isEmpty()) {
                        types = parts[0].trim();
                    }
                } else {
                    types = parts[0].substring(1, rparIndex);
                }
                ++process;
            }
            if (parts.length > process && elementType.getCategory() == JsDocElement.Category.NAMED_PARAMETER) {
                nameOffset = descStartOffset + elementText.indexOf(parts[process], types.length());
                name.append(parts[process].trim());
                ++process;
                if (name.toString().contains("\"") || name.toString().contains("'")) {
                    process = JsDocElementUtils.buildNameForString(name, process, parts);
                }
            }
            StringBuilder sb = new StringBuilder();
            while (process < parts.length) {
                sb.append(parts[process]).append(" ");
                ++process;
            }
            desc = sb.toString().trim();
        }
        if (elementType.getCategory() == JsDocElement.Category.NAMED_PARAMETER) {
            return NamedParameterElement.createWithNameDiagnostics(elementType, new IdentifierImpl(name.toString(), nameOffset), JsDocElementUtils.parseTypes(types, typeOffset), desc);
        }
        return UnnamedParameterElement.create(elementType, JsDocElementUtils.parseTypes(types, typeOffset), desc);
    }

    private static int buildNameForString(StringBuilder name, int currentOffset, String[] parts) {
        String nameString = name.toString();
        if (nameString.indexOf("\"") != -1 && nameString.indexOf("\"") == nameString.lastIndexOf("\"") || nameString.indexOf("'") != -1 && nameString.indexOf("'") == nameString.lastIndexOf("'")) {
            boolean endOfString = false;
            while (currentOffset < parts.length && !endOfString) {
                name.append(" ").append(parts[currentOffset]);
                if (parts[currentOffset].contains("\"") || parts[currentOffset].contains("'")) {
                    endOfString = true;
                }
                ++currentOffset;
            }
        }
        return currentOffset;
    }

}

