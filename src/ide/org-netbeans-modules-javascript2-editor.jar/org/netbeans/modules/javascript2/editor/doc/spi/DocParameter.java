/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

import java.util.List;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public interface DocParameter {
    public Identifier getParamName();

    public String getDefaultValue();

    public boolean isOptional();

    public String getParamDescription();

    public List<Type> getParamTypes();
}

