/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc;

import java.util.LinkedList;
import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.extdoc.completion.DescriptionTag;
import org.netbeans.modules.javascript2.editor.extdoc.completion.IdentDescribedTag;
import org.netbeans.modules.javascript2.editor.extdoc.completion.IdentSimpleTag;
import org.netbeans.modules.javascript2.editor.extdoc.completion.TypeDescribedTag;
import org.netbeans.modules.javascript2.editor.extdoc.completion.TypeNamedTag;
import org.netbeans.modules.javascript2.editor.extdoc.completion.TypeSimpleTag;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;

public class ExtDocAnnotationCompletionTagProvider
extends AnnotationCompletionTagProvider {
    List<AnnotationCompletionTag> annotations = null;

    public ExtDocAnnotationCompletionTagProvider(String name) {
        super(name);
    }

    @Override
    public synchronized List<AnnotationCompletionTag> getAnnotations() {
        if (this.annotations == null) {
            this.initAnnotations();
        }
        return this.annotations;
    }

    private void initAnnotations() {
        this.annotations = new LinkedList<AnnotationCompletionTag>();
        block8 : for (ExtDocElementType type : ExtDocElementType.values()) {
            if (type == ExtDocElementType.UNKNOWN) continue;
            switch (type.getCategory()) {
                case DESCRIPTION: {
                    this.annotations.add(new DescriptionTag(type.toString()));
                    continue block8;
                }
                case IDENT_SIMPLE: {
                    this.annotations.add(new IdentSimpleTag(type.toString()));
                    continue block8;
                }
                case IDENT_DESCRIBED: {
                    this.annotations.add(new IdentDescribedTag(type.toString()));
                    continue block8;
                }
                case TYPE_NAMED: {
                    this.annotations.add(new TypeNamedTag(type.toString()));
                    continue block8;
                }
                case TYPE_SIMPLE: {
                    this.annotations.add(new TypeSimpleTag(type.toString()));
                    continue block8;
                }
                case TYPE_DESCRIBED: {
                    this.annotations.add(new TypeDescribedTag(type.toString()));
                    continue block8;
                }
                default: {
                    this.annotations.add(new AnnotationCompletionTag(type.toString(), type.toString()));
                }
            }
        }
    }

}

