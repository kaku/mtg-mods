/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.modules.csl.api.CodeCompletionContext
 *  org.netbeans.modules.csl.api.CodeCompletionHandler
 *  org.netbeans.modules.csl.api.CodeCompletionHandler$QueryType
 *  org.netbeans.modules.csl.api.CodeCompletionHandler2
 *  org.netbeans.modules.csl.api.CodeCompletionResult
 *  org.netbeans.modules.csl.api.CompletionProposal
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.ParameterInfo
 *  org.netbeans.modules.csl.spi.DefaultCompletionResult
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport
 *  org.netbeans.modules.parsing.spi.indexing.support.QuerySupport$Kind
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.csl.api.CodeCompletionContext;
import org.netbeans.modules.csl.api.CodeCompletionHandler;
import org.netbeans.modules.csl.api.CodeCompletionHandler2;
import org.netbeans.modules.csl.api.CodeCompletionResult;
import org.netbeans.modules.csl.api.CompletionProposal;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.ParameterInfo;
import org.netbeans.modules.csl.spi.DefaultCompletionResult;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.CompletionContextFinder;
import org.netbeans.modules.javascript2.editor.EditorExtender;
import org.netbeans.modules.javascript2.editor.JsCompletionItem;
import org.netbeans.modules.javascript2.editor.JsKeyWords;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationCodeCompletion;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationElement;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.ModelExtender;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.TypeUsageImpl;
import org.netbeans.modules.javascript2.editor.options.OptionsUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.javascript2.editor.spi.CompletionContext;
import org.netbeans.modules.javascript2.editor.spi.CompletionProvider;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

class JsCodeCompletion
implements CodeCompletionHandler2 {
    private static final Logger LOGGER = Logger.getLogger(JsCodeCompletion.class.getName());
    private static final List<String> WINDOW_EXPRESSION_CHAIN = Arrays.asList("window", "@pro");
    private boolean caseSensitive;
    private static final String CHARS_NO_AUTO_COMPLETE = ";,/+-\\:={}[]()";
    private int checkRecursion;

    JsCodeCompletion() {
    }

    public CodeCompletionResult complete(CodeCompletionContext ccContext) {
        long start = System.currentTimeMillis();
        BaseDocument doc = (BaseDocument)ccContext.getParserResult().getSnapshot().getSource().getDocument(false);
        if (doc == null) {
            return CodeCompletionResult.NONE;
        }
        this.caseSensitive = ccContext.isCaseSensitive();
        ParserResult info = ccContext.getParserResult();
        int caretOffset = ccContext.getParserResult().getSnapshot().getEmbeddedOffset(ccContext.getCaretOffset());
        FileObject fileObject = ccContext.getParserResult().getSnapshot().getSource().getFileObject();
        JsParserResult jsParserResult = (JsParserResult)info;
        CompletionContext context = CompletionContextFinder.findCompletionContext(info, caretOffset);
        LOGGER.log(Level.FINE, String.format("CC context: %s", context.toString()));
        JsCompletionItem.CompletionRequest request = new JsCompletionItem.CompletionRequest();
        String pref = ccContext.getPrefix();
        request.anchor = pref == null ? caretOffset : caretOffset - pref.length();
        request.result = jsParserResult;
        request.info = info;
        request.prefix = pref;
        jsParserResult.getModel().resolve();
        ArrayList<CompletionProposal> resultList = new ArrayList<CompletionProposal>();
        HashMap<String, List<JsElement>> added = new HashMap<String, List<JsElement>>();
        if (ccContext.getQueryType() == CodeCompletionHandler.QueryType.ALL_COMPLETION) {
            switch (context) {
                case GLOBAL: {
                    this.addGlobalObjectsFromIndex(request, added);
                    break;
                }
                case EXPRESSION: {
                    this.completeKeywords(request, resultList);
                    this.completeExpression(request, added);
                    break;
                }
                case OBJECT_PROPERTY: {
                    this.completeObjectProperty(request, added);
                    break;
                }
                case OBJECT_MEMBERS: {
                    this.completeObjectMember(request, added);
                    break;
                }
            }
            if (context == CompletionContext.EXPRESSION || context == CompletionContext.OBJECT_MEMBERS || context == CompletionContext.OBJECT_PROPERTY) {
                Collection<? extends IndexResult> indexResults = JsIndex.get(fileObject).query("bn", request.prefix, QuerySupport.Kind.PREFIX, JsIndex.TERMS_BASIC_INFO);
                for (IndexResult indexResult : indexResults) {
                    IndexedElement indexElement = IndexedElement.create(indexResult);
                    this.addPropertyToMap(request, added, indexElement);
                }
            }
        } else {
            switch (context) {
                case STRING: {
                    if (request.prefix.startsWith(".")) {
                        request.prefix = request.prefix.substring(1);
                        ++request.anchor;
                    }
                    List<String> expression = this.resolveExpressionChainFromString(request);
                    Map<String, List<JsElement>> toAdd = this.getCompletionFromExpressionChain(request, expression);
                    JsCompletionItem.Factory.create(toAdd, request, resultList);
                    break;
                }
                case GLOBAL: {
                    HashMap<String, List<JsElement>> addedProperties = new HashMap<String, List<JsElement>>();
                    addedProperties.putAll(this.getDomCompletionResults(request));
                    for (JsObject libGlobal : ModelExtender.getDefault().getExtendingGlobalObjects()) {
                        for (JsObject object : libGlobal.getProperties().values()) {
                            this.addPropertyToMap(request, addedProperties, object);
                        }
                    }
                    for (JsObject object : request.result.getModel().getVariables(caretOffset)) {
                        if (object instanceof JsFunction && ((JsFunction)object).isAnonymous()) continue;
                        this.addPropertyToMap(request, addedProperties, object);
                    }
                    this.completeKeywords(request, resultList);
                    this.addGlobalObjectsFromIndex(request, addedProperties);
                    this.completeInWith(request, addedProperties);
                    JsCompletionItem.Factory.create(addedProperties, request, resultList);
                    break;
                }
                case EXPRESSION: {
                    this.completeKeywords(request, resultList);
                    this.completeExpression(request, added);
                    this.completeObjectProperty(request, added);
                    this.completeInWith(request, added);
                    break;
                }
                case OBJECT_PROPERTY: {
                    this.completeObjectProperty(request, added);
                    break;
                }
                case OBJECT_MEMBERS: {
                    this.completeObjectMember(request, added);
                    break;
                }
                case DOCUMENTATION: {
                    JsDocumentationCodeCompletion.complete(request, resultList);
                    break;
                }
            }
        }
        JsCompletionItem.Factory.create(added, request, resultList);
        long end = System.currentTimeMillis();
        LOGGER.log(Level.FINE, "Counting JS CC took {0}ms ", end - start);
        for (CompletionProvider interceptor : EditorExtender.getDefault().getCompletionProviders()) {
            resultList.addAll(interceptor.complete(ccContext, context, pref));
        }
        if (!resultList.isEmpty()) {
            return new DefaultCompletionResult(resultList, false);
        }
        return CodeCompletionResult.NONE;
    }

    private void addGlobalObjectsFromIndex(JsCompletionItem.CompletionRequest request, HashMap<String, List<JsElement>> addedProperties) {
        FileObject fileObject = request.result.getSnapshot().getSource().getFileObject();
        if (fileObject != null) {
            JsIndex jsIndex = JsIndex.get(fileObject);
            Collection<IndexedElement> fromIndex = jsIndex.getGlobalVar(request.prefix);
            for (IndexedElement indexElement2 : fromIndex) {
                this.addPropertyToMap(request, addedProperties, indexElement2);
            }
            fromIndex = jsIndex.getPropertiesWithPrefix("window", request.prefix);
            for (IndexedElement indexElement2 : fromIndex) {
                this.addPropertyToMap(request, addedProperties, indexElement2);
            }
        }
    }

    public String document(ParserResult info, ElementHandle element) {
        Documentation doc = this.documentElement(info, element, new Callable<Boolean>(){

            @Override
            public Boolean call() throws Exception {
                return false;
            }
        });
        if (doc != null) {
            return doc.getContent();
        }
        return null;
    }

    public Documentation documentElement(ParserResult info, ElementHandle element, Callable<Boolean> cancel) {
        JsObject jsObject;
        if (element == null) {
            return null;
        }
        if (element instanceof IndexedElement) {
            final Documentation[] result = new Documentation[1];
            final IndexedElement indexedElement = (IndexedElement)element;
            FileObject nextFo = indexedElement.getFileObject();
            if (nextFo != null) {
                try {
                    ParserManager.parse(Collections.singleton(Source.create((FileObject)nextFo)), (UserTask)new UserTask(){

                        public void run(ResultIterator resultIterator) throws Exception {
                            Parser.Result parserResult = resultIterator.getParserResult();
                            if (parserResult instanceof JsParserResult) {
                                JsParserResult jsInfo = (JsParserResult)parserResult;
                                String fqn = indexedElement.getFQN();
                                JsObject jsObjectGlobal = jsInfo.getModel().getGlobalObject();
                                JsObject property = ModelUtils.findJsObjectByName(jsObjectGlobal, fqn);
                                if (property != null) {
                                    Documentation doc;
                                    result[0] = doc = property.getDocumentation();
                                }
                            } else {
                                LOGGER.log(Level.INFO, "Not instance of JsParserResult: {0}", (Object)parserResult);
                            }
                        }
                    });
                }
                catch (ParseException ex) {
                    LOGGER.log(Level.WARNING, null, (Throwable)ex);
                }
            }
            if (result[0] != null) {
                return result[0];
            }
        } else if (element instanceof JsObject && (jsObject = (JsObject)element).getDocumentation() != null) {
            return jsObject.getDocumentation();
        }
        for (CompletionProvider interceptor : EditorExtender.getDefault().getCompletionProviders()) {
            String doc = interceptor.getHelpDocumentation(info, element);
            if (doc == null || doc.isEmpty()) continue;
            return Documentation.create((String)doc);
        }
        if (element instanceof JsDocumentationElement) {
            String documentation = ((JsDocumentationElement)element).getDocumentation();
            return documentation != null ? Documentation.create((String)documentation) : null;
        }
        if (OffsetRange.NONE.equals((Object)element.getOffsetRange(info))) {
            return Documentation.create((String)NbBundle.getMessage(JsCodeCompletion.class, (String)"MSG_ItemFromUsageDoc"));
        }
        return Documentation.create((String)NbBundle.getMessage(JsCodeCompletion.class, (String)"MSG_DocNotAvailable"));
    }

    public ElementHandle resolveLink(String link, ElementHandle originalHandle) {
        return null;
    }

    public String getPrefix(ParserResult info, int caretOffset, boolean upToOffset) {
        Token token;
        String prefix = "";
        BaseDocument doc = (BaseDocument)info.getSnapshot().getSource().getDocument(false);
        if (doc == null) {
            return null;
        }
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(info.getSnapshot(), caretOffset);
        if (ts == null) {
            return null;
        }
        int offset = info.getSnapshot().getEmbeddedOffset(caretOffset);
        ts.move(offset);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return null;
        }
        if (ts.offset() == offset) {
            ts.movePrevious();
        }
        if ((token = ts.token()) != null && token.id() != JsTokenId.EOL) {
            JsTokenId id = (JsTokenId)token.id();
            if (id == JsTokenId.STRING_END && ts.movePrevious()) {
                if (ts.token().id() == JsTokenId.STRING_BEGIN) {
                    return "";
                }
                ts.moveNext();
            }
            if (id == JsTokenId.STRING) {
                prefix = token.text().toString();
                if (upToOffset) {
                    int prefixIndex = JsCodeCompletion.getPrefixIndexFromSequence(prefix.substring(0, offset - ts.offset()));
                    prefix = prefix.substring(prefixIndex, offset - ts.offset());
                }
            }
            if (id == JsTokenId.IDENTIFIER || id.isKeyword()) {
                prefix = token.text().toString();
                if (upToOffset) {
                    prefix = prefix.substring(0, offset - ts.offset());
                }
            }
            if (id == JsTokenId.DOC_COMMENT) {
                TokenSequence<? extends JsDocumentationTokenId> docTokenSeq = LexUtilities.getJsDocumentationTokenSequence(info.getSnapshot(), offset);
                if (docTokenSeq == null) {
                    return null;
                }
                docTokenSeq.move(offset);
                if (!docTokenSeq.moveNext() && !docTokenSeq.movePrevious()) {
                    return null;
                }
                if (docTokenSeq.token().id() == JsDocumentationTokenId.KEYWORD) {
                    prefix = docTokenSeq.token().text().toString();
                    if (upToOffset) {
                        prefix = prefix.substring(0, offset - docTokenSeq.offset());
                    }
                } else {
                    docTokenSeq.movePrevious();
                    prefix = docTokenSeq.token().text().toString();
                }
            }
            if (id.isError()) {
                prefix = token.text().toString();
                prefix = prefix.substring(0, offset - ts.offset());
            }
        }
        LOGGER.log(Level.FINE, String.format("Prefix for cc: %s", prefix));
        return prefix;
    }

    public CodeCompletionHandler.QueryType getAutoQuery(JTextComponent component, String typedText) {
        if (typedText.length() == 0) {
            return CodeCompletionHandler.QueryType.NONE;
        }
        int offset = component.getCaretPosition();
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(component.getDocument(), offset);
        if (ts != null) {
            int diff = ts.move(offset);
            TokenId currentTokenId = null;
            if (diff == 0 && ts.movePrevious() || ts.moveNext()) {
                currentTokenId = ts.token().id();
            }
            char lastChar = typedText.charAt(typedText.length() - 1);
            if (currentTokenId == JsTokenId.BLOCK_COMMENT || currentTokenId == JsTokenId.DOC_COMMENT || currentTokenId == JsTokenId.LINE_COMMENT) {
                if (lastChar == '@') {
                    return CodeCompletionHandler.QueryType.COMPLETION;
                }
            } else {
                if (currentTokenId == JsTokenId.STRING && lastChar == '/') {
                    return CodeCompletionHandler.QueryType.COMPLETION;
                }
                switch (lastChar) {
                    case '.': {
                        if (!OptionsUtils.forLanguage(JsTokenId.javascriptLanguage()).autoCompletionAfterDot()) break;
                        return CodeCompletionHandler.QueryType.COMPLETION;
                    }
                    default: {
                        if (OptionsUtils.forLanguage(JsTokenId.javascriptLanguage()).autoCompletionFull() && !Character.isWhitespace(lastChar) && ";,/+-\\:={}[]()".indexOf(lastChar) == -1) {
                            return CodeCompletionHandler.QueryType.COMPLETION;
                        }
                        return CodeCompletionHandler.QueryType.NONE;
                    }
                }
            }
        }
        return CodeCompletionHandler.QueryType.NONE;
    }

    public String resolveTemplateVariable(String variable, ParserResult info, int caretOffset, String name, Map parameters) {
        return null;
    }

    public Set<String> getApplicableTemplates(Document doc, int selectionBegin, int selectionEnd) {
        return null;
    }

    public ParameterInfo parameters(ParserResult info, int caretOffset, CompletionProposal proposal) {
        return ParameterInfo.NONE;
    }

    private void completeExpression(JsCompletionItem.CompletionRequest request, HashMap<String, List<JsElement>> addedItems) {
        FileObject fo = request.info.getSnapshot().getSource().getFileObject();
        addedItems.putAll(this.getDomCompletionResults(request));
        JsIndex index = JsIndex.get(fo);
        Collection<IndexedElement> fromIndex = index.getGlobalVar(request.prefix);
        for (IndexedElement indexedElement : fromIndex) {
            this.addPropertyToMap(request, addedItems, indexedElement);
        }
        for (JsObject libGlobal : ModelExtender.getDefault().getExtendingGlobalObjects()) {
            for (JsObject object : libGlobal.getProperties().values()) {
                this.addPropertyToMap(request, addedItems, object);
            }
        }
        for (JsObject object : request.result.getModel().getVariables(request.anchor)) {
            if (object instanceof JsFunction && ((JsFunction)object).isAnonymous()) continue;
            this.addPropertyToMap(request, addedItems, object);
        }
    }

    private void completeObjectProperty(JsCompletionItem.CompletionRequest request, Map<String, List<JsElement>> addedItems) {
        List<String> expChain = ModelUtils.resolveExpressionChain(request.result.getSnapshot(), request.anchor, false);
        if (!expChain.isEmpty()) {
            Map<String, List<JsElement>> toAdd = this.getCompletionFromExpressionChain(request, expChain);
            FileObject fo = request.result.getSnapshot().getSource().getFileObject();
            if (fo != null) {
                long start = System.currentTimeMillis();
                Collection<IndexedElement> fromUsages = JsIndex.get(request.result.getSnapshot().getSource().getFileObject()).getUsagesFromExpression(expChain);
                for (IndexedElement indexedElement : fromUsages) {
                    if (fo.equals((Object)indexedElement.getFileObject()) && indexedElement.getName().equals(request.prefix)) continue;
                    this.addPropertyToMap(request, addedItems, indexedElement);
                }
                long end = System.currentTimeMillis();
                LOGGER.log(Level.FINE, String.format("Counting cc based on usages took: %dms", end - start));
            }
            addedItems.putAll(toAdd);
        }
    }

    private Map<String, List<JsElement>> getCompletionFromExpressionChain(JsCompletionItem.CompletionRequest request, List<String> expChain) {
        FileObject fo = request.info.getSnapshot().getSource().getFileObject();
        JsIndex jsIndex = JsIndex.get(fo);
        Collection resolveTypeFromExpression = new ArrayList<TypeUsage>();
        resolveTypeFromExpression.addAll(ModelUtils.resolveTypeFromExpression(request.result.getModel(), jsIndex, expChain, request.anchor));
        resolveTypeFromExpression = ModelUtils.resolveTypes(resolveTypeFromExpression, request.result, true);
        ArrayList<String> windowProp = new ArrayList<String>();
        for (TypeUsage typeUsage : resolveTypeFromExpression) {
            if (!typeUsage.isResolved() || typeUsage.getType().startsWith("window")) continue;
            windowProp.add("window." + typeUsage.getType());
        }
        ArrayList<String> prototypeChain = new ArrayList<String>();
        for (TypeUsage typeUsage2 : resolveTypeFromExpression) {
            prototypeChain.addAll(ModelUtils.findPrototypeChain(typeUsage2.getType(), jsIndex));
        }
        for (String string2 : windowProp) {
            resolveTypeFromExpression.add(new TypeUsageImpl(string2));
        }
        for (String string2 : prototypeChain) {
            resolveTypeFromExpression.add(new TypeUsageImpl(string2));
        }
        HashMap<String, List<JsElement>> addedProperties = new HashMap<String, List<JsElement>>();
        boolean isFunction = false;
        ArrayList<JsObject> lastResolvedObjects = new ArrayList<JsObject>();
        for (TypeUsage typeUsage3 : resolveTypeFromExpression) {
            this.checkRecursion = 0;
            boolean addFunctionProp = this.processTypeInModel(request, request.result.getModel(), typeUsage3, lastResolvedObjects, expChain.get(1).equals("@pro"), jsIndex, addedProperties);
            boolean bl = isFunction = isFunction || addFunctionProp;
            if (!typeUsage3.isResolved()) continue;
            this.addObjectPropertiesFromIndex(typeUsage3.getType(), jsIndex, request, addedProperties);
        }
        boolean isPublic = lastResolvedObjects.isEmpty();
        for (JsObject resolved : lastResolvedObjects) {
            if (!isFunction && resolved.getJSKind().isFunction()) {
                isFunction = true;
            }
            this.addObjectPropertiesToCC(resolved, request, addedProperties);
            if (!resolved.isDeclared()) {
                this.addObjectPropertiesFromIndex(resolved.getFullyQualifiedName(), jsIndex, request, addedProperties);
                isPublic = true;
                continue;
            }
            if (resolved.getModifiers().contains((Object)Modifier.PRIVATE)) continue;
            isPublic = true;
        }
        if (isFunction) {
            this.addObjectPropertiesFromIndex("Function", jsIndex, request, addedProperties);
        }
        this.addObjectPropertiesFromIndex("Object", jsIndex, request, addedProperties);
        if (isPublic) {
            StringBuilder fqn = new StringBuilder();
            for (int i = expChain.size() - 1; i > -1; --i) {
                fqn.append(expChain.get(--i));
                fqn.append('.');
            }
            if (fqn.length() > 0) {
                Collection<IndexedElement> indexResults = jsIndex.getPropertiesWithPrefix(fqn.toString().substring(0, fqn.length() - 1), request.prefix);
                for (IndexedElement indexedElement : indexResults) {
                    if (indexedElement.isAnonymous() || !indexedElement.getModifiers().contains((Object)Modifier.PUBLIC)) continue;
                    this.addPropertyToMap(request, addedProperties, indexedElement);
                }
            }
        }
        return addedProperties;
    }

    private List<String> resolveExpressionChainFromString(JsCompletionItem.CompletionRequest request) {
        TokenHierarchy th = request.info.getSnapshot().getTokenHierarchy();
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(th, request.anchor);
        if (ts == null) {
            return Collections.emptyList();
        }
        int offset = request.info.getSnapshot().getEmbeddedOffset(request.anchor);
        ts.move(offset);
        String text = null;
        if (ts.moveNext()) {
            if (ts.token().id() == JsTokenId.STRING_END) {
                if (ts.movePrevious() && ts.token().id() == JsTokenId.STRING) {
                    text = ts.token().text().toString();
                }
            } else if (ts.token().id() == JsTokenId.STRING) {
                text = ts.token().text().toString().substring(0, offset - ts.offset());
            }
        }
        if (text != null && !text.isEmpty()) {
            int index = text.length() - 1;
            ArrayList<String> exp = new ArrayList<String>();
            int parenBalancer = 0;
            boolean methodCall = false;
            char ch = text.charAt(index);
            String part = "";
            while (index > -1 && ch != ' ' && ch != '\n' && ch != ';' && ch != '}' && ch != '{' && ch != '(' && ch != '=' && ch != '+' && ch != '[') {
                if (ch == '.') {
                    if (!part.isEmpty()) {
                        exp.add(part);
                        part = "";
                        if (methodCall) {
                            exp.add("@mtd");
                            methodCall = false;
                        } else {
                            exp.add("@pro");
                        }
                    }
                } else if (ch == ')') {
                    ++parenBalancer;
                    methodCall = true;
                    while (parenBalancer > 0 && --index > -1) {
                        ch = text.charAt(index);
                        if (ch == ')') {
                            ++parenBalancer;
                            continue;
                        }
                        if (ch != '(') continue;
                        --parenBalancer;
                    }
                } else {
                    part = "" + ch + part;
                }
                if (--index <= -1) continue;
                ch = text.charAt(index);
            }
            if (!part.isEmpty()) {
                exp.add(part);
                if (methodCall) {
                    exp.add("@mtd");
                } else {
                    exp.add("@pro");
                }
            }
            return exp;
        }
        return Collections.emptyList();
    }

    private List<String> resolveExpressionChain(JsCompletionItem.CompletionRequest request, int offset, boolean lookBefore) {
        TokenHierarchy th = request.info.getSnapshot().getTokenHierarchy();
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsTokenSequence(th, offset);
        if (ts == null) {
            return Collections.emptyList();
        }
        ts.move(offset);
        if (ts.movePrevious() && (ts.moveNext() || ts.offset() + ts.token().length() == request.result.getSnapshot().getText().length())) {
            if (!lookBefore && ts.token().id() != JsTokenId.OPERATOR_DOT) {
                ts.movePrevious();
            }
            Token<? extends JsTokenId> token = lookBefore ? LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.EOL})) : ts.token();
            int parenBalancer = 0;
            int partType = 0;
            boolean wasLastDot = lookBefore;
            int offsetFirstRightParen = -1;
            ArrayList<String> exp = new ArrayList<String>();
            while (token.id() != JsTokenId.WHITESPACE && token.id() != JsTokenId.OPERATOR_SEMICOLON && token.id() != JsTokenId.BRACKET_RIGHT_CURLY && token.id() != JsTokenId.BRACKET_LEFT_CURLY && token.id() != JsTokenId.BRACKET_LEFT_PAREN && token.id() != JsTokenId.BLOCK_COMMENT && token.id() != JsTokenId.LINE_COMMENT && token.id() != JsTokenId.OPERATOR_ASSIGNMENT && token.id() != JsTokenId.OPERATOR_PLUS) {
                if (token.id() != JsTokenId.EOL) {
                    if (token.id() != JsTokenId.OPERATOR_DOT) {
                        if (token.id() == JsTokenId.BRACKET_RIGHT_PAREN) {
                            ++parenBalancer;
                            partType = 1;
                            if (offsetFirstRightParen == -1) {
                                offsetFirstRightParen = ts.offset();
                            }
                            while (parenBalancer > 0 && ts.movePrevious()) {
                                token = ts.token();
                                if (token.id() == JsTokenId.BRACKET_RIGHT_PAREN) {
                                    ++parenBalancer;
                                    continue;
                                }
                                if (token.id() != JsTokenId.BRACKET_LEFT_PAREN) continue;
                                --parenBalancer;
                            }
                        } else if (token.id() == JsTokenId.BRACKET_RIGHT_BRACKET) {
                            ++parenBalancer;
                            partType = 2;
                            while (parenBalancer > 0 && ts.movePrevious()) {
                                token = ts.token();
                                if (token.id() == JsTokenId.BRACKET_RIGHT_BRACKET) {
                                    ++parenBalancer;
                                    continue;
                                }
                                if (token.id() != JsTokenId.BRACKET_LEFT_BRACKET) continue;
                                --parenBalancer;
                            }
                        } else {
                            if (parenBalancer == 0 && "operator".equals(((JsTokenId)token.id()).primaryCategory())) {
                                return exp;
                            }
                            exp.add(token.text().toString());
                            switch (partType) {
                                case 0: {
                                    exp.add("@pro");
                                    break;
                                }
                                case 1: {
                                    exp.add("@mtd");
                                    offsetFirstRightParen = -1;
                                    break;
                                }
                                case 2: {
                                    exp.add("@arr");
                                    break;
                                }
                            }
                            partType = 0;
                            wasLastDot = false;
                        }
                    } else {
                        wasLastDot = true;
                    }
                } else if (!wasLastDot && ts.movePrevious() && (token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.LINE_COMMENT}))).id() != JsTokenId.OPERATOR_DOT) break;
                if (!ts.movePrevious()) break;
                token = ts.token();
            }
            if (token.id() == JsTokenId.WHITESPACE) {
                if (ts.movePrevious()) {
                    token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE, JsTokenId.BLOCK_COMMENT, JsTokenId.EOL}));
                    if (token.id() == JsTokenId.KEYWORD_NEW && !exp.isEmpty()) {
                        exp.remove(exp.size() - 1);
                        exp.add("@pro");
                    } else if (!lookBefore && offsetFirstRightParen > -1) {
                        exp.addAll(this.resolveExpressionChain(request, offsetFirstRightParen - 1, true));
                    }
                }
            } else if (exp.isEmpty() && !lookBefore && offsetFirstRightParen > -1) {
                exp.addAll(this.resolveExpressionChain(request, offsetFirstRightParen - 1, true));
            }
            return exp;
        }
        return Collections.emptyList();
    }

    private void completeObjectMember(JsCompletionItem.CompletionRequest request, Map<String, List<JsElement>> addedItems) {
        JsParserResult result = (JsParserResult)request.info;
        JsObject jsObject = (JsObject)((Object)ModelUtils.getDeclarationScope(result.getModel(), request.anchor));
        if (jsObject.getJSKind() == JsElement.Kind.METHOD) {
            jsObject = jsObject.getParent();
        }
        this.completeObjectMembers(jsObject, request, addedItems);
        if ("prototype".equals(jsObject.getName())) {
            this.completeObjectMembers(jsObject.getParent(), request, addedItems);
        }
    }

    private void completeObjectMembers(JsObject jsObject, JsCompletionItem.CompletionRequest request, Map<String, List<JsElement>> properties) {
        if (jsObject.getJSKind() == JsElement.Kind.OBJECT || jsObject.getJSKind() == JsElement.Kind.CONSTRUCTOR || jsObject.getJSKind() == JsElement.Kind.OBJECT_LITERAL) {
            for (JsObject property : jsObject.getProperties().values()) {
                if (property.getModifiers().contains((Object)Modifier.PRIVATE) || property.isAnonymous()) continue;
                this.addPropertyToMap(request, properties, property);
            }
        }
        String fqn = jsObject.getFullyQualifiedName();
        FileObject fo = request.info.getSnapshot().getSource().getFileObject();
        Collection<IndexedElement> indexedProperties = JsIndex.get(fo).getProperties(fqn);
        for (IndexedElement indexedElement : indexedProperties) {
            this.addPropertyToMap(request, properties, indexedElement);
        }
    }

    private void completeInWith(JsCompletionItem.CompletionRequest request, HashMap<String, List<JsElement>> addedItems) {
        int offset = request.anchor;
        Collection<? extends TypeUsage> typesFromWith = ModelUtils.getTypeFromWith(request.result.getModel(), offset);
        if (!typesFromWith.isEmpty()) {
            FileObject fo = request.info.getSnapshot().getSource().getFileObject();
            JsIndex jsIndex = JsIndex.get(fo);
            Collection<TypeUsage> resolveTypes = ModelUtils.resolveTypes(typesFromWith, request.result, true);
            for (TypeUsage type : resolveTypes) {
                JsObject localObject = ModelUtils.findJsObjectByName(request.result.getModel(), type.getType());
                if (localObject != null) {
                    this.addObjectPropertiesToCC(localObject, request, addedItems);
                }
                this.addObjectPropertiesFromIndex(type.getType(), jsIndex, request, addedItems);
            }
        }
    }

    private void completeKeywords(JsCompletionItem.CompletionRequest request, List<CompletionProposal> resultList) {
        for (String keyword : JsKeyWords.KEYWORDS.keySet()) {
            if (!this.startsWith(keyword, request.prefix)) continue;
            resultList.add(new JsCompletionItem.KeywordItem(keyword, request));
        }
    }

    private boolean startsWith(String theString, String prefix) {
        if (prefix == null || prefix.length() == 0) {
            return true;
        }
        return this.caseSensitive ? theString.startsWith(prefix) : theString.toLowerCase().startsWith(prefix.toLowerCase());
    }

    private boolean processTypeInModel(JsCompletionItem.CompletionRequest request, Model model, TypeUsage type, List<JsObject> lastResolvedObjects, boolean prop, JsIndex index, Map<String, List<JsElement>> addedProperties) {
        if (++this.checkRecursion > 10) {
            return false;
        }
        boolean isFunction = false;
        JsObject jsObject = ModelUtils.findJsObjectByName(model, type.getType());
        if (jsObject != null) {
            lastResolvedObjects.add(jsObject);
        }
        for (JsObject libGlobal : ModelExtender.getDefault().getExtendingGlobalObjects()) {
            JsObject found = ModelUtils.findJsObjectByName(libGlobal, type.getType());
            if (found == null || found == libGlobal) continue;
            jsObject = found;
            lastResolvedObjects.add(jsObject);
            break;
        }
        if (jsObject == null || !jsObject.isDeclared()) {
            boolean isObject = type.getType().equals("Object");
            if (prop && !isObject) {
                for (IndexResult indexResult : index.findByFqn(type.getType(), "flag")) {
                    JsElement.Kind kind = IndexedElement.Flag.getJsKind(Integer.parseInt(indexResult.getValue("flag")));
                    if (!kind.isFunction()) continue;
                    isFunction = true;
                }
            }
            if (!isObject) {
                this.addObjectPropertiesFromIndex(type.getType(), index, request, addedProperties);
            }
        } else if (jsObject.getDeclarationName() != null) {
            Collection<? extends TypeUsage> assignments = jsObject.getAssignmentForOffset(jsObject.getDeclarationName().getOffsetRange().getEnd());
            for (TypeUsage assignment : assignments) {
                boolean isFun = this.processTypeInModel(request, model, assignment, lastResolvedObjects, prop, index, addedProperties);
                isFunction = isFunction ? true : isFun;
            }
        }
        return isFunction;
    }

    private void addObjectPropertiesToCC(JsObject jsObject, JsCompletionItem.CompletionRequest request, Map<String, List<JsElement>> addedProperties) {
        JsObject prototype = jsObject.getProperty("prototype");
        if (prototype != null) {
            this.addObjectPropertiesToCC(prototype, request, addedProperties);
        }
        for (JsObject property : jsObject.getProperties().values()) {
            if (property instanceof JsFunction && ((JsFunction)property).isAnonymous() || ModelUtils.getDisplayName(property.getName()).isEmpty() || property.getModifiers().contains((Object)Modifier.PRIVATE) || property.getJSKind().isPropertyGetterSetter()) continue;
            this.addPropertyToMap(request, addedProperties, property);
        }
    }

    private void addObjectPropertiesFromIndex(String fqn, JsIndex jsIndex, JsCompletionItem.CompletionRequest request, Map<String, List<JsElement>> addedProperties) {
        Collection<IndexedElement> properties = jsIndex.getProperties(fqn);
        String prototypeFQN = null;
        for (IndexedElement indexedElement2 : properties) {
            this.addPropertyToMap(request, addedProperties, indexedElement2);
            if (!"prototype".equals(indexedElement2.getName())) continue;
            prototypeFQN = indexedElement2.getFQN();
        }
        if (prototypeFQN != null) {
            properties = jsIndex.getProperties(prototypeFQN);
            for (IndexedElement indexedElement2 : properties) {
                this.addPropertyToMap(request, addedProperties, indexedElement2);
            }
        }
    }

    private void addPropertyToMap(JsCompletionItem.CompletionRequest request, Map<String, List<JsElement>> addedProperties, JsElement property) {
        String name = property.getName();
        if (this.startsWith(name, request.prefix) && !ModelUtils.getDisplayName(property.getName()).isEmpty() && (!name.equals(request.prefix) || property.isDeclared() || request.anchor != property.getOffset())) {
            List<JsElement> elements = addedProperties.get(name);
            if (elements == null || elements.isEmpty()) {
                ArrayList<JsElement> properties = new ArrayList<JsElement>(1);
                properties.add(property);
                addedProperties.put(name, properties);
            } else if (!"prototype".equals(name) && property.isDeclared()) {
                boolean addAsNew = true;
                if (!elements.isEmpty()) {
                    for (int i = 0; i < elements.size(); ++i) {
                        JsElement element = elements.get(i);
                        FileObject fo = element.getFileObject();
                        if (!element.isDeclared() || fo != null && fo.equals((Object)property.getFileObject())) {
                            if (!element.isDeclared() || element.getOffsetRange() == OffsetRange.NONE && property.getOffsetRange() != OffsetRange.NONE) {
                                elements.remove(i);
                                elements.add(property);
                                addAsNew = false;
                                break;
                            }
                            if (fo == null || !fo.equals((Object)property.getFileObject())) continue;
                            addAsNew = false;
                            break;
                        }
                        if (!element.isPlatform() || !property.isPlatform()) continue;
                        addAsNew = false;
                        break;
                    }
                }
                if (addAsNew) {
                    elements.add(property);
                }
            }
        }
    }

    private Map<String, List<JsElement>> getDomCompletionResults(JsCompletionItem.CompletionRequest request) {
        HashMap<String, List<JsElement>> result = new HashMap<String, List<JsElement>>(1);
        result.putAll(this.getCompletionFromExpressionChain(request, WINDOW_EXPRESSION_CHAIN));
        return result;
    }

    private static int getPrefixIndexFromSequence(String prefix) {
        int spaceIndex = prefix.lastIndexOf(" ") + 1;
        int dotIndex = prefix.lastIndexOf(".") + 1;
        int hashIndex = prefix.lastIndexOf("#") + 1;
        int bracketIndex = prefix.lastIndexOf("[") + 1;
        int columnIndex = prefix.lastIndexOf(":") + 1;
        int parenIndex = prefix.lastIndexOf("(") + 1;
        int slashIndex = prefix.lastIndexOf(47) + 1;
        return Math.max(0, Math.max(hashIndex, Math.max(dotIndex, Math.max(parenIndex, Math.max(columnIndex, Math.max(bracketIndex, Math.max(spaceIndex, slashIndex)))))));
    }

}

