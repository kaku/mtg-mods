/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.sdoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.modules.javascript2.editor.sdoc.SDocComment;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocDescriptionElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementUtils;
import org.netbeans.modules.parsing.api.Snapshot;

public class SDocParser {
    private static final Logger LOGGER = Logger.getLogger(SDocParser.class.getName());

    public static Map<Integer, SDocComment> parse(Snapshot snapshot) {
        HashMap<Integer, SDocComment> blocks = new HashMap<Integer, SDocComment>();
        TokenSequence tokenSequence = snapshot.getTokenHierarchy().tokenSequence(JsTokenId.javascriptLanguage());
        if (tokenSequence == null) {
            return blocks;
        }
        while (tokenSequence.moveNext()) {
            if (tokenSequence.token().id() != JsTokenId.DOC_COMMENT) continue;
            LOGGER.log(Level.FINEST, "SDocParser:comment block offset=[{0}-{1}],text={2}", new Object[]{tokenSequence.offset(), tokenSequence.offset() + tokenSequence.token().length(), tokenSequence.token().text()});
            OffsetRange offsetRange = new OffsetRange(tokenSequence.offset(), tokenSequence.offset() + tokenSequence.token().length());
            blocks.put(offsetRange.getEnd(), SDocParser.parseCommentBlock(tokenSequence, offsetRange));
        }
        return blocks;
    }

    private static boolean isCommentImportantToken(Token<? extends JsDocumentationTokenId> token) {
        return token.id() != JsDocumentationTokenId.ASTERISK && token.id() != JsDocumentationTokenId.COMMENT_DOC_START;
    }

    private static TokenSequence getEmbeddedSDocTS(TokenSequence ts) {
        return ts.embedded(JsDocumentationTokenId.language());
    }

    private static SDocComment parseCommentBlock(TokenSequence ts, OffsetRange range) {
        TokenSequence ets = SDocParser.getEmbeddedSDocTS(ts);
        ArrayList<SDocElement> sDocElements = new ArrayList<SDocElement>();
        StringBuilder sb = new StringBuilder();
        boolean afterDescriptionEntry = false;
        SDocElementType lastType = null;
        int lastOffset = ts.offset();
        while (ets.moveNext()) {
            Token currentToken = ets.token();
            if (!SDocParser.isCommentImportantToken(currentToken)) continue;
            if (currentToken.id() == JsDocumentationTokenId.KEYWORD || currentToken.id() == JsDocumentationTokenId.COMMENT_END) {
                if (sb.toString().trim().isEmpty()) {
                    if (lastType != null) {
                        sDocElements.add(SDocElementUtils.createElementForType(lastType, "", -1));
                    }
                } else {
                    if (!afterDescriptionEntry) {
                        sDocElements.add(SDocDescriptionElement.create(SDocElementType.DESCRIPTION, sb.toString().trim()));
                    } else {
                        sDocElements.add(SDocElementUtils.createElementForType(lastType, sb.toString().trim(), lastOffset));
                    }
                    sb = new StringBuilder();
                }
                while (ets.moveNext() && ets.token().id() == JsDocumentationTokenId.WHITESPACE) {
                }
                lastOffset = ets.offset();
                if (currentToken.id() != JsDocumentationTokenId.COMMENT_END) {
                    ets.movePrevious();
                }
                afterDescriptionEntry = true;
                lastType = SDocElementType.fromString(CharSequenceUtilities.toString((CharSequence)currentToken.text()));
                continue;
            }
            sb.append(currentToken.text());
        }
        return new SDocComment(range, sDocElements);
    }
}

