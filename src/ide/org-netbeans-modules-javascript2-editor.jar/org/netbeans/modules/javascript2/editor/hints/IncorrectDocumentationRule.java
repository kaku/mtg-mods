/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsFunctionDocumentationRule;

public class IncorrectDocumentationRule
extends JsFunctionDocumentationRule {
    @Override
    public String getId() {
        return "IncorrectDocumentationRule.hint";
    }

    @Override
    public String getDescription() {
        return Bundle.IncorrectDocumentationRuleDesc();
    }

    @Override
    public String getDisplayName() {
        return Bundle.IncorrectDocumentationRuleDN();
    }
}

