/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.DeclarationScope;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.JsWith;
import org.netbeans.modules.javascript2.editor.model.impl.IdentifierImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.model.impl.ModelElementFactory;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class OccurrenceBuilder {
    private final Map<String, Map<OffsetRange, Item>> holder = new HashMap<String, Map<OffsetRange, Item>>();
    private final JsParserResult parserResult;

    public OccurrenceBuilder(JsParserResult parserResult) {
        this.parserResult = parserResult;
    }

    public void addOccurrence(String name, OffsetRange range, DeclarationScope whereUsed, JsObject currentParent, JsWith inWith, boolean isFunction, boolean leftSite) {
        Map<OffsetRange, Item> items = this.holder.get(name);
        if (items == null) {
            items = new HashMap<OffsetRange, Item>(1);
            this.holder.put(name, items);
        }
        if (!items.containsKey((Object)range)) {
            items.put(range, new Item(range, whereUsed, currentParent, inWith, isFunction, leftSite));
        }
    }

    public void processOccurrences(JsObject global) {
        for (String name : this.holder.keySet()) {
            Map<OffsetRange, Item> items = this.holder.get(name);
            for (Item item : items.values()) {
                this.processOccurrence(global, name, item);
            }
        }
        this.holder.clear();
    }

    private void processOccurrence(JsObject global, String name, Item item) {
        JsObject property = null;
        JsObject parameter = null;
        JsObject parent = item.currentParent;
        if (!(parent instanceof JsWith || parent.getParent() != null && parent.getParent() instanceof JsWith)) {
            for (DeclarationScope scope = item.scope; scope != null && property == null && parameter == null; scope = scope.getParentScope()) {
                JsFunction function = (JsFunction)scope;
                property = function.getProperty(name);
                parameter = function.getParameter(name);
            }
            if (parameter != null) {
                if (property == null) {
                    property = parameter;
                } else if (property.getJSKind() != JsElement.Kind.VARIABLE) {
                    property = parameter;
                }
            }
        } else {
            if (!(parent instanceof JsWith) && parent.getParent() != null && parent.getParent() instanceof JsWith) {
                parent = parent.getParent();
            }
            property = parent.getProperty(name);
        }
        if (!(parent instanceof JsWith) && property == null) {
            for (JsObject possibleParent = parent; property == null && possibleParent != null; possibleParent = possibleParent.getParent()) {
                property = possibleParent.getProperty(name);
            }
        }
        if (property != null) {
            this.addDocNameOccurence((JsObjectImpl)property);
            this.addDocTypesOccurence((JsObjectImpl)property);
            ((JsObjectImpl)property).addOccurrence(item.range);
        } else {
            IdentifierImpl nameIden = ModelElementFactory.create(this.parserResult, name, item.range.getStart(), item.range.getEnd());
            if (nameIden != null) {
                if (item.currentWith != null) {
                    JsWith with = item.currentWith;
                    property = with.getProperty(name);
                    if (property != null) {
                        ((JsObjectImpl)property).addOccurrence(item.range);
                    } else {
                        this.createNewProperty(with, item, nameIden);
                    }
                } else {
                    if (!(parent instanceof JsWith)) {
                        parent = global;
                    }
                    this.createNewProperty(parent, item, nameIden);
                }
            }
        }
    }

    private void createNewProperty(JsObject parent, Item item, Identifier nameIden) {
        JsObjectImpl newObject;
        if (!item.isFunction) {
            newObject = new JsObjectImpl(parent, nameIden, nameIden.getOffsetRange(), item.leftSite, this.parserResult.getSnapshot().getMimeType(), null);
        } else {
            FileObject fo = this.parserResult.getSnapshot().getSource().getFileObject();
            newObject = new JsFunctionImpl(fo, parent, nameIden, Collections.EMPTY_LIST, this.parserResult.getSnapshot().getMimeType(), null);
        }
        newObject.addOccurrence(nameIden.getOffsetRange());
        parent.addProperty(nameIden.getName(), newObject);
        this.addDocNameOccurence(newObject);
        this.addDocTypesOccurence(newObject);
    }

    private void addDocNameOccurence(JsObjectImpl jsObject) {
        JsDocumentationHolder holder = this.parserResult.getDocumentationHolder();
        JsComment comment = holder.getCommentForOffset(jsObject.getOffset(), holder.getCommentBlocks());
        if (comment != null) {
            for (DocParameter docParameter : comment.getParameters()) {
                Identifier paramName = docParameter.getParamName();
                String name = docParameter.getParamName() == null ? "" : docParameter.getParamName().getName();
                if (!name.equals(jsObject.getName())) continue;
                jsObject.addOccurrence(paramName.getOffsetRange());
            }
        }
    }

    private void addDocTypesOccurence(JsObjectImpl jsObject) {
        JsDocumentationHolder holder = this.parserResult.getDocumentationHolder();
        if (holder.getOccurencesMap().containsKey(jsObject.getName())) {
            for (OffsetRange offsetRange : holder.getOccurencesMap().get(jsObject.getName())) {
                jsObject.addOccurrence(offsetRange);
            }
        }
    }

    private static class Item {
        final DeclarationScope scope;
        final JsObject currentParent;
        final JsWith currentWith;
        final boolean isFunction;
        final boolean leftSite;
        final OffsetRange range;

        public Item(OffsetRange range, DeclarationScope scope, JsObject currentParent, JsWith currentWith, boolean isFunction, boolean leftSite) {
            this.scope = scope;
            this.currentParent = currentParent;
            this.isFunction = isFunction;
            this.leftSite = leftSite;
            this.range = range;
            this.currentWith = currentWith;
        }
    }

}

