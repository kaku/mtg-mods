/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.jsdoc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocComment;
import org.netbeans.modules.javascript2.editor.jsdoc.JsDocCommentType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.DescriptionElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElement;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementUtils;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsDocParser {
    private static final Logger LOGGER = Logger.getLogger(JsDocParser.class.getName());

    public static Map<Integer, JsDocComment> parse(Snapshot snapshot) {
        HashMap<Integer, JsDocComment> blocks = new HashMap<Integer, JsDocComment>();
        TokenSequence tokenSequence = snapshot.getTokenHierarchy().tokenSequence(JsTokenId.javascriptLanguage());
        if (tokenSequence == null) {
            return blocks;
        }
        while (tokenSequence.moveNext()) {
            if (tokenSequence.token().id() != JsTokenId.DOC_COMMENT) continue;
            JsDocCommentType commentType = JsDocParser.getCommentType(tokenSequence.token().text());
            LOGGER.log(Level.FINEST, "JsDocParser:comment block offset=[{0}-{1}],type={2},text={3}", new Object[]{tokenSequence.offset(), tokenSequence.offset() + tokenSequence.token().length(), commentType, tokenSequence.token().text()});
            OffsetRange offsetRange = new OffsetRange(tokenSequence.offset(), tokenSequence.offset() + tokenSequence.token().length());
            if (commentType == JsDocCommentType.DOC_NO_CODE_START || commentType == JsDocCommentType.DOC_NO_CODE_END || commentType == JsDocCommentType.DOC_SHARED_TAG_END) {
                blocks.put(offsetRange.getEnd(), new JsDocComment(offsetRange, commentType, Collections.<JsDocElement>emptyList()));
                continue;
            }
            blocks.put(offsetRange.getEnd(), JsDocParser.parseCommentBlock(tokenSequence, offsetRange, commentType));
        }
        return blocks;
    }

    private static boolean isTextToken(Token<? extends JsDocumentationTokenId> token) {
        return token.id() != JsDocumentationTokenId.ASTERISK && token.id() != JsDocumentationTokenId.COMMENT_DOC_START;
    }

    private static TokenSequence getEmbeddedJsDocTS(TokenSequence ts) {
        return ts.embedded(JsDocumentationTokenId.language());
    }

    private static JsDocComment parseCommentBlock(TokenSequence ts, OffsetRange range, JsDocCommentType commentType) {
        TokenSequence ets = JsDocParser.getEmbeddedJsDocTS(ts);
        ArrayList<JsDocElement> jsDocElements = new ArrayList<JsDocElement>();
        JsDocElementType type = null;
        boolean afterDescription = false;
        StringBuilder sb = new StringBuilder();
        int offset = ts.offset();
        while (ets.moveNext()) {
            Token token = ets.token();
            if (!JsDocParser.isTextToken(token)) continue;
            JsDocElementType elementType = JsDocParser.getJsDocKeywordType(CharSequenceUtilities.toString((CharSequence)token.text()));
            if (token.id() == JsDocumentationTokenId.KEYWORD && elementType != JsDocElementType.UNKNOWN && elementType != JsDocElementType.LINK || token.id() == JsDocumentationTokenId.COMMENT_END) {
                if (sb.toString().trim().isEmpty()) {
                    if (type != null) {
                        jsDocElements.add(JsDocElementUtils.createElementForType(type, "", -1));
                    }
                } else {
                    if (!afterDescription) {
                        jsDocElements.add(DescriptionElement.create(JsDocElementType.CONTEXT_SENSITIVE, sb.toString().trim()));
                    } else {
                        jsDocElements.add(JsDocElementUtils.createElementForType(type, sb.toString().trim(), offset));
                    }
                    sb = new StringBuilder();
                }
                while (ets.moveNext() && ets.token().id() == JsDocumentationTokenId.WHITESPACE) {
                }
                offset = ets.offset();
                if (token.id() != JsDocumentationTokenId.COMMENT_END) {
                    ets.movePrevious();
                }
                afterDescription = true;
                type = JsDocElementType.fromString(CharSequenceUtilities.toString((CharSequence)token.text()));
                continue;
            }
            sb.append(token.text());
        }
        return new JsDocComment(range, commentType, jsDocElements);
    }

    private static JsDocCommentType getCommentType(CharSequence text) {
        if (CharSequenceUtilities.startsWith((CharSequence)text, (CharSequence)"/**#")) {
            if (CharSequenceUtilities.textEquals((CharSequence)text, (CharSequence)"/**#nocode+*/")) {
                return JsDocCommentType.DOC_NO_CODE_START;
            }
            if (CharSequenceUtilities.textEquals((CharSequence)text, (CharSequence)"/**#nocode-*/")) {
                return JsDocCommentType.DOC_NO_CODE_END;
            }
            if (CharSequenceUtilities.startsWith((CharSequence)text, (CharSequence)"/**#@+")) {
                return JsDocCommentType.DOC_SHARED_TAG_START;
            }
            if (CharSequenceUtilities.textEquals((CharSequence)text, (CharSequence)"/**#@-*/")) {
                return JsDocCommentType.DOC_SHARED_TAG_END;
            }
        }
        return JsDocCommentType.DOC_COMMON;
    }

    private static JsDocElementType getJsDocKeywordType(String string) {
        return JsDocElementType.fromString(string);
    }
}

