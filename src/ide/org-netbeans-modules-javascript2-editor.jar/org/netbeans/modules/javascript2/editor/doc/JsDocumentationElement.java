/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.Collections;
import java.util.Set;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.filesystems.FileObject;

public class JsDocumentationElement
implements ElementHandle {
    private final String name;
    private final String documentation;

    JsDocumentationElement(String name, String documentation) {
        this.name = name;
        this.documentation = documentation;
    }

    public FileObject getFileObject() {
        return null;
    }

    public String getMimeType() {
        return "";
    }

    public String getName() {
        return this.name;
    }

    public String getIn() {
        return "";
    }

    public ElementKind getKind() {
        return ElementKind.OTHER;
    }

    public Set<Modifier> getModifiers() {
        return Collections.emptySet();
    }

    public boolean signatureEquals(ElementHandle handle) {
        return false;
    }

    public OffsetRange getOffsetRange(ParserResult result) {
        return OffsetRange.NONE;
    }

    public String getDocumentation() {
        return this.documentation;
    }
}

