/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.ColoringAttributes
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OccurrencesFinder
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 */
package org.netbeans.modules.javascript2.editor.navigation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.modules.csl.api.ColoringAttributes;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OccurrencesFinder;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.OccurrencesSupport;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;

public class OccurrencesFinderImpl
extends OccurrencesFinder<JsParserResult> {
    private Map<OffsetRange, ColoringAttributes> range2Attribs;
    private int caretPosition;
    private volatile boolean cancelled;

    public void setCaretPosition(int position) {
        this.caretPosition = position;
    }

    public Map<OffsetRange, ColoringAttributes> getOccurrences() {
        return this.range2Attribs;
    }

    public void run(JsParserResult result, SchedulerEvent event) {
        this.range2Attribs = null;
        if (this.cancelled) {
            this.cancelled = false;
            return;
        }
        int offset = result.getSnapshot().getEmbeddedOffset(this.caretPosition);
        Set<OffsetRange> ranges = OccurrencesFinderImpl.findOccurrenceRanges(result, offset);
        this.range2Attribs = new HashMap<OffsetRange, ColoringAttributes>();
        if (this.cancelled) {
            this.cancelled = false;
            return;
        }
        for (OffsetRange offsetRange : ranges) {
            this.range2Attribs.put(ModelUtils.documentOffsetRange(result, offsetRange.getStart(), offsetRange.getEnd()), ColoringAttributes.MARK_OCCURRENCES);
        }
    }

    public int getPriority() {
        return 0;
    }

    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.CURSOR_SENSITIVE_TASK_SCHEDULER;
    }

    public void cancel() {
        this.cancelled = true;
    }

    private static List<OffsetRange> findMemberUsage(JsObject object, String fqnType, String property, int offset) {
        Collection<? extends TypeUsage> assignments;
        ArrayList<OffsetRange> result = new ArrayList<OffsetRange>();
        String fqn = fqnType;
        if (fqn.endsWith(".prototype")) {
            fqn = fqn.substring(0, fqn.length() - 10);
        }
        if (!(assignments = object.getAssignments()).isEmpty()) {
            for (TypeUsage type : assignments) {
                JsObject member;
                if (!type.getType().equals(fqn) || (member = object.getProperty(property)) == null) continue;
                result.add(member.getDeclarationName().getOffsetRange());
                List<Occurrence> occurrences = member.getOccurrences();
                for (Occurrence occurence : occurrences) {
                    result.add(occurence.getOffsetRange());
                }
            }
        }
        if (!(object instanceof JsObjectReference) || !ModelUtils.isDescendant(object, ((JsObjectReference)object).getOriginal())) {
            for (JsObject child : object.getProperties().values()) {
                result.addAll(OccurrencesFinderImpl.findMemberUsage(child, fqn, property, offset));
            }
        }
        return result;
    }

    public static Set<OffsetRange> findOccurrenceRanges(JsParserResult result, int caretPosition) {
        HashSet<OffsetRange> offsets = new HashSet<OffsetRange>();
        Model model = result.getModel();
        OccurrencesSupport os = model.getOccurrencesSupport();
        Occurrence occurrence = os.getOccurrence(caretPosition);
        if (occurrence != null) {
            for (JsObject object : occurrence.getDeclarations()) {
                if (object == null || object.getDeclarationName() == null) continue;
                offsets.add(object.getDeclarationName().getOffsetRange());
                for (Occurrence oc : object.getOccurrences()) {
                    offsets.add(oc.getOffsetRange());
                }
                JsObject parent = object.getParent();
                if (parent == null || parent.getJSKind() == JsElement.Kind.FILE || object.getJSKind() == JsElement.Kind.PARAMETER || object.getModifiers().contains((Object)Modifier.PRIVATE)) continue;
                Collection<? extends TypeUsage> types = parent.getAssignmentForOffset(caretPosition);
                if (types.isEmpty()) {
                    types = parent.getAssignments();
                }
                for (Type type : types) {
                    JsObject declaration = ModelUtils.findJsObjectByName(model, type.getType());
                    if (declaration != null && !object.getName().equals(declaration.getName())) {
                        JsObject prototype = declaration.getProperty("prototype");
                        if ((declaration = declaration.getProperty(object.getName())) == null && prototype != null) {
                            declaration = prototype.getProperty(object.getName());
                        }
                    }
                    if (declaration == null || declaration.getModifiers().contains((Object)Modifier.PRIVATE)) continue;
                    offsets.add(declaration.getDeclarationName().getOffsetRange());
                    for (Occurrence oc2 : declaration.getOccurrences()) {
                        offsets.add(oc2.getOffsetRange());
                    }
                }
                if (!types.isEmpty()) continue;
                List<OffsetRange> usages = OccurrencesFinderImpl.findMemberUsage(result.getModel().getGlobalObject(), parent.getFullyQualifiedName(), object.getName(), caretPosition);
                for (OffsetRange range : usages) {
                    offsets.add(range);
                }
            }
        }
        return offsets;
    }
}

