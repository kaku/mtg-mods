/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.ElementKind
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.netbeans.modules.csl.api.Modifier
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.StructureItem
 *  org.netbeans.modules.csl.api.StructureScanner
 *  org.netbeans.modules.csl.api.StructureScanner$Configuration
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.javascript2.editor;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.ElementKind;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.Modifier;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.Utils;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.openide.util.ImageUtilities;

public class JsStructureScanner
implements StructureScanner {
    private static final String FOLD_FUNCTION = "codeblocks";
    private static final String FOLD_JSDOC = "comments";
    private static final String FOLD_COMMENT = "initial-comment";
    private static final String FOLD_OTHER_CODE_BLOCKS = "othercodeblocks";
    private static final String FONT_GRAY_COLOR = "<font color=\"#999999\">";
    private static final String CLOSE_FONT = "</font>";
    private static final Logger LOGGER = Logger.getLogger(JsStructureScanner.class.getName());
    private final Language<JsTokenId> language;
    private static ImageIcon priviligedIcon = null;

    public JsStructureScanner(Language<JsTokenId> language) {
        this.language = language;
    }

    public List<? extends StructureItem> scan(ParserResult info) {
        ArrayList<StructureItem> items = new ArrayList<StructureItem>();
        long start = System.currentTimeMillis();
        LOGGER.log(Level.FINE, "Structure scanner started at {0} ms", start);
        JsParserResult result = (JsParserResult)info;
        Model model = result.getModel();
        model.resolve();
        JsObject globalObject = model.getGlobalObject();
        this.getEmbededItems(result, globalObject, items);
        long end = System.currentTimeMillis();
        LOGGER.log(Level.FINE, "Creating structure took {0} ms", new Object[]{end - start});
        return items;
    }

    private List<StructureItem> getEmbededItems(JsParserResult result, JsObject jsObject, List<StructureItem> collectedItems) {
        ArrayList<? extends JsObject> properties = new ArrayList<JsObject>(jsObject.getProperties().values());
        boolean countFunctionChild = jsObject.getJSKind().isFunction() && !jsObject.isAnonymous() && jsObject.getJSKind() != JsElement.Kind.CONSTRUCTOR && !this.containsFunction(jsObject) || "prototype".equals(jsObject.getName()) && properties.isEmpty();
        for (JsObject child : properties) {
            if (result.getSnapshot().getOriginalOffset(child.getOffset()) < 0 && !"prototype".equals(child.getName())) continue;
            List children = new ArrayList<StructureItem>();
            if ((countFunctionChild && !child.getModifiers().contains((Object)Modifier.STATIC) && !child.getName().equals("prototype") || child.getJSKind() == JsElement.Kind.ANONYMOUS_OBJECT) && child.getJSKind() != JsElement.Kind.OBJECT_LITERAL || child.getJSKind().isFunction() && child.isAnonymous() && child.getParent().getJSKind().isFunction() && child.getParent().getJSKind() != JsElement.Kind.FILE) continue;
            if (!(child instanceof JsObjectReference) || !ModelUtils.isDescendant(child, ((JsObjectReference)child).getOriginal())) {
                children = this.getEmbededItems(result, child, children);
            }
            if ((child.hasExactName() || child.isAnonymous() || child.getJSKind() == JsElement.Kind.CONSTRUCTOR) && child.getJSKind().isFunction()) {
                JsFunction function = (JsFunction)child;
                if (function.isAnonymous()) {
                    collectedItems.addAll(children);
                    continue;
                }
                if (!function.isDeclared() || jsObject.isAnonymous() && (!jsObject.isAnonymous() || jsObject.getFullyQualifiedName().indexOf(46) != -1)) continue;
                collectedItems.add(new JsFunctionStructureItem(function, children, result));
                continue;
            }
            if ((child.getJSKind() == JsElement.Kind.OBJECT && (children.size() > 0 || child.isDeclared()) || child.getJSKind() == JsElement.Kind.OBJECT_LITERAL || child.getJSKind() == JsElement.Kind.ANONYMOUS_OBJECT) && (children.size() > 0 || child.isDeclared())) {
                collectedItems.add(new JsObjectStructureItem(child, children, result));
                continue;
            }
            if (child.getJSKind() == JsElement.Kind.PROPERTY) {
                if (!child.isDeclared() || !child.getModifiers().contains((Object)Modifier.PUBLIC) && jsObject.getParent() instanceof JsFunction) continue;
                collectedItems.add(new JsSimpleStructureItem(child, "prop-", result));
                continue;
            }
            if (child.getJSKind() != JsElement.Kind.VARIABLE || !child.isDeclared() || jsObject.isAnonymous() && (!jsObject.isAnonymous() || jsObject.getFullyQualifiedName().indexOf(46) != -1)) continue;
            collectedItems.add(new JsSimpleStructureItem(child, "var-", result));
        }
        if (jsObject instanceof JsFunction) {
            for (JsObject param : ((JsFunction)jsObject).getParameters()) {
                if (!this.hasDeclaredProperty(param) || jsObject instanceof JsObjectReference && !((JsObjectReference)jsObject).getOriginal().isAnonymous()) continue;
                ArrayList<StructureItem> items = new ArrayList<StructureItem>();
                this.getEmbededItems(result, param, items);
                collectedItems.add(new JsObjectStructureItem(param, items, result));
            }
        }
        return collectedItems;
    }

    private boolean containsFunction(JsObject jsObject) {
        for (JsObject property : jsObject.getProperties().values()) {
            if (property.getJSKind().isFunction() && property.isDeclared() && !property.isAnonymous()) {
                return true;
            }
            if (!this.containsFunction(property)) continue;
            return true;
        }
        return false;
    }

    private boolean isNotAnonymousFunction(TokenSequence ts, int functionKeywordPosition) {
        int position = ts.offset();
        boolean value = false;
        ts.move(functionKeywordPosition);
        ts.moveNext();
        Token<? extends JsTokenId> token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE}));
        if ((token.id() == JsTokenId.OPERATOR_ASSIGNMENT || token.id() == JsTokenId.OPERATOR_COLON) && ts.movePrevious() && (token = LexUtilities.findPrevious(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE}))).id() == JsTokenId.IDENTIFIER) {
            value = true;
        }
        if (!value) {
            ts.move(functionKeywordPosition);
            ts.moveNext();
            ts.moveNext();
            token = LexUtilities.findNext(ts, Arrays.asList(new JsTokenId[]{JsTokenId.WHITESPACE}));
            if (token.id() == JsTokenId.IDENTIFIER) {
                value = true;
            }
        }
        ts.move(position);
        ts.moveNext();
        return value;
    }

    public Map<String, List<OffsetRange>> folds(ParserResult info) {
        long start = System.currentTimeMillis();
        HashMap<String, List<OffsetRange>> folds = new HashMap<String, List<OffsetRange>>();
        TokenHierarchy th = info.getSnapshot().getTokenHierarchy();
        TokenSequence ts = th.tokenSequence(this.language);
        List list = th.tokenSequenceList(ts.languagePath(), 0, info.getSnapshot().getText().length());
        ArrayList<FoldingItem> stack = new ArrayList<FoldingItem>();
        TokenSequenceIterator tsi = new TokenSequenceIterator(list, false);
        while (tsi.hasMore()) {
            ts = tsi.getSequence();
            JsTokenId lastContextId = null;
            int functionKeywordPosition = 0;
            ts.moveStart();
            while (ts.moveNext()) {
                int startOffset;
                int endOffset;
                TokenId tokenId = ts.token().id();
                if (tokenId == JsTokenId.DOC_COMMENT) {
                    startOffset = ts.offset() + 3;
                    endOffset = ts.offset() + ts.token().length() - 2;
                    this.appendFold(folds, "comments", info.getSnapshot().getOriginalOffset(startOffset), info.getSnapshot().getOriginalOffset(endOffset));
                    continue;
                }
                if (tokenId == JsTokenId.BLOCK_COMMENT) {
                    startOffset = ts.offset() + 2;
                    endOffset = ts.offset() + ts.token().length() - 2;
                    this.appendFold(folds, "initial-comment", info.getSnapshot().getOriginalOffset(startOffset), info.getSnapshot().getOriginalOffset(endOffset));
                    continue;
                }
                if (((JsTokenId)tokenId).isKeyword()) {
                    lastContextId = (JsTokenId)tokenId;
                    if (lastContextId != JsTokenId.KEYWORD_FUNCTION) continue;
                    functionKeywordPosition = ts.offset();
                    continue;
                }
                if (tokenId == JsTokenId.BRACKET_LEFT_CURLY) {
                    String kind = lastContextId == JsTokenId.KEYWORD_FUNCTION && this.isNotAnonymousFunction(ts, functionKeywordPosition) ? "codeblocks" : "othercodeblocks";
                    stack.add(new FoldingItem(kind, ts.offset()));
                    continue;
                }
                if (tokenId != JsTokenId.BRACKET_RIGHT_CURLY || stack.isEmpty()) continue;
                FoldingItem fromStack = (FoldingItem)stack.remove(stack.size() - 1);
                this.appendFold(folds, fromStack.kind, info.getSnapshot().getOriginalOffset(fromStack.start), info.getSnapshot().getOriginalOffset(ts.offset() + 1));
            }
        }
        long end = System.currentTimeMillis();
        LOGGER.log(Level.FINE, "Folding took %s ms", end - start);
        return folds;
    }

    private void appendFold(Map<String, List<OffsetRange>> folds, String kind, int startOffset, int endOffset) {
        if (startOffset >= 0 && endOffset >= startOffset) {
            this.getRanges(folds, kind).add(new OffsetRange(startOffset, endOffset));
        }
    }

    private List<OffsetRange> getRanges(Map<String, List<OffsetRange>> folds, String kind) {
        List<OffsetRange> ranges = folds.get(kind);
        if (ranges == null) {
            ranges = new ArrayList<OffsetRange>();
            folds.put(kind, ranges);
        }
        return ranges;
    }

    public StructureScanner.Configuration getConfiguration() {
        return null;
    }

    private boolean hasDeclaredProperty(JsObject jsObject) {
        boolean result = false;
        Iterator<? extends JsObject> it = jsObject.getProperties().values().iterator();
        while (!result && it.hasNext()) {
            JsObject property = it.next();
            result = property.isDeclared();
            if (result) continue;
            result = this.hasDeclaredProperty(property);
        }
        return result;
    }

    private static final class TokenSequenceIterator {
        private final List<TokenSequence<?>> list;
        private final boolean backward;
        private int index;

        public TokenSequenceIterator(List<TokenSequence<?>> list, boolean backward) {
            this.list = list;
            this.backward = backward;
            this.index = -1;
        }

        public boolean hasMore() {
            return this.backward ? this.hasPrevious() : this.hasNext();
        }

        public TokenSequence<?> getSequence() {
            assert (this.index >= 0 && this.index < this.list.size());
            return this.list.get(this.index);
        }

        private boolean hasPrevious() {
            boolean anotherSeq = false;
            if (this.index == -1) {
                this.index = this.list.size() - 1;
                anotherSeq = true;
            }
            while (this.index >= 0) {
                TokenSequence seq = this.list.get(this.index);
                if (anotherSeq) {
                    seq.moveEnd();
                }
                if (seq.movePrevious()) {
                    return true;
                }
                anotherSeq = true;
                --this.index;
            }
            return false;
        }

        private boolean hasNext() {
            boolean anotherSeq = false;
            if (this.index == -1) {
                this.index = 0;
                anotherSeq = true;
            }
            while (this.index < this.list.size()) {
                TokenSequence seq = this.list.get(this.index);
                if (anotherSeq) {
                    seq.moveStart();
                }
                if (seq.moveNext()) {
                    return true;
                }
                anotherSeq = true;
                ++this.index;
            }
            return false;
        }
    }

    private class JsSimpleStructureItem
    extends JsStructureItem {
        private final JsObject object;
        private final List<TypeUsage> resolvedTypes;

        public JsSimpleStructureItem(JsObject elementHandle, String sortPrefix, JsParserResult parserResult) {
            super(elementHandle, null, sortPrefix, parserResult);
            this.object = elementHandle;
            Collection<? extends TypeUsage> assignmentForOffset = this.object.getAssignmentForOffset(this.object.getDeclarationName().getOffsetRange().getEnd());
            this.resolvedTypes = new ArrayList<TypeUsage>(ModelUtils.resolveTypes(assignmentForOffset, parserResult, true));
        }

        public String getHtml(HtmlFormatter formatter) {
            formatter.reset();
            boolean isDeprecated = this.object.isDeprecated();
            if (isDeprecated) {
                formatter.deprecated(true);
            }
            formatter.appendText(this.getElementHandle().getName());
            if (isDeprecated) {
                formatter.deprecated(false);
            }
            this.appendTypeInfo(formatter, this.resolvedTypes);
            return formatter.getText();
        }
    }

    private class JsObjectStructureItem
    extends JsStructureItem {
        public JsObjectStructureItem(JsObject elementHandle, List<? extends StructureItem> children, JsParserResult parserResult) {
            super(elementHandle, children, "ob", parserResult);
        }

        public String getHtml(HtmlFormatter formatter) {
            formatter.reset();
            this.appendObjectDescription(this.getModelElement(), formatter);
            return formatter.getText();
        }

        protected void appendObjectDescription(JsObject object, HtmlFormatter formatter) {
            formatter.reset();
            if (object == null) {
                return;
            }
            boolean isDeprecated = object.isDeprecated();
            if (isDeprecated) {
                formatter.deprecated(true);
            }
            formatter.appendText(object.getName());
            if (isDeprecated) {
                formatter.deprecated(false);
            }
        }
    }

    private class JsFunctionStructureItem
    extends JsStructureItem {
        private final List<TypeUsage> resolvedTypes;

        public JsFunctionStructureItem(JsFunction elementHandle, List<? extends StructureItem> children, JsParserResult parserResult) {
            super(elementHandle, children, "fn", parserResult);
            Collection<? extends TypeUsage> returnTypes = this.getFunctionScope().getReturnTypes();
            this.resolvedTypes = new ArrayList<TypeUsage>(ModelUtils.resolveTypes(returnTypes, parserResult, true));
        }

        public final JsFunction getFunctionScope() {
            return (JsFunction)this.getModelElement();
        }

        public String getHtml(HtmlFormatter formatter) {
            formatter.reset();
            this.appendFunctionDescription(this.getFunctionScope(), formatter);
            return formatter.getText();
        }

        protected void appendFunctionDescription(JsFunction function, HtmlFormatter formatter) {
            formatter.reset();
            if (function == null) {
                return;
            }
            boolean isDeprecated = this.getFunctionScope().isDeprecated();
            if (isDeprecated) {
                formatter.deprecated(true);
            }
            formatter.appendText(this.getFunctionScope().getDeclarationName().getName());
            if (isDeprecated) {
                formatter.deprecated(false);
            }
            formatter.appendText("(");
            boolean addComma = false;
            for (JsObject jsObject : function.getParameters()) {
                if (addComma) {
                    formatter.appendText(", ");
                } else {
                    addComma = true;
                }
                Collection<? extends TypeUsage> types = jsObject.getAssignmentForOffset(jsObject.getDeclarationName().getOffsetRange().getStart());
                if (!types.isEmpty()) {
                    formatter.appendHtml("<font color=\"#999999\">");
                    StringBuilder typeSb = new StringBuilder();
                    for (TypeUsage type : types) {
                        if (typeSb.length() > 0) {
                            typeSb.append("|");
                        }
                        typeSb.append(type.getType());
                    }
                    if (typeSb.length() > 0) {
                        formatter.appendText(typeSb.toString());
                    }
                    formatter.appendText(" ");
                    formatter.appendHtml("</font>");
                }
                formatter.appendText(jsObject.getName());
            }
            formatter.appendText(")");
            this.appendTypeInfo(formatter, this.resolvedTypes);
        }

        @Override
        public String getName() {
            return this.getFunctionScope().getDeclarationName().getName();
        }

        @Override
        public ImageIcon getCustomIcon() {
            if (this.getModifiers().contains((Object)Modifier.PROTECTED)) {
                if (priviligedIcon == null) {
                    priviligedIcon = new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/javascript2/editor/resources/methodPriviliged.png"));
                }
                return priviligedIcon;
            }
            return super.getCustomIcon();
        }
    }

    private abstract class JsStructureItem
    implements StructureItem {
        private JsObject modelElement;
        private final List<? extends StructureItem> children;
        private final String sortPrefix;
        protected final JsParserResult parserResult;
        private final String fqn;

        public JsStructureItem(JsObject elementHandle, List<? extends StructureItem> children, String sortPrefix, JsParserResult parserResult) {
            this.modelElement = elementHandle;
            this.sortPrefix = sortPrefix;
            this.parserResult = parserResult;
            this.fqn = this.modelElement.getFullyQualifiedName();
            this.children = children != null ? children : Collections.emptyList();
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            JsStructureItem other = (JsStructureItem)obj;
            if (this.fqn == null ? other.fqn != null : !this.fqn.equals(other.fqn)) {
                return false;
            }
            if (this.modelElement == null && other.modelElement != null || this.modelElement != null && other.modelElement == null) {
                return false;
            }
            if (this.modelElement != other.modelElement && (this.modelElement.getJSKind() == null ? other.modelElement.getJSKind() != null : !this.modelElement.getJSKind().equals((Object)other.modelElement.getJSKind()))) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hash = 5;
            hash = 37 * hash + (this.fqn != null ? this.fqn.hashCode() : 0);
            hash = 37 * hash + (this.modelElement != null && this.modelElement.getJSKind() != null ? this.modelElement.getJSKind().hashCode() : 0);
            return hash;
        }

        public String getName() {
            return this.modelElement.getName();
        }

        public String getSortText() {
            return this.sortPrefix + this.modelElement.getName();
        }

        public ElementHandle getElementHandle() {
            return this.modelElement;
        }

        public ElementKind getKind() {
            return this.modelElement.getKind();
        }

        public Set<Modifier> getModifiers() {
            return this.modelElement.getModifiers();
        }

        public boolean isLeaf() {
            return this.children.isEmpty();
        }

        public List<? extends StructureItem> getNestedItems() {
            return this.children;
        }

        public long getPosition() {
            return this.parserResult.getSnapshot().getOriginalOffset(this.modelElement.getOffset());
        }

        public long getEndPosition() {
            return this.parserResult.getSnapshot().getOriginalOffset(this.modelElement.getOffsetRange().getEnd());
        }

        public ImageIcon getCustomIcon() {
            return null;
        }

        public JsObject getModelElement() {
            return this.modelElement;
        }

        protected void appendTypeInfo(HtmlFormatter formatter, Collection<? extends Type> types) {
            Collection<String> displayNames = Utils.getDisplayNames(types);
            if (!displayNames.isEmpty()) {
                formatter.appendHtml("<font color=\"#999999\">");
                formatter.appendText(" : ");
                boolean addDelimiter = false;
                for (String displayName : displayNames) {
                    if (addDelimiter) {
                        formatter.appendText("|");
                    } else {
                        addDelimiter = true;
                    }
                    formatter.appendHtml(displayName);
                }
                formatter.appendHtml("</font>");
            }
        }
    }

    private static class FoldingItem {
        String kind;
        int start;

        public FoldingItem(String kind, int start) {
            this.kind = kind;
            this.start = start;
        }
    }

}

