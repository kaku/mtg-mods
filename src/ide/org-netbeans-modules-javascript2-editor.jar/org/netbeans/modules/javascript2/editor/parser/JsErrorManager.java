/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.parser.Token
 *  jdk.nashorn.internal.parser.TokenType
 *  jdk.nashorn.internal.runtime.ErrorManager
 *  jdk.nashorn.internal.runtime.ParserException
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.Error
 *  org.netbeans.modules.csl.api.Severity
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jdk.nashorn.internal.parser.TokenType;
import jdk.nashorn.internal.runtime.ErrorManager;
import jdk.nashorn.internal.runtime.ParserException;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.api.Severity;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.embedding.JsEmbeddingProvider;
import org.netbeans.modules.javascript2.editor.parser.JsParserError;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class JsErrorManager
extends ErrorManager {
    private static final Logger LOGGER = Logger.getLogger(JsErrorManager.class.getName());
    private static final int MAX_MESSAGE_LENGTH = 100;
    private static final boolean SHOW_BADGES_EMBEDDED = Boolean.getBoolean(JsErrorManager.class.getName() + ".showBadgesEmbedded");
    private static final Comparator<SimpleError> POSITION_COMPARATOR = new Comparator<SimpleError>(){

        @Override
        public int compare(SimpleError o1, SimpleError o2) {
            if (o1.getPosition() < o2.getPosition()) {
                return -1;
            }
            if (o1.getPosition() > o2.getPosition()) {
                return 1;
            }
            return 0;
        }
    };
    private static final Pattern ERROR_MESSAGE_PATTERN = Pattern.compile(".*:\\d+:\\d+ (.*)", 32);
    private static final Pattern REPLACE_POINTER_PATTERN = Pattern.compile("(\\n)+.*\\n\\s*\\^\\s*");
    private static final String EXPECTED = "Expected";
    private final Snapshot snapshot;
    private final Language<JsTokenId> language;
    private List<ParserError> parserErrors;
    private List<JsParserError> convertedErrors;
    private static final Map<String, JsTokenId> JS_TEXT_TOKENS = new HashMap<String, JsTokenId>();

    public JsErrorManager(Snapshot snapshot, Language<JsTokenId> language) {
        this.snapshot = snapshot;
        this.language = language;
    }

    Error getMissingCurlyError() {
        if (this.parserErrors == null) {
            return null;
        }
        for (ParserError error : this.parserErrors) {
            if (error.message == null || !error.message.contains("Expected }") && !error.message.contains("but found }")) continue;
            return new JsParserError(this.convert(error), this.snapshot != null ? this.snapshot.getSource().getFileObject() : null, Severity.ERROR, null, true, false, false);
        }
        return null;
    }

    Error getMissingSemicolonError() {
        if (this.parserErrors == null) {
            return null;
        }
        for (ParserError error : this.parserErrors) {
            if (error.message == null || !error.message.contains("Expected ;")) continue;
            return new JsParserError(this.convert(error), this.snapshot != null ? this.snapshot.getSource().getFileObject() : null, Severity.ERROR, null, true, false, false);
        }
        return null;
    }

    public boolean isEmpty() {
        return this.parserErrors == null;
    }

    public void error(ParserException e) {
        this.addParserError(new ParserError(e.getMessage(), e.getLineNumber(), e.getColumnNumber(), e.getToken()));
    }

    public void error(String message) {
        LOGGER.log(Level.FINE, "Error {0}", message);
        this.addParserError(new ParserError(message));
    }

    public void warning(ParserException e) {
        LOGGER.log(Level.FINE, null, (Throwable)e);
    }

    public void warning(String message) {
        LOGGER.log(Level.FINE, "Warning {0}", message);
    }

    public List<? extends Error> getErrors() {
        if (this.convertedErrors == null) {
            if (this.parserErrors == null) {
                this.convertedErrors = Collections.emptyList();
            } else {
                ArrayList<SimpleError> errors = new ArrayList<SimpleError>(this.parserErrors.size());
                for (ParserError error : this.parserErrors) {
                    errors.add(this.convert(error));
                }
                Collections.sort(errors, POSITION_COMPARATOR);
                this.convertedErrors = JsErrorManager.convert(this.snapshot, errors);
            }
        }
        return Collections.unmodifiableList(this.convertedErrors);
    }

    JsErrorManager fillErrors(JsErrorManager original) {
        assert (this.snapshot == original.snapshot);
        assert (this.language == original.language);
        this.parserErrors = original.parserErrors != null ? new ArrayList<ParserError>(original.parserErrors) : null;
        this.convertedErrors = null;
        return this;
    }

    private void addParserError(ParserError error) {
        this.convertedErrors = null;
        if (this.parserErrors == null) {
            this.parserErrors = new ArrayList<ParserError>();
        }
        this.parserErrors.add(error);
    }

    private SimpleError convert(ParserError error) {
        String[] parts;
        String message = error.message;
        int offset = -1;
        Matcher matcher = ERROR_MESSAGE_PATTERN.matcher(message);
        if (matcher.matches()) {
            message = matcher.group(1);
        }
        message = REPLACE_POINTER_PATTERN.matcher(message).replaceAll("");
        if (error.token > 0) {
            offset = jdk.nashorn.internal.parser.Token.descPosition((long)error.token);
            if (jdk.nashorn.internal.parser.Token.descType((long)error.token) == TokenType.EOF && this.snapshot.getOriginalOffset(offset) == -1) {
                int realOffset = -1;
                TokenSequence<? extends JsTokenId> ts = LexUtilities.getPositionedSequence(this.snapshot, offset, this.language);
                while (ts.movePrevious()) {
                    if (this.snapshot.getOriginalOffset(ts.offset()) <= 0) continue;
                    realOffset = ts.offset() + ts.token().length() - 1;
                    break;
                }
                if (realOffset > 0) {
                    offset = realOffset;
                }
            }
        } else if (error.line == -1 && error.column == -1 && (parts = error.message.split(":")).length > 3) {
            try {
                offset = Integer.parseInt(parts[3]);
            }
            catch (NumberFormatException nfe) {
                // empty catch block
            }
        }
        return new SimpleError(message, offset);
    }

    private static List<JsParserError> convert(Snapshot snapshot, List<SimpleError> errors) {
        FileObject file;
        ArrayList<JsParserError> ret = new ArrayList<JsParserError>(errors.size());
        FileObject fileObject = file = snapshot != null ? snapshot.getSource().getFileObject() : null;
        if (snapshot != null && JsParserResult.isEmbedded(snapshot)) {
            int nextCorrect = -1;
            boolean afterGeneratedIdentifier = false;
            for (SimpleError error : errors) {
                boolean showInEditor = true;
                int pos = snapshot.getOriginalOffset(error.getPosition());
                if (pos >= 0 && nextCorrect <= error.getPosition() && !JsEmbeddingProvider.containsGeneratedIdentifier(error.getMessage())) {
                    TokenSequence<? extends JsTokenId> ts = LexUtilities.getJsPositionedSequence(snapshot, error.getPosition());
                    if (ts != null && ts.movePrevious()) {
                        Token<? extends JsTokenId> token = LexUtilities.findPreviousNonWsNonComment(ts);
                        if (JsEmbeddingProvider.containsGeneratedIdentifier(token.text().toString())) {
                            nextCorrect = JsErrorManager.findNextCorrectOffset(ts, error.getPosition());
                            showInEditor = false;
                            afterGeneratedIdentifier = true;
                        } else if (afterGeneratedIdentifier && error.getMessage().indexOf("Expected") != -1) {
                            String expected = JsErrorManager.getExpected(error.getMessage());
                            if ("eof".equals(expected)) {
                                showInEditor = false;
                            } else {
                                JsTokenId expectedToken = JsErrorManager.getJsTokenFromString(expected);
                                ts.movePrevious();
                                Token<? extends JsTokenId> previousNonWsToken = LexUtilities.findPreviousNonWsNonComment(ts);
                                if (expectedToken != null && expectedToken == previousNonWsToken.id()) {
                                    showInEditor = false;
                                }
                            }
                        }
                    }
                } else {
                    showInEditor = false;
                }
                ret.add(new JsParserError(error, file, Severity.ERROR, null, true, SHOW_BADGES_EMBEDDED, showInEditor));
            }
        } else {
            for (SimpleError error : errors) {
                ret.add(new JsParserError(error, file, Severity.ERROR, null, true, true, true));
            }
        }
        return ret;
    }

    private static String getExpected(String errorMessage) {
        int expectedIndex = errorMessage.indexOf("Expected");
        String afterExpected = errorMessage.substring(expectedIndex + 9);
        int indexOfSpace = afterExpected.indexOf(" ");
        return indexOfSpace != -1 ? afterExpected.substring(0, indexOfSpace) : afterExpected;
    }

    public static JsTokenId getJsTokenFromString(String name) {
        return JS_TEXT_TOKENS.get(name);
    }

    private static int findNextCorrectOffset(TokenSequence<? extends JsTokenId> ts, int offset) {
        ts.move(offset);
        if (ts.moveNext()) {
            LexUtilities.findNextIncluding(ts, Collections.singletonList(JsTokenId.BRACKET_LEFT_CURLY));
            LexUtilities.findNextIncluding(ts, Collections.singletonList(JsTokenId.EOL));
        }
        return ts.offset();
    }

    static {
        for (JsTokenId jsTokenId : JsTokenId.values()) {
            if (jsTokenId.fixedText() == null) continue;
            JS_TEXT_TOKENS.put(jsTokenId.fixedText(), jsTokenId);
        }
    }

    private static class ParserError {
        protected final String message;
        protected final int line;
        protected final int column;
        protected final long token;

        public ParserError(String message, int line, int column, long token) {
            if (message.length() > 100) {
                int index = message.indexOf(10, 100);
                this.message = message.substring(0, index > 0 ? index : 100);
                LOGGER.log(Level.FINE, "Too long error message {0}", message);
            } else {
                this.message = message;
            }
            this.line = line;
            this.column = column;
            this.token = token;
        }

        public ParserError(String message, long token) {
            this(message, -1, -1, token);
        }

        public ParserError(String message) {
            this(message, -1, -1, -1);
        }
    }

    static class SimpleError {
        private final String message;
        private final int position;

        public SimpleError(String message, int position) {
            this.message = message;
            this.position = position;
        }

        public String getMessage() {
            return this.message;
        }

        public int getPosition() {
            return this.position;
        }
    }

}

