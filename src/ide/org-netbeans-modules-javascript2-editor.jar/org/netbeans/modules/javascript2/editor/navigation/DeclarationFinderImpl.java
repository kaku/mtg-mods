/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.SuppressWarnings
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.csl.api.DeclarationFinder
 *  org.netbeans.modules.csl.api.DeclarationFinder$AlternativeLocation
 *  org.netbeans.modules.csl.api.DeclarationFinder$DeclarationLocation
 *  org.netbeans.modules.csl.api.ElementHandle
 *  org.netbeans.modules.csl.api.HtmlFormatter
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.indexing.support.IndexResult
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.javascript2.editor.navigation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.SuppressWarnings;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.csl.api.DeclarationFinder;
import org.netbeans.modules.csl.api.ElementHandle;
import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.EditorExtender;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.index.IndexedElement;
import org.netbeans.modules.javascript2.editor.index.JsIndex;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.OccurrencesSupport;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.model.TypeUsage;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class DeclarationFinderImpl
implements DeclarationFinder {
    private final Language<JsTokenId> language;

    public DeclarationFinderImpl(Language<JsTokenId> language) {
        this.language = language;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public DeclarationFinder.DeclarationLocation findDeclaration(ParserResult info, int caretOffset) {
        if (!(info instanceof JsParserResult)) {
            return DeclarationFinder.DeclarationLocation.NONE;
        }
        JsParserResult jsResult = (JsParserResult)info;
        Model model = jsResult.getModel();
        model.resolve();
        int offset = info.getSnapshot().getEmbeddedOffset(caretOffset);
        OccurrencesSupport os = model.getOccurrencesSupport();
        Occurrence occurrence = os.getOccurrence(offset);
        if (occurrence != null) {
            Collection<? extends TypeUsage> assignments;
            FileObject fo;
            JsObject object = occurrence.getDeclarations().iterator().next();
            JsObject parent = object.getParent();
            Collection<? extends TypeUsage> collection = assignments = parent == null ? null : parent.getAssignmentForOffset(offset);
            if (assignments != null && assignments.isEmpty()) {
                assignments = parent.getAssignments();
            }
            Snapshot snapshot = jsResult.getSnapshot();
            JsIndex jsIndex = JsIndex.get(snapshot.getSource().getFileObject());
            ArrayList<IndexResult> indexResults = new ArrayList<IndexResult>();
            if (assignments == null || assignments.isEmpty()) {
                fo = object.getFileObject();
                if (object.isDeclared()) {
                    if (fo != null) {
                        TokenSequence<? extends JsTokenId> ts;
                        if (!fo.equals((Object)snapshot.getSource().getFileObject()) || object.getDeclarationName() == null) return new DeclarationFinder.DeclarationLocation(fo, this.getDeclarationOffset(object));
                        int docOffset = LexUtilities.getLexerOffset(jsResult, this.getDeclarationOffset(object));
                        if (docOffset > -1 && (ts = LexUtilities.getTokenSequence(snapshot, caretOffset, this.language)) != null) {
                            int docTsOffset;
                            ts.move(offset);
                            if (ts.moveNext() && ((docTsOffset = LexUtilities.getLexerOffset(jsResult, ts.offset())) > docOffset || docOffset > docTsOffset + ts.token().length())) {
                                return new DeclarationFinder.DeclarationLocation(fo, docOffset);
                            }
                        }
                    }
                } else {
                    Collection<? extends IndexResult> items = JsIndex.get(fo).findByFqn(object.getFullyQualifiedName(), JsIndex.TERMS_BASIC_INFO);
                    indexResults.addAll(items);
                    DeclarationFinder.DeclarationLocation location = this.processIndexResult(indexResults);
                    if (location != null) {
                        return location;
                    }
                }
            } else {
                TokenSequence<? extends JsTokenId> ts;
                fo = object.getFileObject();
                if (object.isDeclared() && fo != null) {
                    if (!fo.equals((Object)snapshot.getSource().getFileObject())) return new DeclarationFinder.DeclarationLocation(fo, this.getDeclarationOffset(object));
                    int docOffset = LexUtilities.getLexerOffset(jsResult, this.getDeclarationOffset(object));
                    if (docOffset > -1) {
                        return new DeclarationFinder.DeclarationLocation(fo, docOffset);
                    }
                }
                if ((ts = LexUtilities.getTokenSequence(snapshot, caretOffset, this.language)) != null) {
                    ts.move(offset);
                    if (ts.moveNext() && ts.token().id() == JsTokenId.IDENTIFIER) {
                        String propertyName = ts.token().text().toString();
                        for (Type type : assignments) {
                            String fqn = this.getFQNFromType(type);
                            Collection<? extends IndexResult> items = this.findPropertyOfType(jsIndex, fqn, propertyName);
                            if (items.isEmpty()) {
                                Collection<? extends IndexResult> tmpItems = jsIndex.findByFqn(fqn, JsIndex.TERMS_BASIC_INFO);
                                for (IndexResult indexResult : tmpItems) {
                                    Collection<TypeUsage> tmpAssignments = IndexedElement.getAssignments(indexResult);
                                    for (Type tmpType : tmpAssignments) {
                                        items = this.findPropertyOfType(jsIndex, this.getFQNFromType(tmpType), propertyName);
                                        indexResults.addAll(items);
                                    }
                                }
                                continue;
                            }
                            indexResults.addAll(items);
                        }
                        DeclarationFinder.DeclarationLocation location = this.processIndexResult(indexResults);
                        if (location != null) {
                            return location;
                        }
                    }
                }
            }
        }
        for (DeclarationFinder finder : EditorExtender.getDefault().getDeclarationFinders()) {
            DeclarationFinder.DeclarationLocation loc = finder.findDeclaration(info, caretOffset);
            if (loc == null || loc == DeclarationFinder.DeclarationLocation.NONE) continue;
            return loc;
        }
        return DeclarationFinder.DeclarationLocation.NONE;
    }

    private int getDeclarationOffset(JsObject object) {
        return object.getDeclarationName() != null ? object.getDeclarationName().getOffsetRange().getStart() : object.getOffset();
    }

    private Collection<? extends IndexResult> findPropertyOfType(JsIndex jsIndex, String fqn, String propertyName) {
        Collection<? extends IndexResult> items = jsIndex.findByFqn(fqn + "." + propertyName, JsIndex.TERMS_BASIC_INFO);
        if (items.isEmpty()) {
            items = jsIndex.findByFqn(fqn + ".prototype." + propertyName, JsIndex.TERMS_BASIC_INFO);
        }
        return items;
    }

    private String getFQNFromType(Type type) {
        String fqn = type.getType();
        if (fqn.startsWith("@exp;")) {
            fqn = fqn.substring("@exp;".length());
        }
        if (fqn.contains("@pro;")) {
            fqn = fqn.replace("@pro;", ".");
        }
        return fqn;
    }

    private DeclarationFinder.DeclarationLocation processIndexResult(List<IndexResult> indexResults) {
        if (!indexResults.isEmpty()) {
            IndexResult iResult = indexResults.get(0);
            String value = iResult.getValue("offset");
            int offset = Integer.parseInt(value);
            HashSet<String> alreadyThere = new HashSet<String>();
            DeclarationFinder.DeclarationLocation location = new DeclarationFinder.DeclarationLocation(iResult.getFile(), offset, (ElementHandle)IndexedElement.create(iResult));
            alreadyThere.add(iResult.getFile().getPath() + offset);
            if (indexResults.size() > 1) {
                for (int i = 0; i < indexResults.size(); ++i) {
                    iResult = indexResults.get(i);
                    if (alreadyThere.contains(iResult.getFile().getPath() + offset)) continue;
                    location.addAlternative((DeclarationFinder.AlternativeLocation)new AlternativeLocationImpl(iResult));
                    alreadyThere.add(iResult.getFile().getPath() + offset);
                }
            }
            return location;
        }
        return null;
    }

    public OffsetRange getReferenceSpan(final Document doc, final int caretOffset) {
        if (doc == null) {
            return OffsetRange.NONE;
        }
        final OffsetRange[] value = new OffsetRange[1];
        doc.render(new Runnable(){

            @Override
            public void run() {
                TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence(doc, caretOffset, DeclarationFinderImpl.this.language);
                if (ts != null) {
                    ts.move(caretOffset);
                    if (ts.moveNext() && ts.token().id() == JsTokenId.IDENTIFIER) {
                        value[0] = new OffsetRange(ts.offset(), ts.offset() + ts.token().length());
                    }
                }
            }
        });
        if (value[0] != null) {
            return value[0];
        }
        for (DeclarationFinder finder : EditorExtender.getDefault().getDeclarationFinders()) {
            OffsetRange result = finder.getReferenceSpan(doc, caretOffset);
            if (result == null || result == OffsetRange.NONE) continue;
            return result;
        }
        return OffsetRange.NONE;
    }

    @SuppressWarnings(value={"EQ_COMPARETO_USE_OBJECT_EQUALS"})
    public static class AlternativeLocationImpl
    implements DeclarationFinder.AlternativeLocation {
        private final IndexResult iResult;
        private final int offset;
        private final DeclarationFinder.DeclarationLocation location;
        private final IndexedElement element;

        public AlternativeLocationImpl(IndexResult iResult) {
            this.iResult = iResult;
            String value = iResult.getValue("offset");
            this.offset = Integer.parseInt(value);
            this.location = new DeclarationFinder.DeclarationLocation(iResult.getFile(), this.offset);
            this.element = IndexedElement.create(iResult);
        }

        public ElementHandle getElement() {
            return this.element;
        }

        private String getStringLocation() {
            List asLines;
            int lineNumber = 0;
            int count = 0;
            try {
                asLines = this.element.getFileObject().asLines();
            }
            catch (IOException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                asLines = null;
            }
            if (asLines != null) {
                for (String line : asLines) {
                    ++lineNumber;
                    if ((count += line.length()) < this.offset) continue;
                    break;
                }
            }
            String result = this.iResult.getRelativePath();
            if (lineNumber > 0) {
                result = result + " : " + lineNumber;
            }
            return result;
        }

        public String getDisplayHtml(HtmlFormatter formatter) {
            formatter.appendText(this.getStringLocation());
            return formatter.getText();
        }

        public DeclarationFinder.DeclarationLocation getLocation() {
            return this.location;
        }

        public int compareTo(DeclarationFinder.AlternativeLocation o) {
            AlternativeLocationImpl ali = (AlternativeLocationImpl)o;
            return this.getStringLocation().compareTo(ali.getStringLocation());
        }
    }

}

