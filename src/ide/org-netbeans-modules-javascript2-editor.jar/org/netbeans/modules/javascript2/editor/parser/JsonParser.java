/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.codegen.Compiler
 *  jdk.nashorn.internal.ir.Block
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.FunctionNode$Kind
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.parser.JSONParser
 *  jdk.nashorn.internal.runtime.Context
 *  jdk.nashorn.internal.runtime.ErrorManager
 *  jdk.nashorn.internal.runtime.ParserException
 *  jdk.nashorn.internal.runtime.Source
 *  jdk.nashorn.internal.runtime.options.Options
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.parser;

import java.util.Collections;
import java.util.List;
import jdk.nashorn.internal.codegen.Compiler;
import jdk.nashorn.internal.ir.Block;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.parser.JSONParser;
import jdk.nashorn.internal.runtime.Context;
import jdk.nashorn.internal.runtime.ErrorManager;
import jdk.nashorn.internal.runtime.ParserException;
import jdk.nashorn.internal.runtime.Source;
import jdk.nashorn.internal.runtime.options.Options;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.parser.JsErrorManager;
import org.netbeans.modules.javascript2.editor.parser.SanitizingParser;
import org.netbeans.modules.parsing.api.Snapshot;

public class JsonParser
extends SanitizingParser {
    public JsonParser() {
        super(JsTokenId.jsonLanguage());
    }

    @Override
    protected String getDefaultScriptName() {
        return "json.json";
    }

    @Override
    protected FunctionNode parseSource(Snapshot snapshot, String name, String text, JsErrorManager errorManager) throws Exception {
        Source source = new Source(name, text);
        Options options = new Options("nashorn");
        options.process(new String[]{"--parse-only=true", "--empty-statements=true", "--debug-lines=false"});
        errorManager.setLimit(0);
        Context nashornContext = new Context(options, (ErrorManager)errorManager, JsonParser.class.getClassLoader());
        Compiler compiler = Compiler.compiler((Source)source, (Context)nashornContext);
        JSONParser parser = new JSONParser(source, (ErrorManager)errorManager, nashornContext._strict);
        Node objectNode = null;
        try {
            objectNode = parser.parse();
        }
        catch (ParserException ex) {
            errorManager.error(ex);
        }
        FunctionNode node = null;
        if (objectNode != null) {
            node = new FunctionNode(source, 0, text.length(), compiler, null, null, "runScript");
            node.setKind(FunctionNode.Kind.SCRIPT);
            node.setStatements(Collections.singletonList(objectNode));
            node.setIdent(new IdentNode(source, objectNode.getToken(), 0, node.getName()));
        }
        return node;
    }

    @Override
    protected String getMimeType() {
        return "text/x-json";
    }
}

