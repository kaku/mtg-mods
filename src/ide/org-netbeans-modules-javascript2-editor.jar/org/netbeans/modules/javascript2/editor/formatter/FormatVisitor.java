/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.AccessNode
 *  jdk.nashorn.internal.ir.BinaryNode
 *  jdk.nashorn.internal.ir.Block
 *  jdk.nashorn.internal.ir.CallNode
 *  jdk.nashorn.internal.ir.CaseNode
 *  jdk.nashorn.internal.ir.CatchNode
 *  jdk.nashorn.internal.ir.DoWhileNode
 *  jdk.nashorn.internal.ir.ExecuteNode
 *  jdk.nashorn.internal.ir.ForNode
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.FunctionNode$Kind
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.IfNode
 *  jdk.nashorn.internal.ir.LiteralNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.ObjectNode
 *  jdk.nashorn.internal.ir.PropertyNode
 *  jdk.nashorn.internal.ir.ReferenceNode
 *  jdk.nashorn.internal.ir.SwitchNode
 *  jdk.nashorn.internal.ir.TernaryNode
 *  jdk.nashorn.internal.ir.TryNode
 *  jdk.nashorn.internal.ir.UnaryNode
 *  jdk.nashorn.internal.ir.VarNode
 *  jdk.nashorn.internal.ir.WhileNode
 *  jdk.nashorn.internal.ir.WithNode
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  jdk.nashorn.internal.parser.Token
 *  jdk.nashorn.internal.parser.TokenType
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 */
package org.netbeans.modules.javascript2.editor.formatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jdk.nashorn.internal.ir.AccessNode;
import jdk.nashorn.internal.ir.BinaryNode;
import jdk.nashorn.internal.ir.Block;
import jdk.nashorn.internal.ir.CallNode;
import jdk.nashorn.internal.ir.CaseNode;
import jdk.nashorn.internal.ir.CatchNode;
import jdk.nashorn.internal.ir.DoWhileNode;
import jdk.nashorn.internal.ir.ExecuteNode;
import jdk.nashorn.internal.ir.ForNode;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.IfNode;
import jdk.nashorn.internal.ir.LiteralNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.ObjectNode;
import jdk.nashorn.internal.ir.PropertyNode;
import jdk.nashorn.internal.ir.ReferenceNode;
import jdk.nashorn.internal.ir.SwitchNode;
import jdk.nashorn.internal.ir.TernaryNode;
import jdk.nashorn.internal.ir.TryNode;
import jdk.nashorn.internal.ir.UnaryNode;
import jdk.nashorn.internal.ir.VarNode;
import jdk.nashorn.internal.ir.WhileNode;
import jdk.nashorn.internal.ir.WithNode;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import jdk.nashorn.internal.parser.TokenType;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.formatter.FormatToken;
import org.netbeans.modules.javascript2.editor.formatter.FormatTokenStream;

public class FormatVisitor
extends NodeVisitor {
    private static final Set<TokenType> UNARY_TYPES = EnumSet.noneOf(TokenType.class);
    private final TokenSequence<? extends JsTokenId> ts;
    private final FormatTokenStream tokenStream;
    private final int formatFinish;
    private final Set<Block> caseNodes = new HashSet<Block>();

    public FormatVisitor(FormatTokenStream tokenStream, TokenSequence<? extends JsTokenId> ts, int formatFinish) {
        this.ts = ts;
        this.tokenStream = tokenStream;
        this.formatFinish = formatFinish;
    }

    public Node enter(Block block) {
        boolean isCaseNode = false;
        if (block instanceof FunctionNode || FormatVisitor.isScript((Node)block) || this.caseNodes.contains((Object)block) || !this.isVirtual(block)) {
            if (this.caseNodes.contains((Object)block)) {
                this.caseNodes.remove((Object)block);
                isCaseNode = true;
                this.handleCaseBlock(block);
            } else if (FormatVisitor.isScript((Node)block)) {
                this.handleBlockContent(block);
            } else {
                this.handleStandardBlock(block);
            }
        }
        if (block instanceof FunctionNode || FormatVisitor.isScript((Node)block) || isCaseNode || !this.isVirtual(block)) {
            return null;
        }
        return super.enter(block);
    }

    public Node leave(Block block) {
        if (block instanceof FunctionNode || FormatVisitor.isScript((Node)block) || !this.isVirtual(block)) {
            return null;
        }
        return super.leave(block);
    }

    public Node enter(CaseNode caseNode) {
        this.caseNodes.add(caseNode.getBody());
        return super.enter(caseNode);
    }

    public Node leave(CaseNode caseNode) {
        this.caseNodes.remove((Object)caseNode.getBody());
        return super.leave(caseNode);
    }

    public Node enter(WhileNode whileNode) {
        this.markSpacesWithinParentheses((Node)whileNode, this.getStart((Node)whileNode), this.getStart((Node)whileNode.getBody()), FormatToken.Kind.AFTER_WHILE_PARENTHESIS, FormatToken.Kind.BEFORE_WHILE_PARENTHESIS);
        this.markSpacesBeforeBrace(whileNode.getBody(), FormatToken.Kind.BEFORE_WHILE_BRACE);
        if (this.handleWhile(whileNode, FormatToken.Kind.AFTER_WHILE_START)) {
            return null;
        }
        return super.enter(whileNode);
    }

    public Node enter(DoWhileNode doWhileNode) {
        FormatToken beforeWhile;
        int leftStart;
        Block body = doWhileNode.getBody();
        if (this.isVirtual(body)) {
            List statements = body.getStatements();
            leftStart = this.getFinish((Node)statements.get(statements.size() - 1));
        } else {
            leftStart = this.getFinish((Node)doWhileNode.getBody());
        }
        this.markSpacesWithinParentheses((Node)doWhileNode, leftStart, this.getFinish((Node)doWhileNode), FormatToken.Kind.AFTER_WHILE_PARENTHESIS, FormatToken.Kind.BEFORE_WHILE_PARENTHESIS);
        this.markSpacesBeforeBrace(doWhileNode.getBody(), FormatToken.Kind.BEFORE_DO_BRACE);
        FormatToken whileToken = this.getPreviousToken(this.getFinish((Node)doWhileNode), JsTokenId.KEYWORD_WHILE);
        if (whileToken != null && (beforeWhile = whileToken.previous()) != null) {
            FormatVisitor.appendToken(beforeWhile, FormatToken.forFormat(FormatToken.Kind.BEFORE_WHILE_KEYWORD));
        }
        if (this.handleWhile((WhileNode)doWhileNode, FormatToken.Kind.AFTER_DO_START)) {
            return null;
        }
        return super.enter(doWhileNode);
    }

    public Node enter(ForNode forNode) {
        this.markSpacesWithinParentheses((Node)forNode, this.getStart((Node)forNode), this.getStart((Node)forNode.getBody()), FormatToken.Kind.AFTER_FOR_PARENTHESIS, FormatToken.Kind.BEFORE_FOR_PARENTHESIS);
        this.markSpacesBeforeBrace(forNode.getBody(), FormatToken.Kind.BEFORE_FOR_BRACE);
        if (!forNode.isForEach() && !forNode.isForIn()) {
            Node init = forNode.getInit();
            Node test = forNode.getTest();
            FormatToken formatToken = null;
            formatToken = init != null ? this.getNextToken(this.getFinish(init), JsTokenId.OPERATOR_SEMICOLON) : this.getNextToken(this.getStart((Node)forNode), JsTokenId.OPERATOR_SEMICOLON, this.getStart((Node)forNode.getBody()));
            if (formatToken != null && test != null) {
                FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.BEFORE_FOR_TEST));
            }
            if (test != null) {
                formatToken = this.getNextToken(this.getFinish(forNode.getTest()), JsTokenId.OPERATOR_SEMICOLON);
            } else {
                int start = formatToken != null ? formatToken.getOffset() + 1 : this.getStart((Node)forNode);
                formatToken = this.getNextToken(start, JsTokenId.OPERATOR_SEMICOLON, this.getStart((Node)forNode.getBody()));
            }
            if (formatToken != null && forNode.getModify() != null) {
                FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.BEFORE_FOR_MODIFY));
            }
        }
        if (this.handleWhile((WhileNode)forNode, FormatToken.Kind.AFTER_FOR_START)) {
            return null;
        }
        return super.enter(forNode);
    }

    public Node enter(IfNode ifNode) {
        ifNode.getTest().accept((NodeVisitor)this);
        this.markSpacesWithinParentheses((Node)ifNode, this.getStart((Node)ifNode), this.getStart((Node)ifNode.getPass()), FormatToken.Kind.AFTER_IF_PARENTHESIS, FormatToken.Kind.BEFORE_IF_PARENTHESIS);
        Block body = ifNode.getPass();
        this.markSpacesBeforeBrace(body, FormatToken.Kind.BEFORE_IF_BRACE);
        if (this.isVirtual(body)) {
            this.handleVirtualBlock(body, FormatToken.Kind.AFTER_IF_START);
        } else {
            this.enter(body);
        }
        body = ifNode.getFail();
        if (body != null) {
            if (this.isVirtual(body)) {
                List statements = body.getStatements();
                if (!statements.isEmpty() && statements.get(0) instanceof IfNode) {
                    this.handleVirtualBlock(body, FormatToken.Kind.ELSE_IF_INDENTATION_INC, FormatToken.Kind.ELSE_IF_INDENTATION_DEC, FormatToken.Kind.ELSE_IF_AFTER_BLOCK_START);
                } else {
                    this.markSpacesBeforeBrace(body, FormatToken.Kind.BEFORE_ELSE_BRACE);
                    this.handleVirtualBlock(body, FormatToken.Kind.AFTER_ELSE_START);
                }
            } else {
                this.markSpacesBeforeBrace(body, FormatToken.Kind.BEFORE_ELSE_BRACE);
                this.enter(body);
            }
        }
        return null;
    }

    public Node leave(IfNode ifNode) {
        return null;
    }

    public Node enter(WithNode withNode) {
        this.markSpacesWithinParentheses((Node)withNode, this.getStart((Node)withNode), this.getStart((Node)withNode.getBody()), FormatToken.Kind.AFTER_WITH_PARENTHESIS, FormatToken.Kind.BEFORE_WITH_PARENTHESIS);
        Block body = withNode.getBody();
        this.markSpacesBeforeBrace(body, FormatToken.Kind.BEFORE_WITH_BRACE);
        if (this.isVirtual(body)) {
            this.handleVirtualBlock(body, FormatToken.Kind.AFTER_WITH_START);
            return null;
        }
        return super.enter(withNode);
    }

    public Node enter(FunctionNode functionNode) {
        int start;
        FormatToken leftParen;
        this.enter((Block)functionNode);
        if (!FormatVisitor.isScript((Node)functionNode) && (leftParen = this.getNextToken(start = FormatVisitor.getFunctionStart(functionNode), JsTokenId.BRACKET_LEFT_PAREN)) != null) {
            FormatToken leftBrace;
            FormatToken rightBrace;
            FormatToken previous = leftParen.previous();
            if (previous != null) {
                FormatVisitor.appendToken(previous, FormatToken.forFormat(functionNode.isAnonymous() ? FormatToken.Kind.BEFORE_ANONYMOUS_FUNCTION_DECLARATION : FormatToken.Kind.BEFORE_FUNCTION_DECLARATION));
            }
            FormatToken mark = leftParen.next();
            assert (mark != null && mark.getKind() == FormatToken.Kind.AFTER_LEFT_PARENTHESIS);
            this.tokenStream.removeToken(mark);
            FormatToken rightParen = this.getPreviousToken(this.getStart((Node)functionNode), JsTokenId.BRACKET_RIGHT_PAREN, leftParen.getOffset());
            if (rightParen != null) {
                previous = rightParen.previous();
                assert (previous != null && previous.getKind() == FormatToken.Kind.BEFORE_RIGHT_PARENTHESIS);
                this.tokenStream.removeToken(previous);
            }
            if ((leftBrace = this.getNextToken(this.getStart((Node)functionNode), JsTokenId.BRACKET_LEFT_CURLY, this.getFinish((Node)functionNode))) != null && (previous = leftBrace.previous()) != null) {
                FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_FUNCTION_DECLARATION_BRACE));
            }
            if (!functionNode.getParameters().isEmpty()) {
                FormatVisitor.appendToken(leftParen, FormatToken.forFormat(FormatToken.Kind.AFTER_FUNCTION_DECLARATION_PARENTHESIS));
                if (rightParen != null && (previous = rightParen.previous()) != null) {
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_FUNCTION_DECLARATION_PARENTHESIS));
                }
            }
            for (IdentNode param : functionNode.getParameters()) {
                FormatToken beforeIdent;
                FormatToken ident = this.getNextToken(this.getStart((Node)param), JsTokenId.IDENTIFIER);
                if (ident == null || (beforeIdent = ident.previous()) == null) continue;
                FormatVisitor.appendToken(beforeIdent, FormatToken.forFormat(FormatToken.Kind.BEFORE_FUNCTION_DECLARATION_PARAMETER));
            }
            if (functionNode.isStatement() && (rightBrace = this.getPreviousToken(this.getFinish((Node)functionNode), JsTokenId.BRACKET_RIGHT_CURLY, leftBrace != null ? leftBrace.getOffset() : start)) != null) {
                FormatVisitor.appendToken(rightBrace, FormatToken.forFormat(FormatToken.Kind.AFTER_STATEMENT));
            }
        }
        return null;
    }

    public Node leave(FunctionNode functionNode) {
        this.leave((Block)functionNode);
        return null;
    }

    public Node enter(CallNode callNode) {
        FormatToken leftBrace = this.getNextToken(this.getFinish(callNode.getFunction()), JsTokenId.BRACKET_LEFT_PAREN, this.getFinish((Node)callNode));
        if (leftBrace != null) {
            FormatToken previous = leftBrace.previous();
            if (previous != null) {
                FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_FUNCTION_CALL));
            }
            FormatToken mark = leftBrace.next();
            assert (mark != null && mark.getKind() == FormatToken.Kind.AFTER_LEFT_PARENTHESIS);
            this.tokenStream.removeToken(mark);
            FormatToken rightBrace = this.getPreviousToken(this.getFinish((Node)callNode) - 1, JsTokenId.BRACKET_RIGHT_PAREN, this.getStart((Node)callNode));
            if (rightBrace != null && (previous = FormatVisitor.findVirtualToken(rightBrace, FormatToken.Kind.BEFORE_RIGHT_PARENTHESIS, true)) != null) {
                this.tokenStream.removeToken(previous);
            }
            if (!callNode.getArgs().isEmpty()) {
                FormatVisitor.appendToken(leftBrace, FormatToken.forFormat(FormatToken.Kind.AFTER_FUNCTION_CALL_PARENTHESIS));
                if (rightBrace != null && (previous = rightBrace.previous()) != null) {
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_FUNCTION_CALL_PARENTHESIS));
                }
            }
            for (Node arg : callNode.getArgs()) {
                FormatToken beforeArg;
                FormatToken argToken = this.getNextToken(this.getStart(arg), null);
                if (argToken == null || (beforeArg = argToken.previous()) == null) continue;
                FormatVisitor.appendToken(beforeArg, FormatToken.forFormat(FormatToken.Kind.BEFORE_FUNCTION_CALL_ARGUMENT));
            }
        }
        this.handleFunctionCallChain(callNode);
        return super.enter(callNode);
    }

    public Node enter(ObjectNode objectNode) {
        FormatToken formatToken = this.getPreviousToken(this.getStart((Node)objectNode), JsTokenId.BRACKET_LEFT_CURLY, true);
        if (formatToken != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_INC));
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_OBJECT_START));
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_LEFT_BRACE));
            FormatToken previous = formatToken.previous();
            if (previous != null) {
                FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_OBJECT));
            }
        }
        int objectFinish = this.getFinish((Node)objectNode);
        for (Node property : objectNode.getElements()) {
            property.accept((NodeVisitor)this);
            PropertyNode propertyNode = (PropertyNode)property;
            if (propertyNode.getGetter() != null) {
                ReferenceNode getter = (ReferenceNode)propertyNode.getGetter();
                this.markPropertyFinish(this.getFinish((Node)getter.getReference()), objectFinish, false);
            }
            if (propertyNode.getSetter() != null) {
                ReferenceNode setter = (ReferenceNode)propertyNode.getSetter();
                this.markPropertyFinish(this.getFinish((Node)setter.getReference()), objectFinish, false);
            }
            this.markPropertyFinish(this.getFinish(property), objectFinish, true);
        }
        formatToken = this.getPreviousNonWhiteToken(this.getFinish((Node)objectNode) - 1, this.getStart((Node)objectNode), JsTokenId.BRACKET_RIGHT_CURLY, true);
        if (formatToken != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.BEFORE_RIGHT_BRACE));
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.BEFORE_OBJECT_END));
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_DEC));
        }
        return null;
    }

    public Node enter(PropertyNode propertyNode) {
        FormatToken colon = this.getNextToken(this.getFinish(propertyNode.getKey()), JsTokenId.OPERATOR_COLON, this.getFinish((Node)propertyNode));
        if (colon != null) {
            FormatVisitor.appendToken(colon, FormatToken.forFormat(FormatToken.Kind.AFTER_PROPERTY_OPERATOR));
            FormatToken before = colon.previous();
            if (before != null) {
                FormatVisitor.appendTokenAfterLastVirtual(before, FormatToken.forFormat(FormatToken.Kind.BEFORE_PROPERTY_OPERATOR));
            }
        }
        return super.enter(propertyNode);
    }

    public Node enter(SwitchNode switchNode) {
        this.markSpacesWithinParentheses(switchNode);
        this.markSpacesBeforeBrace(switchNode);
        FormatToken formatToken = this.getNextToken(this.getStart((Node)switchNode), JsTokenId.BRACKET_LEFT_CURLY, true);
        if (formatToken != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_INC));
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_BLOCK_START));
        }
        ArrayList<CaseNode> nodes = new ArrayList<CaseNode>(switchNode.getCases());
        if (switchNode.getDefaultCase() != null) {
            nodes.add(switchNode.getDefaultCase());
        }
        for (CaseNode caseNode : nodes) {
            int start = this.getStart((Node)caseNode.getBody());
            formatToken = this.getPreviousToken(start, JsTokenId.OPERATOR_COLON);
            if (formatToken == null) continue;
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_CASE));
        }
        formatToken = this.getPreviousNonWhiteToken(this.getFinish((Node)switchNode), this.getStart((Node)switchNode), JsTokenId.BRACKET_RIGHT_CURLY, true);
        if (formatToken != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_DEC));
        }
        return super.enter(switchNode);
    }

    public Node enter(UnaryNode unaryNode) {
        TokenType type = unaryNode.tokenType();
        if (UNARY_TYPES.contains((Object)type)) {
            if (TokenType.DECPOSTFIX.equals((Object)type) || TokenType.INCPOSTFIX.equals((Object)type)) {
                FormatToken formatToken = this.getPreviousToken(this.getFinish((Node)unaryNode), TokenType.DECPOSTFIX.equals((Object)type) ? JsTokenId.OPERATOR_DECREMENT : JsTokenId.OPERATOR_INCREMENT);
                if (formatToken != null && (formatToken = formatToken.previous()) != null) {
                    FormatVisitor.appendToken(formatToken, FormatToken.forFormat(FormatToken.Kind.BEFORE_UNARY_OPERATOR));
                }
            } else {
                FormatToken formatToken = this.getNextToken(this.getStart((Node)unaryNode), null);
                if (formatToken != null) {
                    if (TokenType.ADD.equals((Object)type) || TokenType.SUB.equals((Object)type)) {
                        assert (formatToken.getId() == JsTokenId.OPERATOR_PLUS || formatToken.getId() == JsTokenId.OPERATOR_MINUS);
                        FormatToken toRemove = FormatVisitor.findVirtualToken(formatToken, FormatToken.Kind.BEFORE_BINARY_OPERATOR, true);
                        assert (toRemove != null && toRemove.getKind() == FormatToken.Kind.BEFORE_BINARY_OPERATOR);
                        this.tokenStream.removeToken(toRemove);
                        toRemove = FormatVisitor.findVirtualToken(formatToken, FormatToken.Kind.BEFORE_BINARY_OPERATOR_WRAP, true);
                        assert (toRemove != null && toRemove.getKind() == FormatToken.Kind.BEFORE_BINARY_OPERATOR_WRAP);
                        this.tokenStream.removeToken(toRemove);
                        toRemove = FormatVisitor.findVirtualToken(formatToken, FormatToken.Kind.AFTER_BINARY_OPERATOR, false);
                        assert (toRemove != null && toRemove.getKind() == FormatToken.Kind.AFTER_BINARY_OPERATOR);
                        this.tokenStream.removeToken(toRemove);
                        toRemove = FormatVisitor.findVirtualToken(formatToken, FormatToken.Kind.AFTER_BINARY_OPERATOR_WRAP, false);
                        assert (toRemove != null && toRemove.getKind() == FormatToken.Kind.AFTER_BINARY_OPERATOR_WRAP);
                        this.tokenStream.removeToken(toRemove);
                    }
                    FormatVisitor.appendToken(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_UNARY_OPERATOR));
                }
            }
        }
        return super.enter(unaryNode);
    }

    public Node enter(TernaryNode ternaryNode) {
        int start = this.getStart(ternaryNode.rhs());
        FormatToken question = this.getPreviousToken(start, JsTokenId.OPERATOR_TERNARY);
        if (question != null) {
            FormatToken previous = question.previous();
            if (previous != null) {
                FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_TERNARY_OPERATOR));
                FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_TERNARY_OPERATOR_WRAP));
            }
            FormatVisitor.appendToken(question, FormatToken.forFormat(FormatToken.Kind.AFTER_TERNARY_OPERATOR));
            FormatVisitor.appendToken(question, FormatToken.forFormat(FormatToken.Kind.AFTER_TERNARY_OPERATOR_WRAP));
            FormatToken colon = this.getPreviousToken(this.getStart(ternaryNode.third()), JsTokenId.OPERATOR_COLON);
            if (colon != null) {
                previous = colon.previous();
                if (previous != null) {
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_TERNARY_OPERATOR));
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_TERNARY_OPERATOR_WRAP));
                }
                FormatVisitor.appendToken(colon, FormatToken.forFormat(FormatToken.Kind.AFTER_TERNARY_OPERATOR));
                FormatVisitor.appendToken(colon, FormatToken.forFormat(FormatToken.Kind.AFTER_TERNARY_OPERATOR_WRAP));
            }
        }
        return super.enter(ternaryNode);
    }

    public Node enter(CatchNode catchNode) {
        this.markSpacesWithinParentheses((Node)catchNode, this.getStart((Node)catchNode), this.getStart((Node)catchNode.getBody()), FormatToken.Kind.AFTER_CATCH_PARENTHESIS, FormatToken.Kind.BEFORE_CATCH_PARENTHESIS);
        this.markSpacesBeforeBrace(catchNode.getBody(), FormatToken.Kind.BEFORE_CATCH_BRACE);
        return super.enter(catchNode);
    }

    public Node enter(TryNode tryNode) {
        this.markSpacesBeforeBrace(tryNode.getBody(), FormatToken.Kind.BEFORE_TRY_BRACE);
        Block finallyBody = tryNode.getFinallyBody();
        if (finallyBody != null) {
            this.markSpacesBeforeBrace(tryNode.getFinallyBody(), FormatToken.Kind.BEFORE_FINALLY_BRACE);
        }
        return super.enter(tryNode);
    }

    public Node enter(LiteralNode literalNode) {
        Object value = literalNode.getValue();
        if (value instanceof Node[]) {
            Node[] items;
            int finish;
            int start = this.getStart((Node)literalNode);
            FormatToken leftBracket = this.getNextToken(start, JsTokenId.BRACKET_LEFT_BRACKET, finish = this.getFinish((Node)literalNode));
            if (leftBracket != null) {
                FormatToken previous;
                FormatVisitor.appendToken(leftBracket, FormatToken.forFormat(FormatToken.Kind.AFTER_ARRAY_LITERAL_START));
                FormatVisitor.appendToken(leftBracket, FormatToken.forFormat(FormatToken.Kind.AFTER_ARRAY_LITERAL_BRACKET));
                FormatVisitor.appendToken(leftBracket, FormatToken.forFormat(FormatToken.Kind.INDENTATION_INC));
                FormatToken rightBracket = this.getPreviousToken(finish - 1, JsTokenId.BRACKET_RIGHT_BRACKET, start + 1);
                if (rightBracket != null && (previous = rightBracket.previous()) != null) {
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_ARRAY_LITERAL_END));
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_ARRAY_LITERAL_BRACKET));
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.INDENTATION_DEC));
                }
            }
            if ((items = literalNode.getArray()) != null && items.length > 0) {
                int prevItemFinish = start;
                for (int i = 1; i < items.length; ++i) {
                    FormatToken comma;
                    Node prevItem = items[i - 1];
                    if (prevItem != null) {
                        prevItemFinish = this.getFinish(prevItem);
                    }
                    if ((comma = this.getNextToken(prevItemFinish, JsTokenId.OPERATOR_COMMA, finish)) == null) continue;
                    prevItemFinish = comma.getOffset();
                    FormatVisitor.appendTokenAfterLastVirtual(comma, FormatToken.forFormat(FormatToken.Kind.AFTER_ARRAY_LITERAL_ITEM));
                }
            }
        }
        return super.enter(literalNode);
    }

    public Node enter(VarNode varNode) {
        FormatToken formatToken;
        int finish = this.getFinish((Node)varNode) - 1;
        Token nextToken = this.getNextNonEmptyToken(finish);
        if (nextToken != null && nextToken.id() == JsTokenId.OPERATOR_COMMA && (formatToken = this.tokenStream.getToken(this.ts.offset())) != null) {
            FormatToken next = formatToken.next();
            assert (next != null && next.getKind() == FormatToken.Kind.AFTER_COMMA);
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_VAR_DECLARATION));
        }
        return super.enter(varNode);
    }

    private void handleFunctionCallChain(CallNode callNode) {
        int finish;
        CallNode chained;
        Node base;
        FormatToken formatToken;
        Node function = callNode.getFunction();
        if (function instanceof AccessNode && (base = ((AccessNode)function).getBase()) instanceof CallNode && (formatToken = this.getNextToken(finish = this.getFinish((Node)(chained = (CallNode)base)), JsTokenId.OPERATOR_DOT)) != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_CHAIN_CALL_DOT));
            formatToken = formatToken.previous();
            if (formatToken != null) {
                FormatVisitor.appendToken(formatToken, FormatToken.forFormat(FormatToken.Kind.BEFORE_CHAIN_CALL_DOT));
            }
        }
    }

    private boolean handleWhile(WhileNode whileNode, FormatToken.Kind afterStart) {
        Block body = whileNode.getBody();
        if (this.isVirtual(body)) {
            this.handleVirtualBlock(body, afterStart);
            return true;
        }
        return false;
    }

    private void handleStandardBlock(Block block) {
        this.handleBlockContent(block);
        FormatToken formatToken = this.getPreviousToken(this.getStart((Node)block), JsTokenId.BRACKET_LEFT_CURLY, true);
        if (formatToken != null && !FormatVisitor.isScript((Node)block)) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_INC));
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_BLOCK_START));
        }
        if ((formatToken = this.getPreviousNonWhiteToken(this.getFinish((Node)block) - 1, this.getStart((Node)block), JsTokenId.BRACKET_RIGHT_CURLY, true)) != null && !FormatVisitor.isScript((Node)block)) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_DEC));
        }
    }

    private void handleCaseBlock(Block block) {
        Node node;
        this.handleBlockContent(block);
        List nodes = block.getStatements();
        if (nodes.size() == 1 && (node = (Node)nodes.get(0)) instanceof ExecuteNode && (node = ((ExecuteNode)node).getExpression()) instanceof Block) {
            return;
        }
        FormatToken formatToken = this.getPreviousToken(this.getStart((Node)block), JsTokenId.OPERATOR_COLON, true);
        if (formatToken != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_INC));
        }
        if ((formatToken = this.getCaseBlockEndToken(block)) != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.INDENTATION_DEC));
        }
    }

    private void handleVirtualBlock(Block block, FormatToken.Kind afterBlock) {
        this.handleVirtualBlock(block, FormatToken.Kind.INDENTATION_INC, FormatToken.Kind.INDENTATION_DEC, afterBlock);
    }

    private void handleVirtualBlock(Block block, FormatToken.Kind indentationInc, FormatToken.Kind indentationDec, FormatToken.Kind afterBlock) {
        assert (this.isVirtual(block));
        boolean assertsEnabled = false;
        if (!$assertionsDisabled) {
            assertsEnabled = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (assertsEnabled && block.getStatements().size() > 1) {
            int count = 0;
            for (Node node : block.getStatements()) {
                if (node instanceof VarNode) continue;
                ++count;
            }
            assert (count <= 1);
        }
        if (block.getStatements().isEmpty()) {
            return;
        }
        this.handleBlockContent(block);
        Node statement = (Node)block.getStatements().get(0);
        Token token = this.getPreviousNonEmptyToken(this.getStart(statement));
        if (statement instanceof VarNode && token.id() == JsTokenId.KEYWORD_VAR) {
            token = this.getPreviousNonEmptyToken(this.ts.offset());
        }
        if (token != null) {
            FormatToken formatToken = this.tokenStream.getToken(this.ts.offset());
            if (!FormatVisitor.isScript((Node)block)) {
                if (formatToken == null && this.ts.offset() <= this.formatFinish) {
                    formatToken = this.tokenStream.getTokens().get(0);
                }
                if (formatToken != null) {
                    FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(indentationInc));
                    if (afterBlock != null) {
                        FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(afterBlock));
                    }
                }
            }
        }
        int finish = this.getFinish(statement);
        FormatToken formatToken = this.getPreviousToken(statement.getStart() < finish ? finish - 1 : finish, null, true);
        if (formatToken != null && !FormatVisitor.isScript((Node)block)) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(indentationDec));
        }
    }

    private void handleBlockContent(Block block) {
        if (block instanceof FunctionNode) {
            for (FunctionNode function : ((FunctionNode)block).getFunctions()) {
                function.accept((NodeVisitor)this);
            }
        }
        List statements = block.getStatements();
        for (int i = 0; i < statements.size(); ++i) {
            FormatToken formatToken;
            Node statement = (Node)statements.get(i);
            statement.accept((NodeVisitor)this);
            int start = this.getStart(statement);
            int finish = this.getFinish(statement);
            if (statement instanceof VarNode) {
                int index = i + 1;
                Node lastVarNode = statement;
                while (i + 1 < statements.size()) {
                    Node next;
                    if (!((next = (Node)statements.get(++i)) instanceof VarNode)) {
                        --i;
                        break;
                    }
                    Token token = this.getPreviousNonEmptyToken(this.getStart(next));
                    if (token != null && JsTokenId.KEYWORD_VAR == token.id()) {
                        --i;
                        break;
                    }
                    lastVarNode = next;
                }
                for (int j = index; j < i + 1; ++j) {
                    Node skipped = (Node)statements.get(j);
                    skipped.accept((NodeVisitor)this);
                }
                finish = this.getFinish(lastVarNode);
            }
            if ((formatToken = this.getPreviousToken(start < finish ? finish - 1 : finish, null)) == null) continue;
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_STATEMENT), true);
        }
    }

    private void markSpacesWithinParentheses(SwitchNode node) {
        int leftStart = this.getStart((Node)node);
        FormatToken token = this.getNextToken(leftStart, JsTokenId.BRACKET_LEFT_CURLY, this.getFinish((Node)node));
        if (token != null) {
            this.markSpacesWithinParentheses((Node)node, leftStart, token.getOffset(), FormatToken.Kind.AFTER_SWITCH_PARENTHESIS, FormatToken.Kind.BEFORE_SWITCH_PARENTHESIS);
        }
    }

    private void markSpacesBeforeBrace(SwitchNode node) {
        FormatToken previous;
        int leftStart = this.getStart((Node)node);
        FormatToken token = this.getNextToken(leftStart, JsTokenId.BRACKET_LEFT_CURLY, this.getFinish((Node)node));
        if (token != null && (previous = token.previous()) != null) {
            FormatVisitor.appendToken(previous, FormatToken.forFormat(FormatToken.Kind.BEFORE_SWITCH_BRACE));
        }
    }

    private void markSpacesWithinParentheses(Node outerNode, int leftStart, int rightStart, FormatToken.Kind leftMark, FormatToken.Kind rightMark) {
        FormatToken leftParen = this.getNextToken(leftStart, JsTokenId.BRACKET_LEFT_PAREN, this.getFinish(outerNode));
        if (leftParen != null) {
            FormatToken mark = leftParen.next();
            assert (mark != null && mark.getKind() == FormatToken.Kind.AFTER_LEFT_PARENTHESIS);
            this.tokenStream.removeToken(mark);
            FormatVisitor.appendToken(leftParen, FormatToken.forFormat(leftMark));
            FormatToken rightParen = this.getPreviousToken(rightStart, JsTokenId.BRACKET_RIGHT_PAREN, this.getStart(outerNode));
            if (rightParen != null) {
                FormatToken previous = rightParen.previous();
                assert (previous != null && previous.getKind() == FormatToken.Kind.BEFORE_RIGHT_PARENTHESIS);
                this.tokenStream.removeToken(previous);
                previous = rightParen.previous();
                if (previous != null) {
                    FormatVisitor.appendToken(previous, FormatToken.forFormat(rightMark));
                }
            }
        }
    }

    private void markSpacesBeforeBrace(Block block, FormatToken.Kind mark) {
        FormatToken previous;
        FormatToken brace = this.getPreviousToken(this.getStart((Node)block), null, this.getStart((Node)block) - 1);
        if (brace != null && (previous = brace.previous()) != null) {
            FormatVisitor.appendToken(previous, FormatToken.forFormat(mark));
        }
    }

    private void markPropertyFinish(int finish, int objectFinish, boolean checkDuplicity) {
        FormatToken formatToken = this.getNextToken(finish, JsTokenId.OPERATOR_COMMA, objectFinish);
        if (formatToken != null) {
            FormatVisitor.appendTokenAfterLastVirtual(formatToken, FormatToken.forFormat(FormatToken.Kind.AFTER_PROPERTY), checkDuplicity);
        }
    }

    private FormatToken getNextToken(int offset, JsTokenId expected) {
        return this.getToken(offset, expected, false, false, null);
    }

    private FormatToken getNextToken(int offset, JsTokenId expected, int stop) {
        return this.getToken(offset, expected, false, false, stop);
    }

    private FormatToken getNextToken(int offset, JsTokenId expected, boolean startFallback) {
        return this.getToken(offset, expected, false, startFallback, null);
    }

    private FormatToken getPreviousToken(int offset, JsTokenId expected) {
        return this.getPreviousToken(offset, expected, false);
    }

    private FormatToken getPreviousToken(int offset, JsTokenId expected, int stop) {
        return this.getToken(offset, expected, true, false, stop);
    }

    private FormatToken getPreviousToken(int offset, JsTokenId expected, boolean startFallback) {
        return this.getToken(offset, expected, true, startFallback, null);
    }

    private FormatToken getToken(int offset, JsTokenId expected, boolean backward, boolean startFallback, Integer stopMark) {
        this.ts.move(offset);
        if (!this.ts.moveNext() && !this.ts.movePrevious()) {
            return null;
        }
        Token token = this.ts.token();
        if (expected != null) {
            while (expected != token.id() && (stopMark == null || stopMark >= this.ts.offset() && !backward || stopMark <= this.ts.offset() && backward) && (backward && this.ts.movePrevious() || !backward && this.ts.moveNext())) {
                token = this.ts.token();
            }
            if (expected != token.id()) {
                return null;
            }
        }
        if (stopMark != null && (this.ts.offset() > stopMark && !backward || this.ts.offset() < stopMark && backward)) {
            return null;
        }
        if (token != null) {
            return this.getFallback(this.ts.offset(), startFallback);
        }
        return null;
    }

    private Token getPreviousNonEmptyToken(int offset) {
        this.ts.move(offset);
        if (!this.ts.moveNext() && !this.ts.movePrevious()) {
            return null;
        }
        Token ret = null;
        while (this.ts.movePrevious()) {
            Token token = this.ts.token();
            if (token.id() == JsTokenId.BLOCK_COMMENT || token.id() == JsTokenId.DOC_COMMENT || token.id() == JsTokenId.LINE_COMMENT || token.id() == JsTokenId.EOL || token.id() == JsTokenId.WHITESPACE) continue;
            ret = token;
            break;
        }
        return ret;
    }

    private Token getNextNonEmptyToken(int offset) {
        this.ts.move(offset);
        if (!this.ts.moveNext() && !this.ts.movePrevious()) {
            return null;
        }
        Token ret = null;
        while (this.ts.moveNext()) {
            Token token = this.ts.token();
            if (token.id() == JsTokenId.BLOCK_COMMENT || token.id() == JsTokenId.DOC_COMMENT || token.id() == JsTokenId.LINE_COMMENT || token.id() == JsTokenId.EOL || token.id() == JsTokenId.WHITESPACE) continue;
            ret = token;
            break;
        }
        return ret;
    }

    private FormatToken getPreviousNonWhiteToken(int offset, int stop, JsTokenId expected, boolean startFallback) {
        assert (stop <= offset);
        FormatToken ret = this.getPreviousToken(offset, expected, startFallback);
        if (startFallback && ret != null && ret.getKind() == FormatToken.Kind.SOURCE_START) {
            return ret;
        }
        if (ret != null) {
            if (expected == null) {
                return ret;
            }
            Token token = null;
            while (this.ts.movePrevious() && this.ts.offset() >= stop) {
                Token current = this.ts.token();
                if (current.id() == JsTokenId.WHITESPACE) continue;
                token = current;
                break;
            }
            if (token != null) {
                return this.getFallback(this.ts.offset(), startFallback);
            }
        }
        return null;
    }

    private FormatToken getCaseBlockEndToken(Block block) {
        int start = this.getStart((Node)block);
        int finish = this.getFinish((Node)block) - 1;
        this.ts.move(finish);
        if (!this.ts.moveNext() && !this.ts.movePrevious()) {
            return null;
        }
        Token ret = null;
        while (this.ts.moveNext()) {
            Token token = this.ts.token();
            if (token.id() == JsTokenId.BLOCK_COMMENT || token.id() == JsTokenId.DOC_COMMENT || token.id() == JsTokenId.LINE_COMMENT || token.id() == JsTokenId.EOL || token.id() == JsTokenId.WHITESPACE) continue;
            ret = token;
            break;
        }
        if (ret != null) {
            while (this.ts.movePrevious() && this.ts.offset() >= start) {
                Token current = this.ts.token();
                if (current.id() == JsTokenId.WHITESPACE) continue;
                ret = current;
                break;
            }
            if (ret != null) {
                return this.getFallback(this.ts.offset(), true);
            }
        }
        return null;
    }

    private FormatToken getFallback(int offset, boolean fallback) {
        FormatToken ret = this.tokenStream.getToken(offset);
        if (ret == null && fallback && offset < this.formatFinish) {
            ret = this.tokenStream.getTokens().get(0);
            assert (ret != null && ret.getKind() == FormatToken.Kind.SOURCE_START);
        }
        return ret;
    }

    private int getStart(Node node) {
        if (node instanceof BinaryNode) {
            return this.getStart((BinaryNode)node);
        }
        int start = node.getStart();
        long firstToken = node.getToken();
        TokenType type = jdk.nashorn.internal.parser.Token.descType((long)firstToken);
        if (type.equals((Object)TokenType.STRING) || type.equals((Object)TokenType.ESCSTRING)) {
            Token token;
            this.ts.move(start - 1);
            if (this.ts.moveNext() && (token = this.ts.token()).id() == JsTokenId.STRING_BEGIN) {
                --start;
            }
        }
        return start;
    }

    private int getStart(BinaryNode node) {
        return this.getStart(node.lhs());
    }

    private static int getFunctionStart(FunctionNode node) {
        return jdk.nashorn.internal.parser.Token.descPosition((long)node.getFirstToken());
    }

    private int getFinish(Node node) {
        if (node instanceof FunctionNode) {
            FunctionNode function = (FunctionNode)node;
            if (node.getStart() == node.getFinish()) {
                long lastToken = function.getLastToken();
                int finish = node.getStart() + jdk.nashorn.internal.parser.Token.descPosition((long)lastToken) + jdk.nashorn.internal.parser.Token.descLength((long)lastToken);
                if (jdk.nashorn.internal.parser.Token.descType((long)lastToken).equals((Object)TokenType.STRING)) {
                    ++finish;
                }
                return finish;
            }
            return node.getFinish();
        }
        if (node instanceof VarNode) {
            Token token = this.getNextNonEmptyToken(this.getFinishFixed(node) - 1);
            if (token != null && JsTokenId.OPERATOR_SEMICOLON == token.id()) {
                return this.ts.offset() + 1;
            }
            return this.getFinishFixed(node);
        }
        return this.getFinishFixed(node);
    }

    private int getFinishFixed(Node node) {
        int finish = node.getFinish();
        this.ts.move(finish);
        if (!this.ts.moveNext()) {
            return finish;
        }
        Token token = this.ts.token();
        if (token.id() == JsTokenId.STRING_END) {
            return finish + 1;
        }
        return finish;
    }

    private static boolean isScript(Node node) {
        return node instanceof FunctionNode && ((FunctionNode)node).getKind() == FunctionNode.Kind.SCRIPT;
    }

    private boolean isVirtual(Block block) {
        return block.getStart() == block.getFinish() || jdk.nashorn.internal.parser.Token.descType((long)block.getToken()) != TokenType.LBRACE || block.isCatchBlock();
    }

    @CheckForNull
    private static FormatToken findVirtualToken(FormatToken token, FormatToken.Kind kind, boolean backwards) {
        FormatToken result;
        FormatToken formatToken = result = backwards ? token.previous() : token.next();
        while (result != null && result.isVirtual() && result.getKind() != kind) {
            result = backwards ? result.previous() : result.next();
        }
        if (result != null && result.getKind() != kind) {
            return null;
        }
        return result;
    }

    private static void appendTokenAfterLastVirtual(FormatToken previous, FormatToken token) {
        FormatVisitor.appendTokenAfterLastVirtual(previous, token, false);
    }

    private static void appendTokenAfterLastVirtual(FormatToken previous, FormatToken token, boolean checkDuplicity) {
        assert (previous != null);
        FormatToken current = previous;
        for (FormatToken next = current.next(); next != null && next.isVirtual(); next = next.next()) {
            current = next;
        }
        if (!(checkDuplicity && current.isVirtual() && token.isVirtual() && current.getKind() == token.getKind())) {
            FormatVisitor.appendToken(current, token);
        }
    }

    private static void appendToken(FormatToken previous, FormatToken token) {
        FormatToken original = previous.next();
        previous.setNext(token);
        token.setPrevious(previous);
        token.setNext(original);
        if (original != null) {
            original.setPrevious(token);
        }
    }

    static {
        Collections.addAll(UNARY_TYPES, new TokenType[]{TokenType.ADD, TokenType.SUB, TokenType.BIT_NOT, TokenType.NOT, TokenType.INCPOSTFIX, TokenType.INCPREFIX, TokenType.DECPOSTFIX, TokenType.DECPREFIX});
    }
}

