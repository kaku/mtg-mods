/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.Documentation
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.parsing.api.ParserManager
 *  org.netbeans.modules.parsing.api.ResultIterator
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.UserTask
 *  org.netbeans.modules.parsing.spi.ParseException
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.netbeans.modules.csl.api.Documentation;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationPrinter;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.ParserManager;
import org.netbeans.modules.parsing.api.ResultIterator;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.UserTask;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class ParameterObject
extends JsObjectImpl {
    public ParameterObject(JsObject parent, Identifier name, String mimeType, String sourceLabel) {
        super(parent, name, name.getOffsetRange(), mimeType, sourceLabel);
        if (this.hasExactName()) {
            this.addOccurrence(name.getOffsetRange());
        }
    }

    @Override
    public boolean isDeclared() {
        return true;
    }

    @Override
    public JsElement.Kind getJSKind() {
        return JsElement.Kind.PARAMETER;
    }

    @Override
    public Documentation getDocumentation() {
        final String[] result = new String[1];
        try {
            ParserManager.parse(Collections.singleton(Source.create((FileObject)this.getParent().getFileObject())), (UserTask)new UserTask(){

                public void run(ResultIterator resultIterator) throws Exception {
                    JsComment comment;
                    JsDocumentationHolder holder;
                    Parser.Result parserResult = resultIterator.getParserResult(ParameterObject.this.getParent().getOffset());
                    if (parserResult instanceof JsParserResult && (comment = (holder = ((JsParserResult)parserResult).getDocumentationHolder()).getCommentForOffset(ParameterObject.this.getParent().getOffset(), holder.getCommentBlocks())) != null) {
                        for (DocParameter docParameter : comment.getParameters()) {
                            if (!docParameter.getParamName().getName().equals(ParameterObject.this.getDeclarationName().getName())) continue;
                            result[0] = JsDocumentationPrinter.printParameterDocumentation(docParameter);
                            return;
                        }
                    }
                }
            });
        }
        catch (ParseException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        if (result[0] != null && !result[0].isEmpty()) {
            return Documentation.create((String)result[0]);
        }
        return null;
    }

}

