/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementImpl;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;

public class SimpleElement
extends JsDocElementImpl {
    private SimpleElement(JsDocElementType type) {
        super(type);
    }

    public static SimpleElement create(JsDocElementType type) {
        return new SimpleElement(type);
    }
}

