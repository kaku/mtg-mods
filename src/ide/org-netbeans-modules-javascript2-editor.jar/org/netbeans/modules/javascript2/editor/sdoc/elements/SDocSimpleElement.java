/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocBaseElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public class SDocSimpleElement
extends SDocBaseElement {
    private SDocSimpleElement(SDocElementType type) {
        super(type);
    }

    public static SDocSimpleElement create(SDocElementType type) {
        return new SDocSimpleElement(type);
    }
}

