/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$CustomCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.options.ui.json;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class CodeCompletionPanel
extends JPanel {
    private final Preferences preferences;
    private final ItemListener defaultCheckBoxListener;
    private final Map<String, Object> id2Saved;
    private JCheckBox autoCompletionSmartQuotesCheckBox;
    private JLabel autoCompletionSmartQuotesLabel;

    public CodeCompletionPanel(Preferences preferences) {
        this.defaultCheckBoxListener = new DefaultCheckBoxListener();
        this.id2Saved = new HashMap<String, Object>();
        assert (preferences != null);
        this.preferences = preferences;
        this.initComponents();
        this.initAutoCompletion();
    }

    public static PreferencesCustomizer.Factory getCustomizerFactory() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                return new CodeCompletionPreferencesCustomizer(preferences);
            }
        };
    }

    private void initAutoCompletion() {
        boolean codeCompletionSmartQuotes = this.preferences.getBoolean("codeCompletionSmartQuotes", true);
        this.autoCompletionSmartQuotesCheckBox.setSelected(codeCompletionSmartQuotes);
        this.autoCompletionSmartQuotesCheckBox.addItemListener(this.defaultCheckBoxListener);
        this.id2Saved.put("codeCompletionSmartQuotes", this.autoCompletionSmartQuotesCheckBox.isSelected());
    }

    void validateData() {
        this.preferences.putBoolean("codeCompletionSmartQuotes", this.autoCompletionSmartQuotesCheckBox.isSelected());
    }

    private void initComponents() {
        this.autoCompletionSmartQuotesLabel = new JLabel();
        this.autoCompletionSmartQuotesCheckBox = new JCheckBox();
        Mnemonics.setLocalizedText((JLabel)this.autoCompletionSmartQuotesLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionSmartQuotesLabel.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.autoCompletionSmartQuotesCheckBox, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionSmartQuotesCheckBox.text"));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.autoCompletionSmartQuotesLabel).addComponent(this.autoCompletionSmartQuotesCheckBox)).addContainerGap(30, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.autoCompletionSmartQuotesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.autoCompletionSmartQuotesCheckBox).addContainerGap(-1, 32767)));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.AccessibleContext.accessibleDescription"));
    }

    String getSavedValue(String key) {
        return this.id2Saved.get(key).toString();
    }

    public static final class CustomCustomizerImpl
    extends PreferencesCustomizer.CustomCustomizer {
        public String getSavedValue(PreferencesCustomizer customCustomizer, String key) {
            if (customCustomizer instanceof CodeCompletionPreferencesCustomizer) {
                return ((CodeCompletionPanel)customCustomizer.getComponent()).getSavedValue(key);
            }
            return null;
        }
    }

    static final class CodeCompletionPreferencesCustomizer
    implements PreferencesCustomizer {
        private final Preferences preferences;
        private CodeCompletionPanel component;

        private CodeCompletionPreferencesCustomizer(Preferences preferences) {
            this.preferences = preferences;
        }

        public String getId() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getDisplayName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx("org.netbeans.modules.javascript2.editor.options.CodeCompletionPanel");
        }

        public JComponent getComponent() {
            if (this.component == null) {
                this.component = new CodeCompletionPanel(this.preferences);
            }
            return this.component;
        }
    }

    private final class DefaultCheckBoxListener
    implements ItemListener,
    Serializable {
        private DefaultCheckBoxListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            CodeCompletionPanel.this.validateData();
        }
    }

}

