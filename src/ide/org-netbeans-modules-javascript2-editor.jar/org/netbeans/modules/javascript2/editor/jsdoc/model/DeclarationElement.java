/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementImpl;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.model.Type;

public class DeclarationElement
extends JsDocElementImpl {
    private final Type declaredType;

    private DeclarationElement(JsDocElementType type, Type declaredType) {
        super(type);
        this.declaredType = declaredType;
    }

    public static DeclarationElement create(JsDocElementType type, Type declaredType) {
        return new DeclarationElement(type, declaredType);
    }

    public Type getDeclaredType() {
        return this.declaredType;
    }
}

