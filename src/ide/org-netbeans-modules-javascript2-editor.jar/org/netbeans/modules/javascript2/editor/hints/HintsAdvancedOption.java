/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.spi.options.AdvancedOption
 *  org.netbeans.spi.options.OptionsPanelController
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.spi.options.AdvancedOption;
import org.netbeans.spi.options.OptionsPanelController;

public class HintsAdvancedOption
extends AdvancedOption {
    OptionsPanelController panelController;

    public String getDisplayName() {
        return Bundle.CTL_Hints_DisplayName();
    }

    public String getTooltip() {
        return Bundle.CTL_Hints_ToolTip();
    }

    public synchronized OptionsPanelController create() {
        if (this.panelController == null) {
            HintsProvider.HintsManager manager = HintsProvider.HintsManager.getManagerForMimeType((String)"text/javascript");
            assert (manager != null);
            this.panelController = manager.getOptionsController();
        }
        return this.panelController;
    }

    public static OptionsPanelController createStatic() {
        return new HintsAdvancedOption().create();
    }
}

