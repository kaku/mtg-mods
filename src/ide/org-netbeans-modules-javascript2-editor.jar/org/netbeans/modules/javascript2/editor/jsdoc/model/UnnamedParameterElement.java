/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElementType;
import org.netbeans.modules.javascript2.editor.jsdoc.model.ParameterElement;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public class UnnamedParameterElement
extends ParameterElement
implements DocParameter {
    private UnnamedParameterElement(JsDocElementType type, List<Type> paramTypes, String paramDescription) {
        super(type, paramTypes, paramDescription);
    }

    public static UnnamedParameterElement create(JsDocElementType type, List<Type> paramTypes, String paramDescription) {
        return new UnnamedParameterElement(type, paramTypes, paramDescription);
    }

    @Override
    public Identifier getParamName() {
        return null;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOptional() {
        return false;
    }
}

