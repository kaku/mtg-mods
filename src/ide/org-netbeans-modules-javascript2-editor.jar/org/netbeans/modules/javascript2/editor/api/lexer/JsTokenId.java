/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.InputAttributes
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.spi.lexer.LanguageEmbedding
 *  org.netbeans.spi.lexer.LanguageHierarchy
 *  org.netbeans.spi.lexer.Lexer
 *  org.netbeans.spi.lexer.LexerRestartInfo
 */
package org.netbeans.modules.javascript2.editor.api.lexer;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.lexer.InputAttributes;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsDocumentationTokenId;
import org.netbeans.modules.javascript2.editor.lexer.JsLexer;
import org.netbeans.modules.javascript2.editor.lexer.JsonLexer;
import org.netbeans.spi.lexer.LanguageEmbedding;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public enum JsTokenId implements TokenId
{
    ERROR(null, "error"){

        @Override
        public boolean isError() {
            return true;
        }
    }
    ,
    UNKNOWN(null, "unknown"){

        @Override
        public boolean isError() {
            return true;
        }
    }
    ,
    NUMBER(null, "constant"),
    IDENTIFIER(null, "identifier"),
    WHITESPACE(null, "whitespace"),
    EOL(null, "whitespace"),
    LINE_COMMENT(null, "comment"),
    BLOCK_COMMENT(null, "comment"),
    DOC_COMMENT(null, "comment"),
    STRING_BEGIN(null, "string"),
    STRING(null, "string"),
    STRING_END(null, "string"),
    REGEXP_BEGIN(null, "mod-regexp"),
    REGEXP(null, "mod-regexp"),
    REGEXP_END(null, "mod-regexp"),
    BRACKET_LEFT_PAREN("(", "separator"),
    BRACKET_RIGHT_PAREN(")", "separator"),
    BRACKET_LEFT_CURLY("{", "separator"),
    BRACKET_RIGHT_CURLY("}", "separator"),
    BRACKET_LEFT_BRACKET("[", "separator"),
    BRACKET_RIGHT_BRACKET("]", "separator"),
    OPERATOR_SEMICOLON(";", "separator"),
    OPERATOR_COMMA(",", "separator"),
    OPERATOR_DOT(".", "separator"),
    OPERATOR_ASSIGNMENT("=", "operator"),
    OPERATOR_GREATER(">", "operator"),
    OPERATOR_LOWER("<", "operator"),
    OPERATOR_NOT("!", "operator"),
    OPERATOR_BITWISE_NOT("~", "operator"),
    OPERATOR_TERNARY("?", "operator"),
    OPERATOR_COLON(":", "separator"),
    OPERATOR_EQUALS("==", "operator"),
    OPERATOR_EQUALS_EXACTLY("===", "operator"),
    OPERATOR_LOWER_EQUALS("<=", "operator"),
    OPERATOR_GREATER_EQUALS(">=", "operator"),
    OPERATOR_NOT_EQUALS("!=", "operator"),
    OPERATOR_NOT_EQUALS_EXACTLY("!==", "operator"),
    OPERATOR_AND("&&", "operator"),
    OPERATOR_OR("||", "operator"),
    OPERATOR_INCREMENT("++", "operator"),
    OPERATOR_DECREMENT("--", "operator"),
    OPERATOR_PLUS("+", "operator"),
    OPERATOR_MINUS("-", "operator"),
    OPERATOR_MULTIPLICATION("*", "operator"),
    OPERATOR_DIVISION("/", "operator"),
    OPERATOR_BITWISE_AND("&", "operator"),
    OPERATOR_BITWISE_OR("|", "operator"),
    OPERATOR_BITWISE_XOR("^", "operator"),
    OPERATOR_MODULUS("%", "operator"),
    OPERATOR_LEFT_SHIFT_ARITHMETIC("<<", "operator"),
    OPERATOR_RIGHT_SHIFT_ARITHMETIC(">>", "operator"),
    OPERATOR_RIGHT_SHIFT(">>>", "operator"),
    OPERATOR_PLUS_ASSIGNMENT("+=", "operator"),
    OPERATOR_MINUS_ASSIGNMENT("-=", "operator"),
    OPERATOR_MULTIPLICATION_ASSIGNMENT("*=", "operator"),
    OPERATOR_DIVISION_ASSIGNMENT("/=", "operator"),
    OPERATOR_BITWISE_AND_ASSIGNMENT("&=", "operator"),
    OPERATOR_BITWISE_OR_ASSIGNMENT("|=", "operator"),
    OPERATOR_BITWISE_XOR_ASSIGNMENT("^=", "operator"),
    OPERATOR_MODULUS_ASSIGNMENT("%=", "operator"),
    OPERATOR_LEFT_SHIFT_ARITHMETIC_ASSIGNMENT("<<=", "operator"),
    OPERATOR_RIGHT_SHIFT_ARITHMETIC_ASSIGNMENT(">>=", "operator"),
    OPERATOR_RIGHT_SHIFT_ASSIGNMENT(">>>=", "operator"),
    KEYWORD_BREAK("break", "keyword"),
    KEYWORD_CASE("case", "keyword"),
    KEYWORD_CATCH("catch", "keyword"),
    KEYWORD_CONTINUE("continue", "keyword"),
    KEYWORD_DEBUGGER("debugger", "keyword"),
    KEYWORD_DEFAULT("default", "keyword"),
    KEYWORD_DELETE("delete", "keyword"),
    KEYWORD_DO("do", "keyword"),
    KEYWORD_ELSE("else", "keyword"),
    KEYWORD_FINALLY("finally", "keyword"),
    KEYWORD_FOR("for", "keyword"),
    KEYWORD_FUNCTION("function", "keyword"),
    KEYWORD_IF("if", "keyword"),
    KEYWORD_IN("in", "keyword"),
    KEYWORD_INSTANCEOF("instanceof", "keyword"),
    KEYWORD_NEW("new", "keyword"),
    KEYWORD_RETURN("return", "keyword"),
    KEYWORD_SWITCH("switch", "keyword"),
    KEYWORD_THIS("this", "keyword"),
    KEYWORD_THROW("throw", "keyword"),
    KEYWORD_TRY("try", "keyword"),
    KEYWORD_TYPEOF("typeof", "keyword"),
    KEYWORD_VAR("var", "keyword"),
    KEYWORD_VOID("void", "keyword"),
    KEYWORD_WHILE("while", "keyword"),
    KEYWORD_WITH("with", "keyword"),
    RESERVED_CLASS("class", "reserved"),
    RESERVED_CONST("const", "reserved"),
    RESERVED_ENUM("enum", "reserved"),
    RESERVED_EXPORT("export", "reserved"),
    RESERVED_EXTENDS("extends", "reserved"),
    RESERVED_IMPORT("import", "reserved"),
    RESERVED_SUPER("super", "reserved"),
    RESERVED_IMPLEMENTS("implements", "reserved"),
    RESERVED_INTERFACE("interface", "reserved"),
    RESERVED_LET("let", "reserved"),
    RESERVED_PACKAGE("package", "reserved"),
    RESERVED_PRIVATE("private", "reserved"),
    RESERVED_PROTECTED("protected", "reserved"),
    RESERVED_PUBLIC("public", "reserved"),
    RESERVED_STATIC("static", "reserved"),
    RESERVED_YIELD("yield", "reserved"),
    KEYWORD_TRUE("true", "keyword"),
    KEYWORD_FALSE("false", "keyword"),
    KEYWORD_NULL("null", "keyword");
    
    public static final String JAVASCRIPT_MIME_TYPE = "text/javascript";
    public static final String JSON_MIME_TYPE = "text/x-json";
    private final String fixedText;
    private final String primaryCategory;
    private static final Language<JsTokenId> JAVASCRIPT_LANGUAGE;
    private static final Language<JsTokenId> JSON_LANGUAGE;

    private JsTokenId(String fixedText, String primaryCategory) {
        this.fixedText = fixedText;
        this.primaryCategory = primaryCategory;
    }

    public String fixedText() {
        return this.fixedText;
    }

    public String primaryCategory() {
        return this.primaryCategory;
    }

    public boolean isKeyword() {
        return "keyword".equals(this.primaryCategory);
    }

    public boolean isError() {
        return false;
    }

    public static Language<JsTokenId> javascriptLanguage() {
        return JAVASCRIPT_LANGUAGE;
    }

    public static Language<JsTokenId> jsonLanguage() {
        return JSON_LANGUAGE;
    }

    static {
        JAVASCRIPT_LANGUAGE = new LanguageHierarchy<JsTokenId>(){

            protected String mimeType() {
                return "text/javascript";
            }

            protected Collection<JsTokenId> createTokenIds() {
                return EnumSet.allOf(JsTokenId.class);
            }

            protected Map<String, Collection<JsTokenId>> createTokenCategories() {
                HashMap<String, Collection<JsTokenId>> cats = new HashMap<String, Collection<JsTokenId>>();
                return cats;
            }

            protected Lexer<JsTokenId> createLexer(LexerRestartInfo<JsTokenId> info) {
                return JsLexer.create(info);
            }

            protected LanguageEmbedding<?> embedding(Token<JsTokenId> token, LanguagePath languagePath, InputAttributes inputAttributes) {
                JsTokenId id = (JsTokenId)token.id();
                if (id == JsTokenId.DOC_COMMENT || id == JsTokenId.BLOCK_COMMENT) {
                    return LanguageEmbedding.create(JsDocumentationTokenId.language(), (int)0, (int)0);
                }
                return null;
            }
        }.language();
        JSON_LANGUAGE = new LanguageHierarchy<JsTokenId>(){

            protected String mimeType() {
                return "text/x-json";
            }

            protected Collection<JsTokenId> createTokenIds() {
                return EnumSet.allOf(JsTokenId.class);
            }

            protected Map<String, Collection<JsTokenId>> createTokenCategories() {
                HashMap<String, Collection<JsTokenId>> cats = new HashMap<String, Collection<JsTokenId>>();
                return cats;
            }

            protected Lexer<JsTokenId> createLexer(LexerRestartInfo<JsTokenId> info) {
                return JsonLexer.create(info);
            }
        }.language();
    }

}

