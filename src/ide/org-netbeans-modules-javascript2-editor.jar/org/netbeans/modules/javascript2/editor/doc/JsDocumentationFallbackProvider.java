/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.parsing.api.Snapshot
 */
package org.netbeans.modules.javascript2.editor.doc;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationFallbackHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTagProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationProvider;
import org.netbeans.modules.javascript2.editor.doc.spi.SyntaxProvider;
import org.netbeans.modules.parsing.api.Snapshot;

public final class JsDocumentationFallbackProvider
implements JsDocumentationProvider {
    public Set getSupportedTags() {
        return Collections.emptySet();
    }

    @Override
    public JsDocumentationHolder createDocumentationHolder(Snapshot snapshot) {
        return new JsDocumentationFallbackHolder(this, snapshot);
    }

    public List<AnnotationCompletionTagProvider> getAnnotationsProvider() {
        return Collections.emptyList();
    }

    @Override
    public SyntaxProvider getSyntaxProvider() {
        return null;
    }
}

