/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.JsElement;
import org.netbeans.modules.javascript2.editor.model.JsFunction;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.Model;
import org.netbeans.modules.javascript2.editor.model.Occurrence;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectReference;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.OccurrenceImpl;

public class OccurrencesSupport {
    private static final Logger LOGGER = Logger.getLogger(OccurrencesSupport.class.getName());
    private final Model model;

    public OccurrencesSupport(Model model) {
        this.model = model;
    }

    public Occurrence getOccurrence(int offset) {
        long start = System.currentTimeMillis();
        JsObject object = this.model.getGlobalObject();
        Occurrence result = this.findOccurrence(object, offset);
        if (result == null) {
            result = this.findDeclaration(object, offset);
        }
        long end = System.currentTimeMillis();
        LOGGER.log(Level.FINE, "Computing getOccurences({0}) took {1}ms. Returns {2}", new Object[]{offset, end - start, result});
        return result;
    }

    private Occurrence findOccurrence(JsObject object, int offset) {
        Occurrence result = null;
        JsElement.Kind kind = object.getJSKind();
        for (Occurrence occurrence : object.getOccurrences()) {
            if (!occurrence.getOffsetRange().containsInclusive(offset)) continue;
            return occurrence;
        }
        if (kind.isFunction() || kind == JsElement.Kind.CATCH_BLOCK) {
            JsObject param;
            Iterator i$ = ((JsFunction)object).getParameters().iterator();
            while (i$.hasNext() && (result = this.findOccurrence(param = (JsObject)i$.next(), offset)) == null) {
            }
            if (result != null) {
                return result;
            }
        }
        if (!(object instanceof JsObjectReference) || !ModelUtils.isDescendant(object, ((JsObjectReference)object).getOriginal())) {
            for (JsObject property : object.getProperties().values()) {
                if (!(property instanceof JsObjectReference) || ((JsObjectReference)property).getOriginal().isAnonymous()) {
                    result = this.findOccurrence(property, offset);
                    if (result == null) continue;
                    break;
                }
                for (Occurrence occurrence2 : property.getOccurrences()) {
                    if (!occurrence2.getOffsetRange().containsInclusive(offset)) continue;
                    return occurrence2;
                }
            }
        }
        return result;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private Occurrence findDeclaration(JsObject object, int offset) {
        result = null;
        kind = object.getJSKind();
        if (kind == JsElement.Kind.ANONYMOUS_OBJECT) ** GOTO lbl-1000
        if (kind != JsElement.Kind.WITH_OBJECT && object.getDeclarationName() != null && object.getDeclarationName().getOffsetRange().containsInclusive(offset) && !ModelUtils.isGlobal(object)) {
            if (kind.isPropertyGetterSetter()) {
                propertyName = object.getName();
                propertyName = propertyName.substring(propertyName.lastIndexOf(32) + 1);
                property = object.getParent().getProperty(propertyName);
                if (property != null) {
                    return new OccurrenceImpl(property.getDeclarationName().getOffsetRange(), property);
                }
            }
            result = new OccurrenceImpl(object.getDeclarationName().getOffsetRange(), object);
        }
        if (result == null) lbl-1000: // 2 sources:
        {
            if (kind.isFunction() || kind == JsElement.Kind.CATCH_BLOCK) {
                for (JsObject param : ((JsFunction)object).getParameters()) {
                    if (!param.getDeclarationName().getOffsetRange().containsInclusive(offset)) continue;
                    return new OccurrenceImpl(param.getDeclarationName().getOffsetRange(), object);
                }
            }
        }
        if (result != null) return result;
        i$ = object.getProperties().values().iterator();
        do {
            if (i$.hasNext() == false) return result;
        } while ((property = i$.next()) instanceof JsObjectReference || (result = this.findDeclaration(property, offset)) == null);
        return result;
    }
}

