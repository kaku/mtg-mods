/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.csl.api.EditorOptions
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.spi.GsfUtilities
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Context
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$Factory
 *  org.netbeans.spi.editor.typinghooks.TypedTextInterceptor$MutableContext
 */
package org.netbeans.modules.javascript2.editor;

import java.util.concurrent.atomic.AtomicReference;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.csl.api.EditorOptions;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.spi.GsfUtilities;
import org.netbeans.modules.javascript2.editor.api.lexer.JsTokenId;
import org.netbeans.modules.javascript2.editor.api.lexer.LexUtilities;
import org.netbeans.modules.javascript2.editor.options.OptionsUtils;
import org.netbeans.spi.editor.typinghooks.TypedTextInterceptor;

public class JsTypedTextInterceptor
implements TypedTextInterceptor {
    private static final TokenId[] REGEXP_TOKENS = new TokenId[]{JsTokenId.REGEXP, JsTokenId.REGEXP_END};
    private static final TokenId[] STRING_TOKENS = new TokenId[]{JsTokenId.STRING, JsTokenId.STRING_END};
    private final Language<JsTokenId> language;
    private final boolean singleQuote;
    private int previousAdjustmentOffset = -1;
    private int previousAdjustmentIndent;

    public JsTypedTextInterceptor(Language<JsTokenId> language, boolean singleQuote) {
        this.language = language;
        this.singleQuote = singleQuote;
    }

    private boolean isInsertMatchingEnabled() {
        EditorOptions options = EditorOptions.get((String)this.language.mimeType());
        if (options != null) {
            return options.getMatchBrackets();
        }
        return true;
    }

    private boolean isSmartQuotingEnabled() {
        return OptionsUtils.forLanguage(this.language).autoCompletionSmartQuotes();
    }

    public void afterInsert(final TypedTextInterceptor.Context context) throws BadLocationException {
        final BaseDocument doc = (BaseDocument)context.getDocument();
        final AtomicReference ex = new AtomicReference();
        doc.runAtomicAsUser(new Runnable(){

            @Override
            public void run() {
                int dotPos = context.getOffset();
                Caret caret = context.getComponent().getCaret();
                char ch = context.getText().charAt(0);
                try {
                    if (JsTypedTextInterceptor.this.previousAdjustmentOffset != -1) {
                        TokenSequence<? extends JsTokenId> ts;
                        if (dotPos == JsTypedTextInterceptor.this.previousAdjustmentOffset && (ts = LexUtilities.getTokenSequence((Document)doc, dotPos, JsTypedTextInterceptor.this.language)) != null) {
                            ts.move(dotPos);
                            if (ts.moveNext() && ts.offset() < dotPos) {
                                GsfUtilities.setLineIndentation((BaseDocument)doc, (int)dotPos, (int)JsTypedTextInterceptor.this.previousAdjustmentIndent);
                            }
                        }
                        JsTypedTextInterceptor.this.previousAdjustmentOffset = -1;
                    }
                    switch (ch) {
                        case '(': 
                        case '[': 
                        case '{': {
                            if (!JsTypedTextInterceptor.this.isInsertMatchingEnabled()) break;
                        }
                        case ')': 
                        case ']': 
                        case '}': {
                            Token<? extends JsTokenId> token = LexUtilities.getToken((Document)doc, dotPos, JsTypedTextInterceptor.this.language);
                            if (token == null) {
                                return;
                            }
                            TokenId id = token.id();
                            if (id == JsTokenId.IDENTIFIER && token.length() == 1 || id == JsTokenId.BRACKET_LEFT_BRACKET || id == JsTokenId.BRACKET_RIGHT_BRACKET || id == JsTokenId.BRACKET_LEFT_CURLY || id == JsTokenId.BRACKET_RIGHT_CURLY || id == JsTokenId.BRACKET_LEFT_PAREN || id == JsTokenId.BRACKET_RIGHT_PAREN) {
                                if (ch == ']') {
                                    JsTypedTextInterceptor.this.skipClosingBracket(doc, caret, ch, JsTokenId.BRACKET_RIGHT_BRACKET);
                                } else if (ch == ')') {
                                    JsTypedTextInterceptor.this.skipClosingBracket(doc, caret, ch, JsTokenId.BRACKET_RIGHT_PAREN);
                                } else if (ch == '}') {
                                    JsTypedTextInterceptor.this.skipClosingBracket(doc, caret, ch, JsTokenId.BRACKET_RIGHT_CURLY);
                                } else if (ch == '[' || ch == '(') {
                                    JsTypedTextInterceptor.this.completeOpeningBracket(doc, dotPos, caret, ch);
                                }
                            }
                            if (ch == '}') {
                                JsTypedTextInterceptor.this.reindent(doc, dotPos, JsTokenId.BRACKET_RIGHT_CURLY, caret);
                                break;
                            }
                            if (ch != ']') break;
                            JsTypedTextInterceptor.this.reindent(doc, dotPos, JsTokenId.BRACKET_RIGHT_BRACKET, caret);
                            break;
                        }
                    }
                }
                catch (BadLocationException blex) {
                    ex.set(blex);
                }
            }
        });
        BadLocationException blex = (BadLocationException)ex.get();
        if (blex != null) {
            throw blex;
        }
    }

    public boolean beforeInsert(TypedTextInterceptor.Context context) throws BadLocationException {
        return false;
    }

    public void insert(TypedTextInterceptor.MutableContext context) throws BadLocationException {
        char firstChar;
        TokenSequence<? extends JsTokenId> ts;
        int caretOffset = context.getOffset();
        char ch = context.getText().charAt(0);
        BaseDocument doc = (BaseDocument)context.getDocument();
        String selection = context.getReplacedText();
        boolean isTemplate = GsfUtilities.isCodeTemplateEditing((Document)doc);
        if (selection != null && selection.length() > 0 && !isTemplate && ((ch == '\"' || ch == '\'') && this.isSmartQuotingEnabled() || (ch == '(' || ch == '{' || ch == '[') && this.isInsertMatchingEnabled()) && (firstChar = selection.charAt(0)) != ch && (ts = LexUtilities.getPositionedSequence((Document)doc, caretOffset, this.language)) != null && ts.token().id() != JsTokenId.LINE_COMMENT && ts.token().id() != JsTokenId.DOC_COMMENT && ts.token().id() != JsTokenId.BLOCK_COMMENT && ts.token().id() != JsTokenId.STRING) {
            char lastChar = selection.charAt(selection.length() - 1);
            if (selection.length() > 1 && (firstChar == '\"' || firstChar == '\'' || firstChar == '(' || firstChar == '{' || firstChar == '[' || firstChar == '/') && lastChar == JsTypedTextInterceptor.matching(firstChar)) {
                String innerText = selection.substring(1, selection.length() - 1);
                String text = Character.toString(ch) + innerText + Character.toString(JsTypedTextInterceptor.matching(ch));
                context.setText(text, text.length());
            } else {
                String text = "" + ch + selection + JsTypedTextInterceptor.matching(ch);
                context.setText(text, text.length());
            }
            return;
        }
        TokenSequence<? extends JsTokenId> ts2 = LexUtilities.getTokenSequence((Document)doc, caretOffset, this.language);
        if (ts2 == null) {
            return;
        }
        ts2.move(caretOffset);
        if (!ts2.moveNext() && !ts2.movePrevious()) {
            return;
        }
        Token token = ts2.token();
        JsTokenId id = (JsTokenId)token.id();
        TokenId[] stringTokens = null;
        JsTokenId beginTokenId = null;
        if (ch == '\"' || ch == '\'' && this.singleQuote) {
            stringTokens = STRING_TOKENS;
            beginTokenId = JsTokenId.STRING_BEGIN;
        } else if (id.isError()) {
            ts2.movePrevious();
            TokenId prevId = ts2.token().id();
            if (JsTypedTextInterceptor.isCompletableStringBoundary(ts2.token(), this.singleQuote, false)) {
                stringTokens = STRING_TOKENS;
                beginTokenId = prevId;
            } else if (prevId == JsTokenId.REGEXP_BEGIN) {
                stringTokens = REGEXP_TOKENS;
                beginTokenId = JsTokenId.REGEXP_BEGIN;
            }
        } else if (JsTypedTextInterceptor.isCompletableStringBoundary(token, this.singleQuote, false) && caretOffset == ts2.offset() + 1) {
            if (!Character.isLetter(ch)) {
                stringTokens = STRING_TOKENS;
                beginTokenId = id;
            }
        } else if (JsTypedTextInterceptor.isCompletableStringBoundary(token, this.singleQuote, false) && caretOffset == ts2.offset() + 2 || JsTypedTextInterceptor.isCompletableStringBoundary(token, this.singleQuote, true)) {
            stringTokens = STRING_TOKENS;
            beginTokenId = JsTokenId.STRING_BEGIN;
        } else if (id == JsTokenId.REGEXP_BEGIN && caretOffset == ts2.offset() + 2 || id == JsTokenId.REGEXP_END) {
            stringTokens = REGEXP_TOKENS;
            beginTokenId = JsTokenId.REGEXP_BEGIN;
        }
        if (stringTokens != null && this.isSmartQuotingEnabled()) {
            this.completeQuote(context, ch, stringTokens, beginTokenId, isTemplate);
        }
    }

    public void cancelled(TypedTextInterceptor.Context context) {
    }

    private void reindent(BaseDocument doc, int offset, TokenId id, Caret caret) throws BadLocationException {
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence((Document)doc, offset, this.language);
        if (ts != null) {
            ts.move(offset);
            if (!ts.moveNext() && !ts.movePrevious()) {
                return;
            }
            Token token = ts.token();
            if (token.id() == id) {
                int rowFirstNonWhite = Utilities.getRowFirstNonWhite((BaseDocument)doc, (int)offset);
                if (ts.offset() > rowFirstNonWhite) {
                    return;
                }
                OffsetRange begin = OffsetRange.NONE;
                if (id == JsTokenId.BRACKET_RIGHT_CURLY) {
                    begin = LexUtilities.findBwd((Document)doc, ts, JsTokenId.BRACKET_LEFT_CURLY, JsTokenId.BRACKET_RIGHT_CURLY);
                } else if (id == JsTokenId.BRACKET_RIGHT_BRACKET) {
                    begin = LexUtilities.findBwd((Document)doc, ts, JsTokenId.BRACKET_LEFT_BRACKET, JsTokenId.BRACKET_RIGHT_BRACKET);
                }
                if (begin != OffsetRange.NONE) {
                    int beginOffset = begin.getStart();
                    int indent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)beginOffset);
                    this.previousAdjustmentIndent = GsfUtilities.getLineIndent((BaseDocument)doc, (int)offset);
                    GsfUtilities.setLineIndentation((BaseDocument)doc, (int)offset, (int)indent);
                    this.previousAdjustmentOffset = caret.getDot();
                }
            }
        }
    }

    private void completeOpeningBracket(BaseDocument doc, int dotPos, Caret caret, char bracket) throws BadLocationException {
        if (this.isCompletablePosition(doc, dotPos + 1)) {
            String matchingBracket = "" + JsTypedTextInterceptor.matching(bracket);
            doc.insertString(dotPos + 1, matchingBracket, null);
            caret.setDot(dotPos + 1);
        }
    }

    private void completeQuote(TypedTextInterceptor.MutableContext context, char bracket, TokenId[] stringTokens, TokenId beginToken, boolean isTemplate) throws BadLocationException {
        int lastNonWhite;
        boolean eol;
        if (isTemplate) {
            if (bracket == '\"' || bracket == '\'' || bracket == '(' || bracket == '{' || bracket == '[') {
                String text = context.getText() + JsTypedTextInterceptor.matching(bracket);
                context.setText(text, text.length() - 1);
            }
            return;
        }
        int dotPos = context.getOffset();
        BaseDocument doc = (BaseDocument)context.getDocument();
        if (JsTypedTextInterceptor.isEscapeSequence(doc, dotPos)) {
            return;
        }
        if (doc.getLength() < dotPos) {
            return;
        }
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence((Document)doc, dotPos, this.language);
        if (ts == null) {
            return;
        }
        ts.move(dotPos);
        if (!ts.moveNext() && !ts.movePrevious()) {
            return;
        }
        Token<? extends JsTokenId> token = ts.token();
        Token previousToken = null;
        if (ts.movePrevious()) {
            previousToken = ts.token();
        }
        boolean bl = eol = (lastNonWhite = Utilities.getRowLastNonWhite((BaseDocument)doc, (int)dotPos)) < dotPos;
        if (token.id() == JsTokenId.BLOCK_COMMENT || token.id() == JsTokenId.DOC_COMMENT || token.id() == JsTokenId.LINE_COMMENT || previousToken != null && previousToken.id() == JsTokenId.LINE_COMMENT && token.id() == JsTokenId.EOL) {
            return;
        }
        if (token.id() == JsTokenId.WHITESPACE && eol && dotPos - 1 > 0 && (token = LexUtilities.getToken((Document)doc, dotPos - 1, this.language)).id() == JsTokenId.LINE_COMMENT) {
            return;
        }
        boolean completablePosition = this.isQuoteCompletablePosition(doc, dotPos);
        boolean insideString = false;
        JsTokenId id = (JsTokenId)token.id();
        for (TokenId currId : stringTokens) {
            if (id != currId) continue;
            insideString = true;
            break;
        }
        if (id.isError() && previousToken != null && previousToken.id() == beginToken) {
            insideString = true;
        }
        if (id == JsTokenId.EOL && previousToken != null) {
            if (previousToken.id() == beginToken) {
                insideString = true;
            } else if (((JsTokenId)previousToken.id()).isError() && ts.movePrevious() && ts.token().id() == beginToken) {
                insideString = true;
            }
        }
        if (!insideString && token.id() == JsTokenId.WHITESPACE && eol && dotPos - 1 > 0) {
            token = LexUtilities.getToken((Document)doc, dotPos - 1, this.language);
            boolean bl2 = insideString = token.id() == JsTokenId.STRING;
        }
        if (insideString) {
            if (eol) {
                return;
            }
            char chr = doc.getChars(dotPos, 1)[0];
            if (chr == bracket) {
                context.setText(Character.toString(bracket), 1);
                doc.remove(dotPos, 1);
                return;
            }
        }
        if (completablePosition && !insideString || eol) {
            String text = Character.toString(bracket) + JsTypedTextInterceptor.matching(bracket);
            context.setText(text, text.length() - 1);
        }
    }

    private boolean isCompletablePosition(BaseDocument doc, int dotPos) throws BadLocationException {
        if (dotPos == doc.getLength()) {
            return true;
        }
        char chr = doc.getChars(dotPos, 1)[0];
        return chr == ')' || chr == ',' || chr == '\"' || chr == '\'' || chr == ' ' || chr == ']' || chr == '}' || chr == '\n' || chr == '\t' || chr == ';';
    }

    private boolean isQuoteCompletablePosition(BaseDocument doc, int dotPos) throws BadLocationException {
        if (dotPos == doc.getLength()) {
            return true;
        }
        int eol = Utilities.getRowEnd((BaseDocument)doc, (int)dotPos);
        if (dotPos == eol || eol == -1) {
            return false;
        }
        int firstNonWhiteFwd = Utilities.getFirstNonWhiteFwd((BaseDocument)doc, (int)dotPos, (int)eol);
        if (firstNonWhiteFwd == -1) {
            return false;
        }
        char chr = doc.getChars(firstNonWhiteFwd, 1)[0];
        return chr == ')' || chr == ',' || chr == '+' || chr == '}' || chr == ';' || chr == ']';
    }

    private void skipClosingBracket(BaseDocument doc, Caret caret, char bracket, TokenId bracketId) throws BadLocationException {
        int caretOffset = caret.getDot();
        if (this.isSkipClosingBracket(doc, caretOffset, bracketId)) {
            doc.remove(caretOffset - 1, 1);
            caret.setDot(caretOffset);
        }
    }

    private boolean isSkipClosingBracket(BaseDocument doc, int caretOffset, TokenId bracketId) throws BadLocationException {
        if (caretOffset == doc.getLength()) {
            return false;
        }
        boolean skipClosingBracket = false;
        TokenSequence<? extends JsTokenId> ts = LexUtilities.getTokenSequence((Document)doc, caretOffset, this.language);
        if (ts == null) {
            return false;
        }
        ts.move(caretOffset);
        if (!ts.moveNext()) {
            return false;
        }
        Token token = ts.token();
        if (token != null && token.id() == bracketId) {
            int bracketIntId = bracketId.ordinal();
            int leftBracketIntId = bracketIntId == JsTokenId.BRACKET_RIGHT_PAREN.ordinal() ? JsTokenId.BRACKET_LEFT_PAREN.ordinal() : JsTokenId.BRACKET_LEFT_BRACKET.ordinal();
            ts.moveNext();
            Token nextToken = ts.token();
            boolean endOfJs = false;
            while (nextToken != null && nextToken.id() == bracketId) {
                token = nextToken;
                if (!ts.moveNext()) {
                    endOfJs = true;
                    break;
                }
                nextToken = ts.token();
            }
            int braceBalance = 0;
            int bracketBalance = 0;
            Token lastRBracket = token;
            if (!endOfJs) {
                ts.movePrevious();
            }
            token = ts.token();
            boolean finished = false;
            while (!finished && token != null) {
                int tokenIntId = ((JsTokenId)token.id()).ordinal();
                if (token.id() == JsTokenId.BRACKET_LEFT_PAREN || token.id() == JsTokenId.BRACKET_LEFT_BRACKET) {
                    if (tokenIntId == leftBracketIntId && ++bracketBalance == 0) {
                        if (braceBalance != 0) {
                            bracketBalance = 1;
                        }
                        finished = true;
                    }
                } else if (token.id() == JsTokenId.BRACKET_RIGHT_PAREN || token.id() == JsTokenId.BRACKET_RIGHT_BRACKET) {
                    if (tokenIntId == bracketIntId) {
                        --bracketBalance;
                    }
                } else if (token.id() == JsTokenId.BRACKET_LEFT_CURLY) {
                    if (++braceBalance > 0) {
                        finished = true;
                    }
                } else if (token.id() == JsTokenId.BRACKET_RIGHT_CURLY) {
                    --braceBalance;
                }
                if (!ts.movePrevious()) break;
                token = ts.token();
            }
            if (bracketBalance != 0 || bracketId == JsTokenId.BRACKET_RIGHT_CURLY && braceBalance < 0) {
                skipClosingBracket = true;
            } else {
                braceBalance = 0;
                bracketBalance = 0;
                TokenHierarchy th = TokenHierarchy.get((Document)doc);
                int ofs = lastRBracket.offset(th);
                ts.move(ofs);
                ts.moveNext();
                token = ts.token();
                finished = false;
                while (!finished && token != null) {
                    if (token.id() == JsTokenId.BRACKET_LEFT_PAREN || token.id() == JsTokenId.BRACKET_LEFT_BRACKET) {
                        if (((JsTokenId)token.id()).ordinal() == leftBracketIntId) {
                            ++bracketBalance;
                        }
                    } else if (token.id() == JsTokenId.BRACKET_RIGHT_PAREN || token.id() == JsTokenId.BRACKET_RIGHT_BRACKET) {
                        if (((JsTokenId)token.id()).ordinal() == bracketIntId && --bracketBalance == 0) {
                            if (braceBalance != 0) {
                                bracketBalance = -1;
                            }
                            finished = true;
                        }
                    } else if (token.id() == JsTokenId.BRACKET_LEFT_CURLY) {
                        ++braceBalance;
                    } else if (token.id() == JsTokenId.BRACKET_RIGHT_CURLY) {
                        --braceBalance;
                    }
                    if (!ts.movePrevious()) break;
                    token = ts.token();
                }
                skipClosingBracket = braceBalance == 0 && bracketId == JsTokenId.BRACKET_RIGHT_CURLY || bracketBalance > 0 && (bracketId == JsTokenId.BRACKET_RIGHT_BRACKET || bracketId == JsTokenId.BRACKET_RIGHT_PAREN);
            }
        }
        return skipClosingBracket;
    }

    private static boolean isEscapeSequence(BaseDocument doc, int dotPos) throws BadLocationException {
        if (dotPos <= 0) {
            return false;
        }
        char previousChar = doc.getChars(dotPos - 1, 1)[0];
        return previousChar == '\\';
    }

    private static char matching(char bracket) {
        switch (bracket) {
            case '(': {
                return ')';
            }
            case '[': {
                return ']';
            }
            case '\"': {
                return '\"';
            }
            case '\'': {
                return '\'';
            }
            case '{': {
                return '}';
            }
            case '}': {
                return '{';
            }
        }
        return bracket;
    }

    private static boolean isCompletableStringBoundary(Token<? extends JsTokenId> token, boolean singleQuote, boolean end) {
        if ((!end && token.id() == JsTokenId.STRING_BEGIN || end && token.id() == JsTokenId.STRING_END) && (singleQuote || "\"".equals(token.text().toString()))) {
            return true;
        }
        return false;
    }

    public static class JsonFactory
    implements TypedTextInterceptor.Factory {
        public TypedTextInterceptor createTypedTextInterceptor(MimePath mimePath) {
            return new JsTypedTextInterceptor(JsTokenId.jsonLanguage(), false);
        }
    }

    public static class JsFactory
    implements TypedTextInterceptor.Factory {
        public TypedTextInterceptor createTypedTextInterceptor(MimePath mimePath) {
            return new JsTypedTextInterceptor(JsTokenId.javascriptLanguage(), true);
        }
    }

}

