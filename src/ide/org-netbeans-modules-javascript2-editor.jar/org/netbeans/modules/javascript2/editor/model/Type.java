/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.model;

public interface Type {
    public static final String BOOLEAN = "Boolean";
    public static final String NUMBER = "Number";
    public static final String STRING = "String";
    public static final String ARRAY = "Array";
    public static final String REGEXP = "RegExp";
    public static final String FUNCTION = "Function";
    public static final String UNRESOLVED = "unresolved";
    public static final String UNDEFINED = "undefined";
    public static final String NULL = "null";
    public static final String OBJECT = "Object";

    public String getType();

    public int getOffset();

    public String getDisplayName();
}

