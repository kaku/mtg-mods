/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.sdoc.elements;

import java.util.List;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.Type;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocBaseElement;
import org.netbeans.modules.javascript2.editor.sdoc.elements.SDocElementType;

public class SDocTypeSimpleElement
extends SDocBaseElement
implements DocParameter {
    protected final List<Type> declaredTypes;

    protected SDocTypeSimpleElement(SDocElementType type, List<Type> declaredTypes) {
        super(type);
        this.declaredTypes = declaredTypes;
    }

    public static SDocTypeSimpleElement create(SDocElementType type, List<Type> declaredTypes) {
        return new SDocTypeSimpleElement(type, declaredTypes);
    }

    public List<Type> getDeclaredTypes() {
        return this.declaredTypes;
    }

    @Override
    public Identifier getParamName() {
        return null;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public boolean isOptional() {
        return false;
    }

    @Override
    public String getParamDescription() {
        return "";
    }

    @Override
    public List<Type> getParamTypes() {
        return this.declaredTypes;
    }
}

