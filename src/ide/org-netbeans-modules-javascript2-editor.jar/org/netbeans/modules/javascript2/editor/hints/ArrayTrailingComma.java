/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.hints;

import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.JsConventionHint;

public class ArrayTrailingComma
extends JsConventionHint {
    public String getId() {
        return "jsarraytrailingcomma.hint";
    }

    public String getDescription() {
        return Bundle.ArrayTrailingCommaDescription();
    }

    public String getDisplayName() {
        return Bundle.ArrayTrailingCommaDisplayName();
    }
}

