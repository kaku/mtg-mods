/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.HtmlFormatter
 */
package org.netbeans.modules.javascript2.editor.sdoc.completion;

import org.netbeans.modules.csl.api.HtmlFormatter;
import org.netbeans.modules.javascript2.editor.doc.spi.AnnotationCompletionTag;

public class TypeDescribedTag
extends AnnotationCompletionTag {
    public static final String TEMPLATE = " {${type}} ${description}";

    public TypeDescribedTag(String name) {
        super(name, name + " {${type}} ${description}");
    }

    @Override
    public void formatParameters(HtmlFormatter formatter) {
        formatter.appendText(" {");
        formatter.parameters(true);
        formatter.appendText("type");
        formatter.parameters(false);
        formatter.appendText("}");
        formatter.appendText(" ");
        formatter.parameters(true);
        formatter.appendText("description");
        formatter.parameters(false);
    }
}

