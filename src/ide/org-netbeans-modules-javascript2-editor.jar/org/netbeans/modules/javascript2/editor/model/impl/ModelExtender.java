/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.impl.ModelElementFactoryAccessor;
import org.netbeans.modules.javascript2.editor.spi.model.FunctionInterceptor;
import org.netbeans.modules.javascript2.editor.spi.model.ModelElementFactory;
import org.netbeans.modules.javascript2.editor.spi.model.ModelInterceptor;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.Lookups;

public final class ModelExtender {
    public static final String MODEL_INTERCEPTORS_PATH = "JavaScript/Model/ModelInterceptors";
    public static final String FUNCTION_INTERCEPTORS_PATH = "JavaScript/Model/FunctionInterceptors";
    private static final Lookup.Result<ModelInterceptor> MODEL_INTERCEPTORS = Lookups.forPath((String)"JavaScript/Model/ModelInterceptors").lookupResult(ModelInterceptor.class);
    private static final Lookup.Result<FunctionInterceptor> FUNCTION_INTERCEPTORS = Lookups.forPath((String)"JavaScript/Model/FunctionInterceptors").lookupResult(FunctionInterceptor.class);
    private static ModelExtender instance;
    private List<JsObject> extendingObjects;

    private ModelExtender() {
    }

    public static synchronized ModelExtender getDefault() {
        if (instance == null) {
            instance = new ModelExtender();
            MODEL_INTERCEPTORS.addLookupListener(new LookupListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                public void resultChanged(LookupEvent ev) {
                    ModelExtender modelExtender = instance;
                    synchronized (modelExtender) {
                        instance.extendingObjects = null;
                    }
                }
            });
        }
        return instance;
    }

    public List<FunctionInterceptor> getFunctionInterceptors() {
        return new ArrayList<FunctionInterceptor>(FUNCTION_INTERCEPTORS.allInstances());
    }

    public synchronized List<? extends JsObject> getExtendingGlobalObjects() {
        if (this.extendingObjects == null) {
            Collection interceptors = MODEL_INTERCEPTORS.allInstances();
            this.extendingObjects = new ArrayList<JsObject>(interceptors.size());
            for (ModelInterceptor interceptor : interceptors) {
                this.extendingObjects.addAll(interceptor.interceptGlobal(ModelElementFactoryAccessor.getDefault().createModelElementFactory()));
            }
        }
        return this.extendingObjects;
    }

}

