/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.formatter.ui.json;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import java.io.InputStream;
import javax.accessibility.AccessibleContext;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.LayoutStyle;
import org.netbeans.modules.javascript2.editor.formatter.FmtOptions;
import org.netbeans.modules.javascript2.editor.formatter.Utils;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FmtWrapping
extends JPanel
implements FocusListener {
    private JComboBox arrayInitCombo;
    private JComboBox arrayInitItemsCombo;
    private JLabel arrayInitItemsLabel;
    private JLabel arrayInitLabel;
    private JComboBox objectsCombo;
    private JLabel objectsLabel;
    private JPanel panel1;
    private JComboBox propertiesCombo;
    private JLabel propertiesLabel;
    private JScrollPane scrollPane;

    public FmtWrapping() {
        this.initComponents();
        this.scrollPane.getViewport().setBackground(SystemColor.controlLtHighlight);
        this.arrayInitCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapArrayInit");
        this.arrayInitCombo.addFocusListener(this);
        this.arrayInitItemsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapArrayInitItems");
        this.arrayInitItemsCombo.addFocusListener(this);
        this.objectsCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapObjects");
        this.objectsCombo.addFocusListener(this);
        this.propertiesCombo.putClientProperty("org.netbeans.modules.javascript2.editor.formatter.FormatingOptions.ID", "wrapProperties");
        this.propertiesCombo.addFocusListener(this);
        Dimension dimension = new Dimension((int)this.panel1.getPreferredSize().getWidth() + 30, (int)this.scrollPane.getMinimumSize().getHeight());
        this.scrollPane.setMinimumSize(dimension);
    }

    public static PreferencesCustomizer.Factory getController() {
        String preview = "";
        try {
            preview = Utils.loadPreviewText(FmtWrapping.class.getClassLoader().getResourceAsStream("org/netbeans/modules/javascript2/editor/formatter/ui/json/Wrapping.json"));
        }
        catch (IOException ex) {
            // empty catch block
        }
        return new FmtOptions.CategorySupport.Factory("text/x-json", "wrapping", FmtWrapping.class, preview, new String[0][]);
    }

    @Override
    public void focusGained(FocusEvent e) {
        this.scrollPane.getViewport().scrollRectToVisible(e.getComponent().getBounds());
    }

    @Override
    public void focusLost(FocusEvent e) {
    }

    private void initComponents() {
        this.scrollPane = new JScrollPane();
        this.panel1 = new JPanel();
        this.arrayInitLabel = new JLabel();
        this.arrayInitCombo = new JComboBox();
        this.objectsLabel = new JLabel();
        this.objectsCombo = new JComboBox();
        this.propertiesLabel = new JLabel();
        this.propertiesCombo = new JComboBox();
        this.arrayInitItemsLabel = new JLabel();
        this.arrayInitItemsCombo = new JComboBox();
        this.setName(NbBundle.getMessage(FmtWrapping.class, (String)"LBL_Wrapping"));
        this.setOpaque(false);
        this.setLayout(new BorderLayout());
        this.scrollPane.setMinimumSize(new Dimension(300, 200));
        this.panel1.setFocusCycleRoot(true);
        this.panel1.setFocusTraversalPolicy(new FocusTraversalPolicy(){

            @Override
            public Component getDefaultComponent(Container focusCycleRoot) {
                return FmtWrapping.this.arrayInitCombo;
            }

            @Override
            public Component getFirstComponent(Container focusCycleRoot) {
                return FmtWrapping.this.arrayInitCombo;
            }

            @Override
            public Component getLastComponent(Container focusCycleRoot) {
                return FmtWrapping.this.propertiesCombo;
            }

            @Override
            public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
                if (aComponent == FmtWrapping.this.arrayInitCombo) {
                    return FmtWrapping.this.arrayInitItemsCombo;
                }
                if (aComponent == FmtWrapping.this.arrayInitItemsCombo) {
                    return FmtWrapping.this.objectsCombo;
                }
                if (aComponent == FmtWrapping.this.objectsCombo) {
                    return FmtWrapping.this.propertiesCombo;
                }
                return FmtWrapping.this.arrayInitCombo;
            }

            @Override
            public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
                if (aComponent == FmtWrapping.this.arrayInitItemsCombo) {
                    return FmtWrapping.this.arrayInitCombo;
                }
                if (aComponent == FmtWrapping.this.objectsCombo) {
                    return FmtWrapping.this.arrayInitItemsCombo;
                }
                if (aComponent == FmtWrapping.this.propertiesCombo) {
                    return FmtWrapping.this.objectsCombo;
                }
                return FmtWrapping.this.propertiesCombo;
            }
        });
        this.arrayInitLabel.setLabelFor(this.arrayInitCombo);
        Mnemonics.setLocalizedText((JLabel)this.arrayInitLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_arrayInit"));
        this.arrayInitCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.objectsLabel.setLabelFor(this.objectsCombo);
        Mnemonics.setLocalizedText((JLabel)this.objectsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"LBL_wrp_objects"));
        this.objectsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.propertiesLabel.setLabelFor(this.propertiesCombo);
        Mnemonics.setLocalizedText((JLabel)this.propertiesLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesLabel.text"));
        this.propertiesCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.arrayInitItemsLabel.setLabelFor(this.arrayInitItemsCombo);
        Mnemonics.setLocalizedText((JLabel)this.arrayInitItemsLabel, (String)NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitItemsLabel.text"));
        this.arrayInitItemsCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GroupLayout panel1Layout = new GroupLayout(this.panel1);
        this.panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panel1Layout.createSequentialGroup().addContainerGap().addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panel1Layout.createSequentialGroup().addComponent(this.arrayInitLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 138, 32767).addComponent(this.arrayInitCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.arrayInitItemsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.arrayInitItemsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.objectsLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.objectsCombo, -2, -1, -2)).addGroup(panel1Layout.createSequentialGroup().addComponent(this.propertiesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.propertiesCombo, -2, -1, -2))).addContainerGap()));
        panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(panel1Layout.createSequentialGroup().addContainerGap().addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.arrayInitLabel).addComponent(this.arrayInitCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.arrayInitItemsLabel).addComponent(this.arrayInitItemsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.objectsLabel).addComponent(this.objectsCombo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.propertiesLabel).addComponent(this.propertiesCombo, -2, -1, -2)).addContainerGap(361, 32767)));
        this.arrayInitLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitLabel.AccessibleContext.accessibleName"));
        this.arrayInitLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitLabel.AccessibleContext.accessibleDescription"));
        this.arrayInitCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitCombo.AccessibleContext.accessibleName"));
        this.arrayInitCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitCombo.AccessibleContext.accessibleDescription"));
        this.objectsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsLabel.AccessibleContext.accessibleName"));
        this.objectsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsLabel.AccessibleContext.accessibleDescription"));
        this.objectsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsCombo.AccessibleContext.accessibleName"));
        this.objectsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.objectsCombo.AccessibleContext.accessibleDescription"));
        this.propertiesLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesLabel.AccessibleContext.accessibleName"));
        this.propertiesLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesLabel.AccessibleContext.accessibleDescription"));
        this.propertiesCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesCombo.AccessibleContext.accessibleName"));
        this.propertiesCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.propertiesCombo.AccessibleContext.accessibleDescription"));
        this.arrayInitItemsLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitLabel.AccessibleContext.accessibleName"));
        this.arrayInitItemsLabel.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitLabel.AccessibleContext.accessibleDescription"));
        this.arrayInitItemsCombo.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitCombo.AccessibleContext.accessibleName"));
        this.arrayInitItemsCombo.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.arrayInitCombo.AccessibleContext.accessibleDescription"));
        this.scrollPane.setViewportView(this.panel1);
        this.panel1.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.panel1.AccessibleContext.accessibleName"));
        this.panel1.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.panel1.AccessibleContext.accessibleDescription"));
        this.add((Component)this.scrollPane, "Center");
        this.scrollPane.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.scrollPane.AccessibleContext.accessibleName"));
        this.scrollPane.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.scrollPane.AccessibleContext.accessibleDescription"));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FmtWrapping.class, (String)"FmtWrapping.AccessibleContext.accessibleDescription"));
    }

}

