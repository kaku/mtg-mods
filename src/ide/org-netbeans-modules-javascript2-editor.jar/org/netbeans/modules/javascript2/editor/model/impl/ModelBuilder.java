/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.spi.ParserResult
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.model.impl;

import java.util.Stack;
import org.netbeans.modules.csl.spi.ParserResult;
import org.netbeans.modules.javascript2.editor.model.JsObject;
import org.netbeans.modules.javascript2.editor.model.JsWith;
import org.netbeans.modules.javascript2.editor.model.impl.DeclarationScopeImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsFunctionImpl;
import org.netbeans.modules.javascript2.editor.model.impl.JsObjectImpl;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public final class ModelBuilder {
    private final JsFunctionImpl globalObject;
    private Stack<JsObjectImpl> stack;
    private Stack<DeclarationScopeImpl> functionStack;
    private int anonymObjectCount;
    private int withObjectCount;
    private JsWith currentWith;
    protected static final String WITH_OBJECT_NAME_START = "With$";
    protected static final String ANONYMOUS_OBJECT_NAME_START = "Anonym$";

    ModelBuilder(JsFunctionImpl globalObject) {
        this.globalObject = globalObject;
        this.stack = new Stack();
        this.functionStack = new Stack();
        this.anonymObjectCount = 0;
        this.withObjectCount = 0;
        this.setCurrentObject(globalObject);
        this.currentWith = null;
    }

    JsObjectImpl getGlobal() {
        return this.globalObject;
    }

    JsObjectImpl getCurrentObject() {
        return this.stack.isEmpty() ? this.globalObject : this.stack.peek();
    }

    DeclarationScopeImpl getCurrentDeclarationScope() {
        return this.functionStack.isEmpty() ? this.globalObject : this.functionStack.peek();
    }

    JsFunctionImpl getCurrentDeclarationFunction() {
        JsObject declarationScope;
        for (declarationScope = this.getCurrentDeclarationScope(); declarationScope != null && declarationScope.getParent() != null && !(declarationScope instanceof JsFunctionImpl); declarationScope = declarationScope.getParent()) {
        }
        if (declarationScope == null) {
            declarationScope = this.globalObject;
        }
        return (JsFunctionImpl)declarationScope;
    }

    void setCurrentObject(JsObjectImpl object) {
        this.stack.push(object);
        if (object instanceof DeclarationScopeImpl) {
            this.functionStack.push((DeclarationScopeImpl)object);
        }
        if (object instanceof JsWith) {
            this.currentWith = (JsWith)((Object)object);
        }
    }

    void reset() {
        if (!this.stack.empty()) {
            JsObject object = this.stack.pop();
            if (object instanceof DeclarationScopeImpl && !this.functionStack.empty()) {
                this.functionStack.pop();
            }
            if (object instanceof JsWith && this.currentWith != null) {
                this.currentWith = this.currentWith.getOuterWith();
            }
        }
    }

    String getUnigueNameForAnonymObject(ParserResult parserResult) {
        FileObject fo = parserResult.getSnapshot().getSource().getFileObject();
        if (fo != null) {
            return fo.getName() + "Anonym$" + this.anonymObjectCount++;
        }
        return "Anonym$" + this.anonymObjectCount++;
    }

    String getUnigueNameForWithObject() {
        return "With$" + this.withObjectCount++;
    }

    public JsWith getCurrentWith() {
        return this.currentWith;
    }
}

