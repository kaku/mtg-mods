/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.jsdoc.model;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.netbeans.modules.javascript2.editor.jsdoc.model.JsDocElement;

public enum JsDocElementType {
    CONTEXT_SENSITIVE("contextSensitive", JsDocElement.Category.DESCRIPTION),
    UNKNOWN("unknown", JsDocElement.Category.UNKNOWN),
    ARGUMENT("@argument", JsDocElement.Category.NAMED_PARAMETER),
    AUGMENTS("@augments", JsDocElement.Category.DECLARATION),
    AUTHOR("@author", JsDocElement.Category.DESCRIPTION),
    BORROWS("@borrows", JsDocElement.Category.ASSIGN),
    CLASS("@class", JsDocElement.Category.DESCRIPTION),
    CONSTANT("@constant", JsDocElement.Category.SIMPLE),
    CONSTRUCTOR("@constructor", JsDocElement.Category.SIMPLE),
    CONSTRUCTS("@constructs", JsDocElement.Category.SIMPLE),
    DEFAULT("@default", JsDocElement.Category.DESCRIPTION),
    DEPRECATED("@deprecated", JsDocElement.Category.DESCRIPTION),
    DESCRIPTION("@description", JsDocElement.Category.DESCRIPTION),
    EVENT("@event", JsDocElement.Category.SIMPLE),
    EXAMPLE("@example", JsDocElement.Category.DESCRIPTION),
    EXTENDS("@extends", JsDocElement.Category.DECLARATION),
    FIELD("@field", JsDocElement.Category.SIMPLE),
    FILE_OVERVIEW("@fileOverview", JsDocElement.Category.DESCRIPTION),
    FUNCTION("@function", JsDocElement.Category.SIMPLE),
    IGNORE("@ignore", JsDocElement.Category.SIMPLE),
    INNER("@inner", JsDocElement.Category.SIMPLE),
    LENDS("@lends", JsDocElement.Category.LINK),
    LINK("@link", JsDocElement.Category.DESCRIPTION),
    MEMBER_OF("@memberOf", JsDocElement.Category.LINK),
    NAME("@name", JsDocElement.Category.LINK),
    NAMESPACE("@namespace", JsDocElement.Category.DESCRIPTION),
    PARAM("@param", JsDocElement.Category.NAMED_PARAMETER),
    PRIVATE("@private", JsDocElement.Category.SIMPLE),
    PROPERTY("@property", JsDocElement.Category.NAMED_PARAMETER),
    PUBLIC("@public", JsDocElement.Category.SIMPLE),
    REQUIRES("@requires", JsDocElement.Category.DESCRIPTION),
    RETURN("@return", JsDocElement.Category.UNNAMED_PARAMETER),
    RETURNS("@returns", JsDocElement.Category.UNNAMED_PARAMETER),
    SEE("@see", JsDocElement.Category.DESCRIPTION),
    SINCE("@since", JsDocElement.Category.DESCRIPTION),
    STATIC("@static", JsDocElement.Category.SIMPLE),
    SYNTAX("@syntax", JsDocElement.Category.DESCRIPTION),
    THROWS("@throws", JsDocElement.Category.UNNAMED_PARAMETER),
    TYPE("@type", JsDocElement.Category.DECLARATION),
    VERSION("@version", JsDocElement.Category.DESCRIPTION);
    
    private final String value;
    private final JsDocElement.Category category;
    private static Map<String, JsDocElementType> types;

    private JsDocElementType(String textValue, JsDocElement.Category category) {
        this.value = textValue;
        this.category = category;
    }

    public String toString() {
        return this.value;
    }

    public JsDocElement.Category getCategory() {
        return this.category;
    }

    public static synchronized JsDocElementType fromString(String value) {
        JsDocElementType type;
        if (types == null) {
            types = new HashMap<String, JsDocElementType>();
            for (JsDocElementType type2 : JsDocElementType.values()) {
                types.put(type2.toString().toLowerCase(Locale.ENGLISH), type2);
            }
        }
        if (value != null && (type = types.get(value.toLowerCase(Locale.ENGLISH))) != null) {
            return type;
        }
        return UNKNOWN;
    }

    static {
        types = null;
    }
}

