/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.csl.api.OffsetRange
 */
package org.netbeans.modules.javascript2.editor.doc.spi;

import java.util.List;
import java.util.Set;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.javascript2.editor.doc.JsDocumentationPrinter;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsModifier;
import org.netbeans.modules.javascript2.editor.model.Type;

public abstract class JsComment {
    private final OffsetRange offsetRange;

    public JsComment(OffsetRange offsetRange) {
        this.offsetRange = offsetRange;
    }

    public OffsetRange getOffsetRange() {
        return this.offsetRange;
    }

    public final String getDocumentation() {
        return JsDocumentationPrinter.printDocumentation(this);
    }

    public abstract List<String> getSummary();

    public abstract List<String> getSyntax();

    public abstract DocParameter getReturnType();

    public abstract List<DocParameter> getParameters();

    public abstract String getDeprecated();

    public abstract List<DocParameter> getThrows();

    public abstract List<Type> getExtends();

    public abstract List<String> getSee();

    public abstract String getSince();

    public abstract List<String> getExamples();

    public abstract Set<JsModifier> getModifiers();

    public abstract boolean isClass();
}

