/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  jdk.nashorn.internal.ir.Block
 *  jdk.nashorn.internal.ir.FunctionNode
 *  jdk.nashorn.internal.ir.IdentNode
 *  jdk.nashorn.internal.ir.Node
 *  jdk.nashorn.internal.ir.visitor.NodeVisitor
 *  org.netbeans.modules.csl.api.Hint
 *  org.netbeans.modules.csl.api.HintSeverity
 *  org.netbeans.modules.csl.api.HintsProvider
 *  org.netbeans.modules.csl.api.HintsProvider$HintsManager
 *  org.netbeans.modules.csl.api.OffsetRange
 *  org.netbeans.modules.csl.api.Rule
 *  org.netbeans.modules.csl.api.Rule$AstRule
 *  org.netbeans.modules.csl.api.Rule$UserConfigurableRule
 *  org.netbeans.modules.csl.api.RuleContext
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import jdk.nashorn.internal.ir.Block;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.ir.IdentNode;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import org.netbeans.modules.csl.api.Hint;
import org.netbeans.modules.csl.api.HintSeverity;
import org.netbeans.modules.csl.api.HintsProvider;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.Rule;
import org.netbeans.modules.csl.api.RuleContext;
import org.netbeans.modules.javascript2.editor.doc.spi.DocParameter;
import org.netbeans.modules.javascript2.editor.doc.spi.JsComment;
import org.netbeans.modules.javascript2.editor.doc.spi.JsDocumentationHolder;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.javascript2.editor.hints.IncorrectDocumentationRule;
import org.netbeans.modules.javascript2.editor.hints.JsAstRule;
import org.netbeans.modules.javascript2.editor.hints.JsHintsProvider;
import org.netbeans.modules.javascript2.editor.hints.UndocumentedParameterRule;
import org.netbeans.modules.javascript2.editor.model.Identifier;
import org.netbeans.modules.javascript2.editor.model.impl.ModelUtils;
import org.netbeans.modules.javascript2.editor.model.impl.PathNodeVisitor;
import org.netbeans.modules.javascript2.editor.parser.JsParserResult;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.openide.filesystems.FileObject;

public class JsFunctionDocumentationRule
extends JsAstRule {
    public static final String JSDOCUMENTATION_OPTION_HINTS = "jsdocumentation.option.hints";

    @Override
    void computeHints(JsHintsProvider.JsRuleContext context, List<Hint> hints, int offset, HintsProvider.HintsManager manager) {
        Map allHints = manager.getHints();
        List conventionHints = (List)allHints.get("jsdocumentation.option.hints");
        Rule.AstRule undocumentedParameterRule = null;
        Rule.AstRule incorrectDocumentationRule = null;
        if (conventionHints != null) {
            for (Rule.AstRule astRule : conventionHints) {
                if (!manager.isEnabled((Rule.UserConfigurableRule)astRule)) continue;
                if (astRule instanceof UndocumentedParameterRule) {
                    undocumentedParameterRule = astRule;
                    continue;
                }
                if (!(astRule instanceof IncorrectDocumentationRule)) continue;
                incorrectDocumentationRule = astRule;
            }
        }
        JsFunctionDocumentationVisitor conventionVisitor = new JsFunctionDocumentationVisitor((Rule)undocumentedParameterRule, (Rule)incorrectDocumentationRule);
        conventionVisitor.process(context, hints);
    }

    @Override
    public boolean appliesTo(RuleContext context) {
        if (context instanceof JsHintsProvider.JsRuleContext) {
            return true;
        }
        return false;
    }

    @Override
    public boolean showInTasklist() {
        return true;
    }

    @Override
    public HintSeverity getDefaultSeverity() {
        return HintSeverity.WARNING;
    }

    @Override
    public boolean getDefaultEnabled() {
        return true;
    }

    @Override
    public JComponent getCustomizer(Preferences node) {
        return null;
    }

    public Set<?> getKinds() {
        return Collections.singleton("jsdocumentation.option.hints");
    }

    public String getId() {
        return "jsdocumentation.hint";
    }

    public String getDescription() {
        return Bundle.JsDocumentationHintDesc();
    }

    public String getDisplayName() {
        return Bundle.JsDocumentationHintDisplayName();
    }

    private static final class JsFunctionDocumentationVisitor
    extends PathNodeVisitor {
        private List<Hint> hints;
        private JsHintsProvider.JsRuleContext context;
        private final Rule undocumentedParameterRule;
        private final Rule incorrectDocumentationRule;

        private JsFunctionDocumentationVisitor(Rule undocumentedParameterRule, Rule incorrectDocumentationRule) {
            this.incorrectDocumentationRule = incorrectDocumentationRule;
            this.undocumentedParameterRule = undocumentedParameterRule;
        }

        public void process(JsHintsProvider.JsRuleContext context, List<Hint> hints) {
            this.hints = hints;
            this.context = context;
            FunctionNode root = context.getJsParserResult().getRoot();
            if (root != null) {
                context.getJsParserResult().getRoot().accept((NodeVisitor)this);
            }
        }

        @Override
        public Node enter(FunctionNode fn) {
            String superfluousParameters;
            JsDocumentationHolder docHolder = this.context.getJsParserResult().getDocumentationHolder();
            if (fn.getParent() == null || docHolder.getCommentForOffset(fn.getStart(), docHolder.getCommentBlocks()) == null) {
                return super.enter(fn);
            }
            List<DocParameter> docParameters = docHolder.getParameters((Node)fn);
            List funcParameters = fn.getParameters();
            String missingParameters = this.missingParameters(funcParameters, docParameters);
            if (!missingParameters.isEmpty() && this.undocumentedParameterRule != null) {
                this.hints.add(new Hint(this.undocumentedParameterRule, Bundle.UndocumentedParameterRuleDisplayDescription(missingParameters), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), ModelUtils.documentOffsetRange(this.context.getJsParserResult(), fn.getIdent().getStart(), fn.getIdent().getFinish()), null, 600));
            }
            if (!(superfluousParameters = this.superfluousParameters(funcParameters, docParameters)).isEmpty() && this.incorrectDocumentationRule != null) {
                this.hints.add(new Hint(this.incorrectDocumentationRule, Bundle.IncorrectDocumentationRuleDisplayDescription(superfluousParameters), this.context.getJsParserResult().getSnapshot().getSource().getFileObject(), ModelUtils.documentOffsetRange(this.context.getJsParserResult(), fn.getIdent().getStart(), fn.getIdent().getFinish()), null, 600));
            }
            return super.enter(fn);
        }

        private String missingParameters(List<IdentNode> functionParams, List<DocParameter> documentationParams) {
            StringBuilder sb = new StringBuilder();
            String delimiter = "";
            for (IdentNode identNode : functionParams) {
                if (this.containFunctionParamName(documentationParams, identNode.getName())) continue;
                sb.append(delimiter).append(identNode.getName());
                delimiter = ", ";
            }
            return sb.toString();
        }

        private boolean containFunctionParamName(List<DocParameter> documentationParams, String functionParamName) {
            for (DocParameter docParameter : documentationParams) {
                if (docParameter.getParamName() == null || !docParameter.getParamName().getName().equals(functionParamName)) continue;
                return true;
            }
            return false;
        }

        private String superfluousParameters(List<IdentNode> functionParams, List<DocParameter> documentationParams) {
            StringBuilder sb = new StringBuilder();
            String delimiter = "";
            for (DocParameter docParameter : documentationParams) {
                Identifier paramName;
                if (docParameter.isOptional() || (paramName = docParameter.getParamName()) == null || this.containDocParamName(functionParams, paramName.getName())) continue;
                sb.append(delimiter).append(docParameter.getParamName().getName());
                delimiter = ", ";
            }
            return sb.toString();
        }

        private boolean containDocParamName(List<IdentNode> functionParams, String documentationParamName) {
            for (IdentNode identNode : functionParams) {
                if (!identNode.getName().equals(documentationParamName)) continue;
                return true;
            }
            return false;
        }
    }

}

