/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.csl.api.HintFix
 *  org.netbeans.modules.editor.NbEditorDocument
 *  org.netbeans.modules.parsing.api.Snapshot
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.api.indexing.IndexingManager
 *  org.netbeans.modules.parsing.spi.Parser
 *  org.netbeans.modules.parsing.spi.Parser$Result
 *  org.netbeans.spi.lexer.MutableTextInput
 *  org.netbeans.spi.lexer.TokenHierarchyControl
 *  org.openide.cookies.EditorCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.javascript2.editor.hints;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.csl.api.HintFix;
import org.netbeans.modules.editor.NbEditorDocument;
import org.netbeans.modules.javascript2.editor.hints.Bundle;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.api.indexing.IndexingManager;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenHierarchyControl;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

public final class ErrorCheckingSupport {
    private static RequestProcessor RP = new RequestProcessor(ErrorCheckingSupport.class);
    private static final String DISABLE_ERROR_CHECKS_KEY = ErrorCheckingSupport.class.getName() + ".disableErrorChecking";

    public static boolean isErrorCheckingEnabled(Parser.Result result, String mimeType) {
        return ErrorCheckingSupport.isErrorCheckingEnabledForFile(result) && ErrorCheckingSupport.isErrorCheckingEnabledForMimetype(mimeType);
    }

    public static boolean isErrorCheckingEnabledForFile(Parser.Result result) {
        FileObject fo = result.getSnapshot().getSource().getFileObject();
        return fo == null || fo.getAttribute(DISABLE_ERROR_CHECKS_KEY) == null;
    }

    public static boolean isErrorCheckingEnabledForMimetype(String mimeType) {
        Preferences prefs = NbPreferences.forModule(ErrorCheckingSupport.class);
        boolean enabled = ErrorCheckingSupport.isHtmlMimeType(mimeType);
        Preferences user = prefs.node(ErrorCheckingSupport.class.getName());
        enabled = user.getBoolean(mimeType, enabled);
        return enabled;
    }

    public static void setErrorCheckingEnabledForMimetype(String mimeType, boolean enabled) {
        Preferences prefs = NbPreferences.forModule(ErrorCheckingSupport.class);
        Preferences user = prefs.node(ErrorCheckingSupport.class.getName());
        if (ErrorCheckingSupport.isHtmlMimeType(mimeType) && enabled) {
            user.remove(mimeType);
        } else {
            user.putBoolean(mimeType, enabled);
        }
    }

    public static HintFix createErrorFixForFile(Snapshot snapshot, boolean enable) {
        return new ErrorChecksFileFix(snapshot, enable);
    }

    public static HintFix createErrorFixForMimeType(Snapshot snapshot, String mimeType, boolean enable) {
        return new ErrorChecksMimeTypeFix(snapshot, mimeType, enable);
    }

    private static boolean containsHtml(Snapshot snapshot) {
        for (MimePath path : snapshot.getMimePath().getIncludedPaths()) {
            if (!path.getPath().startsWith("text/html") && !path.getPath().startsWith("text/xhtml")) continue;
            return true;
        }
        return false;
    }

    private static boolean isHtmlMimeType(String mimetype) {
        return "text/html".equals(mimetype) || "text/xhtml".equals(mimetype);
    }

    private static void reindexFile(final FileObject fo) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                IndexingManager.getDefault().refreshIndexAndWait(fo.getParent().toURL(), Collections.singleton(fo.toURL()), true, false);
            }
        });
    }

    private static void reindexActionItems() {
        RP.post(new Runnable(){

            @Override
            public void run() {
                IndexingManager.getDefault().refreshAllIndices("TLIndexer");
            }
        });
    }

    private static void refreshDocument(final FileObject fo) throws IOException {
        RP.post(new Runnable(){

            @Override
            public void run() {
                try {
                    DataObject dobj = DataObject.find((FileObject)fo);
                    EditorCookie editorCookie = (EditorCookie)dobj.getLookup().lookup(EditorCookie.class);
                    StyledDocument document = editorCookie.openDocument();
                    ErrorCheckingSupport.forceReparse(document);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        });
    }

    private static void forceReparse(final Document doc) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                NbEditorDocument nbdoc = (NbEditorDocument)doc;
                nbdoc.runAtomic(new Runnable(){

                    @Override
                    public void run() {
                        MutableTextInput mti = (MutableTextInput)doc.getProperty(MutableTextInput.class);
                        if (mti != null) {
                            mti.tokenHierarchyControl().rebuild();
                        }
                    }
                });
            }

        });
    }

    private static final class ErrorChecksMimeTypeFix
    implements HintFix {
        private final FileObject fo;
        private final String mimeType;
        private final boolean enable;

        public ErrorChecksMimeTypeFix(Snapshot snapshot, String mimeType, boolean enable) {
            this.fo = snapshot.getSource().getFileObject();
            this.mimeType = mimeType;
            this.enable = enable;
        }

        public String getDescription() {
            if (this.enable) {
                return Bundle.MSG_HINT_ENABLE_ERROR_CHECKS_MIMETYPE(this.mimeType);
            }
            return Bundle.MSG_HINT_DISABLE_ERROR_CHECKS_MIMETYPE(this.mimeType);
        }

        public void implement() throws Exception {
            ErrorCheckingSupport.setErrorCheckingEnabledForMimetype(this.mimeType, this.enable);
            ErrorCheckingSupport.reindexActionItems();
            ErrorCheckingSupport.reindexFile(this.fo);
            ErrorCheckingSupport.refreshDocument(this.fo);
        }

        public boolean isSafe() {
            return true;
        }

        public boolean isInteractive() {
            return false;
        }
    }

    private static final class ErrorChecksFileFix
    implements HintFix {
        private final FileObject fo;
        private final boolean enable;

        public ErrorChecksFileFix(Snapshot snapshot, boolean enable) {
            this.fo = snapshot.getSource().getFileObject();
            this.enable = enable;
        }

        public String getDescription() {
            if (this.enable) {
                return Bundle.MSG_HINT_ENABLE_ERROR_CHECKS_FILE();
            }
            return Bundle.MSG_HINT_DISABLE_ERROR_CHECKS_FILE();
        }

        public void implement() throws Exception {
            if (this.fo == null) {
                return;
            }
            if (this.enable) {
                this.fo.setAttribute(DISABLE_ERROR_CHECKS_KEY, (Object)null);
            } else {
                this.fo.setAttribute(DISABLE_ERROR_CHECKS_KEY, (Object)Boolean.TRUE);
            }
            ErrorCheckingSupport.reindexFile(this.fo);
            ErrorCheckingSupport.refreshDocument(this.fo);
        }

        public boolean isSafe() {
            return true;
        }

        public boolean isInteractive() {
            return false;
        }
    }

}

