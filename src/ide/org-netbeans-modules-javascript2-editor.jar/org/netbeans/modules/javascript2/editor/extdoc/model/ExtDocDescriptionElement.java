/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.javascript2.editor.extdoc.model;

import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocBaseElement;
import org.netbeans.modules.javascript2.editor.extdoc.model.ExtDocElementType;

public class ExtDocDescriptionElement
extends ExtDocBaseElement {
    private final String description;

    private ExtDocDescriptionElement(ExtDocElementType type, String description) {
        super(type);
        this.description = description;
    }

    public static ExtDocDescriptionElement create(ExtDocElementType type, String description) {
        return new ExtDocDescriptionElement(type, description);
    }

    public String getDescription() {
        return this.description;
    }
}

