/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$CustomCustomizer
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer$Factory
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.javascript2.editor.options.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class CodeCompletionPanel
extends JPanel {
    private final Preferences preferences;
    private final ItemListener defaultCheckBoxListener;
    private final ItemListener defaultRadioButtonListener;
    private final ChangeListener defaultChangeListener;
    private final Map<String, Object> id2Saved;
    private JCheckBox autoCompletionAfterDotCheckBox;
    private ButtonGroup autoCompletionButtonGroup;
    private JRadioButton autoCompletionCustomizeRadioButton;
    private JRadioButton autoCompletionFullRadioButton;
    private JCheckBox autoCompletionSmartQuotesCheckBox;
    private JLabel autoCompletionSmartQuotesLabel;
    private JCheckBox autoCompletionTypeResolutionCheckBox;
    private JLabel autoCompletionTypeResolutionLabel;
    private JCheckBox autoStringConcatenationCheckBox;
    private JLabel codeCompletionSignatureWidthLabel;
    private JSpinner codeCompletionSignatureWidthSpinner;
    private JLabel enableAutocompletionLabel;

    public CodeCompletionPanel(Preferences preferences) {
        this.defaultCheckBoxListener = new DefaultCheckBoxListener();
        this.defaultRadioButtonListener = new DefaultRadioButtonListener();
        this.defaultChangeListener = new DefaultChangeListener();
        this.id2Saved = new HashMap<String, Object>();
        assert (preferences != null);
        this.preferences = preferences;
        this.initComponents();
        this.initAutoCompletion();
    }

    public static PreferencesCustomizer.Factory getCustomizerFactory() {
        return new PreferencesCustomizer.Factory(){

            public PreferencesCustomizer create(Preferences preferences) {
                return new CodeCompletionPreferencesCustomizer(preferences);
            }
        };
    }

    private void initAutoCompletion() {
        boolean codeCompletionTypeResolution = this.preferences.getBoolean("codeCompletionTypeResolution", false);
        this.autoCompletionTypeResolutionCheckBox.setSelected(codeCompletionTypeResolution);
        this.autoCompletionTypeResolutionCheckBox.addItemListener(this.defaultCheckBoxListener);
        boolean codeCompletionSmartQuotes = this.preferences.getBoolean("codeCompletionSmartQuotes", true);
        this.autoCompletionSmartQuotesCheckBox.setSelected(codeCompletionSmartQuotes);
        this.autoCompletionSmartQuotesCheckBox.addItemListener(this.defaultCheckBoxListener);
        boolean codeCompletionStringAutoConcatination = this.preferences.getBoolean("codeCompletionStringAutoConcatination", true);
        this.autoStringConcatenationCheckBox.setSelected(codeCompletionStringAutoConcatination);
        this.autoStringConcatenationCheckBox.addItemListener(this.defaultCheckBoxListener);
        this.autoCompletionFullRadioButton.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == 1) {
                    CodeCompletionPanel.this.setAutoCompletionState(false);
                }
            }
        });
        this.autoCompletionCustomizeRadioButton.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == 1) {
                    CodeCompletionPanel.this.setAutoCompletionState(true);
                }
            }
        });
        boolean autoCompletionFull = this.preferences.getBoolean("autoCompletionFull", false);
        this.autoCompletionFullRadioButton.setSelected(autoCompletionFull);
        this.autoCompletionCustomizeRadioButton.setSelected(!autoCompletionFull);
        this.autoCompletionFullRadioButton.addItemListener(this.defaultRadioButtonListener);
        this.autoCompletionCustomizeRadioButton.addItemListener(this.defaultRadioButtonListener);
        boolean autoCompletionVariables = this.preferences.getBoolean("autoCompletionAfterDot", true);
        this.autoCompletionAfterDotCheckBox.setSelected(autoCompletionVariables);
        this.autoCompletionAfterDotCheckBox.addItemListener(this.defaultCheckBoxListener);
        int codeCompletionItemSignatureWidth = this.preferences.getInt("codeComletionItemDescriptionWith", 40);
        this.codeCompletionSignatureWidthSpinner.setValue(codeCompletionItemSignatureWidth);
        this.codeCompletionSignatureWidthSpinner.addChangeListener(this.defaultChangeListener);
        this.id2Saved.put("codeCompletionTypeResolution", this.autoCompletionTypeResolutionCheckBox.isSelected());
        this.id2Saved.put("codeCompletionSmartQuotes", this.autoCompletionSmartQuotesCheckBox.isSelected());
        this.id2Saved.put("codeCompletionStringAutoConcatination", this.autoStringConcatenationCheckBox.isSelected());
        this.id2Saved.put("autoCompletionFull", this.autoCompletionFullRadioButton.isSelected());
        this.id2Saved.put("autoCompletionAfterDot", this.autoCompletionAfterDotCheckBox.isSelected());
        this.id2Saved.put("codeComletionItemDescriptionWith", this.codeCompletionSignatureWidthSpinner.getValue());
    }

    void setAutoCompletionState(boolean enabled) {
        this.autoCompletionAfterDotCheckBox.setEnabled(enabled);
    }

    void validateData() {
        this.preferences.putBoolean("codeCompletionTypeResolution", this.autoCompletionTypeResolutionCheckBox.isSelected());
        this.preferences.putBoolean("codeCompletionSmartQuotes", this.autoCompletionSmartQuotesCheckBox.isSelected());
        this.preferences.putBoolean("codeCompletionStringAutoConcatination", this.autoStringConcatenationCheckBox.isSelected());
        this.preferences.putBoolean("autoCompletionFull", this.autoCompletionFullRadioButton.isSelected());
        this.preferences.putBoolean("autoCompletionAfterDot", this.autoCompletionAfterDotCheckBox.isSelected());
        this.preferences.putInt("codeComletionItemDescriptionWith", (Integer)this.codeCompletionSignatureWidthSpinner.getValue());
    }

    private void initComponents() {
        this.autoStringConcatenationCheckBox = new JCheckBox();
        this.autoCompletionButtonGroup = new ButtonGroup();
        this.autoCompletionSmartQuotesLabel = new JLabel();
        this.autoCompletionSmartQuotesCheckBox = new JCheckBox();
        this.autoCompletionTypeResolutionLabel = new JLabel();
        this.autoCompletionTypeResolutionCheckBox = new JCheckBox();
        this.enableAutocompletionLabel = new JLabel();
        this.autoCompletionFullRadioButton = new JRadioButton();
        this.autoCompletionCustomizeRadioButton = new JRadioButton();
        this.autoCompletionAfterDotCheckBox = new JCheckBox();
        this.codeCompletionSignatureWidthLabel = new JLabel();
        this.codeCompletionSignatureWidthSpinner = new JSpinner();
        Mnemonics.setLocalizedText((AbstractButton)this.autoStringConcatenationCheckBox, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoStringConcatenationCheckBox.text"));
        Mnemonics.setLocalizedText((JLabel)this.autoCompletionSmartQuotesLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionSmartQuotesLabel.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.autoCompletionSmartQuotesCheckBox, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionSmartQuotesCheckBox.text"));
        Mnemonics.setLocalizedText((JLabel)this.autoCompletionTypeResolutionLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionTypeResolutionLabel.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.autoCompletionTypeResolutionCheckBox, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionTypeResolutionCheckBox.text"));
        Mnemonics.setLocalizedText((JLabel)this.enableAutocompletionLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.enableAutocompletionLabel.text"));
        this.autoCompletionButtonGroup.add(this.autoCompletionFullRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this.autoCompletionFullRadioButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionFullRadioButton.text"));
        this.autoCompletionButtonGroup.add(this.autoCompletionCustomizeRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this.autoCompletionCustomizeRadioButton, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionCustomizeRadioButton.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.autoCompletionAfterDotCheckBox, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.autoCompletionAfterDotCheckBox.text"));
        Mnemonics.setLocalizedText((JLabel)this.codeCompletionSignatureWidthLabel, (String)NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.codeCompletionSignatureWidthLabel.text"));
        this.codeCompletionSignatureWidthSpinner.setMinimumSize(new Dimension(100, 28));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGap(12, 12, 12).addComponent(this.autoCompletionSmartQuotesCheckBox)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.autoCompletionSmartQuotesLabel)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.autoCompletionTypeResolutionCheckBox)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.enableAutocompletionLabel)).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.autoCompletionCustomizeRadioButton, GroupLayout.Alignment.LEADING).addComponent(this.autoCompletionFullRadioButton, GroupLayout.Alignment.LEADING))).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.autoCompletionTypeResolutionLabel)).addGroup(layout.createSequentialGroup().addGap(32, 32, 32).addComponent(this.autoCompletionAfterDotCheckBox, -2, 144, -2)).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.codeCompletionSignatureWidthLabel).addGap(4, 4, 4).addComponent(this.codeCompletionSignatureWidthSpinner, -2, 61, -2))).addContainerGap(30, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.enableAutocompletionLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.autoCompletionFullRadioButton).addGap(1, 1, 1).addComponent(this.autoCompletionCustomizeRadioButton).addGap(1, 1, 1).addComponent(this.autoCompletionAfterDotCheckBox).addGap(18, 18, 18).addComponent(this.autoCompletionTypeResolutionLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.autoCompletionTypeResolutionCheckBox).addGap(18, 18, 18).addComponent(this.autoCompletionSmartQuotesLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.autoCompletionSmartQuotesCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.codeCompletionSignatureWidthLabel).addComponent(this.codeCompletionSignatureWidthSpinner, -2, -1, -2)).addContainerGap(88, 32767)));
        this.getAccessibleContext().setAccessibleName(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.AccessibleContext.accessibleName"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(CodeCompletionPanel.class, (String)"CodeCompletionPanel.AccessibleContext.accessibleDescription"));
    }

    String getSavedValue(String key) {
        return this.id2Saved.get(key).toString();
    }

    public static final class CustomCustomizerImpl
    extends PreferencesCustomizer.CustomCustomizer {
        public String getSavedValue(PreferencesCustomizer customCustomizer, String key) {
            if (customCustomizer instanceof CodeCompletionPreferencesCustomizer) {
                return ((CodeCompletionPanel)customCustomizer.getComponent()).getSavedValue(key);
            }
            return null;
        }
    }

    static final class CodeCompletionPreferencesCustomizer
    implements PreferencesCustomizer {
        private final Preferences preferences;
        private CodeCompletionPanel component;

        private CodeCompletionPreferencesCustomizer(Preferences preferences) {
            this.preferences = preferences;
        }

        public String getId() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getDisplayName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public HelpCtx getHelpCtx() {
            return new HelpCtx("org.netbeans.modules.javascript2.editor.options.CodeCompletionPanel");
        }

        public JComponent getComponent() {
            if (this.component == null) {
                this.component = new CodeCompletionPanel(this.preferences);
            }
            return this.component;
        }
    }

    private final class DefaultChangeListener
    implements ChangeListener,
    Serializable {
        private DefaultChangeListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            CodeCompletionPanel.this.validateData();
        }
    }

    private final class DefaultRadioButtonListener
    implements ItemListener,
    Serializable {
        private DefaultRadioButtonListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == 1) {
                CodeCompletionPanel.this.validateData();
            }
        }
    }

    private final class DefaultCheckBoxListener
    implements ItemListener,
    Serializable {
        private DefaultCheckBoxListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            CodeCompletionPanel.this.validateData();
        }
    }

}

