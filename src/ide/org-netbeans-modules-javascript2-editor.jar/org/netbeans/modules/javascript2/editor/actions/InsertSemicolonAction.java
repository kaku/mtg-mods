/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.javascript2.editor.actions;

import java.awt.event.ActionEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.indent.api.Indent;
import org.openide.util.Exceptions;

public abstract class InsertSemicolonAction
extends BaseAction {
    private static final String SEMICOLON = ";";
    private final NewLineProcessor newLineProcessor;

    protected InsertSemicolonAction(NewLineProcessor newLineProcessor) {
        this.newLineProcessor = newLineProcessor;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        if (target.isEditable() && target.isEnabled()) {
            BaseDocument doc = (BaseDocument)target.getDocument();
            Indent indenter = Indent.get((Document)doc);
            indenter.lock();
            try {
                final class R
                implements Runnable {
                    final /* synthetic */ JTextComponent val$target;
                    final /* synthetic */ BaseDocument val$doc;
                    final /* synthetic */ Indent val$indenter;

                    R() {
                        this.val$target = var2_2;
                        this.val$doc = var3_3;
                        this.val$indenter = var4_4;
                    }

                    @Override
                    public void run() {
                        try {
                            Caret caret = this.val$target.getCaret();
                            int caretPosition = caret.getDot();
                            int eolOffset = Utilities.getRowEnd((JTextComponent)this.val$target, (int)caretPosition);
                            this.val$doc.insertString(eolOffset, ";", null);
                            this$0.newLineProcessor.processNewLine(eolOffset, caret, this.val$indenter);
                        }
                        catch (BadLocationException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                }
                doc.runAtomicAsUser((Runnable)new R(this, target, doc, indenter));
            }
            finally {
                indenter.unlock();
            }
        }
    }

    public static class CompleteLine
    extends InsertSemicolonAction {
        static final String ACTION_NAME = "complete-line";

        public CompleteLine() {
            super(NewLineProcessor.WITHOUT_NEW_LINE);
        }
    }

    public static class CompleteLineNewLine
    extends InsertSemicolonAction {
        static final String ACTION_NAME = "complete-line-newline";

        public CompleteLineNewLine() {
            super(NewLineProcessor.WITH_NEW_LINE);
        }
    }

    public static enum NewLineProcessor {
        WITH_NEW_LINE{

            @Override
            public void processNewLine(int endOfLineOffset, Caret caret, Indent indenter) throws BadLocationException {
                int newCaretPosition = indenter.indentNewLine(endOfLineOffset + ";".length());
                caret.setDot(newCaretPosition);
            }
        }
        ,
        WITHOUT_NEW_LINE{

            @Override
            public void processNewLine(int endOfLineOffset, Caret caret, Indent indenter) throws BadLocationException {
            }
        };
        

        private NewLineProcessor() {
        }

        public abstract void processNewLine(int var1, Caret var2, Indent var3) throws BadLocationException;

    }

}

