/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseImageTokenID
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.Syntax
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.TokenID
 */
package org.netbeans.editor.ext.plain;

import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.ext.plain.PlainTokenContext;

public class PlainSyntax
extends Syntax {
    private static final int ISI_TEXT = 0;

    public PlainSyntax() {
        this.tokenContextPath = PlainTokenContext.contextPath;
    }

    protected TokenID parseToken() {
        while (this.offset < this.stopOffset) {
            char ch = this.buffer[this.offset];
            switch (this.state) {
                case -1: {
                    switch (ch) {
                        case '\n': {
                            ++this.offset;
                            return PlainTokenContext.EOL;
                        }
                    }
                    this.state = 0;
                    break;
                }
                case 0: {
                    switch (ch) {
                        case '\n': {
                            this.state = -1;
                            return PlainTokenContext.TEXT;
                        }
                    }
                }
            }
            ++this.offset;
        }
        switch (this.state) {
            case 0: {
                this.state = -1;
                return PlainTokenContext.TEXT;
            }
        }
        return null;
    }
}

