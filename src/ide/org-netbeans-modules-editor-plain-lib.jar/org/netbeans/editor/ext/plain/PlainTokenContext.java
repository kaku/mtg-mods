/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseImageTokenID
 *  org.netbeans.editor.BaseTokenID
 *  org.netbeans.editor.TokenContext
 *  org.netbeans.editor.TokenContextPath
 *  org.netbeans.editor.Utilities
 */
package org.netbeans.editor.ext.plain;

import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.TokenContext;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.Utilities;

public class PlainTokenContext
extends TokenContext {
    public static final int TEXT_ID = 1;
    public static final int EOL_ID = 2;
    public static final BaseTokenID TEXT = new BaseTokenID("text", 1);
    public static final BaseImageTokenID EOL = new BaseImageTokenID("EOL", 2, "\n");
    public static final PlainTokenContext context = new PlainTokenContext();
    public static final TokenContextPath contextPath = context.getContextPath();

    private PlainTokenContext() {
        super("format-");
        try {
            this.addDeclaredTokenIDs();
        }
        catch (Exception e) {
            Utilities.annotateLoggable((Throwable)e);
        }
    }
}

