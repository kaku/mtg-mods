/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import org.netbeans.lib.editor.util.GapList;

public abstract class FlyOffsetGapList<E>
extends GapList<E> {
    private int offsetGapStart;
    private int offsetGapLength = 1073741823;

    public FlyOffsetGapList() {
        this(10);
    }

    public FlyOffsetGapList(int initialCapacity) {
        super(initialCapacity);
    }

    protected abstract int elementRawOffset(E var1);

    protected abstract void setElementRawOffset(E var1, int var2);

    protected abstract boolean isElementFlyweight(E var1);

    protected abstract int elementLength(E var1);

    protected int startOffset() {
        return 0;
    }

    protected final int elementOffset(int index) {
        int offset;
        E elem = this.get(index);
        if (this.isElementFlyweight(elem)) {
            offset = 0;
            while (--index >= 0) {
                elem = this.get(index);
                offset += this.elementLength(elem);
                if (this.isElementFlyweight(elem)) continue;
                offset += this.raw2RelOffset(this.elementRawOffset(elem));
                break;
            }
        } else {
            offset = this.raw2RelOffset(this.elementRawOffset(elem));
        }
        return this.startOffset() + offset;
    }

    protected final int elementOrEndOffset(int indexOrSize) {
        int offset;
        E elem;
        if (indexOrSize == this.size() || this.isElementFlyweight(elem = this.get(indexOrSize))) {
            offset = 0;
            while (--indexOrSize >= 0) {
                elem = this.get(indexOrSize);
                offset += this.elementLength(elem);
                if (this.isElementFlyweight(elem)) continue;
                offset += this.raw2RelOffset(this.elementRawOffset(elem));
                break;
            }
        } else {
            offset = this.raw2RelOffset(this.elementRawOffset(elem));
        }
        return this.startOffset() + offset;
    }

    public void defaultInsertUpdate(int offset, int length) {
        assert (length >= 0);
        if (offset != this.offsetGapStart()) {
            this.moveOffsetGap(offset, this.findElementIndex(offset));
        }
        this.updateOffsetGapLength(- length);
        this.updateOffsetGapStart(length);
    }

    public void defaultRemoveUpdate(int offset, int length) {
        assert (length >= 0);
        int index = this.findElementIndex(offset);
        if (offset != this.offsetGapStart()) {
            this.moveOffsetGap(offset, index);
        }
        int size = this.size();
        int removeAreaEndRawOffset = offset + this.offsetGapLength + length;
        while (index < size) {
            E elem;
            if (this.isElementFlyweight(elem = this.get(index++))) continue;
            if (this.elementRawOffset(elem) >= removeAreaEndRawOffset) break;
            this.setElementRawOffset(elem, removeAreaEndRawOffset);
        }
        this.updateOffsetGapLength(length);
    }

    protected final void moveOffsetGap(int offset, int index) {
        if (offset < this.offsetGapStart) {
            int bound = this.size();
            for (int i = index; i < bound; ++i) {
                E elem = this.get(i);
                if (this.isElementFlyweight(elem)) continue;
                int rawOffset = this.elementRawOffset(elem);
                if (rawOffset < this.offsetGapStart) {
                    this.setElementRawOffset(elem, rawOffset + this.offsetGapLength);
                    continue;
                }
                break;
            }
        } else {
            for (int i = index - 1; i >= 0; --i) {
                E elem = this.get(i);
                if (this.isElementFlyweight(elem)) continue;
                int rawOffset = this.elementRawOffset(elem);
                if (rawOffset >= this.offsetGapStart) {
                    this.setElementRawOffset(elem, rawOffset - this.offsetGapLength);
                    continue;
                }
                break;
            }
        }
        this.offsetGapStart = offset;
    }

    protected final int offsetGapStart() {
        return this.offsetGapStart;
    }

    protected final void updateOffsetGapStart(int offsetDelta) {
        this.offsetGapStart += offsetDelta;
    }

    protected final int offsetGapLength() {
        return this.offsetGapLength;
    }

    protected final void updateOffsetGapLength(int offsetGapLengthDelta) {
        this.offsetGapLength += offsetGapLengthDelta;
        assert (this.offsetGapLength >= 0);
    }

    protected final int findElementIndex(int offset) {
        int low = 0;
        int high = this.size() - 1;
        while (low <= high) {
            int index = (low + high) / 2;
            int elemOffset = this.elementOffset(index);
            if (elemOffset < offset) {
                low = index + 1;
                continue;
            }
            if (elemOffset > offset) {
                high = index - 1;
                continue;
            }
            while (index > 0) {
                if (this.elementOffset(--index) >= offset) continue;
                ++index;
                break;
            }
            low = index;
            break;
        }
        return low;
    }

    protected void updateElementOffsetAdd(E elem) {
        if (!this.isElementFlyweight(elem)) {
            int offset = this.elementRawOffset(elem);
            this.setElementRawOffset(elem, this.offset2Raw(offset));
        }
    }

    protected void updateElementOffsetRemove(E elem) {
        if (!this.isElementFlyweight(elem)) {
            int rawOffset = this.raw2RelOffset(this.elementRawOffset(elem));
            this.setElementRawOffset(elem, rawOffset += this.startOffset());
        }
    }

    private int raw2RelOffset(int rawOffset) {
        return rawOffset < this.offsetGapStart ? rawOffset : rawOffset - this.offsetGapLength;
    }

    protected final int offset2Raw(int offset) {
        if ((offset -= this.startOffset()) >= this.offsetGapStart) {
            offset += this.offsetGapLength;
        }
        return offset;
    }

    protected void consistencyCheck(boolean checkElementLength) {
        int lastOffset;
        GapList.super.consistencyCheck();
        if (this.offsetGapLength < 0) {
            this.consistencyError("offsetGapLength < 0");
        }
        int lastRawOffset = Integer.MIN_VALUE;
        int lastEndOffset = lastOffset = Integer.MIN_VALUE;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E elem = this.get(i);
            if (!this.isElementFlyweight(elem)) {
                int rawOffset = this.elementRawOffset(elem);
                int offset = this.raw2RelOffset(rawOffset);
                if (rawOffset < lastRawOffset) {
                    this.consistencyError("Invalid rawOffset=" + rawOffset + " >= lastRawOffset=" + lastRawOffset + " at index=" + i);
                }
                if (offset < lastOffset) {
                    this.consistencyError("Invalid offset=" + offset + " >= lastOffset=" + lastOffset + " at index=" + i);
                }
                if (checkElementLength) {
                    int length = this.elementLength(elem);
                    if (i == 0) {
                        lastEndOffset = offset;
                    }
                    if (offset != lastEndOffset) {
                        this.consistencyError("Offset=" + offset + " differs from lastEndOffset=" + lastEndOffset + " at index=" + i);
                    }
                    lastEndOffset += length;
                }
                lastRawOffset = rawOffset;
                lastOffset = offset;
                continue;
            }
            if (!checkElementLength) continue;
            if (i == 0) {
                lastEndOffset = 0;
            }
            int length = this.elementLength(elem);
            lastEndOffset += length;
        }
    }

    @Override
    protected String dumpInternals() {
        return GapList.super.dumpInternals() + ", offGap(s=" + this.offsetGapStart + ", l=" + this.offsetGapLength + ")";
    }
}

