/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

public final class CharacterConversions {
    public static final char LF = '\n';
    public static final char CR = '\r';
    public static final char LS = '\u2028';
    public static final char PS = '\u2029';
    private static String systemDefaultLineSeparator = null;

    public static String lineSeparatorToLineFeed(CharSequence text) {
        if (text == null || text.length() == 0) {
            return "";
        }
        boolean lastCharCR = false;
        StringBuilder output = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); ++i) {
            char ch = text.charAt(i);
            if (lastCharCR && ch == '\n') {
                output.append('\n');
                lastCharCR = false;
                continue;
            }
            if (ch == '\r') {
                lastCharCR = true;
                continue;
            }
            if (ch == '\u2028' || ch == '\u2029') {
                output.append('\n');
                lastCharCR = false;
                continue;
            }
            lastCharCR = false;
            output.append(ch);
        }
        return output.toString();
    }

    public static String lineSeparatorToLineFeed(CharSequence text, CharSequence lineSeparator) {
        if (text == null || text.length() == 0) {
            return "";
        }
        int lineSeparatorIdx = 0;
        StringBuilder output = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); ++i) {
            char lsCh;
            char ch = text.charAt(i);
            if (ch == (lsCh = lineSeparator.charAt(lineSeparatorIdx))) {
                if (lineSeparatorIdx == lineSeparator.length() - 1) {
                    output.append('\n');
                    lineSeparatorIdx = 0;
                    continue;
                }
                ++lineSeparatorIdx;
                continue;
            }
            if (lineSeparatorIdx > 0) {
                output.append(lineSeparator, 0, lineSeparatorIdx);
            }
            output.append(ch);
            lineSeparatorIdx = 0;
        }
        return output.toString();
    }

    public static String lineFeedToLineSeparator(CharSequence text) {
        return CharacterConversions.lineFeedToLineSeparator(text, CharacterConversions.getSystemDefaultLineSeparator());
    }

    public static String lineFeedToLineSeparator(CharSequence text, CharSequence lineSeparator) {
        if (text == null || text.length() == 0) {
            return "";
        }
        StringBuilder output = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); ++i) {
            char ch = text.charAt(i);
            if (ch == '\n') {
                output.append(lineSeparator);
                continue;
            }
            output.append(ch);
        }
        return output.toString();
    }

    private static String getSystemDefaultLineSeparator() {
        if (systemDefaultLineSeparator == null) {
            systemDefaultLineSeparator = System.getProperty("line.separator");
        }
        return systemDefaultLineSeparator;
    }

    private CharacterConversions() {
    }
}

