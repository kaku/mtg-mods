/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class StringEscapeUtils {
    public static String escapeHtml(String text) {
        StringBuilder builder = null;
        int lastChange = 0;
        for (int i = 0; i < text.length(); ++i) {
            char chr = text.charAt(i);
            if (!Escape.isInBasicEscape(chr)) continue;
            if (builder == null) {
                builder = new StringBuilder();
            }
            builder.append(text.substring(lastChange, i));
            lastChange = i + 1;
            builder.append(Escape.get(chr).getEscapedString());
        }
        if (builder == null) {
            return text;
        }
        builder.append(text.substring(lastChange, text.length()));
        return builder.toString();
    }

    public static String unescapeHtml(String text) {
        return text.replace(Escape.QUOT.getEscapedString(), Escape.QUOT.getCharacter().toString()).replace(Escape.LT.getEscapedString(), Escape.LT.getCharacter().toString()).replace(Escape.GT.getEscapedString(), Escape.GT.getCharacter().toString()).replace(Escape.AMP.getEscapedString(), Escape.AMP.getCharacter().toString());
    }

    private static enum Escape {
        AMP('&', "&amp;"),
        QUOT('\"', "&quot;"),
        LT('<', "&lt;"),
        GT('>', "&gt;");
        
        private final Character character;
        private final String escapedString;
        private static final Map<Character, Escape> lookup;

        private Escape(char character, String escapedChar) {
            this.character = Character.valueOf(character);
            this.escapedString = escapedChar;
        }

        public Character getCharacter() {
            return this.character;
        }

        public String getEscapedString() {
            return this.escapedString;
        }

        public static Escape get(char c) {
            return lookup.get(Character.valueOf(c));
        }

        public static boolean isInBasicEscape(char c) {
            return lookup.keySet().contains(Character.valueOf(c));
        }

        static {
            lookup = new HashMap<Character, Escape>();
            for (Escape d : Escape.values()) {
                lookup.put(d.getCharacter(), d);
            }
        }
    }

}

