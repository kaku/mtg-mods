/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import java.util.EventListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.netbeans.lib.editor.util.PriorityListenerList;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;

class PriorityDocumentListenerList
extends PriorityListenerList<DocumentListener>
implements DocumentListener {
    private static final Logger LOG = Logger.getLogger(PriorityDocumentListenerList.class.getName());

    PriorityDocumentListenerList() {
    }

    @Override
    public void insertUpdate(DocumentEvent evt) {
        PriorityDocumentListenerList.logEvent(evt, "insertUpdate");
        EventListener[][] listenersArray = this.getListenersArray();
        RuntimeException runtimeException = null;
        for (int priority = listenersArray.length - 1; priority >= 0; --priority) {
            PriorityDocumentListenerList.logPriority(priority);
            EventListener[] listeners = listenersArray[priority];
            for (int i = listeners.length - 1; i >= 0; --i) {
                DocumentListener l = (DocumentListener)listeners[i];
                PriorityDocumentListenerList.logListener(l);
                try {
                    l.insertUpdate(evt);
                    continue;
                }
                catch (RuntimeException ex) {
                    if (runtimeException != null) continue;
                    runtimeException = ex;
                }
            }
        }
        if (runtimeException != null) {
            throw runtimeException;
        }
        PriorityDocumentListenerList.logEventEnd("insertUpdate");
    }

    @Override
    public void removeUpdate(DocumentEvent evt) {
        PriorityDocumentListenerList.logEvent(evt, "removeUpdate");
        EventListener[][] listenersArray = this.getListenersArray();
        RuntimeException runtimeException = null;
        for (int priority = listenersArray.length - 1; priority >= 0; --priority) {
            PriorityDocumentListenerList.logPriority(priority);
            EventListener[] listeners = listenersArray[priority];
            for (int i = listeners.length - 1; i >= 0; --i) {
                DocumentListener l = (DocumentListener)listeners[i];
                PriorityDocumentListenerList.logListener(l);
                try {
                    l.removeUpdate(evt);
                    continue;
                }
                catch (RuntimeException ex) {
                    if (runtimeException != null) continue;
                    runtimeException = ex;
                }
            }
        }
        if (runtimeException != null) {
            throw runtimeException;
        }
        PriorityDocumentListenerList.logEventEnd("removeUpdate");
    }

    @Override
    public void changedUpdate(DocumentEvent evt) {
        PriorityDocumentListenerList.logEvent(evt, "changedUpdate");
        EventListener[][] listenersArray = this.getListenersArray();
        RuntimeException runtimeException = null;
        for (int priority = listenersArray.length - 1; priority >= 0; --priority) {
            PriorityDocumentListenerList.logPriority(priority);
            EventListener[] listeners = listenersArray[priority];
            for (int i = listeners.length - 1; i >= 0; --i) {
                DocumentListener l = (DocumentListener)listeners[i];
                PriorityDocumentListenerList.logListener(l);
                try {
                    l.changedUpdate(evt);
                    continue;
                }
                catch (RuntimeException ex) {
                    if (runtimeException != null) continue;
                    runtimeException = ex;
                }
            }
        }
        if (runtimeException != null) {
            throw runtimeException;
        }
        PriorityDocumentListenerList.logEventEnd("changedUpdate");
    }

    private static void logEvent(DocumentEvent evt, String methodName) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("FIRING PriorityDocumentListenerList." + methodName + "() evt: type=" + evt.getType() + ", off=" + evt.getOffset() + ", len=" + evt.getLength() + "-----------------\n" + "doc: " + evt.getDocument());
        }
    }

    private static void logPriority(int priority) {
        if (LOG.isLoggable(Level.FINE)) {
            String prioMsg = priority < DocumentListenerPriority.PRIORITIES.length ? DocumentListenerPriority.PRIORITIES[priority].getDescription() : String.valueOf(priority);
            LOG.fine("  " + prioMsg + ":\n");
        }
    }

    private static void logEventEnd(String methodName) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("END-FIRING of " + methodName + "() ------------------------------------------------\n");
        }
    }

    private static void logListener(DocumentListener l) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("    " + l.getClass() + '\n');
        }
    }
}

