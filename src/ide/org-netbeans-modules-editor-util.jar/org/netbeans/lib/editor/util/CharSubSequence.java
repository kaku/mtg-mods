/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import org.netbeans.lib.editor.util.AbstractCharSequence;
import org.netbeans.lib.editor.util.CharSequenceUtilities;

public class CharSubSequence
extends AbstractCharSequence {
    private int length;
    private int start;
    private CharSequence backingSequence;

    public static void checkIndexesValid(CharSequence text, int start, int end) {
        CharSequenceUtilities.checkIndexesValid(text, start, end);
    }

    public CharSubSequence(CharSequence backingSequence, int start, int end) {
        CharSubSequence.checkIndexesValid(backingSequence, start, end);
        this.backingSequence = backingSequence;
        this.start = start;
        this.length = end - start;
    }

    protected CharSequence backingSequence() {
        return this.backingSequence;
    }

    protected int start() {
        return this.start;
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    public char charAt(int index) {
        CharSequenceUtilities.checkIndexValid(index, this.length);
        return this.backingSequence.charAt(this.start() + index);
    }

    public static class StringLike
    extends CharSubSequence {
        public StringLike(CharSequence backingSequence, int start, int end) {
            super(backingSequence, start, end);
        }

        public int hashCode() {
            return CharSequenceUtilities.stringLikeHashCode(this);
        }

        public boolean equals(Object o) {
            return CharSequenceUtilities.equals(this, o);
        }
    }

}

