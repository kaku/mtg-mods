/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;

public final class ArrayUtilities {
    private static boolean[] EMPTY_BOOLEAN_ARRAY;
    private static char[] EMPTY_CHAR_ARRAY;
    private static int[] EMPTY_INT_ARRAY;

    private ArrayUtilities() {
    }

    public static boolean[] booleanArray(boolean[] oldArray) {
        return ArrayUtilities.booleanArray(oldArray, oldArray != null ? oldArray.length << 1 : 1);
    }

    public static boolean[] booleanArray(boolean[] oldArray, int newSize) {
        return ArrayUtilities.booleanArray(oldArray, newSize, oldArray != null ? Math.min(newSize, oldArray.length) : 0, true);
    }

    public static boolean[] booleanArray(boolean[] oldArray, int newSize, int copyLen, boolean forwardFill) {
        boolean[] newArray = new boolean[newSize];
        if (copyLen > 0) {
            if (forwardFill) {
                System.arraycopy(oldArray, 0, newArray, 0, copyLen);
            } else {
                System.arraycopy(oldArray, oldArray.length - copyLen, newArray, newSize - copyLen, copyLen);
            }
        }
        return newArray;
    }

    public static char[] charArray(char[] oldArray) {
        return ArrayUtilities.charArray(oldArray, oldArray != null ? oldArray.length << 1 : 1);
    }

    public static char[] charArray(char[] oldArray, int newSize) {
        return ArrayUtilities.charArray(oldArray, newSize, oldArray != null ? Math.min(newSize, oldArray.length) : 0, true);
    }

    public static char[] charArray(char[] oldArray, int newSize, int copyLen, boolean forwardFill) {
        char[] newArray = new char[newSize];
        if (copyLen > 0) {
            if (forwardFill) {
                System.arraycopy(oldArray, 0, newArray, 0, copyLen);
            } else {
                System.arraycopy(oldArray, oldArray.length - copyLen, newArray, newSize - copyLen, copyLen);
            }
        }
        return newArray;
    }

    public static char[] charArray(char[] oldArray, int newSize, int gapStart, int gapLength) {
        char[] newArray = new char[newSize];
        if (gapStart > 0) {
            System.arraycopy(oldArray, 0, newArray, 0, gapStart);
        }
        if ((gapLength = oldArray.length - (gapStart += gapLength)) > 0) {
            System.arraycopy(oldArray, gapStart, newArray, newSize - gapLength, gapLength);
        }
        return newArray;
    }

    public static int[] intArray(int[] oldArray) {
        return ArrayUtilities.intArray(oldArray, oldArray != null ? oldArray.length << 1 : 1);
    }

    public static int[] intArray(int[] oldArray, int newSize) {
        return ArrayUtilities.intArray(oldArray, newSize, oldArray != null ? Math.min(newSize, oldArray.length) : 0, true);
    }

    public static int[] intArray(int[] oldArray, int newSize, int copyLen, boolean forwardFill) {
        int[] newArray = new int[newSize];
        if (copyLen > 0) {
            if (forwardFill) {
                System.arraycopy(oldArray, 0, newArray, 0, copyLen);
            } else {
                System.arraycopy(oldArray, oldArray.length - copyLen, newArray, newSize - copyLen, copyLen);
            }
        }
        return newArray;
    }

    public static int[] intArray(int[] oldArray, int newSize, int gapStart, int gapLength) {
        int[] newArray = new int[newSize];
        if (gapStart > 0) {
            System.arraycopy(oldArray, 0, newArray, 0, gapStart);
        }
        if ((gapLength = oldArray.length - (gapStart += gapLength)) > 0) {
            System.arraycopy(oldArray, gapStart, newArray, newSize - gapLength, gapLength);
        }
        return newArray;
    }

    public static boolean[] emptyBooleanArray() {
        if (EMPTY_BOOLEAN_ARRAY == null) {
            EMPTY_BOOLEAN_ARRAY = new boolean[0];
        }
        return EMPTY_BOOLEAN_ARRAY;
    }

    public static char[] emptyCharArray() {
        if (EMPTY_CHAR_ARRAY == null) {
            EMPTY_CHAR_ARRAY = new char[0];
        }
        return EMPTY_CHAR_ARRAY;
    }

    public static int[] emptyIntArray() {
        if (EMPTY_INT_ARRAY == null) {
            EMPTY_INT_ARRAY = new int[0];
        }
        return EMPTY_INT_ARRAY;
    }

    public static int digitCount(int number) {
        return String.valueOf(Math.abs(number)).length();
    }

    public static void appendIndex(StringBuilder sb, int index, int maxDigitCount) {
        String indexStr = String.valueOf(index);
        ArrayUtilities.appendSpaces(sb, maxDigitCount - indexStr.length());
        sb.append(indexStr);
    }

    public static void appendIndex(StringBuffer sb, int index, int maxDigitCount) {
        String indexStr = String.valueOf(index);
        ArrayUtilities.appendSpaces(sb, maxDigitCount - indexStr.length());
        sb.append(indexStr);
    }

    public static void appendSpaces(StringBuilder sb, int spaceCount) {
        while (--spaceCount >= 0) {
            sb.append(' ');
        }
    }

    public static void appendSpaces(StringBuffer sb, int spaceCount) {
        while (--spaceCount >= 0) {
            sb.append(' ');
        }
    }

    public static void appendBracketedIndex(StringBuilder sb, int index, int maxDigitCount) {
        sb.append('[');
        ArrayUtilities.appendIndex(sb, index, maxDigitCount);
        sb.append("]: ");
    }

    public static void appendBracketedIndex(StringBuffer sb, int index, int maxDigitCount) {
        sb.append('[');
        ArrayUtilities.appendIndex(sb, index, maxDigitCount);
        sb.append("]: ");
    }

    public static <E> List<E> unmodifiableList(E[] array) {
        return new UnmodifiableList<E>(array);
    }

    public static String toString(Object[] array) {
        StringBuilder sb = new StringBuilder();
        int maxDigitCount = ArrayUtilities.digitCount(array.length);
        for (int i = 0; i < array.length; ++i) {
            ArrayUtilities.appendBracketedIndex(sb, i, maxDigitCount);
            sb.append(array[i]);
            sb.append('\n');
        }
        return sb.toString();
    }

    public static String toString(int[] array) {
        StringBuilder sb = new StringBuilder();
        int maxDigitCount = ArrayUtilities.digitCount(array.length);
        for (int i = 0; i < array.length; ++i) {
            ArrayUtilities.appendBracketedIndex(sb, i, maxDigitCount);
            sb.append(array[i]);
            sb.append('\n');
        }
        return sb.toString();
    }

    public static int binarySearch(int[] array, int key) {
        return ArrayUtilities.binarySearch(array, 0, array.length - 1, key);
    }

    public static int binarySearch(int[] array, int start, int end, int key) {
        assert (start >= 0 && start < array.length);
        assert (end >= start && end < array.length);
        int low = start;
        int high = end;
        while (low <= high) {
            int mid = low + high >> 1;
            int midVal = array[mid];
            int cmp = midVal - key;
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    private static final class UnmodifiableList<E>
    extends AbstractList<E>
    implements RandomAccess {
        private E[] array;

        UnmodifiableList(E[] array) {
            this.array = array;
        }

        @Override
        public E get(int index) {
            if (index >= 0 && index < this.array.length) {
                return this.array[index];
            }
            throw new IndexOutOfBoundsException("index = " + index + ", size = " + this.array.length);
        }

        @Override
        public int size() {
            return this.array.length;
        }

        @Override
        public Object[] toArray() {
            return (Object[])this.array.clone();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            if (a.length < this.array.length) {
                Object[] aa = (Object[])Array.newInstance(a.getClass().getComponentType(), this.array.length);
                a = aa;
            }
            System.arraycopy(this.array, 0, a, 0, this.array.length);
            if (a.length > this.array.length) {
                a[this.array.length] = null;
            }
            return a;
        }
    }

}

