/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

public class PriorityMutex {
    private static final Logger LOG = Logger.getLogger(PriorityMutex.class.getName());
    private static final int WAIT_TIMEOUT = 2000;
    private static final int TIMEOUTS_BEFORE_LOGGING = 5;
    private Thread lockThread;
    private int lockDepth;
    private Thread waitingPriorityThread;
    private Exception logLockStackTrace;

    public synchronized void lock() {
        boolean log = LOG.isLoggable(Level.FINE);
        Thread thread = Thread.currentThread();
        int waitTimeouts = 0;
        try {
            if (thread != this.lockThread) {
                while (this.lockThread != null || this.waitingPriorityThread != null && this.waitingPriorityThread != thread) {
                    if (this.waitingPriorityThread == null && this.isPriorityThread()) {
                        this.waitingPriorityThread = thread;
                    }
                    this.wait(2000);
                    if (!log || ++waitTimeouts <= 5) continue;
                    LOG.fine("PriorityMutex: Timeout expired for thread " + thread + "\n  waiting for lockThread=" + this.lockThread + "\n");
                    if (this.logLockStackTrace != null) {
                        LOG.log(Level.INFO, "Locker thread's lock() call follows:", this.logLockStackTrace);
                    }
                    waitTimeouts = 0;
                }
                this.lockThread = thread;
                if (log && LOG.isLoggable(Level.FINER)) {
                    this.logLockStackTrace = new Exception();
                    this.logLockStackTrace.fillInStackTrace();
                }
                assert (this.lockDepth == 0);
                if (thread == this.waitingPriorityThread) {
                    this.waitingPriorityThread = null;
                }
            }
            ++this.lockDepth;
        }
        catch (InterruptedException e) {
            this.waitingPriorityThread = null;
            throw new Error("Interrupted mutex acquiring");
        }
    }

    public synchronized void unlock() {
        if (Thread.currentThread() != this.lockThread) {
            throw new IllegalStateException("Not locker. lockThread=" + this.lockThread);
        }
        if (--this.lockDepth == 0) {
            this.lockThread = null;
            this.logLockStackTrace = null;
            this.notifyAll();
        }
    }

    public boolean isPriorityThreadWaiting() {
        return this.waitingPriorityThread != null;
    }

    public final synchronized Thread getLockThread() {
        return this.lockThread;
    }

    protected boolean isPriorityThread() {
        return SwingUtilities.isEventDispatchThread();
    }

    public String toString() {
        return "lockThread=" + this.lockThread + ", lockDepth=" + this.lockDepth;
    }
}

