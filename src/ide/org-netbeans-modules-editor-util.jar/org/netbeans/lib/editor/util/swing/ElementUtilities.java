/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import javax.swing.text.Element;

public final class ElementUtilities {
    private ElementUtilities() {
    }

    public static void updateOffsetRange(Element[] elements, int[] offsetRange) {
        int elementsLength = elements.length;
        if (elementsLength > 0) {
            offsetRange[0] = Math.min(offsetRange[0], elements[0].getStartOffset());
            offsetRange[1] = Math.max(offsetRange[1], elements[elementsLength - 1].getEndOffset());
        }
    }
}

