/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.lib.editor.util.swing.PositionRegion;

public class MutablePositionRegion
extends PositionRegion {
    public MutablePositionRegion(Position startPosition, Position endPosition) {
        super(startPosition, endPosition);
    }

    public MutablePositionRegion(Document doc, int startOffset, int endOffset) throws BadLocationException {
        this(doc.createPosition(startOffset), doc.createPosition(endOffset));
    }

    public final void reset(Position startPosition, Position endPosition) {
        this.resetImpl(startPosition, endPosition);
    }

    public final void setStartPosition(Position startPosition) {
        this.setStartPositionImpl(startPosition);
    }

    public final void setEndPosition(Position endPosition) {
        this.setEndPositionImpl(endPosition);
    }
}

