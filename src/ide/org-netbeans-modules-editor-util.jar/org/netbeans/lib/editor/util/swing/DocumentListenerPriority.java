/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

public final class DocumentListenerPriority {
    public static final DocumentListenerPriority FIRST = new DocumentListenerPriority(6, "first");
    public static final DocumentListenerPriority LEXER = new DocumentListenerPriority(5, "lexer");
    public static final DocumentListenerPriority FOLD_UPDATE = new DocumentListenerPriority(4, "fold-update");
    public static final DocumentListenerPriority DEFAULT = new DocumentListenerPriority(3, "default");
    public static final DocumentListenerPriority VIEW = new DocumentListenerPriority(2, "view");
    public static final DocumentListenerPriority CARET_UPDATE = new DocumentListenerPriority(1, "caret-update");
    public static final DocumentListenerPriority AFTER_CARET_UPDATE = new DocumentListenerPriority(0, "after-caret-update");
    static final DocumentListenerPriority[] PRIORITIES = new DocumentListenerPriority[]{AFTER_CARET_UPDATE, CARET_UPDATE, VIEW, DEFAULT, FOLD_UPDATE, LEXER, FIRST};
    private int priority;
    private String description;

    private DocumentListenerPriority(int priority, String description) {
        this.priority = priority;
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public String toString() {
        return this.getDescription();
    }

    int getPriority() {
        return this.priority;
    }
}

