/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class CompactMap<K, V>
implements Map<K, V> {
    private MapEntry<K, V>[] table;
    private int size;

    public CompactMap() {
        this.table = this.allocateTableArray(1);
    }

    public CompactMap(int initialCapacity) {
        int capacity;
        for (capacity = 1; capacity < initialCapacity; capacity <<= 1) {
        }
        this.table = this.allocateTableArray(capacity);
    }

    @Override
    public V get(Object key) {
        MapEntry<K, V> e = this.findEntry(key);
        return e != null ? (V)e.getValue() : null;
    }

    public MapEntry<K, V> getFirstEntry(int hashCode) {
        return this.table[hashCode & this.table.length - 1];
    }

    @Override
    public boolean containsKey(Object key) {
        MapEntry<K, V> e = this.findEntry(key);
        return e != null;
    }

    @Override
    public boolean containsValue(Object value) {
        for (int i = this.table.length - 1; i >= 0; --i) {
            for (MapEntry<K, V> e = this.table[i]; e != null; e = e.nextMapEntry()) {
                if ((value != null || e.getValue() != null) && (value == null || !value.equals(e.getValue()))) continue;
                return true;
            }
        }
        return false;
    }

    public MapEntry<K, V> putEntry(MapEntry<K, V> entry) {
        K key = entry.getKey();
        int hash = key.hashCode();
        int tableIndex = hash & this.table.length - 1;
        entry.setKeyHashCode(hash);
        MapEntry prevEntry = null;
        for (MapEntry e = this.table[tableIndex]; e != null; e = e.nextMapEntry()) {
            if (e == entry) {
                return entry;
            }
            if (hash == e.keyHashCode() && (key == e.getKey() || key.equals(e.getKey()))) {
                if (prevEntry == null) {
                    this.table[tableIndex] = entry;
                } else {
                    prevEntry.setNextMapEntry(entry);
                }
                entry.setNextMapEntry(e.nextMapEntry());
                e.setNextMapEntry(null);
                return e;
            }
            prevEntry = e;
        }
        this.addEntry(entry, tableIndex);
        return null;
    }

    @Override
    public V put(K key, V value) {
        MapEntry<K, V> e;
        int hash = key.hashCode();
        int tableIndex = hash & this.table.length - 1;
        for (e = this.table[tableIndex]; e != null; e = e.nextMapEntry()) {
            if (hash != e.keyHashCode() || key != e.getKey() && !key.equals(e.getKey())) continue;
            V oldValue = e.getValue();
            e.setValue(value);
            return oldValue;
        }
        e = new DefaultMapEntry(key);
        e.setValue(value);
        e.setKeyHashCode(hash);
        this.addEntry(e, tableIndex);
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry<K, V> e : map.entrySet()) {
            this.put(e.getKey(), e.getValue());
        }
    }

    @Override
    public V remove(Object key) {
        MapEntry<K, V> e = this.removeEntryForKey(key);
        return e != null ? (V)e.getValue() : null;
    }

    public MapEntry<K, V> removeEntry(MapEntry<K, V> entry) {
        int hash = entry.keyHashCode();
        int tableIndex = hash & this.table.length - 1;
        MapEntry<K, V> e = this.table[tableIndex];
        MapEntry<K, V> prev = null;
        while (e != null) {
            if (e == entry) {
                if (prev == null) {
                    this.table[tableIndex] = e.nextMapEntry();
                } else {
                    prev.setNextMapEntry(e.nextMapEntry());
                }
                entry.setNextMapEntry(null);
                --this.size;
                return entry;
            }
            prev = entry;
            entry = entry.nextMapEntry();
        }
        return null;
    }

    @Override
    public void clear() {
        for (int i = this.table.length - 1; i >= 0; --i) {
            MapEntry e = this.table[i];
            this.table[i] = null;
            while (e != null) {
                MapEntry<K, V> next = e.nextMapEntry();
                e.setNextMapEntry(null);
                e = next;
            }
        }
        this.size = 0;
    }

    @Override
    public final int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return new EntrySet();
    }

    @Override
    public Collection<V> values() {
        throw new IllegalStateException("Not yet implemented");
    }

    @Override
    public Set<K> keySet() {
        throw new IllegalStateException("Not yet implemented");
    }

    private MapEntry<K, V> findEntry(Object key) {
        int hash = key.hashCode();
        int tableIndex = hash & this.table.length - 1;
        for (MapEntry<K, V> e = this.table[tableIndex]; e != null; e = e.nextMapEntry()) {
            if (hash != e.keyHashCode() || key != e.getKey() && !key.equals(e.getKey())) continue;
            return e;
        }
        return null;
    }

    private void addEntry(MapEntry<K, V> entry, int tableIndex) {
        entry.setNextMapEntry(this.table[tableIndex]);
        this.table[tableIndex] = entry;
        ++this.size;
        if (this.size > this.table.length) {
            MapEntry<K, V>[] newTable = this.allocateTableArray(Math.max(this.table.length << 1, 4));
            for (int i = this.table.length - 1; i >= 0; --i) {
                entry = this.table[i];
                while (entry != null) {
                    MapEntry<K, V> next = entry.nextMapEntry();
                    int newIndex = entry.keyHashCode() & newTable.length - 1;
                    entry.setNextMapEntry(newTable[newIndex]);
                    newTable[newIndex] = entry;
                    entry = next;
                }
            }
            this.table = newTable;
        }
    }

    private MapEntry<K, V> removeEntryForKey(Object key) {
        int hash = key.hashCode();
        int tableIndex = hash & this.table.length - 1;
        MapEntry prev = null;
        for (MapEntry e = this.table[tableIndex]; e != null; e = e.nextMapEntry()) {
            if (hash == e.keyHashCode() && (key == e.getKey() || key.equals(e.getKey()))) {
                if (prev == null) {
                    this.table[tableIndex] = e.nextMapEntry();
                } else {
                    prev.setNextMapEntry(e.nextMapEntry());
                }
                e.setNextMapEntry(null);
                --this.size;
                return e;
            }
            prev = e;
        }
        return null;
    }

    private MapEntry<K, V>[] allocateTableArray(int capacity) {
        return new MapEntry[capacity];
    }

    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("{");
        Iterator<Map.Entry<K, V>> i = this.entrySet().iterator();
        boolean hasNext = i.hasNext();
        while (hasNext) {
            Map.Entry<K, V> e = i.next();
            K key = e.getKey();
            V value = e.getValue();
            if (key == this) {
                buf.append("(this Map)");
            } else {
                buf.append(key);
            }
            buf.append("=");
            if (value == this) {
                buf.append("(this Map)");
            } else {
                buf.append(value);
            }
            if (!(hasNext = i.hasNext())) continue;
            buf.append(", ");
        }
        buf.append("}");
        return buf.toString();
    }

    private final class EntryIterator
    extends CompactMap<K, V>
    implements Iterator<Map.Entry<K, V>> {
        private EntryIterator() {
            super();
        }

        @Override
        public Map.Entry<K, V> next() {
            return this.nextEntry();
        }
    }

    private final class KeyIterator
    extends CompactMap<K, V>
    implements Iterator<K> {
        private KeyIterator() {
            super();
        }

        @Override
        public K next() {
            return this.nextEntry().getKey();
        }
    }

    private final class ValueIterator
    extends CompactMap<K, V>
    implements Iterator<V> {
        private ValueIterator() {
            super();
        }

        @Override
        public V next() {
            return this.nextEntry().getValue();
        }
    }

    private abstract class HashIterator {
        MapEntry<K, V> next;
        int index;
        MapEntry<K, V> current;

        HashIterator() {
            MapEntry[] t = CompactMap.this.table;
            int i = t.length;
            MapEntry n = null;
            if (CompactMap.this.size != 0) {
                while (i > 0 && (n = t[--i]) == null) {
                }
            }
            this.next = n;
            this.index = i;
        }

        public boolean hasNext() {
            return this.next != null;
        }

        MapEntry<K, V> nextEntry() {
            MapEntry<K, V> e = this.next;
            if (e == null) {
                throw new NoSuchElementException();
            }
            MapEntry n = e.nextMapEntry();
            MapEntry[] t = CompactMap.this.table;
            int i = this.index;
            while (n == null && i > 0) {
                n = t[--i];
            }
            this.index = i;
            this.next = n;
            this.current = e;
            return this.current;
        }

        public void remove() {
            if (this.current == null) {
                throw new IllegalStateException();
            }
            K k = this.current.getKey();
            this.current = null;
            CompactMap.this.removeEntryForKey(k);
        }
    }

    private final class EntrySet
    extends AbstractSet<Map.Entry<K, V>> {
        private EntrySet() {
        }

        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator();
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry e = (Map.Entry)o;
            MapEntry candidate = CompactMap.this.findEntry(e.getKey());
            return candidate != null && candidate.equals(e);
        }

        @Override
        public boolean remove(Object o) {
            MapEntry e = (MapEntry)o;
            return CompactMap.this.removeEntry(e) != null;
        }

        @Override
        public int size() {
            return CompactMap.this.size();
        }

        @Override
        public void clear() {
            CompactMap.this.clear();
        }
    }

    public static class DefaultMapEntry<K, V>
    extends MapEntry<K, V> {
        private K key;
        private V value;

        public DefaultMapEntry(K key) {
            this.key = key;
        }

        @Override
        public final K getKey() {
            return this.key;
        }

        @Override
        public final V getValue() {
            return this.value;
        }

        @Override
        public final V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        @Override
        protected final int valueHashCode() {
            return this.value != null ? this.value.hashCode() : 0;
        }

        @Override
        protected final boolean valueEquals(Object value2) {
            return this.value == value2 || this.value != null && this.value.equals(value2);
        }

        public String toString() {
            return "key=" + this.getKey() + ", value=" + this.getValue();
        }
    }

    public static abstract class MapEntry<K, V>
    implements Map.Entry<K, V> {
        private MapEntry<K, V> nextMapEntry;
        private int keyHashCode;

        @Override
        public abstract K getKey();

        @Override
        public abstract V getValue();

        @Override
        public abstract V setValue(V var1);

        protected abstract int valueHashCode();

        protected abstract boolean valueEquals(Object var1);

        public final MapEntry<K, V> nextMapEntry() {
            return this.nextMapEntry;
        }

        final void setNextMapEntry(MapEntry<K, V> next) {
            this.nextMapEntry = next;
        }

        public final int keyHashCode() {
            return this.keyHashCode;
        }

        final void setKeyHashCode(int keyHashCode) {
            this.keyHashCode = keyHashCode;
        }

        @Override
        public final int hashCode() {
            int keyHash = this.keyHashCode != 0 ? this.keyHashCode : this.getKey().hashCode();
            return keyHash ^ this.valueHashCode();
        }

        @Override
        public final boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof Map.Entry) {
                K key2;
                Map.Entry e = (Map.Entry)o;
                K key = this.getKey();
                if (key == (key2 = e.getKey()) || key.equals(key2)) {
                    return this.valueEquals(e.getValue());
                }
            }
            return false;
        }
    }

}

