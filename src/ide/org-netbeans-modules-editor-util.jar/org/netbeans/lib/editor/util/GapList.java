/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import org.netbeans.lib.editor.util.ArrayUtilities;

public class GapList<E>
extends AbstractList<E>
implements List<E>,
RandomAccess,
Cloneable,
Serializable {
    private transient E[] elementData;
    private int gapStart;
    private int gapLength;

    public GapList(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
        this.elementData = this.allocateElementsArray(initialCapacity);
        this.gapLength = initialCapacity;
    }

    public GapList() {
        this(10);
    }

    public GapList(Collection<? extends E> c) {
        int size = c.size();
        int capacity = (int)Math.min((long)size * 110 / 100, Integer.MAX_VALUE);
        Object[] data = c.toArray(new Object[capacity]);
        this.elementData = data;
        this.gapStart = size;
        this.gapLength = this.elementData.length - size;
    }

    public void trimToSize() {
        ++this.modCount;
        if (this.gapLength > 0) {
            int newLength = this.elementData.length - this.gapLength;
            Object[] newElementData = this.allocateElementsArray(newLength);
            this.copyAllData(newElementData);
            this.elementData = newElementData;
            this.gapLength = 0;
        }
    }

    public void ensureCapacity(int minCapacity) {
        ++this.modCount;
        int oldCapacity = this.elementData.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 3 / 2 + 1;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            int gapEnd = this.gapStart + this.gapLength;
            int afterGapLength = oldCapacity - gapEnd;
            int newGapEnd = newCapacity - afterGapLength;
            E[] newElementData = this.allocateElementsArray(newCapacity);
            System.arraycopy(this.elementData, 0, newElementData, 0, this.gapStart);
            System.arraycopy(this.elementData, gapEnd, newElementData, newGapEnd, afterGapLength);
            this.elementData = newElementData;
            this.gapLength = newGapEnd - this.gapStart;
        }
    }

    @Override
    public int size() {
        return this.elementData.length - this.gapLength;
    }

    @Override
    public boolean isEmpty() {
        return this.elementData.length == this.gapLength;
    }

    @Override
    public boolean contains(Object elem) {
        return this.indexOf(elem) >= 0;
    }

    @Override
    public int indexOf(Object elem) {
        if (elem == null) {
            int i;
            for (i = 0; i < this.gapStart; ++i) {
                if (this.elementData[i] != null) continue;
                return i;
            }
            i += this.gapLength;
            int elementDataLength = this.elementData.length;
            while (i < elementDataLength) {
                if (this.elementData[i] == null) {
                    return i - this.gapLength;
                }
                ++i;
            }
        } else {
            int i;
            for (i = 0; i < this.gapStart; ++i) {
                if (!elem.equals(this.elementData[i])) continue;
                return i;
            }
            i += this.gapLength;
            int elementDataLength = this.elementData.length;
            while (i < elementDataLength) {
                if (elem.equals(this.elementData[i])) {
                    return i - this.gapLength;
                }
                ++i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object elem) {
        if (elem == null) {
            int i;
            int gapEnd = this.gapStart + this.gapLength;
            for (i = this.elementData.length - 1; i >= gapEnd; --i) {
                if (this.elementData[i] != null) continue;
                return i - this.gapLength;
            }
            i -= this.gapLength;
            while (i >= 0) {
                if (this.elementData[i] == null) {
                    return i;
                }
                --i;
            }
        } else {
            int i;
            int gapEnd = this.gapStart + this.gapLength;
            for (i = this.elementData.length - 1; i >= gapEnd; --i) {
                if (!elem.equals(this.elementData[i])) continue;
                return i - this.gapLength;
            }
            i -= this.gapLength;
            while (i >= 0) {
                if (elem.equals(this.elementData[i])) {
                    return i;
                }
                --i;
            }
        }
        return -1;
    }

    public Object clone() {
        try {
            GapList clonedList = (GapList)Object.super.clone();
            int size = this.size();
            Object[] clonedElementData = this.allocateElementsArray(size);
            this.copyAllData(clonedElementData);
            clonedList.elementData = clonedElementData;
            clonedList.gapStart = size;
            clonedList.resetModCount();
            return clonedList;
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    public void copyItems(int startIndex, int endIndex, Object[] dest, int destIndex) {
        this.copyElements(startIndex, endIndex, dest, destIndex);
    }

    public void copyElements(int startIndex, int endIndex, Object[] dest, int destIndex) {
        if (startIndex < 0 || endIndex < startIndex || endIndex > this.size()) {
            throw new IndexOutOfBoundsException("startIndex=" + startIndex + ", endIndex=" + endIndex + ", size()=" + this.size());
        }
        if (endIndex < this.gapStart) {
            System.arraycopy(this.elementData, startIndex, dest, destIndex, endIndex - startIndex);
        } else if (startIndex >= this.gapStart) {
            System.arraycopy(this.elementData, startIndex + this.gapLength, dest, destIndex, endIndex - startIndex);
        } else {
            int beforeGap = this.gapStart - startIndex;
            System.arraycopy(this.elementData, startIndex, dest, destIndex, beforeGap);
            System.arraycopy(this.elementData, this.gapStart + this.gapLength, dest, destIndex + beforeGap, endIndex - startIndex - beforeGap);
        }
    }

    public void copyElements(int startIndex, int endIndex, Collection<E> dest) {
        if (startIndex < 0 || endIndex < startIndex || endIndex > this.size()) {
            throw new IndexOutOfBoundsException("startIndex=" + startIndex + ", endIndex=" + endIndex + ", size()=" + this.size());
        }
        if (endIndex < this.gapStart) {
            while (startIndex < endIndex) {
                dest.add(this.elementData[startIndex++]);
            }
        } else if (startIndex >= this.gapStart) {
            startIndex += this.gapLength;
            while (startIndex < (endIndex += this.gapLength)) {
                dest.add(this.elementData[startIndex++]);
            }
        } else {
            while (startIndex < this.gapStart) {
                dest.add(this.elementData[startIndex++]);
            }
            startIndex += this.gapLength;
            while (startIndex < (endIndex += this.gapLength)) {
                dest.add(this.elementData[startIndex++]);
            }
        }
    }

    @Override
    public Object[] toArray() {
        int size = this.size();
        Object[] result = new Object[size];
        this.copyAllData(result);
        return result;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int size = this.size();
        if (a.length < size) {
            Object[] tmp = (Object[])Array.newInstance(a.getClass().getComponentType(), size);
            a = tmp;
        }
        this.copyAllData(a);
        if (a.length > size) {
            a[size] = null;
        }
        return a;
    }

    @Override
    public E get(int index) {
        return this.elementData[index < this.gapStart ? index : index + this.gapLength];
    }

    @Override
    public E set(int index, E element) {
        if (index >= this.gapStart) {
            index += this.gapLength;
        }
        E oldValue = this.elementData[index];
        this.elementData[index] = element;
        return oldValue;
    }

    public void swap(int index1, int index2) {
        if (index1 >= this.gapStart) {
            index1 += this.gapLength;
        }
        if (index2 >= this.gapStart) {
            index2 += this.gapLength;
        }
        E tmpValue = this.elementData[index1];
        this.elementData[index1] = this.elementData[index2];
        this.elementData[index2] = tmpValue;
    }

    @Override
    public boolean add(E element) {
        int size = this.size();
        this.ensureCapacity(size + 1);
        this.addImpl(size, element);
        return true;
    }

    @Override
    public void add(int index, E element) {
        int size = this.size();
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        this.ensureCapacity(size + 1);
        this.addImpl(index, element);
    }

    private void addImpl(int index, E element) {
        this.moveGap(index);
        this.elementData[this.gapStart++] = element;
        --this.gapLength;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return this.addAll(this.size(), c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return this.addArray(index, c.toArray());
    }

    public boolean addArray(int index, Object[] elements) {
        return this.addArray(index, elements, 0, elements.length);
    }

    public boolean addArray(int index, Object[] elements, int off, int len) {
        int size = this.size();
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        this.ensureCapacity(size + len);
        this.moveGap(index);
        System.arraycopy(elements, off, this.elementData, index, len);
        this.gapStart += len;
        this.gapLength -= len;
        return len != 0;
    }

    @Override
    public void clear() {
        this.removeRange(0, this.size());
    }

    @Override
    public E remove(int index) {
        int size = this.size();
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("remove(): Index: " + index + ", Size: " + size);
        }
        ++this.modCount;
        this.moveGap(index + 1);
        E oldValue = this.elementData[index];
        this.elementData[index] = null;
        --this.gapStart;
        ++this.gapLength;
        return oldValue;
    }

    public void remove(int index, int count) {
        int toIndex = index + count;
        if (index < 0 || toIndex < index || toIndex > this.size()) {
            throw new IndexOutOfBoundsException("index=" + index + ", count=" + count + ", size()=" + this.size());
        }
        this.removeRange(index, toIndex);
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        ++this.modCount;
        if (fromIndex == toIndex) {
            return;
        }
        int removeCount = toIndex - fromIndex;
        if (fromIndex >= this.gapStart) {
            this.moveGap(fromIndex);
            fromIndex += this.gapLength;
            while (fromIndex < (toIndex += this.gapLength)) {
                this.elementData[fromIndex] = null;
                ++fromIndex;
            }
        } else {
            if (toIndex <= this.gapStart) {
                this.moveGap(toIndex);
                this.gapStart = fromIndex;
            } else {
                for (int clearIndex = fromIndex; clearIndex < this.gapStart; ++clearIndex) {
                    this.elementData[clearIndex] = null;
                }
                fromIndex = this.gapStart + this.gapLength;
                this.gapStart = toIndex - removeCount;
                toIndex += this.gapLength;
            }
            while (fromIndex < toIndex) {
                this.elementData[fromIndex++] = null;
            }
        }
        this.gapLength += removeCount;
    }

    private void moveGap(int index) {
        if (index == this.gapStart) {
            return;
        }
        if (this.gapLength > 0) {
            if (index < this.gapStart) {
                int moveSize = this.gapStart - index;
                System.arraycopy(this.elementData, index, this.elementData, this.gapStart + this.gapLength - moveSize, moveSize);
                this.clearEmpty(index, Math.min(moveSize, this.gapLength));
            } else {
                int gapEnd = this.gapStart + this.gapLength;
                int moveSize = index - this.gapStart;
                System.arraycopy(this.elementData, gapEnd, this.elementData, this.gapStart, moveSize);
                if (index < gapEnd) {
                    this.clearEmpty(gapEnd, moveSize);
                } else {
                    this.clearEmpty(index, this.gapLength);
                }
            }
        }
        this.gapStart = index;
    }

    private void copyAllData(Object[] toArray) {
        if (this.gapLength != 0) {
            int gapEnd = this.gapStart + this.gapLength;
            System.arraycopy(this.elementData, 0, toArray, 0, this.gapStart);
            System.arraycopy(this.elementData, gapEnd, toArray, this.gapStart, this.elementData.length - gapEnd);
        } else {
            System.arraycopy(this.elementData, 0, toArray, 0, this.elementData.length);
        }
    }

    private void clearEmpty(int index, int length) {
        while (--length >= 0) {
            this.elementData[index++] = null;
        }
    }

    private void resetModCount() {
        this.modCount = 0;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        int i;
        s.defaultWriteObject();
        s.writeInt(this.elementData.length);
        for (i = 0; i < this.gapStart; ++i) {
            s.writeObject(this.elementData[i]);
        }
        i += this.gapLength;
        int elementDataLength = this.elementData.length;
        while (i < elementDataLength) {
            s.writeObject(this.elementData[i]);
            ++i;
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        int i;
        s.defaultReadObject();
        int arrayLength = s.readInt();
        this.elementData = this.allocateElementsArray(arrayLength);
        for (i = 0; i < this.gapStart; ++i) {
            Object e;
            this.elementData[i] = e = s.readObject();
        }
        i += this.gapLength;
        int elementDataLength = this.elementData.length;
        while (i < elementDataLength) {
            Object e;
            this.elementData[i] = e = s.readObject();
            ++i;
        }
    }

    protected void consistencyCheck() {
        if (this.gapStart < 0 || this.gapLength < 0 || this.gapStart + this.gapLength > this.elementData.length) {
            this.consistencyError("Inconsistent gap");
        }
        for (int i = this.gapStart + this.gapLength - 1; i >= this.gapStart; --i) {
            if (this.elementData[i] == null) continue;
            this.consistencyError("Non-null value at raw-index i");
        }
    }

    protected final void consistencyError(String s) {
        throw new IllegalStateException(s + ": " + this.dumpDetails());
    }

    protected String dumpDetails() {
        return this.dumpInternals() + "; DATA:\n" + this.toString();
    }

    protected String dumpInternals() {
        return "elems: " + this.size() + '(' + this.elementData.length + "), gap(s=" + this.gapStart + ", l=" + this.gapLength + ')';
    }

    private E[] allocateElementsArray(int capacity) {
        return new Object[capacity];
    }

    @Override
    public String toString() {
        return GapList.dumpElements(this);
    }

    public static String dumpElements(List l) {
        StringBuffer sb = new StringBuffer();
        int size = l.size();
        int digitCount = String.valueOf(size - 1).length();
        for (int i = 0; i < size; ++i) {
            ArrayUtilities.appendBracketedIndex(sb, i, digitCount);
            sb.append(l.get(i));
            sb.append("\n");
        }
        return sb.toString();
    }
}

