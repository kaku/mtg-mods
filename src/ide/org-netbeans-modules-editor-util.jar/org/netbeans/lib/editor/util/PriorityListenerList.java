/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EventListener;

public class PriorityListenerList<T extends EventListener>
implements Serializable {
    static final long serialVersionUID = 0;
    private static final EventListener[] EMPTY_LISTENER_ARRAY = new EventListener[0];
    private static final EventListener[][] EMPTY_LISTENER_ARRAY_ARRAY = new EventListener[0][];
    private transient T[][] listenersArray;
    private int listenerCount;

    public PriorityListenerList() {
        this.listenersArray = this.emptyTArrayArray();
    }

    public synchronized void add(T listener, int priority) {
        if (listener == null) {
            return;
        }
        if (priority >= this.listenersArray.length) {
            EventListener[][] newListenersArray = this.allocateTArrayArray(priority + 1);
            System.arraycopy(this.listenersArray, 0, newListenersArray, 0, this.listenersArray.length);
            for (int i = this.listenersArray.length; i < priority; ++i) {
                newListenersArray[i] = this.emptyTArray();
            }
            EventListener[] arr = this.allocateTArray(1);
            arr[0] = listener;
            newListenersArray[priority] = arr;
            this.listenersArray = newListenersArray;
        } else {
            EventListener[][] newListenersArray = (EventListener[][])this.listenersArray.clone();
            T[] listeners = this.listenersArray[priority];
            EventListener[] newListeners = this.allocateTArray(listeners.length + 1);
            System.arraycopy(listeners, 0, newListeners, 1, listeners.length);
            newListeners[0] = listener;
            newListenersArray[priority] = newListeners;
            this.listenersArray = newListenersArray;
        }
        ++this.listenerCount;
    }

    public synchronized void remove(T listener, int priority) {
        if (listener == null) {
            return;
        }
        if (priority < this.listenersArray.length) {
            int index;
            T[] listeners = this.listenersArray[priority];
            for (index = 0; index < listeners.length && listeners[index] != listener; ++index) {
            }
            if (index < listeners.length) {
                EventListener[] newListeners;
                boolean removeHighestPriorityLevel;
                --this.listenerCount;
                if (listeners.length == 1) {
                    newListeners = this.emptyTArray();
                    removeHighestPriorityLevel = priority == this.listenersArray.length - 1;
                } else {
                    newListeners = this.allocateTArray(listeners.length - 1);
                    System.arraycopy(listeners, 0, newListeners, 0, index);
                    System.arraycopy(listeners, index + 1, newListeners, index, newListeners.length - index);
                    removeHighestPriorityLevel = false;
                }
                if (removeHighestPriorityLevel) {
                    EventListener[][] newListenersArray = this.allocateTArrayArray(this.listenersArray.length - 1);
                    System.arraycopy(this.listenersArray, 0, newListenersArray, 0, newListenersArray.length);
                    this.listenersArray = newListenersArray;
                } else {
                    EventListener[][] newListenersArray = (EventListener[][])this.listenersArray.clone();
                    newListenersArray[priority] = newListeners;
                    this.listenersArray = newListenersArray;
                }
            }
        }
    }

    public T[][] getListenersArray() {
        return this.listenersArray;
    }

    public int getListenerCount() {
        return this.listenerCount;
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        int priority;
        s.defaultWriteObject();
        s.writeInt(priority);
        for (priority = this.listenersArray.length - 1; priority >= 0; --priority) {
            T[] listeners = this.listenersArray[priority];
            for (int i = 0; i < listeners.length; ++i) {
                T listener = listeners[i];
                if (!(listener instanceof Serializable)) continue;
                s.writeObject(listener);
            }
            s.writeObject(null);
        }
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        int priority;
        s.defaultReadObject();
        EventListener[][] arreventListener = this.listenersArray = priority != -1 ? this.allocateTArrayArray(priority + 1) : this.emptyTArrayArray();
        for (priority = s.readInt(); priority >= 0; --priority) {
            Object listenerOrNull;
            ArrayList<EventListener> lList = new ArrayList<EventListener>();
            while (null != (listenerOrNull = s.readObject())) {
                EventListener l = (EventListener)listenerOrNull;
                lList.add(l);
            }
            EventListener[] lArr = lList.toArray(new EventListener[lList.size()]);
            this.listenersArray[priority] = lArr;
        }
    }

    private T[] emptyTArray() {
        return EMPTY_LISTENER_ARRAY;
    }

    private T[][] emptyTArrayArray() {
        return EMPTY_LISTENER_ARRAY_ARRAY;
    }

    private T[] allocateTArray(int length) {
        return new EventListener[length];
    }

    private T[][] allocateTArrayArray(int length) {
        return new EventListener[length][];
    }
}

