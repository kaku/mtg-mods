/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import org.netbeans.lib.editor.util.GapList;

public abstract class OffsetGapList<E>
extends GapList<E> {
    private int offsetGapStart;
    private int offsetGapLength = 1073741823;

    public OffsetGapList() {
        this(10);
    }

    public OffsetGapList(int initialCapacity) {
        super(initialCapacity);
    }

    protected abstract int elementRawOffset(E var1);

    protected abstract void setElementRawOffset(E var1, int var2);

    protected int elementOffset(E elem) {
        return this.raw2Offset(this.elementRawOffset(elem));
    }

    protected int elementOffset(int index) {
        return this.elementOffset(this.get(index));
    }

    public void defaultInsertUpdate(int offset, int length) {
        assert (length >= 0);
        if (offset != this.offsetGapStart()) {
            this.moveOffsetGap(offset, this.findElementIndex(offset));
        }
        this.updateOffsetGapLength(- length);
        this.updateOffsetGapStart(length);
    }

    public void defaultRemoveUpdate(int offset, int length) {
        E elem;
        assert (length >= 0);
        int index = this.findElementIndex(offset);
        if (offset != this.offsetGapStart()) {
            this.moveOffsetGap(offset, index);
        }
        int size = this.size();
        int removeAreaEndRawOffset = offset + this.offsetGapLength + length;
        while (index < size && this.elementRawOffset(elem = this.get(index++)) < removeAreaEndRawOffset) {
            this.setElementRawOffset(elem, removeAreaEndRawOffset);
        }
        this.updateOffsetGapLength(length);
    }

    protected final void moveOffsetGap(int offset, int index) {
        if (offset < this.offsetGapStart) {
            int rawOffset;
            E elem;
            int bound = this.size();
            for (int i = index; i < bound && (rawOffset = this.elementRawOffset(elem = this.get(i))) < this.offsetGapStart; ++i) {
                this.setElementRawOffset(elem, rawOffset + this.offsetGapLength);
            }
        } else {
            int rawOffset;
            E elem;
            for (int i = index - 1; i >= 0 && (rawOffset = this.elementRawOffset(elem = this.get(i))) >= this.offsetGapStart; --i) {
                this.setElementRawOffset(elem, rawOffset - this.offsetGapLength);
            }
        }
        this.offsetGapStart = offset;
    }

    protected final int offsetGapStart() {
        return this.offsetGapStart;
    }

    protected final void updateOffsetGapStart(int offsetDelta) {
        this.offsetGapStart += offsetDelta;
    }

    protected final int offsetGapLength() {
        return this.offsetGapLength;
    }

    protected final void updateOffsetGapLength(int offsetGapLengthDelta) {
        this.offsetGapLength += offsetGapLengthDelta;
        assert (this.offsetGapLength >= 0);
    }

    protected final int findElementIndex(int offset) {
        int low = 0;
        int high = this.size() - 1;
        while (low <= high) {
            int index = (low + high) / 2;
            int elemOffset = this.elementOffset(index);
            if (elemOffset < offset) {
                low = index + 1;
                continue;
            }
            if (elemOffset > offset) {
                high = index - 1;
                continue;
            }
            while (index > 0) {
                if (this.elementOffset(--index) >= offset) continue;
                ++index;
                break;
            }
            low = index;
            break;
        }
        return low;
    }

    protected void updateElementOffsetAdd(E elem) {
        int rawOffset = this.elementRawOffset(elem);
        if (rawOffset >= this.offsetGapStart) {
            this.setElementRawOffset(elem, rawOffset + this.offsetGapLength);
        }
    }

    protected void updateElementOffsetRemove(E elem) {
        int rawOffset = this.elementRawOffset(elem);
        if (rawOffset >= this.offsetGapStart) {
            this.setElementRawOffset(elem, rawOffset - this.offsetGapLength);
        }
    }

    protected final int raw2Offset(int rawOffset) {
        return rawOffset < this.offsetGapStart ? rawOffset : rawOffset - this.offsetGapLength;
    }

    protected final int offset2raw(int offset) {
        return offset < this.offsetGapStart ? offset : offset + this.offsetGapLength;
    }

    @Override
    protected void consistencyCheck() {
        GapList.super.consistencyCheck();
        if (this.offsetGapLength < 0) {
            this.consistencyError("offsetGapLength < 0");
        }
        int lastRawOffset = Integer.MIN_VALUE;
        int lastOffset = Integer.MIN_VALUE;
        int size = this.size();
        for (int i = 0; i < size; ++i) {
            E elem = this.get(i);
            int rawOffset = this.elementRawOffset(elem);
            int offset = this.raw2Offset(rawOffset);
            if (rawOffset < lastRawOffset) {
                this.consistencyError("Invalid rawOffset=" + rawOffset + " >= lastRawOffset=" + lastRawOffset + " at index=" + i);
            }
            if (offset < lastOffset) {
                this.consistencyError("Invalid offset=" + offset + " >= lastOffset=" + lastOffset + " at index=" + i);
            }
            lastRawOffset = rawOffset;
            lastOffset = offset;
        }
    }

    @Override
    protected String dumpInternals() {
        return GapList.super.dumpInternals() + ", offGap(s=" + this.offsetGapStart + ", l=" + this.offsetGapLength + ")";
    }
}

