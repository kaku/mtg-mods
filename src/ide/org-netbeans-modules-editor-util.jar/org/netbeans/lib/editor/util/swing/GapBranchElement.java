/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import javax.swing.event.DocumentEvent;
import javax.swing.text.Element;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.netbeans.lib.editor.util.GapList;

public abstract class GapBranchElement
implements Element {
    protected static final Element[] EMPTY_ELEMENT_ARRAY = new Element[0];
    private final GapList children = new GapList();

    @Override
    public int getElementCount() {
        return this.children.size();
    }

    @Override
    public Element getElement(int index) {
        return (Element)this.children.get(index);
    }

    public void copyElements(int srcBegin, int srcEnd, Element[] dst, int dstBegin) {
        this.children.copyElements(srcBegin, srcEnd, dst, dstBegin);
    }

    @Override
    public int getElementIndex(int offset) {
        int low = 0;
        int high = this.getElementCount() - 1;
        if (high == -1) {
            return -1;
        }
        while (low <= high) {
            int mid = (low + high) / 2;
            int elemStartOffset = this.getElement(mid).getStartOffset();
            if (elemStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (elemStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        if (high < 0) {
            high = 0;
        }
        return high;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    protected void replace(int index, int removeCount, Element[] addedElems) {
        if (removeCount > 0) {
            this.children.remove(index, removeCount);
        }
        if (addedElems != null) {
            this.children.addArray(index, addedElems);
        }
    }

    public String toString() {
        return this.children.toString();
    }

    public abstract class LastIndex {
        private int lastReturnedElementIndex;

        public int getElementIndex(int offset) {
            int low = 0;
            int high = GapBranchElement.this.getElementCount() - 1;
            if (high == -1) {
                return -1;
            }
            int lastIndex = this.lastReturnedElementIndex;
            if (lastIndex >= low && lastIndex <= high) {
                Element lastElem = GapBranchElement.this.getElement(lastIndex);
                int lastElemStartOffset = lastElem.getStartOffset();
                if (offset >= lastElemStartOffset) {
                    int lastElemEndOffset = lastElem.getEndOffset();
                    if (offset < lastElemEndOffset) {
                        return lastIndex;
                    }
                    low = lastIndex + 1;
                } else {
                    high = lastIndex - 1;
                }
            }
            while (low <= high) {
                int mid = (low + high) / 2;
                int elemStartOffset = ((Element)GapBranchElement.this.children.get(mid)).getStartOffset();
                if (elemStartOffset < offset) {
                    low = mid + 1;
                    continue;
                }
                if (elemStartOffset > offset) {
                    high = mid - 1;
                    continue;
                }
                this.lastReturnedElementIndex = mid;
                return mid;
            }
            if (high < 0) {
                high = 0;
            }
            this.lastReturnedElementIndex = high;
            return high;
        }
    }

    public class Edit
    extends AbstractUndoableEdit
    implements DocumentEvent.ElementChange {
        private int index;
        private Element[] childrenAdded;
        private Element[] childrenRemoved;

        public Edit(int index, Element[] childrenRemoved, Element[] childrenAdded) {
            this.index = index;
            this.childrenRemoved = childrenRemoved;
            this.childrenAdded = childrenAdded;
        }

        @Override
        public Element getElement() {
            return GapBranchElement.this;
        }

        @Override
        public int getIndex() {
            return this.index;
        }

        @Override
        public Element[] getChildrenRemoved() {
            return this.childrenRemoved;
        }

        @Override
        public Element[] getChildrenAdded() {
            return this.childrenAdded;
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            GapBranchElement.this.replace(this.index, this.childrenAdded.length, this.childrenRemoved);
            Element[] tmp = this.childrenRemoved;
            this.childrenRemoved = this.childrenAdded;
            this.childrenAdded = tmp;
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            Element[] tmp = this.childrenRemoved;
            this.childrenRemoved = this.childrenAdded;
            this.childrenAdded = tmp;
            GapBranchElement.this.replace(this.index, this.childrenRemoved.length, this.childrenAdded);
        }
    }

}

