/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Field;
import java.util.EventListener;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Segment;
import javax.swing.text.StyledDocument;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.lib.editor.util.AbstractCharSequence;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.CompactMap;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.PriorityDocumentListenerList;

public final class DocumentUtilities {
    private static final String VERSION_PROP = "version";
    private static final String LAST_MODIFICATION_TIMESTAMP_PROP = "last-modification-timestamp";
    private static final Object TYPING_MODIFICATION_DOCUMENT_PROPERTY = new Object();
    private static final Object TYPING_MODIFICATION_KEY = new Object();
    private static Field numReadersField;
    private static Field currWriterField;

    private DocumentUtilities() {
    }

    public static void addDocumentListener(Document doc, DocumentListener listener, DocumentListenerPriority priority) {
        if (!DocumentUtilities.addPriorityDocumentListener(doc, listener, priority)) {
            doc.addDocumentListener(listener);
        }
    }

    public static boolean addPriorityDocumentListener(Document doc, DocumentListener listener, DocumentListenerPriority priority) {
        PriorityDocumentListenerList priorityDocumentListenerList = (PriorityDocumentListenerList)doc.getProperty(PriorityDocumentListenerList.class);
        if (priorityDocumentListenerList != null) {
            priorityDocumentListenerList.add(listener, priority.getPriority());
            return true;
        }
        return false;
    }

    public static void removeDocumentListener(Document doc, DocumentListener listener, DocumentListenerPriority priority) {
        if (!DocumentUtilities.removePriorityDocumentListener(doc, listener, priority)) {
            doc.removeDocumentListener(listener);
        }
    }

    public static boolean removePriorityDocumentListener(Document doc, DocumentListener listener, DocumentListenerPriority priority) {
        PriorityDocumentListenerList priorityDocumentListenerList = (PriorityDocumentListenerList)doc.getProperty(PriorityDocumentListenerList.class);
        if (priorityDocumentListenerList != null) {
            priorityDocumentListenerList.remove(listener, priority.getPriority());
            return true;
        }
        return false;
    }

    public static DocumentListener initPriorityListening(Document doc) {
        if (doc.getProperty(PriorityDocumentListenerList.class) != null) {
            throw new IllegalStateException("PriorityDocumentListenerList already initialized for doc=" + doc);
        }
        PriorityDocumentListenerList listener = new PriorityDocumentListenerList();
        doc.putProperty(PriorityDocumentListenerList.class, listener);
        return listener;
    }

    public static int getDocumentListenerCount(Document doc) {
        PriorityDocumentListenerList pdll = (PriorityDocumentListenerList)doc.getProperty(PriorityDocumentListenerList.class);
        return pdll != null ? pdll.getListenerCount() : (doc instanceof AbstractDocument ? ((DocumentListener[])((AbstractDocument)doc).getListeners(DocumentListener.class)).length : 0);
    }

    public static void setTypingModification(Document doc, boolean typingModification) {
        doc.putProperty(TYPING_MODIFICATION_DOCUMENT_PROPERTY, typingModification);
    }

    public static boolean isTypingModification(Document doc) {
        Boolean b = (Boolean)doc.getProperty(TYPING_MODIFICATION_DOCUMENT_PROPERTY);
        return b != null ? b : false;
    }

    public static boolean isTypingModification(DocumentEvent evt) {
        return DocumentUtilities.isTypingModification(evt.getDocument());
    }

    public static CharSequence getText(Document doc) {
        CharSequence text = (CharSequence)doc.getProperty(CharSequence.class);
        if (text == null) {
            text = new DocumentCharSequence(doc);
            doc.putProperty(CharSequence.class, text);
        }
        return text;
    }

    public static CharSequence getText(Document doc, int offset, int length) throws BadLocationException {
        CharSequence text = DocumentUtilities.getText(doc);
        try {
            return text.subSequence(offset, offset + length);
        }
        catch (IndexOutOfBoundsException e) {
            int badOffset = offset;
            if (offset >= 0 && offset + length > text.length()) {
                badOffset = length;
            }
            BadLocationException ble = new BadLocationException(e.getMessage(), badOffset);
            ble.initCause(e);
            throw ble;
        }
    }

    public static void addEventPropertyStorage(DocumentEvent evt) {
        if (!(evt instanceof UndoableEdit)) {
            throw new IllegalStateException("evt not instanceof UndoableEdit: " + evt);
        }
        ((UndoableEdit)((Object)evt)).addEdit(new EventPropertiesElementChange());
    }

    public static Object getEventProperty(DocumentEvent evt, Object key) {
        EventPropertiesElementChange change = (EventPropertiesElementChange)evt.getChange(EventPropertiesElement.INSTANCE);
        return change != null ? change.getProperty(key) : null;
    }

    public static void putEventProperty(DocumentEvent evt, Object key, Object value) {
        EventPropertiesElementChange change = (EventPropertiesElementChange)evt.getChange(EventPropertiesElement.INSTANCE);
        if (change == null) {
            throw new IllegalStateException("addEventPropertyStorage() not called for evt=" + evt);
        }
        change.putProperty(key, value);
    }

    public static void putEventProperty(DocumentEvent evt, Map.Entry mapEntry) {
        if (mapEntry instanceof CompactMap.MapEntry) {
            EventPropertiesElementChange change = (EventPropertiesElementChange)evt.getChange(EventPropertiesElement.INSTANCE);
            if (change == null) {
                throw new IllegalStateException("addEventPropertyStorage() not called for evt=" + evt);
            }
            change.putEntry((CompactMap.MapEntry)mapEntry);
        } else {
            DocumentUtilities.putEventProperty(evt, mapEntry.getKey(), mapEntry.getValue());
        }
    }

    public static int fixOffset(int offset, DocumentEvent evt) {
        int modOffset = evt.getOffset();
        if (evt.getType() == DocumentEvent.EventType.INSERT) {
            if (offset >= modOffset) {
                offset += evt.getLength();
            }
        } else if (evt.getType() == DocumentEvent.EventType.REMOVE && offset > modOffset) {
            offset = Math.min(offset - evt.getLength(), modOffset);
        }
        return offset;
    }

    public static String getModificationText(DocumentEvent evt) {
        return (String)DocumentUtilities.getEventProperty(evt, String.class);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isReadLocked(Document doc) {
        if (DocumentUtilities.checkAbstractDoc(doc)) {
            Object f;
            if (DocumentUtilities.isWriteLocked(doc)) {
                return true;
            }
            if (numReadersField == null) {
                f = null;
                try {
                    f = AbstractDocument.class.getDeclaredField("numReaders");
                }
                catch (NoSuchFieldException ex2) {
                    throw new IllegalStateException(ex2);
                }
                f.setAccessible(true);
                Document ex2 = doc;
                synchronized (ex2) {
                    numReadersField = f;
                }
            }
            try {
                f = doc;
                synchronized (f) {
                    return numReadersField.getInt(doc) > 0;
                }
            }
            catch (IllegalAccessException ex) {
                throw new IllegalStateException(ex);
            }
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isWriteLocked(Document doc) {
        if (DocumentUtilities.checkAbstractDoc(doc)) {
            Object f;
            if (currWriterField == null) {
                f = null;
                try {
                    f = AbstractDocument.class.getDeclaredField("currWriter");
                }
                catch (NoSuchFieldException ex2) {
                    throw new IllegalStateException(ex2);
                }
                f.setAccessible(true);
                Document ex2 = doc;
                synchronized (ex2) {
                    currWriterField = f;
                }
            }
            try {
                f = doc;
                synchronized (f) {
                    return currWriterField.get(doc) == Thread.currentThread();
                }
            }
            catch (IllegalAccessException ex) {
                throw new IllegalStateException(ex);
            }
        }
        return false;
    }

    private static boolean checkAbstractDoc(Document doc) {
        if (doc == null) {
            throw new IllegalArgumentException("document is null");
        }
        return doc instanceof AbstractDocument;
    }

    public static Element getParagraphElement(Document doc, int offset) {
        Element paragraph;
        if (doc instanceof StyledDocument) {
            paragraph = ((StyledDocument)doc).getParagraphElement(offset);
        } else {
            int index;
            Element rootElem = doc.getDefaultRootElement();
            paragraph = rootElem.getElement(index = rootElem.getElementIndex(offset));
            if (offset < paragraph.getStartOffset() || offset >= paragraph.getEndOffset()) {
                paragraph = null;
            }
        }
        return paragraph;
    }

    public static Element getParagraphRootElement(Document doc) {
        if (doc instanceof StyledDocument) {
            return ((StyledDocument)doc).getParagraphElement(0).getParentElement();
        }
        return doc.getDefaultRootElement().getElement(0).getParentElement();
    }

    public static String debugOffset(Document doc, int offset) {
        return DocumentUtilities.appendOffset(null, doc, offset).toString();
    }

    public static StringBuilder appendOffset(StringBuilder sb, Document doc, int offset) {
        if (sb == null) {
            sb = new StringBuilder(50);
        }
        sb.append(offset).append('[');
        if (offset < 0) {
            sb.append("<0");
        } else if (offset > doc.getLength() + 1) {
            sb.append(">").append(doc.getLength());
        } else {
            Element paragraphRoot = DocumentUtilities.getParagraphRootElement(doc);
            int lineIndex = paragraphRoot.getElementIndex(offset);
            Element lineElem = paragraphRoot.getElement(lineIndex);
            sb.append(lineIndex + 1).append(':');
            sb.append(DocumentUtilities.visualColumn(doc, lineElem.getStartOffset(), offset) + 1);
        }
        sb.append(']');
        return sb;
    }

    public static StringBuilder appendEvent(StringBuilder sb, DocumentEvent evt) {
        if (sb == null) {
            sb = new StringBuilder(100);
        }
        DocumentEvent.EventType type = evt.getType();
        sb.append(type).append(", ");
        DocumentUtilities.appendOffset(sb, evt.getDocument(), evt.getOffset());
        sb.append(", l=").append(evt.getLength());
        String modText = DocumentUtilities.getModificationText(evt);
        if (modText != null) {
            sb.append(", modText=\"");
            CharSequenceUtilities.debugText(sb, (CharSequence)modText);
            sb.append('\"');
        }
        return sb;
    }

    private static int visualColumn(Document doc, int lineStartOffset, int offset) {
        Integer tabSizeInteger = (Integer)doc.getProperty("tabSize");
        int tabSize = tabSizeInteger != null ? tabSizeInteger : 8;
        CharSequence docText = DocumentUtilities.getText(doc);
        int column = 0;
        for (int i = lineStartOffset; i < offset; ++i) {
            char c = docText.charAt(i);
            if (c == '\t') {
                column = (column + tabSize) / tabSize * tabSize;
                continue;
            }
            ++column;
        }
        return column;
    }

    public static String getMimeType(Document doc) {
        return (String)doc.getProperty("mimeType");
    }

    public static String getMimeType(JTextComponent component) {
        EditorKit kit;
        Document doc = component.getDocument();
        String mimeType = DocumentUtilities.getMimeType(doc);
        if (mimeType == null && (kit = component.getUI().getEditorKit(component)) != null) {
            mimeType = kit.getContentType();
        }
        return mimeType;
    }

    public static long getDocumentVersion(Document doc) {
        Object version = doc.getProperty("version");
        return version instanceof AtomicLong ? ((AtomicLong)version).get() : 0;
    }

    public static long getDocumentTimestamp(Document doc) {
        Object version = doc.getProperty("last-modification-timestamp");
        return version instanceof AtomicLong ? ((AtomicLong)version).get() : 0;
    }

    public static void addPropertyChangeListener(Document doc, PropertyChangeListener l) {
        PropertyChangeSupport pcs = (PropertyChangeSupport)doc.getProperty(PropertyChangeSupport.class);
        if (pcs != null) {
            pcs.addPropertyChangeListener(l);
        }
    }

    public static void removePropertyChangeListener(Document doc, PropertyChangeListener l) {
        PropertyChangeSupport pcs = (PropertyChangeSupport)doc.getProperty(PropertyChangeSupport.class);
        if (pcs != null) {
            pcs.removePropertyChangeListener(l);
        }
    }

    private static final class EventPropertiesElementChange
    implements DocumentEvent.ElementChange,
    UndoableEdit {
        private CompactMap eventProperties = new CompactMap();

        private EventPropertiesElementChange() {
        }

        public synchronized Object getProperty(Object key) {
            return this.eventProperties != null ? this.eventProperties.get(key) : null;
        }

        public synchronized Object putProperty(Object key, Object value) {
            return this.eventProperties.put(key, value);
        }

        public synchronized CompactMap.MapEntry putEntry(CompactMap.MapEntry entry) {
            return this.eventProperties.putEntry(entry);
        }

        @Override
        public int getIndex() {
            return -1;
        }

        @Override
        public Element getElement() {
            return EventPropertiesElement.INSTANCE;
        }

        @Override
        public Element[] getChildrenRemoved() {
            return null;
        }

        @Override
        public Element[] getChildrenAdded() {
            return null;
        }

        @Override
        public boolean replaceEdit(UndoableEdit anEdit) {
            return false;
        }

        @Override
        public boolean addEdit(UndoableEdit anEdit) {
            return false;
        }

        @Override
        public void undo() throws CannotUndoException {
        }

        @Override
        public void redo() throws CannotRedoException {
        }

        @Override
        public boolean isSignificant() {
            return false;
        }

        @Override
        public String getUndoPresentationName() {
            return "";
        }

        @Override
        public String getRedoPresentationName() {
            return "";
        }

        @Override
        public String getPresentationName() {
            return "";
        }

        @Override
        public void die() {
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public boolean canRedo() {
            return true;
        }
    }

    private static final class EventPropertiesElement
    implements Element {
        static final EventPropertiesElement INSTANCE = new EventPropertiesElement();

        private EventPropertiesElement() {
        }

        @Override
        public int getStartOffset() {
            return 0;
        }

        @Override
        public int getEndOffset() {
            return 0;
        }

        @Override
        public int getElementCount() {
            return 0;
        }

        @Override
        public int getElementIndex(int offset) {
            return -1;
        }

        @Override
        public Element getElement(int index) {
            return null;
        }

        @Override
        public boolean isLeaf() {
            return true;
        }

        @Override
        public Element getParentElement() {
            return null;
        }

        @Override
        public String getName() {
            return "Helper element for modification text providing";
        }

        @Override
        public Document getDocument() {
            return null;
        }

        @Override
        public AttributeSet getAttributes() {
            return null;
        }

        public String toString() {
            return this.getName();
        }
    }

    private static final class DocumentCharSequence
    extends AbstractCharSequence.StringLike {
        private final Segment segment = new Segment();
        private final Document doc;

        DocumentCharSequence(Document doc) {
            this.doc = doc;
        }

        @Override
        public int length() {
            return this.doc.getLength() + 1;
        }

        @Override
        public synchronized char charAt(int index) {
            try {
                this.doc.getText(index, 1, this.segment);
            }
            catch (BadLocationException e) {
                IndexOutOfBoundsException ioobe = new IndexOutOfBoundsException(e.getMessage() + " at offset=" + e.offsetRequested());
                ioobe.initCause(e);
                throw ioobe;
            }
            char ch = this.segment.array[this.segment.offset];
            this.segment.array = null;
            return ch;
        }
    }

}

