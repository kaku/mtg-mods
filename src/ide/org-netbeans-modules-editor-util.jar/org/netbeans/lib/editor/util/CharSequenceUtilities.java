/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

public final class CharSequenceUtilities {
    private CharSequenceUtilities() {
    }

    public static int stringLikeHashCode(CharSequence text) {
        int len = text.length();
        int h = 0;
        for (int i = 0; i < len; ++i) {
            h = 31 * h + text.charAt(i);
        }
        return h;
    }

    public static boolean equals(CharSequence text, Object o) {
        if (text == o) {
            return true;
        }
        if (text != null && o instanceof CharSequence) {
            return CharSequenceUtilities.textEquals(text, (CharSequence)o);
        }
        return false;
    }

    public static boolean textEquals(CharSequence text1, CharSequence text2) {
        if (text1 == text2) {
            return true;
        }
        int len = text1.length();
        if (len == text2.length()) {
            for (int i = len - 1; i >= 0; --i) {
                if (text1.charAt(i) == text2.charAt(i)) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    public static String toString(CharSequence text) {
        StringBuilder sb = new StringBuilder(text.length());
        sb.append(text);
        return sb.toString();
    }

    public static String toString(CharSequence text, int start, int end) {
        CharSequenceUtilities.checkIndexesValid(text, start, end);
        StringBuilder sb = new StringBuilder(end - start);
        sb.append(text, start, end);
        return sb.toString();
    }

    public static void append(StringBuffer sb, CharSequence text) {
        sb.append(text);
    }

    public static void append(StringBuffer sb, CharSequence text, int start, int end) {
        CharSequenceUtilities.checkIndexesValid(text, start, end);
        while (start < end) {
            sb.append(text.charAt(start++));
        }
    }

    public static int indexOf(CharSequence text, int ch) {
        return CharSequenceUtilities.indexOf(text, ch, 0);
    }

    public static int indexOf(CharSequence text, int ch, int fromIndex) {
        int length = text.length();
        while (fromIndex < length) {
            if (text.charAt(fromIndex) == ch) {
                return fromIndex;
            }
            ++fromIndex;
        }
        return -1;
    }

    public static int indexOf(CharSequence text, CharSequence seq) {
        return CharSequenceUtilities.indexOf(text, seq, 0);
    }

    public static int indexOf(CharSequence text, CharSequence seq, int fromIndex) {
        int textLength = text.length();
        int seqLength = seq.length();
        if (fromIndex >= textLength) {
            return seqLength == 0 ? textLength : -1;
        }
        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (seqLength == 0) {
            return fromIndex;
        }
        char first = seq.charAt(0);
        int max = textLength - seqLength;
        for (int i = fromIndex; i <= max; ++i) {
            if (text.charAt(i) != first) {
                while (++i <= max && text.charAt(i) != first) {
                }
            }
            if (i > max) continue;
            int j = i + 1;
            int end = j + seqLength - 1;
            int k = 1;
            while (j < end && text.charAt(j) == seq.charAt(k)) {
                ++j;
                ++k;
            }
            if (j != end) continue;
            return i;
        }
        return -1;
    }

    public static int lastIndexOf(CharSequence text, CharSequence seq) {
        return CharSequenceUtilities.lastIndexOf(text, seq, text.length());
    }

    public static int lastIndexOf(CharSequence text, CharSequence seq, int fromIndex) {
        int start;
        int textLength = text.length();
        int seqLength = seq.length();
        int rightIndex = textLength - seqLength;
        if (fromIndex < 0) {
            return -1;
        }
        if (fromIndex > rightIndex) {
            fromIndex = rightIndex;
        }
        if (seqLength == 0) {
            return fromIndex;
        }
        int strLastIndex = seqLength - 1;
        char strLastChar = seq.charAt(strLastIndex);
        int min = seqLength - 1;
        int i = min + fromIndex;
        block0 : do {
            if (i >= min && text.charAt(i) != strLastChar) {
                --i;
                continue;
            }
            if (i < min) {
                return -1;
            }
            int j = i - 1;
            start = j - (seqLength - 1);
            int k = strLastIndex - 1;
            while (j > start) {
                if (text.charAt(j--) == seq.charAt(k--)) continue;
                --i;
                continue block0;
                continue block0;
            }
            break block0;
            break;
        } while (true);
        return start + 1;
    }

    public static int lastIndexOf(CharSequence text, int ch) {
        return CharSequenceUtilities.lastIndexOf(text, ch, text.length() - 1);
    }

    public static int lastIndexOf(CharSequence text, int ch, int fromIndex) {
        if (fromIndex > text.length() - 1) {
            fromIndex = text.length() - 1;
        }
        while (fromIndex >= 0) {
            if (text.charAt(fromIndex) == ch) {
                return fromIndex;
            }
            --fromIndex;
        }
        return -1;
    }

    public static boolean startsWith(CharSequence text, CharSequence prefix) {
        int p_length = prefix.length();
        if (p_length > text.length()) {
            return false;
        }
        for (int x = 0; x < p_length; ++x) {
            if (text.charAt(x) == prefix.charAt(x)) continue;
            return false;
        }
        return true;
    }

    public static boolean endsWith(CharSequence text, CharSequence suffix) {
        int text_length;
        int s_length = suffix.length();
        if (s_length > (text_length = text.length())) {
            return false;
        }
        for (int x = 0; x < s_length; ++x) {
            if (text.charAt(text_length - s_length + x) == suffix.charAt(x)) continue;
            return false;
        }
        return true;
    }

    public static CharSequence trim(CharSequence text) {
        int start;
        int length = text.length();
        if (length == 0) {
            return text;
        }
        int end = length - 1;
        for (start = 0; start < length && text.charAt(start) <= ' '; ++start) {
        }
        if (start == length) {
            return text.subSequence(0, 0);
        }
        while (end > start && text.charAt(end) <= ' ') {
            --end;
        }
        return text.subSequence(start, end + 1);
    }

    public static void debugChar(StringBuffer sb, char ch) {
        switch (ch) {
            case '\n': {
                sb.append("\\n");
                break;
            }
            case '\r': {
                sb.append("\\r");
                break;
            }
            case '\t': {
                sb.append("\\t");
                break;
            }
            case '\b': {
                sb.append("\\b");
                break;
            }
            case '\f': {
                sb.append("\\f");
                break;
            }
            case '\\': {
                sb.append("\\\\");
                break;
            }
            default: {
                sb.append(ch);
            }
        }
    }

    public static void debugChar(StringBuilder sb, char ch) {
        switch (ch) {
            case '\n': {
                sb.append("\\n");
                break;
            }
            case '\r': {
                sb.append("\\r");
                break;
            }
            case '\t': {
                sb.append("\\t");
                break;
            }
            case '\b': {
                sb.append("\\b");
                break;
            }
            case '\f': {
                sb.append("\\f");
                break;
            }
            case '\\': {
                sb.append("\\\\");
                break;
            }
            default: {
                sb.append(ch);
            }
        }
    }

    public static String debugChar(char ch) {
        StringBuilder sb = new StringBuilder();
        CharSequenceUtilities.debugChar(sb, ch);
        return sb.toString();
    }

    public static void debugText(StringBuffer sb, CharSequence text) {
        for (int i = 0; i < text.length(); ++i) {
            CharSequenceUtilities.debugChar(sb, text.charAt(i));
        }
    }

    public static void debugText(StringBuilder sb, CharSequence text) {
        for (int i = 0; i < text.length(); ++i) {
            CharSequenceUtilities.debugChar(sb, text.charAt(i));
        }
    }

    public static String debugText(CharSequence text) {
        StringBuilder sb = new StringBuilder();
        CharSequenceUtilities.debugText(sb, text);
        return sb.toString();
    }

    public static void checkIndexNonNegative(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("index=" + index + " < 0");
        }
    }

    public static void checkIndexValid(int index, int length) {
        CharSequenceUtilities.checkIndexNonNegative(index);
        if (index >= length) {
            throw new IndexOutOfBoundsException("index=" + index + " >= length()=" + length);
        }
    }

    public static void checkIndexesValid(int start, int end, int length) {
        if (start < 0) {
            throw new IndexOutOfBoundsException("start=" + start + " < 0");
        }
        if (end < start) {
            throw new IndexOutOfBoundsException("end=" + end + " < start=" + start);
        }
        if (end > length) {
            throw new IndexOutOfBoundsException("end=" + end + " > length()=" + length);
        }
    }

    public static void checkIndexesValid(CharSequence text, int start, int end) {
        CharSequenceUtilities.checkIndexesValid(start, end, text.length());
    }
}

