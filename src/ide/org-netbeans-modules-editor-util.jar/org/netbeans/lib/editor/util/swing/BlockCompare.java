/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

public final class BlockCompare {
    private static final int BEFORE = 1;
    private static final int AFTER = 2;
    private static final int INSIDE = 4;
    private static final int CONTAINS = 8;
    private static final int OVERLAP_START = 16;
    private static final int OVERLAP_END = 32;
    private static final int EMPTY_X = 64;
    private static final int EMPTY_Y = 128;
    private static final int EQUAL_START = 256;
    private static final int EQUAL_END = 512;
    private static final int LOWER_START = 1024;
    private static final int LOWER_END = 2048;
    private static final int INVALID_X = 4096;
    private static final int INVALID_Y = 8192;
    private final int value;

    public static BlockCompare get(int xStartOffset, int xEndOffset, int yStartOffset, int yEndOffset) {
        return new BlockCompare(BlockCompare.resolve(xStartOffset, xEndOffset, yStartOffset, yEndOffset));
    }

    private BlockCompare(int value) {
        this.value = value;
    }

    public boolean before() {
        return (this.value & 1) != 0;
    }

    public boolean after() {
        return (this.value & 2) != 0;
    }

    public boolean inside() {
        return (this.value & 4) != 0;
    }

    public boolean insideStrict() {
        return (this.value & 12) == 4;
    }

    public boolean contains() {
        return (this.value & 8) != 0;
    }

    public boolean containsStrict() {
        return (this.value & 12) == 8;
    }

    public boolean equal() {
        return (this.value & 12) == 12;
    }

    public boolean overlap() {
        return (this.value & 48) != 0;
    }

    public boolean overlapStart() {
        return (this.value & 16) != 0;
    }

    public boolean overlapEnd() {
        return (this.value & 32) != 0;
    }

    public boolean emptyX() {
        return (this.value & 64) != 0;
    }

    public boolean emptyY() {
        return (this.value & 128) != 0;
    }

    public boolean invalidX() {
        return (this.value & 4096) != 0;
    }

    public boolean invalidY() {
        return (this.value & 8192) != 0;
    }

    public boolean equalStart() {
        return (this.value & 256) != 0;
    }

    public boolean equalEnd() {
        return (this.value & 512) != 0;
    }

    public boolean lowerStart() {
        return (this.value & 1024) != 0;
    }

    public boolean lowerEnd() {
        return (this.value & 2048) != 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(50);
        this.appendBit(sb, 1, "BEFORE");
        this.appendBit(sb, 2, "AFTER");
        this.appendBit(sb, 8, "CONTAINS");
        this.appendBit(sb, 4, "INSIDE");
        this.appendBit(sb, 16, "OVERLAP_START");
        this.appendBit(sb, 32, "OVERLAP_END");
        this.appendBit(sb, 64, "EMPTY_X");
        this.appendBit(sb, 128, "EMPTY_Y");
        this.appendBit(sb, 256, "EQUAL_START");
        this.appendBit(sb, 512, "EQUAL_END");
        this.appendBit(sb, 1024, "LOWER_START");
        this.appendBit(sb, 2048, "LOWER_END");
        this.appendBit(sb, 4096, "INVALID_X");
        this.appendBit(sb, 8192, "INVALID_Y");
        return sb.toString();
    }

    private void appendBit(StringBuilder sb, int bitValue, String bitText) {
        if ((this.value & bitValue) != 0) {
            if (sb.length() != 0) {
                sb.append('|');
            }
            sb.append(bitText);
        }
    }

    private static int resolve(int xStartOffset, int xEndOffset, int yStartOffset, int yEndOffset) {
        int value = 0;
        if (xStartOffset > xEndOffset) {
            value |= 4096;
            xEndOffset = xStartOffset;
        }
        if (yStartOffset > yEndOffset) {
            yEndOffset = yStartOffset;
            value |= 8192;
        }
        if (xEndOffset < yStartOffset) {
            value |= 3073;
            if (xStartOffset == xEndOffset) {
                value |= 64;
            }
            if (yStartOffset == yEndOffset) {
                value |= 128;
            }
        } else if (xEndOffset == yStartOffset) {
            value = xStartOffset == xEndOffset ? (yStartOffset == yEndOffset ? (value |= 975) : (value |= 2377)) : (yStartOffset == yEndOffset ? (value |= 1669) : (value |= 3073));
        } else if (yEndOffset < xStartOffset) {
            value |= 2;
            if (xStartOffset == xEndOffset) {
                value |= 64;
            }
            if (yStartOffset == yEndOffset) {
                value |= 128;
            }
        } else if (xStartOffset == yEndOffset) {
            value = xStartOffset == xEndOffset ? (yStartOffset == yEndOffset ? (value |= 975) : (value |= 1610)) : (yStartOffset == yEndOffset ? (value |= 399) : (value |= 2));
        } else {
            if (xStartOffset < yStartOffset) {
                if (xEndOffset < yEndOffset) {
                    value |= 3088;
                } else {
                    value |= 1032;
                    if (xEndOffset == yEndOffset) {
                        value |= 512;
                    }
                }
            } else if (xStartOffset == yStartOffset) {
                value = xEndOffset < yEndOffset ? (value |= 2308) : (xEndOffset == yEndOffset ? (value |= 780) : (value |= 264));
            } else if (xEndOffset <= yEndOffset) {
                value |= 4;
                value = xEndOffset == yEndOffset ? (value |= 512) : (value |= 2048);
            } else {
                value |= 32;
            }
            if (xStartOffset == xEndOffset) {
                value |= 64;
            }
            if (yStartOffset == yEndOffset) {
                value |= 128;
            }
        }
        return value;
    }
}

