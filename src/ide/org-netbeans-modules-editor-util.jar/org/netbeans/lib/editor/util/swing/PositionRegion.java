/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import java.util.Comparator;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;

public class PositionRegion {
    private static Comparator<PositionRegion> comparator;
    private Position startPosition;
    private Position endPosition;

    public static final Comparator<PositionRegion> getComparator() {
        if (comparator == null) {
            comparator = new Comparator<PositionRegion>(){

                @Override
                public int compare(PositionRegion pr1, PositionRegion pr2) {
                    return pr1.getStartOffset() - pr2.getStartOffset();
                }
            };
        }
        return comparator;
    }

    public static Position createFixedPosition(final int offset) {
        if (offset < 0) {
            throw new IllegalArgumentException("offset < 0");
        }
        return new Position(){

            @Override
            public int getOffset() {
                return offset;
            }
        };
    }

    public static boolean isRegionsSorted(List<? extends PositionRegion> positionRegionList) {
        for (int i = positionRegionList.size() - 2; i >= 0; --i) {
            if (PositionRegion.getComparator().compare(positionRegionList.get(i), positionRegionList.get(i + 1)) <= 0) continue;
            return false;
        }
        return true;
    }

    public PositionRegion(Position startPosition, Position endPosition) {
        PositionRegion.assertPositionsValid(startPosition, endPosition);
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    public PositionRegion(Document doc, int startOffset, int endOffset) throws BadLocationException {
        this(doc.createPosition(startOffset), doc.createPosition(endOffset));
    }

    public final int getStartOffset() {
        return this.startPosition.getOffset();
    }

    public final Position getStartPosition() {
        return this.startPosition;
    }

    public final int getEndOffset() {
        return this.endPosition.getOffset();
    }

    public final Position getEndPosition() {
        return this.endPosition;
    }

    public final int getLength() {
        return this.getEndOffset() - this.getStartOffset();
    }

    public CharSequence getText(Document doc) {
        int startOffset = this.getStartOffset();
        return DocumentUtilities.getText(doc).subSequence(startOffset, this.getEndOffset() - startOffset);
    }

    public String getString(Document doc) throws BadLocationException {
        int startOffset = this.getStartOffset();
        return doc.getText(startOffset, this.getEndOffset() - startOffset);
    }

    public String toString(Document doc) {
        return new StringBuilder(35).append('<').append(DocumentUtilities.debugOffset(doc, this.getStartOffset())).append(',').append(DocumentUtilities.debugOffset(doc, this.getEndOffset())).append('>').toString();
    }

    public String toString() {
        return new StringBuilder(15).append('<').append(this.getStartOffset()).append(", ").append(this.getEndOffset()).append('>').toString();
    }

    void resetImpl(Position startPosition, Position endPosition) {
        PositionRegion.assertPositionsValid(startPosition, endPosition);
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    void setStartPositionImpl(Position startPosition) {
        PositionRegion.assertPositionsValid(startPosition, this.endPosition);
        this.startPosition = startPosition;
    }

    void setEndPositionImpl(Position endPosition) {
        PositionRegion.assertPositionsValid(this.startPosition, endPosition);
        this.endPosition = endPosition;
    }

    private static void assertPositionsValid(Position startPos, Position endPos) {
        assert (startPos.getOffset() <= endPos.getOffset());
    }

}

