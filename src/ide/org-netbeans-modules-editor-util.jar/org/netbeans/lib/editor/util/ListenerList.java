/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public final class ListenerList<T extends EventListener>
implements Serializable {
    static final long serialVersionUID = 0;
    private static final EventListener[] EMPTY_LISTENER_ARRAY = new EventListener[0];
    private transient ImmutableList<T> listenersList = new ImmutableList(EMPTY_LISTENER_ARRAY);

    public synchronized List<T> getListeners() {
        return this.listenersList;
    }

    public synchronized int getListenerCount() {
        return this.listenersList.size();
    }

    public synchronized void add(T listener) {
        if (listener == null) {
            return;
        }
        EventListener[] arr = new EventListener[this.listenersList.getArray().length + 1];
        if (arr.length > 1) {
            System.arraycopy(this.listenersList.getArray(), 0, arr, 0, arr.length - 1);
        }
        arr[arr.length - 1] = listener;
        this.listenersList = new ImmutableList(arr);
    }

    public synchronized void remove(T listener) {
        if (listener == null) {
            return;
        }
        int idx = this.listenersList.indexOf(listener);
        if (idx == -1) {
            return;
        }
        EventListener[] arr = new EventListener[this.listenersList.getArray().length - 1];
        if (arr.length > 0) {
            System.arraycopy(this.listenersList.getArray(), 0, arr, 0, idx);
        }
        if (arr.length > idx) {
            System.arraycopy(this.listenersList.getArray(), idx + 1, arr, idx, this.listenersList.getArray().length - idx - 1);
        }
        this.listenersList = new ImmutableList(arr);
    }

    private void writeObject(ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();
        for (EventListener l : this.listenersList) {
            if (!(l instanceof Serializable)) continue;
            s.writeObject(l);
        }
        s.writeObject(null);
    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        Object listenerOrNull;
        s.defaultReadObject();
        ArrayList<EventListener> lList = new ArrayList<EventListener>();
        while (null != (listenerOrNull = s.readObject())) {
            EventListener l = (EventListener)listenerOrNull;
            lList.add(l);
        }
        this.listenersList = new ImmutableList(lList.toArray(new EventListener[lList.size()]));
    }

    public String toString() {
        return this.listenersList.toString();
    }

    private static final class ImmutableList<E extends EventListener>
    extends AbstractList<E> {
        private EventListener[] array;

        public ImmutableList(EventListener[] array) {
            assert (array != null);
            this.array = array;
        }

        @Override
        public E get(int index) {
            if (index >= 0 && index < this.array.length) {
                EventListener element = this.array[index];
                return (E)element;
            }
            throw new IndexOutOfBoundsException("index = " + index + ", size = " + this.array.length);
        }

        @Override
        public int size() {
            return this.array.length;
        }

        public EventListener[] getArray() {
            return this.array;
        }
    }

}

