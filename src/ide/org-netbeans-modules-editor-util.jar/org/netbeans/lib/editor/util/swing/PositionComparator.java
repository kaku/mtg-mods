/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util.swing;

import java.util.Comparator;
import javax.swing.text.Position;

public final class PositionComparator
implements Comparator<Position> {
    public static final PositionComparator INSTANCE = new PositionComparator();

    @Override
    public int compare(Position p1, Position p2) {
        return p1.getOffset() - p2.getOffset();
    }
}

