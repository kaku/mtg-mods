/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.util;

import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.CharSubSequence;

public abstract class AbstractCharSequence
implements CharSequence {
    @Override
    public abstract int length();

    @Override
    public abstract char charAt(int var1);

    private String toString(int start, int end) {
        return CharSequenceUtilities.toString(this, start, end);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return new CharSubSequence(this, start, end);
    }

    @Override
    public String toString() {
        return this.toString(0, this.length());
    }

    public static abstract class StringLike
    extends AbstractCharSequence {
        public int hashCode() {
            return CharSequenceUtilities.stringLikeHashCode(this);
        }

        public boolean equals(Object o) {
            return CharSequenceUtilities.equals(this, o);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return new CharSubSequence.StringLike(this, start, end);
        }
    }

}

