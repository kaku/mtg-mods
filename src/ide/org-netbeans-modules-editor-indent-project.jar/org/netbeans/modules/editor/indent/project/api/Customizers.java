/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer$CompositeCategoryProvider
 */
package org.netbeans.modules.editor.indent.project.api;

import java.util.Map;
import org.netbeans.modules.editor.indent.project.FormattingCustomizerPanel;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;

public final class Customizers {
    private Customizers() {
    }

    public static ProjectCustomizer.CompositeCategoryProvider createFormattingCategoryProvider(Map attrs) {
        return new FormattingCustomizerPanel.Factory((String)attrs.get("allowedMimeTypes"));
    }
}

