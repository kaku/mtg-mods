/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.project.FileOwnerQuery
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectManager
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences$Provider
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.indent.project;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.project.ProxyPreferences;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class ProjectAwareCodeStylePreferences
implements CodeStylePreferences.Provider {
    private static final Logger LOG = Logger.getLogger(ProjectAwareCodeStylePreferences.class.getName());
    private static final RequestProcessor RP = new RequestProcessor("Project Aware Code Style Preferences", 1, false, false);
    private static final CodeStylePreferences.Provider singleton = new CodeStylePreferences.Provider(){
        private final Map<Object, Reference<Map<String, Csp>>> cache = new WeakHashMap<Object, Reference<Map<String, Csp>>>();

        public Preferences forFile(FileObject file, String mimeType) {
            return this.getCsp((Object)file, mimeType).getPreferences();
        }

        public Preferences forDocument(Document doc, String mimeType) {
            return this.getCsp(doc, mimeType).getPreferences();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Csp getCsp(Object obj, String mimeType) {
            Csp csp;
            FileObject file = null;
            Map<Object, Reference<Map<String, Csp>>> map = this.cache;
            synchronized (map) {
                Reference<Map<String, Csp>> cspsRef = this.cache.get(obj);
                Map<String, Csp> csps = cspsRef != null ? cspsRef.get() : null;
                Csp csp2 = csp = csps != null ? csps.get(mimeType) : null;
                if (csp == null) {
                    Document doc;
                    if (obj instanceof FileObject) {
                        doc = null;
                        file = (FileObject)obj;
                    } else {
                        doc = (Document)obj;
                        file = ProjectAwareCodeStylePreferences.findFileObject(doc);
                    }
                    csp = new Csp(mimeType, doc == null ? null : new CleaningWeakReference(doc), file);
                    if (csps == null) {
                        csps = new HashMap<String, Csp>();
                        this.cache.put(obj, new SoftReference<Map<String, Csp>>(csps));
                    }
                    csps.put(mimeType, csp);
                }
            }
            if (file != null) {
                if (SwingUtilities.isEventDispatchThread()) {
                    final FileObject ffo = file;
                    final Csp fcsp = csp;
                    RP.post(new Runnable(){

                        @Override
                        public void run() {
                            fcsp.initProjectPreferences(ffo);
                        }
                    });
                } else {
                    csp.initProjectPreferences(file);
                }
            }
            return csp;
        }

        final class CleaningWeakReference
        extends WeakReference<Document>
        implements Runnable {
            public CleaningWeakReference(Document referent) {
                super(referent, Utilities.activeReferenceQueue());
            }

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Map map = 1.this.cache;
                synchronized (map) {
                    1.this.cache.size();
                }
            }
        }

    };

    public Preferences forFile(FileObject file, String mimeType) {
        return singleton.forFile(file, mimeType);
    }

    public Preferences forDocument(Document doc, String mimeType) {
        return singleton.forDocument(doc, mimeType);
    }

    private static Preferences findProjectPreferences(FileObject file) {
        Project p;
        if (file != null && (p = FileOwnerQuery.getOwner((FileObject)file)) != null) {
            return ProjectUtils.getPreferences((Project)p, IndentUtils.class, (boolean)true);
        }
        return null;
    }

    private static FileObject findFileObject(Document doc) {
        if (doc != null) {
            Object sdp = doc.getProperty("stream");
            if (sdp instanceof DataObject) {
                return ((DataObject)sdp).getPrimaryFile();
            }
            if (sdp instanceof FileObject) {
                return (FileObject)sdp;
            }
        }
        return null;
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    private static final class Csp {
        private static final String NODE_CODE_STYLE = "CodeStyle";
        private static final String PROP_USED_PROFILE = "usedProfile";
        private static final String DEFAULT_PROFILE = "default";
        private static final String PROJECT_PROFILE = "project";
        private final String mimeType;
        private final Reference<Document> refDoc;
        private final String filePath;
        private final Preferences globalPrefs;
        private Preferences projectPrefs;
        private boolean useProject;
        private final PreferenceChangeListener switchTrakcer;

        public Csp(String mimeType, Reference<Document> refDoc, FileObject file) {
            this.switchTrakcer = new PreferenceChangeListener(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void preferenceChange(PreferenceChangeEvent evt) {
                    if (evt.getKey() == null || "usedProfile".equals(evt.getKey())) {
                        Csp csp = Csp.this;
                        synchronized (csp) {
                            Csp.this.useProject = "project".equals(evt.getNewValue());
                            LOG.fine("file '" + Csp.this.filePath + "' (" + Csp.this.mimeType + ") is using " + (Csp.this.useProject ? "project" : "global") + " Preferences");
                        }
                    }
                }
            };
            this.mimeType = mimeType;
            this.refDoc = refDoc;
            this.filePath = file == null ? "no file" : file.getPath();
            this.globalPrefs = (Preferences)MimeLookup.getLookup((MimePath)(mimeType == null ? MimePath.EMPTY : MimePath.parse((String)mimeType))).lookup(Preferences.class);
            this.projectPrefs = null;
            this.useProject = false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Preferences getPreferences() {
            Csp csp = this;
            synchronized (csp) {
                Preferences prefs = this.useProject ? this.projectPrefs : this.globalPrefs;
                return prefs == null ? AbstractPreferences.systemRoot() : prefs;
            }
        }

        private void initProjectPreferences(final FileObject file) {
            ProjectManager.mutex().postReadRequest(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Preferences projectRoot = ProjectAwareCodeStylePreferences.findProjectPreferences(file);
                    if (projectRoot != null) {
                        Csp csp = Csp.this;
                        synchronized (csp) {
                            Preferences allLangCodeStyle = projectRoot.node("CodeStyle");
                            Preferences p = allLangCodeStyle.node("project");
                            String usedProfile = allLangCodeStyle.get("usedProfile", "default");
                            Csp.this.useProject = "project".equals(usedProfile);
                            Csp.this.projectPrefs = Csp.this.mimeType == null ? p : new ProxyPreferences(projectRoot.node(Csp.this.mimeType).node("CodeStyle").node("project"), p);
                            allLangCodeStyle.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)Csp.this.switchTrakcer, (Object)allLangCodeStyle));
                        }
                    }
                }
            });
            LOG.fine("file '" + this.filePath + "' (" + this.mimeType + ") is using " + (this.useProject ? "project" : "global") + " Preferences; doc=" + ProjectAwareCodeStylePreferences.s2s(this.refDoc == null ? null : this.refDoc.get()));
        }

    }

}

