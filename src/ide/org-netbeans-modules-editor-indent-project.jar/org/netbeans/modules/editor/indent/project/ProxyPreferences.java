/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.indent.project;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.openide.util.WeakListeners;

public final class ProxyPreferences
extends AbstractPreferences {
    private static final Logger LOG = Logger.getLogger(ProxyPreferences.class.getName());
    private static final String[] EMPTY = new String[0];
    private final Preferences[] delegates;
    private final Preferences[] roots;
    private final String[] paths;
    private final PreferenceChangeListener[] prefTrackers;

    public /* varargs */ ProxyPreferences(Preferences ... delegates) {
        this("", null, delegates);
    }

    @Override
    protected void putSpi(String key, String value) {
        this.checkDelegates();
        for (int i = 0; i < this.delegates.length; ++i) {
            if (this.delegates[i] == null) continue;
            this.delegates[i].put(key, value);
            return;
        }
    }

    @Override
    protected String getSpi(String key) {
        this.checkDelegates();
        for (int i = 0; i < this.delegates.length; ++i) {
            if (this.delegates[i] == null) continue;
            try {
                String value = this.delegates[i].get(key, null);
                if (value == null) continue;
                return value;
            }
            catch (Exception e) {
                this.delegates[i] = null;
            }
        }
        return null;
    }

    @Override
    protected void removeSpi(String key) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void removeNodeSpi() throws BackingStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected String[] keysSpi() throws BackingStoreException {
        HashSet<String> keys = new HashSet<String>();
        this.checkDelegates();
        for (int i = 0; i < this.delegates.length; ++i) {
            if (this.delegates[i] == null) continue;
            try {
                keys.addAll(Arrays.asList(this.delegates[i].keys()));
                continue;
            }
            catch (Exception e) {
                this.delegates[i] = null;
            }
        }
        return keys.toArray(new String[keys.size()]);
    }

    @Override
    protected String[] childrenNamesSpi() throws BackingStoreException {
        return EMPTY;
    }

    @Override
    protected AbstractPreferences childSpi(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void syncSpi() throws BackingStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void flushSpi() throws BackingStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private /* varargs */ ProxyPreferences(String name, ProxyPreferences parent, Preferences ... delegates) {
        super(parent, name);
        assert (delegates.length > 0);
        this.delegates = delegates;
        this.roots = new Preferences[delegates.length];
        this.paths = new String[delegates.length];
        this.prefTrackers = new PreferenceChangeListener[delegates.length];
        for (int i = 0; i < delegates.length; ++i) {
            this.roots[i] = delegates[i].node("/");
            this.paths[i] = delegates[i].absolutePath();
            this.prefTrackers[i] = new PrefTracker(i);
            delegates[i].addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefTrackers[i], (Object)delegates[i]));
        }
    }

    private void checkDelegates() {
        for (int i = 0; i < this.delegates.length; ++i) {
            if (this.delegates[i] != null) {
                try {
                    if (this.delegates[i].nodeExists("")) {
                        continue;
                    }
                }
                catch (BackingStoreException bse) {
                    // empty catch block
                }
                this.delegates[i] = null;
            }
            assert (this.delegates[i] == null);
            try {
                if (!this.roots[i].nodeExists(this.paths[i])) continue;
                this.delegates[i] = this.roots[i].node(this.paths[i]);
                continue;
            }
            catch (BackingStoreException bse) {
                // empty catch block
            }
        }
    }

    private void firePrefChange(String key, String newValue) {
        try {
            Method m = AbstractPreferences.class.getDeclaredMethod("enqueuePreferenceChangeEvent", String.class, String.class);
            m.setAccessible(true);
            m.invoke(this, key, newValue);
        }
        catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
    }

    private class PrefTracker
    implements PreferenceChangeListener {
        private final int delegateIdx;

        public PrefTracker(int idx) {
            this.delegateIdx = idx;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            Object object = ProxyPreferences.this.lock;
            synchronized (object) {
                if (evt.getKey() != null) {
                    ProxyPreferences.this.checkDelegates();
                    for (int i = 0; i < this.delegateIdx; ++i) {
                        if (ProxyPreferences.this.delegates[i] == null || ProxyPreferences.this.delegates[i].get(evt.getKey(), null) == null) continue;
                        return;
                    }
                }
            }
            ProxyPreferences.this.firePrefChange(evt.getKey(), evt.getNewValue());
        }
    }

}

