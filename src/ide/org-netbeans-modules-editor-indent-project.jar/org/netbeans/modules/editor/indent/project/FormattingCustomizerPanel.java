/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.netbeans.api.project.Project
 *  org.netbeans.api.project.ProjectManager
 *  org.netbeans.api.project.ProjectUtils
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage
 *  org.netbeans.modules.editor.settings.storage.spi.TypedValue
 *  org.netbeans.modules.options.editor.spi.PreferencesCustomizer
 *  org.netbeans.modules.options.indentation.CustomizerSelector
 *  org.netbeans.modules.options.indentation.CustomizerSelector$PreferencesFactory
 *  org.netbeans.modules.options.indentation.FormattingPanel
 *  org.netbeans.modules.options.indentation.ProxyPreferences
 *  org.netbeans.spi.project.ui.support.ProjectChooser
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer$Category
 *  org.netbeans.spi.project.ui.support.ProjectCustomizer$CompositeCategoryProvider
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$ExceptionAction
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.indent.project;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.project.api.Customizers;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage;
import org.netbeans.modules.editor.settings.storage.spi.TypedValue;
import org.netbeans.modules.options.editor.spi.PreferencesCustomizer;
import org.netbeans.modules.options.indentation.CustomizerSelector;
import org.netbeans.modules.options.indentation.FormattingPanel;
import org.netbeans.modules.options.indentation.ProxyPreferences;
import org.netbeans.spi.project.ui.support.ProjectChooser;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

public final class FormattingCustomizerPanel
extends JPanel
implements ActionListener,
HelpCtx.Provider {
    private JPanel customizerPanel;
    private JButton editGlobalButton;
    private JRadioButton globalButton;
    private ButtonGroup group;
    private JScrollPane jScrollPane1;
    private JButton loadButton;
    private JRadioButton projectButton;
    private static final Logger LOG = Logger.getLogger(FormattingCustomizerPanel.class.getName());
    private static final String GLOBAL_OPTIONS_CATEGORY = "Editor/Formatting";
    private static final String CODE_STYLE_PROFILE = "CodeStyle";
    private static final String DEFAULT_PROFILE = "default";
    private static final String PROJECT_PROFILE = "project";
    private static final String USED_PROFILE = "usedProfile";
    private final String allowedMimeTypes;
    private ProjectPreferencesFactory pf;
    private CustomizerSelector selector;
    private final FormattingPanel panel;
    private boolean copyOnFork;

    @Deprecated
    public static ProjectCustomizer.CompositeCategoryProvider createCategoryProvider(Map attrs) {
        return Customizers.createFormattingCategoryProvider(attrs);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                JTextComponent lastFocused;
                if ("default".equals(FormattingCustomizerPanel.this.pf.getPreferences("").parent().get("usedProfile", "default"))) {
                    Preferences p = ProjectUtils.getPreferences((Project)FormattingCustomizerPanel.this.pf.getProject(), IndentUtils.class, (boolean)true);
                    try {
                        FormattingCustomizerPanel.removeAllKidsAndKeys(p);
                    }
                    catch (BackingStoreException bse) {
                        LOG.log(Level.WARNING, null, bse);
                    }
                } else {
                    FormattingCustomizerPanel.this.pf.applyChanges();
                    HashSet mimeTypes = new HashSet(EditorSettings.getDefault().getAllMimeTypes());
                    mimeTypes.removeAll(FormattingCustomizerPanel.this.selector.getMimeTypes());
                    Preferences p = ProjectUtils.getPreferences((Project)FormattingCustomizerPanel.this.pf.getProject(), IndentUtils.class, (boolean)true);
                    for (String mimeType : mimeTypes) {
                        try {
                            p.node(mimeType).removeNode();
                        }
                        catch (BackingStoreException bse) {
                            LOG.log(Level.WARNING, null, bse);
                        }
                    }
                }
                if ((lastFocused = EditorRegistry.lastFocusedComponent()) != null) {
                    lastFocused.getDocument().putProperty("text-line-wrap", "");
                    lastFocused.getDocument().putProperty("tab-size", "");
                    lastFocused.getDocument().putProperty("text-limit-width", "");
                }
                for (JTextComponent jtc : EditorRegistry.componentList()) {
                    if (lastFocused != null && lastFocused == jtc) continue;
                    jtc.getDocument().putProperty("text-line-wrap", "");
                    jtc.getDocument().putProperty("tab-size", "");
                    jtc.getDocument().putProperty("text-limit-width", "");
                }
            }
        });
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx(FormattingCustomizerPanel.class);
    }

    private void initComponents() {
        this.group = new ButtonGroup();
        this.globalButton = new JRadioButton();
        this.editGlobalButton = new JButton();
        this.projectButton = new JRadioButton();
        this.loadButton = new JButton();
        this.jScrollPane1 = new JScrollPane();
        this.customizerPanel = new JPanel();
        this.group.add(this.globalButton);
        Mnemonics.setLocalizedText((AbstractButton)this.globalButton, (String)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"LBL_FormattingCustomizer_Global"));
        this.globalButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FormattingCustomizerPanel.this.globalButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.editGlobalButton, (String)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"LBL_FormattingCustomizer_EditGlobal"));
        this.editGlobalButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FormattingCustomizerPanel.this.editGlobalButtonActionPerformed(evt);
            }
        });
        this.group.add(this.projectButton);
        Mnemonics.setLocalizedText((AbstractButton)this.projectButton, (String)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"LBL_FormattingCustomizer_Project"));
        this.projectButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FormattingCustomizerPanel.this.projectButtonActionPerformed(evt);
            }
        });
        Mnemonics.setLocalizedText((AbstractButton)this.loadButton, (String)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"LBL_ForamttingCustomizer_Load"));
        this.loadButton.setHorizontalAlignment(4);
        this.loadButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FormattingCustomizerPanel.this.loadButtonActionPerformed(evt);
            }
        });
        this.jScrollPane1.setBorder(null);
        this.customizerPanel.setLayout(new BorderLayout());
        this.jScrollPane1.setViewportView(this.customizerPanel);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.projectButton, -1, 373, 32767).addComponent(this.globalButton, -1, 339, 32767)).addGap(14, 14, 14).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.editGlobalButton, -1, -1, 32767).addComponent(this.loadButton, -1, -1, 32767))).addComponent(this.jScrollPane1));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.globalButton).addComponent(this.editGlobalButton)).addGap(8, 8, 8).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.projectButton).addComponent(this.loadButton)).addGap(12, 12, 12).addComponent(this.jScrollPane1, -1, 239, 32767)));
        this.globalButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"FormattingCustomizerPanel.globalButton.AccessibleContext.accessibleDescription"));
        this.editGlobalButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"FormattingCustomizerPanel.editGlobalButton.AccessibleContext.accessibleDescription"));
        this.projectButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"FormattingCustomizerPanel.projectButton.AccessibleContext.accessibleDescription"));
        this.loadButton.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"FormattingCustomizerPanel.loadButton.AccessibleContext.accessibleDescription"));
    }

    private void globalButtonActionPerformed(ActionEvent evt) {
        NotifyDescriptor.Confirmation d = new NotifyDescriptor.Confirmation((Object)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"MSG_use_global_settings_confirmation"), NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"MSG_use_global_settings_confirmation_title"), 2);
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)d) == NotifyDescriptor.OK_OPTION) {
            this.pf.getPreferences("").parent().put("usedProfile", "default");
            this.loadButton.setEnabled(false);
            this.setEnabled(this.jScrollPane1, false);
        } else {
            this.projectButton.setSelected(true);
        }
    }

    private void projectButtonActionPerformed(ActionEvent evt) {
        this.pf.getPreferences("").parent().put("usedProfile", "project");
        this.loadButton.setEnabled(true);
        this.setEnabled(this.jScrollPane1, true);
        if (this.copyOnFork) {
            this.copyOnFork = false;
            EditorSettingsStorage storage = EditorSettingsStorage.get((String)"Preferences");
            for (String mimeType : this.selector.getMimeTypes()) {
                Map mimePathLocalPrefs;
                try {
                    mimePathLocalPrefs = storage.load(MimePath.parse((String)mimeType), null, false);
                }
                catch (IOException ioe) {
                    LOG.log(Level.WARNING, null, ioe);
                    continue;
                }
                Preferences projectPrefs = this.pf.getPreferences(mimeType);
                boolean copied = false;
                copied |= FormattingCustomizerPanel.copyValueIfExists(mimePathLocalPrefs, projectPrefs, "expand-tabs");
                copied |= FormattingCustomizerPanel.copyValueIfExists(mimePathLocalPrefs, projectPrefs, "indent-shift-width");
                copied |= FormattingCustomizerPanel.copyValueIfExists(mimePathLocalPrefs, projectPrefs, "spaces-per-tab");
                copied |= FormattingCustomizerPanel.copyValueIfExists(mimePathLocalPrefs, projectPrefs, "tab-size");
                copied |= FormattingCustomizerPanel.copyValueIfExists(mimePathLocalPrefs, projectPrefs, "text-limit-width");
                if (mimeType.length() <= 0 || !(copied |= FormattingCustomizerPanel.copyValueIfExists(mimePathLocalPrefs, projectPrefs, "text-line-wrap"))) continue;
                projectPrefs.putBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", true);
            }
        }
    }

    private void loadButtonActionPerformed(ActionEvent evt) {
        File f;
        FileObject fo;
        JFileChooser chooser = ProjectChooser.projectChooser();
        if (chooser.showOpenDialog(this) == 0 && (fo = FileUtil.toFileObject((File)(f = FileUtil.normalizeFile((File)chooser.getSelectedFile())))) != null) {
            Object ret;
            try {
                final Project prjFrom = ProjectManager.getDefault().findProject(fo);
                if (prjFrom == this.pf.getProject()) {
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"MSG_CodeStyle_Import_Forbidden_From_The_Same_Project"), -1));
                    return;
                }
                ret = ProjectManager.mutex().readAccess((Mutex.ExceptionAction)new Mutex.ExceptionAction<Object>(){

                    public Object run() throws Exception {
                        Preferences fromPrjPrefs = ProjectUtils.getPreferences((Project)prjFrom, IndentUtils.class, (boolean)true);
                        if (!fromPrjPrefs.nodeExists("CodeStyle") || fromPrjPrefs.node("CodeStyle").get("usedProfile", null) == null) {
                            return NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"MSG_No_CodeStyle_Info_To_Import");
                        }
                        ProjectPreferencesFactory newPrefsFactory = new ProjectPreferencesFactory(FormattingCustomizerPanel.this.pf.getProject());
                        ProxyPreferences toPrjPrefs = newPrefsFactory.projectPrefs;
                        FormattingCustomizerPanel.removeAllKidsAndKeys((Preferences)toPrjPrefs);
                        FormattingCustomizerPanel.deepCopy(fromPrjPrefs, (Preferences)toPrjPrefs);
                        return newPrefsFactory;
                    }
                });
            }
            catch (Exception e) {
                LOG.log(Level.INFO, null, e);
                ret = e;
            }
            if (ret instanceof ProjectPreferencesFactory) {
                String selectedMimeType = this.selector.getSelectedMimeType();
                PreferencesCustomizer c = this.selector.getSelectedCustomizer();
                String selectedCustomizerId = c != null ? c.getId() : null;
                this.pf.destroy();
                this.pf = (ProjectPreferencesFactory)ret;
                this.selector = new CustomizerSelector((CustomizerSelector.PreferencesFactory)this.pf, false, this.allowedMimeTypes);
                this.panel.setSelector(this.selector);
                if (selectedMimeType != null) {
                    this.selector.setSelectedMimeType(selectedMimeType);
                }
                if (selectedCustomizerId != null) {
                    this.selector.setSelectedCustomizer(selectedCustomizerId);
                }
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"MSG_CodeStyle_Import_Successful"), -1));
            } else if (ret instanceof Exception) {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(FormattingCustomizerPanel.class, (String)"MSG_CodeStyle_Import_Failed"), 2));
            } else {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)((String)ret), -1));
            }
        }
    }

    private void dump(Preferences prefs, String prefsId) throws BackingStoreException {
        for (String key : prefs.keys()) {
            System.out.println(prefsId + ", " + prefs.absolutePath() + "/" + key + "=" + prefs.get(key, null));
        }
        for (String child : prefs.childrenNames()) {
            this.dump(prefs.node(child), prefsId);
        }
    }

    private void editGlobalButtonActionPerformed(ActionEvent evt) {
        OptionsDisplayer.getDefault().open("Editor/Formatting");
    }

    private FormattingCustomizerPanel(Lookup context, String allowedMimeTypes) {
        this.allowedMimeTypes = allowedMimeTypes;
        this.pf = new ProjectPreferencesFactory((Project)context.lookup(Project.class));
        this.selector = new CustomizerSelector((CustomizerSelector.PreferencesFactory)this.pf, false, allowedMimeTypes);
        this.panel = new FormattingPanel();
        this.panel.setSelector(this.selector);
        this.initComponents();
        this.customizerPanel.add((Component)this.panel, "Center");
        Preferences prefs = this.pf.getPreferences("").parent();
        this.copyOnFork = prefs.get("usedProfile", null) == null;
        String profile = prefs.get("usedProfile", "default");
        if ("default".equals(profile)) {
            this.globalButton.setSelected(true);
            this.loadButton.setEnabled(false);
            this.setEnabled(this.jScrollPane1, false);
        } else {
            this.projectButton.setSelected(true);
            this.loadButton.setEnabled(true);
            this.setEnabled(this.jScrollPane1, true);
        }
    }

    private void setEnabled(Component component, boolean enabled) {
        component.setEnabled(enabled);
        if (component instanceof Container && !(component instanceof JSpinner)) {
            for (Component c : ((Container)component).getComponents()) {
                this.setEnabled(c, enabled);
            }
        }
    }

    private static boolean copyValueIfExists(Map<String, TypedValue> src, Preferences trg, String key) {
        TypedValue value = src.get(key);
        if (value != null) {
            trg.put(key, value.getValue());
            return true;
        }
        return false;
    }

    private static void removeAllKidsAndKeys(Preferences prefs) throws BackingStoreException {
        for (String kid : prefs.childrenNames()) {
            FormattingCustomizerPanel.removeAllKidsAndKeys(prefs.node(kid));
        }
        for (String key : prefs.keys()) {
            prefs.remove(key);
        }
    }

    private static void deepCopy(Preferences from, Preferences to) throws BackingStoreException {
        for (String kid : from.childrenNames()) {
            Preferences fromKid = from.node(kid);
            Preferences toKid = to.node(kid);
            FormattingCustomizerPanel.deepCopy(fromKid, toKid);
        }
        for (String key : from.keys()) {
            String value = from.get(key, null);
            if (value == null) continue;
            Class type = FormattingCustomizerPanel.guessType(value);
            if (Integer.class == type) {
                to.putInt(key, from.getInt(key, -1));
                continue;
            }
            if (Long.class == type) {
                to.putLong(key, from.getLong(key, -1));
                continue;
            }
            if (Float.class == type) {
                to.putFloat(key, from.getFloat(key, -1.0f));
                continue;
            }
            if (Double.class == type) {
                to.putDouble(key, from.getDouble(key, -1.0));
                continue;
            }
            if (Boolean.class == type) {
                to.putBoolean(key, from.getBoolean(key, false));
                continue;
            }
            if (String.class == type) {
                to.put(key, value);
                continue;
            }
            to.putByteArray(key, from.getByteArray(key, new byte[0]));
        }
    }

    private static Class guessType(String value) {
        if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
            return Boolean.class;
        }
        try {
            Integer.parseInt(value);
            return Integer.class;
        }
        catch (NumberFormatException nfe) {
            try {
                Long.parseLong(value);
                return Long.class;
            }
            catch (NumberFormatException nfe) {
                try {
                    Float.parseFloat(value);
                    return Float.class;
                }
                catch (NumberFormatException nfe) {
                    try {
                        Double.parseDouble(value);
                        return Double.class;
                    }
                    catch (NumberFormatException nfe) {
                        return String.class;
                    }
                }
            }
        }
    }

    private static final class ProjectPreferencesFactory
    implements CustomizerSelector.PreferencesFactory {
        private final Project project;
        private final Set<String> accessedMimeTypes = new HashSet<String>();
        private ProxyPreferences projectPrefs;

        public ProjectPreferencesFactory(Project project) {
            this.project = project;
            Preferences p = ProjectUtils.getPreferences((Project)project, IndentUtils.class, (boolean)true);
            this.projectPrefs = ProxyPreferences.getProxyPreferences((Object)this, (Preferences)p);
        }

        public Project getProject() {
            return this.project;
        }

        public void destroy() {
            this.accessedMimeTypes.clear();
            this.projectPrefs.destroy();
            this.projectPrefs = null;
        }

        public synchronized Preferences getPreferences(String mimeType) {
            assert (this.projectPrefs != null);
            this.accessedMimeTypes.add(mimeType);
            return this.projectPrefs.node(mimeType).node("CodeStyle").node("project");
        }

        public synchronized void applyChanges() {
            for (String mimeType : this.accessedMimeTypes) {
                ProxyPreferences pp;
                if (mimeType.length() == 0 || null == (pp = (ProxyPreferences)this.getPreferences(mimeType)).get("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", null)) continue;
                pp.silence();
                if (!pp.getBoolean("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS", false)) {
                    pp.remove("expand-tabs");
                    pp.remove("indent-shift-width");
                    pp.remove("spaces-per-tab");
                    pp.remove("tab-size");
                    pp.remove("text-limit-width");
                    pp.remove("text-line-wrap");
                }
                pp.remove("FormattingPanelController.OVERRIDE_GLOBAL_FORMATTING_OPTIONS");
            }
            this.projectPrefs.silence();
            try {
                LOG.fine("Flushing root pp");
                this.projectPrefs.flush();
            }
            catch (BackingStoreException ex) {
                LOG.log(Level.WARNING, "Can't flush project codestyle root preferences", ex);
            }
            this.destroy();
        }

        public boolean isKeyOverridenForMimeType(String key, String mimeType) {
            Object p = this.projectPrefs != null ? this.projectPrefs : ProjectUtils.getPreferences((Project)this.project, IndentUtils.class, (boolean)true);
            p = p.node(mimeType).node("CodeStyle").node("project");
            return p.get(key, null) != null;
        }
    }

    public static class Factory
    implements ProjectCustomizer.CompositeCategoryProvider {
        private static final String CATEGORY_FORMATTING = "Formatting";
        private final String allowedMimeTypes;

        public Factory() {
            this(null);
        }

        public Factory(String allowedMimeTypes) {
            this.allowedMimeTypes = allowedMimeTypes;
        }

        public ProjectCustomizer.Category createCategory(Lookup context) {
            return context.lookup(Project.class) == null ? null : ProjectCustomizer.Category.create((String)"Formatting", (String)NbBundle.getMessage(Factory.class, (String)"LBL_CategoryFormatting"), (Image)null, (ProjectCustomizer.Category[])new ProjectCustomizer.Category[0]);
        }

        public JComponent createComponent(ProjectCustomizer.Category category, Lookup context) {
            FormattingCustomizerPanel customizerPanel = new FormattingCustomizerPanel(context, this.allowedMimeTypes);
            category.setStoreListener((ActionListener)customizerPanel);
            return customizerPanel;
        }
    }

}

