/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorUtilities
 *  org.netbeans.spi.editor.AbstractEditorAction
 */
package org.netbeans.modules.editor.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.plaf.TextUI;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorUtilities;
import org.netbeans.spi.editor.AbstractEditorAction;

public class MoveCodeElementAction
extends AbstractEditorAction {
    public void actionPerformed(ActionEvent evt, JTextComponent component) {
        if (component != null) {
            String actionName = "move-code-element-up".equals(this.actionName()) ? "move-selection-else-line-up" : "move-selection-else-line-down";
            Action action = EditorUtilities.getAction((EditorKit)component.getUI().getEditorKit(component), (String)actionName);
            if (action != null) {
                action.actionPerformed(evt);
                return;
            }
        }
        Toolkit.getDefaultToolkit().beep();
    }
}

