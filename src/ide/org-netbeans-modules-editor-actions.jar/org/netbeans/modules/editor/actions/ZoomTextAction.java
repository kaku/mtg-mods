/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.AbstractEditorAction
 */
package org.netbeans.modules.editor.actions;

import java.awt.event.ActionEvent;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.AbstractEditorAction;

public class ZoomTextAction
extends AbstractEditorAction {
    private static final long serialVersionUID = 1;
    private static final String TEXT_ZOOM_PROPERTY = "text-zoom";

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        int delta;
        String actionName = this.actionName();
        int n = delta = "zoom-text-in".equals(actionName) ? 1 : -1;
        if (target != null) {
            int newZoom = 0;
            Integer currentZoom = (Integer)target.getClientProperty("text-zoom");
            if (currentZoom != null) {
                newZoom += currentZoom.intValue();
            }
            target.putClientProperty("text-zoom", newZoom += delta);
        }
    }
}

