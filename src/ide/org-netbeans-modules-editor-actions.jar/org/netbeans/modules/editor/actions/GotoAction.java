/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.JumpList
 *  org.netbeans.editor.SyntaxSupport
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.ext.ExtSyntaxSupport
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt
 *  org.netbeans.lib.editor.hyperlink.spi.HyperlinkType
 *  org.netbeans.spi.editor.AbstractEditorAction
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.actions;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.spi.editor.AbstractEditorAction;
import org.openide.util.Lookup;

public final class GotoAction
extends AbstractEditorAction {
    private static final Logger LOG = Logger.getLogger(GotoAction.class.getName());
    private static final long serialVersionUID = 1;

    public void actionPerformed(ActionEvent evt, JTextComponent target) {
        String actionName = this.actionName();
        if ("goto-declaration".equals(actionName)) {
            this.resetCaretMagicPosition(target);
            if (target != null) {
                if (this.hyperlinkGoTo(target)) {
                    return;
                }
                BaseDocument doc = Utilities.getDocument((JTextComponent)target);
                if (doc != null) {
                    try {
                        int decPos;
                        Caret caret = target.getCaret();
                        int dotPos = caret.getDot();
                        int[] idBlk = Utilities.getIdentifierBlock((BaseDocument)doc, (int)dotPos);
                        ExtSyntaxSupport extSup = (ExtSyntaxSupport)doc.getSyntaxSupport();
                        if (idBlk != null && (decPos = extSup.findDeclarationPosition(doc.getText(idBlk), idBlk[1])) >= 0) {
                            caret.setDot(decPos);
                        }
                    }
                    catch (BadLocationException e) {
                        // empty catch block
                    }
                }
            }
        }
    }

    private boolean hyperlinkGoTo(JTextComponent component) {
        Document doc = component.getDocument();
        Object mimeTypeObj = doc.getProperty("mimeType");
        if (!(mimeTypeObj instanceof String)) {
            return false;
        }
        String mimeType = (String)mimeTypeObj;
        int position = component.getCaretPosition();
        Lookup lookup = MimeLookup.getLookup((String)mimeType);
        for (HyperlinkProviderExt provider2 : lookup.lookupAll(HyperlinkProviderExt.class)) {
            if (!provider2.getSupportedHyperlinkTypes().contains((Object)HyperlinkType.GO_TO_DECLARATION) || !provider2.isHyperlinkPoint(doc, position, HyperlinkType.GO_TO_DECLARATION)) continue;
            JumpList.checkAddEntry((JTextComponent)component, (int)position);
            provider2.performClickAction(doc, position, HyperlinkType.GO_TO_DECLARATION);
            return true;
        }
        for (HyperlinkProviderExt provider2 : lookup.lookupAll(HyperlinkProvider.class)) {
            if (!provider2.isHyperlinkPoint(doc, position)) continue;
            JumpList.checkAddEntry((JTextComponent)component, (int)position);
            provider2.performClickAction(doc, position);
            return true;
        }
        return false;
    }
}

