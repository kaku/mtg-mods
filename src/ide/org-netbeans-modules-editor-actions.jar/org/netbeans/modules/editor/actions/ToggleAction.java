/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.AbstractEditorAction
 */
package org.netbeans.modules.editor.actions;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.JTextComponent;
import org.netbeans.spi.editor.AbstractEditorAction;

public final class ToggleAction
extends AbstractEditorAction {
    private static final Logger LOG = Logger.getLogger(ToggleAction.class.getName());
    private static final long serialVersionUID = 1;

    public void actionPerformed(ActionEvent evt, JTextComponent component) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("actionPerformed: actionName=" + this.actionName());
        }
    }
}

