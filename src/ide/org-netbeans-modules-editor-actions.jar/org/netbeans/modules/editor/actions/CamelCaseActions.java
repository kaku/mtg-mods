/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseCaret
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.RectangularSelectionUtils
 *  org.netbeans.modules.editor.lib2.typinghooks.CamelCaseInterceptorsManager
 *  org.netbeans.modules.editor.lib2.typinghooks.CamelCaseInterceptorsManager$Transaction
 *  org.netbeans.spi.editor.AbstractEditorAction
 */
package org.netbeans.modules.editor.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseCaret;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.RectangularSelectionUtils;
import org.netbeans.modules.editor.lib2.typinghooks.CamelCaseInterceptorsManager;
import org.netbeans.spi.editor.AbstractEditorAction;

public class CamelCaseActions {
    static final String deleteNextCamelCasePosition = "delete-next-camel-case-position";
    static final String SYSTEM_ACTION_CLASS_NAME_PROPERTY = "systemActionClassName";

    public static class SelectPreviousCamelCasePosition
    extends CamelCaseAction {
        @Override
        protected boolean isForward() {
            return false;
        }

        @Override
        protected void moveToNewOffset(JTextComponent target, int offset, int length) throws BadLocationException {
            Caret caret = target.getCaret();
            if (caret instanceof BaseCaret && RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                ((BaseCaret)caret).extendRectangularSelection(false, true);
            } else {
                target.getCaret().moveDot(offset);
            }
        }
    }

    public static class SelectNextCamelCasePosition
    extends CamelCaseAction {
        @Override
        protected boolean isForward() {
            return true;
        }

        @Override
        protected void moveToNewOffset(JTextComponent target, int offset, int length) throws BadLocationException {
            Caret caret = target.getCaret();
            if (caret instanceof BaseCaret && RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                ((BaseCaret)caret).extendRectangularSelection(true, true);
            } else {
                target.getCaret().moveDot(offset + length);
            }
        }
    }

    public static class PreviousCamelCasePosition
    extends CamelCaseAction {
        @Override
        protected boolean isForward() {
            return false;
        }

        @Override
        protected void moveToNewOffset(JTextComponent target, int offset, int length) throws BadLocationException {
            target.setCaretPosition(offset);
        }
    }

    public static class NextCamelCasePosition
    extends CamelCaseAction {
        @Override
        protected boolean isForward() {
            return true;
        }

        @Override
        protected void moveToNewOffset(JTextComponent target, int offset, int length) throws BadLocationException {
            target.setCaretPosition(offset + length);
        }
    }

    public static class RemoveWordPreviousAction
    extends CamelCaseAction {
        @Override
        protected boolean isForward() {
            return false;
        }

        @Override
        protected void moveToNewOffset(JTextComponent target, int offset, int length) throws BadLocationException {
            target.getDocument().remove(offset, length);
        }

        @Override
        protected boolean doesTypingModification() {
            return true;
        }
    }

    public static class RemoveWordNextAction
    extends CamelCaseAction {
        @Override
        protected boolean isForward() {
            return true;
        }

        @Override
        protected void moveToNewOffset(JTextComponent target, int offset, int length) throws BadLocationException {
            target.getDocument().remove(offset, length);
        }

        @Override
        protected boolean doesTypingModification() {
            return true;
        }
    }

    public static abstract class CamelCaseAction
    extends AbstractEditorAction {
        public CamelCaseAction(Map<String, ?> attrs) {
            super(attrs);
        }

        public CamelCaseAction() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                final CamelCaseInterceptorsManager.Transaction t = CamelCaseInterceptorsManager.getInstance().openTransaction(target, caret.getDot(), !this.isForward());
                try {
                    if (!t.beforeChange()) {
                        final Boolean[] result = new Boolean[]{Boolean.FALSE};
                        doc.runAtomicAsUser(new Runnable(){

                            /*
                             * WARNING - Removed try catching itself - possible behaviour change.
                             */
                            @Override
                            public void run() {
                                if (CamelCaseAction.this.doesTypingModification()) {
                                    DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                                }
                                Object[] r = t.change();
                                try {
                                    int wsPos;
                                    int dotPos = caret.getDot();
                                    if (r == null) {
                                        if (CamelCaseAction.this.isForward()) {
                                            int eolPos = Utilities.getRowEnd((BaseDocument)doc, (int)dotPos);
                                            wsPos = Utilities.getNextWord((JTextComponent)target, (int)dotPos);
                                            wsPos = dotPos == eolPos ? wsPos : Math.min(eolPos, wsPos);
                                        } else {
                                            int bolPos = Utilities.getRowStart((BaseDocument)doc, (int)dotPos);
                                            wsPos = Utilities.getPreviousWord((JTextComponent)target, (int)dotPos);
                                            wsPos = dotPos == bolPos ? wsPos : Math.max(bolPos, wsPos);
                                        }
                                    } else {
                                        wsPos = (Integer)r[0];
                                    }
                                    if (CamelCaseAction.this.isForward()) {
                                        CamelCaseAction.this.moveToNewOffset(target, dotPos, wsPos - dotPos);
                                    } else {
                                        CamelCaseAction.this.moveToNewOffset(target, wsPos, dotPos - wsPos);
                                    }
                                    result[0] = Boolean.TRUE;
                                }
                                catch (BadLocationException e) {
                                    target.getToolkit().beep();
                                }
                                finally {
                                    if (CamelCaseAction.this.doesTypingModification()) {
                                        DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                                    }
                                }
                            }
                        });
                        if (result[0].booleanValue()) {
                            t.afterChange();
                        }
                    }
                }
                finally {
                    t.close();
                }
            }
        }

        protected abstract boolean isForward();

        protected boolean doesTypingModification() {
            return false;
        }

        protected abstract void moveToNewOffset(JTextComponent var1, int var2, int var3) throws BadLocationException;

    }

}

