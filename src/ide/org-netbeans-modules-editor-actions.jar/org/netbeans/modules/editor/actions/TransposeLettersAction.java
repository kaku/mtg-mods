/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseAction
 *  org.netbeans.modules.editor.lib2.DocUtils
 */
package org.netbeans.modules.editor.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.BaseAction;
import org.netbeans.modules.editor.lib2.DocUtils;

public class TransposeLettersAction
extends BaseAction {
    private static final long serialVersionUID = 1;

    public void actionPerformed(ActionEvent evt, final JTextComponent target) {
        if (target != null) {
            final Document doc = target.getDocument();
            DocUtils.runAtomicAsUser((Document)doc, (Runnable)new Runnable(){

                @Override
                public void run() {
                    if (!DocUtils.transposeLetters((Document)doc, (int)target.getCaretPosition())) {
                        target.getToolkit().beep();
                    }
                }
            });
        }
    }

}

