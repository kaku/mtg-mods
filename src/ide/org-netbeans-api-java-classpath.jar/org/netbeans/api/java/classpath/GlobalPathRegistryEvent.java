/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.classpath;

import java.util.EventObject;
import java.util.Set;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;

public final class GlobalPathRegistryEvent
extends EventObject {
    private final String id;
    private final Set<ClassPath> changed;

    GlobalPathRegistryEvent(GlobalPathRegistry r, String id, Set<ClassPath> changed) {
        super(r);
        assert (id != null);
        assert (changed != null && !changed.isEmpty());
        this.id = id;
        this.changed = changed;
    }

    public GlobalPathRegistry getRegistry() {
        return (GlobalPathRegistry)this.getSource();
    }

    public String getId() {
        return this.id;
    }

    public Set<ClassPath> getChangedPaths() {
        return this.changed;
    }
}

