/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.api.java.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistryEvent;
import org.netbeans.api.java.classpath.GlobalPathRegistryListener;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.openide.filesystems.FileObject;

public final class GlobalPathRegistry {
    private static final Logger LOG = Logger.getLogger(GlobalPathRegistry.class.getName());
    private static GlobalPathRegistry DEFAULT = new GlobalPathRegistry();
    private int resetCount;
    private final Map<String, List<ClassPath>> paths = new HashMap<String, List<ClassPath>>();
    private final List<GlobalPathRegistryListener> listeners = new ArrayList<GlobalPathRegistryListener>();
    private Set<FileObject> sourceRoots = null;
    private Set<SourceForBinaryQuery.Result> results = new HashSet<SourceForBinaryQuery.Result>();
    private final ChangeListener resultListener;
    private PropertyChangeListener classpathListener;

    public static GlobalPathRegistry getDefault() {
        return DEFAULT;
    }

    private GlobalPathRegistry() {
        this.resultListener = new SFBQListener();
        this.classpathListener = new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                GlobalPathRegistry globalPathRegistry = GlobalPathRegistry.this;
                synchronized (globalPathRegistry) {
                    GlobalPathRegistry.this.resetSourceRootsCache();
                }
            }
        };
    }

    void clear() {
        this.paths.clear();
        this.listeners.clear();
        this.sourceRoots = null;
    }

    public synchronized Set<ClassPath> getPaths(String id) {
        if (id == null) {
            throw new NullPointerException();
        }
        List<ClassPath> l = this.paths.get(id);
        if (l != null && !l.isEmpty()) {
            return Collections.unmodifiableSet(new HashSet<ClassPath>(l));
        }
        return Collections.emptySet();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void register(String id, ClassPath[] paths) {
        if (id == null || paths == null) {
            throw new NullPointerException();
        }
        LOG.log(Level.FINE, "registering paths {0} of type {1}", new Object[]{Arrays.asList(paths), id});
        GlobalPathRegistryEvent evt = null;
        GlobalPathRegistryListener[] _listeners = null;
        GlobalPathRegistry globalPathRegistry = this;
        synchronized (globalPathRegistry) {
            List<ClassPath> l = this.paths.get(id);
            if (l == null) {
                l = new ArrayList<ClassPath>();
                this.paths.put(id, l);
            }
            HashSet<ClassPath> added = this.listeners.isEmpty() ? null : new HashSet<ClassPath>();
            for (ClassPath path : paths) {
                if (path == null) {
                    throw new NullPointerException("Null path encountered in " + Arrays.asList(paths) + " of type " + id);
                }
                if (added != null && !added.contains(path) && !l.contains(path)) {
                    added.add(path);
                }
                if (!l.contains(path)) {
                    path.addPropertyChangeListener(this.classpathListener);
                }
                l.add(path);
            }
            LOG.log(Level.FINER, "now have {0} paths of type {1}", new Object[]{l.size(), id});
            if (added != null && !added.isEmpty()) {
                _listeners = this.listeners.toArray(new GlobalPathRegistryListener[this.listeners.size()]);
                evt = new GlobalPathRegistryEvent(this, id, Collections.unmodifiableSet(added));
            }
            this.resetSourceRootsCache();
        }
        if (_listeners != null) {
            assert (evt != null);
            for (GlobalPathRegistryListener listener : _listeners) {
                listener.pathsAdded(evt);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void unregister(String id, ClassPath[] paths) throws IllegalArgumentException {
        LOG.log(Level.FINE, "unregistering paths {0} of type {1}", new Object[]{Arrays.asList(paths), id});
        if (id == null || paths == null) {
            throw new NullPointerException();
        }
        GlobalPathRegistryEvent evt = null;
        GlobalPathRegistryListener[] _listeners = null;
        GlobalPathRegistry globalPathRegistry = this;
        synchronized (globalPathRegistry) {
            List<ClassPath> l = this.paths.get(id);
            if (l == null) {
                l = new ArrayList<ClassPath>();
            }
            ArrayList<ClassPath> l2 = new ArrayList<ClassPath>(l);
            HashSet<ClassPath> removed = this.listeners.isEmpty() ? null : new HashSet<ClassPath>();
            for (ClassPath path : paths) {
                if (path == null) {
                    throw new NullPointerException();
                }
                if (!l2.remove(path)) {
                    throw new IllegalArgumentException("Attempt to remove nonexistent path [" + path + "] from list of registered paths [" + l2 + "] for id " + id + ". All paths: " + this.paths);
                }
                if (removed != null && !removed.contains(path) && !l2.contains(path)) {
                    removed.add(path);
                }
                if (l2.contains(path)) continue;
                path.removePropertyChangeListener(this.classpathListener);
            }
            this.paths.put(id, l2);
            LOG.log(Level.FINER, "now have {0} paths of type {1}", new Object[]{l2.size(), id});
            if (removed != null && !removed.isEmpty()) {
                _listeners = this.listeners.toArray(new GlobalPathRegistryListener[this.listeners.size()]);
                evt = new GlobalPathRegistryEvent(this, id, Collections.unmodifiableSet(removed));
            }
            this.resetSourceRootsCache();
        }
        if (_listeners != null) {
            assert (evt != null);
            for (GlobalPathRegistryListener listener : _listeners) {
                listener.pathsRemoved(evt);
            }
        }
    }

    public synchronized void addGlobalPathRegistryListener(GlobalPathRegistryListener l) {
        if (l == null) {
            throw new NullPointerException();
        }
        this.listeners.add(l);
    }

    public synchronized void removeGlobalPathRegistryListener(GlobalPathRegistryListener l) {
        if (l == null) {
            throw new NullPointerException();
        }
        this.listeners.remove(l);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Set<FileObject> getSourceRoots() {
        LinkedHashSet<ClassPath> compileAndBootPaths;
        Set<ClassPath> sourcePaths;
        int currentResetCount;
        GlobalPathRegistry globalPathRegistry = this;
        synchronized (globalPathRegistry) {
            if (this.sourceRoots != null) {
                return this.sourceRoots;
            }
            currentResetCount = this.resetCount;
            sourcePaths = this.getPaths("classpath/source");
            compileAndBootPaths = new LinkedHashSet<ClassPath>(this.getPaths("classpath/compile"));
            compileAndBootPaths.addAll(this.getPaths("classpath/boot"));
        }
        LinkedHashSet<FileObject> newSourceRoots = new LinkedHashSet<FileObject>();
        for (ClassPath sp : sourcePaths) {
            newSourceRoots.addAll(Arrays.asList(sp.getRoots()));
        }
        LinkedList<SourceForBinaryQuery.Result> newResults = new LinkedList<SourceForBinaryQuery.Result>();
        SFBQListener tmpResultListener = new SFBQListener();
        for (ClassPath cp : compileAndBootPaths) {
            for (ClassPath.Entry entry : cp.entries()) {
                SourceForBinaryQuery.Result result = SourceForBinaryQuery.findSourceRoots(entry.getURL());
                result.addChangeListener(tmpResultListener);
                newResults.add(result);
                FileObject[] someRoots = result.getRoots();
                newSourceRoots.addAll(Arrays.asList(someRoots));
            }
        }
        newSourceRoots = Collections.unmodifiableSet(newSourceRoots);
        GlobalPathRegistry i$ = this;
        synchronized (i$) {
            if (this.resetCount == currentResetCount) {
                this.sourceRoots = newSourceRoots;
                this.removeTmpSFBQListeners(newResults, tmpResultListener, true);
                this.results.addAll(newResults);
            } else {
                this.removeTmpSFBQListeners(newResults, tmpResultListener, false);
            }
            return newSourceRoots;
        }
    }

    private void removeTmpSFBQListeners(List<? extends SourceForBinaryQuery.Result> results, ChangeListener listener, boolean addListener) {
        for (SourceForBinaryQuery.Result res : results) {
            if (addListener) {
                res.addChangeListener(this.resultListener);
            }
            res.removeChangeListener(listener);
        }
    }

    public FileObject findResource(String resource) {
        FileObject f;
        for (ClassPath cp : this.getPaths("classpath/source")) {
            f = cp.findResource(resource);
            if (f == null) continue;
            return f;
        }
        for (FileObject root : this.getSourceRoots()) {
            f = root.getFileObject(resource);
            if (f == null) continue;
            for (ClassPath cp2 : this.getPaths("classpath/source")) {
                if (cp2.findOwnerRoot(f) == null) continue;
                return null;
            }
            return f;
        }
        return null;
    }

    private synchronized void resetSourceRootsCache() {
        this.sourceRoots = null;
        Iterator<SourceForBinaryQuery.Result> it = this.results.iterator();
        while (it.hasNext()) {
            SourceForBinaryQuery.Result result = it.next();
            it.remove();
            result.removeChangeListener(this.resultListener);
        }
        ++this.resetCount;
    }

    Set<? extends SourceForBinaryQuery.Result> getResults() {
        return Collections.unmodifiableSet(this.results);
    }

    private class SFBQListener
    implements ChangeListener {
        private SFBQListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void stateChanged(ChangeEvent event) {
            GlobalPathRegistry globalPathRegistry = GlobalPathRegistry.this;
            synchronized (globalPathRegistry) {
                GlobalPathRegistry.this.resetSourceRootsCache();
            }
        }
    }

}

