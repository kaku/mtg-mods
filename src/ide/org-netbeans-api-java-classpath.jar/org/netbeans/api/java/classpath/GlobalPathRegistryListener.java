/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.java.classpath;

import java.util.EventListener;
import org.netbeans.api.java.classpath.GlobalPathRegistryEvent;

public interface GlobalPathRegistryListener
extends EventListener {
    public void pathsAdded(GlobalPathRegistryEvent var1);

    public void pathsRemoved(GlobalPathRegistryEvent var1);
}

