/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbClassLoader
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.WeakListeners
 *  org.openide.windows.InputOutput
 */
package org.netbeans.api.java.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.security.AllPermission;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.java.classpath.ClassPath;
import org.openide.execution.NbClassLoader;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.util.WeakListeners;
import org.openide.windows.InputOutput;

class ClassLoaderSupport
extends NbClassLoader
implements FileChangeListener,
PropertyChangeListener {
    private FileChangeListener listener;
    private PropertyChangeListener propListener;
    private final Object lock = new Object();
    private final Map<FileObject, Boolean> emittedFileObjects = new HashMap<FileObject, Boolean>();
    private boolean detachedFromCp;
    private static PermissionCollection allPermission;
    private ClassPath classPath;

    public static ClassLoader create(ClassPath cp) {
        return ClassLoaderSupport.create(cp, ClassLoader.getSystemClassLoader());
    }

    static ClassLoader create(ClassPath cp, ClassLoader parentClassLoader) {
        return new ClassLoaderSupport(cp, parentClassLoader);
    }

    private ClassLoaderSupport(ClassPath cp, ClassLoader parentClassLoader) {
        super(cp.getRoots(), parentClassLoader, null);
        this.classPath = cp;
        this.setDefaultPermissions(ClassLoaderSupport.getAllPermissions());
        this.listener = FileUtil.weakFileChangeListener((FileChangeListener)this, (Object)null);
        this.propListener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)null);
        cp.addPropertyChangeListener(this.propListener);
    }

    protected Class findClass(String name) throws ClassNotFoundException {
        String resName;
        FileObject fo;
        Class c = super.findClass(name);
        if (c != null && (fo = this.classPath.findResource(resName = name.replace('.', '/') + ".class")) != null) {
            this.addFileChangeListener(fo);
        }
        return c;
    }

    public URL findResource(String name) {
        FileObject fo;
        URL url = super.findResource(name);
        if (url != null && (fo = this.classPath.findResource(name)) != null) {
            this.addFileChangeListener(fo);
        }
        return url;
    }

    private void test(FileObject fo) {
        this.classPath.resetClassLoader((ClassLoader)((Object)this));
        this.removeAllListeners();
    }

    private void reset() {
        this.classPath.resetClassLoader((ClassLoader)((Object)this));
        this.removeAllListeners();
    }

    private void testRemove(FileObject fo) {
        this.removeFileChangeListener(fo);
    }

    public void fileFolderCreated(FileEvent fe) {
        this.testRemove(fe.getFile());
    }

    public void fileDataCreated(FileEvent fe) {
        this.testRemove(fe.getFile());
    }

    public void fileChanged(FileEvent fe) {
        this.test(fe.getFile());
    }

    public void fileDeleted(FileEvent fe) {
        this.test(fe.getFile());
    }

    public void fileRenamed(FileRenameEvent fe) {
        this.test(fe.getFile());
    }

    public void fileAttributeChanged(FileAttributeEvent fe) {
        this.testRemove(fe.getFile());
    }

    static synchronized PermissionCollection getAllPermissions() {
        if (allPermission == null) {
            allPermission = new Permissions();
            allPermission.add(new AllPermission());
        }
        return allPermission;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("roots".equals(evt.getPropertyName())) {
            this.reset();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addFileChangeListener(FileObject fo) {
        boolean add;
        Object object = this.lock;
        synchronized (object) {
            if (this.detachedFromCp) {
                return;
            }
            add = this.emittedFileObjects.put(fo, Boolean.FALSE) == null;
        }
        if (add) {
            fo.addFileChangeListener(this.listener);
            object = this.lock;
            synchronized (object) {
                if (!this.detachedFromCp) {
                    assert (this.emittedFileObjects.get((Object)fo) == Boolean.FALSE);
                    this.emittedFileObjects.put(fo, Boolean.TRUE);
                } else {
                    this.emittedFileObjects.remove((Object)fo);
                    add = false;
                }
            }
            if (!add) {
                fo.removeFileChangeListener(this.listener);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeFileChangeListener(FileObject fo) {
        boolean remove;
        Object object = this.lock;
        synchronized (object) {
            remove = this.emittedFileObjects.remove((Object)fo) == Boolean.TRUE;
        }
        if (remove) {
            fo.removeFileChangeListener(this.listener);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeAllListeners() {
        Map.Entry[] removeListenerFrom;
        Object object = this.lock;
        synchronized (object) {
            this.detachedFromCp = true;
            if (this.emittedFileObjects.isEmpty()) {
                return;
            }
            removeListenerFrom = this.emittedFileObjects.entrySet().toArray(new Map.Entry[this.emittedFileObjects.size()]);
            this.emittedFileObjects.clear();
        }
        for (Map.Entry e : removeListenerFrom) {
            if (e.getValue() != Boolean.TRUE) continue;
            ((FileObject)e.getKey()).removeFileChangeListener(this.listener);
        }
    }
}

