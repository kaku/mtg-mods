/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.filesystems.FileAttributeEvent
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileEvent
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.api.java.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.api.java.classpath.ClassLoaderSupport;
import org.netbeans.modules.java.classpath.ClassPathAccessor;
import org.netbeans.modules.java.classpath.SimplePathResourceImplementation;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.ClassPathProvider;
import org.netbeans.spi.java.classpath.FilteringPathResourceImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.URLMapper;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class ClassPath {
    public static final String EXECUTE = "classpath/execute";
    @Deprecated
    public static final String DEBUG = "classpath/debug";
    public static final String COMPILE = "classpath/compile";
    public static final String SOURCE = "classpath/source";
    public static final String BOOT = "classpath/boot";
    public static final String PROP_ROOTS = "roots";
    public static final String PROP_ENTRIES = "entries";
    public static final String PROP_INCLUDES = "includes";
    public static final ClassPath EMPTY;
    private static final Logger LOG;
    private static final AtomicReference<Lookup.Result<? extends ClassPathProvider>> implementations;
    private final ClassPathImplementation impl;
    private final Throwable caller;
    private FileObject[] rootsCache;
    private Map<FileObject, FilteringPathResourceImplementation> root2Filter = new WeakHashMap<FileObject, FilteringPathResourceImplementation>();
    private PropertyChangeListener pListener;
    private final List<Object[]> weakPListeners = new LinkedList<Object[]>();
    private RootsListener rootsListener;
    private List<Entry> entriesCache;
    private long invalidEntries;
    private long invalidRoots;
    private final PropertyChangeSupport propSupport;
    private static final Reference<ClassLoader> EMPTY_REF;
    private Reference<ClassLoader> refClassLoader = EMPTY_REF;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public FileObject[] getRoots() {
        long current;
        FileObject[] ret;
        ClassPath classPath = this;
        synchronized (classPath) {
            if (this.rootsCache != null) {
                return this.rootsCache;
            }
            current = this.invalidRoots;
        }
        List<Entry> entries = this.entries();
        ClassPath classPath2 = this;
        synchronized (classPath2) {
            if (this.invalidRoots == current) {
                if (this.rootsCache == null || this.rootsListener == null) {
                    this.attachRootsListener();
                    this.rootsCache = this.createRoots(entries);
                }
                ret = this.rootsCache;
            } else {
                ret = this.createRoots(entries);
            }
        }
        assert (ret != null);
        return ret;
    }

    private FileObject[] createRoots(List<Entry> entries) {
        ArrayList<FileObject> l = new ArrayList<FileObject>();
        HashSet<Object> listenOn = new HashSet<Object>();
        for (Entry entry : entries) {
            FileObject fo = entry.getRoot();
            File file = null;
            if (fo != null) {
                l.add(fo);
                this.root2Filter.put(fo, entry.filter);
                FileObject fileFo = FileUtil.getArchiveFile((FileObject)fo);
                if (fileFo == null) {
                    fileFo = fo;
                }
                file = FileUtil.toFile((FileObject)fileFo);
            }
            if (file == null) {
                URL external;
                FileObject fileFo;
                URL url = entry.getURL();
                if ("jar".equals(url.getProtocol())) {
                    url = FileUtil.getArchiveFile((URL)url);
                }
                if (!"file".equals(url.getProtocol()) && (fileFo = URLMapper.findFileObject((URL)url)) != null && (external = URLMapper.findURL((FileObject)fileFo, (int)1)) != null) {
                    url = external;
                }
                try {
                    if ("file".equals(url.getProtocol())) {
                        file = FileUtil.normalizeFile((File)Utilities.toFile((URI)url.toURI()));
                    }
                }
                catch (IllegalArgumentException e) {
                    LOG.log(Level.WARNING, "Unexpected URL <{0}>: {1}", new Object[]{url, e});
                }
                catch (URISyntaxException e) {
                    LOG.log(Level.WARNING, "Invalid URL: {0}", url);
                }
            }
            if (file == null) continue;
            listenOn.add(file);
        }
        RootsListener rL = this.getRootsListener();
        if (rL != null) {
            rL.addRoots(listenOn);
        }
        return l.toArray((T[])new FileObject[l.size()]);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<Entry> entries() {
        List<Entry> result;
        long current;
        ClassPath classPath = this;
        synchronized (classPath) {
            if (this.entriesCache != null) {
                return this.entriesCache;
            }
            current = this.invalidEntries;
        }
        List<? extends PathResourceImplementation> resources = this.impl.getResources();
        if (resources == null) {
            throw new NullPointerException("ClassPathImplementation.getResources() returned null. ClassPathImplementation.class: " + this.impl.getClass().toString() + " ClassPathImplementation: " + this.impl.toString());
        }
        ArrayList<Object[]> snapshot = new ArrayList<Object[]>();
        for (PathResourceImplementation pr22 : resources) {
            snapshot.add(new Object[]{pr22, pr22.getRoots()});
        }
        ClassPath pr22 = this;
        synchronized (pr22) {
            if (this.invalidEntries == current) {
                if (this.entriesCache == null) {
                    this.entriesCache = this.createEntries(snapshot);
                }
                result = this.entriesCache;
            } else {
                result = this.createEntries(snapshot);
            }
        }
        assert (result != null);
        return result;
    }

    private List<Entry> createEntries(List<Object[]> resources) {
        ArrayList<Entry> cache = new ArrayList<Entry>();
        Iterator<Object[]> it = this.weakPListeners.iterator();
        while (it.hasNext()) {
            Object[] rwp = it.next();
            it.remove();
            ((PathResourceImplementation)rwp[0]).removePropertyChangeListener((PropertyChangeListener)rwp[1]);
        }
        assert (this.weakPListeners.isEmpty());
        for (Object[] pair : resources) {
            PathResourceImplementation pr = (PathResourceImplementation)pair[0];
            URL[] roots = (URL[])pair[1];
            PropertyChangeListener weakPListener = WeakListeners.propertyChange((PropertyChangeListener)this.pListener, (Object)pr);
            pr.addPropertyChangeListener(weakPListener);
            this.weakPListeners.add(new Object[]{pr, weakPListener});
            for (URL root : roots) {
                if (!(pr instanceof SimplePathResourceImplementation)) {
                    SimplePathResourceImplementation.verify(root, " From: " + pr.getClass().getName(), this.caller);
                }
                cache.add(new Entry(root, pr instanceof FilteringPathResourceImplementation ? (FilteringPathResourceImplementation)pr : null));
            }
        }
        return Collections.unmodifiableList(cache);
    }

    private ClassPath(ClassPathImplementation impl) {
        if (impl == null) {
            throw new IllegalArgumentException();
        }
        this.propSupport = new PropertyChangeSupport(this);
        this.impl = impl;
        this.pListener = new SPIListener();
        this.impl.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.pListener, (Object)this.impl));
        this.caller = new IllegalArgumentException();
    }

    private ClassPath() {
        this.propSupport = new PropertyChangeSupport(this){

            @Override
            public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
            }

            @Override
            public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
            }

            @Override
            public void firePropertyChange(PropertyChangeEvent evt) {
            }
        };
        this.impl = new ClassPathImplementation(){

            @Override
            public List<? extends PathResourceImplementation> getResources() {
                return Collections.emptyList();
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {
            }
        };
        this.pListener = new SPIListener();
        this.caller = new IllegalArgumentException();
    }

    public final FileObject findResource(String resourceName) {
        return this.findResourceImpl(this.getRoots(), new int[]{0}, ClassPath.parseResourceName(resourceName));
    }

    public final List<FileObject> findAllResources(String resourceName) {
        FileObject[] roots = this.getRoots();
        ArrayList<FileObject> l = new ArrayList<FileObject>(roots.length);
        int[] idx = new int[]{0};
        String[] namec = ClassPath.parseResourceName(resourceName);
        while (idx[0] < roots.length) {
            FileObject f = this.findResourceImpl(roots, idx, namec);
            if (f == null) continue;
            l.add(f);
        }
        return l;
    }

    public final String getResourceName(FileObject f) {
        return this.getResourceName(f, '/', true);
    }

    public final String getResourceName(FileObject f, char dirSep, boolean includeExt) {
        int index;
        FileObject owner = this.findOwnerRoot(f);
        if (owner == null) {
            return null;
        }
        String partName = FileUtil.getRelativePath((FileObject)owner, (FileObject)f);
        assert (partName != null);
        if (!includeExt && (index = partName.lastIndexOf(46)) > 0 && index > partName.lastIndexOf(47) + 1) {
            partName = partName.substring(0, index);
        }
        if (dirSep != '/') {
            partName = partName.replace('/', dirSep);
        }
        return partName;
    }

    @CheckForNull
    public final FileObject findOwnerRoot(FileObject resource) {
        FileObject[] roots = this.getRoots();
        HashSet<FileObject> rootsSet = new HashSet<FileObject>(Arrays.asList(roots));
        for (FileObject f = resource; f != null; f = f.getParent()) {
            if (!rootsSet.contains((Object)f)) continue;
            return f;
        }
        return null;
    }

    public final boolean contains(FileObject f) {
        FileObject root = this.findOwnerRoot(f);
        if (root == null) {
            return false;
        }
        FilteringPathResourceImplementation filter = this.root2Filter.get((Object)root);
        if (filter == null) {
            return true;
        }
        String path = FileUtil.getRelativePath((FileObject)root, (FileObject)f);
        assert (path != null);
        if (f.isFolder()) {
            path = path + "/";
        }
        return filter.includes(root.toURL(), path);
    }

    public final boolean isResourceVisible(FileObject resource) {
        String resourceName = this.getResourceName(resource);
        if (resourceName == null) {
            return false;
        }
        return this.findResource(resourceName) == resource;
    }

    public final synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        this.attachRootsListener();
        this.propSupport.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.propSupport.removePropertyChangeListener(l);
    }

    @CheckForNull
    public static ClassPath getClassPath(@NonNull FileObject f, @NonNull String id) {
        if (f == null) {
            Thread.dumpStack();
            return null;
        }
        Lookup.Result<? extends ClassPathProvider> impls = implementations.get();
        if (impls == null && !implementations.compareAndSet((Lookup.Result)null, (Lookup.Result)(impls = Lookup.getDefault().lookupResult(ClassPathProvider.class)))) {
            impls = implementations.get();
        }
        for (ClassPathProvider impl : impls.allInstances()) {
            ClassPath cp = impl.findClassPath(f, id);
            if (cp == null) continue;
            LOG.log(Level.FINE, "getClassPath({0}, {1}) -> {2} from {3}", new Object[]{f, id, cp, impl});
            return cp;
        }
        LOG.log(Level.FINE, "getClassPath({0}, {1}) -> nil", new Object[]{f, id});
        return null;
    }

    final void firePropertyChange(String what, Object oldV, Object newV, Object propagationId) {
        PropertyChangeEvent event = new PropertyChangeEvent(this, what, oldV, newV);
        event.setPropagationId(propagationId);
        this.propSupport.firePropertyChange(event);
    }

    public String toString(PathConversionMode conversionMode) {
        StringBuilder b = new StringBuilder();
        block6 : for (Entry e : this.entries()) {
            URL u = e.getURL();
            File f = FileUtil.archiveOrDirForURL((URL)u);
            if (f != null) {
                if (b.length() > 0) {
                    b.append(File.pathSeparatorChar);
                }
                b.append(f.getAbsolutePath());
                continue;
            }
            switch (conversionMode) {
                case SKIP: {
                    continue block6;
                }
                case PRINT: {
                    if (b.length() > 0) {
                        b.append(File.pathSeparatorChar);
                    }
                    b.append(u);
                    continue block6;
                }
                case WARN: {
                    LOG.log(Level.WARNING, "Encountered untranslatable classpath entry: {0}", u);
                    continue block6;
                }
                case FAIL: {
                    throw new IllegalArgumentException("Encountered untranslatable classpath entry: " + u);
                }
            }
            assert (false);
        }
        return b.toString();
    }

    public String toString() {
        return this.toString(PathConversionMode.PRINT);
    }

    public boolean equals(Object obj) {
        return obj instanceof ClassPath && this.impl.equals(((ClassPath)obj).impl);
    }

    public int hashCode() {
        return this.impl.hashCode() ^ 22;
    }

    private void attachRootsListener() {
        if (this.rootsListener == null) {
            assert (this.rootsCache == null);
            this.rootsListener = new RootsListener(this);
        }
    }

    private static String[] parseResourceName(String name) {
        int pos;
        ArrayList<String> parsed = new ArrayList<String>(name.length() / 4);
        char[] chars = name.toCharArray();
        int dotPos = -1;
        int startPos = 0;
        block4 : for (pos = 0; pos < chars.length; ++pos) {
            char ch = chars[pos];
            switch (ch) {
                case '.': {
                    dotPos = pos;
                    continue block4;
                }
                case '/': {
                    if (dotPos != -1) {
                        parsed.add(name.substring(startPos, dotPos));
                        parsed.add(name.substring(dotPos + 1, pos));
                    } else {
                        parsed.add(name.substring(startPos, pos));
                        parsed.add(null);
                    }
                    startPos = pos + 1;
                    dotPos = -1;
                }
            }
        }
        if (pos > startPos) {
            if (dotPos != -1) {
                parsed.add(name.substring(startPos, dotPos));
                parsed.add(name.substring(dotPos + 1, pos));
            } else {
                parsed.add(name.substring(startPos, pos));
                parsed.add(null);
            }
        }
        if (parsed.size() % 2 != 0) {
            System.err.println("parsed size is not even!!");
            System.err.println("input = " + name);
        }
        return parsed.toArray(new String[parsed.size()]);
    }

    private static FileObject findPath(@NonNull FileObject parent, @NonNull String[] nameParts, @NonNull String[] relativePath) {
        assert (relativePath.length == 1);
        StringBuilder relativePathBuilder = new StringBuilder();
        String separator = "";
        for (int i = 0; i < nameParts.length && parent != null; i += 2) {
            FileObject child = parent.getFileObject(nameParts[i], nameParts[i + 1]);
            if (child != null) {
                relativePathBuilder.append(separator).append(child.getNameExt());
            }
            separator = "/";
            parent = child;
        }
        if (parent != null) {
            if (parent.isFolder()) {
                relativePathBuilder.append(separator);
            }
            relativePath[0] = relativePathBuilder.toString();
        } else {
            relativePath[0] = null;
        }
        return parent;
    }

    private FileObject findResourceImpl(FileObject[] roots, int[] rootIndex, String[] nameComponents) {
        int ridx;
        FileObject f = null;
        String[] pathOut = new String[1];
        for (ridx = rootIndex[0]; ridx < roots.length && f == null; ++ridx) {
            f = ClassPath.findPath(roots[ridx], nameComponents, pathOut);
            FilteringPathResourceImplementation filter = this.root2Filter.get((Object)roots[ridx]);
            if (filter == null || f == null || filter.includes(roots[ridx].toURL(), pathOut[0].toString())) continue;
            f = null;
        }
        rootIndex[0] = ridx;
        return f;
    }

    synchronized void resetClassLoader(ClassLoader cl) {
        if (this.refClassLoader.get() == cl) {
            this.refClassLoader = EMPTY_REF;
        }
    }

    public final synchronized ClassLoader getClassLoader(boolean cache) {
        ClassLoader o = this.refClassLoader.get();
        if (!cache || o == null) {
            o = ClassLoaderSupport.create(this);
            this.refClassLoader = new SoftReference<ClassLoader>(o);
        }
        return o;
    }

    private synchronized RootsListener getRootsListener() {
        return this.rootsListener;
    }

    static {
        ClassPathAccessor.DEFAULT = new ClassPathAccessor(){

            @Override
            public ClassPath createClassPath(ClassPathImplementation spiClasspath) {
                return new ClassPath(spiClasspath);
            }

            @Override
            public ClassPathImplementation getClassPathImpl(ClassPath cp) {
                return cp == null ? null : cp.impl;
            }
        };
        EMPTY = new ClassPath();
        LOG = Logger.getLogger(ClassPath.class.getName());
        implementations = new AtomicReference();
        EMPTY_REF = new SoftReference<Object>(null);
    }

    private static class RootsListener
    extends WeakReference<ClassPath>
    implements FileChangeListener,
    Runnable {
        private final Set<File> roots = new HashSet<File>();

        private RootsListener(ClassPath owner) {
            super(owner, Utilities.activeReferenceQueue());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addRoots(Set<? extends File> newRoots) {
            Parameters.notNull((CharSequence)"urls", newRoots);
            RootsListener rootsListener = this;
            synchronized (rootsListener) {
                HashSet<File> toRemove = new HashSet<File>(this.roots);
                toRemove.removeAll(newRoots);
                HashSet<? extends File> toAdd = new HashSet<File>(newRoots);
                toAdd.removeAll(this.roots);
                for (File root2 : toRemove) {
                    this.safeRemoveListener(root2);
                    this.roots.remove(root2);
                }
                for (File root : toAdd) {
                    this.safeAddListener(root);
                    this.roots.add(root);
                }
            }
        }

        public synchronized void removeAllRoots() {
            Iterator<File> it = this.roots.iterator();
            while (it.hasNext()) {
                File root = it.next();
                it.remove();
                FileUtil.removeFileChangeListener((FileChangeListener)this, (File)root);
            }
        }

        public void fileFolderCreated(FileEvent fe) {
            this.processEvent(fe);
        }

        public void fileDataCreated(FileEvent fe) {
            this.processEvent(fe);
        }

        public void fileChanged(FileEvent fe) {
            this.processEvent(fe);
        }

        public void fileDeleted(FileEvent fe) {
            this.processEvent(fe);
        }

        public void fileRenamed(FileRenameEvent fe) {
            this.processEvent((FileEvent)fe);
        }

        public void fileAttributeChanged(FileAttributeEvent fe) {
        }

        @Override
        public void run() {
            try {
                this.removeAllRoots();
            }
            catch (IllegalArgumentException iae) {
                // empty catch block
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void processEvent(FileEvent fe) {
            ClassPath cp = (ClassPath)this.get();
            if (cp == null) {
                return;
            }
            ClassPath classPath = cp;
            synchronized (classPath) {
                cp.rootsCache = null;
                cp.invalidRoots++;
            }
            cp.firePropertyChange("roots", null, null, null);
        }

        private void safeAddListener(@NonNull File root) {
            try {
                FileUtil.addFileChangeListener((FileChangeListener)this, (File)root);
            }
            catch (IllegalArgumentException iae) {
                LOG.warning(iae.getMessage());
            }
        }

        private void safeRemoveListener(@NonNull File root) {
            try {
                FileUtil.removeFileChangeListener((FileChangeListener)this, (File)root);
            }
            catch (IllegalArgumentException iae) {
                LOG.warning(iae.getMessage());
            }
        }
    }

    private class SPIListener
    implements PropertyChangeListener {
        private Object propIncludesPropagationId;

        private SPIListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Object object;
            String prop = evt.getPropertyName();
            if ("resources".equals(prop) || "roots".equals(prop)) {
                ClassPath classPath = ClassPath.this;
                synchronized (classPath) {
                    if (ClassPath.this.rootsListener != null) {
                        ClassPath.this.rootsListener.removeAllRoots();
                    }
                    ClassPath.this.entriesCache = null;
                    ClassPath.this.rootsCache = null;
                    ClassPath.this.invalidEntries++;
                    ClassPath.this.invalidRoots++;
                }
                ClassPath.this.firePropertyChange("entries", null, null, null);
                ClassPath.this.firePropertyChange("roots", null, null, null);
            } else if ("includes".equals(prop)) {
                boolean fire;
                object = this;
                synchronized (object) {
                    Object id = evt.getPropagationId();
                    fire = this.propIncludesPropagationId == null || !this.propIncludesPropagationId.equals(id);
                    this.propIncludesPropagationId = id;
                }
                if (fire) {
                    ClassPath.this.firePropertyChange("includes", null, null, evt.getPropagationId());
                }
            }
            if ("resources".equals(prop)) {
                List<? extends PathResourceImplementation> resources = ClassPath.this.impl.getResources();
                if (resources == null) {
                    LOG.log(Level.WARNING, "ClassPathImplementation.getResources cannot return null; impl class: {0}", ClassPath.this.impl.getClass().getName());
                    return;
                }
                object = ClassPath.this;
                synchronized (object) {
                    Iterator it = ClassPath.this.weakPListeners.iterator();
                    while (it.hasNext()) {
                        Object[] rwp = (Object[])it.next();
                        it.remove();
                        ((PathResourceImplementation)rwp[0]).removePropertyChangeListener((PropertyChangeListener)rwp[1]);
                    }
                    assert (ClassPath.this.weakPListeners.isEmpty());
                    for (PathResourceImplementation pri : resources) {
                        PropertyChangeListener weakPListener = WeakListeners.propertyChange((PropertyChangeListener)ClassPath.this.pListener, (Object)pri);
                        pri.addPropertyChangeListener(weakPListener);
                        ClassPath.this.weakPListeners.add(new Object[]{pri, weakPListener});
                    }
                }
            }
        }
    }

    public final class Entry {
        private final URL url;
        private volatile FileObject root;
        private IOException lastError;
        private FilteringPathResourceImplementation filter;
        Boolean isDataResult;

        public ClassPath getDefiningClassPath() {
            return ClassPath.this;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public FileObject getRoot() {
            FileObject _root = this.root;
            if (_root != null && _root.isValid()) {
                return _root;
            }
            for (int retryCount = 0; retryCount <= 1; ++retryCount) {
                _root = URLMapper.findFileObject((URL)this.url);
                Entry entry = this;
                synchronized (entry) {
                    if (this.root == null || !this.root.isValid()) {
                        if (_root == null) {
                            this.lastError = new IOException(MessageFormat.format("The package root {0} does not exist or can not be read.", this.url));
                            return null;
                        }
                        if (this.isData(_root)) {
                            if (retryCount == 0) {
                                Logger l = Logger.getLogger("org.netbeans.modules.masterfs");
                                Level prev = l.getLevel();
                                try {
                                    l.setLevel(Level.FINEST);
                                    LOG.log(Level.WARNING, "Root is not folder {0}; about to refresh", (Object)_root);
                                    _root.refresh();
                                    FileObject parent = _root.getParent();
                                    if (parent != null) {
                                        LOG.log(Level.WARNING, "Refreshing its parent {0}", (Object)parent);
                                        FileObject[] arr = parent.getChildren();
                                        parent.refresh();
                                    }
                                }
                                finally {
                                    l.setLevel(prev);
                                    LOG.warning("End of refresh");
                                }
                                continue;
                            }
                            String fileState = null;
                            try {
                                File file = Utilities.toFile((URI)this.url.toURI());
                                boolean exists = file.exists();
                                boolean isDirectory = file.isDirectory();
                                if (exists && !isDirectory) {
                                    LOG.log(Level.WARNING, "Ignoring non folder root : {0} on classpath ", file);
                                    return null;
                                }
                                fileState = "(exists: " + exists + " file: " + file.isFile() + " directory: " + isDirectory + " read: " + file.canRead() + " write: " + file.canWrite() + " root: " + (Object)this.root + " _root: " + (Object)_root + ")";
                            }
                            catch (IllegalArgumentException e) {
                            }
                            catch (URISyntaxException e) {
                                // empty catch block
                            }
                            throw new IllegalArgumentException("Invalid ClassPath root: " + this.url + ". The root must be a folder." + (fileState != null ? fileState : ""));
                        }
                        this.root = _root;
                    }
                    return this.root;
                }
            }
            return null;
        }

        public boolean isValid() {
            FileObject r = this.getRoot();
            return r != null && r.isValid();
        }

        public IOException getError() {
            IOException error = this.lastError;
            this.lastError = null;
            return error;
        }

        public URL getURL() {
            return this.url;
        }

        public boolean includes(String resource) {
            return this.filter == null || this.filter.includes(this.url, resource);
        }

        public boolean includes(URL file) {
            URI relative;
            if (!file.toExternalForm().startsWith(this.url.toExternalForm())) {
                throw new IllegalArgumentException(file + " not in " + this.url);
            }
            try {
                relative = this.url.toURI().relativize(file.toURI());
            }
            catch (URISyntaxException x) {
                throw new AssertionError(x);
            }
            assert (!relative.isAbsolute());
            return this.filter == null || this.filter.includes(this.url, relative.toString());
        }

        public boolean includes(FileObject file) {
            if (!file.isValid()) {
                return false;
            }
            FileObject r = this.getRoot();
            if (r == null) {
                file.refresh();
                if (!file.isValid()) {
                    return false;
                }
                throw new IllegalArgumentException("no root in " + this.url);
            }
            String path = FileUtil.getRelativePath((FileObject)r, (FileObject)file);
            if (path == null) {
                if (!file.isValid()) {
                    return false;
                }
                StringBuilder sb = new StringBuilder();
                sb.append((Object)file).append(" (valid: ").append(file.isValid()).append(") not in ").append((Object)r).append(" (valid: ").append(r.isValid()).append(")");
                if (file.getPath().startsWith(r.getPath())) {
                    while (file.getPath().length() > r.getPath().length()) {
                        file = file.getParent();
                        sb.append("\nChildren of ").append((Object)file).append(" (valid: ").append(file.isValid()).append(")").append(" are:\n  ").append(Arrays.toString((Object[])file.getChildren()));
                    }
                } else {
                    sb.append("\nRoot path is not prefix");
                }
                throw new IllegalArgumentException(sb.toString());
            }
            if (file.isFolder()) {
                path = path + "/";
            }
            return this.filter == null || this.filter.includes(this.url, path);
        }

        Entry(@NullAllowed URL url, FilteringPathResourceImplementation filter) {
            Parameters.notNull((CharSequence)"url", (Object)url);
            this.url = url;
            this.filter = filter;
        }

        public String toString() {
            return "Entry[" + this.url + "]";
        }

        public boolean equals(Object other) {
            if (other instanceof Entry) {
                return Utilities.compareObjects((Object)((Entry)other).url, (Object)this.url);
            }
            return false;
        }

        public int hashCode() {
            return this.url == null ? 0 : this.url.hashCode();
        }

        private synchronized boolean isData(FileObject fo) {
            if (this.isDataResult != null) {
                boolean res = this.isDataResult;
                this.isDataResult = null;
                return res;
            }
            return fo.isData();
        }
    }

    public static enum PathConversionMode {
        SKIP,
        WARN,
        FAIL,
        PRINT;
        

        private PathConversionMode() {
        }
    }

}

