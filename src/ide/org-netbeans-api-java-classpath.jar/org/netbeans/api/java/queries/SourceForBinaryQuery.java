/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Parameters
 *  org.openide.util.WeakListeners
 */
package org.netbeans.api.java.queries;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.java.classpath.SimplePathResourceImplementation;
import org.netbeans.modules.java.queries.SFBQImpl2Result;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.util.ChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.Parameters;
import org.openide.util.WeakListeners;

public class SourceForBinaryQuery {
    private static final Logger LOG = Logger.getLogger(SourceForBinaryQuery.class.getName());
    private static final Lookup.Result<? extends SourceForBinaryQueryImplementation> implementations = Lookup.getDefault().lookupResult(SourceForBinaryQueryImplementation.class);
    private static final Result EMPTY_RESULT = new EmptyResult();
    private static final Result2 EMPTY_RESULT2 = new Result2(new SFBQImpl2Result(EMPTY_RESULT));

    private SourceForBinaryQuery() {
    }

    public static Result findSourceRoots(URL binaryRoot) {
        SimplePathResourceImplementation.verify(binaryRoot, null);
        for (SourceForBinaryQueryImplementation impl : implementations.allInstances()) {
            Result result = impl.findSourceRoots(binaryRoot);
            if (result == null) continue;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "findSourceRoots({0}) -> {1} from {2}", new Object[]{binaryRoot, Arrays.asList(result.getRoots()), impl});
            }
            return result;
        }
        LOG.log(Level.FINE, "findSourceRoots({0}) -> nil", binaryRoot);
        return EMPTY_RESULT;
    }

    public static Result2 findSourceRoots2(URL binaryRoot) {
        SimplePathResourceImplementation.verify(binaryRoot, null);
        for (SourceForBinaryQueryImplementation impl : implementations.allInstances()) {
            Result _result;
            Result2 result = null;
            if (impl instanceof SourceForBinaryQueryImplementation2) {
                _result = ((SourceForBinaryQueryImplementation2)impl).findSourceRoots2(binaryRoot);
                if (_result != null) {
                    result = new Result2((SourceForBinaryQueryImplementation2.Result)_result);
                }
            } else {
                _result = impl.findSourceRoots(binaryRoot);
                if (_result != null) {
                    result = new Result2(new SFBQImpl2Result(_result));
                }
            }
            if (result == null) continue;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "findSourceRoots2({0}) -> {1} from {2}", new Object[]{binaryRoot, Arrays.asList(result.getRoots()), impl});
            }
            return result;
        }
        LOG.log(Level.FINE, "findSourceRoots2({0}) -> nil", binaryRoot);
        return EMPTY_RESULT2;
    }

    private static final class EmptyResult
    implements Result {
        private static final FileObject[] NO_ROOTS = new FileObject[0];

        EmptyResult() {
        }

        @Override
        public FileObject[] getRoots() {
            return NO_ROOTS;
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }
    }

    public static class Result2
    implements Result {
        SourceForBinaryQueryImplementation2.Result delegate;
        private ChangeListener spiListener;
        private final ChangeSupport changeSupport;

        private Result2(SourceForBinaryQueryImplementation2.Result result) {
            assert (result != null);
            this.delegate = result;
            this.changeSupport = new ChangeSupport((Object)this);
        }

        @Override
        public FileObject[] getRoots() {
            return this.delegate.getRoots();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void addChangeListener(ChangeListener l) {
            Parameters.notNull((CharSequence)"l", (Object)l);
            Result2 result2 = this;
            synchronized (result2) {
                if (this.spiListener == null) {
                    this.spiListener = new ChangeListener(){

                        @Override
                        public void stateChanged(ChangeEvent e) {
                            Result2.this.changeSupport.fireChange();
                        }
                    };
                    this.delegate.addChangeListener(WeakListeners.change((ChangeListener)this.spiListener, (Object)this.delegate));
                }
            }
            this.changeSupport.addChangeListener(l);
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
            Parameters.notNull((CharSequence)"l", (Object)l);
            this.changeSupport.removeChangeListener(l);
        }

        public boolean preferSources() {
            return this.delegate.preferSources();
        }

    }

    public static interface Result {
        public FileObject[] getRoots();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

