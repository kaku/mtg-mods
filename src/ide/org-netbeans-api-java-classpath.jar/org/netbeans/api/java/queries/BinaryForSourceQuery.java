/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Lookup
 */
package org.netbeans.api.java.queries;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.spi.java.queries.BinaryForSourceQueryImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Lookup;

public final class BinaryForSourceQuery {
    private static final Logger LOG = Logger.getLogger(BinaryForSourceQuery.class.getName());

    private BinaryForSourceQuery() {
    }

    public static Result findBinaryRoots(URL sourceRoot) {
        assert (sourceRoot != null);
        for (BinaryForSourceQueryImplementation impl : Lookup.getDefault().lookupAll(BinaryForSourceQueryImplementation.class)) {
            Result result = impl.findBinaryRoots(sourceRoot);
            if (result == null) continue;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "findBinaryRoots({0}) -> {1} from {2}", new Object[]{sourceRoot, Arrays.asList(result.getRoots()), impl});
            }
            return result;
        }
        LOG.log(Level.FINE, "findBinaryRoots({0}) -> nil", sourceRoot);
        return new DefaultResult(sourceRoot);
    }

    private static class DefaultResult
    implements Result {
        private final URL sourceRoot;

        DefaultResult(URL sourceRoot) {
            this.sourceRoot = sourceRoot;
        }

        @Override
        public URL[] getRoots() {
            FileObject fo = URLMapper.findFileObject((URL)this.sourceRoot);
            if (fo == null) {
                return new URL[0];
            }
            ClassPath exec = ClassPath.getClassPath(fo, "classpath/execute");
            if (exec == null) {
                return new URL[0];
            }
            HashSet<URL> result = new HashSet<URL>();
            for (ClassPath.Entry e : exec.entries()) {
                FileObject[] roots;
                URL eurl = e.getURL();
                for (FileObject root : roots = SourceForBinaryQuery.findSourceRoots(eurl).getRoots()) {
                    if (!this.sourceRoot.equals(root.toURL())) continue;
                    result.add(eurl);
                }
            }
            return result.toArray(new URL[result.size()]);
        }

        @Override
        public void addChangeListener(ChangeListener l) {
        }

        @Override
        public void removeChangeListener(ChangeListener l) {
        }
    }

    public static interface Result {
        public URL[] getRoots();

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

