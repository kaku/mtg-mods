/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.java.classpath;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.support.PathResourceBase;
import org.openide.util.Utilities;

public final class SimplePathResourceImplementation
extends PathResourceBase {
    private URL url;

    public static void verify(URL root, String context) throws IllegalArgumentException {
        SimplePathResourceImplementation.verify(root, context, null);
    }

    public static void verify(URL root, String context, Throwable initiatedIn) throws IllegalArgumentException {
        IllegalArgumentException iae;
        if (context == null) {
            context = "";
        }
        if (root == null) {
            IllegalArgumentException iae2 = new IllegalArgumentException("Root cannot be null." + context);
            if (initiatedIn != null) {
                iae2.initCause(initiatedIn);
            }
            throw iae2;
        }
        String rootS = root.toString();
        if (rootS.matches("file:.+[.]jar/?")) {
            File f = null;
            boolean dir = false;
            try {
                f = Utilities.toFile((URI)root.toURI());
                dir = f.isDirectory();
            }
            catch (URISyntaxException use) {
                // empty catch block
            }
            if (!(dir || f != null && !f.exists() && f.getName().equals(".jar"))) {
                IllegalArgumentException iae3 = new IllegalArgumentException(rootS + " is not a valid classpath entry; use a jar-protocol URL." + context);
                if (initiatedIn != null) {
                    iae3.initCause(initiatedIn);
                }
                throw iae3;
            }
        }
        if (!rootS.endsWith("/")) {
            iae = new IllegalArgumentException(rootS + " is not a valid classpath entry; it must end with a slash." + context);
            if (initiatedIn != null) {
                iae.initCause(initiatedIn);
            }
            throw iae;
        }
        if (rootS.contains("/../") || rootS.contains("/./")) {
            iae = new IllegalArgumentException(rootS + " is not a valid classpath entry; it cannot contain current or parent dir reference." + context);
            if (initiatedIn != null) {
                iae.initCause(initiatedIn);
            }
            throw iae;
        }
    }

    public SimplePathResourceImplementation(URL root) {
        SimplePathResourceImplementation.verify(root, null);
        this.url = root;
    }

    @Override
    public URL[] getRoots() {
        return new URL[]{this.url};
    }

    @Override
    public ClassPathImplementation getContent() {
        return null;
    }

    public String toString() {
        return "SimplePathResource{" + this.url + "}";
    }

    public int hashCode() {
        return this.url.hashCode();
    }

    public boolean equals(Object other) {
        if (other instanceof SimplePathResourceImplementation) {
            SimplePathResourceImplementation opr = (SimplePathResourceImplementation)other;
            return this.url.equals(opr.url);
        }
        return false;
    }
}

