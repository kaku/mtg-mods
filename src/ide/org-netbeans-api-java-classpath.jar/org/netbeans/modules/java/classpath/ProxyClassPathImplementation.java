/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.java.classpath;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;
import org.openide.util.WeakListeners;

public class ProxyClassPathImplementation
implements ClassPathImplementation {
    private ClassPathImplementation[] classPaths;
    private List<PathResourceImplementation> resourcesCache;
    private ArrayList<PropertyChangeListener> listeners;
    private PropertyChangeListener classPathsListener;

    public ProxyClassPathImplementation(ClassPathImplementation[] classPaths) {
        if (classPaths == null) {
            throw new IllegalArgumentException();
        }
        ArrayList<ClassPathImplementation> impls = new ArrayList<ClassPathImplementation>();
        this.classPathsListener = new DelegatesListener();
        for (ClassPathImplementation cpImpl : classPaths) {
            if (cpImpl == null) continue;
            cpImpl.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.classPathsListener, (Object)cpImpl));
            impls.add(cpImpl);
        }
        this.classPaths = impls.toArray(new ClassPathImplementation[impls.size()]);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<? extends PathResourceImplementation> getResources() {
        ProxyClassPathImplementation proxyClassPathImplementation = this;
        synchronized (proxyClassPathImplementation) {
            if (this.resourcesCache != null) {
                return this.resourcesCache;
            }
        }
        ArrayList<? extends PathResourceImplementation> result = new ArrayList<PathResourceImplementation>(this.classPaths.length * 10);
        for (ClassPathImplementation cpImpl : this.classPaths) {
            List<? extends PathResourceImplementation> subPath = cpImpl.getResources();
            if (subPath == null) {
                throw new NullPointerException("ClassPathImplementation.getResources() returned null. ClassPathImplementation.class: " + cpImpl.getClass().toString() + " ClassPathImplementation: " + cpImpl.toString());
            }
            result.addAll(subPath);
        }
        ProxyClassPathImplementation arr$ = this;
        synchronized (arr$) {
            if (this.resourcesCache == null) {
                this.resourcesCache = Collections.unmodifiableList(result);
            }
            return this.resourcesCache;
        }
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners == null) {
            this.listeners = new ArrayList();
        }
        this.listeners.add(listener);
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.listeners == null) {
            return;
        }
        this.listeners.remove(listener);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        for (ClassPathImplementation cpImpl : this.classPaths) {
            builder.append(cpImpl.toString());
            builder.append(", ");
        }
        builder.append("]");
        return builder.toString();
    }

    private class DelegatesListener
    implements PropertyChangeListener {
        private DelegatesListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            PropertyChangeListener[] _listeners;
            ProxyClassPathImplementation proxyClassPathImplementation = ProxyClassPathImplementation.this;
            synchronized (proxyClassPathImplementation) {
                ProxyClassPathImplementation.this.resourcesCache = null;
                if (ProxyClassPathImplementation.this.listeners == null) {
                    return;
                }
                _listeners = ProxyClassPathImplementation.this.listeners.toArray(new PropertyChangeListener[ProxyClassPathImplementation.this.listeners.size()]);
            }
            PropertyChangeEvent event = new PropertyChangeEvent(ProxyClassPathImplementation.this, evt.getPropertyName(), null, null);
            for (PropertyChangeListener l : _listeners) {
                l.propertyChange(event);
            }
        }
    }

}

