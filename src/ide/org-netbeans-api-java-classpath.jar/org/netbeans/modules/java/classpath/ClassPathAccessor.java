/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.classpath;

import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.spi.java.classpath.ClassPathImplementation;

public abstract class ClassPathAccessor {
    public static ClassPathAccessor DEFAULT;

    public abstract ClassPath createClassPath(ClassPathImplementation var1);

    public abstract ClassPathImplementation getClassPathImpl(ClassPath var1);

    static {
        Class<ClassPath> c = ClassPath.class;
        try {
            Class.forName(c.getName(), true, c.getClassLoader());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

