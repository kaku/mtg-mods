/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.java.classpath;

import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;

public class SimpleClassPathImplementation
implements ClassPathImplementation {
    List<? extends PathResourceImplementation> entries;

    public SimpleClassPathImplementation() {
        this(new ArrayList());
    }

    public SimpleClassPathImplementation(List<? extends PathResourceImplementation> entries) {
        this.entries = entries;
    }

    @Override
    public List<? extends PathResourceImplementation> getResources() {
        return Collections.unmodifiableList(this.entries);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("SimpleClassPathImplementation[");
        for (PathResourceImplementation impl : this.entries) {
            URL[] roots;
            for (URL root : roots = impl.getRoots()) {
                builder.append(root.toExternalForm());
                builder.append(", ");
            }
        }
        builder.append("]");
        return builder.toString();
    }
}

