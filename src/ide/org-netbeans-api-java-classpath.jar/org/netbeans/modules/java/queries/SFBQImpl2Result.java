/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.modules.java.queries;

import javax.swing.event.ChangeListener;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation2;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class SFBQImpl2Result
implements SourceForBinaryQueryImplementation2.Result {
    private final SourceForBinaryQuery.Result delegate;

    public SFBQImpl2Result(SourceForBinaryQuery.Result result) {
        assert (result != null);
        this.delegate = result;
    }

    @Override
    public boolean preferSources() {
        FileObject[] roots;
        for (FileObject root : roots = this.delegate.getRoots()) {
            if (root == null) {
                throw new NullPointerException("SFBQ.Result: " + this.delegate.getClass().getName() + " returned null in roots.");
            }
            if (FileUtil.getArchiveFile((FileObject)root) == null) continue;
            return false;
        }
        return true;
    }

    @Override
    public FileObject[] getRoots() {
        return this.delegate.getRoots();
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        this.delegate.addChangeListener(l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        this.delegate.removeChangeListener(l);
    }
}

