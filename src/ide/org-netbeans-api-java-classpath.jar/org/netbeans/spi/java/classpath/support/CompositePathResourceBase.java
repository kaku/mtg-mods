/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.classpath.support;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;

public abstract class CompositePathResourceBase
implements PathResourceImplementation {
    private URL[] roots;
    private ClassPathImplementation model;
    private ArrayList<PropertyChangeListener> pListeners;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public final URL[] getRoots() {
        if (this.roots == null) {
            CompositePathResourceBase compositePathResourceBase = this;
            synchronized (compositePathResourceBase) {
                if (this.roots == null) {
                    this.initContent();
                    ArrayList<URL> result = new ArrayList<URL>();
                    List<? extends PathResourceImplementation> resources = this.model.getResources();
                    if (resources == null) {
                        throw new NullPointerException("ClassPathImplementation.getResources() returned null. ClassPathImplementation.class: " + this.model.getClass().toString() + " ClassPathImplementation: " + this.model.toString());
                    }
                    for (PathResourceImplementation pri : resources) {
                        result.addAll(Arrays.asList(pri.getRoots()));
                    }
                    this.roots = result.toArray(new URL[result.size()]);
                }
            }
        }
        return this.roots;
    }

    @Override
    public final ClassPathImplementation getContent() {
        this.initContent();
        return this.model;
    }

    @Override
    public final synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.pListeners == null) {
            this.pListeners = new ArrayList();
        }
        this.pListeners.add(listener);
    }

    @Override
    public final synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.pListeners == null) {
            return;
        }
        this.pListeners.remove(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void firePropertyChange(String propName, Object oldValue, Object newValue) {
        PropertyChangeListener[] _listeners;
        CompositePathResourceBase compositePathResourceBase = this;
        synchronized (compositePathResourceBase) {
            if (this.pListeners == null) {
                return;
            }
            _listeners = this.pListeners.toArray(new PropertyChangeListener[this.pListeners.size()]);
        }
        PropertyChangeEvent event = new PropertyChangeEvent(this, propName, oldValue, newValue);
        for (PropertyChangeListener l : _listeners) {
            l.propertyChange(event);
        }
    }

    protected abstract ClassPathImplementation createContent();

    private synchronized void initContent() {
        if (this.model == null) {
            ClassPathImplementation cp = this.createContent();
            assert (cp != null);
            cp.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent event) {
                    CompositePathResourceBase.this.roots = null;
                    CompositePathResourceBase.this.firePropertyChange("roots", null, null);
                }
            });
            this.model = cp;
        }
    }

}

