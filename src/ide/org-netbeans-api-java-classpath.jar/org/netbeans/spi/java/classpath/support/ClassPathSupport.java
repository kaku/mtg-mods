/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package org.netbeans.spi.java.classpath.support;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.classpath.ClassPathAccessor;
import org.netbeans.modules.java.classpath.ProxyClassPathImplementation;
import org.netbeans.modules.java.classpath.SimpleClassPathImplementation;
import org.netbeans.modules.java.classpath.SimplePathResourceImplementation;
import org.netbeans.spi.java.classpath.ClassPathFactory;
import org.netbeans.spi.java.classpath.ClassPathImplementation;
import org.netbeans.spi.java.classpath.PathResourceImplementation;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class ClassPathSupport {
    private ClassPathSupport() {
    }

    public static PathResourceImplementation createResource(URL url) {
        return new SimplePathResourceImplementation(url);
    }

    public static ClassPathImplementation createClassPathImplementation(List<? extends PathResourceImplementation> entries) {
        if (entries == null) {
            throw new NullPointerException("Cannot pass null entries");
        }
        return new SimpleClassPathImplementation(entries);
    }

    public static ClassPath createClassPath(List<? extends PathResourceImplementation> entries) {
        if (entries == null) {
            throw new NullPointerException("Cannot pass null entries");
        }
        return ClassPathFactory.createClassPath(ClassPathSupport.createClassPathImplementation(entries));
    }

    public static /* varargs */ ClassPath createClassPath(FileObject ... roots) {
        assert (roots != null);
        ArrayList<PathResourceImplementation> l = new ArrayList<PathResourceImplementation>();
        for (FileObject root : roots) {
            if (root == null || !root.isValid()) continue;
            URL u = root.toURL();
            l.add(ClassPathSupport.createResource(u));
        }
        return ClassPathSupport.createClassPath(l);
    }

    public static /* varargs */ ClassPath createClassPath(URL ... roots) {
        assert (roots != null);
        ArrayList<PathResourceImplementation> l = new ArrayList<PathResourceImplementation>();
        for (URL root : roots) {
            if (root == null) continue;
            l.add(ClassPathSupport.createResource(root));
        }
        return ClassPathSupport.createClassPath(l);
    }

    public static ClassPath createClassPath(String jvmPath) throws IllegalArgumentException {
        ArrayList<PathResourceImplementation> l = new ArrayList<PathResourceImplementation>();
        for (String piece : jvmPath.split(File.pathSeparator)) {
            File f = FileUtil.normalizeFile((File)new File(piece));
            URL u = FileUtil.urlForArchiveOrDir((File)f);
            if (u == null) {
                throw new IllegalArgumentException("Path entry looks to be invalid: " + piece);
            }
            l.add(ClassPathSupport.createResource(u));
        }
        return ClassPathSupport.createClassPath(l);
    }

    public static /* varargs */ ClassPathImplementation createProxyClassPathImplementation(ClassPathImplementation ... delegates) {
        return new ProxyClassPathImplementation(delegates);
    }

    public static /* varargs */ ClassPath createProxyClassPath(ClassPath ... delegates) {
        assert (delegates != null);
        ClassPathImplementation[] impls = new ClassPathImplementation[delegates.length];
        for (int i = 0; i < delegates.length; ++i) {
            impls[i] = ClassPathAccessor.DEFAULT.getClassPathImpl(delegates[i]);
        }
        return ClassPathFactory.createClassPath(ClassPathSupport.createProxyClassPathImplementation(impls));
    }
}

