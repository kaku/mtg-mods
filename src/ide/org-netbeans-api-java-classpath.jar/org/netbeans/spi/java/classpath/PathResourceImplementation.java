/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.classpath;

import java.beans.PropertyChangeListener;
import java.net.URL;
import org.netbeans.spi.java.classpath.ClassPathImplementation;

public interface PathResourceImplementation {
    public static final String PROP_ROOTS = "roots";

    public URL[] getRoots();

    public ClassPathImplementation getContent();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

