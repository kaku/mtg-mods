/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.classpath;

import java.net.URL;
import org.netbeans.spi.java.classpath.PathResourceImplementation;

public interface FilteringPathResourceImplementation
extends PathResourceImplementation {
    public static final String PROP_INCLUDES = "includes";

    public boolean includes(URL var1, String var2);
}

