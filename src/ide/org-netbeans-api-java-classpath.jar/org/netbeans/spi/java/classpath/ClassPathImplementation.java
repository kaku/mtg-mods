/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.classpath;

import java.beans.PropertyChangeListener;
import java.util.List;
import org.netbeans.spi.java.classpath.PathResourceImplementation;

public interface ClassPathImplementation {
    public static final String PROP_RESOURCES = "resources";

    public List<? extends PathResourceImplementation> getResources();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

