/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.classpath;

import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.java.classpath.ClassPathAccessor;
import org.netbeans.spi.java.classpath.ClassPathImplementation;

public final class ClassPathFactory {
    private ClassPathFactory() {
    }

    public static ClassPath createClassPath(ClassPathImplementation spiClasspath) {
        return ClassPathAccessor.DEFAULT.createClassPath(spiClasspath);
    }
}

