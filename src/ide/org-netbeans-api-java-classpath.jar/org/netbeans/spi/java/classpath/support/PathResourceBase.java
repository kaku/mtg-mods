/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.classpath.support;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import org.netbeans.spi.java.classpath.PathResourceImplementation;

public abstract class PathResourceBase
implements PathResourceImplementation {
    private ArrayList<PropertyChangeListener> pListeners;

    @Override
    public final synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        if (this.pListeners == null) {
            this.pListeners = new ArrayList();
        }
        this.pListeners.add(listener);
    }

    @Override
    public final synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        if (this.pListeners == null) {
            return;
        }
        this.pListeners.remove(listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected final void firePropertyChange(String propName, Object oldValue, Object newValue) {
        PropertyChangeListener[] _listeners;
        PathResourceBase pathResourceBase = this;
        synchronized (pathResourceBase) {
            if (this.pListeners == null) {
                return;
            }
            _listeners = this.pListeners.toArray(new PropertyChangeListener[this.pListeners.size()]);
        }
        PropertyChangeEvent event = new PropertyChangeEvent(this, propName, oldValue, newValue);
        for (PropertyChangeListener l : _listeners) {
            l.propertyChange(event);
        }
    }
}

