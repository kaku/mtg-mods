/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.queries;

import java.net.URL;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation;

public interface SourceForBinaryQueryImplementation2
extends SourceForBinaryQueryImplementation {
    public Result findSourceRoots2(URL var1);

    public static interface Result
    extends SourceForBinaryQuery.Result {
        public boolean preferSources();
    }

}

