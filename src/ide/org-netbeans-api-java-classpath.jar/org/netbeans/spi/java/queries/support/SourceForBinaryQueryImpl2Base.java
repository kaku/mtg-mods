/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.java.queries.support;

import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.modules.java.queries.SFBQImpl2Result;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation2;
import org.openide.util.Parameters;

public abstract class SourceForBinaryQueryImpl2Base
implements SourceForBinaryQueryImplementation2 {
    protected final SourceForBinaryQueryImplementation2.Result asResult(SourceForBinaryQuery.Result result) {
        Parameters.notNull((CharSequence)"result", (Object)result);
        if (result instanceof SourceForBinaryQueryImplementation2.Result) {
            return (SourceForBinaryQueryImplementation2.Result)result;
        }
        return new SFBQImpl2Result(result);
    }
}

