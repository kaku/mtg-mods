/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.queries;

import java.net.URL;
import org.netbeans.api.java.queries.SourceForBinaryQuery;

public interface SourceForBinaryQueryImplementation {
    public SourceForBinaryQuery.Result findSourceRoots(URL var1);
}

