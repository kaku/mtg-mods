/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.java.queries;

import java.net.URL;
import org.netbeans.api.java.queries.BinaryForSourceQuery;

public interface BinaryForSourceQueryImplementation {
    public BinaryForSourceQuery.Result findBinaryRoots(URL var1);
}

