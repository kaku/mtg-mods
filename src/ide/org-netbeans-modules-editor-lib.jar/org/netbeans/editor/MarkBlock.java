/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.Utilities;

public class MarkBlock {
    public static final int INVALID = 0;
    public static final int OVERLAP = 1;
    public static final int CONTINUE = 2;
    public static final int EMPTY = 4;
    public static final int THIS_EMPTY = 8;
    public static final int EXTEND = 17;
    public static final int INSIDE = 33;
    public static final int BEFORE = 64;
    public static final int AFTER = 128;
    public static final int CONTINUE_BEGIN = 66;
    public static final int CONTINUE_END = 130;
    public static final int OVERLAP_BEGIN = 257;
    public static final int OVERLAP_END = 513;
    public static final int EXTEND_BEGIN = 273;
    public static final int EXTEND_END = 529;
    public static final int INCLUDE = 785;
    public static final int INSIDE_BEGIN = 1057;
    public static final int INSIDE_END = 2081;
    public static final int INNER = 4129;
    public static final int SAME = 3105;
    public static final int IGNORE_EMPTY = -13;
    protected MarkBlock next;
    protected MarkBlock prev;
    public Mark startMark;
    public Mark endMark;
    protected BaseDocument doc;

    public MarkBlock(BaseDocument doc, Mark startMark, Mark endMark) {
        this.doc = doc;
        this.startMark = startMark;
        this.endMark = endMark;
    }

    public MarkBlock(BaseDocument doc, int startPos, int endPos) throws BadLocationException {
        this(doc, new Mark(), new Mark(), startPos, endPos);
    }

    public MarkBlock(BaseDocument doc, Mark startMark, Mark endMark, int startPos, int endPos) throws BadLocationException {
        this(doc, startMark, endMark);
        try {
            startMark.insert(doc, startPos);
            try {
                endMark.insert(doc, endPos);
            }
            catch (BadLocationException e) {
                try {
                    startMark.remove();
                }
                catch (InvalidMarkException e2) {
                    Utilities.annotateLoggable(e2);
                }
                throw e;
            }
            catch (InvalidMarkException e) {
                Utilities.annotateLoggable(e);
            }
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
    }

    public MarkBlock insertChain(MarkBlock blk) {
        MarkBlock thisPrev;
        blk.prev = thisPrev = this.prev;
        blk.next = this;
        if (thisPrev != null) {
            thisPrev.next = blk;
        }
        this.prev = blk;
        return blk;
    }

    public MarkBlock addChain(MarkBlock blk) {
        if (this.next != null) {
            this.next.insertChain(blk);
        } else {
            this.setNextChain(blk);
        }
        return blk;
    }

    public MarkBlock removeChain() {
        MarkBlock thisNext = this.next;
        MarkBlock thisPrev = this.prev;
        if (thisPrev != null) {
            thisPrev.next = thisNext;
            this.prev = null;
        }
        if (thisNext != null) {
            thisNext.prev = thisPrev;
            this.next = null;
        }
        this.destroyMarks();
        return thisNext;
    }

    public int compare(int startPos, int endPos) {
        if (this.startMark == null || this.endMark == null) {
            return 0;
        }
        try {
            int startThis = this.startMark.getOffset();
            int endThis = this.endMark.getOffset();
            if (startThis > endThis) {
                int tmp = startThis;
                startThis = endThis;
                endThis = tmp;
            }
            if (startPos == endPos) {
                if (startThis == endThis) {
                    if (startPos < startThis) {
                        return 76;
                    }
                    if (startPos > startThis) {
                        return 140;
                    }
                    return 3117;
                }
                if (startPos <= startThis) {
                    return startPos < startThis ? 68 : 1061;
                }
                if (startPos >= endThis) {
                    return startPos > endThis ? 132 : 2085;
                }
                return 4133;
            }
            if (startThis == endThis) {
                if (startPos >= startThis) {
                    return startPos > startThis ? 136 : 537;
                }
                if (endPos >= startThis) {
                    return endPos > startThis ? 72 : 281;
                }
                return 793;
            }
            if (endPos <= startThis) {
                return endPos < startThis ? 64 : 66;
            }
            if (startPos >= endThis) {
                return startPos > endThis ? 128 : 130;
            }
            if (endPos < endThis) {
                if (startPos > startThis) {
                    return 4129;
                }
                if (startPos == startThis) {
                    return 1057;
                }
                return 257;
            }
            if (endPos == endThis) {
                if (startPos > startThis) {
                    return 2081;
                }
                if (startPos == startThis) {
                    return 3105;
                }
                return 273;
            }
            if (startPos > startThis) {
                return 513;
            }
            if (startPos == startThis) {
                return 529;
            }
            return 785;
        }
        catch (InvalidMarkException e) {
            return 0;
        }
    }

    public final MarkBlock getNext() {
        return this.next;
    }

    public final void setNext(MarkBlock b) {
        this.next = b;
    }

    public void setNextChain(MarkBlock b) {
        this.next = b;
        if (b != null) {
            b.prev = this;
        }
    }

    public final MarkBlock getPrev() {
        return this.prev;
    }

    public final void setPrev(MarkBlock b) {
        this.prev = b;
    }

    public void setPrevChain(MarkBlock b) {
        this.prev = b;
        if (b != null) {
            b.next = this;
        }
    }

    public boolean isReverse() {
        try {
            return this.startMark.getOffset() > this.endMark.getOffset();
        }
        catch (InvalidMarkException e) {
            return false;
        }
    }

    public void reverse() {
        Mark tmp = this.startMark;
        this.startMark = this.endMark;
        this.endMark = tmp;
    }

    public boolean checkReverse() {
        if (this.isReverse()) {
            this.reverse();
            return true;
        }
        return false;
    }

    public int extendStart(int startPos) throws BadLocationException {
        try {
            int markPos = this.startMark.getOffset();
            startPos = Math.min(startPos, markPos);
            if (startPos != markPos) {
                this.startMark.move(this.doc, startPos);
            }
            return startPos;
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            return 0;
        }
    }

    public int extendEnd(int endPos) throws BadLocationException {
        try {
            int markPos = this.endMark.getOffset();
            endPos = Math.max(endPos, markPos);
            if (endPos != markPos) {
                this.endMark.move(this.doc, endPos);
            }
            return endPos;
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            return 0;
        }
    }

    public boolean extend(int startPos, int endPos, boolean concat) throws BadLocationException {
        try {
            boolean extended = false;
            int rel = this.compare(startPos, endPos);
            if ((rel & 257) == 257 || (rel & 66) == 66 && concat) {
                extended = true;
                this.startMark.move(this.doc, startPos);
            }
            if ((rel & 513) == 513 || (rel & 130) == 130 && concat) {
                extended = true;
                this.endMark.move(this.doc, endPos);
            }
            return extended;
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            return false;
        }
    }

    public boolean extend(MarkBlock blk, boolean concat) {
        try {
            return this.extend(blk.startMark.getOffset(), blk.endMark.getOffset(), concat);
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
        return false;
    }

    public int shrink(int startPos, int endPos) throws BadLocationException {
        try {
            int rel = this.compare(startPos, endPos);
            switch (rel) {
                case 257: 
                case 1057: {
                    this.startMark.move(this.doc, endPos);
                    break;
                }
                case 513: 
                case 2081: {
                    this.endMark.move(this.doc, startPos);
                }
            }
            return rel;
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            return 0;
        }
    }

    public Document getDocument() {
        return this.doc;
    }

    public int getStartOffset() {
        try {
            return this.startMark.getOffset();
        }
        catch (InvalidMarkException e) {
            return 0;
        }
    }

    public int getEndOffset() {
        try {
            return this.endMark.getOffset();
        }
        catch (InvalidMarkException e) {
            return 0;
        }
    }

    void destroyMarks() {
        try {
            if (this.startMark != null) {
                this.startMark.remove();
                this.startMark = null;
            }
        }
        catch (InvalidMarkException e) {
            // empty catch block
        }
        try {
            if (this.endMark != null) {
                this.endMark.remove();
                this.endMark = null;
            }
        }
        catch (InvalidMarkException e) {
            // empty catch block
        }
    }

    protected void finalize() throws Throwable {
        this.destroyMarks();
        super.finalize();
    }

    public String toString() {
        try {
            return "startPos=" + (this.startMark != null ? new StringBuilder().append(String.valueOf(this.startMark.getOffset())).append('[').append(Utilities.debugPosition(this.doc, this.startMark.getOffset())).append(']').toString() : "null") + ", endPos=" + (this.endMark != null ? new StringBuilder().append(String.valueOf(this.endMark.getOffset())).append('[').append(Utilities.debugPosition(this.doc, this.endMark.getOffset())).append(']').toString() : "null") + ", " + (this.prev != null ? (this.next != null ? "chain member" : "last member") : (this.next != null ? "first member" : "standalone member"));
        }
        catch (InvalidMarkException e) {
            return "";
        }
    }

    public String toStringChain() {
        return this.toString() + (this.next != null ? new StringBuilder().append("\n").append(this.next.toStringChain()).toString() : "");
    }

    public static String debugRelation(int rel) {
        String s = (rel & 4) != 0 ? "EMPTY | " : "";
        s = s + ((rel & 8) != 0 ? "THIS_EMPTY | " : "");
        switch (rel &= -13) {
            case 64: {
                return s + "BEFORE";
            }
            case 128: {
                return s + "AFTER";
            }
            case 66: {
                return s + "CONTINUE_BEGIN";
            }
            case 130: {
                return s + "CONTINUE_END";
            }
            case 257: {
                return s + "OVERLAP_BEGIN";
            }
            case 513: {
                return s + "OVERLAP_END";
            }
            case 273: {
                return s + "EXTEND_BEGIN";
            }
            case 529: {
                return s + "EXTEND_END";
            }
            case 785: {
                return s + "INCLUDE";
            }
            case 1057: {
                return s + "INSIDE_BEGIN";
            }
            case 2081: {
                return s + "INSIDE_END";
            }
            case 4129: {
                return s + "INNER";
            }
            case 3105: {
                return s + "SAME";
            }
            case 0: {
                return s + "INVALID";
            }
        }
        return s + "UNKNOWN_STATE " + rel;
    }
}

