/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.WeakHashMap;
import javax.swing.JComponent;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.modules.editor.lib.drawing.ColoringAccessor;
import org.netbeans.modules.editor.lib.drawing.DrawContext;

public final class Coloring
implements Serializable {
    public static final int FONT_MODE_APPLY_NAME = 1;
    public static final int FONT_MODE_APPLY_STYLE = 2;
    public static final int FONT_MODE_APPLY_SIZE = 4;
    public static final int FONT_MODE_DEFAULT = 7;
    private Font font;
    private int fontMode;
    private Color foreColor;
    private Color backColor;
    private Color underlineColor;
    private Color waveUnderlineColor;
    private Color strikeThroughColor;
    private Color topBorderLineColor;
    private Color rightBorderLineColor;
    private Color bottomBorderLineColor;
    private Color leftBorderLineColor;
    private final String cacheLock = new String("Coloring.cacheLock");
    private transient HashMap fontAndForeColorCache;
    private transient HashMap backColorCache;
    static final long serialVersionUID = -1382649127124476675L;
    private static final WeakHashMap<AttributeSet, Coloring> colorings = new WeakHashMap();

    public Coloring() {
        this(null, null, null);
    }

    public Coloring(Font font, Color foreColor, Color backColor) {
        this(font, 7, foreColor, backColor, null, null);
    }

    public Coloring(Font font, int fontMode, Color foreColor, Color backColor) {
        this(font, fontMode, foreColor, backColor, null, null);
    }

    public Coloring(Font font, int fontMode, Color foreColor, Color backColor, Color underlineColor, Color strikeThroughColor) {
        this(font, fontMode, foreColor, backColor, underlineColor, strikeThroughColor, null);
    }

    public Coloring(Font font, int fontMode, Color foreColor, Color backColor, Color underlineColor, Color strikeThroughColor, Color waveUnderlineColor) {
        this(font, fontMode, foreColor, backColor, underlineColor, strikeThroughColor, waveUnderlineColor, null, null, null, null);
    }

    public Coloring(Font font, int fontMode, Color foreColor, Color backColor, Color underlineColor, Color strikeThroughColor, Color waveUnderlineColor, Color topBorderLineColor, Color rightBorderLineColor, Color bottomBorderLineColor, Color leftBorderLineColor) {
        font = fontMode != 0 ? font : null;
        fontMode = font != null ? fontMode : 7;
        this.font = font;
        this.fontMode = fontMode;
        this.foreColor = foreColor;
        this.backColor = backColor;
        this.underlineColor = underlineColor;
        this.strikeThroughColor = strikeThroughColor;
        this.waveUnderlineColor = waveUnderlineColor;
        this.topBorderLineColor = topBorderLineColor;
        this.rightBorderLineColor = rightBorderLineColor;
        this.bottomBorderLineColor = bottomBorderLineColor;
        this.leftBorderLineColor = leftBorderLineColor;
        this.checkCaches();
    }

    private void checkCaches() {
        boolean createForeColorCache;
        boolean createFontCache = this.font != null && this.fontMode != 0 && this.fontMode != 7;
        boolean bl = createForeColorCache = this.foreColor != null && this.hasAlpha(this.foreColor);
        if (createFontCache || createForeColorCache) {
            this.fontAndForeColorCache = new HashMap(createFontCache && createForeColorCache ? 47 : 23);
        }
        if (this.backColor != null && this.hasAlpha(this.backColor)) {
            this.backColorCache = new HashMap(23);
        }
    }

    private boolean hasAlpha(Color c) {
        return (c.getRGB() & -16777216) != -16777216;
    }

    public Font getFont() {
        return this.font;
    }

    public int getFontMode() {
        return this.fontMode;
    }

    public Color getForeColor() {
        return this.foreColor;
    }

    public Color getBackColor() {
        return this.backColor;
    }

    public Color getUnderlineColor() {
        return this.underlineColor;
    }

    public Color getWaveUnderlineColor() {
        return this.waveUnderlineColor;
    }

    public Color getStrikeThroughColor() {
        return this.strikeThroughColor;
    }

    public Color getTopBorderLineColor() {
        return this.topBorderLineColor;
    }

    public Color getRightBorderLineColor() {
        return this.rightBorderLineColor;
    }

    public Color getBottomBorderLineColor() {
        return this.bottomBorderLineColor;
    }

    public Color getLeftBorderLineColor() {
        return this.leftBorderLineColor;
    }

    private Font modifyFont(Font f) {
        return new Font((this.fontMode & 1) != 0 ? this.font.getName() : f.getName(), (this.fontMode & 2) != 0 ? this.font.getStyle() : f.getStyle(), (this.fontMode & 4) != 0 ? this.font.getSize() : f.getSize());
    }

    private Color modifyForeColor(Color underForeColor) {
        int alpha = this.foreColor.getAlpha();
        int fcRGB = this.foreColor.getRGB();
        int underRGB = underForeColor.getRGB();
        int rgb = ((this.foreColor.getRed() * alpha + underForeColor.getRed() * (255 - alpha)) / 255 & 255) << 16;
        return new Color(rgb += (((fcRGB & 65280) * alpha + (underRGB & 65280) * (255 - alpha)) / 255 & 65280) + (((fcRGB & 255) * alpha + (underRGB & 255) * (255 - alpha)) / 255 & 255), false);
    }

    private Color modifyBackColor(Color underBackColor) {
        int alpha = this.backColor.getAlpha();
        int bcRGB = this.backColor.getRGB();
        int underRGB = underBackColor.getRGB();
        int rgb = ((this.backColor.getRed() * alpha + underBackColor.getRed() * (255 - alpha)) / 255 & 255) << 16;
        return new Color(rgb += (((bcRGB & 65280) * alpha + (underRGB & 65280) * (255 - alpha)) / 255 & 65280) + (((bcRGB & 255) * alpha + (underRGB & 255) * (255 - alpha)) / 255 & 255), false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void apply(DrawContext ctx) {
        String string;
        if (this.font != null) {
            if (this.fontMode == 7) {
                ctx.setFont(this.font);
            } else {
                Font origFont = ctx.getFont();
                if (origFont != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Font f = (Font)this.fontAndForeColorCache.get(origFont);
                        if (f == null) {
                            f = this.modifyFont(origFont);
                            this.fontAndForeColorCache.put(origFont, f);
                        }
                        ctx.setFont(f);
                    }
                }
            }
        }
        if (this.foreColor != null) {
            if (!this.hasAlpha(this.foreColor)) {
                ctx.setForeColor(this.foreColor);
            } else {
                Color origForeColor = ctx.getForeColor();
                if (origForeColor != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Color fc = (Color)this.fontAndForeColorCache.get(origForeColor);
                        if (fc == null) {
                            fc = this.modifyForeColor(origForeColor);
                            this.fontAndForeColorCache.put(origForeColor, fc);
                        }
                        ctx.setForeColor(fc);
                    }
                }
            }
        }
        if (this.backColor != null) {
            if (!this.hasAlpha(this.backColor)) {
                ctx.setBackColor(this.backColor);
            } else {
                Color origBackColor = ctx.getBackColor();
                if (origBackColor != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Color bc = (Color)this.backColorCache.get(origBackColor);
                        if (bc == null) {
                            bc = this.modifyBackColor(origBackColor);
                            this.backColorCache.put(origBackColor, bc);
                        }
                        ctx.setBackColor(bc);
                    }
                }
            }
        }
        if (this.underlineColor != null) {
            ctx.setUnderlineColor(this.underlineColor);
        }
        if (this.waveUnderlineColor != null) {
            ctx.setWaveUnderlineColor(this.waveUnderlineColor);
        }
        if (this.strikeThroughColor != null) {
            ctx.setStrikeThroughColor(this.strikeThroughColor);
        }
        if (this.topBorderLineColor != null) {
            ctx.setTopBorderLineColor(this.topBorderLineColor);
        }
        if (this.rightBorderLineColor != null) {
            ctx.setRightBorderLineColor(this.rightBorderLineColor);
        }
        if (this.bottomBorderLineColor != null) {
            ctx.setBottomBorderLineColor(this.bottomBorderLineColor);
        }
        if (this.leftBorderLineColor != null) {
            ctx.setLeftBorderLineColor(this.leftBorderLineColor);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void apply(JComponent c) {
        String string;
        if (this.font != null) {
            if (this.fontMode == 7) {
                c.setFont(this.font);
            } else {
                Font origFont = c.getFont();
                if (origFont != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Font f = (Font)this.fontAndForeColorCache.get(origFont);
                        if (f == null) {
                            f = this.modifyFont(origFont);
                            this.fontAndForeColorCache.put(origFont, f);
                        }
                        c.setFont(f);
                    }
                }
            }
        }
        if (this.foreColor != null) {
            if (!this.hasAlpha(this.foreColor)) {
                c.setForeground(this.foreColor);
            } else {
                Color origForeColor = c.getForeground();
                if (origForeColor != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Color fc = (Color)this.fontAndForeColorCache.get(origForeColor);
                        if (fc == null) {
                            fc = this.modifyForeColor(origForeColor);
                            this.fontAndForeColorCache.put(origForeColor, fc);
                        }
                        c.setForeground(fc);
                    }
                }
            }
        }
        if (this.backColor != null) {
            if (!this.hasAlpha(this.backColor)) {
                c.setBackground(this.backColor);
            } else {
                Color origBackColor = c.getBackground();
                if (origBackColor != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Color bc = (Color)this.backColorCache.get(origBackColor);
                        if (bc == null) {
                            bc = this.modifyBackColor(origBackColor);
                            this.backColorCache.put(origBackColor, bc);
                        }
                        c.setBackground(bc);
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Coloring apply(Coloring c) {
        String string;
        if (c == null) {
            return this;
        }
        Font newFont = c.font;
        int newFontMode = 7;
        Color newForeColor = c.foreColor;
        Color newBackColor = c.backColor;
        Color newUnderlineColor = c.underlineColor;
        Color newWaveUnderlineColor = c.waveUnderlineColor;
        Color newStrikeThroughColor = c.strikeThroughColor;
        Color newTopBorderLineColor = c.topBorderLineColor;
        Color newRightBorderLineColor = c.rightBorderLineColor;
        Color newBottomBorderLineColor = c.bottomBorderLineColor;
        Color newLeftBorderLineColor = c.leftBorderLineColor;
        if (this.font != null) {
            if (this.fontMode == 7) {
                newFont = this.font;
            } else if (newFont != null) {
                string = this.cacheLock;
                synchronized (string) {
                    Font f = (Font)this.fontAndForeColorCache.get(newFont);
                    if (f == null) {
                        f = this.modifyFont(newFont);
                        this.fontAndForeColorCache.put(newFont, f);
                    }
                    newFont = f;
                }
            }
        } else {
            newFontMode = c.fontMode;
        }
        if (this.foreColor != null) {
            if (!this.hasAlpha(this.foreColor)) {
                newForeColor = this.foreColor;
            } else if (newForeColor != null) {
                string = this.cacheLock;
                synchronized (string) {
                    Color fc = (Color)this.fontAndForeColorCache.get(newForeColor);
                    if (fc == null) {
                        fc = this.modifyForeColor(newForeColor);
                        this.fontAndForeColorCache.put(newForeColor, fc);
                    }
                    newForeColor = fc;
                }
            }
        }
        if (this.backColor != null) {
            if (!this.hasAlpha(this.backColor)) {
                newBackColor = this.backColor;
            } else {
                newBackColor = this.backColor;
                if (newBackColor != null) {
                    string = this.cacheLock;
                    synchronized (string) {
                        Color bc = (Color)this.backColorCache.get(newBackColor);
                        if (bc == null) {
                            bc = this.modifyBackColor(newBackColor);
                            this.backColorCache.put(newBackColor, bc);
                        }
                        newBackColor = bc;
                    }
                }
            }
        }
        if (this.underlineColor != null) {
            newUnderlineColor = this.underlineColor;
        }
        if (this.waveUnderlineColor != null) {
            newWaveUnderlineColor = this.waveUnderlineColor;
        }
        if (this.strikeThroughColor != null) {
            newStrikeThroughColor = this.strikeThroughColor;
        }
        if (this.topBorderLineColor != null) {
            newTopBorderLineColor = this.topBorderLineColor;
        }
        if (this.rightBorderLineColor != null) {
            newRightBorderLineColor = this.rightBorderLineColor;
        }
        if (this.bottomBorderLineColor != null) {
            newBottomBorderLineColor = this.bottomBorderLineColor;
        }
        if (this.leftBorderLineColor != null) {
            newLeftBorderLineColor = this.leftBorderLineColor;
        }
        if (c.fontMode != 7 || newFont != c.font || newForeColor != c.foreColor || newBackColor != c.backColor || newUnderlineColor != c.underlineColor || newWaveUnderlineColor != c.waveUnderlineColor || newStrikeThroughColor != c.strikeThroughColor || newTopBorderLineColor != c.topBorderLineColor || newRightBorderLineColor != c.rightBorderLineColor || newBottomBorderLineColor != c.bottomBorderLineColor || newLeftBorderLineColor != c.leftBorderLineColor) {
            return new Coloring(newFont, newFontMode, newForeColor, newBackColor, newUnderlineColor, newStrikeThroughColor, newWaveUnderlineColor, newTopBorderLineColor, newRightBorderLineColor, newBottomBorderLineColor, newLeftBorderLineColor);
        }
        return c;
    }

    public boolean equals(Object o) {
        if (o instanceof Coloring) {
            Coloring c = (Coloring)o;
            return (this.font == null && c.font == null || this.font != null && this.font.equals(c.font)) && this.fontMode == c.fontMode && (this.foreColor == null && c.foreColor == null || this.foreColor != null && this.foreColor.equals(c.foreColor)) && (this.backColor == null && c.backColor == null || this.backColor != null && this.backColor.equals(c.backColor)) && (this.underlineColor == null && c.underlineColor == null || this.underlineColor != null && this.underlineColor.equals(c.underlineColor)) && (this.waveUnderlineColor == null && c.waveUnderlineColor == null || this.waveUnderlineColor != null && this.waveUnderlineColor.equals(c.waveUnderlineColor)) && (this.strikeThroughColor == null && c.strikeThroughColor == null || this.strikeThroughColor != null && this.strikeThroughColor.equals(c.strikeThroughColor)) && (this.topBorderLineColor == null && c.topBorderLineColor == null || this.topBorderLineColor != null && this.topBorderLineColor.equals(c.topBorderLineColor)) && (this.rightBorderLineColor == null && c.rightBorderLineColor == null || this.rightBorderLineColor != null && this.rightBorderLineColor.equals(c.rightBorderLineColor)) && (this.bottomBorderLineColor == null && c.bottomBorderLineColor == null || this.bottomBorderLineColor != null && this.bottomBorderLineColor.equals(c.bottomBorderLineColor)) && (this.leftBorderLineColor == null && c.leftBorderLineColor == null || this.leftBorderLineColor != null && this.leftBorderLineColor.equals(c.leftBorderLineColor));
        }
        return false;
    }

    public int hashCode() {
        int hash = 0;
        hash += this.font != null ? this.font.hashCode() : 0;
        hash += this.fontMode;
        hash += this.foreColor != null ? this.foreColor.hashCode() : 0;
        hash += this.backColor != null ? this.backColor.hashCode() : 0;
        hash += this.underlineColor != null ? this.underlineColor.hashCode() : 0;
        hash += this.waveUnderlineColor != null ? this.waveUnderlineColor.hashCode() : 0;
        hash += this.strikeThroughColor != null ? this.strikeThroughColor.hashCode() : 0;
        hash += this.topBorderLineColor != null ? this.topBorderLineColor.hashCode() : 0;
        hash += this.rightBorderLineColor != null ? this.rightBorderLineColor.hashCode() : 0;
        hash += this.bottomBorderLineColor != null ? this.bottomBorderLineColor.hashCode() : 0;
        return hash += this.leftBorderLineColor != null ? this.leftBorderLineColor.hashCode() : 0;
    }

    public static Coloring changeFont(Coloring c, Font newFont) {
        return Coloring.changeFont(c, newFont, c.getFontMode());
    }

    public static Coloring changeFont(Coloring c, Font newFont, int newFontMode) {
        if (newFont == null && c.font == null || newFont != null && newFont.equals(c.font) && c.fontMode == newFontMode) {
            return c;
        }
        return new Coloring(newFont, c.foreColor, c.backColor);
    }

    public static Coloring changeForeColor(Coloring c, Color newForeColor) {
        if (newForeColor == null && c.foreColor == null || newForeColor != null && newForeColor.equals(c.foreColor)) {
            return c;
        }
        return new Coloring(c.font, newForeColor, c.backColor);
    }

    public static Coloring changeBackColor(Coloring c, Color newBackColor) {
        if (newBackColor == null && c.backColor == null || newBackColor != null && newBackColor.equals(c.backColor)) {
            return c;
        }
        return new Coloring(c.font, c.foreColor, newBackColor);
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        if (this.fontMode == 0) {
            this.fontMode = 7;
        }
        this.checkCaches();
    }

    public String toString() {
        return "font=" + this.font + ", fontMode=" + this.fontMode + ", foreColor=" + this.foreColor + ", backColor=" + this.backColor + ", underlineColor=" + this.underlineColor + ", waveUnderlineColor=" + this.waveUnderlineColor + ", strikeThroughColor=" + this.strikeThroughColor + ", topBorderLineColor=" + this.topBorderLineColor + ", rightBorderLineColor=" + this.rightBorderLineColor + ", bottomBorderLineColor=" + this.bottomBorderLineColor + ", leftBorderLineColor=" + this.leftBorderLineColor;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Coloring fromAttributeSet(AttributeSet as) {
        WeakHashMap<AttributeSet, Coloring> weakHashMap = colorings;
        synchronized (weakHashMap) {
            Coloring coloring = colorings.get(as);
            if (coloring == null) {
                Object[] fontObj = Coloring.toFont(as);
                Color foreColor = (Color)as.getAttribute(StyleConstants.Foreground);
                Object strikeThrough = as.getAttribute(StyleConstants.StrikeThrough);
                if (strikeThrough instanceof Boolean) {
                    strikeThrough = Boolean.TRUE.equals(strikeThrough) ? foreColor : null;
                }
                coloring = new Coloring((Font)fontObj[0], (Integer)fontObj[1], foreColor, (Color)as.getAttribute(StyleConstants.Background), (Color)as.getAttribute(StyleConstants.Underline), (Color)strikeThrough, (Color)as.getAttribute(EditorStyleConstants.WaveUnderlineColor), (Color)as.getAttribute(EditorStyleConstants.TopBorderLineColor), (Color)as.getAttribute(EditorStyleConstants.RightBorderLineColor), (Color)as.getAttribute(EditorStyleConstants.BottomBorderLineColor), (Color)as.getAttribute(EditorStyleConstants.LeftBorderLineColor));
                colorings.put(as, coloring);
            }
            return coloring;
        }
    }

    private static Object[] toFont(AttributeSet as) {
        int applyMode = 0;
        String fontFamily = null;
        Object fontFamilyObj = as.getAttribute(StyleConstants.FontFamily);
        if (fontFamilyObj instanceof String) {
            fontFamily = (String)fontFamilyObj;
            ++applyMode;
        }
        int fontSize = 0;
        Object fontSizeObj = as.getAttribute(StyleConstants.FontSize);
        if (fontSizeObj instanceof Integer) {
            fontSize = (Integer)fontSizeObj;
            applyMode += 4;
        }
        int style = 0;
        Object boldStyleObj = as.getAttribute(StyleConstants.Bold);
        Object italicStyleObj = as.getAttribute(StyleConstants.Italic);
        if (boldStyleObj != null || italicStyleObj != null) {
            if (Boolean.TRUE.equals(boldStyleObj)) {
                ++style;
            }
            if (Boolean.TRUE.equals(italicStyleObj)) {
                style += 2;
            }
            applyMode += 2;
        }
        return new Object[]{new Font(fontFamily, style, fontSize), new Integer(applyMode)};
    }

    static {
        ColoringAccessor.register(new Accessor());
    }

    private static final class Accessor
    extends ColoringAccessor {
        private Accessor() {
        }

        @Override
        public void apply(Coloring c, DrawContext ctx) {
            c.apply(ctx);
        }
    }

}

