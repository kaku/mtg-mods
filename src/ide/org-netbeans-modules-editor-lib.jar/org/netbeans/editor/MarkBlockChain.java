/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.text.BadLocationException;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.MarkBlock;
import org.netbeans.editor.Utilities;

public class MarkBlockChain {
    public static final String PROP_BLOCKS_CHANGED = "MarkBlockChain.PROP_BLOCKS_CHANGED";
    protected MarkBlock chain;
    protected MarkBlock currentBlock;
    protected BaseDocument doc;
    private final PropertyChangeSupport PCS;

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.PCS.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.PCS.removePropertyChangeListener(l);
    }

    public MarkBlockChain(BaseDocument doc) {
        this.PCS = new PropertyChangeSupport(this);
        this.doc = doc;
    }

    public final MarkBlock getChain() {
        return this.chain;
    }

    public synchronized int compareBlock(int startPos, int endPos) {
        int rel;
        if (this.currentBlock == null) {
            this.currentBlock = this.chain;
            if (this.currentBlock == null) {
                return 0;
            }
        }
        boolean afterPrev = false;
        boolean beforeNext = false;
        boolean cont = false;
        MarkBlock contBlk = null;
        int contRel = 0;
        do {
            if (((rel = this.currentBlock.compare(startPos, endPos)) & 1) != 0) {
                return rel;
            }
            if ((rel & 128) != 0) {
                if (beforeNext) {
                    if (!cont || (rel & 2) != 0) {
                        return rel;
                    }
                    this.currentBlock = contBlk;
                    return contRel;
                }
                if (this.currentBlock.next != null) {
                    afterPrev = true;
                    boolean bl = cont = (rel & 2) != 0;
                    if (cont) {
                        contRel = rel;
                        contBlk = this.currentBlock;
                    }
                    this.currentBlock = this.currentBlock.next;
                    continue;
                }
                return rel;
            }
            if (afterPrev) {
                if (!cont || (rel & 17) != 0) {
                    return rel;
                }
                this.currentBlock = contBlk;
                return contRel;
            }
            if (this.currentBlock.prev == null) break;
            beforeNext = true;
            boolean bl = cont = (rel & 2) != 0;
            if (cont) {
                contRel = rel;
                contBlk = this.currentBlock;
            }
            this.currentBlock = this.currentBlock.prev;
        } while (true);
        return rel;
    }

    public void removeEmptyBlocks() {
        try {
            int startPos = Integer.MAX_VALUE;
            int endPos = Integer.MIN_VALUE;
            MarkBlock blk = this.chain;
            while (blk != null) {
                if (blk.startMark.getOffset() == blk.endMark.getOffset()) {
                    if (startPos > blk.startMark.getOffset()) {
                        startPos = blk.startMark.getOffset();
                    }
                    if (endPos < blk.endMark.getOffset()) {
                        endPos = blk.endMark.getOffset();
                    }
                    blk = this.checkedRemove(blk);
                    continue;
                }
                blk = blk.next;
            }
            this.PCS.firePropertyChange("MarkBlockChain.PROP_BLOCKS_CHANGED", startPos, endPos);
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
    }

    protected MarkBlock createBlock(int startPos, int endPos) throws BadLocationException {
        return new MarkBlock(this.doc, this.createBlockStartMark(), this.createBlockEndMark(), startPos, endPos);
    }

    protected Mark createBlockStartMark() {
        return new Mark();
    }

    protected Mark createBlockEndMark() {
        return new Mark();
    }

    private void removeCurrentIfEmpty() {
        try {
            while (this.currentBlock != null && this.currentBlock.startMark.getOffset() >= this.currentBlock.endMark.getOffset()) {
                this.checkedRemove(this.currentBlock);
                this.currentBlock = this.chain;
            }
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public synchronized void addBlock(int startPos, int endPos, boolean concat) {
        if (startPos == endPos) {
            return;
        }
        this.removeCurrentIfEmpty();
        try {
            rel = this.compareBlock(startPos, endPos) & -13;
            if ((rel & 64) == 0) ** GOTO lbl15
            if (!concat || rel != 66) ** GOTO lbl10
            this.currentBlock.startMark.move(this.doc, startPos);
            ** GOTO lbl38
lbl10: // 1 sources:
            first = this.currentBlock == this.chain;
            blk = this.currentBlock.insertChain(this.createBlock(startPos, endPos));
            if (!first) ** GOTO lbl38
            this.chain = blk;
            ** GOTO lbl38
lbl15: // 1 sources:
            if ((rel & 128) != 0) {
                if (concat && rel == 130) {
                    this.currentBlock.endMark.move(this.doc, endPos);
                } else {
                    this.currentBlock.addChain(this.createBlock(startPos, endPos));
                }
            } else if (this.currentBlock == null) {
                this.chain = this.createBlock(startPos, endPos);
            } else {
                this.currentBlock.extendStart(startPos);
                this.currentBlock.extendEnd(endPos);
                blk = this.chain;
                while (blk != null) {
                    if (blk != this.currentBlock) {
                        if (this.currentBlock.extend(blk, concat)) {
                            tempCurBlk = this.currentBlock;
                            blk = this.checkedRemove(blk);
                            this.currentBlock = tempCurBlk;
                            continue;
                        }
                        blk = blk.next;
                        continue;
                    }
                    blk = blk.next;
                }
            }
lbl38: // 7 sources:
            this.PCS.firePropertyChange("MarkBlockChain.PROP_BLOCKS_CHANGED", startPos, endPos);
            return;
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            return;
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeBlock(int startPos, int endPos) {
        if (startPos == endPos) {
            return;
        }
        try {
            MarkBlockChain markBlockChain = this;
            synchronized (markBlockChain) {
                int rel;
                while (((rel = this.compareBlock(startPos, endPos)) & 1) != 0) {
                    if ((rel & 8) != 0) {
                        this.checkedRemove(this.currentBlock);
                        continue;
                    }
                    switch (this.currentBlock.shrink(startPos, endPos)) {
                        case 4129: {
                            int endMarkPos = this.currentBlock.endMark.getOffset();
                            this.currentBlock.endMark.move(this.doc, startPos);
                            this.currentBlock.addChain(this.createBlock(endPos, endMarkPos));
                            return;
                        }
                        case 257: 
                        case 1057: {
                            this.currentBlock.startMark.move(this.doc, endPos);
                            return;
                        }
                        case 513: 
                        case 2081: {
                            this.currentBlock.endMark.move(this.doc, startPos);
                            return;
                        }
                    }
                    this.checkedRemove(this.currentBlock);
                }
            }
            this.PCS.firePropertyChange("MarkBlockChain.PROP_BLOCKS_CHANGED", startPos, endPos);
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
    }

    protected synchronized MarkBlock checkedRemove(MarkBlock blk) {
        boolean first = blk == this.chain;
        blk = blk.removeChain();
        if (first) {
            this.chain = blk;
        }
        this.currentBlock = null;
        return blk;
    }

    public synchronized int adjustToBlockEnd(int pos) {
        int rel = this.compareBlock(pos, pos) & -13;
        if (rel == 1057 || rel == 4129) {
            pos = this.currentBlock.getEndOffset();
        }
        return pos;
    }

    public synchronized int adjustToNextBlockStart(int pos) {
        MarkBlock nextBlk;
        int rel = this.compareBlock(pos, pos) & -13;
        pos = (rel & 64) != 0 ? this.currentBlock.getStartOffset() : (this.currentBlock != null ? ((nextBlk = this.currentBlock.getNext()) != null ? nextBlk.getStartOffset() : -1) : -1);
        return pos;
    }

    public synchronized String toString() {
        return "MarkBlockChain: currentBlock=" + this.currentBlock + "\nblock chain: " + (this.chain != null ? new StringBuilder().append("\n").append(this.chain.toStringChain()).toString() : " Empty");
    }
}

