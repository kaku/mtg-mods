/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

public class InvalidMarkException
extends Exception {
    static final long serialVersionUID = -7408566695283816594L;

    InvalidMarkException() {
    }

    InvalidMarkException(String s) {
        super(s);
    }
}

