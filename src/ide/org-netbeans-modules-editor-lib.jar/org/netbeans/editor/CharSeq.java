/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

public interface CharSeq {
    public int length();

    public char charAt(int var1);
}

