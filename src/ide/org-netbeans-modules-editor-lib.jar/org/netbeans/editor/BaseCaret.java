/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchyEvent
 *  org.netbeans.api.editor.fold.FoldHierarchyListener
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.RectangularSelectionTransferHandler
 *  org.netbeans.modules.editor.lib2.RectangularSelectionUtils
 *  org.netbeans.modules.editor.lib2.view.DocumentView
 *  org.netbeans.modules.editor.lib2.view.LockedViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyListener
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.AtomicLockEvent;
import org.netbeans.editor.AtomicLockListener;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WeakTimerListener;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.netbeans.modules.editor.lib2.RectangularSelectionTransferHandler;
import org.netbeans.modules.editor.lib2.RectangularSelectionUtils;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyListener;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public class BaseCaret
implements Caret,
MouseListener,
MouseMotionListener,
PropertyChangeListener,
DocumentListener,
ActionListener,
AtomicLockListener,
FoldHierarchyListener {
    public static final String BLOCK_CARET = "block-caret";
    public static final String LINE_CARET = "line-caret";
    public static final String THIN_LINE_CARET = "thin-line-caret";
    public static final String THICK_LINE_CARET = "thick-line-caret";
    private static final String RECTANGULAR_SELECTION_PROPERTY = "rectangular-selection";
    private static final String RECTANGULAR_SELECTION_REGIONS_PROPERTY = "rectangular-selection-regions";
    private static final Logger LOG = Logger.getLogger(BaseCaret.class.getName());
    private static final Logger LOG_EDT = Logger.getLogger(BaseCaret.class.getName() + ".EDT");
    private final ListenerImpl listenerImpl;
    private volatile Rectangle caretBounds;
    protected JTextComponent component;
    Point magicCaretPosition;
    Position caretPos;
    Position markPos;
    boolean caretVisible;
    boolean blinkVisible;
    boolean selectionVisible;
    protected EventListenerList listenerList = new EventListenerList();
    protected Timer flasher;
    String type;
    int width;
    boolean italic;
    private int[] xPoints = new int[4];
    private int[] yPoints = new int[4];
    private Action selectWordAction;
    private Action selectLineAction;
    protected ChangeEvent changeEvent;
    protected char[] dotChar = new char[]{' '};
    private boolean overwriteMode;
    private BaseDocument listenDoc;
    protected Font afterCaretFont;
    protected Font beforeCaretFont;
    protected Color textForeColor;
    protected Color textBackColor;
    private transient boolean inAtomicLock = false;
    private transient boolean inAtomicUnlock = false;
    private transient boolean modified;
    private transient int undoOffset = -1;
    static final long serialVersionUID = -9113841520331402768L;
    private boolean updateAfterFoldHierarchyChange;
    private boolean typingModificationOccurred;
    private Preferences prefs = null;
    private final PreferenceChangeListener prefsListener;
    private PreferenceChangeListener weakPrefsListener;
    private boolean caretUpdatePending;
    private MouseState mouseState;
    private int minSelectionStartOffset;
    private int minSelectionEndOffset;
    private boolean rectangularSelection;
    private Rectangle rsDotRect;
    private Rectangle rsMarkRect;
    private Rectangle rsPaintRect;
    private List<Position> rsRegions;
    private boolean showingTextCursor;

    public BaseCaret() {
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String setingName;
                String string = setingName = evt == null ? null : evt.getKey();
                if (setingName == null || "caret-blink-rate".equals(setingName)) {
                    SettingsConversions.callSettingsChange(BaseCaret.this);
                    int rate = BaseCaret.this.prefs.getInt("caret-blink-rate", -1);
                    if (rate == -1) {
                        JTextComponent c = BaseCaret.this.component;
                        Integer rateI = c == null ? null : (Integer)c.getClientProperty("nbeditor-default-swing-caret-blink-rate");
                        rate = rateI != null ? rateI : 300;
                    }
                    BaseCaret.this.setBlinkRate(rate);
                    BaseCaret.this.refresh();
                }
            }
        };
        this.weakPrefsListener = null;
        this.mouseState = MouseState.DEFAULT;
        this.showingTextCursor = true;
        this.listenerImpl = new ListenerImpl();
    }

    void updateType() {
        JTextComponent c = this.component;
        if (c != null && this.prefs != null && !Boolean.TRUE.equals(c.getClientProperty("AsTextField"))) {
            String newType;
            boolean newItalic;
            int newWidth = 0;
            Color caretColor = Color.black;
            if (this.overwriteMode) {
                newType = this.prefs.get("caret-type-overwrite-mode", "block-caret");
                newItalic = this.prefs.getBoolean("caret-italic-overwrite-mode", false);
            } else {
                newType = this.prefs.get("caret-type-insert-mode", "line-caret");
                newItalic = this.prefs.getBoolean("caret-italic-insert-mode", false);
                newWidth = this.prefs.getInt("thick-caret-width", 5);
            }
            FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)c)).lookup(FontColorSettings.class);
            if (fcs != null) {
                if (this.overwriteMode) {
                    AttributeSet attribs = fcs.getFontColors("caret-color-overwrite-mode");
                    if (attribs != null) {
                        caretColor = (Color)attribs.getAttribute(StyleConstants.Foreground);
                    }
                } else {
                    AttributeSet attribs = fcs.getFontColors("caret-color-insert-mode");
                    if (attribs != null) {
                        caretColor = (Color)attribs.getAttribute(StyleConstants.Foreground);
                    }
                }
            }
            this.type = newType;
            this.italic = newItalic;
            this.width = newWidth;
            c.setCaretColor(caretColor);
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("Updating caret color:" + caretColor + '\n');
            }
            this.resetBlink();
            this.dispatchUpdate(false);
        }
    }

    private boolean updateCaretBounds() {
        final JTextComponent c = this.component;
        final boolean[] ret = new boolean[]{false};
        if (c != null) {
            final Document doc = c.getDocument();
            doc.render(new Runnable(){

                @Override
                public void run() {
                    Rectangle newCaretBounds;
                    int offset = BaseCaret.this.getDot();
                    if (offset > doc.getLength()) {
                        offset = doc.getLength();
                    }
                    if (doc != null) {
                        CharSequence docText = DocumentUtilities.getText((Document)doc);
                        BaseCaret.this.dotChar[0] = docText.charAt(offset);
                    }
                    try {
                        DocumentView docView = DocumentView.get((JTextComponent)c);
                        if (docView != null) {
                            // empty if block
                        }
                        if ((newCaretBounds = c.getUI().modelToView(c, offset, Position.Bias.Forward)) != null) {
                            newCaretBounds.width = Math.max(newCaretBounds.width, 2);
                        }
                    }
                    catch (BadLocationException e) {
                        newCaretBounds = null;
                        Utilities.annotateLoggable(e);
                    }
                    if (newCaretBounds != null) {
                        LOG.log(Level.FINE, "updateCaretBounds: old={0}, new={1}, offset={2}", new Object[]{BaseCaret.this.caretBounds, newCaretBounds, offset});
                        BaseCaret.this.caretBounds = newCaretBounds;
                        ret[0] = true;
                    }
                }
            });
        }
        LOG.log(Level.FINE, "updateCaretBounds: no change, old={0}", this.caretBounds);
        return ret[0];
    }

    @Override
    public void install(JTextComponent c) {
        assert (SwingUtilities.isEventDispatchThread());
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Installing to " + BaseCaret.s2s(c));
        }
        this.component = c;
        this.blinkVisible = true;
        BaseDocument doc = Utilities.getDocument(c);
        if (doc != null) {
            this.modelChanged(null, doc);
        }
        this.updateCaretBounds();
        if (this.caretBounds == null) {
            this.component.addComponentListener(this.listenerImpl);
        }
        this.component.addPropertyChangeListener(this);
        this.component.addFocusListener(this.listenerImpl);
        this.component.addMouseListener(this);
        this.component.addMouseMotionListener(this);
        ViewHierarchy.get((JTextComponent)this.component).addViewHierarchyListener((ViewHierarchyListener)this.listenerImpl);
        EditorUI editorUI = Utilities.getEditorUI(this.component);
        editorUI.addPropertyChangeListener(this);
        if (this.component.hasFocus()) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Component has focus, calling BaseCaret.focusGained(); doc=" + this.component.getDocument().getProperty("title") + '\n');
            }
            this.listenerImpl.focusGained(null);
        }
        this.dispatchUpdate(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void deinstall(JTextComponent c) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Deinstalling from " + BaseCaret.s2s(c));
        }
        this.component = null;
        this.caretBounds = null;
        BaseCaret baseCaret = this;
        synchronized (baseCaret) {
            ListenerImpl listenerImpl = this.listenerImpl;
            synchronized (listenerImpl) {
                if (this.flasher != null) {
                    this.setBlinkRate(0);
                }
            }
        }
        c.removeComponentListener(this.listenerImpl);
        c.removePropertyChangeListener(this);
        c.removeFocusListener(this.listenerImpl);
        c.removeMouseListener(this);
        c.removeMouseMotionListener(this);
        ViewHierarchy.get((JTextComponent)c).removeViewHierarchyListener((ViewHierarchyListener)this.listenerImpl);
        EditorUI editorUI = Utilities.getEditorUI(c);
        editorUI.removePropertyChangeListener(this);
        this.modelChanged(this.listenDoc, null);
    }

    protected void modelChanged(BaseDocument oldDoc, BaseDocument newDoc) {
        if (oldDoc != null) {
            assert (oldDoc == this.listenDoc);
            DocumentUtilities.removeDocumentListener((Document)oldDoc, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.CARET_UPDATE);
            oldDoc.removeAtomicLockListener(this);
            this.caretPos = null;
            this.markPos = null;
            this.listenDoc = null;
            if (this.prefs != null && this.weakPrefsListener != null) {
                this.prefs.removePreferenceChangeListener(this.weakPrefsListener);
            }
        }
        if (newDoc != null) {
            DocumentUtilities.addDocumentListener((Document)newDoc, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.CARET_UPDATE);
            this.listenDoc = newDoc;
            newDoc.addAtomicLockListener(this);
            this.prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((Document)newDoc)).lookup(Preferences.class);
            if (this.prefs != null) {
                this.weakPrefsListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs);
                this.prefs.addPreferenceChangeListener(this.weakPrefsListener);
            }
            Utilities.runInEventDispatchThread(new Runnable(){

                @Override
                public void run() {
                    BaseCaret.this.updateType();
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g) {
        JTextComponent c = this.component;
        if (c == null) {
            return;
        }
        if (this.getDot() != 0 && this.caretBounds == null) {
            this.update(true);
        }
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("BaseCaret.paint(): caretBounds=" + this.caretBounds + this.dumpVisibility() + '\n');
        }
        if (this.caretBounds != null && this.isVisible() && this.blinkVisible) {
            this.paintCustomCaret(g);
        }
        if (this.rectangularSelection && this.rsPaintRect != null && g instanceof Graphics2D) {
            Graphics2D g2d = (Graphics2D)g;
            BasicStroke stroke = new BasicStroke(1.0f, 0, 2, 0.0f, new float[]{4.0f, 2.0f}, 0.0f);
            Stroke origStroke = g2d.getStroke();
            Color origColor = g2d.getColor();
            try {
                Color selColor = c.getSelectionColor();
                g2d.setColor(selColor);
                Composite origComposite = g2d.getComposite();
                try {
                    g2d.setComposite(AlphaComposite.SrcOver.derive(0.2f));
                    g2d.fill(this.rsPaintRect);
                }
                finally {
                    g2d.setComposite(origComposite);
                }
                g.setColor(c.getCaretColor());
                g2d.setStroke(stroke);
                Rectangle onePointSmallerRect = new Rectangle(this.rsPaintRect);
                --onePointSmallerRect.width;
                --onePointSmallerRect.height;
                g2d.draw(onePointSmallerRect);
            }
            finally {
                g2d.setStroke(origStroke);
                g2d.setColor(origColor);
            }
        }
    }

    protected void paintCustomCaret(Graphics g) {
        JTextComponent c = this.component;
        if (c != null) {
            EditorUI editorUI = Utilities.getEditorUI(c);
            g.setColor(c.getCaretColor());
            if ("thin-line-caret".equals(this.type)) {
                int upperX = this.caretBounds.x;
                if (this.beforeCaretFont != null && this.beforeCaretFont.isItalic() && this.italic) {
                    upperX = (int)((double)upperX + Math.tan(this.beforeCaretFont.getItalicAngle()) * (double)this.caretBounds.height);
                }
                g.drawLine(upperX, this.caretBounds.y, this.caretBounds.x, this.caretBounds.y + this.caretBounds.height - 1);
            } else if ("thick-line-caret".equals(this.type)) {
                Color textBackgroundColor;
                int blkWidth = this.width;
                if (blkWidth <= 0) {
                    blkWidth = 5;
                }
                if (this.afterCaretFont != null) {
                    g.setFont(this.afterCaretFont);
                }
                if ((textBackgroundColor = c.getBackground()) != null) {
                    g.setXORMode(textBackgroundColor);
                }
                g.fillRect(this.caretBounds.x, this.caretBounds.y, blkWidth, this.caretBounds.height - 1);
            } else if ("block-caret".equals(this.type)) {
                if (this.afterCaretFont != null) {
                    g.setFont(this.afterCaretFont);
                }
                if (this.afterCaretFont != null && this.afterCaretFont.isItalic() && this.italic) {
                    int upperX;
                    this.xPoints[0] = upperX = (int)((double)this.caretBounds.x + Math.tan(this.afterCaretFont.getItalicAngle()) * (double)this.caretBounds.height);
                    this.yPoints[0] = this.caretBounds.y;
                    this.xPoints[1] = upperX + this.caretBounds.width;
                    this.yPoints[1] = this.caretBounds.y;
                    this.xPoints[2] = this.caretBounds.x + this.caretBounds.width;
                    this.yPoints[2] = this.caretBounds.y + this.caretBounds.height - 1;
                    this.xPoints[3] = this.caretBounds.x;
                    this.yPoints[3] = this.caretBounds.y + this.caretBounds.height - 1;
                    g.fillPolygon(this.xPoints, this.yPoints, 4);
                } else {
                    g.fillRect(this.caretBounds.x, this.caretBounds.y, this.caretBounds.width, this.caretBounds.height);
                }
                if (!Character.isWhitespace(this.dotChar[0])) {
                    Color textBackgroundColor = c.getBackground();
                    if (textBackgroundColor != null) {
                        g.setColor(textBackgroundColor);
                    }
                    g.drawChars(this.dotChar, 0, 1, this.caretBounds.x, this.caretBounds.y + editorUI.getLineAscent());
                }
            } else {
                int blkWidth = 2;
                if (this.beforeCaretFont != null && this.beforeCaretFont.isItalic() && this.italic) {
                    int upperX;
                    this.xPoints[0] = upperX = (int)((double)this.caretBounds.x + Math.tan(this.beforeCaretFont.getItalicAngle()) * (double)this.caretBounds.height);
                    this.yPoints[0] = this.caretBounds.y;
                    this.xPoints[1] = upperX + blkWidth;
                    this.yPoints[1] = this.caretBounds.y;
                    this.xPoints[2] = this.caretBounds.x + blkWidth;
                    this.yPoints[2] = this.caretBounds.y + this.caretBounds.height - 1;
                    this.xPoints[3] = this.caretBounds.x;
                    this.yPoints[3] = this.caretBounds.y + this.caretBounds.height - 1;
                    g.fillPolygon(this.xPoints, this.yPoints, 4);
                } else {
                    g.fillRect(this.caretBounds.x, this.caretBounds.y, blkWidth, this.caretBounds.height - 1);
                }
            }
        }
    }

    void dispatchUpdate(final boolean scrollViewToCaret) {
        Utilities.runInEventDispatchThread(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                BaseDocument doc;
                JTextComponent c = BaseCaret.this.component;
                if (c != null && (doc = Utilities.getDocument(c)) != null) {
                    doc.readLock();
                    try {
                        BaseCaret.this.update(scrollViewToCaret);
                    }
                    finally {
                        doc.readUnlock();
                    }
                }
            }
        });
    }

    protected void update(boolean scrollViewToCaret) {
        this.caretUpdatePending = false;
        JTextComponent c = this.component;
        if (c != null) {
            if (!c.isValid()) {
                c.validate();
            }
            BaseTextUI ui = (BaseTextUI)c.getUI();
            BaseDocument doc = Utilities.getDocument(c);
            if (doc != null) {
                Rectangle oldCaretBounds = this.caretBounds;
                if (oldCaretBounds != null) {
                    if (this.italic) {
                        oldCaretBounds.width += oldCaretBounds.height;
                    }
                    c.repaint(oldCaretBounds);
                }
                if (this.updateCaretBounds() || this.updateAfterFoldHierarchyChange) {
                    JScrollBar hScrollBar;
                    Container scrollPane;
                    Container viewport;
                    Rectangle scrollBounds = new Rectangle(this.caretBounds);
                    if (oldCaretBounds == null && (viewport = c.getParent()) instanceof JViewport && (scrollPane = viewport.getParent()) instanceof JScrollPane && (hScrollBar = ((JScrollPane)scrollPane).getHorizontalScrollBar()) != null) {
                        int hScrollBarHeight = hScrollBar.getPreferredSize().height;
                        Dimension extentSize = ((JViewport)viewport).getExtentSize();
                        if (extentSize.height >= this.caretBounds.height + hScrollBarHeight) {
                            scrollBounds.height += hScrollBarHeight;
                        }
                    }
                    Rectangle visibleBounds = c.getVisibleRect();
                    boolean doScroll = scrollViewToCaret;
                    boolean explicit = false;
                    if (oldCaretBounds != null && (!scrollViewToCaret || this.updateAfterFoldHierarchyChange)) {
                        int oldRelY = oldCaretBounds.y - visibleBounds.y;
                        if (LOG.isLoggable(Level.FINER)) {
                            LOG.log(Level.FINER, "oldCaretBounds: {0}, visibleBounds: {1}, caretBounds: {2}", new Object[]{oldCaretBounds, visibleBounds, this.caretBounds});
                        }
                        if (oldRelY >= 0 && oldRelY < visibleBounds.height && (oldCaretBounds.y != this.caretBounds.y || oldCaretBounds.x != this.caretBounds.x)) {
                            doScroll = true;
                            explicit = true;
                            int oldRelX = oldCaretBounds.x - visibleBounds.x;
                            scrollBounds.y = Math.max(this.caretBounds.y - oldRelY, 0);
                            scrollBounds.height = visibleBounds.height;
                        }
                    }
                    if (scrollViewToCaret && !explicit && (this.caretBounds.y > visibleBounds.y + visibleBounds.height + this.caretBounds.height || this.caretBounds.y + this.caretBounds.height < visibleBounds.y - this.caretBounds.height)) {
                        scrollBounds.y -= (visibleBounds.height - this.caretBounds.height) / 2;
                        scrollBounds.height = visibleBounds.height;
                    }
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("Resetting fold flag, current: " + this.updateAfterFoldHierarchyChange);
                    }
                    this.updateAfterFoldHierarchyChange = false;
                    if (doScroll) {
                        if (LOG.isLoggable(Level.FINER)) {
                            LOG.finer("Scrolling to: " + scrollBounds);
                        }
                        c.scrollRectToVisible(scrollBounds);
                        if (!c.getVisibleRect().intersects(scrollBounds)) {
                            c.scrollRectToVisible(scrollBounds);
                        }
                    }
                    this.resetBlink();
                    c.repaint(this.caretBounds);
                }
            }
        }
    }

    private void updateSystemSelection() {
        Clipboard clip;
        if (this.getDot() != this.getMark() && this.component != null && (clip = this.getSystemSelection()) != null) {
            clip.setContents(new StringSelection(this.component.getSelectedText()), null);
        }
    }

    private Clipboard getSystemSelection() {
        return this.component.getToolkit().getSystemSelection();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateRectangularSelectionPositionBlocks() {
        JTextComponent c = this.component;
        if (this.rectangularSelection && this.listenDoc != null) {
            this.listenDoc.readLock();
            try {
                if (this.rsRegions == null) {
                    this.rsRegions = new ArrayList<Position>();
                    c.putClientProperty("rectangular-selection-regions", this.rsRegions);
                }
                List<Position> list = this.rsRegions;
                synchronized (list) {
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Rectangular-selection position regions:\n");
                    }
                    this.rsRegions.clear();
                    if (this.rsPaintRect != null) {
                        LockedViewHierarchy lvh = ViewHierarchy.get((JTextComponent)c).lock();
                        try {
                            float rowHeight = lvh.getDefaultRowHeight();
                            double y = this.rsPaintRect.y;
                            double maxY = y + (double)this.rsPaintRect.height;
                            double minX = this.rsPaintRect.getMinX();
                            double maxX = this.rsPaintRect.getMaxX();
                            do {
                                int endOffset;
                                int startOffset;
                                if ((startOffset = lvh.viewToModel(minX, y, null)) > (endOffset = lvh.viewToModel(maxX, y, null))) {
                                    int tmp = startOffset;
                                    startOffset = endOffset;
                                    endOffset = tmp;
                                }
                                Position startPos = this.listenDoc.createPosition(startOffset);
                                Position endPos = this.listenDoc.createPosition(endOffset);
                                this.rsRegions.add(startPos);
                                this.rsRegions.add(endPos);
                                if (!LOG.isLoggable(Level.FINE)) continue;
                                LOG.fine("  <" + startOffset + "," + endOffset + ">\n");
                            } while ((y += (double)rowHeight) < maxY);
                            c.putClientProperty("rectangular-selection-regions", this.rsRegions);
                        }
                        finally {
                            lvh.unlock();
                        }
                    }
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            finally {
                this.listenDoc.readUnlock();
            }
        }
    }

    public boolean equals(Object o) {
        return this == o;
    }

    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public void addChangeListener(ChangeListener l) {
        this.listenerList.add(ChangeListener.class, l);
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
        this.listenerList.remove(ChangeListener.class, l);
    }

    protected void fireStateChanged() {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                Object[] listeners = BaseCaret.this.listenerList.getListenerList();
                for (int i = listeners.length - 2; i >= 0; i -= 2) {
                    if (listeners[i] != ChangeListener.class) continue;
                    if (BaseCaret.this.changeEvent == null) {
                        BaseCaret.this.changeEvent = new ChangeEvent(BaseCaret.this);
                    }
                    ((ChangeListener)listeners[i + 1]).stateChanged(BaseCaret.this.changeEvent);
                }
            }
        };
        if (this.inAtomicUnlock) {
            SwingUtilities.invokeLater(runnable);
        } else {
            Utilities.runInEventDispatchThread(runnable);
        }
        this.updateSystemSelection();
    }

    @Override
    public final boolean isVisible() {
        return this.caretVisible;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void setVisibleImpl(boolean v) {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("BaseCaret.setVisible(" + v + ")\n");
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.INFO, "", new Exception());
            }
        }
        boolean visible = this.isVisible();
        BaseCaret baseCaret = this;
        synchronized (baseCaret) {
            ListenerImpl listenerImpl = this.listenerImpl;
            synchronized (listenerImpl) {
                if (this.flasher != null) {
                    if (visible) {
                        this.flasher.stop();
                    }
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer((v ? "Starting" : "Stopping") + " the caret blinking timer: " + this.dumpVisibility() + '\n');
                    }
                    if (v) {
                        this.flasher.start();
                    } else {
                        this.flasher.stop();
                    }
                }
            }
            this.caretVisible = v;
        }
        JTextComponent c = this.component;
        if (c != null && this.caretBounds != null) {
            Rectangle repaintRect = this.caretBounds;
            if (this.italic) {
                repaintRect = new Rectangle(repaintRect);
                repaintRect.width += repaintRect.height;
            }
            c.repaint(repaintRect);
        }
    }

    private String dumpVisibility() {
        return "visible=" + this.isVisible() + ", blinkVisible=" + this.blinkVisible;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void resetBlink() {
        BaseCaret baseCaret = this;
        synchronized (baseCaret) {
            boolean visible = this.isVisible();
            ListenerImpl listenerImpl = this.listenerImpl;
            synchronized (listenerImpl) {
                if (this.flasher != null) {
                    this.flasher.stop();
                    this.blinkVisible = true;
                    if (visible) {
                        if (LOG.isLoggable(Level.FINER)) {
                            LOG.finer("Reset blinking (caret already visible) - starting the caret blinking timer: " + this.dumpVisibility() + '\n');
                        }
                        this.flasher.start();
                    } else if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("Reset blinking (caret not visible) - caret blinking timer not started: " + this.dumpVisibility() + '\n');
                    }
                }
            }
        }
    }

    @Override
    public void setVisible(final boolean v) {
        Utilities.runInEventDispatchThread(new Runnable(){

            @Override
            public void run() {
                BaseCaret.this.setVisibleImpl(v);
            }
        });
    }

    @Override
    public final boolean isSelectionVisible() {
        return this.selectionVisible;
    }

    @Override
    public void setSelectionVisible(boolean v) {
        Document doc;
        if (this.selectionVisible == v) {
            return;
        }
        JTextComponent c = this.component;
        if (c != null && (doc = c.getDocument()) != null) {
            this.selectionVisible = v;
            final BaseTextUI ui = (BaseTextUI)c.getUI();
            doc.render(new Runnable(){

                @Override
                public void run() {
                    try {
                        ui.getEditorUI().repaintBlock(BaseCaret.this.getDot(), BaseCaret.this.getMark());
                    }
                    catch (BadLocationException e) {
                        Utilities.annotateLoggable(e);
                    }
                }
            });
        }
    }

    @Override
    public void setMagicCaretPosition(Point p) {
        this.magicCaretPosition = p;
    }

    @Override
    public final Point getMagicCaretPosition() {
        return this.magicCaretPosition;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public synchronized void setBlinkRate(int rate) {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("setBlinkRate(" + rate + ")" + this.dumpVisibility() + '\n');
        }
        ListenerImpl listenerImpl = this.listenerImpl;
        synchronized (listenerImpl) {
            if (this.flasher == null && rate > 0) {
                this.flasher = new Timer(rate, new WeakTimerListener(this));
            }
            if (this.flasher != null) {
                if (rate > 0) {
                    if (this.flasher.getDelay() != rate) {
                        this.flasher.setDelay(rate);
                    }
                } else {
                    this.flasher.stop();
                    this.flasher.removeActionListener(this);
                    this.flasher = null;
                    this.blinkVisible = true;
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("Zero blink rate - no blinking. flasher=null; blinkVisible=true");
                    }
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getBlinkRate() {
        BaseCaret baseCaret = this;
        synchronized (baseCaret) {
            ListenerImpl listenerImpl = this.listenerImpl;
            synchronized (listenerImpl) {
                return this.flasher != null ? this.flasher.getDelay() : 0;
            }
        }
    }

    @Override
    public int getDot() {
        if (this.component != null) {
            return this.caretPos != null ? this.caretPos.getOffset() : 0;
        }
        return 0;
    }

    @Override
    public int getMark() {
        if (this.component != null) {
            return this.markPos != null ? this.markPos.getOffset() : 0;
        }
        return 0;
    }

    @Override
    public void setDot(int offset) {
        this.setDot(offset, this.caretBounds, 0);
    }

    public void setDot(int offset, boolean expandFold) {
        this.setDot(offset, this.caretBounds, 0, expandFold);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setDot(int offset, Rectangle scrollRect, int scrollPolicy, boolean expandFold) {
        JTextComponent c;
        if (LOG_EDT.isLoggable(Level.FINE) && !SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("BaseCaret.setDot() not in EDT: offset=" + offset);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("setDot: offset=" + offset);
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.INFO, "setDot call stack", new Exception());
            }
        }
        if ((c = this.component) != null) {
            boolean dotChanged;
            block14 : {
                BaseDocument doc = (BaseDocument)c.getDocument();
                dotChanged = false;
                doc.readLock();
                try {
                    if (doc == null || offset < 0 || offset > doc.getLength()) break block14;
                    dotChanged = true;
                    try {
                        this.caretPos = doc.createPosition(offset);
                        this.markPos = doc.createPosition(offset);
                        Callable cc = (Callable)c.getClientProperty("org.netbeans.api.fold.expander");
                        if (cc != null && expandFold) {
                            try {
                                cc.call();
                            }
                            catch (Exception ex) {
                                Exceptions.printStackTrace((Throwable)ex);
                            }
                        }
                        if (this.rectangularSelection) {
                            this.setRectangularSelectionToDotAndMark();
                        }
                    }
                    catch (BadLocationException e) {
                        throw new IllegalStateException(e.toString());
                    }
                }
                finally {
                    doc.readUnlock();
                }
            }
            if (dotChanged) {
                this.fireStateChanged();
                this.dispatchUpdate(true);
            }
        }
    }

    public void setDot(int offset, Rectangle scrollRect, int scrollPolicy) {
        this.setDot(offset, scrollRect, scrollPolicy, true);
    }

    @Override
    public void moveDot(int offset) {
        this.moveDot(offset, this.caretBounds, 1);
    }

    public void moveDot(int offset, Rectangle scrollRect, int scrollPolicy) {
        JTextComponent c;
        if (LOG_EDT.isLoggable(Level.FINE) && !SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("BaseCaret.moveDot() not in EDT: offset=" + offset);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("moveDot: offset=" + offset);
        }
        if ((c = this.component) != null) {
            BaseDocument doc = (BaseDocument)c.getDocument();
            if (doc != null && offset >= 0 && offset <= doc.getLength()) {
                doc.readLock();
                try {
                    int oldCaretPos = this.getDot();
                    if (offset == oldCaretPos) {
                        return;
                    }
                    this.caretPos = doc.createPosition(offset);
                    if (this.selectionVisible) {
                        Utilities.getEditorUI(c).repaintBlock(oldCaretPos, offset);
                    }
                    if (this.rectangularSelection) {
                        Rectangle r = c.modelToView(offset);
                        if (this.rsDotRect != null) {
                            this.rsDotRect.y = r.y;
                            this.rsDotRect.height = r.height;
                        } else {
                            this.rsDotRect = r;
                        }
                        this.updateRectangularSelectionPaintRect();
                    }
                }
                catch (BadLocationException e) {
                    throw new IllegalStateException(e.toString());
                }
                finally {
                    doc.readUnlock();
                }
            }
            this.fireStateChanged();
            this.dispatchUpdate(true);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent evt) {
        JTextComponent c = this.component;
        if (c != null) {
            boolean typingModification;
            BaseDocumentEvent bevt;
            int offset = evt.getOffset();
            int endOffset = offset + evt.getLength();
            if (evt.getOffset() == 0) {
                if (this.getMark() == 0) {
                    try {
                        this.markPos = this.listenDoc.createPosition(endOffset);
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                if (this.getDot() == 0) {
                    try {
                        this.caretPos = this.listenDoc.createPosition(endOffset);
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
            }
            if (((bevt = (BaseDocumentEvent)evt).isInUndo() || bevt.isInRedo()) && this.component == Utilities.getLastActiveComponent() && !Boolean.TRUE.equals(DocumentUtilities.getEventProperty((DocumentEvent)evt, (Object)"caretIgnore"))) {
                this.undoOffset = evt.getOffset() + evt.getLength();
                typingModification = true;
            } else {
                this.undoOffset = -1;
                typingModification = DocumentUtilities.isTypingModification((Document)this.component.getDocument());
            }
            this.modified = true;
            this.modifiedUpdate(typingModification);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent evt) {
        JTextComponent c = this.component;
        if (c != null) {
            boolean typingModification;
            BaseDocumentEvent bevt = (BaseDocumentEvent)evt;
            if ((bevt.isInUndo() || bevt.isInRedo()) && c == Utilities.getLastActiveComponent() && !Boolean.TRUE.equals(DocumentUtilities.getEventProperty((DocumentEvent)evt, (Object)"caretIgnore"))) {
                this.undoOffset = evt.getOffset();
                typingModification = true;
            } else {
                this.undoOffset = -1;
                typingModification = DocumentUtilities.isTypingModification((Document)this.component.getDocument());
            }
            this.modified = true;
            this.modifiedUpdate(typingModification);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void modifiedUpdate(boolean typingModification) {
        if (!this.inAtomicLock) {
            JTextComponent c = this.component;
            if (this.modified && c != null) {
                if (this.undoOffset >= 0) {
                    this.setDot(this.undoOffset);
                } else {
                    BaseDocument doc = this.listenDoc;
                    if (doc != null) {
                        doc.readLock();
                        try {
                            this.updateRectangularSelectionPaintRect();
                        }
                        finally {
                            doc.readUnlock();
                        }
                    }
                    this.fireStateChanged();
                    this.dispatchUpdate(c.hasFocus() && typingModification);
                }
                this.modified = false;
            }
        } else {
            this.typingModificationOccurred |= typingModification;
        }
    }

    @Override
    public void atomicLock(AtomicLockEvent evt) {
        this.inAtomicLock = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void atomicUnlock(AtomicLockEvent evt) {
        this.inAtomicLock = false;
        this.inAtomicUnlock = true;
        try {
            this.modifiedUpdate(this.typingModificationOccurred);
        }
        finally {
            this.inAtomicUnlock = false;
            this.typingModificationOccurred = false;
        }
    }

    @Override
    public void changedUpdate(DocumentEvent evt) {
        if (evt == null) {
            this.dispatchUpdate(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        JTextComponent c;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("mousePressed: " + BaseCaret.logMouseEvent(evt) + ", state=" + (Object)((Object)this.mouseState) + '\n');
        }
        if ((c = this.component) != null && this.isLeftMouseButtonExt(evt)) {
            int offset = this.mouse2Offset(evt);
            switch (evt.getClickCount()) {
                case 1: {
                    if (c.isEnabled() && !c.hasFocus()) {
                        c.requestFocus();
                    }
                    c.setDragEnabled(true);
                    if (evt.isShiftDown()) {
                        this.moveDot(offset);
                        this.adjustRectangularSelectionMouseX(evt.getX(), evt.getY());
                        this.mouseState = MouseState.CHAR_SELECTION;
                        break;
                    }
                    if (this.isDragPossible(evt) && this.mapDragOperationFromModifiers(evt) != 0) {
                        this.mouseState = MouseState.DRAG_SELECTION_POSSIBLE;
                        break;
                    }
                    this.mouseState = MouseState.CHAR_SELECTION;
                    this.setDot(offset);
                    break;
                }
                case 2: {
                    this.mouseState = MouseState.WORD_SELECTION;
                    c.setDragEnabled(false);
                    try {
                        Callable cc = (Callable)c.getClientProperty("org.netbeans.api.fold.expander");
                        if (cc != null && cc.equals(this)) break;
                        if (this.selectWordAction == null) {
                            this.selectWordAction = ((BaseKit)c.getUI().getEditorKit(c)).getActionByName("select-word");
                        }
                        if (this.selectWordAction != null) {
                            this.selectWordAction.actionPerformed(null);
                        }
                        this.minSelectionStartOffset = this.getMark();
                        this.minSelectionEndOffset = this.getDot();
                    }
                    catch (Exception ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    break;
                }
                case 3: {
                    this.mouseState = MouseState.LINE_SELECTION;
                    c.setDragEnabled(false);
                    if (this.selectLineAction == null) {
                        this.selectLineAction = ((BaseKit)c.getUI().getEditorKit(c)).getActionByName("select-line");
                    }
                    if (this.selectLineAction == null) break;
                    this.selectLineAction.actionPerformed(null);
                    this.minSelectionStartOffset = this.getMark();
                    this.minSelectionEndOffset = this.getDot();
                    break;
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent evt) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("mouseReleased: " + BaseCaret.logMouseEvent(evt) + ", state=" + (Object)((Object)this.mouseState) + '\n');
        }
        int offset = this.mouse2Offset(evt);
        switch (this.mouseState) {
            case DRAG_SELECTION_POSSIBLE: {
                this.setDot(offset);
                this.adjustRectangularSelectionMouseX(evt.getX(), evt.getY());
                break;
            }
            case CHAR_SELECTION: {
                this.moveDot(offset);
                this.adjustRectangularSelectionMouseX(evt.getX(), evt.getY());
            }
        }
        this.mouseState = MouseState.DEFAULT;
        this.component.setDragEnabled(true);
    }

    int mouse2Offset(MouseEvent evt) {
        JTextComponent c = this.component;
        int offset = 0;
        if (c != null) {
            int y = evt.getY();
            offset = y < 0 ? 0 : ((double)y > c.getSize().getHeight() ? c.getDocument().getLength() : c.viewToModel(new Point(evt.getX(), evt.getY())));
        }
        return offset;
    }

    @Override
    public void mouseClicked(MouseEvent evt) {
        JTextComponent c;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("mouseClicked: " + BaseCaret.logMouseEvent(evt) + ", state=" + (Object)((Object)this.mouseState) + '\n');
        }
        if ((c = this.component) != null && this.isMiddleMouseButtonExt(evt) && evt.getClickCount() == 1) {
            if (c == null) {
                return;
            }
            Clipboard buffer = this.getSystemSelection();
            if (buffer == null) {
                return;
            }
            Transferable trans = buffer.getContents(null);
            if (trans == null) {
                return;
            }
            final BaseDocument doc = (BaseDocument)c.getDocument();
            if (doc == null) {
                return;
            }
            final int offset = ((BaseTextUI)c.getUI()).viewToModel(c, evt.getX(), evt.getY());
            try {
                final String pastingString = (String)trans.getTransferData(DataFlavor.stringFlavor);
                if (pastingString == null) {
                    return;
                }
                doc.runAtomicAsUser(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            doc.insertString(offset, pastingString, null);
                            BaseCaret.this.setDot(offset + pastingString.length());
                        }
                        catch (BadLocationException exc) {
                            // empty catch block
                        }
                    }
                });
            }
            catch (UnsupportedFlavorException ufe) {
            }
            catch (IOException ioe) {
                // empty catch block
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent evt) {
    }

    @Override
    public void mouseExited(MouseEvent evt) {
    }

    @Override
    public void mouseMoved(MouseEvent evt) {
        if (this.mouseState == MouseState.DEFAULT) {
            boolean textCursor = true;
            int position = this.component.viewToModel(evt.getPoint());
            if (RectangularSelectionUtils.isRectangularSelection((JComponent)this.component)) {
                List positions = RectangularSelectionUtils.regionsCopy((JComponent)this.component);
                for (int i = 0; textCursor && i < positions.size(); i += 2) {
                    int b;
                    int a = ((Position)positions.get(i)).getOffset();
                    if (a == (b = ((Position)positions.get(i + 1)).getOffset())) continue;
                    textCursor &= !(position >= a && position <= b || position >= b && position <= a);
                }
            } else if (this.getDot() == this.getMark()) {
                textCursor = true;
            } else {
                int dot = this.getDot();
                int mark = this.getMark();
                textCursor = !(position >= dot && position <= mark || position >= mark && position <= dot);
            }
            if (textCursor != this.showingTextCursor) {
                int cursorType = textCursor ? 2 : 0;
                this.component.setCursor(Cursor.getPredefinedCursor(cursorType));
                this.showingTextCursor = textCursor;
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent evt) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("mouseDragged: " + BaseCaret.logMouseEvent(evt) + ", state=" + (Object)((Object)this.mouseState) + '\n');
        }
        if (this.isLeftMouseButtonExt(evt)) {
            JTextComponent c = this.component;
            int offset = this.mouse2Offset(evt);
            int dot = this.getDot();
            int mark = this.getMark();
            try {
                switch (this.mouseState) {
                    case DEFAULT: 
                    case DRAG_SELECTION: {
                        break;
                    }
                    case DRAG_SELECTION_POSSIBLE: {
                        this.mouseState = MouseState.DRAG_SELECTION;
                        break;
                    }
                    case CHAR_SELECTION: {
                        this.moveDot(offset);
                        this.adjustRectangularSelectionMouseX(evt.getX(), evt.getY());
                        break;
                    }
                    case WORD_SELECTION: {
                        offset = offset >= mark ? Utilities.getWordEnd(c, offset) : Utilities.getWordStart(c, offset);
                        this.selectEnsureMinSelection(mark, dot, offset);
                        break;
                    }
                    case LINE_SELECTION: {
                        offset = offset >= mark ? Math.min(Utilities.getRowEnd(c, offset) + 1, c.getDocument().getLength()) : Utilities.getRowStart(c, offset);
                        this.selectEnsureMinSelection(mark, dot, offset);
                        break;
                    }
                    default: {
                        throw new AssertionError((Object)("Invalid state " + (Object)((Object)this.mouseState)));
                    }
                }
            }
            catch (BadLocationException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void adjustRectangularSelectionMouseX(int x, int y) {
        if (!this.rectangularSelection) {
            return;
        }
        JTextComponent c = this.component;
        int offset = c.viewToModel(new Point(x, y));
        Rectangle r = null;
        if (offset >= 0) {
            try {
                r = c.modelToView(offset);
            }
            catch (BadLocationException ex) {
                r = null;
            }
        }
        if (r != null) {
            float xDiff = x - r.x;
            if (xDiff > 0.0f) {
                float charWidth;
                LockedViewHierarchy lvh = ViewHierarchy.get((JTextComponent)c).lock();
                try {
                    charWidth = lvh.getDefaultCharWidth();
                }
                finally {
                    lvh.unlock();
                }
                int n = (int)(xDiff / charWidth);
                r.x = (int)((float)r.x + (float)n * charWidth);
                r.width = (int)charWidth;
            }
            this.rsDotRect.x = r.x;
            this.rsDotRect.width = r.width;
            this.updateRectangularSelectionPaintRect();
            this.fireStateChanged();
        }
    }

    void setRectangularSelectionToDotAndMark() {
        int dotOffset = this.getDot();
        int markOffset = this.getMark();
        try {
            this.rsDotRect = this.component.modelToView(dotOffset);
            this.rsMarkRect = this.component.modelToView(markOffset);
        }
        catch (BadLocationException ex) {
            this.rsMarkRect = null;
            this.rsDotRect = null;
        }
        this.updateRectangularSelectionPaintRect();
    }

    public void updateRectangularUpDownSelection() {
        JTextComponent c = this.component;
        int dotOffset = this.getDot();
        try {
            Rectangle r = c.modelToView(dotOffset);
            this.rsDotRect.y = r.y;
            this.rsDotRect.height = r.height;
        }
        catch (BadLocationException ex) {
            // empty catch block
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void extendRectangularSelection(boolean toRight, boolean ctrl) {
        float charWidth;
        JTextComponent c = this.component;
        Document doc = c.getDocument();
        int dotOffset = this.getDot();
        Element lineRoot = doc.getDefaultRootElement();
        int lineIndex = lineRoot.getElementIndex(dotOffset);
        Element lineElement = lineRoot.getElement(lineIndex);
        LockedViewHierarchy lvh = ViewHierarchy.get((JTextComponent)c).lock();
        try {
            charWidth = lvh.getDefaultCharWidth();
        }
        finally {
            lvh.unlock();
        }
        int newDotOffset = -1;
        try {
            int newlineOffset = lineElement.getEndOffset() - 1;
            Rectangle newlineRect = c.modelToView(newlineOffset);
            if (!ctrl) {
                if (toRight) {
                    if (this.rsDotRect.x < newlineRect.x) {
                        newDotOffset = dotOffset + 1;
                    } else {
                        this.rsDotRect.x = (int)((float)this.rsDotRect.x + charWidth);
                    }
                } else if (this.rsDotRect.x > newlineRect.x) {
                    this.rsDotRect.x = (int)((float)this.rsDotRect.x - charWidth);
                    if (this.rsDotRect.x < newlineRect.x) {
                        newDotOffset = newlineOffset;
                    }
                } else {
                    newDotOffset = Math.max(dotOffset - 1, lineElement.getStartOffset());
                }
            } else {
                int numVirtualChars = 8;
                if (toRight) {
                    if (this.rsDotRect.x < newlineRect.x) {
                        newDotOffset = Math.min(Utilities.getNextWord(c, dotOffset), lineElement.getEndOffset() - 1);
                    } else {
                        this.rsDotRect.x = (int)((float)this.rsDotRect.x + (float)numVirtualChars * charWidth);
                    }
                } else if (this.rsDotRect.x > newlineRect.x) {
                    this.rsDotRect.x = (int)((float)this.rsDotRect.x - (float)numVirtualChars * charWidth);
                    if (this.rsDotRect.x < newlineRect.x) {
                        newDotOffset = newlineOffset;
                    }
                } else {
                    newDotOffset = Math.max(Utilities.getPreviousWord(c, dotOffset), lineElement.getStartOffset());
                }
            }
            if (newDotOffset != -1) {
                this.rsDotRect = c.modelToView(newDotOffset);
                this.moveDot(newDotOffset);
            } else {
                this.updateRectangularSelectionPaintRect();
                this.fireStateChanged();
            }
        }
        catch (BadLocationException ex) {
            // empty catch block
        }
    }

    private void updateRectangularSelectionPaintRect() {
        JTextComponent c = this.component;
        Rectangle repaintRect = this.rsPaintRect;
        if (this.rsDotRect == null || this.rsMarkRect == null) {
            return;
        }
        Rectangle newRect = new Rectangle();
        if (this.rsDotRect.x < this.rsMarkRect.x) {
            newRect.x = this.rsDotRect.x;
            newRect.width = this.rsMarkRect.x - newRect.x;
        } else {
            newRect.x = this.rsMarkRect.x;
            newRect.width = this.rsDotRect.x - newRect.x;
        }
        if (this.rsDotRect.y < this.rsMarkRect.y) {
            newRect.y = this.rsDotRect.y;
            newRect.height = this.rsMarkRect.y + this.rsMarkRect.height - newRect.y;
        } else {
            newRect.y = this.rsMarkRect.y;
            newRect.height = this.rsDotRect.y + this.rsDotRect.height - newRect.y;
        }
        if (newRect.width < 2) {
            newRect.width = 2;
        }
        this.rsPaintRect = newRect;
        repaintRect = repaintRect == null ? this.rsPaintRect : repaintRect.union(this.rsPaintRect);
        c.repaint(repaintRect);
        this.updateRectangularSelectionPositionBlocks();
    }

    private void selectEnsureMinSelection(int mark, int dot, int newDot) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("selectEnsureMinSelection: mark=" + mark + ", dot=" + dot + ", newDot=" + newDot);
        }
        if (dot >= mark) {
            if (newDot >= mark) {
                this.moveDot(Math.max(newDot, this.minSelectionEndOffset));
            } else {
                this.setDot(this.minSelectionEndOffset);
                this.moveDot(Math.min(newDot, this.minSelectionStartOffset));
            }
        } else if (newDot <= mark) {
            this.moveDot(Math.min(newDot, this.minSelectionStartOffset));
        } else {
            this.setDot(this.minSelectionStartOffset);
            this.moveDot(Math.max(newDot, this.minSelectionEndOffset));
        }
    }

    private boolean isLeftMouseButtonExt(MouseEvent evt) {
        return SwingUtilities.isLeftMouseButton(evt) && !evt.isPopupTrigger() && (evt.getModifiers() & 12) == 0;
    }

    private boolean isMiddleMouseButtonExt(MouseEvent evt) {
        return evt.getButton() == 2 && (evt.getModifiersEx() & 8576) == 0;
    }

    protected int mapDragOperationFromModifiers(MouseEvent e) {
        int mods = e.getModifiersEx();
        if ((mods & 1024) == 0) {
            return 0;
        }
        return 3;
    }

    protected boolean isDragPossible(MouseEvent e) {
        Caret caret;
        int dot;
        boolean possible;
        JTextComponent c;
        int mark;
        JComponent comp = this.getEventComponent(e);
        boolean bl = comp == null ? false : (possible = comp.getTransferHandler() != null);
        if (possible && (c = (JTextComponent)this.getEventComponent(e)).getDragEnabled() && (dot = (caret = c.getCaret()).getDot()) != (mark = caret.getMark())) {
            Point p = new Point(e.getX(), e.getY());
            int pos = c.viewToModel(p);
            int p0 = Math.min(dot, mark);
            int p1 = Math.max(dot, mark);
            if (pos >= p0 && pos < p1) {
                return true;
            }
        }
        return false;
    }

    protected JComponent getEventComponent(MouseEvent e) {
        Object src = e.getSource();
        if (src instanceof JComponent) {
            JComponent c = (JComponent)src;
            return c;
        }
        return null;
    }

    private static String logMouseEvent(MouseEvent evt) {
        return "x=" + evt.getX() + ", y=" + evt.getY() + ", clicks=" + evt.getClickCount() + ", component=" + BaseCaret.s2s(evt.getComponent()) + ", source=" + BaseCaret.s2s(evt.getSource()) + ", button=" + evt.getButton() + ", mods=" + evt.getModifiers() + ", modsEx=" + evt.getModifiersEx();
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if ("document".equals(propName)) {
            BaseDocument newDoc = evt.getNewValue() instanceof BaseDocument ? (BaseDocument)evt.getNewValue() : null;
            this.modelChanged(this.listenDoc, newDoc);
        } else if ("overwriteMode".equals(propName)) {
            Boolean b = (Boolean)evt.getNewValue();
            this.overwriteMode = b != null ? b : false;
            this.updateType();
        } else if ("ancestor".equals(propName) && evt.getSource() == this.component) {
            JScrollPane scrollPane;
            JScrollBar hScrollBar;
            Container parent = this.component.getParent();
            if (parent instanceof JViewport && (parent = parent.getParent()) instanceof JScrollPane && (hScrollBar = (scrollPane = (JScrollPane)parent).getHorizontalScrollBar()) != null) {
                hScrollBar.addComponentListener((ComponentListener)WeakListeners.create(ComponentListener.class, (EventListener)this.listenerImpl, (Object)hScrollBar));
            }
        } else if ("enabled".equals(propName)) {
            Boolean enabled = (Boolean)evt.getNewValue();
            if (this.component.isFocusOwner()) {
                if (enabled == Boolean.TRUE) {
                    if (this.component.isEditable()) {
                        this.setVisible(true);
                    }
                    this.setSelectionVisible(true);
                } else {
                    this.setVisible(false);
                    this.setSelectionVisible(false);
                }
            }
        } else if ("rectangular-selection".equals(propName)) {
            boolean origRectangularSelection = this.rectangularSelection;
            this.rectangularSelection = Boolean.TRUE.equals(this.component.getClientProperty("rectangular-selection"));
            if (this.rectangularSelection != origRectangularSelection) {
                if (this.rectangularSelection) {
                    this.setRectangularSelectionToDotAndMark();
                    RectangularSelectionTransferHandler.install((JTextComponent)this.component);
                } else {
                    RectangularSelectionTransferHandler.uninstall((JTextComponent)this.component);
                }
                this.fireStateChanged();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        JTextComponent c = this.component;
        if (c != null) {
            boolean bl = this.blinkVisible = !this.blinkVisible;
            if (this.caretBounds != null) {
                Rectangle repaintRect = this.caretBounds;
                if (this.italic) {
                    repaintRect = new Rectangle(repaintRect);
                    repaintRect.width += repaintRect.height;
                }
                c.repaint(repaintRect);
            }
        }
    }

    @Deprecated
    public void foldHierarchyChanged(FoldHierarchyEvent evt) {
    }

    void scheduleCaretUpdate() {
        if (!this.caretUpdatePending) {
            this.caretUpdatePending = true;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    BaseCaret.this.update(false);
                }
            });
        }
    }

    public final void refresh() {
        this.updateType();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                BaseCaret.this.updateCaretBounds();
            }
        });
    }

    public void refresh(boolean retainInView) {
        Rectangle b = this.caretBounds;
        this.updateAfterFoldHierarchyChange = b != null;
        boolean wasInView = b != null && this.component.getVisibleRect().intersects(b);
        this.update(!retainInView || wasInView);
    }

    static {
        if (Boolean.getBoolean("netbeans.debug.editor.caret.focus") && LOG.getLevel().intValue() < Level.FINE.intValue()) {
            LOG.setLevel(Level.FINE);
        }
        if (Boolean.getBoolean("netbeans.debug.editor.caret.focus.extra") && LOG.getLevel().intValue() < Level.FINER.intValue()) {
            LOG.setLevel(Level.FINER);
        }
    }

    private static enum MouseState {
        DEFAULT,
        CHAR_SELECTION,
        WORD_SELECTION,
        LINE_SELECTION,
        DRAG_SELECTION_POSSIBLE,
        DRAG_SELECTION;
        

        private MouseState() {
        }
    }

    private class ListenerImpl
    extends ComponentAdapter
    implements FocusListener,
    ViewHierarchyListener {
        ListenerImpl() {
        }

        @Override
        public void focusGained(FocusEvent evt) {
            JTextComponent c;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("BaseCaret.focusGained(); doc=" + BaseCaret.this.component.getDocument().getProperty("title") + '\n');
            }
            if ((c = BaseCaret.this.component) != null) {
                BaseCaret.this.updateType();
                if (BaseCaret.this.component.isEnabled()) {
                    if (BaseCaret.this.component.isEditable()) {
                        BaseCaret.this.setVisible(true);
                    }
                    BaseCaret.this.setSelectionVisible(true);
                }
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("Caret visibility: " + BaseCaret.this.isVisible() + '\n');
                }
            } else if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("Text component is null, caret will not be visible\n");
            }
        }

        @Override
        public void focusLost(FocusEvent evt) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("BaseCaret.focusLost(); doc=" + BaseCaret.this.component.getDocument().getProperty("title") + "\nFOCUS GAINER: " + evt.getOppositeComponent() + '\n');
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("FOCUS EVENT: " + evt + '\n');
                }
            }
            BaseCaret.this.setVisible(false);
            BaseCaret.this.setSelectionVisible(evt.isTemporary());
        }

        @Override
        public void componentShown(ComponentEvent e) {
            Component hScrollBar = e.getComponent();
            if (hScrollBar != BaseCaret.this.component) {
                Container scrollPane = hScrollBar.getParent();
                if (BaseCaret.this.caretBounds != null && scrollPane instanceof JScrollPane) {
                    Rectangle viewRect = ((JScrollPane)scrollPane).getViewport().getViewRect();
                    Rectangle hScrollBarRect = new Rectangle(viewRect.x, viewRect.y + viewRect.height, hScrollBar.getWidth(), hScrollBar.getHeight());
                    if (hScrollBarRect.intersects(BaseCaret.this.caretBounds)) {
                        BaseCaret.this.dispatchUpdate(true);
                    }
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            Component c = e.getComponent();
            if (c == BaseCaret.this.component && BaseCaret.this.caretBounds == null) {
                BaseCaret.this.dispatchUpdate(true);
                if (BaseCaret.this.caretBounds != null) {
                    c.removeComponentListener(this);
                }
            }
        }

        public void viewHierarchyChanged(ViewHierarchyEvent evt) {
            BaseCaret.this.scheduleCaretUpdate();
        }
    }

}

