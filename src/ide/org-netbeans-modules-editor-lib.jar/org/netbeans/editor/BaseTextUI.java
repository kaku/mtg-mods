/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.EditorApiPackageAccessor
 *  org.netbeans.modules.editor.lib2.view.DocumentView
 *  org.netbeans.modules.editor.lib2.view.LockedViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchy
 *  org.netbeans.spi.lexer.MutableTextInput
 *  org.netbeans.spi.lexer.TokenHierarchyControl
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.plaf.basic.BasicTextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.ComponentView;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.LabelView;
import javax.swing.text.Position;
import javax.swing.text.TextAction;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.AtomicLockEvent;
import org.netbeans.editor.AtomicLockListener;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.MultiKeymap;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.view.spi.LockView;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.view.GapDocumentView;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.netbeans.modules.editor.lib.drawing.DrawEngineDocView;
import org.netbeans.modules.editor.lib.drawing.DrawEngineLineView;
import org.netbeans.modules.editor.lib2.EditorApiPackageAccessor;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenHierarchyControl;

public class BaseTextUI
extends BasicTextUI
implements PropertyChangeListener,
DocumentListener,
AtomicLockListener {
    static final String PROP_DEFAULT_CARET_BLINK_RATE = "nbeditor-default-swing-caret-blink-rate";
    private static final int LENGTHY_ATOMIC_EDIT_THRESHOLD = 80;
    private EditorUI editorUI;
    private boolean needsRefresh = false;
    int componentID = -1;
    private AbstractDocument lastDocument;
    private int atomicModCount = -1;
    private static final GetFocusedComponentAction gfcAction = new GetFocusedComponentAction();
    private Preferences prefs = null;

    @Override
    protected String getPropertyPrefix() {
        return "EditorPane";
    }

    public static JTextComponent getFocusedComponent() {
        return gfcAction.getFocusedComponent2();
    }

    protected boolean isRootViewReplaceNecessary() {
        boolean replaceNecessary = false;
        Document doc = this.getComponent().getDocument();
        if (doc != this.lastDocument) {
            replaceNecessary = true;
        }
        return replaceNecessary;
    }

    protected void rootViewReplaceNotify() {
        this.lastDocument = (AbstractDocument)this.getComponent().getDocument();
    }

    @Override
    protected void modelChanged() {
        Document doc;
        JTextComponent component = this.getComponent();
        Document document = doc = component != null ? component.getDocument() : null;
        if (component != null && doc != null) {
            BaseKit baseKit;
            boolean documentReplaced = this.isRootViewReplaceNecessary();
            component.removeAll();
            if (documentReplaced) {
                ViewFactory f = this.getRootView(component).getViewFactory();
                this.rootViewReplaceNotify();
                Element elem = doc.getDefaultRootElement();
                View v = f.create(elem);
                this.setView(v);
            }
            component.revalidate();
            if (documentReplaced && (baseKit = Utilities.getKit(component)) != null && this.prefs != null) {
                ArrayList<String> actionNamesList = new ArrayList<String>();
                String actionNames = this.prefs.get("doc-install-action-name-list", "");
                StringTokenizer t = new StringTokenizer(actionNames, ",");
                while (t.hasMoreTokens()) {
                    String actionName = t.nextToken().trim();
                    actionNamesList.add(actionName);
                }
                List<Action> actionsList = baseKit.translateActionNameList(actionNamesList);
                for (Action a : actionsList) {
                    a.actionPerformed(new ActionEvent(component, 1001, ""));
                }
            }
        }
    }

    @Override
    protected void installKeyboardActions() {
        String mapName = this.getPropertyPrefix() + ".actionMap";
        UIManager.getLookAndFeelDefaults().put(mapName, null);
        UIManager.getDefaults().put(mapName, null);
        super.installKeyboardActions();
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        if (!(c instanceof JTextComponent)) {
            return;
        }
        JTextComponent component = this.getComponent();
        this.prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)component)).lookup(Preferences.class);
        String value = this.prefs.get("margin", null);
        Insets margin = value != null ? SettingsConversions.parseInsets(value) : null;
        component.setMargin(margin != null ? margin : EditorUI.NULL_INSETS);
        this.getEditorUI().installUI(component);
        if (component.getClientProperty(UIWatcher.class) == null) {
            UIWatcher uiWatcher = new UIWatcher(this.getClass());
            component.addPropertyChangeListener(uiWatcher);
            component.putClientProperty(UIWatcher.class, uiWatcher);
        }
        BaseKit kit = (BaseKit)this.getEditorKit(component);
        ViewFactory vf = kit.getViewFactory();
        Caret defaultCaret = component.getCaret();
        Caret caret = kit.createCaret();
        component.setCaretColor(Color.black);
        component.setCaret(caret);
        component.putClientProperty("nbeditor-default-swing-caret-blink-rate", defaultCaret.getBlinkRate());
        component.setKeymap(kit.getKeymap());
        int br = this.prefs.getInt("caret-blink-rate", -1);
        if (br == -1) {
            br = defaultCaret.getBlinkRate();
        }
        caret.setBlinkRate(br);
        SwingUtilities.replaceUIInputMap(c, 0, null);
        EditorApiPackageAccessor.get().register(component);
        component.setCursor(Cursor.getPredefinedCursor(2));
    }

    @Override
    public void uninstallUI(JComponent c) {
        if (this.prefs == null) {
            return;
        }
        if (this.getComponent() != null && this.getComponent().getDocument() != null) {
            super.uninstallUI(c);
        }
        this.prefs = null;
        if (c instanceof JTextComponent) {
            JTextComponent comp = (JTextComponent)c;
            BaseDocument doc = Utilities.getDocument(comp);
            if (doc != null) {
                doc.removeDocumentListener(this);
                doc.removeAtomicLockListener(this);
            }
            comp.setKeymap(null);
            comp.setCaret(null);
            this.getEditorUI().uninstallUI(comp);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int getYFromPos(int pos) throws BadLocationException {
        JTextComponent component = this.getComponent();
        int y = 0;
        if (component != null) {
            ViewHierarchy vh = ViewHierarchy.get((JTextComponent)component);
            LockedViewHierarchy lvh = vh.lock();
            try {
                y = (int)lvh.modelToY(pos);
            }
            finally {
                lvh.unlock();
            }
        }
        return y;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int getPosFromY(int y) throws BadLocationException {
        JTextComponent component = this.getComponent();
        int offset = 0;
        if (component != null) {
            ViewHierarchy vh = ViewHierarchy.get((JTextComponent)component);
            LockedViewHierarchy lvh = vh.lock();
            try {
                offset = lvh.viewToModel(0.0, (double)y, null);
            }
            finally {
                lvh.unlock();
            }
        }
        return offset;
    }

    public int getBaseX(int y) {
        return this.getEditorUI().getTextMargin().left;
    }

    public int viewToModel(JTextComponent c, int x, int y) {
        return this.viewToModel(c, new Point(x, y));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void damageRange(JTextComponent t, int p0, int p1, Position.Bias p0Bias, Position.Bias p1Bias) {
        View view;
        View rootView = this.getRootView(this.getComponent());
        boolean doDamageRange = true;
        if (rootView.getViewCount() > 0 && (view = rootView.getView(0)) instanceof LockView) {
            LockView lockView = (LockView)view;
            lockView.lock();
            try {
                GapDocumentView docView = (GapDocumentView)view.getView(0);
                doDamageRange = docView.checkDamageRange(p0, p1, p0Bias, p1Bias);
            }
            finally {
                lockView.unlock();
            }
        }
        if (doDamageRange) {
            Document doc = t.getDocument();
            if (doc != null && p1 < doc.getLength()) {
                ++p1;
            }
            super.damageRange(t, p0, p1, p0Bias, p1Bias);
        }
    }

    @Override
    public int getNextVisualPositionFrom(JTextComponent t, int pos, Position.Bias b, int direction, Position.Bias[] biasRet) throws BadLocationException {
        if (biasRet == null) {
            biasRet = new Position.Bias[]{Position.Bias.Forward};
        }
        return super.getNextVisualPositionFrom(t, pos, b, direction, biasRet);
    }

    @Override
    public EditorKit getEditorKit(JTextComponent c) {
        JEditorPane pane = (JEditorPane)this.getComponent();
        return pane == null ? null : pane.getEditorKit();
    }

    public EditorUI getEditorUI() {
        JTextComponent c;
        BaseKit kit;
        if (this.editorUI == null && (kit = (BaseKit)this.getEditorKit(c = this.getComponent())) != null) {
            this.editorUI = kit.createEditorUI();
            this.editorUI.initLineHeight(c);
        }
        return this.editorUI;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JTextComponent comp;
        String propName = evt.getPropertyName();
        if ("document".equals(propName)) {
            BaseDocument oldDoc;
            BaseDocument newDoc;
            BaseDocument baseDocument = oldDoc = evt.getOldValue() instanceof BaseDocument ? (BaseDocument)evt.getOldValue() : null;
            if (oldDoc != null) {
                oldDoc.removeDocumentListener(this);
                oldDoc.removeAtomicLockListener(this);
            }
            BaseDocument baseDocument2 = newDoc = evt.getNewValue() instanceof BaseDocument ? (BaseDocument)evt.getNewValue() : null;
            if (newDoc != null) {
                newDoc.addDocumentListener(this);
                this.atomicModCount = -1;
                newDoc.addAtomicLockListener(this);
            }
        } else if ("ancestor".equals(propName) && (comp = (JTextComponent)evt.getSource()).isDisplayable() && this.editorUI != null && this.editorUI.hasExtComponent() && !Boolean.TRUE.equals(comp.getClientProperty("ancestorOverride"))) {
            comp.putClientProperty("ancestorOverride", Boolean.TRUE);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent evt) {
        this.checkLengthyAtomicEdit(evt);
    }

    @Override
    public void removeUpdate(DocumentEvent evt) {
        this.checkLengthyAtomicEdit(evt);
    }

    @Override
    public void changedUpdate(DocumentEvent evt) {
        if (evt instanceof BaseDocumentEvent) {
            try {
                JTextComponent comp = this.getComponent();
                if (comp != null && comp.isShowing()) {
                    this.getEditorUI().repaintBlock(evt.getOffset(), evt.getOffset() + evt.getLength());
                }
            }
            catch (BadLocationException ex) {
                Utilities.annotateLoggable(ex);
            }
        }
    }

    private void checkLengthyAtomicEdit(DocumentEvent evt) {
        if (this.atomicModCount != -1 && ++this.atomicModCount == 80) {
            View view;
            Document doc = evt.getDocument();
            View rootView = this.getRootView(this.getComponent());
            if (rootView != null && rootView.getViewCount() > 0 && (view = rootView.getView(0)) instanceof DocumentView) {
                ((DocumentView)view).updateLengthyAtomicEdit(1);
            }
        }
    }

    @Override
    public void atomicLock(AtomicLockEvent evt) {
        assert (this.atomicModCount == -1);
        this.atomicModCount = 0;
    }

    @Override
    public void atomicUnlock(AtomicLockEvent evt) {
        if (this.atomicModCount != -1) {
            if (this.atomicModCount >= 80) {
                MutableTextInput input;
                Document doc;
                View view;
                View rootView = this.getRootView(this.getComponent());
                if (rootView != null && rootView.getViewCount() > 0 && (view = rootView.getView(0)) instanceof DocumentView) {
                    ((DocumentView)view).updateLengthyAtomicEdit(-1);
                }
                if ((input = (MutableTextInput)(doc = this.getComponent().getDocument()).getProperty(MutableTextInput.class)) != null) {
                    input.tokenHierarchyControl().setActive(true);
                }
            }
            this.atomicModCount = -1;
        }
    }

    @Override
    public View create(Element elem) {
        String kind = elem.getName();
        if (kind != null) {
            if (kind.equals("content")) {
                return new LabelView(elem);
            }
            if (kind.equals("paragraph")) {
                return new DrawEngineLineView(elem);
            }
            if (kind.equals("section")) {
                return new LockView(new DrawEngineDocView(elem));
            }
            if (kind.equals("component")) {
                return new ComponentView(elem);
            }
            if (kind.equals("icon")) {
                return new IconView(elem);
            }
        }
        return new DrawEngineLineView(elem);
    }

    @Override
    public View create(Element elem, int p0, int p1) {
        return null;
    }

    public void preferenceChanged(boolean width, boolean height) {
        this.modelChanged();
    }

    public void invalidateStartY() {
    }

    protected void refresh() {
        if (this.getComponent().isShowing() && this.needsRefresh) {
            this.modelChanged();
            this.needsRefresh = false;
        }
    }

    static void uninstallUIWatcher(JTextComponent c) {
        UIWatcher uiWatcher = (UIWatcher)c.getClientProperty(UIWatcher.class);
        if (uiWatcher != null) {
            c.removePropertyChangeListener(uiWatcher);
            c.putClientProperty(UIWatcher.class, null);
        }
    }

    static class UIWatcher
    implements PropertyChangeListener {
        private Class uiClass;

        UIWatcher(Class uiClass) {
            this.uiClass = uiClass;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            JTextComponent c;
            EditorKit kit;
            Object newValue = evt.getNewValue();
            if ("UI".equals(evt.getPropertyName()) && newValue != null && !(newValue instanceof BaseTextUI) && (kit = ((TextUI)newValue).getEditorKit(c = (JTextComponent)evt.getSource())) instanceof BaseKit) {
                try {
                    BaseTextUI oldUI;
                    JComponent oldExtComponent;
                    Container parent;
                    BaseTextUI newUI = (BaseTextUI)this.uiClass.newInstance();
                    c.setUI(newUI);
                    if (evt.getOldValue() instanceof BaseTextUI && (oldUI = (BaseTextUI)evt.getOldValue()).getEditorUI().hasExtComponent() && (parent = (oldExtComponent = oldUI.getEditorUI().getExtComponent()).getParent()) != null) {
                        parent.remove(oldExtComponent);
                        parent.add(newUI.getEditorUI().getExtComponent());
                    }
                }
                catch (InstantiationException e) {
                }
                catch (IllegalAccessException e) {
                    // empty catch block
                }
            }
        }
    }

    private static class GetFocusedComponentAction
    extends TextAction {
        private GetFocusedComponentAction() {
            super("get-focused-component");
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
        }

        JTextComponent getFocusedComponent2() {
            return super.getFocusedComponent();
        }
    }

}

