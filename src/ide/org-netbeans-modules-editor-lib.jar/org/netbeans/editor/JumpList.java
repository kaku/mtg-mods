/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.openide.modules.PatchedPublic
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WeakPropertyChangeSupport;
import org.openide.modules.PatchedPublic;
import org.openide.util.WeakListeners;

public final class JumpList {
    private static final Logger LOG = Logger.getLogger(JumpList.class.getName());
    private static final WeakPropertyChangeSupport support = new WeakPropertyChangeSupport();
    private static PropertyChangeListener listener = new PropertyChangeListener(){

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            support.firePropertyChange(JumpList.class, null, null, null);
        }
    };

    static void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    public static void checkAddEntry() {
        JTextComponent c = Utilities.getLastActiveComponent();
        if (c != null) {
            JumpList.addEntry(c, c.getCaret().getDot());
        }
    }

    public static void checkAddEntry(JTextComponent c) {
        JumpList.addEntry(c, c.getCaret().getDot());
    }

    public static void checkAddEntry(JTextComponent c, int pos) {
        JumpList.addEntry(c, pos);
    }

    public static void addEntry(JTextComponent c, int pos) {
        try {
            NavigationHistory.getNavigations().markWaypoint(c, pos, false, false);
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, "Can't add position to the navigation history.", e);
        }
    }

    public static void jumpPrev(JTextComponent c) {
        NavigationHistory.Waypoint wpt = NavigationHistory.getNavigations().navigateBack();
        JumpList.show(wpt);
    }

    public static void jumpPrevComponent(JTextComponent c) {
        List list = NavigationHistory.getNavigations().getPreviousWaypoints();
        for (NavigationHistory.Waypoint wpt : list) {
            JTextComponent wptComp = wpt.getComponent();
            if (wptComp == null || wptComp == c) continue;
            JumpList.show(wpt);
            return;
        }
    }

    public static boolean hasPrev() {
        return NavigationHistory.getNavigations().hasPreviousWaypoints();
    }

    public static void jumpNext(JTextComponent c) {
        NavigationHistory.Waypoint wpt = NavigationHistory.getNavigations().navigateForward();
        JumpList.show(wpt);
    }

    public static void jumpNextComponent(JTextComponent c) {
        List list = NavigationHistory.getNavigations().getNextWaypoints();
        for (NavigationHistory.Waypoint wpt : list) {
            JTextComponent wptComp = wpt.getComponent();
            if (wptComp == null || wptComp == c) continue;
            JumpList.show(wpt);
            return;
        }
    }

    public static boolean hasNext() {
        return NavigationHistory.getNavigations().hasNextWaypoints();
    }

    public static String dump() {
        StringBuilder sb = new StringBuilder();
        sb.append("Previous waypoints: {\n");
        List prev = NavigationHistory.getNavigations().getPreviousWaypoints();
        for (NavigationHistory.Waypoint wpt : prev) {
            URL url = wpt.getUrl();
            sb.append("    ").append(url.toString()).append("\n");
        }
        sb.append("}\n");
        sb.append("Next waypoints: {\n");
        List next = NavigationHistory.getNavigations().getNextWaypoints();
        for (NavigationHistory.Waypoint wpt2 : next) {
            URL url = wpt2.getUrl();
            sb.append("    ").append(url.toString()).append("\n");
        }
        sb.append("}\n");
        return sb.toString();
    }

    @PatchedPublic
    private JumpList() {
    }

    private static void show(NavigationHistory.Waypoint wpt) {
        JTextComponent c;
        JTextComponent jTextComponent = c = wpt == null ? null : wpt.getComponent();
        if (c != null) {
            int offset;
            if (Utilities.getLastActiveComponent() != c) {
                Utilities.requestFocus(c);
            }
            if ((offset = wpt.getOffset()) >= 0 && offset <= c.getDocument().getLength()) {
                c.getCaret().setDot(offset);
            }
        }
    }

    static {
        NavigationHistory.getNavigations().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)listener, (Object)NavigationHistory.getNavigations()));
    }

    public static final class Entry {
        private Entry(JTextComponent component, int offset, Entry last) throws BadLocationException {
        }

        public int getPosition() {
            return -1;
        }

        public JTextComponent getComponent() {
            return null;
        }

        public boolean setDot() {
            return false;
        }
    }

}

