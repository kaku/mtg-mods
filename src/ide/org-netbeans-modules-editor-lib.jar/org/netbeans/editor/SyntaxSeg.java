/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javax.swing.text.BadLocationException;
import javax.swing.text.Segment;
import org.netbeans.editor.BaseDocument;

class SyntaxSeg
extends Segment {
    private static final char[] EMPTY_CHAR_ARRAY = new char[0];
    private static final int MAX_SLOT_COUNT = 100;
    private static final int REALLOC_INCREMENT = 2048;
    private static ArrayList slotList = new ArrayList();

    SyntaxSeg() {
    }

    static synchronized Slot getFreeSlot() {
        int cnt = slotList.size();
        return cnt > 0 ? (Slot)slotList.remove(cnt - 1) : new Slot();
    }

    static synchronized void releaseSlot(Slot slot) {
        slotList.add(slot);
    }

    static synchronized void invalidate(BaseDocument doc, int pos) {
        int cnt = slotList.size();
        for (int i = 0; i < cnt; ++i) {
            ((Slot)slotList.get(i)).invalidate(doc, pos);
        }
    }

    static class Slot
    extends Segment {
        WeakReference segDocRef = new WeakReference<Object>(null);
        int segPos;
        int segLen;

        Slot() {
            this.array = EMPTY_CHAR_ARRAY;
        }

        int load(BaseDocument doc, int pos, int len) throws BadLocationException {
            boolean difDoc;
            if (len <= 0) {
                if (len == 0) {
                    this.count = 0;
                    return 0;
                }
                throw new RuntimeException("len=" + len);
            }
            BaseDocument segDoc = (BaseDocument)this.segDocRef.get();
            boolean bl = difDoc = doc != segDoc;
            if (difDoc) {
                segDoc = doc;
                this.segDocRef = new WeakReference<BaseDocument>(segDoc);
            }
            if (difDoc || pos < this.segPos || pos > this.segPos + this.segLen || pos - this.segPos + len > this.array.length) {
                if (len > this.array.length) {
                    char[] tmp = new char[len + 2048];
                    this.array = tmp;
                }
                this.segPos = pos;
                this.segLen = len;
                doc.getChars(pos, this.array, 0, len);
            } else {
                int endSegPos = this.segPos + this.segLen;
                int restLen = pos + len - endSegPos;
                if (restLen > 0) {
                    doc.getChars(endSegPos, this.array, this.segLen, restLen);
                    this.segLen += restLen;
                }
            }
            this.offset = pos - this.segPos;
            this.count = len;
            if (this.offset < 0 || len < 0) {
                throw new BadLocationException("pos=" + pos + ", offset=" + this.offset + "len=" + len, this.offset);
            }
            return len;
        }

        boolean isAreaInside(BaseDocument doc, int pos, int len) {
            return doc == (BaseDocument)this.segDocRef.get() && pos >= this.segPos && pos + len <= this.segPos + this.segLen;
        }

        void invalidate(BaseDocument doc, int pos) {
            if (doc == (BaseDocument)this.segDocRef.get()) {
                if (pos < this.segPos) {
                    this.segLen = 0;
                } else if (pos < this.segPos + this.segLen) {
                    this.segLen = pos - this.segPos;
                }
            }
        }
    }

}

