/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WeakTimerListener;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class StatusBar
implements PropertyChangeListener,
DocumentListener {
    private static final Logger CARET_OFFSET_LOG = Logger.getLogger("org.netbeans.editor.caret.offset");
    public static final String CELL_MAIN = "main";
    public static final String CELL_POSITION = "position";
    public static final String CELL_TYPING_MODE = "typing-mode";
    public static final String INSERT_LOCALE = "status-bar-insert";
    public static final String OVERWRITE_LOCALE = "status-bar-overwrite";
    private static final String[] POS_MAX_STRINGS = new String[]{"99999:999/9999:9999"};
    private static final String[] POS_MAX_STRINGS_OFFSET = new String[]{"99999:999/9999:9999 <99999999>"};
    private static final Insets NULL_INSETS = new Insets(0, 0, 0, 0);
    static final Border CELL_BORDER = BorderFactory.createCompoundBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getDefaults().getColor("control")), BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, UIManager.getDefaults().getColor("controlHighlight")), BorderFactory.createLineBorder(UIManager.getDefaults().getColor("controlDkShadow")))), BorderFactory.createEmptyBorder(0, 2, 0, 2));
    private static Map<String, JLabel> cellName2GlobalCell = new HashMap<String, JLabel>();
    protected EditorUI editorUI;
    private JPanel panel;
    private boolean visible;
    private List cellList = new ArrayList();
    private Caret caret;
    private CaretListener caretL;
    private int caretDelay;
    private boolean overwriteModeDisplayed;
    private String insText;
    private String ovrText;
    private String caretPositionLocaleString;
    private String insertModeLocaleString;
    private String overwriteModeLocaleString;
    private Preferences prefs = null;
    private final PreferenceChangeListener prefsListener;
    private PreferenceChangeListener weakListener;
    static final long serialVersionUID = -6266183959929157349L;

    public static void setGlobalCell(String cellName, JLabel globalCell) {
        cellName2GlobalCell.put(cellName, globalCell);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public StatusBar(EditorUI editorUI) {
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        StatusBar.this.refreshPanel();
                    }
                });
                if (evt == null || "status-bar-caret-delay".equals(evt.getKey())) {
                    StatusBar.this.caretDelay = StatusBar.this.prefs.getInt("status-bar-caret-delay", 200);
                    if (StatusBar.this.caretL != null) {
                        StatusBar.this.caretL.setDelay(StatusBar.this.caretDelay);
                    }
                }
            }

        };
        this.weakListener = null;
        this.editorUI = editorUI;
        this.caretDelay = 10;
        this.caretL = new CaretListener(this.caretDelay);
        ResourceBundle bundle = NbBundle.getBundle(BaseKit.class);
        this.insText = bundle.getString("status-bar-insert");
        this.ovrText = bundle.getString("status-bar-overwrite");
        this.caretPositionLocaleString = bundle.getString("status-bar-caret-position");
        this.insertModeLocaleString = bundle.getString("status-bar-insert-mode");
        this.overwriteModeLocaleString = bundle.getString("status-bar-overwrite-mode");
        Object object = editorUI.getComponentLock();
        synchronized (object) {
            JTextComponent component = editorUI.getComponent();
            if (component != null) {
                this.propertyChange(new PropertyChangeEvent(editorUI, "component", null, component));
            }
            editorUI.addPropertyChangeListener(this);
        }
    }

    private void documentUndo(DocumentEvent evt) {
        Utilities.runInEventDispatchThread(new Runnable(){

            @Override
            public void run() {
                StatusBar.this.setText("main", "");
            }
        });
    }

    @Override
    public void insertUpdate(DocumentEvent evt) {
        if (evt.getType() == DocumentEvent.EventType.REMOVE) {
            this.documentUndo(evt);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent evt) {
        if (evt.getType() == DocumentEvent.EventType.INSERT) {
            this.documentUndo(evt);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent evt) {
    }

    protected JPanel createPanel() {
        return new JPanel(new GridBagLayout());
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean v) {
        if (v != this.visible) {
            this.visible = v;
            if (this.panel != null || this.visible) {
                if (this.visible) {
                    this.refreshPanel();
                }
                if (SwingUtilities.isEventDispatchThread()) {
                    this.getPanel().setVisible(this.visible);
                } else {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            StatusBar.this.getPanel().setVisible(StatusBar.this.visible);
                        }
                    });
                }
            }
        }
    }

    public void updateGlobal() {
        for (Map.Entry<String, JLabel> e : cellName2GlobalCell.entrySet()) {
            if ("main".equals(e.getKey())) continue;
            String s = this.getText(e.getKey());
            e.getValue().setText(s);
        }
    }

    public final JPanel getPanel() {
        if (this.panel == null) {
            this.panel = this.createPanel();
            this.initPanel();
        }
        return this.panel;
    }

    protected void initPanel() {
        JLabel cell = this.addCell("position", CARET_OFFSET_LOG.isLoggable(Level.FINE) ? POS_MAX_STRINGS_OFFSET : POS_MAX_STRINGS);
        cell.setHorizontalAlignment(0);
        cell.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                BaseKit kit;
                JTextComponent jtc;
                Action a;
                if (e.getClickCount() == 2 && (jtc = StatusBar.this.editorUI.getComponent()) != null && (kit = Utilities.getKit(jtc)) != null && (a = kit.getActionByName("goto")) != null) {
                    a.actionPerformed(new ActionEvent(jtc, 0, null));
                }
            }
        });
        this.addCell("typing-mode", new String[]{this.insText, this.ovrText}).setHorizontalAlignment(0);
        this.setText("typing-mode", this.insText);
        this.addCell("main", null);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if ("component".equals(propName)) {
            JTextComponent component;
            if (this.prefs != null && this.weakListener != null) {
                this.prefs.removePreferenceChangeListener(this.weakListener);
            }
            if ((component = (JTextComponent)evt.getNewValue()) != null) {
                Document doc;
                component.addPropertyChangeListener(this);
                this.caret = component.getCaret();
                if (this.caret != null) {
                    this.caret.addChangeListener(this.caretL);
                }
                if ((doc = component.getDocument()) != null) {
                    doc.addDocumentListener(this);
                }
                String mimeType = DocumentUtilities.getMimeType((JTextComponent)component);
                this.prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                this.weakListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs);
                this.prefs.addPreferenceChangeListener(this.weakListener);
                this.prefsListener.preferenceChange(null);
                this.refreshPanel();
            } else {
                Document doc;
                component = (JTextComponent)evt.getOldValue();
                component.removePropertyChangeListener(this);
                this.caret = component.getCaret();
                if (this.caret != null) {
                    this.caret.removeChangeListener(this.caretL);
                }
                if ((doc = component.getDocument()) != null) {
                    doc.removeDocumentListener(this);
                }
            }
        } else if ("caret".equals(propName)) {
            if (this.caret != null) {
                this.caret.removeChangeListener(this.caretL);
            }
            this.caret = (Caret)evt.getNewValue();
            if (this.caret != null) {
                this.caret.addChangeListener(this.caretL);
            }
        } else if ("document".equals(propName)) {
            Document old = (Document)evt.getOldValue();
            Document cur = (Document)evt.getNewValue();
            if (old != null) {
                old.removeDocumentListener(this);
            }
            if (cur != null) {
                cur.addDocumentListener(this);
            }
        }
        if ("overwriteMode".equals(propName)) {
            this.caretL.actionPerformed(null);
        } else {
            this.caretL.stateChanged(null);
        }
    }

    private void applyColoring(Cell cell, Coloring coloring) {
        coloring.apply(cell);
        if (coloring.getForeColor() == null) {
            cell.setForeground(cell.getDefaultForeground());
        }
        if (coloring.getBackColor() == null) {
            cell.setBackground(cell.getDefaultBackground());
        }
    }

    public int getCellCount() {
        return this.cellList.size();
    }

    public JLabel addCell(String name, String[] widestStrings) {
        return this.addCell(-1, name, widestStrings);
    }

    public JLabel addCell(int i, String name, String[] widestStrings) {
        Cell c = new Cell(name, widestStrings);
        this.addCellImpl(i, c);
        return c;
    }

    public void addCustomCell(int i, JLabel c) {
        this.addCellImpl(i, c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addCellImpl(int i, JLabel c) {
        List list = this.cellList;
        synchronized (list) {
            ArrayList<JLabel> newCellList = new ArrayList<JLabel>(this.cellList);
            int cnt = newCellList.size();
            if (i < 0 || i > cnt) {
                i = cnt;
            }
            newCellList.add(i, c);
            this.cellList = newCellList;
            this.updateCellBorders(i);
        }
        this.refreshPanel();
    }

    private void updateCellBorders(int addedIndex) {
        int cellCount = this.getCellCount();
        Border innerBorder = (Border)UIManager.get("Nb.Editor.Status.innerBorder");
        Border leftBorder = (Border)UIManager.get("Nb.Editor.Status.leftBorder");
        Border rightBorder = (Border)UIManager.get("Nb.Editor.Status.rightBorder");
        Border onlyOneBorder = (Border)UIManager.get("Nb.Editor.Status.onlyOneBorder");
        if (innerBorder == null || leftBorder == null || rightBorder == null || onlyOneBorder == null) {
            return;
        }
        if (cellCount == 1) {
            ((JLabel)this.cellList.get(0)).setBorder(onlyOneBorder);
            return;
        }
        if (addedIndex == 0) {
            ((JLabel)this.cellList.get(0)).setBorder(leftBorder);
            JLabel second = (JLabel)this.cellList.get(1);
            second.setBorder(cellCount == 2 ? rightBorder : innerBorder);
        } else if (addedIndex == cellCount - 1) {
            ((JLabel)this.cellList.get(cellCount - 1)).setBorder(rightBorder);
            JLabel previous = (JLabel)this.cellList.get(cellCount - 2);
            previous.setBorder(cellCount == 2 ? leftBorder : innerBorder);
        } else {
            ((JLabel)this.cellList.get(addedIndex)).setBorder(innerBorder);
        }
    }

    public JLabel getCellByName(String name) {
        for (JLabel c : this.cellList) {
            if (!name.equals(c.getName())) continue;
            return c;
        }
        return null;
    }

    public String getText(String cellName) {
        JLabel cell = this.getCellByName(cellName);
        return cell != null ? cell.getText() : null;
    }

    public void setText(String cellName, String text) {
        this.setText(cellName, text, null);
    }

    private static Coloring getColoring(String mimeType, String highlight) {
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        AttributeSet attribs = fcs == null ? null : fcs.getFontColors(highlight);
        return attribs == null ? null : Coloring.fromAttributeSet(attribs);
    }

    public void setBoldText(String cellName, String text) {
        JTextComponent jtc = this.editorUI.getComponent();
        this.setText(cellName, text, jtc != null ? StatusBar.getColoring(DocumentUtilities.getMimeType((JTextComponent)jtc), "status-bar-bold") : null);
    }

    public void setText(String cellName, String text, Coloring extraColoring) {
        this.setText(cellName, text, extraColoring, 1);
    }

    public void setText(String cellName, String text, Coloring extraColoring, int importance) {
        JLabel cell = this.getCellByName(cellName);
        if (cell != null) {
            cell.setText(text);
            if (this.visible) {
                Coloring c;
                JTextComponent jtc = this.editorUI.getComponent();
                Coloring coloring = c = jtc != null ? StatusBar.getColoring(DocumentUtilities.getMimeType((JTextComponent)jtc), "status-bar") : null;
                if (c != null && extraColoring != null) {
                    c = extraColoring.apply(c);
                } else if (c == null) {
                    c = extraColoring;
                }
                if ("position".equals(cellName)) {
                    cell.setToolTipText(this.caretPositionLocaleString);
                } else if ("typing-mode".equals(cellName)) {
                    cell.setToolTipText(this.insText.equals(text) ? this.insertModeLocaleString : this.overwriteModeLocaleString);
                } else {
                    cell.setToolTipText(text == null || text.length() == 0 ? null : text);
                }
                if (c != null && cell instanceof Cell) {
                    this.applyColoring((Cell)cell, c);
                }
            } else {
                JLabel globalCell = cellName2GlobalCell.get(cellName);
                if (globalCell != null) {
                    if ("main".equals(cellName)) {
                        globalCell.putClientProperty("importance", importance);
                    }
                    globalCell.setText(text);
                }
            }
        }
    }

    public void setText(String text, int importance) {
        JTextComponent jtc = this.editorUI.getComponent();
        this.setText("main", text, jtc != null ? StatusBar.getColoring(DocumentUtilities.getMimeType((JTextComponent)jtc), "status-bar-bold") : null, importance);
    }

    private void refreshPanel() {
        if (this.isVisible()) {
            for (JLabel c : this.cellList) {
                Coloring col;
                JTextComponent jtc = this.editorUI.getComponent();
                if (!(c instanceof Cell) || jtc == null || (col = StatusBar.getColoring(DocumentUtilities.getMimeType((JTextComponent)jtc), "status-bar")) == null) continue;
                this.applyColoring((Cell)c, col);
            }
            GridBagConstraints gc = new GridBagConstraints();
            gc.gridx = -1;
            gc.gridwidth = 1;
            gc.gridheight = 1;
            for (JLabel c2 : this.cellList) {
                boolean main = "main".equals(c2.getName());
                if (main) {
                    gc.fill = 2;
                    gc.weightx = 1.0;
                }
                this.getPanel().add((Component)c2, gc);
                if (!main) continue;
                gc.fill = 0;
                gc.weightx = 0.0;
            }
        }
    }

    public static final class StatusBarFactory
    implements SideBarFactory {
        @Override
        public JComponent createSideBar(JTextComponent target) {
            return Utilities.getEditorUI(target).getStatusBar().getPanel();
        }
    }

    static class Cell
    extends JLabel {
        Dimension maxDimension;
        String[] widestStrings;
        private final Color defaultBackground;
        private final Color defaultForeground;
        static final long serialVersionUID = -2554600362177165648L;

        Cell(String name, String[] widestStrings) {
            this.setName(name);
            this.setBorder(StatusBar.CELL_BORDER);
            this.setOpaque(true);
            this.widestStrings = widestStrings;
            this.defaultBackground = this.getBackground();
            this.defaultForeground = this.getForeground();
            this.updateSize();
        }

        private void updateSize() {
            Font f = this.getFont();
            if (this.maxDimension == null) {
                this.maxDimension = new Dimension();
            }
            if (f != null) {
                Border b = this.getBorder();
                Insets ins = b != null ? b.getBorderInsets(this) : NULL_INSETS;
                FontMetrics fm = this.getFontMetrics(f);
                String text = this.getText();
                int mw = text == null ? 0 : fm.stringWidth(text);
                this.maxDimension.height = fm.getHeight() + ins.top + ins.bottom;
                if (this.widestStrings != null) {
                    for (int i = 0; i < this.widestStrings.length; ++i) {
                        String widestString = this.widestStrings[i];
                        if (widestString == null) continue;
                        mw = Math.max(mw, fm.stringWidth(widestString));
                    }
                }
                this.maxDimension.width = mw + ins.left + ins.right;
            }
        }

        @Override
        public Dimension getPreferredSize() {
            if (this.maxDimension == null) {
                this.maxDimension = new Dimension();
            }
            return new Dimension(this.maxDimension);
        }

        @Override
        public Dimension getMinimumSize() {
            if (this.maxDimension == null) {
                this.maxDimension = new Dimension();
            }
            return new Dimension(this.maxDimension);
        }

        @Override
        public void setFont(Font f) {
            super.setFont(f);
            this.updateSize();
        }

        @Override
        public void setBorder(Border border) {
            super.setBorder(border);
            this.updateSize();
        }

        public Color getDefaultForeground() {
            Color color = (Color)UIManager.get("Label.foreground");
            return color != null ? color : this.defaultForeground;
        }

        public Color getDefaultBackground() {
            Color color = UIManager.getColor("NbEditorStatusBar.background");
            if (null == color) {
                color = (Color)UIManager.get("Label.background");
            }
            return color != null ? color : this.defaultBackground;
        }
    }

    class CaretListener
    implements ChangeListener,
    ActionListener {
        Timer timer;

        CaretListener(int delay) {
            this.timer = new Timer(delay, new WeakTimerListener(this));
            this.timer.setRepeats(false);
        }

        void setDelay(int delay) {
            this.timer.setInitialDelay(delay);
        }

        @Override
        public void stateChanged(ChangeEvent evt) {
            this.timer.restart();
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            Caret c = StatusBar.this.caret;
            JTextComponent component = StatusBar.this.editorUI.getComponent();
            if (component != null && component == EditorRegistry.lastFocusedComponent()) {
                Boolean b;
                boolean om;
                BaseDocument doc;
                if (c != null && (doc = Utilities.getDocument(component)) != null && doc.getDefaultRootElement().getElementCount() > 0) {
                    boolean hasSelection;
                    int countOfSelectedChars;
                    int pos = c.getDot();
                    String s = Utilities.debugPosition(doc, pos, ":");
                    if (CARET_OFFSET_LOG.isLoggable(Level.FINE)) {
                        s = s + " <" + pos + ">";
                    }
                    boolean bl = hasSelection = (countOfSelectedChars = component.getSelectionEnd() - component.getSelectionStart()) > 0;
                    if (hasSelection) {
                        try {
                            int lineEnd = Utilities.getLineOffset(doc, component.getSelectionEnd());
                            int lineStart = Utilities.getLineOffset(doc, component.getSelectionStart());
                            s = s + "/" + (lineEnd - lineStart + 1);
                        }
                        catch (BadLocationException ex) {
                            // empty catch block
                        }
                        s = s + ":" + countOfSelectedChars;
                    }
                    StatusBar.this.setText("position", s);
                }
                boolean bl = om = (b = (Boolean)StatusBar.this.editorUI.getProperty("overwriteMode")) != null && b != false;
                if (om != StatusBar.this.overwriteModeDisplayed) {
                    StatusBar.this.overwriteModeDisplayed = om;
                    StatusBar.this.setText("typing-mode", StatusBar.this.overwriteModeDisplayed ? StatusBar.this.ovrText : StatusBar.this.insText);
                }
            }
        }
    }

}

