/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.KeyBindingSettings
 *  org.netbeans.api.editor.settings.MultiKeyBinding
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.ListenerList
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.netbeans.modules.editor.indent.api.IndentUtils
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.netbeans.modules.editor.lib2.RectangularSelectionUtils
 *  org.netbeans.modules.editor.lib2.actions.EditorActionUtilities
 *  org.netbeans.modules.editor.lib2.actions.KeyBindingsUpdater
 *  org.netbeans.modules.editor.lib2.actions.SearchableEditorKit
 *  org.netbeans.modules.editor.lib2.typinghooks.DeletedTextInterceptorsManager
 *  org.netbeans.modules.editor.lib2.typinghooks.DeletedTextInterceptorsManager$Transaction
 *  org.netbeans.modules.editor.lib2.typinghooks.TypedBreakInterceptorsManager
 *  org.netbeans.modules.editor.lib2.typinghooks.TypedBreakInterceptorsManager$Transaction
 *  org.netbeans.modules.editor.lib2.typinghooks.TypedTextInterceptorsManager
 *  org.netbeans.modules.editor.lib2.typinghooks.TypedTextInterceptorsManager$Transaction
 *  org.netbeans.modules.editor.lib2.view.ViewFactoryImpl
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.HelpCtx$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.WeakSet
 */
package org.netbeans.editor;

import java.awt.Component;
import java.awt.Container;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.KeyBindingSettings;
import org.netbeans.editor.ActionFactory;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseCaret;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseImageTokenID;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.LocalBaseAction;
import org.netbeans.editor.MultiKeyBinding;
import org.netbeans.editor.MultiKeymap;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TokenContext;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WordMatch;
import org.netbeans.lib.editor.hyperlink.HyperlinkOperation;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.editor.indent.api.IndentUtils;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.netbeans.modules.editor.lib.KitsTracker;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.netbeans.modules.editor.lib2.RectangularSelectionUtils;
import org.netbeans.modules.editor.lib2.actions.EditorActionUtilities;
import org.netbeans.modules.editor.lib2.actions.KeyBindingsUpdater;
import org.netbeans.modules.editor.lib2.actions.SearchableEditorKit;
import org.netbeans.modules.editor.lib2.typinghooks.DeletedTextInterceptorsManager;
import org.netbeans.modules.editor.lib2.typinghooks.TypedBreakInterceptorsManager;
import org.netbeans.modules.editor.lib2.typinghooks.TypedTextInterceptorsManager;
import org.netbeans.modules.editor.lib2.view.ViewFactoryImpl;
import org.openide.awt.StatusDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.WeakSet;

public class BaseKit
extends DefaultEditorKit {
    static ThreadLocal<Boolean> IN_PASTE = new ThreadLocal();
    private static final Logger LOG = Logger.getLogger(BaseKit.class.getName());
    public static final String splitLineAction = "split-line";
    public static final String annotationsCyclingAction = "annotations-cycling";
    public static final String collapseFoldAction = "collapse-fold";
    public static final String expandFoldAction = "expand-fold";
    public static final String collapseAllFoldsAction = "collapse-all-folds";
    public static final String expandAllFoldsAction = "expand-all-folds";
    public static final String selectionPageUpAction = "selection-page-up";
    public static final String selectionPageDownAction = "selection-page-down";
    public static final String removeTabAction = "remove-tab";
    public static final String removeSelectionAction = "remove-selection";
    public static final String abbrevExpandAction = "abbrev-expand";
    public static final String abbrevResetAction = "abbrev-reset";
    public static final String removePreviousWordAction = "remove-word-previous";
    public static final String removeNextWordAction = "remove-word-next";
    public static final String removeLineBeginAction = "remove-line-begin";
    public static final String removeLineAction = "remove-line";
    public static final String moveSelectionElseLineUpAction = "move-selection-else-line-up";
    public static final String moveSelectionElseLineDownAction = "move-selection-else-line-down";
    public static final String copySelectionElseLineUpAction = "copy-selection-else-line-up";
    public static final String copySelectionElseLineDownAction = "copy-selection-else-line-down";
    public static final String toggleTypingModeAction = "toggle-typing-mode";
    public static final String toUpperCaseAction = "to-upper-case";
    public static final String toLowerCaseAction = "to-lower-case";
    public static final String switchCaseAction = "switch-case";
    public static final String findNextAction = "find-next";
    public static final String findPreviousAction = "find-previous";
    public static final String toggleHighlightSearchAction = "toggle-highlight-search";
    public static final String findSelectionAction = "find-selection";
    public static final String undoAction = "undo";
    public static final String redoAction = "redo";
    public static final String wordMatchNextAction = "word-match-next";
    public static final String wordMatchPrevAction = "word-match-prev";
    public static final String reindentLineAction = "reindent-line";
    public static final String reformatLineAction = "reformat-line";
    public static final String shiftLineRightAction = "shift-line-right";
    public static final String shiftLineLeftAction = "shift-line-left";
    public static final String adjustWindowCenterAction = "adjust-window-center";
    public static final String adjustWindowTopAction = "adjust-window-top";
    public static final String adjustWindowBottomAction = "adjust-window-bottom";
    public static final String adjustCaretCenterAction = "adjust-caret-center";
    public static final String adjustCaretTopAction = "adjust-caret-top";
    public static final String adjustCaretBottomAction = "adjust-caret-bottom";
    public static final String formatAction = "format";
    public static final String indentAction = "indent";
    public static final String firstNonWhiteAction = "first-non-white";
    public static final String lastNonWhiteAction = "last-non-white";
    public static final String selectionFirstNonWhiteAction = "selection-first-non-white";
    public static final String selectionLastNonWhiteAction = "selection-last-non-white";
    public static final String selectIdentifierAction = "select-identifier";
    public static final String selectNextParameterAction = "select-next-parameter";
    public static final String jumpListNextAction = "jump-list-next";
    public static final String jumpListPrevAction = "jump-list-prev";
    public static final String jumpListNextComponentAction = "jump-list-next-component";
    public static final String jumpListPrevComponentAction = "jump-list-prev-component";
    public static final String scrollUpAction = "scroll-up";
    public static final String scrollDownAction = "scroll-down";
    public static final String macroActionPrefix = "macro-";
    public static final String startMacroRecordingAction = "start-macro-recording";
    public static final String stopMacroRecordingAction = "stop-macro-recording";
    public static final String lineFirstColumnAction = "caret-line-first-column";
    public static final String insertDateTimeAction = "insert-date-time";
    public static final String selectionLineFirstColumnAction = "selection-line-first-column";
    public static final String generateGutterPopupAction = "generate-gutter-popup";
    public static final String toggleLineNumbersAction = "toggle-line-numbers";
    public static final String pasteFormatedAction = "paste-formated";
    public static final String startNewLineAction = "start-new-line";
    public static final String cutToLineBeginAction = "cut-to-line-begin";
    public static final String cutToLineEndAction = "cut-to-line-end";
    public static final String removeTrailingSpacesAction = "remove-trailing-spaces";
    public static final String DOC_REPLACE_SELECTION_PROPERTY = "doc-replace-selection-property";
    private static final int KIT_CNT_PREALLOC = 7;
    static final long serialVersionUID = -8570495408376659348L;
    private static final Map<Class, BaseKit> kits = new HashMap<Class, BaseKit>(7);
    private static final Object KEYMAPS_AND_ACTIONS_LOCK = new String("BaseKit.KEYMAPS_AND_ACTIONS_LOCK");
    private static final Map<MimePath, KeybindingsAndPreferencesTracker> keymapTrackers = new WeakHashMap<MimePath, KeybindingsAndPreferencesTracker>(7);
    private static final Map<MimePath, MultiKeymap> kitKeymaps = new WeakHashMap<MimePath, MultiKeymap>(7);
    private static final Map<MimePath, Action[]> kitActions = new WeakHashMap<MimePath, Action[]>(7);
    private static final Map<MimePath, Map<String, Action>> kitActionMaps = new WeakHashMap<MimePath, Map<String, Action>>(7);
    private static CopyAction copyActionDef = new CopyAction();
    private static CutAction cutActionDef = new CutAction();
    private static PasteAction pasteActionDef = new PasteAction(false);
    private static DeleteCharAction deletePrevCharActionDef = new DeleteCharAction("delete-previous", false);
    private static DeleteCharAction deleteNextCharActionDef = new DeleteCharAction("delete-next", true);
    private static ActionFactory.RemoveSelectionAction removeSelectionActionDef = new ActionFactory.RemoveSelectionAction();
    private static final Action insertTabActionDef = new InsertTabAction();
    private static final Action removeTabActionDef = new ActionFactory.RemoveTabAction();
    private static final Action insertBreakActionDef = new InsertBreakAction();
    private static ActionFactory.UndoAction undoActionDef = new ActionFactory.UndoAction();
    private static ActionFactory.RedoAction redoActionDef = new ActionFactory.RedoAction();
    public static final int MAGIC_POSITION_MAX = 2147483646;
    private final SearchableKit searchableKit;
    private boolean keyBindingsUpdaterInited;
    private static final String PROP_NAVIGATE_BOUNDARIES = "NetBeansEditor.navigateBoundaries";

    public static BaseKit getKit(Class kitClass) {
        Object mimeType;
        if (kitClass != null && BaseKit.class.isAssignableFrom(kitClass) && BaseKit.class != kitClass) {
            EditorKit kit;
            mimeType = KitsTracker.getInstance().findMimeType(kitClass);
            if (mimeType != null && (kit = (EditorKit)MimeLookup.getLookup((MimePath)MimePath.parse((String)mimeType)).lookup(EditorKit.class)) instanceof BaseKit) {
                return (BaseKit)kit;
            }
        } else {
            kitClass = BaseKit.class;
        }
        mimeType = kits;
        synchronized (mimeType) {
            BaseKit kit;
            Class<BaseKit> classToTry = kitClass;
            while ((kit = kits.get(classToTry)) == null) {
                try {
                    kit = classToTry.newInstance();
                    kits.put(classToTry, kit);
                    return kit;
                }
                catch (IllegalAccessException e) {
                    LOG.log(Level.WARNING, "Can't instantiate editor kit from: " + classToTry, e);
                }
                catch (InstantiationException e) {
                    LOG.log(Level.WARNING, "Can't instantiate editor kit from: " + classToTry, e);
                }
                if (classToTry != BaseKit.class) {
                    classToTry = BaseKit.class;
                    continue;
                }
                throw new IllegalStateException("Can't create editor kit for: " + kitClass);
            }
            return kit;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public BaseKit() {
        Map<Class, BaseKit> map = kits;
        synchronized (map) {
            if (kits.get(this.getClass()) == null) {
                kits.put(this.getClass(), this);
            }
        }
        this.searchableKit = new SearchableKit(this);
        EditorActionUtilities.registerSearchableKit((EditorKit)this, (SearchableEditorKit)this.searchableKit);
    }

    @Override
    public Object clone() {
        return this;
    }

    @Override
    public ViewFactory getViewFactory() {
        return ViewFactoryImpl.INSTANCE;
    }

    @Override
    public Caret createCaret() {
        return new BaseCaret();
    }

    @Override
    public Document createDefaultDocument() {
        return new BaseDocument(this.getClass(), true);
    }

    public Syntax createSyntax(Document doc) {
        return new DefaultSyntax();
    }

    public Syntax createFormatSyntax(Document doc) {
        return this.createSyntax(doc);
    }

    public SyntaxSupport createSyntaxSupport(BaseDocument doc) {
        return new SyntaxSupport(doc);
    }

    protected BaseTextUI createTextUI() {
        return new BaseTextUI();
    }

    protected EditorUI createEditorUI() {
        return new EditorUI();
    }

    protected EditorUI createPrintEditorUI(BaseDocument doc) {
        return new EditorUI(doc);
    }

    protected EditorUI createPrintEditorUI(BaseDocument doc, boolean usePrintColoringMap, boolean lineNumberEnabled) {
        return new EditorUI(doc, usePrintColoringMap, lineNumberEnabled);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public MultiKeymap getKeymap() {
        Object object = KEYMAPS_AND_ACTIONS_LOCK;
        synchronized (object) {
            MimePath mimePath = MimePath.parse((String)this.getContentType());
            MultiKeymap km = kitKeymaps.get((Object)mimePath);
            if (km == null) {
                km = new MultiKeymap("Keymap for " + mimePath.getPath());
                KeyBindingSettings kbs = (KeyBindingSettings)MimeLookup.getLookup((MimePath)mimePath).lookup(KeyBindingSettings.class);
                List mkbList = kbs.getKeyBindings();
                ArrayList<MultiKeyBinding> editorMkbList = new ArrayList<MultiKeyBinding>();
                for (org.netbeans.api.editor.settings.MultiKeyBinding mkb : mkbList) {
                    List keyStrokes = mkb.getKeyStrokeList();
                    MultiKeyBinding editorMkb = new MultiKeyBinding(keyStrokes.toArray(new KeyStroke[keyStrokes.size()]), mkb.getActionName());
                    editorMkbList.add(editorMkb);
                }
                km.load(editorMkbList.toArray(new JTextComponent.KeyBinding[editorMkbList.size()]), this.getActionMap());
                km.setDefaultAction(this.getActionMap().get("default-typed"));
                kitKeymaps.put(mimePath, km);
            }
            return km;
        }
    }

    @Override
    public void read(Reader in, Document doc, int pos) throws IOException, BadLocationException {
        if (doc instanceof BaseDocument) {
            ((BaseDocument)doc).read(in, pos);
        } else {
            super.read(in, doc, pos);
        }
    }

    @Override
    public void write(Writer out, Document doc, int pos, int len) throws IOException, BadLocationException {
        if (doc instanceof BaseDocument) {
            ((BaseDocument)doc).write(out, pos, len);
        } else {
            super.write(out, doc, pos, len);
        }
    }

    public static void addActionsToMap(Map<String, Action> map, Action[] actions, String logActionsType) {
        boolean fineLoggable = LOG.isLoggable(Level.FINE);
        if (fineLoggable) {
            LOG.fine(logActionsType + " start --------------------\n");
        }
        for (int i = 0; i < actions.length; ++i) {
            Action a = actions[i];
            if (a == null) {
                LOG.info("actions[] contains null at index " + i + (i > 0 ? new StringBuilder().append(". Preceding action is ").append(actions[i - 1]).toString() : "."));
                continue;
            }
            String name = (String)a.getValue("Name");
            if (name == null) {
                LOG.info("Null Action.NAME property of action " + a);
                continue;
            }
            if (fineLoggable) {
                String overriding = map.containsKey(name) ? " OVERRIDING\n" : "\n";
                LOG.fine("    " + name + ": " + a + overriding);
            }
            map.put(name, a);
        }
        if (fineLoggable) {
            LOG.fine(logActionsType + " end ----------------------\n");
        }
    }

    public static Action[] mapToActions(Map map) {
        Action[] actions = new Action[map.size()];
        int i = 0;
        for (Action actions[i++] : map.values()) {
        }
        return actions;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void install(JEditorPane c) {
        boolean enableIM;
        MultiKeymap keymap;
        assert (SwingUtilities.isEventDispatchThread());
        BaseTextUI ui = this.createTextUI();
        c.setUI(ui);
        String propName = "netbeans.editor.noinputmethods";
        String noInputMethods = System.getProperty(propName);
        if (noInputMethods != null) {
            enableIM = !Boolean.getBoolean(propName);
        } else {
            Preferences prefs = (Preferences)MimeLookup.getLookup((String)this.getContentType()).lookup(Preferences.class);
            enableIM = prefs.getBoolean("input-methods-enabled", true);
        }
        c.enableInputMethods(enableIM);
        HyperlinkOperation.ensureRegistered(c, this.getContentType());
        c.putClientProperty("context-api-aware", Boolean.TRUE);
        if (!(this instanceof HelpCtx.Provider)) {
            HelpCtx.setHelpIDString((JComponent)c, (String)this.getContentType().replace('/', '.').replace('+', '.'));
        }
        Object object = KEYMAPS_AND_ACTIONS_LOCK;
        synchronized (object) {
            MimePath mimePath = MimePath.get((String)this.getContentType());
            KeybindingsAndPreferencesTracker tracker = keymapTrackers.get((Object)mimePath);
            if (tracker == null) {
                tracker = new KeybindingsAndPreferencesTracker(mimePath.getPath());
                keymapTrackers.put(mimePath, tracker);
            }
            tracker.addComponent(c);
            keymap = this.getKeymap();
        }
        c.setKeymap(keymap);
        c.addAncestorListener(new AncestorListener(){
            private JScrollPane scrollPane;
            private InputMap origMap;
            int condition;

            @Override
            public void ancestorAdded(AncestorEvent event) {
                Component c = (Component)event.getSource();
                Container parent = c.getParent();
                if (parent instanceof JViewport) {
                    c = parent;
                    parent = c.getParent();
                    if (parent instanceof JScrollPane) {
                        this.scrollPane = (JScrollPane)parent;
                        this.origMap = this.scrollPane.getInputMap(this.condition);
                        this.scrollPane.setInputMap(this.condition, null);
                    }
                }
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                if (this.scrollPane != null && this.scrollPane.getInputMap(this.condition) == null) {
                    this.scrollPane.setInputMap(this.condition, this.origMap);
                }
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {
            }
        });
        this.executeInstallActions(c);
    }

    protected void executeInstallActions(JEditorPane c) {
        MimePath mimePath = MimePath.parse((String)this.getContentType());
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        List actionNamesList = (List)SettingsConversions.callFactory(prefs, mimePath, "kit-install-action-name-list", null);
        List<Action> actionsList = this.translateActionNameList(actionNamesList);
        for (Action a : actionsList) {
            a.actionPerformed(new ActionEvent(c, 1001, ""));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void deinstall(JEditorPane c) {
        assert (SwingUtilities.isEventDispatchThread());
        this.executeDeinstallActions(c);
        c.setKeymap(null);
        Object object = KEYMAPS_AND_ACTIONS_LOCK;
        synchronized (object) {
            MimePath mimePath = MimePath.get((String)this.getContentType());
            KeybindingsAndPreferencesTracker tracker = keymapTrackers.get((Object)mimePath);
            if (tracker != null) {
                tracker.removeComponent(c);
            }
        }
        BaseTextUI.uninstallUIWatcher(c);
        c.updateUI();
        if (c.getClientProperty("ancestorOverride") != null) {
            c.putClientProperty("ancestorOverride", Boolean.FALSE);
        }
    }

    protected void executeDeinstallActions(JEditorPane c) {
        MimePath mimePath = MimePath.parse((String)this.getContentType());
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        List actionNamesList = (List)SettingsConversions.callFactory(prefs, mimePath, "kit-deinstall-action-name-list", null);
        List<Action> actionsList = this.translateActionNameList(actionNamesList);
        for (Action a : actionsList) {
            a.actionPerformed(new ActionEvent(c, 1001, ""));
        }
    }

    protected void initDocument(BaseDocument doc) {
    }

    protected Action[] createActions() {
        return new Action[]{insertBreakActionDef, insertTabActionDef, deletePrevCharActionDef, deleteNextCharActionDef, cutActionDef, copyActionDef, pasteActionDef, new PasteAction(true), removeTabActionDef, removeSelectionActionDef, undoActionDef, redoActionDef, new ActionFactory.ToggleRectangularSelectionAction()};
    }

    protected Action[] getCustomActions() {
        MimePath mimePath = MimePath.parse((String)this.getContentType());
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        List customActions = (List)SettingsConversions.callFactory(prefs, mimePath, "custom-action-list", null);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("custom-action-list for '" + this.getContentType() + "' {");
            if (customActions != null) {
                for (Action a : customActions) {
                    LOG.fine("    " + a);
                }
            }
            LOG.fine("} End of custom-action-list for '" + this.getContentType() + "'");
        }
        return customActions == null ? null : customActions.toArray(new Action[customActions.size()]);
    }

    protected Action[] getMacroActions() {
        return new Action[0];
    }

    @Override
    public final Action[] getActions() {
        Action[] actions = (Action[])this.addActionsToMap()[0];
        if (!this.keyBindingsUpdaterInited) {
            this.keyBindingsUpdaterInited = true;
            KeyBindingsUpdater.get((String)this.getContentType()).addKit((EditorKit)this);
        }
        return actions;
    }

    Map<String, Action> getActionMap() {
        return (Map)this.addActionsToMap()[1];
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Object[] addActionsToMap() {
        Object object = KEYMAPS_AND_ACTIONS_LOCK;
        synchronized (object) {
            MimePath mimePath = MimePath.parse((String)this.getContentType());
            Action[] actions = kitActions.get((Object)mimePath);
            Map<String, Action> actionMap = kitActionMaps.get((Object)mimePath);
            if (actions == null || actionMap == null) {
                Action[] customActions;
                actions = this.getDeclaredActions();
                actionMap = new HashMap<String, Action>(actions.length << 1);
                BaseKit.addActionsToMap(actionMap, actions, "Declared actions");
                Action[] createActionsMethodResult = this.createActions();
                if (createActionsMethodResult != null) {
                    BaseKit.addActionsToMap(actionMap, createActionsMethodResult, "Actions from createActions()");
                }
                if ((customActions = this.getCustomActions()) != null) {
                    BaseKit.addActionsToMap(actionMap, customActions, "Custom actions");
                }
                actions = actionMap.values().toArray(new Action[actionMap.values().size()]);
                kitActions.put(mimePath, actions);
                kitActionMaps.put(mimePath, actionMap);
                this.updateActions();
            }
            return new Object[]{actions, actionMap};
        }
    }

    protected Action[] getDeclaredActions() {
        return new Action[0];
    }

    protected void updateActions() {
    }

    public Action getActionByName(String name) {
        return name != null ? this.getActionMap().get(name) : null;
    }

    public List<Action> translateActionNameList(List<String> actionNameList) {
        ArrayList<Action> ret = new ArrayList<Action>();
        if (actionNameList != null) {
            for (String actionName : actionNameList) {
                Action a = this.getActionByName(actionName);
                if (a == null) continue;
                ret.add(a);
            }
        }
        return ret;
    }

    static boolean isValidDefaultTypedAction(ActionEvent evt) {
        boolean ctrl;
        int mod = evt.getModifiers();
        boolean bl = ctrl = (mod & 2) != 0;
        boolean alt = org.openide.util.Utilities.isMac() ? (mod & 4) != 0 : (mod & 8) != 0;
        return !alt && !ctrl;
    }

    static boolean isValidDefaultTypedCommand(ActionEvent evt) {
        String cmd = evt.getActionCommand();
        return cmd != null && cmd.length() == 1 && cmd.charAt(0) >= ' ' && cmd.charAt(0) != '';
    }

    static void insertTabString(final BaseDocument doc, final int dotPos) throws BadLocationException {
        final BadLocationException[] badLocationExceptions = new BadLocationException[1];
        doc.runAtomic(new Runnable(){

            @Override
            public void run() {
                try {
                    int ind;
                    int rsPos = Utilities.getRowStart(doc, dotPos);
                    int startPos = Utilities.getFirstNonWhiteBwd(doc, dotPos, rsPos);
                    startPos = startPos >= 0 ? startPos + 1 : rsPos;
                    int startCol = Utilities.getVisualColumn(doc, startPos);
                    int endCol = Utilities.getNextTabColumn(doc, dotPos);
                    Preferences prefs = CodeStylePreferences.get((Document)doc).getPreferences();
                    String tabStr = Analyzer.getWhitespaceString(startCol, endCol, prefs.getBoolean("expand-tabs", true), prefs.getInt("tab-size", 8));
                    char[] removeChars = doc.getChars(startPos, dotPos - startPos);
                    for (ind = 0; ind < removeChars.length && removeChars[ind] == tabStr.charAt(ind); ++ind) {
                    }
                    doc.remove(startPos, dotPos - (startPos += ind));
                    doc.insertString(startPos, tabStr.substring(ind), null);
                }
                catch (BadLocationException ex) {
                    badLocationExceptions[0] = ex;
                }
            }
        });
        if (badLocationExceptions[0] != null) {
            throw badLocationExceptions[0];
        }
    }

    static void changeRowIndent(final BaseDocument doc, final int pos, final int newIndent) throws BadLocationException {
        final BadLocationException[] badLocationExceptions = new BadLocationException[1];
        doc.runAtomic(new Runnable(){

            @Override
            public void run() {
                try {
                    int indent = newIndent < 0 ? 0 : newIndent;
                    int firstNW = Utilities.getRowFirstNonWhite(doc, pos);
                    if (firstNW == -1) {
                        firstNW = Utilities.getRowEnd(doc, pos);
                    }
                    int replacePos = Utilities.getRowStart(doc, pos);
                    int removeLen = firstNW - replacePos;
                    CharSequence removeText = DocumentUtilities.getText((Document)doc, (int)replacePos, (int)removeLen);
                    String newIndentText = IndentUtils.createIndentString((Document)doc, (int)indent);
                    int newIndentTextLength = newIndentText.length();
                    if (indent >= removeLen) {
                        if (CharSequenceUtilities.startsWith((CharSequence)newIndentText, (CharSequence)removeText)) {
                            newIndentText = newIndentText.substring(removeLen);
                            replacePos += removeLen;
                            removeLen = 0;
                        } else if (CharSequenceUtilities.endsWith((CharSequence)newIndentText, (CharSequence)removeText)) {
                            newIndentText = newIndentText.substring(0, newIndentText.length() - removeLen);
                            removeLen = 0;
                        }
                    } else if (CharSequenceUtilities.startsWith((CharSequence)removeText, (CharSequence)newIndentText)) {
                        replacePos += newIndentTextLength;
                        removeLen -= newIndentTextLength;
                        newIndentText = null;
                    } else if (CharSequenceUtilities.endsWith((CharSequence)removeText, (CharSequence)newIndentText)) {
                        removeLen -= newIndentTextLength;
                        newIndentText = null;
                    }
                    if (removeLen != 0) {
                        doc.remove(replacePos, removeLen);
                    }
                    if (newIndentText != null) {
                        doc.insertString(replacePos, newIndentText, null);
                    }
                }
                catch (BadLocationException ex) {
                    badLocationExceptions[0] = ex;
                }
            }
        });
        if (badLocationExceptions[0] != null) {
            throw badLocationExceptions[0];
        }
    }

    static void changeBlockIndent(final BaseDocument doc, final int startPos, final int endPos, final int shiftCnt) throws BadLocationException {
        GuardedDocument gdoc;
        GuardedDocument guardedDocument = gdoc = doc instanceof GuardedDocument ? (GuardedDocument)doc : null;
        if (gdoc != null) {
            for (int i = startPos; i < endPos; ++i) {
                if (!gdoc.isPosGuarded(i)) continue;
                Toolkit.getDefaultToolkit().beep();
                return;
            }
        }
        final BadLocationException[] badLocationExceptions = new BadLocationException[1];
        doc.runAtomic(new Runnable(){

            @Override
            public void run() {
                try {
                    int shiftWidth = doc.getShiftWidth();
                    if (shiftWidth <= 0) {
                        return;
                    }
                    int indentDelta = shiftCnt * shiftWidth;
                    int end = endPos > 0 && Utilities.getRowStart(doc, endPos) == endPos ? endPos - 1 : endPos;
                    int lineStartOffset = Utilities.getRowStart(doc, startPos);
                    int lineCount = Utilities.getRowCount(doc, startPos, end);
                    Integer delta = null;
                    for (int i = lineCount - 1; i >= 0; --i) {
                        int indent = Utilities.getRowIndent(doc, lineStartOffset);
                        if (indent >= 0 && delta == null) {
                            delta = (indent + indentDelta + (shiftCnt < 0 ? shiftWidth - 1 : 0)) / shiftWidth * shiftWidth - indent;
                        }
                        int newIndent = indent < 0 ? 0 : indent + delta;
                        BaseKit.changeRowIndent(doc, lineStartOffset, Math.max(newIndent, 0));
                        lineStartOffset = Utilities.getRowStart(doc, lineStartOffset, 1);
                    }
                }
                catch (BadLocationException ex) {
                    badLocationExceptions[0] = ex;
                }
            }
        });
        if (badLocationExceptions[0] != null) {
            throw badLocationExceptions[0];
        }
    }

    static void shiftLine(BaseDocument doc, int dotPos, boolean right) throws BadLocationException {
        int ind = doc.getShiftWidth();
        if (!right) {
            ind = - ind;
        }
        ind = Utilities.isRowWhite(doc, dotPos) ? (ind += Utilities.getVisualColumn(doc, dotPos)) : (ind += Utilities.getRowIndent(doc, dotPos));
        ind = Math.max(ind, 0);
        BaseKit.changeRowIndent(doc, dotPos, ind);
    }

    static void shiftBlock(final BaseDocument doc, final int startPos, final int endPos, final boolean right) throws BadLocationException {
        GuardedDocument gdoc;
        GuardedDocument guardedDocument = gdoc = doc instanceof GuardedDocument ? (GuardedDocument)doc : null;
        if (gdoc != null) {
            for (int i = startPos; i < endPos; ++i) {
                if (!gdoc.isPosGuarded(i)) continue;
                Toolkit.getDefaultToolkit().beep();
                return;
            }
        }
        final BadLocationException[] badLocationExceptions = new BadLocationException[1];
        doc.runAtomic(new Runnable(){

            @Override
            public void run() {
                try {
                    int shiftWidth = doc.getShiftWidth();
                    if (shiftWidth <= 0) {
                        return;
                    }
                    int indentDelta = right ? shiftWidth : - shiftWidth;
                    int end = endPos > 0 && Utilities.getRowStart(doc, endPos) == endPos ? endPos - 1 : endPos;
                    int lineStartOffset = Utilities.getRowStart(doc, startPos);
                    int lineCount = Utilities.getRowCount(doc, startPos, end);
                    for (int i = lineCount - 1; i >= 0; --i) {
                        int indent = Utilities.getRowIndent(doc, lineStartOffset);
                        int newIndent = indent == -1 ? 0 : indent + indentDelta;
                        BaseKit.changeRowIndent(doc, lineStartOffset, Math.max(newIndent, 0));
                        lineStartOffset = Utilities.getRowStart(doc, lineStartOffset, 1);
                    }
                }
                catch (BadLocationException ex) {
                    badLocationExceptions[0] = ex;
                }
            }
        });
        if (badLocationExceptions[0] != null) {
            throw badLocationExceptions[0];
        }
    }

    private static final class SearchableKit
    implements SearchableEditorKit {
        private final BaseKit baseKit;
        private final ListenerList<ChangeListener> actionsListenerList = new ListenerList();

        SearchableKit(BaseKit baseKit) {
            this.baseKit = baseKit;
        }

        public Action getAction(String actionName) {
            return this.baseKit.getActionByName(actionName);
        }

        public void addActionsChangeListener(ChangeListener listener) {
            this.actionsListenerList.add((EventListener)listener);
        }

        public void removeActionsChangeListener(ChangeListener listener) {
            this.actionsListenerList.remove((EventListener)listener);
        }

        void fireActionsChange() {
            ChangeEvent evt = new ChangeEvent(this);
            for (ChangeListener listener : this.actionsListenerList.getListeners()) {
                listener.stateChanged(evt);
            }
        }
    }

    private class KeybindingsAndPreferencesTracker
    implements LookupListener,
    PreferenceChangeListener {
        private final String mimeType;
        private final Lookup.Result<KeyBindingSettings> lookupResult;
        private final Preferences prefs;
        private final Set<JTextComponent> components;

        public KeybindingsAndPreferencesTracker(String mimeType) {
            this.components = new WeakSet();
            this.mimeType = mimeType;
            Lookup lookup = MimeLookup.getLookup((String)mimeType);
            this.lookupResult = lookup.lookupResult(KeyBindingSettings.class);
            this.lookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this.lookupResult));
            this.lookupResult.allInstances();
            this.prefs = (Preferences)lookup.lookup(Preferences.class);
            this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.prefs));
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addComponent(JTextComponent c) {
            Object object = KEYMAPS_AND_ACTIONS_LOCK;
            synchronized (object) {
                assert (c != null);
                this.components.add(c);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void removeComponent(JTextComponent c) {
            Object object = KEYMAPS_AND_ACTIONS_LOCK;
            synchronized (object) {
                this.components.remove(c);
            }
        }

        public void resultChanged(LookupEvent ev) {
            this.refreshShortcutsAndActions(false);
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            String settingName;
            String string = settingName = evt == null ? null : evt.getKey();
            if (settingName == null || "custom-action-list".equals(settingName)) {
                this.refreshShortcutsAndActions(true);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void refreshShortcutsAndActions(boolean refreshActions) {
            MultiKeymap keymap;
            JTextComponent[] arr;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("BaseKit.KeymapTracker('" + this.mimeType + "') refreshing keymap " + (refreshActions ? "and actions" : ""));
            }
            Object object = KEYMAPS_AND_ACTIONS_LOCK;
            synchronized (object) {
                MimePath mimePath = MimePath.parse((String)this.mimeType);
                if (refreshActions) {
                    kitActions.remove((Object)mimePath);
                    kitActionMaps.remove((Object)mimePath);
                }
                kitKeymaps.remove((Object)mimePath);
                keymap = BaseKit.this.getKeymap();
                arr = this.components.toArray(new JTextComponent[this.components.size()]);
            }
            for (JTextComponent c : arr) {
                if (c == null) continue;
                c.setKeymap(keymap);
            }
            BaseKit.this.searchableKit.fireActionsChange();
        }
    }

    static final class DefaultSyntaxTokenContext
    extends TokenContext {
        public static final int TEXT_ID = 1;
        public static final int EOL_ID = 2;
        public static final BaseTokenID TEXT = new BaseTokenID("text", 1);
        public static final BaseImageTokenID EOL = new BaseImageTokenID("EOL", 2, "\n");
        public static final DefaultSyntaxTokenContext CONTEXT = new DefaultSyntaxTokenContext();

        private DefaultSyntaxTokenContext() {
            super("defaultSyntax-token-");
            try {
                this.addDeclaredTokenIDs();
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, "Can't load token IDs", e);
            }
        }
    }

    static final class DefaultSyntax
    extends Syntax {
        private static final int ISI_TEXT = 0;

        public DefaultSyntax() {
            this.tokenContextPath = DefaultSyntaxTokenContext.CONTEXT.getContextPath();
        }

        @Override
        protected TokenID parseToken() {
            while (this.offset < this.stopOffset) {
                char ch = this.buffer[this.offset];
                switch (this.state) {
                    case -1: {
                        switch (ch) {
                            case '\n': {
                                ++this.offset;
                                return DefaultSyntaxTokenContext.EOL;
                            }
                        }
                        this.state = 0;
                        break;
                    }
                    case 0: {
                        switch (ch) {
                            case '\n': {
                                this.state = -1;
                                return DefaultSyntaxTokenContext.TEXT;
                            }
                        }
                    }
                }
                ++this.offset;
            }
            switch (this.state) {
                case 0: {
                    this.state = -1;
                    return DefaultSyntaxTokenContext.TEXT;
                }
            }
            return null;
        }
    }

    public static class RemoveTrailingSpacesAction
    extends LocalBaseAction {
        @Override
        protected boolean asynchonous() {
            return true;
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomic(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            Element lineRootElem = doc.getDefaultRootElement();
                            int count = lineRootElem.getElementCount();
                            boolean removed = false;
                            for (int x = 0; x < count; ++x) {
                                int end;
                                int index;
                                Element elem = lineRootElem.getElement(x);
                                int start = elem.getStartOffset();
                                CharSequence line = DocumentUtilities.getText((Document)doc, (int)start, (int)((end = elem.getEndOffset()) - start));
                                int endIndex = line.length() - 1;
                                if (endIndex >= 0 && line.charAt(endIndex) == '\n' && --endIndex >= 0 && line.charAt(endIndex) == '\r') {
                                    --endIndex;
                                }
                                for (index = endIndex; index >= 0 && Character.isWhitespace(line.charAt(index)) && line.charAt(index) != '\n'; --index) {
                                }
                                if (index >= endIndex) continue;
                                doc.remove(start + index + 1, endIndex - index);
                                removed = true;
                            }
                            if (removed) {
                                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(BaseKit.class, (String)"TrailingSpacesWereRemoved_Lbl"));
                            } else {
                                StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(BaseKit.class, (String)"TrailingSpacesWereNotRemoved_Lbl"));
                            }
                        }
                        catch (BadLocationException e) {
                            e.printStackTrace();
                            target.getToolkit().beep();
                        }
                    }
                });
            }
        }

    }

    public static class SelectAllAction
    extends KitCompoundAction {
        static final long serialVersionUID = -3502499718130556524L;

        public SelectAllAction() {
            super(new String[]{"caret-begin", "selection-end"});
        }
    }

    public static class SelectLineAction
    extends LocalBaseAction {
        static final long serialVersionUID = -7407681863035740281L;

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int dotPos = caret.getDot();
                            int bolPos = Utilities.getRowStart(target, dotPos);
                            int eolPos = Utilities.getRowEnd(target, dotPos);
                            eolPos = Math.min(eolPos + 1, doc.getLength());
                            caret.setDot(bolPos);
                            caret.moveDot(eolPos);
                        }
                        catch (BadLocationException e) {
                            target.getToolkit().beep();
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class SelectWordAction
    extends KitCompoundAction {
        static final long serialVersionUID = 7678848538073016357L;

        public SelectWordAction() {
            super(new String[]{"caret-begin-word", "selection-end-word"});
        }
    }

    public static class EndWordAction
    extends LocalBaseAction {
        static final long serialVersionUID = 3812523676620144633L;

        public EndWordAction() {
            super(62);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    int dot = Utilities.getWordEnd(target, caret.getDot());
                    boolean select = "selection-end-word".equals(this.getValue("Name"));
                    if (select) {
                        caret.moveDot(dot);
                    } else {
                        caret.setDot(dot);
                    }
                }
                catch (BadLocationException ex) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class BeginWordAction
    extends LocalBaseAction {
        static final long serialVersionUID = 3991338381212491110L;

        public BeginWordAction() {
            super(62);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    int dot = Utilities.getWordStart(target, caret.getDot());
                    boolean select = "selection-begin-word".equals(this.getValue("Name"));
                    if (select) {
                        caret.moveDot(dot);
                    } else {
                        caret.setDot(dot);
                    }
                }
                catch (BadLocationException ex) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class PreviousWordAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5465143382669785799L;

        public PreviousWordAction(String name) {
            super(name, 62);
        }

        protected int getPreviousWordOffset(JTextComponent target) throws BadLocationException {
            return Utilities.getPreviousWord(target, target.getCaretPosition());
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    int newDotOffset = this.getPreviousWordOffset(target);
                    boolean select = "selection-previous-word".equals(this.getValue("Name"));
                    if (select) {
                        if (caret instanceof BaseCaret && RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                            ((BaseCaret)caret).extendRectangularSelection(false, true);
                        } else {
                            caret.moveDot(newDotOffset);
                        }
                    } else if (caret instanceof BaseCaret) {
                        ((BaseCaret)caret).setDot(newDotOffset, false);
                    } else {
                        caret.setDot(newDotOffset);
                    }
                }
                catch (BadLocationException ex) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class NextWordAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5909906947175434032L;

        public NextWordAction(String name) {
            super(name, 62);
        }

        protected int getNextWordOffset(JTextComponent target) throws BadLocationException {
            return Utilities.getNextWord(target, target.getCaretPosition());
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    int newDotOffset = this.getNextWordOffset(target);
                    boolean select = "selection-next-word".equals(this.getValue("Name"));
                    if (select) {
                        if (caret instanceof BaseCaret && RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                            ((BaseCaret)caret).extendRectangularSelection(true, true);
                        } else {
                            caret.moveDot(newDotOffset);
                        }
                    } else if (caret instanceof BaseCaret) {
                        ((BaseCaret)caret).setDot(newDotOffset, false);
                    } else {
                        caret.setDot(newDotOffset);
                    }
                }
                catch (BadLocationException ex) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class EndAction
    extends LocalBaseAction {
        static final long serialVersionUID = 8547506353130203657L;

        public EndAction() {
            super(190);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Rectangle bounds = target.getBounds();
                bounds.x = 0;
                bounds.y = bounds.height;
                bounds.width = 1;
                bounds.height = 1;
                target.scrollRectToVisible(bounds);
                Caret caret = target.getCaret();
                int dot = target.getDocument().getLength();
                boolean select = "selection-end".equals(this.getValue("Name"));
                if (select) {
                    caret.moveDot(dot);
                } else {
                    caret.setDot(dot);
                }
            }
        }
    }

    public static class BeginAction
    extends LocalBaseAction {
        static final long serialVersionUID = 3463563396210234361L;

        public BeginAction() {
            super(190);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                int dot = 0;
                boolean select = "selection-begin".equals(this.getValue("Name"));
                if (select) {
                    caret.moveDot(dot);
                } else {
                    caret.setDot(dot);
                }
            }
        }
    }

    public static class EndLineAction
    extends LocalBaseAction {
        static final long serialVersionUID = 5216077634055190170L;

        public EndLineAction() {
            super(62);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    Object o = target.getClientProperty("NetBeansEditor.navigateBoundaries");
                    int dot = -1;
                    if (o instanceof PositionRegion) {
                        PositionRegion bounds = (PositionRegion)o;
                        int start = bounds.getStartOffset();
                        int end = bounds.getEndOffset();
                        int d = caret.getDot();
                        if (d >= start && d < end) {
                            dot = end;
                        }
                    }
                    if (dot == -1) {
                        dot = Utilities.getRowEnd(target, caret.getDot());
                    }
                    dot = Math.min(dot, target.getUI().getRootView(target).getEndOffset());
                    boolean select = "selection-end-line".equals(this.getValue("Name"));
                    if (select) {
                        caret.moveDot(dot);
                    } else {
                        caret.setDot(dot);
                    }
                    Rectangle r = target.modelToView(dot);
                    if (r != null) {
                        Point p = new Point(2147483646, r.y);
                        caret.setMagicCaretPosition(p);
                    }
                }
                catch (BadLocationException e) {
                    e.printStackTrace();
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class BeginLineAction
    extends LocalBaseAction {
        boolean homeKeyColumnOne;
        static final long serialVersionUID = 3269462923524077779L;

        public static BeginLineAction create() {
            return new BeginLineAction(false);
        }

        public static BeginLineAction createColumnOne() {
            return new BeginLineAction(true);
        }

        public BeginLineAction(boolean columnOne) {
            super(62);
            this.homeKeyColumnOne = columnOne;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                BaseDocument doc = (BaseDocument)target.getDocument();
                try {
                    int dot = caret.getDot();
                    Object o = target.getClientProperty("NetBeansEditor.navigateBoundaries");
                    PositionRegion bounds = null;
                    if (o instanceof PositionRegion) {
                        bounds = (PositionRegion)o;
                        int start = bounds.getStartOffset();
                        int end = bounds.getEndOffset();
                        if (dot > start && dot <= end) {
                            dot = start;
                        } else {
                            bounds = null;
                        }
                    }
                    if (bounds == null) {
                        int lineStartPos = Utilities.getRowStart(target, dot);
                        if (this.homeKeyColumnOne) {
                            dot = lineStartPos;
                        } else {
                            int textStartPos = Utilities.getRowFirstNonWhite(doc, lineStartPos);
                            if (textStartPos < 0) {
                                textStartPos = Utilities.getRowEnd(target, lineStartPos);
                            }
                            dot = dot == lineStartPos ? textStartPos : (dot <= textStartPos ? lineStartPos : textStartPos);
                        }
                    }
                    dot = Math.max(dot, target.getUI().getRootView(target).getStartOffset());
                    String actionName = (String)this.getValue("Name");
                    boolean select = "selection-begin-line".equals(actionName) || "selection-line-first-column".equals(actionName);
                    Rectangle r = target.modelToView(dot);
                    Rectangle visRect = target.getVisibleRect();
                    if (r.getMaxX() < visRect.getWidth()) {
                        r.x = 0;
                        target.scrollRectToVisible(r);
                    }
                    if (select) {
                        caret.moveDot(dot);
                    } else {
                        caret.setDot(dot);
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class BackwardAction
    extends LocalBaseAction {
        static final long serialVersionUID = -3048379822817847356L;

        public BackwardAction() {
            super(62);
        }

        /*
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target == null) return;
            Caret caret = target.getCaret();
            try {
                int pos;
                boolean select = "selection-backward".equals(this.getValue("Name"));
                if (!select && Utilities.isSelectionShowing(caret)) {
                    pos = target.getSelectionStart();
                    if (pos == caret.getDot()) {
                        caret.setDot(pos);
                        return;
                    }
                    ++pos;
                } else {
                    pos = caret.getDot();
                }
                int dot = target.getUI().getNextVisualPositionFrom(target, pos, Position.Bias.Backward, 7, null);
                if (!select) {
                    caret.setDot(dot);
                    return;
                }
                if (caret instanceof BaseCaret && RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                    ((BaseCaret)caret).extendRectangularSelection(false, false);
                    return;
                }
                caret.moveDot(dot);
                return;
            }
            catch (BadLocationException ex) {
                target.getToolkit().beep();
            }
        }
    }

    public static class PageDownAction
    extends LocalBaseAction {
        static final long serialVersionUID = 8942534850985048862L;

        public PageDownAction() {
            super(60);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                try {
                    Rectangle newCaretBounds;
                    Rectangle visibleBounds;
                    int newCaretOffset;
                    Caret caret = target.getCaret();
                    int caretOffset = caret.getDot();
                    Rectangle caretBounds = ((BaseTextUI)target.getUI()).modelToView(target, caretOffset);
                    if (caretBounds == null) {
                        return;
                    }
                    Point magicCaretPosition = caret.getMagicCaretPosition();
                    if (magicCaretPosition == null) {
                        magicCaretPosition = new Point(caretBounds.x, caretBounds.y);
                    }
                    if ((visibleBounds = target.getVisibleRect()).contains(caretBounds)) {
                        Rectangle newVisibleBounds = new Rectangle(visibleBounds);
                        int bottomLeftOffset = target.viewToModel(new Point(visibleBounds.x, visibleBounds.y + visibleBounds.height));
                        Rectangle bottomLeftLineBounds = target.modelToView(bottomLeftOffset);
                        newVisibleBounds.y = bottomLeftLineBounds.y;
                        int caretRelY = caretBounds.y - visibleBounds.y;
                        int caretNewY = newVisibleBounds.y + caretRelY;
                        newCaretOffset = target.viewToModel(new Point(magicCaretPosition.x, caretNewY));
                        newCaretBounds = target.modelToView(newCaretOffset);
                        if (newCaretBounds.y > caretNewY) {
                            newCaretOffset = target.viewToModel(new Point(magicCaretPosition.x, newCaretBounds.y - newCaretBounds.height));
                            newCaretBounds = target.modelToView(newCaretOffset);
                        }
                        newVisibleBounds.y = newCaretBounds.y - caretRelY;
                        target.scrollRectToVisible(newVisibleBounds);
                    } else {
                        Point newCaretPoint = new Point(magicCaretPosition.x, caretBounds.y + visibleBounds.height);
                        newCaretOffset = target.viewToModel(newCaretPoint);
                        newCaretBounds = target.modelToView(newCaretOffset);
                    }
                    boolean select = "selection-page-down".equals(this.getValue("Name"));
                    if (select) {
                        caret.moveDot(newCaretOffset);
                    } else {
                        caret.setDot(newCaretOffset);
                    }
                    magicCaretPosition.y = newCaretBounds.y;
                    caret.setMagicCaretPosition(magicCaretPosition);
                }
                catch (BadLocationException ex) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class ForwardAction
    extends LocalBaseAction {
        static final long serialVersionUID = 8007293230193334414L;

        public ForwardAction() {
            super(62);
        }

        /*
         * Enabled aggressive block sorting
         * Enabled unnecessary exception pruning
         * Enabled aggressive exception aggregation
         */
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target == null) return;
            Caret caret = target.getCaret();
            try {
                int pos;
                boolean select = "selection-forward".equals(this.getValue("Name"));
                if (!select && Utilities.isSelectionShowing(caret)) {
                    pos = target.getSelectionEnd();
                    if (pos == caret.getDot()) {
                        caret.setDot(pos);
                        return;
                    }
                    --pos;
                } else {
                    pos = caret.getDot();
                }
                int dot = target.getUI().getNextVisualPositionFrom(target, pos, Position.Bias.Forward, 3, null);
                if (!select) {
                    caret.setDot(dot);
                    return;
                }
                if (caret instanceof BaseCaret && RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                    ((BaseCaret)caret).extendRectangularSelection(true, false);
                    return;
                }
                caret.moveDot(dot);
                return;
            }
            catch (BadLocationException ex) {
                target.getToolkit().beep();
            }
        }
    }

    public static class PageUpAction
    extends LocalBaseAction {
        static final long serialVersionUID = -3107382148581661079L;

        public PageUpAction() {
            super(60);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                try {
                    int newCaretOffset;
                    Rectangle newCaretBounds;
                    Rectangle visibleBounds;
                    Caret caret = target.getCaret();
                    BaseDocument doc = (BaseDocument)target.getDocument();
                    int caretOffset = caret.getDot();
                    Rectangle caretBounds = ((BaseTextUI)target.getUI()).modelToView(target, caretOffset);
                    if (caretBounds == null) {
                        return;
                    }
                    Point magicCaretPosition = caret.getMagicCaretPosition();
                    if (magicCaretPosition == null) {
                        magicCaretPosition = new Point(caretBounds.x, caretBounds.y);
                    }
                    if ((visibleBounds = target.getVisibleRect()).contains(caretBounds)) {
                        Rectangle newVisibleBounds = new Rectangle(visibleBounds);
                        int topLeftOffset = target.viewToModel(new Point(visibleBounds.x, visibleBounds.y));
                        Rectangle topLeftLineBounds = target.modelToView(topLeftOffset);
                        if (topLeftLineBounds.y != visibleBounds.y) {
                            newVisibleBounds.y = topLeftLineBounds.y + topLeftLineBounds.height;
                        }
                        newVisibleBounds.y -= visibleBounds.height;
                        int caretRelY = caretBounds.y - visibleBounds.y;
                        int caretNewY = newVisibleBounds.y + caretRelY;
                        newCaretOffset = target.viewToModel(new Point(magicCaretPosition.x, caretNewY));
                        newCaretBounds = target.modelToView(newCaretOffset);
                        if (newCaretBounds.y < caretNewY) {
                            newCaretOffset = target.viewToModel(new Point(magicCaretPosition.x, newCaretBounds.y + newCaretBounds.height));
                            newCaretBounds = target.modelToView(newCaretOffset);
                        }
                        newVisibleBounds.y = newCaretBounds.y - caretRelY;
                        target.scrollRectToVisible(newVisibleBounds);
                    } else {
                        Point newCaretPoint = new Point(magicCaretPosition.x, caretBounds.y - visibleBounds.height);
                        newCaretOffset = target.viewToModel(newCaretPoint);
                        newCaretBounds = target.modelToView(newCaretOffset);
                    }
                    boolean select = "selection-page-up".equals(this.getValue("Name"));
                    if (select) {
                        caret.moveDot(newCaretOffset);
                    } else {
                        caret.setDot(newCaretOffset);
                    }
                    magicCaretPosition.y = newCaretBounds.y;
                    caret.setMagicCaretPosition(magicCaretPosition);
                }
                catch (BadLocationException ex) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class DownAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5635702355125266822L;

        public DownAction() {
            super(60);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            block10 : {
                if (target != null) {
                    try {
                        Caret caret = target.getCaret();
                        int dot = caret.getDot();
                        Point p = caret.getMagicCaretPosition();
                        if (p == null) {
                            Rectangle r = target.modelToView(dot);
                            if (r != null) {
                                p = new Point(r.x, r.y);
                                caret.setMagicCaretPosition(p);
                            } else {
                                return;
                            }
                        }
                        try {
                            dot = Utilities.getPositionBelow(target, dot, p.x);
                            boolean select = "selection-down".equals(this.getValue("Name"));
                            if (select) {
                                caret.moveDot(dot);
                                if (RectangularSelectionUtils.isRectangularSelection((JComponent)target) && caret instanceof BaseCaret) {
                                    ((BaseCaret)caret).updateRectangularUpDownSelection();
                                }
                                break block10;
                            }
                            caret.setDot(dot);
                        }
                        catch (BadLocationException e) {}
                    }
                    catch (BadLocationException ex) {
                        target.getToolkit().beep();
                    }
                }
            }
        }
    }

    public static class UpAction
    extends LocalBaseAction {
        static final long serialVersionUID = 4621760742646981563L;

        public UpAction() {
            super(60);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            block10 : {
                if (target != null) {
                    try {
                        Caret caret = target.getCaret();
                        int dot = caret.getDot();
                        Point p = caret.getMagicCaretPosition();
                        if (p == null) {
                            Rectangle r = target.modelToView(dot);
                            if (r != null) {
                                p = new Point(r.x, r.y);
                                caret.setMagicCaretPosition(p);
                            } else {
                                return;
                            }
                        }
                        try {
                            dot = Utilities.getPositionAbove(target, dot, p.x);
                            boolean select = "selection-up".equals(this.getValue("Name"));
                            if (select) {
                                caret.moveDot(dot);
                                if (RectangularSelectionUtils.isRectangularSelection((JComponent)target) && caret instanceof BaseCaret) {
                                    ((BaseCaret)caret).updateRectangularUpDownSelection();
                                }
                                break block10;
                            }
                            caret.setDot(dot);
                        }
                        catch (BadLocationException e) {}
                    }
                    catch (BadLocationException ex) {
                        target.getToolkit().beep();
                    }
                }
            }
        }
    }

    public static class BeepAction
    extends LocalBaseAction {
        static final long serialVersionUID = -4474054576633223968L;

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                target.getToolkit().beep();
            }
        }
    }

    public static class PasteAction
    extends LocalBaseAction {
        static final long serialVersionUID = 5839791453996432149L;

        public PasteAction(boolean formatted) {
            super(formatted ? "paste-formated" : "paste-from-clipboard", 28);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                BaseDocument doc;
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                if (LOG.isLoggable(Level.FINER)) {
                    StringBuilder sb = new StringBuilder("PasteAction stackTrace: \n");
                    for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                        sb.append(ste.toString()).append("\n");
                    }
                    LOG.log(Level.FINER, sb.toString());
                }
                if ((doc = Utilities.getDocument(target)) == null) {
                    return;
                }
                try {
                    NavigationHistory.getEdits().markWaypoint(target, target.getCaret().getDot(), false, true);
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, "Can't add position to the history of edits.", e);
                }
                if (RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                    doc.putProperty("rectangular-document-change-allowed", Boolean.TRUE);
                }
                final Reformat formatter = Reformat.get((Document)doc);
                final boolean formatted = "paste-formated".equals(this.getValue("Name"));
                if (formatted) {
                    formatter.lock();
                }
                try {
                    doc.runAtomicAsUser(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                Caret caret = target.getCaret();
                                int startOffset = target.getSelectionStart();
                                BaseKit.IN_PASTE.set(true);
                                if (target.getSelectedText() != null) {
                                    doc.putProperty("doc-replace-selection-property", true);
                                }
                                try {
                                    target.paste();
                                }
                                finally {
                                    BaseKit.IN_PASTE.set(false);
                                    doc.putProperty("doc-replace-selection-property", null);
                                }
                                int endOffset = caret.getDot();
                                if (formatted) {
                                    formatter.reformat(startOffset, endOffset);
                                }
                            }
                            catch (Exception e) {
                                target.getToolkit().beep();
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    });
                }
                finally {
                    if (formatted) {
                        formatter.unlock();
                    }
                }
            }
        }

    }

    public static class CopyAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5119779005431986964L;

        public CopyAction() {
            super("copy-to-clipboard", 28);
            this.setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                try {
                    int caretPosition = target.getCaretPosition();
                    boolean emptySelection = !Utilities.isSelectionShowing(target);
                    boolean disableNoSelectionCopy = Boolean.getBoolean("org.netbeans.editor.disable.no.selection.copy");
                    if (emptySelection && !disableNoSelectionCopy) {
                        Element elem = ((AbstractDocument)target.getDocument()).getParagraphElement(caretPosition);
                        if (!Utilities.isRowWhite((BaseDocument)target.getDocument(), elem.getStartOffset())) {
                            target.select(elem.getStartOffset(), elem.getEndOffset());
                        }
                    }
                    target.copy();
                    if (emptySelection && !disableNoSelectionCopy) {
                        target.setCaretPosition(caretPosition);
                    }
                }
                catch (BadLocationException ble) {
                    LOG.log(Level.FINE, null, ble);
                }
            }
        }
    }

    public static class CutAction
    extends LocalBaseAction {
        static final long serialVersionUID = 6377157040901778853L;

        public CutAction() {
            super("cut-to-clipboard", 28);
            this.setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                try {
                    NavigationHistory.getEdits().markWaypoint(target, target.getCaret().getDot(), false, true);
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, "Can't add position to the history of edits.", e);
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        block10 : {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                int removeNewlineOffset = -1;
                                if (!Utilities.isSelectionShowing(target)) {
                                    Element elem = ((AbstractDocument)target.getDocument()).getParagraphElement(target.getCaretPosition());
                                    int lineStartOffset = elem.getStartOffset();
                                    int lineEndOffset = elem.getEndOffset();
                                    if (lineEndOffset == doc.getLength() + 1) {
                                        try {
                                            doc.insertString(lineEndOffset - 1, "\n", null);
                                            if (lineStartOffset > 0) {
                                                removeNewlineOffset = lineStartOffset - 1;
                                            }
                                        }
                                        catch (BadLocationException e) {
                                            // empty catch block
                                        }
                                    }
                                    target.select(lineStartOffset, lineEndOffset);
                                }
                                target.cut();
                                if (removeNewlineOffset == -1) break block10;
                                try {
                                    doc.remove(removeNewlineOffset, 1);
                                }
                                catch (BadLocationException e) {
                                    // empty catch block
                                }
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    }
                });
            }
        }

    }

    @Deprecated
    public static class WritableAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5982547952800937954L;

        public WritableAction() {
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                target.setEditable(true);
            }
        }
    }

    @Deprecated
    public static class ReadOnlyAction
    extends LocalBaseAction {
        static final long serialVersionUID = 9204335480208463193L;

        public ReadOnlyAction() {
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                target.setEditable(false);
            }
        }
    }

    public static class DeleteCharAction
    extends LocalBaseAction {
        protected boolean nextChar;
        static final long serialVersionUID = -4321971925753148556L;
        private static final boolean disableDeleteFromScreenMenu = Boolean.TRUE.equals(Boolean.getBoolean("netbeans.editor.disable.delete.from.screen.menu"));

        public DeleteCharAction(String nm, boolean nextChar) {
            super(nm, 22);
            this.nextChar = nextChar;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                int mark;
                if (disableDeleteFromScreenMenu && (evt.getSource() instanceof MenuItem || evt.getSource() instanceof JMenuItem)) {
                    return;
                }
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                if (LOG.isLoggable(Level.FINER)) {
                    StringBuilder sb = new StringBuilder("DeleteCharAction stackTrace: \n");
                    for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                        sb.append(ste.toString()).append("\n");
                    }
                    LOG.log(Level.FINER, sb.toString());
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                final Caret caret = target.getCaret();
                final int dot = caret.getDot();
                if (dot != (mark = caret.getMark())) {
                    doc.runAtomicAsUser(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            block8 : {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                                try {
                                    if (RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                                        if (!RectangularSelectionUtils.removeSelection((JTextComponent)target)) {
                                            RectangularSelectionUtils.removeChar((JTextComponent)target, (boolean)DeleteCharAction.this.nextChar);
                                        }
                                        if (caret instanceof BaseCaret) {
                                            ((BaseCaret)caret).setRectangularSelectionToDotAndMark();
                                        }
                                        break block8;
                                    }
                                    doc.remove(Math.min(dot, mark), Math.abs(dot - mark));
                                }
                                catch (BadLocationException e) {
                                    target.getToolkit().beep();
                                }
                                finally {
                                    DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                                }
                            }
                        }
                    });
                } else {
                    char[] removedChar = null;
                    try {
                        removedChar = this.nextChar ? (dot < doc.getLength() ? doc.getChars(dot, 1) : null) : (dot > 0 ? doc.getChars(dot - 1, 1) : null);
                    }
                    catch (BadLocationException ble) {
                        target.getToolkit().beep();
                    }
                    if (removedChar != null) {
                        final String removedText = String.valueOf(removedChar);
                        final DeletedTextInterceptorsManager.Transaction t = DeletedTextInterceptorsManager.getInstance().openTransaction(target, dot, removedText, !this.nextChar);
                        try {
                            if (!t.beforeRemove()) {
                                final boolean[] result = new boolean[]{false};
                                doc.runAtomicAsUser(new Runnable(){

                                    /*
                                     * WARNING - Removed try catching itself - possible behaviour change.
                                     */
                                    @Override
                                    public void run() {
                                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                                        try {
                                            if (DeleteCharAction.this.nextChar) {
                                                doc.remove(dot, 1);
                                            } else {
                                                doc.remove(dot - 1, 1);
                                            }
                                            t.textDeleted();
                                            if (DeleteCharAction.this.nextChar) {
                                                DeleteCharAction.this.charDeleted(doc, dot, caret, removedText.charAt(0));
                                            } else {
                                                DeleteCharAction.this.charBackspaced(doc, dot - 1, caret, removedText.charAt(0));
                                            }
                                            result[0] = true;
                                        }
                                        catch (BadLocationException e) {
                                            target.getToolkit().beep();
                                        }
                                        finally {
                                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                                        }
                                    }
                                });
                                if (result[0]) {
                                    t.afterRemove();
                                }
                            }
                        }
                        finally {
                            t.close();
                        }
                    }
                }
            }
        }

        protected void charBackspaced(BaseDocument doc, int dotPos, Caret caret, char ch) throws BadLocationException {
        }

        protected void charDeleted(BaseDocument doc, int dotPos, Caret caret, char ch) throws BadLocationException {
        }

    }

    public static class InsertStringAction
    extends LocalBaseAction {
        String text;
        static final long serialVersionUID = -2755852016584693328L;

        public InsertStringAction(String nm, String text) {
            super(nm, 22);
            this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                target.replaceSelection(this.text);
            }
        }
    }

    @Deprecated
    public static class InsertContentAction
    extends LocalBaseAction {
        static final long serialVersionUID = 5647751370952797218L;

        public InsertContentAction() {
            super(22);
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null && evt != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                String content = evt.getActionCommand();
                if (content != null) {
                    target.replaceSelection(content);
                } else {
                    target.getToolkit().beep();
                }
            }
        }
    }

    @Deprecated
    public static class KitCompoundAction
    extends LocalBaseAction {
        private String[] actionNames;
        static final long serialVersionUID = 8415246475764264835L;

        public KitCompoundAction(String[] actionNames) {
            this(0, actionNames);
        }

        public KitCompoundAction(int resetMask, String[] actionNames) {
            super(resetMask);
            this.actionNames = actionNames;
        }

        public KitCompoundAction(String nm, String[] actionNames) {
            this(nm, 0, actionNames);
        }

        public KitCompoundAction(String nm, int resetMask, String[] actionNames) {
            super(nm, resetMask);
            this.actionNames = actionNames;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            BaseKit kit;
            if (target != null && (kit = Utilities.getKit(target)) != null) {
                for (int i = 0; i < this.actionNames.length; ++i) {
                    Action a = kit.getActionByName(this.actionNames[i]);
                    if (a == null) continue;
                    if (a instanceof BaseAction) {
                        ((BaseAction)a).actionPerformed(evt, target);
                        continue;
                    }
                    a.actionPerformed(evt);
                }
            }
        }
    }

    @Deprecated
    public static class CompoundAction
    extends LocalBaseAction {
        Action[] actions;
        static final long serialVersionUID = 1649688300969753758L;

        public CompoundAction(String nm, Action[] actions) {
            this(nm, 0, actions);
        }

        public CompoundAction(String nm, int resetMask, Action[] actions) {
            super(nm, resetMask);
            this.actions = actions;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                for (int i = 0; i < this.actions.length; ++i) {
                    Action a = this.actions[i];
                    if (a instanceof BaseAction) {
                        ((BaseAction)a).actionPerformed(evt, target);
                        continue;
                    }
                    a.actionPerformed(evt);
                }
            }
        }
    }

    public static class InsertTabAction
    extends LocalBaseAction {
        static final long serialVersionUID = -3379768531715989243L;

        public InsertTabAction() {
            super("insert-tab", 22);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                final Indent indenter = Indent.get((Document)doc);
                indenter.lock();
                try {
                    doc.runAtomicAsUser(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            block16 : {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                                try {
                                    if (Utilities.isSelectionShowing(caret)) {
                                        try {
                                            if (target.getSelectedText().trim().isEmpty()) {
                                                doc.remove(target.getSelectionStart(), target.getSelectionEnd() - target.getSelectionStart());
                                                BaseKit.insertTabString(doc, target.getSelectionStart());
                                            } else {
                                                int lineStartOffset;
                                                int newSelectionStartOffset;
                                                boolean selectionAtLineStart = Utilities.getRowStart(doc, target.getSelectionStart()) == target.getSelectionStart();
                                                BaseKit.changeBlockIndent(doc, target.getSelectionStart(), target.getSelectionEnd(), 1);
                                                if (selectionAtLineStart && (lineStartOffset = Utilities.getRowStart(doc, newSelectionStartOffset = target.getSelectionStart())) != newSelectionStartOffset) {
                                                    target.select(lineStartOffset, target.getSelectionEnd());
                                                }
                                            }
                                            break block16;
                                        }
                                        catch (GuardedException ge) {
                                            LOG.log(Level.FINE, null, ge);
                                            target.getToolkit().beep();
                                        }
                                        catch (BadLocationException e) {
                                            LOG.log(Level.WARNING, null, e);
                                        }
                                        break block16;
                                    }
                                    int dotPos = caret.getDot();
                                    try {
                                        int indent = Utilities.getRowIndent(doc, dotPos);
                                        if (indent == -1) {
                                            int caretCol = Utilities.getVisualColumn(doc, dotPos);
                                            int nextTabCol = Utilities.getNextTabColumn(doc, dotPos);
                                            indenter.reindent(dotPos);
                                            dotPos = caret.getDot();
                                            int newCaretCol = Utilities.getVisualColumn(doc, dotPos);
                                            if (newCaretCol <= caretCol) {
                                                int upperCol = Utilities.getRowIndent(doc, dotPos, false);
                                                BaseKit.changeRowIndent(doc, dotPos, upperCol > nextTabCol ? upperCol : nextTabCol);
                                                dotPos = caret.getDot();
                                                caret.setDot(Utilities.getRowEnd(doc, dotPos));
                                            }
                                        } else {
                                            BaseKit.insertTabString(doc, dotPos);
                                        }
                                    }
                                    catch (GuardedException ge) {
                                        LOG.log(Level.FINE, null, ge);
                                        target.getToolkit().beep();
                                    }
                                    catch (BadLocationException e) {
                                        target.getToolkit().beep();
                                        LOG.log(Level.FINE, null, e);
                                    }
                                }
                                finally {
                                    DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                                }
                            }
                        }
                    });
                }
                finally {
                    indenter.unlock();
                }
            }
        }

    }

    public static class SplitLineAction
    extends LocalBaseAction {
        static final long serialVersionUID = 7966576342334158659L;

        public SplitLineAction() {
            super(22);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                final Caret caret = target.getCaret();
                final Indent formatter = Indent.get((Document)doc);
                formatter.lock();
                try {
                    doc.runAtomicAsUser(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                target.replaceSelection("");
                                int dotPos = caret.getDot();
                                doc.insertString(dotPos, "\n", null);
                                formatter.reindent(dotPos + 1);
                                caret.setDot(dotPos);
                            }
                            catch (GuardedException e) {
                                target.getToolkit().beep();
                            }
                            catch (BadLocationException ble) {
                                LOG.log(Level.WARNING, null, ble);
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    });
                }
                finally {
                    formatter.unlock();
                }
            }
        }

    }

    public static class InsertBreakAction
    extends LocalBaseAction {
        static final long serialVersionUID = 7966576342334158659L;

        public InsertBreakAction() {
            super("insert-break", 22);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                BaseDocument doc = (BaseDocument)target.getDocument();
                final int insertionOffset = this.computeInsertionOffset(target.getCaret());
                final TypedBreakInterceptorsManager.Transaction transaction = TypedBreakInterceptorsManager.getInstance().openTransaction(target, insertionOffset, insertionOffset);
                try {
                    if (!transaction.beforeInsertion()) {
                        final Boolean[] result = new Boolean[]{Boolean.FALSE};
                        final Indent indenter = Indent.get((Document)doc);
                        indenter.lock();
                        try {
                            doc.runAtomicAsUser(new Runnable(){

                                @Override
                                public void run() {
                                    Object[] r = transaction.textTyped();
                                    String insertionText = r == null ? "\n" : (String)r[0];
                                    int breakInsertPosition = r == null ? -1 : (Integer)r[1];
                                    int caretPosition = r == null ? -1 : (Integer)r[2];
                                    int[] reindentBlocks = r == null ? null : (int[])r[3];
                                    try {
                                        InsertBreakAction.this.performLineBreakInsertion(target, insertionOffset, insertionText, breakInsertPosition, caretPosition, reindentBlocks, indenter);
                                        result[0] = Boolean.TRUE;
                                    }
                                    catch (BadLocationException ble) {
                                        LOG.log(Level.FINE, null, ble);
                                        target.getToolkit().beep();
                                    }
                                }
                            });
                        }
                        finally {
                            indenter.unlock();
                        }
                        if (result[0].booleanValue()) {
                            transaction.afterInsertion();
                        }
                    }
                }
                finally {
                    transaction.close();
                }
            }
        }

        protected Object beforeBreak(JTextComponent target, BaseDocument doc, Caret caret) {
            return null;
        }

        protected void afterBreak(JTextComponent target, BaseDocument doc, Caret caret, Object data) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void performLineBreakInsertion(JTextComponent target, int insertionOffset, String insertionText, int breakInsertPosition, int caretPosition, int[] reindentBlocks, Indent indenter) throws BadLocationException {
            BaseDocument doc = (BaseDocument)target.getDocument();
            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
            try {
                String selectedText = target.getSelectedText();
                int origDot = target.getCaretPosition();
                Caret caret = target.getCaret();
                doc.remove(this.computeInsertionOffset(caret), this.computeInsertionLength(caret));
                Object cookie = this.beforeBreak(target, doc, caret);
                int dotPos = caret.getDot();
                assert (cookie instanceof Integer || dotPos == insertionOffset);
                doc.insertString(dotPos, insertionText, null);
                Position newDotPos = doc.createPosition(dotPos += caretPosition != -1 ? caretPosition : (breakInsertPosition != -1 ? breakInsertPosition + 1 : insertionText.indexOf(10) + 1));
                if (reindentBlocks != null && reindentBlocks.length > 0) {
                    for (int i = 0; i < reindentBlocks.length / 2; ++i) {
                        int startOffset = insertionOffset + reindentBlocks[2 * i];
                        int endOffset = insertionOffset + reindentBlocks[2 * i + 1];
                        indenter.reindent(startOffset, endOffset);
                    }
                } else {
                    indenter.reindent(dotPos);
                }
                caret.setDot(newDotPos.getOffset());
                this.afterBreak(target, doc, caret, cookie);
            }
            finally {
                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
            }
        }

        private int computeInsertionOffset(Caret caret) {
            return Math.min(caret.getMark(), caret.getDot());
        }

        private int computeInsertionLength(Caret caret) {
            return Math.max(caret.getMark(), caret.getDot()) - Math.min(caret.getMark(), caret.getDot());
        }

    }

    public static class DefaultKeyTypedAction
    extends LocalBaseAction {
        static final long serialVersionUID = 3069164318144463899L;

        public DefaultKeyTypedAction() {
            super("default-typed", 32);
            this.putValue("no-keybinding", Boolean.TRUE);
            LOG.fine("DefaultKeyTypedAction with enhanced logging, see issue #145306");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         * Enabled force condition propagation
         * Lifted jumps to return sites
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target == null || evt == null) return;
            if (!BaseKit.isValidDefaultTypedAction(evt)) {
                return;
            }
            if (!target.isEditable() || !target.isEnabled()) {
                target.getToolkit().beep();
                return;
            }
            if (target.getCaret() != null) {
                target.getCaret().setMagicCaretPosition(null);
            }
            final String cmd = evt.getActionCommand();
            if (BaseKit.isValidDefaultTypedCommand(evt)) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Processing command char: {0}", Integer.toHexString(cmd.charAt(0)));
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                if (RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                    final boolean[] changed = new boolean[1];
                    doc.runAtomicAsUser(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                List regions = RectangularSelectionUtils.regionsCopy((JComponent)target);
                                if (regions != null && regions.size() > 2) {
                                    changed[0] = true;
                                    RectangularSelectionUtils.removeSelection((Document)doc, (List)regions);
                                    RectangularSelectionUtils.insertText((Document)doc, (List)regions, (String)cmd);
                                }
                            }
                            catch (BadLocationException ble) {
                                LOG.log(Level.FINE, null, ble);
                                target.getToolkit().beep();
                            }
                        }
                    });
                    Caret caret = target.getCaret();
                    if (caret instanceof BaseCaret) {
                        ((BaseCaret)caret).setRectangularSelectionToDotAndMark();
                    }
                    if (changed[0]) {
                        return;
                    }
                }
                try {
                    final Position insertionOffset = doc.createPosition(this.computeInsertionOffset(target.getCaret()), Position.Bias.Backward);
                    String replacedText = "";
                    if (Utilities.isSelectionShowing(target.getCaret())) {
                        int p0 = Math.min(target.getCaret().getDot(), target.getCaret().getMark());
                        int p1 = Math.max(target.getCaret().getDot(), target.getCaret().getMark());
                        replacedText = doc.getText(p0, p1 - p0);
                    }
                    final TypedTextInterceptorsManager.Transaction transaction = TypedTextInterceptorsManager.getInstance().openTransaction(target, insertionOffset, cmd, replacedText);
                    try {
                        if (transaction.beforeInsertion()) return;
                        final Object[] result = new Object[]{Boolean.FALSE, ""};
                        doc.runAtomicAsUser(new Runnable(){

                            /*
                             * WARNING - Removed try catching itself - possible behaviour change.
                             */
                            @Override
                            public void run() {
                                block8 : {
                                    Object[] r;
                                    boolean alreadyBeeped = false;
                                    if (Utilities.isSelectionShowing(target.getCaret())) {
                                        EditorUI editorUI = Utilities.getEditorUI(target);
                                        Boolean overwriteMode = (Boolean)editorUI.getProperty("overwriteMode");
                                        boolean ovr = overwriteMode != null && overwriteMode != false;
                                        try {
                                            doc.putProperty("doc-replace-selection-property", true);
                                            DefaultKeyTypedAction.this.replaceSelection(target, insertionOffset.getOffset(), target.getCaret(), "", ovr);
                                        }
                                        catch (BadLocationException ble) {
                                            LOG.log(Level.FINE, null, ble);
                                            target.getToolkit().beep();
                                            alreadyBeeped = true;
                                        }
                                        finally {
                                            doc.putProperty("doc-replace-selection-property", null);
                                        }
                                    }
                                    String insertionText = (r = transaction.textTyped()) == null ? cmd : (String)r[0];
                                    int caretPosition = r == null ? -1 : (Integer)r[1];
                                    try {
                                        DefaultKeyTypedAction.this.performTextInsertion(target, insertionOffset.getOffset(), insertionText, caretPosition);
                                        result[0] = Boolean.TRUE;
                                        result[1] = insertionText;
                                    }
                                    catch (BadLocationException ble) {
                                        LOG.log(Level.FINE, null, ble);
                                        if (alreadyBeeped) break block8;
                                        target.getToolkit().beep();
                                    }
                                }
                            }
                        });
                        if (!((Boolean)result[0]).booleanValue()) return;
                        transaction.afterInsertion();
                        this.checkIndent(target, (String)result[1]);
                    }
                    finally {
                        transaction.close();
                    }
                }
                catch (BadLocationException ble) {
                    LOG.log(Level.FINE, null, ble);
                    target.getToolkit().beep();
                    return;
                }
                return;
            } else {
                if (!LOG.isLoggable(Level.FINE)) return;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < cmd.length(); ++i) {
                    String hex = Integer.toHexString(cmd.charAt(i));
                    sb.append(hex);
                    if (i + 1 >= cmd.length()) continue;
                    sb.append(" ");
                }
                LOG.log(Level.FINE, "Invalid command: {0}", sb);
            }
        }

        protected void insertString(BaseDocument doc, int dotPos, Caret caret, String str, boolean overwrite) throws BadLocationException {
            if (overwrite) {
                doc.remove(dotPos, 1);
            }
            doc.insertString(dotPos, str, null);
        }

        protected void replaceSelection(JTextComponent target, int dotPos, Caret caret, String str, boolean overwrite) throws BadLocationException {
            target.replaceSelection(str);
        }

        protected void checkIndent(JTextComponent target, String typedText) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void performTextInsertion(JTextComponent target, int insertionOffset, String insertionText, int caretPosition) throws BadLocationException {
            BaseDocument doc = (BaseDocument)target.getDocument();
            try {
                NavigationHistory.getEdits().markWaypoint(target, insertionOffset, false, true);
            }
            catch (BadLocationException e) {
                LOG.log(Level.WARNING, "Can't add position to the history of edits.", e);
            }
            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
            try {
                boolean ovr;
                EditorUI editorUI = Utilities.getEditorUI(target);
                Caret caret = target.getCaret();
                editorUI.getWordMatch().clear();
                Boolean overwriteMode = (Boolean)editorUI.getProperty("overwriteMode");
                boolean bl = ovr = overwriteMode != null && overwriteMode != false;
                if (Utilities.isSelectionShowing(caret)) {
                    try {
                        doc.putProperty("doc-replace-selection-property", true);
                        this.replaceSelection(target, insertionOffset, caret, insertionText, ovr);
                    }
                    finally {
                        doc.putProperty("doc-replace-selection-property", null);
                    }
                }
                if (ovr && insertionOffset < doc.getLength() && doc.getChars(insertionOffset, 1)[0] != '\n') {
                    this.insertString(doc, insertionOffset, caret, insertionText, true);
                } else {
                    this.insertString(doc, insertionOffset, caret, insertionText, false);
                }
                if (caretPosition != -1) {
                    assert (caretPosition >= 0 && caretPosition <= insertionText.length());
                    caret.setDot(insertionOffset + caretPosition);
                }
            }
            finally {
                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
            }
        }

        private int computeInsertionOffset(Caret caret) {
            if (Utilities.isSelectionShowing(caret)) {
                return Math.min(caret.getMark(), caret.getDot());
            }
            return caret.getDot();
        }

    }

}

