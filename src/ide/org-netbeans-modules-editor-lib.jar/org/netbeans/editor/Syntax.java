/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;

public class Syntax {
    public static final int EQUAL_STATE = 0;
    public static final int DIFFERENT_STATE = 1;
    public static final int INIT = -1;
    protected int state = -1;
    protected char[] buffer;
    protected int offset;
    protected int tokenOffset;
    protected int tokenLength;
    protected TokenContextPath tokenContextPath;
    protected boolean lastBuffer;
    protected int stopOffset;
    protected int stopPosition;
    protected TokenID supposedTokenID;

    public TokenID nextToken() {
        if (this.tokenOffset >= this.stopOffset) {
            this.tokenLength = 0;
            return null;
        }
        this.supposedTokenID = null;
        TokenID tokenID = this.parseToken();
        if (tokenID != null) {
            this.tokenLength = this.offset - this.tokenOffset;
            this.tokenOffset = this.offset;
            if (this.tokenLength == 0) {
                return this.nextToken();
            }
        } else {
            this.tokenLength = 0;
        }
        return tokenID;
    }

    protected TokenID parseToken() {
        return null;
    }

    public void load(StateInfo stateInfo, char[] buffer, int offset, int len, boolean lastBuffer, int stopPosition) {
        this.buffer = buffer;
        this.offset = offset;
        this.tokenOffset = offset;
        this.stopOffset = offset + len;
        this.lastBuffer = lastBuffer;
        this.stopPosition = stopPosition;
        if (stateInfo != null) {
            this.loadState(stateInfo);
        } else {
            this.loadInitState();
        }
    }

    public void relocate(char[] buffer, int offset, int len, boolean lastBuffer, int stopPosition) {
        this.buffer = buffer;
        this.lastBuffer = lastBuffer;
        int delta = offset - this.offset;
        this.offset += delta;
        this.tokenOffset += delta;
        this.stopOffset = offset + len;
        this.stopPosition = stopPosition;
    }

    public char[] getBuffer() {
        return this.buffer;
    }

    public int getOffset() {
        return this.offset;
    }

    public int getTokenOffset() {
        return this.offset - this.tokenLength;
    }

    public int getTokenLength() {
        return this.tokenLength;
    }

    public TokenContextPath getTokenContextPath() {
        return this.tokenContextPath;
    }

    public TokenID getSupposedTokenID() {
        return this.supposedTokenID;
    }

    public int getPreScan() {
        return this.offset - this.tokenOffset;
    }

    public void loadInitState() {
        this.state = -1;
    }

    public void reset() {
        this.offset = 0;
        this.tokenOffset = 0;
        this.stopOffset = 0;
        this.tokenLength = 0;
        this.loadInitState();
    }

    public void loadState(StateInfo stateInfo) {
        this.state = stateInfo.getState();
        this.tokenOffset -= stateInfo.getPreScan();
    }

    public void storeState(StateInfo stateInfo) {
        stateInfo.setState(this.state);
        stateInfo.setPreScan(this.getPreScan());
    }

    public int compareState(StateInfo stateInfo) {
        if (stateInfo != null) {
            return stateInfo.getState() == this.state && stateInfo.getPreScan() == this.getPreScan() ? 0 : 1;
        }
        return 1;
    }

    public StateInfo createStateInfo() {
        return new BaseStateInfo();
    }

    public String getStateName(int stateNumber) {
        switch (stateNumber) {
            case -1: {
                return "INIT";
            }
        }
        return "Unknown state " + stateNumber;
    }

    public String toString() {
        return "tokenOffset=" + this.tokenOffset + ", offset=" + this.offset + ", state=" + this.getStateName(this.state) + ", stopOffset=" + this.stopOffset + ", lastBuffer=" + this.lastBuffer;
    }

    public static class BaseStateInfo
    implements StateInfo {
        private int state;
        private int preScan;

        @Override
        public int getState() {
            return this.state;
        }

        @Override
        public void setState(int state) {
            this.state = state;
        }

        @Override
        public int getPreScan() {
            return this.preScan;
        }

        @Override
        public void setPreScan(int preScan) {
            this.preScan = preScan;
        }

        public String toString(Syntax syntax) {
            return "state=" + (syntax != null ? syntax.getStateName(this.getState()) : Integer.toString(this.getState())) + ", preScan=" + this.getPreScan();
        }

        public String toString() {
            return this.toString(null);
        }
    }

    public static interface StateInfo {
        public int getState();

        public void setState(int var1);

        public int getPreScan();

        public void setPreScan(int var1);
    }

}

