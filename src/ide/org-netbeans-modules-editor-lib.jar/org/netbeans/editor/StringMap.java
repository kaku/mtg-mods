/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.util.HashMap;
import java.util.Map;

public class StringMap
extends HashMap {
    char[] testChars;
    int testOffset;
    int testLen;
    static final long serialVersionUID = 967608225972123714L;

    public StringMap() {
    }

    public StringMap(int initialCapacity) {
        super(initialCapacity);
    }

    public StringMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public StringMap(Map t) {
        super(t);
    }

    public Object get(char[] chars, int offset, int len) {
        this.testChars = chars;
        this.testOffset = offset;
        this.testLen = len;
        Object o = this.get(this);
        this.testChars = null;
        return o;
    }

    public boolean containsKey(char[] chars, int offset, int len) {
        this.testChars = chars;
        this.testOffset = offset;
        this.testLen = len;
        boolean b = this.containsKey(this);
        this.testChars = null;
        return b;
    }

    public Object remove(char[] chars, int offset, int len) {
        this.testChars = chars;
        this.testOffset = offset;
        this.testLen = len;
        Object o = this.remove(this);
        this.testChars = null;
        return o;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof String) {
            String s = (String)o;
            if (this.testLen == s.length()) {
                for (int i = this.testLen - 1; i >= 0; --i) {
                    if (this.testChars[this.testOffset + i] == s.charAt(i)) continue;
                    return false;
                }
                return true;
            }
            return false;
        }
        if (o instanceof char[]) {
            char[] chars = (char[])o;
            if (this.testLen == chars.length) {
                for (int i = this.testLen - 1; i >= 0; --i) {
                    if (this.testChars[this.testOffset + i] == chars[i]) continue;
                    return false;
                }
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int h = 0;
        char[] chars = this.testChars;
        int off = this.testOffset;
        for (int i = this.testLen; i > 0; --i) {
            h = 31 * h + chars[off++];
        }
        return h;
    }
}

