/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Segment;
import org.netbeans.editor.GapStart;

public class DocumentUtilities {
    private DocumentUtilities() {
    }

    public static int getGapStart(Document doc) {
        GapStart gs = (GapStart)doc.getProperty(GapStart.class);
        return gs != null ? gs.getGapStart() : -1;
    }

    public static void copyText(Document srcDoc, int srcStartOffset, int srcEndOffset, char[] dst, int dstOffset) throws BadLocationException {
        Segment text = new Segment();
        int gapStart = DocumentUtilities.getGapStart(srcDoc);
        if (gapStart != -1 && srcStartOffset < gapStart && gapStart < srcEndOffset) {
            srcDoc.getText(srcStartOffset, gapStart - srcStartOffset, text);
            System.arraycopy(text.array, text.offset, dst, dstOffset, text.count);
            dstOffset += text.count;
            srcStartOffset = gapStart;
        }
        srcDoc.getText(srcStartOffset, srcEndOffset - srcStartOffset, text);
        System.arraycopy(text.array, text.offset, dst, dstOffset, srcEndOffset - srcStartOffset);
    }
}

