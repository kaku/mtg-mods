/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.EditorImplementation
 *  org.netbeans.modules.editor.lib2.EditorImplementationProvider
 */
package org.netbeans.editor;

import java.util.ResourceBundle;
import javax.swing.Action;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.lib2.EditorImplementation;
import org.netbeans.modules.editor.lib2.EditorImplementationProvider;

public abstract class ImplementationProvider {
    private static final ImplementationProvider PROVIDER = new ProviderBridge();

    public static ImplementationProvider getDefault() {
        return PROVIDER;
    }

    public static void registerDefault(ImplementationProvider prov) {
        EditorImplementation.getDefault().setExternalProvider((EditorImplementationProvider)new Wrapper(prov));
    }

    public abstract ResourceBundle getResourceBundle(String var1);

    public abstract Action[] getGlyphGutterActions(JTextComponent var1);

    public boolean activateComponent(JTextComponent c) {
        return false;
    }

    private static final class Wrapper
    implements EditorImplementationProvider {
        private ImplementationProvider origProvider;

        public Wrapper(ImplementationProvider origProvider) {
            this.origProvider = origProvider;
        }

        public ResourceBundle getResourceBundle(String localizer) {
            return this.origProvider.getResourceBundle(localizer);
        }

        public Action[] getGlyphGutterActions(JTextComponent target) {
            return this.origProvider.getGlyphGutterActions(target);
        }

        public boolean activateComponent(JTextComponent c) {
            return this.origProvider.activateComponent(c);
        }
    }

    private static final class ProviderBridge
    extends ImplementationProvider {
        private ProviderBridge() {
        }

        @Override
        public ResourceBundle getResourceBundle(String localizer) {
            return EditorImplementation.getDefault().getResourceBundle(localizer);
        }

        @Override
        public Action[] getGlyphGutterActions(JTextComponent target) {
            return EditorImplementation.getDefault().getGlyphGutterActions(target);
        }

        @Override
        public boolean activateComponent(JTextComponent c) {
            return EditorImplementation.getDefault().activateComponent(c);
        }
    }

}

