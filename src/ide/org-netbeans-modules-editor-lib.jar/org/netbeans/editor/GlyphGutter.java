/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.view.LockedViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ParagraphViewDescriptor
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyListener
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.FontMetricsCache;
import org.netbeans.editor.ImplementationProvider;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib.ColoringMap;
import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ParagraphViewDescriptor;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyListener;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public class GlyphGutter
extends JComponent
implements Annotations.AnnotationsListener,
Accessible,
SideBarFactory {
    private static final Logger LOG = Logger.getLogger(GlyphGutter.class.getName());
    private static final String TEXT_ZOOM_PROPERTY = "text-zoom";
    private volatile EditorUI editorUI;
    private Annotations annos;
    private Image gutterButton;
    private Color backgroundColor;
    private Color foreColor;
    private Font font;
    private boolean init;
    private int glyphGutterWidth;
    private static final int glyphWidth = 16;
    private static final int glyphButtonWidth = 9;
    private static final int leftGap = 10;
    private static final int rightGap = 4;
    private boolean showLineNumbers = true;
    private static final int ENLARGE_GUTTER_HEIGHT = 300;
    private int highestLineNumber = 0;
    private boolean drawOverLineNumbers = false;
    private volatile int cachedCountOfAnnos = -1;
    private int cachedCountOfAnnosForLine = -1;
    private PropertyChangeListener annoTypesListener;
    private PropertyChangeListener editorUIListener;
    private GutterMouseListener gutterMouseListener;
    private ColoringMap coloringMap;
    private final PropertyChangeListener coloringMapListener;
    private Preferences prefs;
    private final PreferenceChangeListener prefsListener;
    private static final Color DEFAULT_GUTTER_LINE = new Color(184, 184, 184);
    private Rectangle toRepaint;
    private final Object toRepaintLock;
    private static final int REPAINT_TASK_DELAY = 50;
    private final RequestProcessor.Task repaintTask;

    public GlyphGutter() {
        this.coloringMapListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName() == null || "ColoringMap.PROP_COLORING_MAP".equals(evt.getPropertyName())) {
                    GlyphGutter.this.update();
                }
            }
        };
        this.prefs = null;
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                Rectangle rect;
                EditorUI eui = GlyphGutter.this.editorUI;
                JTextComponent c = eui == null ? null : eui.getComponent();
                Rectangle rectangle = rect = c == null ? null : c.getVisibleRect();
                if (rect != null && rect.width == 0) {
                    if (SwingUtilities.isEventDispatchThread()) {
                        GlyphGutter.this.resize();
                    } else {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                GlyphGutter.this.resize();
                            }
                        });
                    }
                }
            }

        };
        this.toRepaint = null;
        this.toRepaintLock = new Object();
        this.repaintTask = new RequestProcessor(GlyphGutter.class.getName(), 1, false, false).create(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Rectangle repaint;
                Object object = GlyphGutter.this.toRepaintLock;
                synchronized (object) {
                    repaint = GlyphGutter.this.toRepaint;
                    GlyphGutter.this.toRepaint = null;
                }
                if (repaint != null) {
                    GlyphGutter.this.doRepaint(repaint);
                }
            }
        });
    }

    public GlyphGutter(EditorUI eui) {
        this.coloringMapListener = new ;
        this.prefs = null;
        this.prefsListener = new ;
        this.toRepaint = null;
        this.toRepaintLock = new Object();
        this.repaintTask = new RequestProcessor(GlyphGutter.class.getName(), 1, false, false).create(new );
        this.editorUI = eui;
        this.init = false;
        this.annos = eui.getDocument().getAnnotations();
        this.annos.addAnnotationsListener(this);
        this.init();
        this.update();
        this.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        this.editorUIListener = new EditorUIListener();
        eui.addPropertyChangeListener(this.editorUIListener);
        eui.getComponent().addPropertyChangeListener(this.editorUIListener);
        this.setOpaque(true);
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)eui.getComponent());
        this.coloringMap = ColoringMap.get(mimeType);
        this.coloringMap.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this.coloringMapListener, (Object)this.coloringMap));
        this.prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs));
        this.prefsListener.preferenceChange(null);
    }

    private void checkRepaint(ViewHierarchyEvent vhe) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("ViewHierarchyEvent: " + (Object)vhe + " changeY=" + vhe.isChangeY() + "\n");
        }
        if (!vhe.isChangeY()) {
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                GlyphGutter.this.update();
            }
        });
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PANEL;
                }
            };
        }
        return this.accessibleContext;
    }

    protected void init() {
        if (this.editorUI == null) {
            return;
        }
        this.gutterButton = ImageUtilities.icon2Image((Icon)ImageUtilities.loadImageIcon((String)"org/netbeans/editor/resources/glyphbutton.gif", (boolean)false));
        this.setToolTipText("");
        this.getAccessibleContext().setAccessibleName(NbBundle.getBundle(BaseKit.class).getString("ACSN_Glyph_Gutter"));
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getBundle(BaseKit.class).getString("ACSD_Glyph_Gutter"));
        this.gutterMouseListener = new GutterMouseListener();
        this.addMouseListener(this.gutterMouseListener);
        this.addMouseMotionListener(this.gutterMouseListener);
        this.annoTypesListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName() == null || evt.getPropertyName().equals("glyphsOverLineNumbers") || evt.getPropertyName().equals("showGlyphGutter")) {
                    GlyphGutter.this.update();
                }
            }
        };
        AnnotationTypes.getTypes().addPropertyChangeListener(this.annoTypesListener);
        ViewHierarchy.get((JTextComponent)this.editorUI.getComponent()).addViewHierarchyListener(new ViewHierarchyListener(){

            public void viewHierarchyChanged(ViewHierarchyEvent evt) {
                GlyphGutter.this.checkRepaint(evt);
            }
        });
    }

    public void update() {
        Font lineFont;
        Integer textZoom;
        EditorUI eui = this.editorUI;
        if (eui == null) {
            return;
        }
        Coloring lineColoring = eui.getColoringMap().get("line-number");
        Coloring defaultColoring = eui.getDefaultColoring();
        if (lineColoring == null) {
            return;
        }
        Color backColor = lineColoring.getBackColor();
        this.backgroundColor = org.openide.util.Utilities.isMac() ? backColor : UIManager.getColor("NbEditorGlyphGutter.background");
        if (null == this.backgroundColor) {
            this.backgroundColor = backColor != null ? backColor : defaultColoring.getBackColor();
        }
        this.foreColor = lineColoring.getForeColor() != null ? lineColoring.getForeColor() : defaultColoring.getForeColor();
        if (lineColoring.getFont() != null) {
            Font f = lineColoring.getFont();
            lineFont = f != null ? f.deriveFont((float)f.getSize() - 1.0f) : null;
        } else {
            lineFont = defaultColoring.getFont();
            lineFont = new Font("Monospaced", 0, lineFont.getSize() - 1);
        }
        JTextComponent tc = eui.getComponent();
        if (tc != null && (textZoom = (Integer)tc.getClientProperty("text-zoom")) != null && textZoom != 0) {
            lineFont = new Font(lineFont.getFamily(), lineFont.getStyle(), Math.max(lineFont.getSize() + textZoom, 2));
        }
        this.font = lineFont;
        this.showLineNumbers = eui.lineNumberVisibleSetting;
        this.drawOverLineNumbers = AnnotationTypes.getTypes().isGlyphsOverLineNumbers();
        this.init = true;
        this.highestLineNumber = this.getLineCount();
        this.resize();
        this.repaint();
    }

    protected void resize() {
        EditorUI eui = this.editorUI;
        if (eui != null) {
            Dimension dim = new Dimension();
            dim.width = this.glyphGutterWidth = this.getWidthDimension();
            dim.height = this.getHeightDimension();
            this.setPreferredSize(dim);
            this.revalidate();
            this.putDimensionForPrinting();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected int getLineCount() {
        int lineCnt;
        EditorUI eui = this.editorUI;
        try {
            BaseDocument document;
            BaseDocument baseDocument = document = eui != null ? eui.getDocument() : null;
            if (document != null) {
                document.readLock();
                try {
                    lineCnt = Utilities.getLineOffset(document, document.getLength()) + 1;
                }
                finally {
                    document.readUnlock();
                }
            }
            lineCnt = 1;
        }
        catch (BadLocationException e) {
            lineCnt = 1;
        }
        return lineCnt;
    }

    protected int getDigitCount(int number) {
        return Integer.toString(number).length();
    }

    protected int getLineNumberWidth() {
        int newWidth = 0;
        EditorUI eui = this.editorUI;
        if (eui != null) {
            JTextComponent tc = eui.getComponent();
            if (this.font != null && tc != null) {
                FontRenderContext frc;
                Graphics g = tc.getGraphics();
                if (g != null && g instanceof Graphics2D && (frc = ((Graphics2D)g).getFontRenderContext()) != null) {
                    newWidth = (int)((float)newWidth + new TextLayout(String.valueOf(this.highestLineNumber), this.font, frc).getAdvance());
                } else {
                    FontMetrics fm = this.getFontMetrics(this.font);
                    if (fm != null) {
                        newWidth += fm.stringWidth(String.valueOf(this.highestLineNumber));
                    }
                }
            }
        }
        return newWidth;
    }

    protected int getWidthDimension() {
        int newWidth = 0;
        if (this.showLineNumbers) {
            int lineNumberWidth = this.getLineNumberWidth();
            newWidth = 10 + lineNumberWidth + 4;
        } else if (this.editorUI != null) {
            if (this.annos.isGlyphColumn() || AnnotationTypes.getTypes().isShowGlyphGutter().booleanValue()) {
                newWidth += 16;
            }
            if (this.annos.isGlyphButtonColumn()) {
                newWidth += 9;
            }
        }
        return newWidth;
    }

    protected int getHeightDimension() {
        EditorUI eui = this.editorUI;
        if (eui == null) {
            return 0;
        }
        JTextComponent comp = eui.getComponent();
        if (comp == null) {
            return 0;
        }
        return (int)comp.getSize().getHeight();
    }

    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        EditorUI eui = this.editorUI;
        if (eui == null) {
            return;
        }
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)eui.getComponent());
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        Map hints = (Map)fcs.getFontColors("default").getAttribute(EditorStyleConstants.RenderingHints);
        if (!hints.isEmpty()) {
            ((Graphics2D)g).addRenderingHints(hints);
        }
        if (!this.init) {
            return;
        }
        final Rectangle clip = g.getClipBounds();
        final JTextComponent component = eui.getComponent();
        if (component == null) {
            return;
        }
        View rootView = Utilities.getDocumentView(component);
        if (rootView == null) {
            return;
        }
        g.setColor(this.backgroundColor);
        g.fillRect(clip.x, clip.y, clip.width, clip.height);
        g.setColor(DEFAULT_GUTTER_LINE);
        g.drawLine(this.glyphGutterWidth - 1, clip.y, this.glyphGutterWidth - 1, clip.height + clip.y);
        final Document doc = component.getDocument();
        doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                ViewHierarchy vh = ViewHierarchy.get((JTextComponent)component);
                LockedViewHierarchy lockedVH = vh.lock();
                try {
                    int pViewIndex = lockedVH.yToParagraphViewIndex((double)clip.y);
                    if (pViewIndex >= 0) {
                        int pViewCount = lockedVH.getParagraphViewCount();
                        int repaintWidth = (int)GlyphGutter.this.getSize().getWidth();
                        int endRepaintY = clip.y + clip.height;
                        Element lineElementRoot = doc.getDefaultRootElement();
                        ParagraphViewDescriptor pViewDesc = lockedVH.getParagraphViewDescriptor(pViewIndex);
                        int pViewStartOffset = pViewDesc.getStartOffset();
                        int lineIndex = lineElementRoot.getElementIndex(pViewStartOffset);
                        int lineEndOffset = lineElementRoot.getElement(lineIndex).getEndOffset();
                        int lineWithAnno = -1;
                        float rowHeight = lockedVH.getDefaultRowHeight();
                        int lineNumberMaxWidth = GlyphGutter.this.getLineNumberWidth();
                        g.setFont(GlyphGutter.this.font);
                        g.setColor(GlyphGutter.this.foreColor);
                        FontMetrics fm = FontMetricsCache.getFontMetrics(GlyphGutter.this.font, g);
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.fine("GlyphGutter: clipY:<" + clip.y + "," + endRepaintY + ">" + ", pViewIndex=" + pViewIndex + ", lineIndex=" + lineIndex + "\n");
                        }
                        do {
                            Shape pViewAlloc = pViewDesc.getAllocation();
                            Rectangle pViewRect = pViewAlloc.getBounds();
                            pViewRect.width = repaintWidth;
                            if (pViewRect.y >= endRepaintY) {
                                if (LOG.isLoggable(Level.FINE)) {
                                    LOG.fine("GlyphGutter: pViewRect.y=" + pViewRect.y + " >= endRepaintY=" + endRepaintY + " -> break;\n");
                                }
                            } else {
                                Image annoGlyph;
                                int annoCount;
                                AnnotationDesc annoDesc;
                                while (pViewStartOffset >= lineEndOffset) {
                                    lineEndOffset = lineElementRoot.getElement(++lineIndex).getEndOffset();
                                    lineWithAnno = -1;
                                }
                                String lineNumberString = String.valueOf(lineIndex + 1);
                                int lineNumberWidth = fm.stringWidth(lineNumberString);
                                if (lineWithAnno == -1) {
                                    lineWithAnno = GlyphGutter.this.annos.getNextLineWithAnnotation(lineIndex);
                                }
                                if (lineWithAnno == lineIndex) {
                                    annoCount = GlyphGutter.this.annos.getNumberOfAnnotations(lineIndex);
                                    annoDesc = GlyphGutter.this.annos.getActiveAnnotation(lineIndex);
                                    annoGlyph = annoDesc != null ? annoDesc.getGlyph() : null;
                                } else {
                                    annoCount = 0;
                                    annoDesc = null;
                                    annoGlyph = null;
                                }
                                if (GlyphGutter.this.showLineNumbers) {
                                    boolean glyphHasIcon = false;
                                    if (!(lineIndex != lineWithAnno || annoDesc == null || annoDesc.isDefaultGlyph() && annoCount == 1 || annoGlyph == null)) {
                                        glyphHasIcon = true;
                                    }
                                    if (!glyphHasIcon || !GlyphGutter.this.drawOverLineNumbers || GlyphGutter.this.drawOverLineNumbers && lineIndex != lineWithAnno) {
                                        int x = GlyphGutter.this.glyphGutterWidth - lineNumberWidth - 4;
                                        int y = Math.round((float)pViewRect.y + pViewDesc.getAscent());
                                        g.drawString(lineNumberString, x, y);
                                        if (LOG.isLoggable(Level.FINER)) {
                                            LOG.finer("GlyphGutter: drawString: \"" + lineNumberString + "\" x=" + x + ", y=" + y + "\n");
                                        }
                                    }
                                }
                                if (lineIndex == lineWithAnno) {
                                    int xPos;
                                    int n = xPos = GlyphGutter.this.showLineNumbers ? lineNumberMaxWidth : 0;
                                    if (GlyphGutter.this.drawOverLineNumbers) {
                                        xPos = GlyphGutter.this.getWidth() - 16;
                                        if (annoCount > 1) {
                                            xPos -= 9;
                                        }
                                    }
                                    if (annoGlyph != null) {
                                        int glyphHeight = annoGlyph.getHeight(null);
                                        if (annoCount != 1 || !annoDesc.isDefaultGlyph()) {
                                            g.drawImage(annoGlyph, xPos, Math.round((float)pViewRect.y + (rowHeight - (float)glyphHeight) / 2.0f + 1.0f), null);
                                        }
                                        if (annoCount > 1) {
                                            g.drawImage(GlyphGutter.this.gutterButton, xPos + 16 - 1, Math.round((float)pViewRect.y + (rowHeight - (float)glyphHeight) / 2.0f), null);
                                        }
                                    }
                                    lineWithAnno = -1;
                                }
                                if (++pViewIndex < pViewCount) {
                                    pViewDesc = lockedVH.getParagraphViewDescriptor(pViewIndex);
                                    pViewStartOffset = pViewDesc.getStartOffset();
                                    continue;
                                }
                            }
                            break;
                            break;
                        } while (true);
                    }
                }
                finally {
                    lockedVH.unlock();
                }
            }
        });
    }

    private void doRepaint(Rectangle r) {
        this.repaint(r);
        this.checkSize();
    }

    @Override
    public void changedLine(final int lineIndex) {
        EditorUI eui = this.editorUI;
        if (!this.init || eui == null) {
            return;
        }
        this.cachedCountOfAnnos = -1;
        final JTextComponent component = eui.getComponent();
        if (component != null) {
            final Document doc = component.getDocument();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    block9 : {
                        Element rootElem = doc.getDefaultRootElement();
                        if (lineIndex >= rootElem.getElementCount()) {
                            return;
                        }
                        Element lineElem = rootElem.getElement(lineIndex);
                        ViewHierarchy vh = ViewHierarchy.get((JTextComponent)component);
                        LockedViewHierarchy lvh = vh.lock();
                        try {
                            int pViewIndex = lvh.modelToParagraphViewIndex(lineElem.getStartOffset());
                            if (pViewIndex < 0) break block9;
                            ParagraphViewDescriptor pViewDesc = lvh.getParagraphViewDescriptor(pViewIndex);
                            Shape pViewAlloc = pViewDesc.getAllocation();
                            Rectangle repaintRect = pViewAlloc.getBounds();
                            repaintRect.width = (int)GlyphGutter.this.getSize().getWidth();
                            if (LOG.isLoggable(Level.FINE)) {
                                LOG.fine("GlyphGutter.changedLine() lineIndex=" + lineIndex + ", repaintRect=" + repaintRect + "\n");
                            }
                            if (SwingUtilities.isEventDispatchThread()) {
                                GlyphGutter.this.doRepaint(repaintRect);
                                break block9;
                            }
                            Object object = GlyphGutter.this.toRepaintLock;
                            synchronized (object) {
                                GlyphGutter.this.toRepaint = GlyphGutter.this.toRepaint != null ? GlyphGutter.this.toRepaint.union(repaintRect) : repaintRect;
                                GlyphGutter.this.repaintTask.schedule(50);
                            }
                        }
                        finally {
                            lvh.unlock();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void changedAll() {
        if (!this.init || this.editorUI == null) {
            return;
        }
        this.cachedCountOfAnnos = -1;
        Utilities.runInEventDispatchThread(new Runnable(){

            @Override
            public void run() {
                GlyphGutter.this.repaint();
                GlyphGutter.this.checkSize();
            }
        });
    }

    protected void checkSize() {
        int count = this.getLineCount();
        if (count != this.highestLineNumber) {
            this.highestLineNumber = count;
        }
        Dimension dim = this.getPreferredSize();
        if (this.getWidthDimension() != dim.width || this.getHeightDimension() > dim.height) {
            this.resize();
        }
        this.putDimensionForPrinting();
    }

    private void putDimensionForPrinting() {
        this.putClientProperty("print.size", new Dimension(this.getWidthDimension(), this.getHeightDimension()));
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        if (this.editorUI == null) {
            return null;
        }
        int line = this.getLineFromMouseEvent(e);
        if (this.annos.getNumberOfAnnotations(line) == 0) {
            return null;
        }
        if (this.isMouseOverCycleButton(e) && this.annos.getNumberOfAnnotations(line) > 1) {
            return MessageFormat.format(NbBundle.getBundle(BaseKit.class).getString("cycling-glyph_tooltip"), new Integer(this.annos.getNumberOfAnnotations(line)));
        }
        if (this.isMouseOverGlyph(e)) {
            AnnotationDesc activeAnnotation = this.annos.getActiveAnnotation(line);
            return activeAnnotation != null ? activeAnnotation.getShortDescription() : null;
        }
        return null;
    }

    private int getXPosOfGlyph(int line) {
        if (this.editorUI == null) {
            return -1;
        }
        if (this.cachedCountOfAnnos == -1 || this.cachedCountOfAnnosForLine != line) {
            this.cachedCountOfAnnos = this.annos.getNumberOfAnnotations(line);
            this.cachedCountOfAnnosForLine = line;
        }
        if (this.cachedCountOfAnnos > 0) {
            int xPos;
            int n = xPos = this.showLineNumbers ? this.getLineNumberWidth() : 0;
            if (this.drawOverLineNumbers) {
                xPos = this.getWidth() - 16;
                if (this.cachedCountOfAnnos > 1) {
                    xPos -= 9;
                }
            }
            return xPos;
        }
        return -1;
    }

    private boolean isMouseOverGlyph(MouseEvent e) {
        int line = this.getLineFromMouseEvent(e);
        int xPos = this.getXPosOfGlyph(line);
        if (xPos != -1 && e.getX() >= xPos && e.getX() <= xPos + 16) {
            return true;
        }
        return false;
    }

    private boolean isMouseOverCycleButton(MouseEvent e) {
        int line = this.getLineFromMouseEvent(e);
        int xPos = this.getXPosOfGlyph(line);
        if (xPos != -1 && e.getX() >= xPos + 16 && e.getX() <= xPos + 16 + 9) {
            return true;
        }
        return false;
    }

    @Override
    public JComponent createSideBar(JTextComponent target) {
        EditorUI eui = Utilities.getEditorUI(target);
        if (eui == null) {
            return null;
        }
        GlyphGutter glyph = new GlyphGutter(eui);
        eui.setGlyphGutter(glyph);
        return glyph;
    }

    private int getLineFromMouseEvent(MouseEvent e) {
        int line = -1;
        EditorUI eui = this.editorUI;
        if (eui != null) {
            try {
                JTextComponent component = eui.getComponent();
                BaseDocument document = eui.getDocument();
                BaseTextUI textUI = (BaseTextUI)component.getUI();
                int clickOffset = textUI.viewToModel(component, new Point(0, e.getY()));
                line = Utilities.getLineOffset(document, clickOffset);
            }
            catch (BadLocationException ble) {
                LOG.log(Level.WARNING, null, ble);
            }
        }
        return line;
    }

    class EditorUIListener
    implements PropertyChangeListener {
        EditorUIListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getSource() instanceof EditorUI) {
                if ((evt.getPropertyName() == null || "component".equals(evt.getPropertyName())) && evt.getNewValue() == null) {
                    GlyphGutter.this.editorUI.removePropertyChangeListener(this);
                    if (evt.getOldValue() instanceof JTextComponent) {
                        ((JTextComponent)evt.getOldValue()).removePropertyChangeListener(this);
                    }
                    GlyphGutter.this.annos.removeAnnotationsListener(GlyphGutter.this);
                    if (GlyphGutter.this.gutterMouseListener != null) {
                        GlyphGutter.this.removeMouseListener(GlyphGutter.this.gutterMouseListener);
                        GlyphGutter.this.removeMouseMotionListener(GlyphGutter.this.gutterMouseListener);
                    }
                    if (GlyphGutter.this.annoTypesListener != null) {
                        AnnotationTypes.getTypes().removePropertyChangeListener(GlyphGutter.this.annoTypesListener);
                    }
                    GlyphGutter.this.editorUI = null;
                    GlyphGutter.this.annos = null;
                }
            } else if (evt.getSource() instanceof JTextComponent) {
                if (evt.getPropertyName() == null || "document".equals(evt.getPropertyName())) {
                    GlyphGutter.this.annos.removeAnnotationsListener(GlyphGutter.this);
                    EditorUI eui = GlyphGutter.this.editorUI;
                    if (eui != null && eui.getDocument() != null) {
                        GlyphGutter.this.annos = eui.getDocument().getAnnotations();
                    }
                    GlyphGutter.this.annos.addAnnotationsListener(GlyphGutter.this);
                    GlyphGutter.this.update();
                } else if ("text-zoom".equals(evt.getPropertyName())) {
                    GlyphGutter.this.update();
                } else if ("font".equals(evt.getPropertyName())) {
                    GlyphGutter.this.update();
                }
            }
        }
    }

    class GutterMouseListener
    extends MouseAdapter
    implements MouseMotionListener {
        private int dragStartOffset;
        private int dragEndOffset;

        GutterMouseListener() {
            this.dragStartOffset = -1;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            EditorUI eui = GlyphGutter.this.editorUI;
            if (eui == null) {
                return;
            }
            eui.getComponent().requestFocus();
            if (e.getModifiers() == 16) {
                if (GlyphGutter.this.isMouseOverCycleButton(e)) {
                    int line = GlyphGutter.this.getLineFromMouseEvent(e);
                    e.consume();
                    GlyphGutter.this.annos.activateNextAnnotation(line);
                } else {
                    Action[] actions = ImplementationProvider.getDefault().getGlyphGutterActions(eui.getComponent());
                    if (actions == null && actions.length == 0) {
                        Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                    int currentLine = -1;
                    int line = GlyphGutter.this.getLineFromMouseEvent(e);
                    if (line == -1) {
                        Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                    Action toInvoke = null;
                    Action defaultAction = null;
                    AnnotationDesc active = GlyphGutter.this.annos.getActiveAnnotation(line);
                    if (active == null || !GlyphGutter.this.isMouseOverGlyph(e)) {
                        AnnotationDesc[] passive;
                        HashSet<AnnotationDesc> base = new HashSet<AnnotationDesc>();
                        if (active != null) {
                            base.add(active);
                        }
                        if ((passive = GlyphGutter.this.annos.getPassiveAnnotationsForLine(line)) != null) {
                            base.addAll(Arrays.asList(passive));
                        }
                        Collection<String> annotationTypes = this.computeAnnotationTypesToAnalyze(base);
                        for (Action a : actions) {
                            Object supportedAnnotationTypes;
                            Object defAction = a.getValue("default-action");
                            if (toInvoke == null && defAction != null && ((Boolean)defAction).booleanValue() && ((supportedAnnotationTypes = a.getValue("default-action-excluded-annotation-types")) == null || !(supportedAnnotationTypes instanceof String[]) || Collections.disjoint(Arrays.asList((String[])supportedAnnotationTypes), annotationTypes))) {
                                toInvoke = a;
                            }
                            if (defaultAction != null || !this.isLegacyAction(a)) continue;
                            defaultAction = a;
                        }
                    } else {
                        Collection<String> annotationTypes = this.computeAnnotationTypesToAnalyze(Arrays.asList(active));
                        for (Action a : actions) {
                            Object supportedAnnotationTypes = a.getValue("supported-annotation-types");
                            if (!(supportedAnnotationTypes instanceof String[])) continue;
                            if (toInvoke == null && !Collections.disjoint(Arrays.asList((String[])supportedAnnotationTypes), annotationTypes)) {
                                toInvoke = a;
                            }
                            if (defaultAction != null || !this.isLegacyAction(a)) continue;
                            defaultAction = a;
                        }
                    }
                    Action action = toInvoke = toInvoke != null ? toInvoke : defaultAction;
                    if (toInvoke != null && toInvoke.isEnabled()) {
                        BaseDocument document = eui.getDocument();
                        try {
                            currentLine = Utilities.getLineOffset(document, eui.getComponent().getCaret().getDot());
                        }
                        catch (BadLocationException ex) {
                            return;
                        }
                        if (line != currentLine) {
                            int offset = Utilities.getRowStartFromLineOffset(document, line);
                            JumpList.checkAddEntry();
                            eui.getComponent().getCaret().setDot(offset);
                        }
                        e.consume();
                        toInvoke.actionPerformed(new ActionEvent(eui.getComponent(), 0, ""));
                        GlyphGutter.this.repaint();
                    } else {
                        Toolkit.getDefaultToolkit().beep();
                    }
                }
            }
        }

        private boolean isLegacyAction(Action a) {
            return a.getValue("default-action") == null && a.getValue("supported-annotation-types") == null;
        }

        private Collection<String> computeAnnotationTypesToAnalyze(Collection<AnnotationDesc> startAt) {
            HashSet<String> annotationTypes = new HashSet<String>();
            LinkedList<AnnotationDesc> combinationsToAnalyze = new LinkedList<AnnotationDesc>(startAt);
            while (!combinationsToAnalyze.isEmpty()) {
                AnnotationDesc desc = combinationsToAnalyze.remove(0);
                annotationTypes.add(desc.getAnnotationType());
                if (!(desc instanceof Annotations.AnnotationCombination)) continue;
                combinationsToAnalyze.addAll(((Annotations.AnnotationCombination)desc).getCombinedAnnotations());
            }
            return annotationTypes;
        }

        private void showPopup(MouseEvent e) {
            final EditorUI eui = GlyphGutter.this.editorUI;
            if (eui == null) {
                return;
            }
            if (e.isPopupTrigger()) {
                int offset;
                int line = GlyphGutter.this.getLineFromMouseEvent(e);
                if (GlyphGutter.this.annos.getActiveAnnotation(line) != null) {
                    offset = GlyphGutter.this.annos.getActiveAnnotation(line).getOffset();
                } else {
                    BaseDocument document = eui.getDocument();
                    offset = Utilities.getRowStartFromLineOffset(document, line);
                }
                if (eui.getComponent().getCaret().getDot() != offset) {
                    JumpList.checkAddEntry();
                }
                eui.getComponent().getCaret().setDot(offset);
                JPopupMenu pm = GlyphGutter.this.annos.createPopupMenu(Utilities.getKit(eui.getComponent()), line);
                if (pm != null) {
                    e.consume();
                    pm.show(GlyphGutter.this, e.getX(), e.getY());
                    pm.addPopupMenuListener(new PopupMenuListener(){

                        @Override
                        public void popupMenuCanceled(PopupMenuEvent e2) {
                            eui.getComponent().requestFocus();
                        }

                        @Override
                        public void popupMenuWillBecomeInvisible(PopupMenuEvent e2) {
                            eui.getComponent().requestFocus();
                        }

                        @Override
                        public void popupMenuWillBecomeVisible(PopupMenuEvent e2) {
                        }
                    });
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            this.showPopup(e);
            if (!e.isConsumed() && (GlyphGutter.this.isMouseOverGlyph(e) || GlyphGutter.this.isMouseOverCycleButton(e))) {
                e.consume();
            }
            this.dragStartOffset = -1;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            this.showPopup(e);
            if (!e.isConsumed() && (GlyphGutter.this.isMouseOverGlyph(e) || GlyphGutter.this.isMouseOverCycleButton(e))) {
                e.consume();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void mouseDragged(MouseEvent e) {
            EditorUI eui = GlyphGutter.this.editorUI;
            if (eui == null) {
                return;
            }
            JTextComponent component = eui.getComponent();
            BaseTextUI textUI = (BaseTextUI)component.getUI();
            AbstractDocument aDoc = (AbstractDocument)component.getDocument();
            aDoc.readLock();
            try {
                int lineStartOffset = textUI.getPosFromY(e.getY());
                boolean updateDragEndOffset = false;
                if (this.dragStartOffset == -1) {
                    this.dragStartOffset = lineStartOffset;
                    this.dragEndOffset = lineStartOffset;
                } else if (this.dragStartOffset == this.dragEndOffset) {
                    if (lineStartOffset != this.dragStartOffset) {
                        updateDragEndOffset = true;
                    }
                } else {
                    updateDragEndOffset = true;
                }
                if (updateDragEndOffset) {
                    Caret caret = component.getCaret();
                    if (lineStartOffset >= this.dragStartOffset) {
                        if (caret.getMark() != this.dragStartOffset) {
                            caret.setDot(this.dragStartOffset);
                        }
                        this.dragEndOffset = Math.min(Utilities.getRowEnd((BaseDocument)aDoc, lineStartOffset) + 1, aDoc.getLength());
                    } else {
                        if (caret.getMark() == this.dragStartOffset) {
                            caret.setDot(Utilities.getRowEnd((BaseDocument)aDoc, this.dragStartOffset) + 1);
                        }
                        this.dragEndOffset = lineStartOffset;
                    }
                    component.moveCaretPosition(this.dragEndOffset);
                }
            }
            catch (BadLocationException ble) {}
            finally {
                aDoc.readUnlock();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

    }

}

