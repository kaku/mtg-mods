/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Font;

public interface PrintContainer {
    public void add(char[] var1, Font var2, Color var3, Color var4);

    public void eol();

    public boolean initEmptyLines();
}

