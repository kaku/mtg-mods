/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.TokenCategory;

public class BaseTokenCategory
implements TokenCategory {
    private final String name;
    private final int numericID;

    public BaseTokenCategory(String name) {
        this(name, 0);
    }

    public BaseTokenCategory(String name, int numericID) {
        this.name = name;
        this.numericID = numericID;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getNumericID() {
        return this.numericID;
    }

    public String toString() {
        return this.getName();
    }
}

