/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.actions.MacroRecording
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.editor;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.LocaleSupport;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WordMatch;
import org.netbeans.modules.editor.lib2.actions.MacroRecording;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public abstract class BaseAction
extends TextAction {
    public static final String POPUP_MENU_TEXT = "PopupMenuText";
    public static final String LOCALE_DESC_PREFIX = "desc-";
    public static final String LOCALE_POPUP_PREFIX = "popup-";
    public static final String ICON_RESOURCE_PROPERTY = "IconResource";
    public static final int SELECTION_REMOVE = 1;
    public static final int MAGIC_POSITION_RESET = 2;
    public static final int ABBREV_RESET = 4;
    public static final int UNDO_MERGE_RESET = 8;
    public static final int WORD_MATCH_RESET = 16;
    public static final int CLEAR_STATUS_TEXT = 32;
    public static final int NO_RECORDING = 64;
    public static final int SAVE_POSITION = 128;
    public static final String NO_KEYBINDING = "no-keybinding";
    private static Logger UILOG = Logger.getLogger("org.netbeans.ui.actions.editor");
    protected int updateMask;
    private static boolean recording;
    static final long serialVersionUID = -4255521122272110786L;

    public BaseAction() {
        this(null);
    }

    public BaseAction(int updateMask) {
        this(null, updateMask);
    }

    public BaseAction(String name) {
        this(name, 0);
    }

    public BaseAction(String name, int updateMask) {
        super(name);
        this.updateMask = updateMask;
    }

    protected Object findValue(String key) {
        return LocaleSupport.getString(key);
    }

    @Override
    public Object getValue(String key) {
        Object obj = super.getValue(key);
        if (obj == null && (obj = this.createDefaultValue(key)) != null) {
            this.putValue(key, obj);
        }
        return obj;
    }

    @Override
    public void putValue(String key, Object value) {
        super.putValue(key, value);
        if ("Name".equals(key) && value instanceof String) {
            this.actionNameUpdate((String)value);
        }
    }

    protected void actionNameUpdate(String actionName) {
    }

    protected Object createDefaultValue(String key) {
        String bundleKey;
        Object ret = null;
        if ("ShortDescription".equals(key)) {
            Class bundleClass = this.getShortDescriptionBundleClass();
            if (bundleClass != null) {
                String bundleKey2 = (String)this.getValue("Name");
                try {
                    ret = NbBundle.getBundle((Class)bundleClass).getString(bundleKey2);
                }
                catch (MissingResourceException mre) {
                    MissingResourceException mre2 = new MissingResourceException("Can't find SHORT_DESCRIPTION for " + this + "; bundleClass=" + bundleClass + "; bundleKey=" + bundleKey2, bundleClass.getName(), bundleKey2);
                    mre2.initCause(mre);
                    throw mre2;
                }
            } else {
                ret = this.getDefaultShortDescription();
            }
        } else if ("PopupMenuText".equals(key) && (ret = this.findValue(bundleKey = "popup-" + this.getValue("Name"))) == null) {
            ret = this.getValue("ShortDescription");
        }
        return ret;
    }

    protected Class getShortDescriptionBundleClass() {
        return null;
    }

    protected Object getDefaultShortDescription() {
        String actionName = (String)this.getValue("Name");
        String localizerKey = "desc-" + actionName;
        Object obj = this.findValue(localizerKey);
        if (obj == null && (obj = this.findValue(actionName)) == null) {
            obj = actionName;
        }
        return obj;
    }

    @Override
    public final void actionPerformed(final ActionEvent evt) {
        final JTextComponent target = this.getTextComponent(evt);
        if (target == null || !(target.getDocument() instanceof BaseDocument)) {
            return;
        }
        if (0 == (this.updateMask & 64)) {
            MacroRecording.get().recordAction((Action)this, evt, target);
        }
        this.updateComponent(target);
        if (UILOG.isLoggable(Level.FINE)) {
            String actionName;
            String string = actionName = this.getValue("Name") != null ? this.getValue("Name").toString().toLowerCase() : null;
            if (actionName != null && !"default-typed".equals(actionName) && -1 == actionName.indexOf("caret") && -1 == actionName.indexOf("delete") && -1 == actionName.indexOf("selection") && -1 == actionName.indexOf("build-tool-tip") && -1 == actionName.indexOf("build-popup-menu") && -1 == actionName.indexOf("page-up") && -1 == actionName.indexOf("page-down") && -1 == actionName.indexOf("-kit-install")) {
                LogRecord r = new LogRecord(Level.FINE, "UI_ACTION_EDITOR");
                r.setResourceBundle(NbBundle.getBundle(BaseAction.class));
                if (evt != null) {
                    r.setParameters(new Object[]{evt, evt.toString(), this, this.toString(), this.getValue("Name")});
                } else {
                    r.setParameters(new Object[]{"no-ActionEvent", "no-ActionEvent", this, this.toString(), this.getValue("Name")});
                }
                r.setLoggerName(UILOG.getName());
                UILOG.log(r);
            }
        }
        if (this.asynchonous()) {
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    BaseAction.this.actionPerformed(evt, target);
                }
            });
        } else {
            this.actionPerformed(evt, target);
        }
    }

    @Deprecated
    boolean startRecording(JTextComponent target) {
        boolean b = MacroRecording.get().startRecording();
        if (b) {
            recording = true;
            Utilities.setStatusText(target, NbBundle.getBundle(BaseAction.class).getString("macro-recording"));
        }
        return b;
    }

    @Deprecated
    String stopRecording(JTextComponent target) {
        String s = MacroRecording.get().stopRecording();
        if (s == null) {
            return s;
        }
        recording = false;
        Utilities.setStatusText(target, "");
        return s;
    }

    public abstract void actionPerformed(ActionEvent var1, JTextComponent var2);

    protected boolean asynchonous() {
        return false;
    }

    public JMenuItem getPopupMenuItem(JTextComponent target) {
        return null;
    }

    public String getPopupMenuText(JTextComponent target) {
        String txt = (String)this.getValue("PopupMenuText");
        if (txt == null) {
            txt = (String)this.getValue("Name");
        }
        return txt;
    }

    public void updateComponent(JTextComponent target) {
        this.updateComponent(target, this.updateMask);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateComponent(JTextComponent target, int updateMask) {
        if (target != null && target.getDocument() instanceof BaseDocument) {
            BaseDocument doc = (BaseDocument)target.getDocument();
            boolean writeLocked = false;
            try {
                if ((updateMask & 1) != 0) {
                    writeLocked = true;
                    doc.extWriteLock();
                    Caret caret = target.getCaret();
                    if (caret != null && Utilities.isSelectionShowing(caret)) {
                        int markPos;
                        int dot = caret.getDot();
                        if (dot < (markPos = caret.getMark())) {
                            int tmpPos = dot;
                            dot = markPos;
                            markPos = tmpPos;
                        }
                        try {
                            target.getDocument().remove(markPos, dot - markPos);
                        }
                        catch (BadLocationException e) {
                            Utilities.annotateLoggable(e);
                        }
                    }
                }
                if ((updateMask & 2) != 0 && target.getCaret() != null) {
                    target.getCaret().setMagicCaretPosition(null);
                }
                if ((updateMask & 8) != 0) {
                    doc.resetUndoMerge();
                }
                if ((updateMask & 16) != 0) {
                    ((BaseTextUI)target.getUI()).getEditorUI().getWordMatch().clear();
                }
                if (!recording && (updateMask & 32) != 0) {
                    Utilities.clearStatusText(target);
                }
                if ((updateMask & 128) != 0) {
                    JumpList.checkAddEntry(target);
                }
            }
            finally {
                if (writeLocked) {
                    doc.extWriteUnlock();
                }
            }
        }
    }

}

