/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import javax.swing.text.JTextComponent;
import javax.swing.text.Segment;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;

public class EditorDebug {
    private EditorDebug() {
    }

    public static void dumpPlanes(BaseDocument doc) {
    }

    public static void dumpSyntaxMarks(BaseDocument doc) {
        System.out.println("--------------------------- DUMP OF SYNTAX MARKS --------------------------------");
        int docLen = doc.getLength();
        System.out.println("--------------------------------------------------------------------------------\n");
    }

    public static void test(JTextComponent component) {
    }

    public static void checkSettings(Class kitClass) throws Exception {
    }

    public static String debugString(String s) {
        return s != null ? EditorDebug.debugChars(s.toCharArray(), 0, s.length()) : "NULL STRING";
    }

    public static String debugChars(Segment seg) {
        return EditorDebug.debugChars(seg.array, seg.offset, seg.count);
    }

    public static String debugChars(char[] chars) {
        return EditorDebug.debugChars(chars, 0, chars.length);
    }

    public static String debugChars(char[] chars, int offset, int len) {
        if (len < 0) {
            return "EditorDebug.debugChars() !ERROR! len=" + len + " < 0";
        }
        if (offset < 0) {
            return "EditorDebug.debugChars() !ERROR! offset=" + offset + " < 0";
        }
        if (offset + len > chars.length) {
            return "EditorDebug.debugChars() !ERROR! offset=" + offset + " + len=" + len + " > chars.length=" + chars.length;
        }
        StringBuffer sb = new StringBuffer(len);
        int endOffset = offset + len;
        while (offset < endOffset) {
            switch (chars[offset]) {
                case '\n': {
                    sb.append("\\n");
                    break;
                }
                case '\t': {
                    sb.append("\\t");
                    break;
                }
                case '\r': {
                    sb.append("\\r");
                    break;
                }
                default: {
                    sb.append(chars[offset]);
                }
            }
            ++offset;
        }
        return sb.toString();
    }

    public static String debugChar(char ch) {
        switch (ch) {
            case '\n': {
                return "\\n";
            }
            case '\t': {
                return "\\t";
            }
            case '\r': {
                return "\\r";
            }
        }
        return String.valueOf(ch);
    }

    public static String debugPairs(int[] pairs) {
        String ret;
        if (pairs == null) {
            ret = "Null pairs";
        } else if (pairs.length == 0) {
            ret = "No pairs";
        } else {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < pairs.length; i += 2) {
                sb.append('[');
                sb.append(pairs[i]);
                sb.append(", ");
                sb.append(pairs[i + 1]);
                if (i >= pairs.length - 1) continue;
                sb.append("]\n");
            }
            ret = sb.toString();
        }
        return ret;
    }

    public static String debugArray(Object[] array) {
        String ret;
        if (array == null) {
            ret = "Null array";
        } else if (array.length == 0) {
            ret = "Empty array";
        } else {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append('[');
                sb.append(i);
                sb.append("]=");
                sb.append(array[i]);
                if (i == array.length - 1) continue;
                sb.append('\n');
            }
            ret = sb.toString();
        }
        return ret;
    }

    public static String debugArray(int[] array) {
        String ret;
        if (array == null) {
            ret = "Null array";
        } else if (array.length == 0) {
            ret = "Empty array";
        } else {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append('[');
                sb.append(i);
                sb.append("]=");
                sb.append(array[i]);
                if (i == array.length - 1) continue;
                sb.append('\n');
            }
            ret = sb.toString();
        }
        return ret;
    }

    public static String debugBlocks(BaseDocument doc, int[] blocks) {
        String ret;
        if (blocks == null) {
            ret = "Null blocks";
        } else if (blocks.length == 0) {
            ret = "Empty blocks";
        } else if (blocks.length % 2 != 0) {
            ret = "Blocks.length=" + blocks.length + " is not even!";
        } else {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < blocks.length; i += 2) {
                sb.append('[');
                sb.append(i);
                sb.append("]=(");
                sb.append(blocks[i]);
                sb.append(", ");
                sb.append(blocks[i + 1]);
                sb.append(") or (");
                sb.append(Utilities.debugPosition(doc, blocks[i]));
                sb.append(", ");
                sb.append(Utilities.debugPosition(doc, blocks[i + 1]));
                sb.append(')');
                if (i == blocks.length - 1) continue;
                sb.append('\n');
            }
            ret = sb.toString();
        }
        return ret;
    }

    public static String debugList(List l) {
        String ret;
        if (l == null) {
            ret = "Null list";
        } else if (l.size() == 0) {
            ret = "Empty list";
        } else {
            int cnt = l.size();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < cnt; ++i) {
                sb.append('[');
                sb.append(i);
                sb.append("]=");
                sb.append(l.get(i));
                if (i == cnt - 1) continue;
                sb.append('\n');
            }
            ret = sb.toString();
        }
        return ret;
    }

    public static String debugIterator(Iterator i) {
        String ret;
        if (i == null) {
            ret = "Null iterator";
        } else if (!i.hasNext()) {
            ret = "Empty iterator";
        } else {
            StringBuffer sb = new StringBuffer();
            int ind = 0;
            while (i.hasNext()) {
                sb.append('[');
                sb.append(ind++);
                sb.append("]=");
                sb.append(i.next().toString());
                if (!i.hasNext()) continue;
                sb.append('\n');
            }
            ret = sb.toString();
        }
        return ret;
    }
}

