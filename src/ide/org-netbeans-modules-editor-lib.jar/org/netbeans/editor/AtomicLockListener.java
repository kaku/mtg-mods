/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.util.EventListener;
import org.netbeans.editor.AtomicLockEvent;

public interface AtomicLockListener
extends EventListener {
    public void atomicLock(AtomicLockEvent var1);

    public void atomicUnlock(AtomicLockEvent var1);
}

