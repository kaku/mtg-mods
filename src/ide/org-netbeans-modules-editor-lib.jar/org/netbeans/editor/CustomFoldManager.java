/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.lexer.Token
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenId
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.spi.editor.fold.FoldHierarchyTransaction
 *  org.netbeans.spi.editor.fold.FoldManager
 *  org.netbeans.spi.editor.fold.FoldManagerFactory
 *  org.netbeans.spi.editor.fold.FoldOperation
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.GapObjectArray;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.util.RequestProcessor;

final class CustomFoldManager
implements FoldManager,
Runnable {
    private static final Logger LOG = Logger.getLogger(CustomFoldManager.class.getName());
    public static final FoldType CUSTOM_FOLD_TYPE = new FoldType("custom-fold");
    private FoldOperation operation;
    private Document doc;
    private GapObjectArray markArray = new GapObjectArray();
    private int minUpdateMarkOffset = Integer.MAX_VALUE;
    private int maxUpdateMarkOffset = -1;
    private List removedFoldList;
    private HashMap customFoldId = new HashMap();
    private static final RequestProcessor RP = new RequestProcessor(CustomFoldManager.class.getName(), 1, false, false);
    private final RequestProcessor.Task task;
    private static Pattern pattern = Pattern.compile("(<\\s*editor-fold(?:(?:\\s+id=\"(\\S*)\")?(?:\\s+defaultstate=\"(\\S*?)\")?(?:\\s+desc=\"([\\S \\t]*?)\")?(?:\\s+defaultstate=\"(\\S*?)\")?)\\s*>)|(?:</\\s*editor-fold\\s*>)");

    CustomFoldManager() {
        this.task = RP.create((Runnable)this);
    }

    public void init(FoldOperation operation) {
        this.operation = operation;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Initialized: {0}", System.identityHashCode(this));
        }
    }

    private FoldOperation getOperation() {
        return this.operation;
    }

    public void initFolds(FoldHierarchyTransaction transaction) {
        this.doc = this.getOperation().getHierarchy().getComponent().getDocument();
        this.task.schedule(300);
    }

    public void insertUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
        this.processRemovedFolds(transaction);
        this.task.schedule(300);
    }

    public void removeUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
        this.processRemovedFolds(transaction);
        this.removeAffectedMarks(evt, transaction);
        this.task.schedule(300);
    }

    public void changedUpdate(DocumentEvent evt, FoldHierarchyTransaction transaction) {
    }

    public void removeEmptyNotify(Fold emptyFold) {
        this.removeFoldNotify(emptyFold);
    }

    public void removeDamagedNotify(Fold damagedFold) {
        this.removeFoldNotify(damagedFold);
    }

    public void expandNotify(Fold expandedFold) {
    }

    public void release() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Released: {0}", System.identityHashCode(this));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        if (this.operation.isReleased()) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Update skipped, already relaesed: {0}", System.identityHashCode(this));
            }
            return;
        }
        ((BaseDocument)this.doc).readLock();
        try {
            TokenHierarchy th = TokenHierarchy.get((Document)this.doc);
            if (th != null && th.isActive()) {
                FoldHierarchy hierarchy = this.getOperation().getHierarchy();
                hierarchy.lock();
                try {
                    if (this.operation.isReleased()) {
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.log(Level.FINE, "Update skipped, already relaesed: {0}", System.identityHashCode(this));
                        }
                        return;
                    }
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.log(Level.FINE, "Updating: {0}", System.identityHashCode(this));
                    }
                    FoldHierarchyTransaction transaction = this.getOperation().openTransaction();
                    try {
                        this.updateFolds(th.tokenSequence(), transaction);
                    }
                    finally {
                        transaction.commit();
                    }
                }
                finally {
                    hierarchy.unlock();
                }
            }
        }
        finally {
            ((BaseDocument)this.doc).readUnlock();
        }
    }

    private void removeFoldNotify(Fold removedFold) {
        if (this.removedFoldList == null) {
            this.removedFoldList = new ArrayList(3);
        }
        this.removedFoldList.add(removedFold);
    }

    private void removeAffectedMarks(DocumentEvent evt, FoldHierarchyTransaction transaction) {
        int removeOffset = evt.getOffset();
        int markIndex = this.findMarkIndex(removeOffset);
        if (markIndex < this.getMarkCount()) {
            FoldMarkInfo mark;
            while (markIndex >= 0 && (mark = this.getMark(markIndex)).getOffset() == removeOffset) {
                mark.release(false, transaction);
                this.removeMark(markIndex);
                --markIndex;
            }
        }
    }

    private void processRemovedFolds(FoldHierarchyTransaction transaction) {
        if (this.removedFoldList != null) {
            for (int i = this.removedFoldList.size() - 1; i >= 0; --i) {
                Fold removedFold = (Fold)this.removedFoldList.get(i);
                FoldMarkInfo startMark = (FoldMarkInfo)this.getOperation().getExtraInfo(removedFold);
                if (startMark.getId() != null) {
                    this.customFoldId.put(startMark.getId(), removedFold.isCollapsed());
                }
                FoldMarkInfo endMark = startMark.getPairMark();
                if (this.getOperation().isStartDamaged(removedFold)) {
                    startMark.release(true, transaction);
                }
                if (!this.getOperation().isEndDamaged(removedFold)) continue;
                endMark.release(true, transaction);
            }
        }
        this.removedFoldList = null;
    }

    private void markUpdate(FoldMarkInfo mark) {
        this.markUpdate(mark.getOffset());
    }

    private void markUpdate(int offset) {
        if (offset < this.minUpdateMarkOffset) {
            this.minUpdateMarkOffset = offset;
        }
        if (offset > this.maxUpdateMarkOffset) {
            this.maxUpdateMarkOffset = offset;
        }
    }

    private FoldMarkInfo getMark(int index) {
        return (FoldMarkInfo)this.markArray.getItem(index);
    }

    private int getMarkCount() {
        return this.markArray.getItemCount();
    }

    private void removeMark(int index) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Removing mark from ind=" + index + ": " + this.getMark(index));
        }
        this.markArray.remove(index, 1);
    }

    private void insertMark(int index, FoldMarkInfo mark) {
        this.markArray.insertItem(index, mark);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Inserted mark at ind=" + index + ": " + mark);
        }
    }

    private int findMarkIndex(int offset) {
        int markCount = this.getMarkCount();
        int low = 0;
        int high = markCount - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            int midMarkOffset = this.getMark(mid).getOffset();
            if (midMarkOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midMarkOffset > offset) {
                high = mid - 1;
                continue;
            }
            ++mid;
            while (mid < markCount && this.getMark(mid).getOffset() == offset) {
                ++mid;
            }
            return --mid;
        }
        return low;
    }

    private List<FoldMarkInfo> getMarkList(TokenSequence seq) {
        ArrayList<FoldMarkInfo> markList = null;
        seq.moveStart();
        while (seq.moveNext()) {
            FoldMarkInfo info;
            Token token = seq.token();
            try {
                info = this.scanToken(token);
            }
            catch (BadLocationException e) {
                LOG.log(Level.WARNING, null, e);
                info = null;
            }
            if (info == null) continue;
            if (markList == null) {
                markList = new ArrayList<FoldMarkInfo>();
            }
            markList.add(info);
        }
        return markList;
    }

    private void processTokenList(TokenSequence seq, FoldHierarchyTransaction transaction) {
        int markListSize;
        List<FoldMarkInfo> markList = this.getMarkList(seq);
        if (markList != null && (markListSize = markList.size()) > 0) {
            int arrayMarkOffset;
            FoldMarkInfo arrayMark;
            int offset = markList.get(0).getOffset();
            int arrayMarkIndex = this.findMarkIndex(offset);
            if (arrayMarkIndex < this.getMarkCount()) {
                arrayMark = this.getMark(arrayMarkIndex);
                arrayMarkOffset = arrayMark.getOffset();
            } else {
                arrayMark = null;
                arrayMarkOffset = Integer.MAX_VALUE;
            }
            for (int i = 0; i < markListSize; ++i) {
                FoldMarkInfo listMark = markList.get(i);
                int listMarkOffset = listMark.getOffset();
                if (i == 0 || i == markListSize - 1) {
                    this.markUpdate(listMarkOffset);
                }
                while (listMarkOffset >= arrayMarkOffset) {
                    if (listMarkOffset == arrayMarkOffset) {
                        listMark.setCollapsed(arrayMark.isCollapsed());
                    }
                    if (!arrayMark.isReleased()) {
                        arrayMark.release(false, transaction);
                    }
                    this.removeMark(arrayMarkIndex);
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.fine("Removed dup mark from ind=" + arrayMarkIndex + ": " + arrayMark);
                    }
                    if (arrayMarkIndex < this.getMarkCount()) {
                        arrayMark = this.getMark(arrayMarkIndex);
                        arrayMarkOffset = arrayMark.getOffset();
                        continue;
                    }
                    arrayMark = null;
                    arrayMarkOffset = Integer.MAX_VALUE;
                }
                this.insertMark(arrayMarkIndex, listMark);
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Inserted mark at ind=" + arrayMarkIndex + ": " + listMark);
                }
                ++arrayMarkIndex;
            }
        }
    }

    private void updateFolds(TokenSequence seq, FoldHierarchyTransaction transaction) {
        FoldMarkInfo parentMark;
        FoldMarkInfo prevMark;
        if (seq != null && !seq.isEmpty()) {
            this.processTokenList(seq, transaction);
        }
        if (this.maxUpdateMarkOffset == -1) {
            return;
        }
        int index = this.findMarkIndex(this.minUpdateMarkOffset);
        if (index == 0) {
            prevMark = null;
            parentMark = null;
        } else {
            prevMark = this.getMark(index - 1);
            parentMark = prevMark.getParentMark();
        }
        int markCount = this.getMarkCount();
        while (index < markCount) {
            FoldMarkInfo mark = this.getMark(index);
            if (mark.isReleased()) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Removing released mark at ind=" + index + ": " + mark);
                }
                this.removeMark(index);
                --markCount;
                continue;
            }
            if (mark.isStartMark()) {
                if (prevMark == null || prevMark.isStartMark()) {
                    mark.setParentMark(prevMark);
                    parentMark = prevMark;
                }
            } else if (prevMark != null) {
                if (prevMark.isStartMark()) {
                    prevMark.setEndMark(mark, false, transaction);
                } else if (parentMark != null) {
                    parentMark.setEndMark(mark, false, transaction);
                    parentMark = parentMark.getParentMark();
                } else {
                    mark.makeSolitaire(false, transaction);
                }
            } else {
                mark.makeSolitaire(false, transaction);
            }
            mark.setParentMark(parentMark);
            prevMark = mark;
            ++index;
        }
        this.minUpdateMarkOffset = Integer.MAX_VALUE;
        this.maxUpdateMarkOffset = -1;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("MARKS DUMP:\n" + this);
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        int markCount = this.getMarkCount();
        int markCountDigitCount = Integer.toString(markCount).length();
        for (int i = 0; i < markCount; ++i) {
            sb.append("[");
            String iStr = Integer.toString(i);
            CustomFoldManager.appendSpaces(sb, markCountDigitCount - iStr.length());
            sb.append(iStr);
            sb.append("]:");
            FoldMarkInfo mark = this.getMark(i);
            int indent = 0;
            for (FoldMarkInfo parentMark = mark.getParentMark(); parentMark != null; parentMark = parentMark.getParentMark()) {
                indent += 4;
            }
            CustomFoldManager.appendSpaces(sb, indent);
            sb.append(mark);
            sb.append('\n');
        }
        return sb.toString();
    }

    private static void appendSpaces(StringBuffer sb, int spaces) {
        while (--spaces >= 0) {
            sb.append(' ');
        }
    }

    private FoldMarkInfo scanToken(Token token) throws BadLocationException {
        Matcher matcher;
        if (token.id().primaryCategory() != null && token.id().primaryCategory().startsWith("comment") && (matcher = pattern.matcher(token.text())).find()) {
            if (matcher.group(1) != null) {
                boolean state = matcher.group(3) != null ? "collapsed".equals(matcher.group(3)) : "collapsed".equals(matcher.group(5));
                if (matcher.group(2) != null) {
                    Boolean collapsed = (Boolean)this.customFoldId.get(matcher.group(2));
                    if (collapsed != null) {
                        state = collapsed;
                    } else {
                        this.customFoldId.put(matcher.group(2), state);
                    }
                }
                return new FoldMarkInfo(true, token.offset(null), matcher.end(0), matcher.group(2), state, matcher.group(4));
            }
            return new FoldMarkInfo(false, token.offset(null), matcher.end(0), null, false, null);
        }
        return null;
    }

    public static final class Factory
    implements FoldManagerFactory {
        public FoldManager createFoldManager() {
            return new CustomFoldManager();
        }
    }

    private final class FoldMarkInfo {
        private boolean startMark;
        private Position pos;
        private int length;
        private String id;
        private boolean collapsed;
        private String description;
        private FoldMarkInfo pairMark;
        private FoldMarkInfo parentMark;
        private Fold fold;
        private boolean released;

        private FoldMarkInfo(boolean startMark, int offset, int length, String id, boolean collapsed, String description) throws BadLocationException {
            this.startMark = startMark;
            this.pos = CustomFoldManager.this.doc.createPosition(offset);
            this.length = length;
            this.id = id;
            this.collapsed = collapsed;
            this.description = description;
        }

        public String getId() {
            return this.id;
        }

        public String getDescription() {
            return this.description;
        }

        public boolean isStartMark() {
            return this.startMark;
        }

        public int getLength() {
            return this.length;
        }

        public int getOffset() {
            return this.pos.getOffset();
        }

        public int getEndOffset() {
            return this.getOffset() + this.getLength();
        }

        public boolean isCollapsed() {
            return this.fold != null ? this.fold.isCollapsed() : this.collapsed;
        }

        public boolean hasFold() {
            return this.fold != null;
        }

        public void setCollapsed(boolean collapsed) {
            this.collapsed = collapsed;
        }

        public boolean isSolitaire() {
            return this.pairMark == null;
        }

        public void makeSolitaire(boolean forced, FoldHierarchyTransaction transaction) {
            if (!this.isSolitaire()) {
                if (this.isStartMark()) {
                    this.setEndMark(null, forced, transaction);
                } else {
                    this.getPairMark().setEndMark(null, forced, transaction);
                }
            }
        }

        public boolean isReleased() {
            return this.released;
        }

        public void release(boolean forced, FoldHierarchyTransaction transaction) {
            if (!this.released) {
                this.makeSolitaire(forced, transaction);
                this.released = true;
                CustomFoldManager.this.markUpdate(this);
            }
        }

        public FoldMarkInfo getPairMark() {
            return this.pairMark;
        }

        private void setPairMark(FoldMarkInfo pairMark) {
            this.pairMark = pairMark;
        }

        public void setEndMark(FoldMarkInfo endMark, boolean forced, FoldHierarchyTransaction transaction) {
            if (!this.isStartMark()) {
                throw new IllegalStateException("Not start mark");
            }
            if (this.pairMark == endMark) {
                return;
            }
            if (this.pairMark != null) {
                this.releaseFold(forced, transaction);
                this.pairMark.setPairMark(null);
            }
            this.pairMark = endMark;
            if (endMark != null) {
                if (!endMark.isSolitaire()) {
                    endMark.makeSolitaire(false, transaction);
                }
                endMark.setPairMark(this);
                endMark.setParentMark(this.getParentMark());
                this.ensureFoldExists(transaction);
            }
        }

        public FoldMarkInfo getParentMark() {
            return this.parentMark;
        }

        public void setParentMark(FoldMarkInfo parentMark) {
            this.parentMark = parentMark;
        }

        private void releaseFold(boolean forced, FoldHierarchyTransaction transaction) {
            if (this.isSolitaire() || !this.isStartMark()) {
                throw new IllegalStateException();
            }
            if (this.fold != null) {
                this.setCollapsed(this.fold.isCollapsed());
                if (!forced) {
                    CustomFoldManager.this.getOperation().removeFromHierarchy(this.fold, transaction);
                }
                this.fold = null;
            }
        }

        public Fold getFold() {
            if (this.isSolitaire()) {
                return null;
            }
            if (!this.isStartMark()) {
                return this.pairMark.getFold();
            }
            return this.fold;
        }

        public void ensureFoldExists(FoldHierarchyTransaction transaction) {
            if (this.isSolitaire() || !this.isStartMark()) {
                throw new IllegalStateException();
            }
            if (this.fold == null) {
                try {
                    if (!this.startMark) {
                        throw new IllegalStateException("Not start mark: " + this);
                    }
                    if (this.pairMark == null) {
                        throw new IllegalStateException("No pairMark for mark:" + this);
                    }
                    int startOffset = this.getOffset();
                    int startGuardedLength = this.getLength();
                    int endGuardedLength = this.pairMark.getLength();
                    int endOffset = this.pairMark.getOffset() + endGuardedLength;
                    this.fold = CustomFoldManager.this.getOperation().addToHierarchy(CustomFoldManager.CUSTOM_FOLD_TYPE, this.getDescription(), this.collapsed, startOffset, endOffset, startGuardedLength, endGuardedLength, (Object)this, transaction);
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, null, e);
                }
            }
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(this.isStartMark() ? 'S' : 'E');
            if (this.hasFold() || !this.isSolitaire() && this.getPairMark().hasFold()) {
                sb.append("F");
                if (this.isStartMark() && (this.isSolitaire() || this.getOffset() != this.fold.getStartOffset() || this.getPairMark().getEndOffset() != this.fold.getEndOffset())) {
                    sb.append("!!<");
                    sb.append(this.fold.getStartOffset());
                    sb.append(",");
                    sb.append(this.fold.getEndOffset());
                    sb.append(">!!");
                }
            }
            sb.append(" (");
            sb.append("o=");
            sb.append(this.pos.getOffset());
            sb.append(", l=");
            sb.append(this.length);
            sb.append(", d='");
            sb.append(this.description);
            sb.append('\'');
            if (this.getPairMark() != null) {
                sb.append(", <->");
                sb.append(this.getPairMark().getOffset());
            }
            if (this.getParentMark() != null) {
                sb.append(", ^");
                sb.append(this.getParentMark().getOffset());
            }
            sb.append(')');
            return sb.toString();
        }
    }

}

