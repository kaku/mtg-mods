/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.GapBranchElement
 *  org.netbeans.lib.editor.util.swing.GapBranchElement$Edit
 *  org.openide.ErrorManager
 */
package org.netbeans.editor;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.StyleContext;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.LineElement;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.GapBranchElement;
import org.openide.ErrorManager;

final class LineRootElement
extends GapBranchElement {
    private static final LineElement[] EMPTY_LINE_ELEMENT_ARRAY = new LineElement[0];
    private static final String NAME = "section";
    private BaseDocument doc;
    private LineElement[] addedLines = EMPTY_LINE_ELEMENT_ARRAY;

    LineRootElement(BaseDocument doc) {
        this.doc = doc;
        assert (doc.getLength() == 0);
        Position startPos = doc.getStartPosition();
        assert (startPos.getOffset() == 0);
        Position endPos = doc.getEndPosition();
        assert (endPos.getOffset() == 1);
        LineElement line = new LineElement(this, startPos, endPos);
        this.replace(0, 0, new Element[]{line});
        assert (this.getElement(0) != null);
    }

    private int doubleAddedLinesCapacity() {
        int addedLinesLength = this.addedLines.length;
        int newCapacity = Math.max(4, addedLinesLength * 2);
        LineElement[] newAddedLines = new LineElement[newCapacity];
        System.arraycopy(this.addedLines, 0, newAddedLines, newCapacity - addedLinesLength, addedLinesLength);
        this.addedLines = newAddedLines;
        return newCapacity - addedLinesLength;
    }

    public Element getElement(int index) {
        if (index < 0) {
            throw new IndexOutOfBoundsException("Invalid line index=" + index + " < 0");
        }
        int elementCount = this.getElementCount();
        if (index >= elementCount) {
            throw new IndexOutOfBoundsException("Invalid line index=" + index + " >= lineCount=" + elementCount);
        }
        LineElement elem = (LineElement)super.getElement(index);
        if (elem == null) {
            throw new IndexOutOfBoundsException("Can't find element, index=" + index + ", count=" + this.getElementCount() + ", documentLocked=" + (DocumentUtilities.isReadLocked((Document)this.doc) || DocumentUtilities.isWriteLocked((Document)this.doc)));
        }
        return elem;
    }

    UndoableEdit insertUpdate(int insertOffset, int insertLength) {
        int beforeInsertOffset;
        boolean insertAtPrevLineEndOffset;
        int lastInsertedCharOffset = insertOffset + insertLength - 1;
        CharSequence text = DocumentUtilities.getText((Document)this.doc);
        GapBranchElement.Edit edit = null;
        int index = -1;
        Element[] removeElements = null;
        int firstAddedLineIndex = this.addedLines.length;
        int offset = lastInsertedCharOffset;
        if (insertOffset == 0) {
            beforeInsertOffset = 0;
            insertAtPrevLineEndOffset = false;
        } else {
            beforeInsertOffset = insertOffset - 1;
            insertAtPrevLineEndOffset = text.charAt(beforeInsertOffset) == '\n';
        }
        try {
            Position futureAddedLineEndPos = null;
            while (offset >= beforeInsertOffset) {
                if (text.charAt(offset) == '\n') {
                    boolean addLine = true;
                    if (futureAddedLineEndPos == null) {
                        index = this.getElementIndex(insertOffset);
                        LineElement removeLine = (LineElement)this.getElement(index);
                        if (insertAtPrevLineEndOffset) {
                            if (offset == lastInsertedCharOffset) {
                                removeElements = new Element[]{removeLine};
                                futureAddedLineEndPos = removeLine.getEndPosition();
                                addLine = false;
                            } else {
                                LineElement nextRemoveLine = (LineElement)this.getElement(index + 1);
                                removeElements = new Element[]{removeLine, nextRemoveLine};
                                futureAddedLineEndPos = nextRemoveLine.getEndPosition();
                            }
                        } else {
                            removeElements = new Element[]{removeLine};
                            futureAddedLineEndPos = removeLine.getEndPosition();
                        }
                    }
                    if (addLine) {
                        if (firstAddedLineIndex == 0) {
                            firstAddedLineIndex = this.doubleAddedLinesCapacity();
                        }
                        Position lineStartPos = this.doc.createPosition(offset + 1);
                        this.addedLines[--firstAddedLineIndex] = new LineElement(this, lineStartPos, futureAddedLineEndPos);
                        futureAddedLineEndPos = lineStartPos;
                    }
                }
                --offset;
            }
            if (futureAddedLineEndPos != null) {
                int addedLineCount = this.addedLines.length - firstAddedLineIndex;
                Element[] addElements = new Element[addedLineCount + 1];
                System.arraycopy(this.addedLines, firstAddedLineIndex, addElements, 1, addedLineCount);
                addElements[0] = new LineElement(this, ((LineElement)removeElements[0]).getStartPosition(), futureAddedLineEndPos);
                this.replace(index, removeElements.length, addElements);
                edit = new GapBranchElement.Edit((GapBranchElement)this, index, removeElements, addElements);
            }
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify(65536, (Throwable)e);
        }
        return edit;
    }

    UndoableEdit removeUpdate(int removeOffset, int removeLength) {
        int line1;
        GapBranchElement.Edit edit = null;
        int removeEndOffset = removeOffset + removeLength;
        int line0 = this.getElementIndex(removeOffset);
        if (line0 != (line1 = this.getElementIndex(removeEndOffset))) {
            Element[] removeElements = new Element[++line1 - line0];
            this.copyElements(line0, line1, removeElements, 0);
            Element[] addElements = new Element[]{new LineElement(this, ((LineElement)removeElements[0]).getStartPosition(), ((LineElement)removeElements[removeElements.length - 1]).getEndPosition())};
            this.replace(line0, removeElements.length, addElements);
            edit = new GapBranchElement.Edit((GapBranchElement)this, line0, removeElements, addElements);
        }
        return edit;
    }

    public Document getDocument() {
        return this.doc;
    }

    public Element getParentElement() {
        return null;
    }

    public String getName() {
        return "section";
    }

    public AttributeSet getAttributes() {
        return StyleContext.getDefaultStyleContext().getEmptySet();
    }

    public int getStartOffset() {
        return 0;
    }

    public int getEndOffset() {
        return this.doc.getLength() + 1;
    }

    public int getElementIndex(int offset) {
        if (offset == 0) {
            return 0;
        }
        return super.getElementIndex(offset);
    }

    private void checkConsistency() {
        int lineCount = this.getElementCount();
        assert (lineCount > 0);
        int prevLineEndOffset = 0;
        for (int i = 0; i < lineCount; ++i) {
            LineElement elem = (LineElement)this.getElement(i);
            assert (prevLineEndOffset == elem.getStartOffset());
            assert (prevLineEndOffset < elem.getEndOffset());
            prevLineEndOffset = elem.getEndOffset();
        }
        assert (prevLineEndOffset == this.doc.getLength() + 1);
    }

    private String lineToString(Element line) {
        return "<" + line.getStartOffset() + ", " + line.getEndOffset() + ">";
    }
}

