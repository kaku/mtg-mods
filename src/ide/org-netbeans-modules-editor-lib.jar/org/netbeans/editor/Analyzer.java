/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.spi.lexer.MutableTextInput
 *  org.netbeans.spi.lexer.TokenHierarchyControl
 */
package org.netbeans.editor;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Segment;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.LineSeparatorConversion;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenHierarchyControl;

public class Analyzer {
    private static Object platformLS;
    public static final char[] EMPTY_CHAR_ARRAY;
    private static char[] spacesBuffer;
    private static char[] tabsBuffer;
    private static final int MAX_CACHED_SPACES_STRING_LENGTH = 50;
    private static final String[] spacesStrings;

    private Analyzer() {
    }

    public static Object getPlatformLS() {
        if (platformLS == null) {
            platformLS = System.getProperty("line.separator");
        }
        return platformLS;
    }

    public static String testLS(char[] chars, int len) {
        for (int i = 0; i < len; ++i) {
            switch (chars[i]) {
                case '\r': {
                    if (i + 1 < len && chars[i + 1] == '\n') {
                        return "\r\n";
                    }
                    return "\r";
                }
                case '\n': {
                    return "\n";
                }
            }
        }
        return null;
    }

    public static int convertLSToLF(char[] chars, int len) {
        int moveLen;
        int tgtOffset = 0;
        int lsLen = 0;
        int moveStart = 0;
        for (int i = 0; i < len; ++i) {
            if (chars[i] == '\r') {
                lsLen = i + 1 < len && chars[i + 1] == '\n' ? 2 : 1;
            } else if (chars[i] == '\u2028' || chars[i] == '\u2029') {
                lsLen = 1;
            }
            if (lsLen <= 0) continue;
            moveLen = i - moveStart;
            if (moveLen > 0) {
                if (tgtOffset != moveStart) {
                    System.arraycopy(chars, moveStart, chars, tgtOffset, moveLen);
                }
                tgtOffset += moveLen;
            }
            chars[tgtOffset++] = 10;
            moveStart += moveLen + lsLen;
            i += lsLen - 1;
            lsLen = 0;
        }
        moveLen = len - moveStart;
        if (moveLen > 0) {
            if (tgtOffset != moveStart) {
                System.arraycopy(chars, moveStart, chars, tgtOffset, moveLen);
            }
            tgtOffset += moveLen;
        }
        return tgtOffset;
    }

    public static String convertLSToLF(String text) {
        int moveLen;
        char[] tgtChars = null;
        int tgtOffset = 0;
        int lsLen = 0;
        int moveStart = 0;
        int textLen = text.length();
        for (int i = 0; i < textLen; ++i) {
            if (text.charAt(i) == '\r') {
                lsLen = i + 1 < textLen && text.charAt(i + 1) == '\n' ? 2 : 1;
            } else if (text.charAt(i) == '\u2028' || text.charAt(i) == '\u2029') {
                lsLen = 1;
            }
            if (lsLen <= 0) continue;
            if (tgtChars == null) {
                tgtChars = new char[textLen];
                text.getChars(0, textLen, tgtChars, 0);
            }
            if ((moveLen = i - moveStart) > 0) {
                if (tgtOffset != moveStart) {
                    text.getChars(moveStart, moveStart + moveLen, tgtChars, tgtOffset);
                }
                tgtOffset += moveLen;
            }
            tgtChars[tgtOffset++] = 10;
            moveStart += moveLen + lsLen;
            i += lsLen - 1;
            lsLen = 0;
        }
        moveLen = textLen - moveStart;
        if (moveLen > 0) {
            if (tgtOffset != moveStart) {
                text.getChars(moveStart, moveStart + moveLen, tgtChars, tgtOffset);
            }
            tgtOffset += moveLen;
        }
        return tgtChars == null ? text : new String(tgtChars, 0, tgtOffset);
    }

    public static boolean isSpace(String s) {
        int len = s.length();
        for (int i = 0; i < len; ++i) {
            if (s.charAt(i) == ' ') continue;
            return false;
        }
        return true;
    }

    public static boolean isSpace(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (chars[offset++] != ' ') {
                return false;
            }
            --len;
        }
        return true;
    }

    public static boolean isWhitespace(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (!Character.isWhitespace(chars[offset])) {
                return false;
            }
            ++offset;
            --len;
        }
        return true;
    }

    public static int findFirstNonTab(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (chars[offset] != '\t') {
                return offset;
            }
            ++offset;
            --len;
        }
        return -1;
    }

    public static int findFirstNonSpace(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (chars[offset] != ' ') {
                return offset;
            }
            ++offset;
            --len;
        }
        return -1;
    }

    public static int findFirstNonWhite(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (!Character.isWhitespace(chars[offset])) {
                return offset;
            }
            ++offset;
            --len;
        }
        return -1;
    }

    public static int findLastNonWhite(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        for (int i = offset + len - 1; i >= offset; --i) {
            if (Character.isWhitespace(chars[i])) continue;
            return i;
        }
        return -1;
    }

    public static int getLFCount(char[] chars) {
        return Analyzer.getLFCount(chars, 0, chars.length);
    }

    public static int getLFCount(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        int lfCount = 0;
        while (len > 0) {
            if (chars[offset++] == '\n') {
                ++lfCount;
            }
            --len;
        }
        return lfCount;
    }

    public static int getLFCount(String s) {
        int lfCount = 0;
        int len = s.length();
        for (int i = 0; i < len; ++i) {
            if (s.charAt(i) != '\n') continue;
            ++lfCount;
        }
        return lfCount;
    }

    public static int findFirstLFOffset(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (chars[offset++] == '\n') {
                return offset - 1;
            }
            --len;
        }
        return -1;
    }

    public static int findFirstLFOffset(String s) {
        int len = s.length();
        for (int i = 0; i < len; ++i) {
            if (s.charAt(i) != '\n') continue;
            return i;
        }
        return -1;
    }

    public static int findFirstTab(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            if (chars[offset++] == '\t') {
                return offset - 1;
            }
            --len;
        }
        return -1;
    }

    public static int findFirstTabOrLF(char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        while (len > 0) {
            switch (chars[offset++]) {
                case '\t': 
                case '\n': {
                    return offset - 1;
                }
            }
            --len;
        }
        return -1;
    }

    public static void reverse(char[] chars, int len) {
        for (int i = --len - 1 >> 1; i >= 0; --i) {
            char ch = chars[i];
            chars[i] = chars[len - i];
            chars[len - i] = ch;
        }
    }

    public static boolean equals(String s, char[] chars) {
        return Analyzer.equals(s, chars, 0, chars.length);
    }

    public static boolean equals(String s, char[] chars, int offset, int len) {
        assert (offset + len <= chars.length);
        if (s.length() != len) {
            return false;
        }
        for (int i = 0; i < len; ++i) {
            if (s.charAt(i) == chars[offset + i]) continue;
            return false;
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void initialRead(BaseDocument doc, Reader reader, boolean testLS) throws IOException {
        if (doc.getLength() > 0) {
            return;
        }
        if (reader != null) {
            boolean deactivateTokenHierarchy;
            int readBufferSize = (Integer)doc.getProperty("read-buffer-size");
            if (testLS) {
                reader = new LineSeparatorConversion.InitialSeparatorReader(reader);
            }
            LineSeparatorConversion.ToLineFeed toLF = new LineSeparatorConversion.ToLineFeed(reader, readBufferSize);
            boolean firstRead = true;
            int pos = 0;
            boolean line = false;
            boolean maxLineLength = false;
            boolean lineStartPos = false;
            boolean markCount = false;
            Segment text = toLF.nextConverted();
            TokenHierarchy hi = TokenHierarchy.get((Document)doc);
            MutableTextInput mti = (MutableTextInput)doc.getProperty(MutableTextInput.class);
            boolean bl = deactivateTokenHierarchy = mti != null && toLF.isReadWholeBuffer();
            if (deactivateTokenHierarchy) {
                mti.tokenHierarchyControl().setActive(false);
            }
            try {
                while (text != null) {
                    try {
                        doc.insertString(pos, new String(text.array, text.offset, text.count), null);
                    }
                    catch (BadLocationException e) {
                        throw new IllegalStateException(e.toString());
                    }
                    pos += text.count;
                    text = toLF.nextConverted();
                }
            }
            finally {
                if (deactivateTokenHierarchy) {
                    mti.tokenHierarchyControl().setActive(true);
                }
            }
            if (testLS) {
                doc.putProperty("__EndOfLine__", ((LineSeparatorConversion.InitialSeparatorReader)reader).getInitialSeparator());
            }
        }
    }

    static void read(BaseDocument doc, Reader reader, int pos) throws BadLocationException, IOException {
        int readBufferSize = (Integer)doc.getProperty("read-buffer-size");
        LineSeparatorConversion.ToLineFeed toLF = new LineSeparatorConversion.ToLineFeed(reader, readBufferSize);
        Segment text = toLF.nextConverted();
        while (text != null) {
            doc.insertString(pos, new String(text.array, text.offset, text.count), null);
            pos += text.count;
            text = toLF.nextConverted();
        }
    }

    static void write(BaseDocument doc, Writer writer, int pos, int len) throws BadLocationException, IOException {
        String lsType = (String)doc.getProperty("write-line-separator");
        if (lsType == null && (lsType = (String)doc.getProperty("__EndOfLine__")) == null) {
            lsType = "\n";
        }
        int writeBufferSize = (Integer)doc.getProperty("write-buffer-size");
        char[] getBuf = new char[writeBufferSize];
        char[] writeBuf = new char[2 * writeBufferSize];
        int actLen = 0;
        while (len > 0) {
            actLen = Math.min(len, writeBufferSize);
            doc.getChars(pos, getBuf, 0, actLen);
            int tgtLen = Analyzer.convertLFToLS(getBuf, actLen, writeBuf, lsType);
            writer.write(writeBuf, 0, tgtLen);
            pos += actLen;
            len -= actLen;
        }
    }

    public static int getColumn(char[] buffer, int offset, int len, int tabSize, int startCol) {
        int col = startCol;
        int endOffset = offset + len;
        if (tabSize <= 0) {
            new Exception("Wrong tab size=" + tabSize).printStackTrace();
            tabSize = 8;
        }
        block3 : while (offset < endOffset) {
            switch (buffer[offset++]) {
                case '\t': {
                    col = (col + tabSize) / tabSize * tabSize;
                    continue block3;
                }
            }
            ++col;
        }
        return col;
    }

    public static synchronized char[] getSpacesBuffer(int numSpaces) {
        while (numSpaces > spacesBuffer.length) {
            char[] tmpBuf = new char[spacesBuffer.length * 2];
            System.arraycopy(spacesBuffer, 0, tmpBuf, 0, spacesBuffer.length);
            System.arraycopy(spacesBuffer, 0, tmpBuf, spacesBuffer.length, spacesBuffer.length);
            spacesBuffer = tmpBuf;
        }
        return spacesBuffer;
    }

    public static synchronized String getSpacesString(int numSpaces) {
        if (numSpaces <= 50) {
            String ret = spacesStrings[numSpaces];
            if (ret == null) {
                Analyzer.spacesStrings[numSpaces] = ret = spacesStrings[50].substring(0, numSpaces);
            }
            return ret;
        }
        return new String(Analyzer.getSpacesBuffer(numSpaces), 0, numSpaces);
    }

    public static char[] createSpacesBuffer(int numSpaces) {
        char[] ret = new char[numSpaces];
        System.arraycopy(Analyzer.getSpacesBuffer(numSpaces), 0, ret, 0, numSpaces);
        return ret;
    }

    public static char[] getTabsBuffer(int numTabs) {
        if (numTabs > tabsBuffer.length) {
            char[] tmpBuf = new char[numTabs * 2];
            for (int i = 0; i < tmpBuf.length; i += Analyzer.tabsBuffer.length) {
                System.arraycopy(tabsBuffer, 0, tmpBuf, i, Math.min(tabsBuffer.length, tmpBuf.length - i));
            }
            tabsBuffer = tmpBuf;
        }
        return tabsBuffer;
    }

    public static String getIndentString(int indent, boolean expandTabs, int tabSize) {
        return Analyzer.getWhitespaceString(0, indent, expandTabs, tabSize);
    }

    public static String getWhitespaceString(int startCol, int endCol, boolean expandTabs, int tabSize) {
        return expandTabs || tabSize <= 0 ? Analyzer.getSpacesString(endCol - startCol) : new String(Analyzer.createWhiteSpaceFillBuffer(startCol, endCol, tabSize));
    }

    public static char[] createWhiteSpaceFillBuffer(int startCol, int endCol, int tabSize) {
        return Analyzer.createWhitespaceFillBuffer(startCol, endCol, tabSize);
    }

    public static char[] createWhitespaceFillBuffer(int startCol, int endCol, int tabSize) {
        if (startCol >= endCol) {
            return EMPTY_CHAR_ARRAY;
        }
        if (tabSize <= 0) {
            new Exception("Wrong tab size=" + tabSize).printStackTrace();
            tabSize = 8;
        }
        int tabs = 0;
        int spaces = 0;
        int nextTab = (startCol + tabSize) / tabSize * tabSize;
        if (nextTab > endCol) {
            spaces += endCol - startCol;
        } else {
            ++tabs;
            int endSpaces = endCol - endCol / tabSize * tabSize;
            tabs += (endCol - endSpaces - nextTab) / tabSize;
            spaces += endSpaces;
        }
        char[] ret = new char[tabs + spaces];
        if (tabs > 0) {
            System.arraycopy(Analyzer.getTabsBuffer(tabs), 0, ret, 0, tabs);
        }
        if (spaces > 0) {
            System.arraycopy(Analyzer.getSpacesBuffer(spaces), 0, ret, tabs, spaces);
        }
        return ret;
    }

    public static char[] loadFile(String fileName) throws IOException {
        File file = new File(fileName);
        char[] chars = new char[(int)file.length()];
        FileReader reader = new FileReader(file);
        reader.read(chars);
        reader.close();
        int len = Analyzer.convertLSToLF(chars, chars.length);
        if (len != chars.length) {
            char[] copyChars = new char[len];
            System.arraycopy(chars, 0, copyChars, 0, len);
            chars = copyChars;
        }
        return chars;
    }

    public static int convertLFToLS(char[] src, int len, char[] tgt, String lsType) {
        if (lsType != null && lsType.length() == 1) {
            char ls = lsType.charAt(0);
            if (ls == '\r' || ls == '\u2028' || ls == '\u2029') {
                for (int i = 0; i < len; ++i) {
                    tgt[i] = src[i] == '\n' ? ls : src[i];
                }
                return len;
            }
        } else if (lsType.equals("\r\n")) {
            int moveLen;
            int tgtLen = 0;
            int moveStart = 0;
            for (int i = 0; i < len; ++i) {
                if (src[i] != '\n') continue;
                moveLen = i - moveStart;
                if (moveLen > 0) {
                    System.arraycopy(src, moveStart, tgt, tgtLen, moveLen);
                    tgtLen += moveLen;
                }
                tgt[tgtLen++] = 13;
                tgt[tgtLen++] = 10;
                moveStart = i + 1;
            }
            moveLen = len - moveStart;
            if (moveLen > 0) {
                System.arraycopy(src, moveStart, tgt, tgtLen, moveLen);
                tgtLen += moveLen;
            }
            return tgtLen;
        }
        System.arraycopy(src, 0, tgt, 0, len);
        return len;
    }

    public static boolean startsWith(char[] chars, char[] prefix) {
        if (chars == null || chars.length < prefix.length) {
            return false;
        }
        for (int i = 0; i < prefix.length; ++i) {
            if (chars[i] == prefix[i]) continue;
            return false;
        }
        return true;
    }

    public static boolean endsWith(char[] chars, char[] suffix) {
        if (chars == null || chars.length < suffix.length) {
            return false;
        }
        for (int i = chars.length - suffix.length; i < chars.length; ++i) {
            if (chars[i] == suffix[i]) continue;
            return false;
        }
        return true;
    }

    public static char[] concat(char[] chars1, char[] chars2) {
        if (chars1 == null || chars1.length == 0) {
            return chars2;
        }
        if (chars2 == null || chars2.length == 0) {
            return chars1;
        }
        char[] ret = new char[chars1.length + chars2.length];
        System.arraycopy(chars1, 0, ret, 0, chars1.length);
        System.arraycopy(chars2, 0, ret, chars1.length, chars2.length);
        return ret;
    }

    public static char[] extract(char[] chars, int offset, int len) {
        char[] ret = new char[len];
        System.arraycopy(chars, offset, ret, 0, len);
        return ret;
    }

    public static boolean blocksHit(int[] blocks, int startPos, int endPos) {
        return Analyzer.blocksIndex(blocks, startPos, endPos) >= 0;
    }

    public static int blocksIndex(int[] blocks, int startPos, int endPos) {
        if (blocks.length > 0) {
            int onlyEven = -2;
            int low = 0;
            int high = blocks.length - 2;
            while (low <= high) {
                int mid = (low + high) / 2 & onlyEven;
                if (blocks[mid + 1] <= startPos) {
                    low = mid + 2;
                    continue;
                }
                if (blocks[mid] >= endPos) {
                    high = mid - 2;
                    continue;
                }
                return low;
            }
        }
        return -1;
    }

    public static String removeSpaces(String s) {
        int spcInd = s.indexOf(32);
        if (spcInd >= 0) {
            StringBuffer sb = new StringBuffer(s.substring(0, spcInd));
            int sLen = s.length();
            for (int i = spcInd + 1; i < sLen; ++i) {
                char ch = s.charAt(i);
                if (ch == ' ') continue;
                sb.append(ch);
            }
            return sb.toString();
        }
        return s;
    }

    static {
        EMPTY_CHAR_ARRAY = new char[0];
        spacesBuffer = new char[]{' '};
        tabsBuffer = new char[]{'\t'};
        spacesStrings = new String[51];
        Analyzer.spacesStrings[0] = "";
        Analyzer.spacesStrings[50] = new String(Analyzer.getSpacesBuffer(50), 0, 50);
    }
}

