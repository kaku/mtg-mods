/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

public interface ObjectArray {
    public Object getItem(int var1);

    public int getItemCount();

    public static interface Modification {
        public ObjectArray getArray();

        public int getIndex();

        public Object[] getAddedItems();

        public int getRemovedItemsCount();
    }

    public static interface CopyItems {
        public void copyItems(int var1, int var2, Object[] var3, int var4);
    }

}

