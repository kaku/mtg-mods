/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.io.IOException;
import java.io.PrintStream;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.EditorDebug;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;

public class SyntaxDebug {
    public static final String NO_STATE_ASSIGNED = "NO STATE ASSIGNED";
    public static final String NULL_STATE = "NULL STATE";
    public static final String NULL_SYNTAX_MARK = "NULL SYNTAX MARK";
    public Syntax syntax;

    public SyntaxDebug(Syntax syntax) {
        this.syntax = syntax;
    }

    public int parseFile(String fileName) throws IOException {
        char[] chars = Analyzer.loadFile(fileName);
        this.syntax.load(null, chars, 0, chars.length, true, 0);
        int tokenCnt = this.debugScan();
        return tokenCnt;
    }

    public int debugScan() {
        int tokenCnt = 0;
        do {
            TokenID tokenID;
            if ((tokenID = this.syntax.nextToken()) == null) {
                System.out.println("EOT at offset=" + this.syntax.getTokenOffset());
                return tokenCnt;
            }
            ++tokenCnt;
            System.out.println(tokenID.getName() + " in " + this.syntax.getTokenContextPath() + ": TEXT='" + EditorDebug.debugChars(this.syntax.getBuffer(), this.syntax.getTokenOffset(), this.syntax.getTokenLength()) + "', offset=" + this.syntax.getTokenOffset() + ", len=" + this.syntax.getTokenLength());
        } while (true);
    }
}

