/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.DialogSupport;
import org.netbeans.editor.KeySequenceInputPanel;
import org.netbeans.editor.Utilities;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class MacroSavePanel
extends JPanel {
    private final ResourceBundle bundle = NbBundle.getBundle(BaseKit.class);
    private Vector bindings = new Vector();
    private Class kitClass;
    public JButton addButton;
    public JLabel bindingLabel;
    public JList bindingList;
    public JPanel bindingPanel;
    public JScrollPane bindingScrollPane;
    public JTextField macroField;
    public JLabel macroLabel;
    public JPanel macroPanel;
    public JTextField nameField;
    public JLabel nameLabel;
    public JButton removeButton;

    public MacroSavePanel(Class kitClass) {
        this.kitClass = kitClass;
        this.initComponents();
        this.nameField.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_MSP_Name"));
        this.macroField.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_MSP_Macro"));
        this.bindingList.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_MSP_Keys"));
        this.getAccessibleContext().setAccessibleName(this.bundle.getString("MDS_title"));
        this.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_MSP"));
        this.setMaximumSize(new Dimension(400, 200));
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension pref = super.getPreferredSize();
        Dimension max = this.getMaximumSize();
        if (pref.width > max.width) {
            pref.width = max.width;
        }
        if (pref.height > max.height) {
            pref.height = max.height;
        }
        return pref;
    }

    private void initComponents() {
        this.macroPanel = new JPanel();
        this.nameLabel = new JLabel();
        this.macroLabel = new JLabel();
        this.nameField = new JTextField();
        this.macroField = new JTextField();
        this.bindingPanel = new JPanel();
        this.bindingLabel = new JLabel();
        this.bindingScrollPane = new JScrollPane();
        this.bindingList = new JList();
        this.addButton = new JButton();
        this.removeButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 11, 11));
        this.setLayout(new GridBagLayout());
        this.macroPanel.setLayout(new GridBagLayout());
        this.nameLabel.setLabelFor(this.nameField);
        Mnemonics.setLocalizedText((JLabel)this.nameLabel, (String)this.bundle.getString("MSP_Name"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.macroPanel.add((Component)this.nameLabel, gridBagConstraints);
        this.macroLabel.setLabelFor(this.macroField);
        Mnemonics.setLocalizedText((JLabel)this.macroLabel, (String)this.bundle.getString("MSP_Macro"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(5, 0, 0, 12);
        this.macroPanel.add((Component)this.macroLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        this.macroPanel.add((Component)this.nameField, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(5, 0, 0, 0);
        this.macroPanel.add((Component)this.macroField, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        this.add((Component)this.macroPanel, gridBagConstraints);
        this.bindingPanel.setLayout(new GridBagLayout());
        this.bindingLabel.setLabelFor(this.bindingList);
        Mnemonics.setLocalizedText((JLabel)this.bindingLabel, (String)this.bundle.getString("MSP_Keys"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        this.bindingPanel.add((Component)this.bindingLabel, gridBagConstraints);
        this.bindingList.setCellRenderer(new KeySequenceCellRenderer());
        this.bindingScrollPane.setViewportView(this.bindingList);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.bindingPanel.add((Component)this.bindingScrollPane, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.addButton, (String)this.bundle.getString("MSP_Add"));
        this.addButton.setToolTipText(this.bundle.getString("MSP_AddToolTip"));
        this.addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                MacroSavePanel.this.addBindingActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        this.bindingPanel.add((Component)this.addButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this.removeButton, (String)this.bundle.getString("MSP_Remove"));
        this.removeButton.setToolTipText(this.bundle.getString("MSP_RemoveToolTip"));
        this.removeButton.setEnabled(false);
        this.removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                MacroSavePanel.this.removeBindingActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        this.bindingPanel.add((Component)this.removeButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.bindingPanel, gridBagConstraints);
    }

    private void removeBindingActionPerformed(ActionEvent evt) {
        int index = this.bindingList.getSelectedIndex();
        if (index >= 0) {
            this.bindings.remove(index);
            this.bindingList.setListData(this.bindings);
        }
        if (this.bindingList.getModel().getSize() <= 0) {
            this.removeButton.setEnabled(false);
        } else {
            this.bindingList.setSelectedIndex(0);
        }
    }

    private void addBindingActionPerformed(ActionEvent evt) {
        KeyStroke[] newKeyStrokes = new KeySequenceRequester().getKeySequence();
        if (newKeyStrokes != null) {
            this.bindings.add(newKeyStrokes);
            this.bindingList.setListData(this.bindings);
            this.bindingList.setSelectedIndex(0);
            this.removeButton.setEnabled(true);
        }
    }

    public String getMacroName() {
        return this.nameField.getText();
    }

    public void setMacroName(String name) {
        this.nameField.setText(name);
    }

    public String getMacroBody() {
        return this.macroField.getText();
    }

    public void setMacroBody(String body) {
        this.macroField.setText(body);
    }

    public List getKeySequences() {
        return new ArrayList(this.bindings);
    }

    public void setKeySequences(List sequences) {
        this.bindings = new Vector(sequences);
        this.bindingList.setListData(this.bindings);
    }

    public void popupNotify() {
        this.nameField.requestFocus();
    }

    class KeySequenceRequester {
        KeySequenceInputPanel panel;
        Dialog dial;
        JButton[] buttons;
        KeyStroke[] retVal;

        KeySequenceRequester() {
            this.buttons = new JButton[]{new JButton(MacroSavePanel.this.bundle.getString("MSP_ok")), new JButton(), new JButton(MacroSavePanel.this.bundle.getString("MSP_cancel"))};
            this.retVal = null;
            this.buttons[0].getAccessibleContext().setAccessibleDescription(MacroSavePanel.this.bundle.getString("ACSD_MSP_ok"));
            this.buttons[1].getAccessibleContext().setAccessibleDescription(MacroSavePanel.this.bundle.getString("ACSD_MSP_clear"));
            Mnemonics.setLocalizedText((AbstractButton)this.buttons[1], (String)MacroSavePanel.this.bundle.getString("MSP_clear"));
            this.buttons[2].getAccessibleContext().setAccessibleDescription(MacroSavePanel.this.bundle.getString("ACSD_MSP_cancel"));
            this.buttons[0].setEnabled(false);
            this.panel = new KeySequenceInputPanel();
            this.panel.addPropertyChangeListener(new PropertyChangeListener(MacroSavePanel.this){
                final /* synthetic */ MacroSavePanel val$this$0;

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if ("keySequence" != evt.getPropertyName()) {
                        return;
                    }
                    KeyStroke[] seq = KeySequenceRequester.this.panel.getKeySequence();
                    String warn = KeySequenceRequester.this.isAlreadyBounded(seq);
                    if (warn == null) {
                        warn = KeySequenceRequester.this.getCollisionString(seq);
                    }
                    KeySequenceRequester.this.buttons[0].setEnabled(seq.length > 0 && warn == null);
                    KeySequenceRequester.this.panel.setInfoText(warn == null ? "" : warn);
                }
            });
            this.dial = DialogSupport.createDialog(MacroSavePanel.this.bundle.getString("MSP_AddTitle"), this.panel, true, this.buttons, false, -1, 2, new ActionListener(MacroSavePanel.this){
                final /* synthetic */ MacroSavePanel val$this$0;

                @Override
                public void actionPerformed(ActionEvent evt) {
                    if (evt.getSource() == KeySequenceRequester.this.buttons[1]) {
                        KeySequenceRequester.this.panel.clear();
                        KeySequenceRequester.this.panel.requestFocus();
                    } else if (evt.getSource() == KeySequenceRequester.this.buttons[0]) {
                        KeySequenceRequester.this.retVal = KeySequenceRequester.this.panel.getKeySequence();
                        KeySequenceRequester.this.dial.dispose();
                    } else if (evt.getSource() == KeySequenceRequester.this.buttons[2]) {
                        KeySequenceRequester.this.retVal = null;
                        KeySequenceRequester.this.dial.dispose();
                    }
                }
            });
        }

        KeyStroke[] getKeySequence() {
            this.dial.pack();
            this.panel.requestFocus();
            this.dial.show();
            return this.retVal;
        }

        String isAlreadyBounded(KeyStroke[] seq) {
            if (seq.length == 0) {
                return null;
            }
            Iterator it = MacroSavePanel.this.bindings.iterator();
            while (it.hasNext()) {
                if (!this.isOverlapingSequence((KeyStroke[])it.next(), seq)) continue;
                return MacroSavePanel.this.bundle.getString("MSP_Collision");
            }
            return null;
        }

        String getCollisionString(KeyStroke[] seq) {
            return null;
        }

        private boolean isOverlapingSequence(KeyStroke[] s1, KeyStroke[] s2) {
            int l = Math.min(s1.length, s2.length);
            if (l == 0) {
                return false;
            }
            while (l-- > 0) {
                if (s1[l].equals(s2[l])) continue;
                return false;
            }
            return true;
        }

    }

    private static class KeySequenceCellRenderer
    extends JLabel
    implements ListCellRenderer {
        public KeySequenceCellRenderer() {
            this.setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            this.setText(Utilities.keySequenceToString((KeyStroke[])value));
            this.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
            this.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
            return this;
        }
    }

}

