/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;

public interface TokenProcessor {
    public boolean token(TokenID var1, TokenContextPath var2, int var3, int var4);

    public int eot(int var1);

    public void nextBuffer(char[] var1, int var2, int var3, int var4, int var5, boolean var6);
}

