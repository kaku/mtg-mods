/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.util.HashMap;

public class EditorState {
    private static HashMap state = new HashMap();

    private EditorState() {
    }

    public static Object get(Object key) {
        return state.get(key);
    }

    public static void put(Object key, Object value) {
        state.put(key, value);
    }

    public static HashMap getStateObject() {
        return state;
    }

    public static void setStateObject(HashMap stateObject) {
        state = stateObject;
    }
}

