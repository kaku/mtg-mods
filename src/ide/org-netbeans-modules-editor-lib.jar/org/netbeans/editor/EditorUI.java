/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.EditorPreferencesDefaults
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.undo.UndoManager;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.Abbrev;
import org.netbeans.editor.BaseCaret;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.FontMetricsCache;
import org.netbeans.editor.GlyphGutter;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.PopupManager;
import org.netbeans.editor.StatusBar;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WordMatch;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib.ColoringMap;
import org.netbeans.modules.editor.lib.EditorExtPackageAccessor;
import org.netbeans.modules.editor.lib.KitsTracker;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.netbeans.modules.editor.lib.drawing.DrawLayerList;
import org.netbeans.modules.editor.lib.drawing.EditorUiAccessor;
import org.netbeans.modules.editor.lib2.EditorPreferencesDefaults;
import org.openide.util.WeakListeners;

public class EditorUI
implements ChangeListener,
PropertyChangeListener,
MouseListener {
    private static final Logger LOG = Logger.getLogger(EditorUI.class.getName());
    public static final String OVERWRITE_MODE_PROPERTY = "overwriteMode";
    public static final String COMPONENT_PROPERTY = "component";
    public static final int SCROLL_DEFAULT = 0;
    public static final int SCROLL_MOVE = 1;
    public static final int SCROLL_SMALLEST = 2;
    public static final int SCROLL_FIND = 3;
    static final Insets NULL_INSETS = new Insets(0, 0, 0, 0);
    private static final Insets DEFAULT_INSETS = new Insets(0, 2, 0, 0);
    public static final Insets defaultLineNumberMargin = new Insets(0, 3, 0, 3);
    private JTextComponent component;
    private JComponent extComponent;
    private JToolBar toolBarComponent;
    PropertyChangeSupport propertyChangeSupport;
    private BaseDocument printDoc;
    private ColoringMap coloringMap;
    private int lineHeight;
    private float lineHeightCorrection;
    private int lineAscent;
    int defaultSpaceWidth;
    boolean highlightSearch;
    boolean lineNumberEnabled;
    boolean lineNumberVisibleSetting;
    boolean lineNumberVisible;
    int lineNumberWidth;
    int lineNumberDigitWidth;
    int lineNumberMaxDigitCount;
    int textLeftMarginWidth;
    Insets textMargin;
    Insets scrollJumpInsets;
    Insets scrollFindInsets;
    private final HashMap<Object, Object> props;
    boolean textLimitLineVisible;
    int textLimitWidth;
    private Abbrev abbrev;
    private WordMatch wordMatch;
    private Object componentLock;
    StatusBar statusBar;
    private FocusAdapter focusL;
    Map<?, ?> renderingHints;
    private GlyphGutter glyphGutter;
    private boolean disableLineNumbers;
    private JPanel glyphCorner;
    private boolean popupMenuEnabled;
    public static final String LINE_HEIGHT_CHANGED_PROP = "line-height-changed-prop";
    public static final String TAB_SIZE_CHANGED_PROP = "tab-size-changed-prop";
    private static boolean isPasteActionInited = false;
    private Preferences prefs;
    private final Listener listener;
    private PreferenceChangeListener weakPrefsListener;
    private final DrawLayerList drawLayerList;
    private ToolTipSupport toolTipSupport;
    private JPopupMenu popupMenu;
    private PopupManager popupManager;

    public EditorUI() {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.lineHeight = -1;
        this.lineHeightCorrection = 1.0f;
        this.lineAscent = -1;
        this.defaultSpaceWidth = 1;
        this.textMargin = DEFAULT_INSETS;
        this.props = new HashMap(11);
        this.glyphGutter = null;
        this.disableLineNumbers = true;
        this.prefs = null;
        this.listener = new Listener();
        this.weakPrefsListener = null;
        this.drawLayerList = new DrawLayerList();
        this.focusL = new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent evt) {
                BaseTextUI ui;
                EditorUI.this.stateChanged(null);
                if (EditorUI.this.component != null && (ui = (BaseTextUI)EditorUI.this.component.getUI()) != null) {
                    ui.refresh();
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (e.isTemporary()) {
                    EditorUI.this.doStateChange(true);
                }
            }
        };
        this.getToolTipSupport();
    }

    public EditorUI(BaseDocument printDoc) {
        this(printDoc, true, true);
    }

    public EditorUI(BaseDocument printDoc, boolean usePrintColoringMap, boolean lineNumberEnabled) {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.lineHeight = -1;
        this.lineHeightCorrection = 1.0f;
        this.lineAscent = -1;
        this.defaultSpaceWidth = 1;
        this.textMargin = DEFAULT_INSETS;
        this.props = new HashMap(11);
        this.glyphGutter = null;
        this.disableLineNumbers = true;
        this.prefs = null;
        this.listener = new Listener();
        this.weakPrefsListener = null;
        this.drawLayerList = new DrawLayerList();
        this.printDoc = printDoc;
        this.listener.preferenceChange(null);
        this.setLineNumberEnabled(lineNumberEnabled);
        this.updateLineNumberWidth(0);
    }

    protected static Map<String, Coloring> getSharedColoringMap(Class kitClass) {
        String mimeType = KitsTracker.getInstance().findMimeType(kitClass);
        return ColoringMap.get(mimeType).getMap();
    }

    void initLineHeight(JTextComponent c) {
        this.updateLineHeight(c);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void installUI(JTextComponent c) {
        String mimeType = Utilities.getMimeType(c);
        MimePath mimePath = MimePath.parse((String)mimeType);
        this.prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        this.coloringMap = ColoringMap.get(mimeType);
        this.coloringMap.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.coloringMap));
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)mimePath).lookup(FontColorSettings.class);
        this.renderingHints = (Map)fcs.getFontColors("default").getAttribute(EditorStyleConstants.RenderingHints);
        Object object = this.getComponentLock();
        synchronized (object) {
            BaseDocument doc;
            this.component = c;
            this.putProperty("component", c);
            this.component.addPropertyChangeListener(this);
            this.component.addFocusListener(this.focusL);
            this.component.addMouseListener(this);
            Caret caret = this.component.getCaret();
            if (caret != null) {
                caret.addChangeListener(this);
            }
            if ((doc = this.getDocument()) != null) {
                this.modelChanged(null, doc);
            }
        }
        this.weakPrefsListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.listener, (Object)this.prefs);
        this.prefs.addPreferenceChangeListener(this.weakPrefsListener);
        this.listener.preferenceChange(null);
        if (!GraphicsEnvironment.isHeadless()) {
            this.component.setDragEnabled(true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void uninstallUI(JTextComponent c) {
        if (this.prefs != null && this.weakPrefsListener != null) {
            this.prefs.removePreferenceChangeListener(this.weakPrefsListener);
        }
        if (this.coloringMap != null) {
            this.coloringMap.removePropertyChangeListener(this);
        }
        Object object = this.getComponentLock();
        synchronized (object) {
            BaseDocument doc;
            if (this.component != null) {
                Caret caret = this.component.getCaret();
                if (caret != null) {
                    caret.removeChangeListener(this);
                }
                this.component.removePropertyChangeListener(this);
                this.component.removeFocusListener(this.focusL);
                this.component.removeMouseListener(this);
            }
            if ((doc = this.getDocument()) != null) {
                this.modelChanged(doc, null);
            }
            this.component = null;
            this.putProperty("component", null);
            FontMetricsCache.clear();
        }
        this.coloringMap = null;
        this.prefs = null;
        this.weakPrefsListener = null;
        this.renderingHints = null;
    }

    public Object getComponentLock() {
        if (this.componentLock == null) {
            this.componentLock = new ComponentLock();
        }
        return this.componentLock;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.propertyChangeSupport.addPropertyChangeListener(l);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.propertyChangeSupport.addPropertyChangeListener(propertyName, l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.propertyChangeSupport.removePropertyChangeListener(l);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.propertyChangeSupport.removePropertyChangeListener(propertyName, l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected void settingsChangeImpl(String settingName) {
    }

    @Override
    public void stateChanged(ChangeEvent evt) {
        this.doStateChange(false);
    }

    private void doStateChange(final boolean b) {
        SwingUtilities.invokeLater(new Runnable(){

            private boolean[] isCaretGuarded() {
                JTextComponent c = EditorUI.this.component;
                BaseDocument bdoc = EditorUI.this.getDocument();
                if (bdoc instanceof GuardedDocument) {
                    boolean guarded;
                    GuardedDocument gdoc = (GuardedDocument)bdoc;
                    boolean selectionSpansGuardedSection = false;
                    for (int i = c.getSelectionStart(); i < c.getSelectionEnd(); ++i) {
                        if (!gdoc.isPosGuarded(i)) continue;
                        selectionSpansGuardedSection = true;
                        break;
                    }
                    if (selectionSpansGuardedSection) {
                        return new boolean[]{true, true};
                    }
                    int offset = c.getCaretPosition();
                    boolean startGuarded = guarded = gdoc.isPosGuarded(offset);
                    if (offset > 0 && !gdoc.isPosGuarded(offset - 1) && DocumentUtilities.getText((Document)bdoc).charAt(offset - 1) == '\n') {
                        startGuarded = false;
                    }
                    return new boolean[]{guarded, startGuarded};
                }
                return new boolean[]{false, false};
            }

            @Override
            public void run() {
                BaseKit kit;
                JTextComponent c = EditorUI.this.component;
                if (c != null && (b || c.hasFocus()) && (kit = Utilities.getKit(c)) != null) {
                    boolean isEditable = c.isEditable();
                    boolean selectionVisible = Utilities.isSelectionShowing(c);
                    boolean[] caretGuarded = this.isCaretGuarded();
                    Action a = kit.getActionByName("copy-to-clipboard");
                    if (a != null) {
                        a.setEnabled(true);
                    }
                    if ((a = kit.getActionByName("cut-to-clipboard")) != null) {
                        a.setEnabled(!caretGuarded[0] && isEditable);
                    }
                    if ((a = kit.getActionByName("remove-selection")) != null) {
                        a.setEnabled(selectionVisible && !caretGuarded[0] && isEditable);
                    }
                    if ((a = kit.getActionByName("paste-from-clipboard")) != null) {
                        if (!isPasteActionInited) {
                            a.setEnabled(!a.isEnabled());
                            isPasteActionInited = true;
                        }
                        a.setEnabled(!caretGuarded[1] && isEditable);
                    }
                    if ((a = kit.getActionByName("paste-formated")) != null) {
                        a.setEnabled(!caretGuarded[1] && isEditable);
                    }
                }
            }
        });
    }

    protected void modelChanged(BaseDocument oldDoc, BaseDocument newDoc) {
        if (newDoc != null) {
            this.coloringMap = ColoringMap.get(Utilities.getMimeType(newDoc));
            this.listener.preferenceChange(null);
            this.checkUndoManager(newDoc);
        }
    }

    private void checkUndoManager(Document doc) {
        if (doc instanceof AbstractDocument && ((AbstractDocument)doc).getUndoableEditListeners().length >= 1) {
            return;
        }
        UndoManager undoManager = (UndoManager)doc.getProperty("undo-manager");
        if (undoManager == null) {
            undoManager = (UndoManager)doc.getProperty(UndoManager.class);
        }
        if (this.hasExtComponent()) {
            if (undoManager != null) {
                doc.removeUndoableEditListener(undoManager);
                doc.putProperty("undo-manager", null);
                doc.putProperty(UndoManager.class, null);
            }
        } else if (undoManager == null) {
            undoManager = new UndoManager();
            doc.addUndoableEditListener(undoManager);
            doc.putProperty("undo-manager", undoManager);
            doc.putProperty(UndoManager.class, undoManager);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if ("document".equals(propName)) {
            BaseDocument oldDoc = evt.getOldValue() instanceof BaseDocument ? (BaseDocument)evt.getOldValue() : null;
            BaseDocument newDoc = evt.getNewValue() instanceof BaseDocument ? (BaseDocument)evt.getNewValue() : null;
            this.modelChanged(oldDoc, newDoc);
        } else if ("margin".equals(propName)) {
            this.updateTextMargin();
        } else if ("caret".equals(propName)) {
            if (evt.getOldValue() instanceof Caret) {
                ((Caret)evt.getOldValue()).removeChangeListener(this);
            }
            if (evt.getNewValue() instanceof Caret) {
                ((Caret)evt.getNewValue()).addChangeListener(this);
            }
        } else if ("enabled".equals(propName) && !this.component.isEnabled()) {
            this.component.getCaret().setVisible(false);
        }
        if (propName == null || "ColoringMap.PROP_COLORING_MAP".equals(propName)) {
            this.listener.preferenceChange(null);
        }
    }

    protected Map createColoringMap() {
        return Collections.emptyMap();
    }

    public int getLineHeight() {
        if (this.lineHeight == -1 && this.component != null) {
            this.updateLineHeight(this.component);
        }
        return this.lineHeight > 0 ? this.lineHeight : 1;
    }

    public int getLineAscent() {
        if (this.lineAscent == -1 && this.component != null) {
            this.updateLineHeight(this.component);
        }
        return this.lineAscent > 0 ? this.lineAscent : 1;
    }

    public Map<String, Coloring> getColoringMap() {
        return new HashMap<String, Coloring>(this.getCMInternal());
    }

    private Map<String, Coloring> getCMInternal() {
        ColoringMap cm = this.coloringMap;
        if (cm != null) {
            return cm.getMap();
        }
        return ColoringMap.get(null).getMap();
    }

    public Coloring getDefaultColoring() {
        MimePath mimePath = this.component != null ? MimePath.get((String)DocumentUtilities.getMimeType((JTextComponent)this.component)) : MimePath.EMPTY;
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)mimePath).lookup(FontColorSettings.class);
        Coloring c = Coloring.fromAttributeSet(fcs.getFontColors("default"));
        assert (c != null);
        return c;
    }

    public Coloring getColoring(String coloringName) {
        return this.getCMInternal().get(coloringName);
    }

    private void updateLineHeight(final JTextComponent component) {
        if (component == null) {
            return;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Computing lineHeight for '" + Utilities.getMimeType(component) + "'");
        }
        Map<String, Coloring> cm = this.getCMInternal();
        int maxHeight = -1;
        int maxAscent = -1;
        for (String coloringName : cm.keySet()) {
            Font font;
            FontMetrics fm;
            if ("status-bar".equals(coloringName) || "status-bar-bold".equals(coloringName)) continue;
            Coloring c = cm.get(coloringName);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("Probing coloring '" + coloringName + "' : " + c);
            }
            if (c == null || (font = c.getFont()) == null || (c.getFontMode() & 4) == 0 || (fm = FontMetricsCache.getFontMetrics(font, component)) == null) continue;
            if (LOG.isLoggable(Level.FINE)) {
                if (maxHeight < fm.getHeight()) {
                    LOG.fine("Updating maxHeight from " + maxHeight + " to " + fm.getHeight() + ", coloringName=" + coloringName + ", font=" + font);
                }
                if (maxAscent < fm.getAscent()) {
                    LOG.fine("Updating maxAscent from " + maxAscent + " to " + fm.getAscent() + ", coloringName=" + coloringName + ", font=" + font);
                }
            }
            maxHeight = Math.max(maxHeight, fm.getHeight());
            maxAscent = Math.max(maxAscent, fm.getAscent());
        }
        final View rootView = Utilities.getDocumentView(component);
        final int[] wrapMaxHeight = new int[]{-1};
        if (rootView != null) {
            Utilities.runViewHierarchyTransaction(component, true, new Runnable(){

                @Override
                public void run() {
                    View view;
                    for (int i = 0; i < 1 && (view = rootView.getView(i)) != null; ++i) {
                        int offset = view.getStartOffset();
                        Rectangle r = null;
                        try {
                            r = component.getUI().modelToView(component, offset);
                        }
                        catch (BadLocationException ble) {
                            LOG.log(Level.INFO, null, ble);
                        }
                        if (r == null) break;
                        if (LOG.isLoggable(Level.FINE) && (double)wrapMaxHeight[0] < r.getHeight()) {
                            try {
                                LOG.fine("Updating maxHeight from " + wrapMaxHeight + " to " + r.getHeight() + ", line=" + i + ", text=" + component.getDocument().getText(offset, view.getEndOffset() - offset));
                            }
                            catch (BadLocationException ble) {
                                LOG.log(Level.FINE, null, ble);
                            }
                        }
                        wrapMaxHeight[0] = Math.max(wrapMaxHeight[0], (int)r.getHeight());
                    }
                }
            });
        }
        if (wrapMaxHeight[0] > 0) {
            maxHeight = wrapMaxHeight[0];
        }
        if (maxAscent > 0) {
            this.lineAscent = (int)((float)maxAscent * this.lineHeightCorrection);
        }
        if (maxHeight > 0) {
            int oldLineHeight = this.lineHeight;
            this.lineHeight = (int)((float)maxHeight * this.lineHeightCorrection);
            if (oldLineHeight != this.lineHeight && oldLineHeight != -1) {
                this.firePropertyChange("line-height-changed-prop", new Integer(oldLineHeight), new Integer(this.lineHeight));
            }
        }
    }

    void updateComponentProperties() {
        String mimeType = DocumentUtilities.getMimeType((JTextComponent)this.component);
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)mimeType).lookup(FontColorSettings.class);
        this.renderingHints = (Map)fcs.getFontColors("default").getAttribute(EditorStyleConstants.RenderingHints);
        String value = this.prefs.get("margin", null);
        Insets margin = value != null ? SettingsConversions.parseInsets(value) : null;
        this.component.setMargin(margin != null ? margin : NULL_INSETS);
        this.lineNumberDigitWidth = this.computeLineNumberDigitWidth();
        this.updateLineHeight(this.getComponent());
        FontMetricsCache.Info fmcInfo = FontMetricsCache.getInfo(this.getDefaultColoring().getFont());
        this.defaultSpaceWidth = fmcInfo.getSpaceWidth(this.component);
        this.updateLineNumberWidth(0);
        if (this.isGlyphGutterVisible()) {
            this.glyphGutter.update();
            this.updateScrollPaneCornerColor();
        }
    }

    protected void update(Graphics g) {
        if (this.renderingHints != null) {
            ((Graphics2D)g).addRenderingHints(this.renderingHints);
        }
    }

    public final JTextComponent getComponent() {
        return this.component;
    }

    public final BaseDocument getDocument() {
        return this.component != null ? Utilities.getDocument(this.component) : this.printDoc;
    }

    private Class getKitClass() {
        return this.component != null ? Utilities.getKitClass(this.component) : (this.printDoc != null ? this.printDoc.getKitClass() : null);
    }

    public Object getProperty(Object key) {
        return this.props.get(key);
    }

    public void putProperty(Object key, Object value) {
        Object oldValue = value != null ? this.props.put(key, value) : this.props.remove(key);
        this.firePropertyChange(key.toString(), oldValue, value);
    }

    public JComponent getExtComponent() {
        if (this.extComponent == null && this.component != null) {
            this.extComponent = this.createExtComponent();
            this.checkUndoManager(this.getDocument());
        }
        return this.extComponent;
    }

    public JToolBar getToolBarComponent() {
        if (this.toolBarComponent == null && this.component != null) {
            this.toolBarComponent = this.createToolBarComponent();
        }
        return this.toolBarComponent;
    }

    protected JToolBar createToolBarComponent() {
        return null;
    }

    protected void initGlyphCorner(JScrollPane scroller) {
        this.glyphCorner = new JPanel();
        this.updateScrollPaneCornerColor();
        scroller.setCorner("LOWER_LEFT_CORNER", this.glyphCorner);
    }

    protected void setGlyphGutter(GlyphGutter gutter) {
        this.glyphGutter = gutter;
    }

    public final int getSideBarWidth() {
        Rectangle bounds;
        JScrollPane scroll = (JScrollPane)SwingUtilities.getAncestorOfClass(JScrollPane.class, this.getParentViewport());
        if (scroll != null && scroll.getRowHeader() != null && (bounds = scroll.getRowHeader().getBounds()) != null) {
            return bounds.width;
        }
        return 40;
    }

    protected JComponent createExtComponent() {
        this.setLineNumberEnabled(true);
        JPanel ec = new JPanel(new BorderLayout());
        ec.putClientProperty(JTextComponent.class, this.component);
        JScrollPane scroller = new JScrollPane(this.component);
        scroller.getViewport().setMinimumSize(new Dimension(4, 4));
        scroller.setBorder(null);
        this.setGlyphGutter(new GlyphGutter(this));
        scroller.setRowHeaderView(this.glyphGutter);
        this.initGlyphCorner(scroller);
        ec.add(scroller);
        ec.add((Component)this.getStatusBar().getPanel(), "South");
        return ec;
    }

    public boolean hasExtComponent() {
        return this.extComponent != null;
    }

    public Abbrev getAbbrev() {
        if (this.abbrev == null) {
            this.abbrev = new Abbrev(this, true, true);
        }
        return this.abbrev;
    }

    public WordMatch getWordMatch() {
        if (this.wordMatch == null) {
            this.wordMatch = new WordMatch(this);
        }
        return this.wordMatch;
    }

    public StatusBar getStatusBar() {
        if (this.statusBar == null) {
            this.statusBar = new StatusBar(this);
        }
        return this.statusBar;
    }

    public void repaint(int startY) {
        this.repaint(startY, this.component.getHeight());
    }

    public void repaint(int startY, int height) {
        if (height <= 0) {
            return;
        }
        int width = Math.max(this.component.getWidth(), 0);
        startY = Math.max(startY, 0);
        this.component.repaint(0, startY, width, height);
    }

    public void repaintOffset(int pos) throws BadLocationException {
        this.repaintBlock(pos, pos);
    }

    public void repaintBlock(int startPos, int endPos) throws BadLocationException {
        int yFrom;
        int yTo;
        BaseTextUI ui = (BaseTextUI)this.component.getUI();
        if (startPos > endPos) {
            int tmpPos = startPos;
            startPos = endPos;
            endPos = tmpPos;
        }
        try {
            yFrom = ui.getYFromPos(startPos);
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
            yFrom = 0;
        }
        try {
            yTo = ui.getYFromPos(endPos) + this.getLineHeight();
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
            yTo = (int)ui.getRootView(this.component).getPreferredSpan(1);
        }
        this.repaint(yFrom, yTo - yFrom);
    }

    private JViewport getParentViewport() {
        Container pc = this.component.getParent();
        return pc instanceof JViewport ? (JViewport)pc : null;
    }

    public static Frame getParentFrame(Component c) {
        do {
            if (!((c = c.getParent()) instanceof Frame)) continue;
            return (Frame)c;
        } while (c != null);
        return null;
    }

    public boolean updateVirtualWidth(int width) {
        return false;
    }

    public boolean updateVirtualHeight(int height) {
        return false;
    }

    public boolean isLineNumberEnabled() {
        return this.lineNumberEnabled;
    }

    public void setLineNumberEnabled(boolean lineNumberEnabled) {
        this.lineNumberEnabled = lineNumberEnabled;
        boolean bl = this.lineNumberVisible = lineNumberEnabled && this.lineNumberVisibleSetting;
        if (this.disableLineNumbers) {
            this.lineNumberVisible = false;
        }
    }

    void setLineNumberVisibleSetting(boolean lineNumberVisibleSetting) {
        this.lineNumberVisibleSetting = lineNumberVisibleSetting;
    }

    public void updateLineNumberWidth(int maxDigitCount) {
        int oldWidth = this.lineNumberWidth;
        if (this.lineNumberVisible) {
            try {
                if (maxDigitCount <= 0) {
                    BaseDocument doc = this.getDocument();
                    int lineCnt = Utilities.getLineOffset(doc, doc.getLength()) + 1;
                    maxDigitCount = Integer.toString(lineCnt).length();
                }
                if (maxDigitCount > this.lineNumberMaxDigitCount) {
                    this.lineNumberMaxDigitCount = maxDigitCount;
                }
            }
            catch (BadLocationException e) {
                this.lineNumberMaxDigitCount = 1;
            }
            this.lineNumberWidth = this.lineNumberMaxDigitCount * this.lineNumberDigitWidth;
            Insets lineMargin = this.getLineNumberMargin();
            if (lineMargin != null) {
                this.lineNumberWidth += lineMargin.left + lineMargin.right;
            }
        } else {
            this.lineNumberWidth = 0;
        }
        this.updateTextMargin();
        if (oldWidth != this.lineNumberWidth && this.component != null) {
            this.component.repaint();
        }
    }

    public void updateTextMargin() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    EditorUI.this.updateTextMargin();
                }
            });
        }
        Insets orig = this.textMargin;
        Insets cm = this.component != null ? this.component.getMargin() : null;
        int leftWidth = this.lineNumberWidth + this.textLeftMarginWidth;
        this.textMargin = cm != null ? new Insets(cm.top, cm.left + leftWidth, cm.bottom, cm.right) : new Insets(0, leftWidth, 0, 0);
        if (orig.top != this.textMargin.top || orig.bottom != this.textMargin.bottom) {
            ((BaseTextUI)this.component.getUI()).invalidateStartY();
        }
    }

    public Rectangle getExtentBounds() {
        return this.getExtentBounds(null);
    }

    public Rectangle getExtentBounds(Rectangle r) {
        if (r == null) {
            r = new Rectangle();
        }
        if (this.component != null) {
            JViewport port = this.getParentViewport();
            if (port != null) {
                Point p = port.getViewPosition();
                r.width = port.getWidth();
                r.height = port.getHeight();
                r.x = p.x;
                r.y = p.y;
            } else {
                r.setBounds(this.component.getVisibleRect());
            }
        }
        return r;
    }

    public Insets getTextMargin() {
        return this.textMargin;
    }

    public void scrollRectToVisible(final Rectangle r, final int scrollPolicy) {
        Utilities.runInEventDispatchThread(new Runnable(){
            boolean docLocked;

            @Override
            public void run() {
                if (!this.docLocked) {
                    this.docLocked = true;
                    BaseDocument doc = EditorUI.this.getDocument();
                    if (doc != null) {
                        doc.render(this);
                    }
                } else {
                    EditorUI.this.scrollRectToVisibleFragile(r, scrollPolicy);
                }
            }
        });
    }

    boolean scrollRectToVisibleFragile(Rectangle r, int scrollPolicy) {
        Insets margin = this.getTextMargin();
        Rectangle bounds = this.getExtentBounds();
        r = new Rectangle(r);
        r.x -= margin.left;
        r.y -= margin.top;
        bounds.width -= margin.left + margin.right;
        bounds.height -= margin.top + margin.bottom;
        return this.scrollRectToVisibleImpl(r, scrollPolicy, bounds);
    }

    private boolean scrollRectToVisibleImpl(Rectangle r, int scrollPolicy, Rectangle bounds) {
        if (bounds.width <= 0 || bounds.height <= 0) {
            return false;
        }
        if (scrollPolicy == 3) {
            int cnvFI = this.scrollFindInsets.left < 0 ? (- bounds.width) * this.scrollFindInsets.left / 100 : this.scrollFindInsets.left * this.defaultSpaceWidth;
            int nx = Math.max(r.x - cnvFI, 0);
            cnvFI = this.scrollFindInsets.right < 0 ? (- bounds.width) * this.scrollFindInsets.right / 100 : this.scrollFindInsets.right * this.defaultSpaceWidth;
            r.width += r.x - nx + cnvFI;
            r.x = nx;
            cnvFI = this.scrollFindInsets.top < 0 ? (- bounds.height) * this.scrollFindInsets.top / 100 : this.scrollFindInsets.top * this.getLineHeight();
            int ny = Math.max(r.y - cnvFI, 0);
            cnvFI = this.scrollFindInsets.bottom < 0 ? (- bounds.height) * this.scrollFindInsets.bottom / 100 : this.scrollFindInsets.bottom * this.getLineHeight();
            r.height += r.y - ny + cnvFI;
            r.y = ny;
            return this.scrollRectToVisibleImpl(r, 2, bounds);
        }
        int viewWidth = (int)this.component.getUI().getRootView(this.component).getPreferredSpan(0);
        int viewHeight = (int)this.component.getUI().getRootView(this.component).getPreferredSpan(1);
        if (r.x + r.width > viewWidth) {
            r.x = viewWidth - r.width;
            if (r.x < 0) {
                r.x = 0;
                r.width = viewWidth;
            }
            return this.scrollRectToVisibleImpl(r, scrollPolicy, bounds);
        }
        if (r.y + r.height > viewHeight) {
            r.y = viewHeight - r.height;
            if (r.y < 0) {
                r.y = 0;
                r.height = viewHeight;
            }
            return this.scrollRectToVisibleImpl(r, scrollPolicy, bounds);
        }
        if (r.width > bounds.width || r.height > bounds.height) {
            try {
                Rectangle caretRect = this.component.getUI().modelToView(this.component, this.component.getCaret().getDot(), Position.Bias.Forward);
                if (caretRect.x >= r.x && caretRect.x + caretRect.width <= r.x + r.width && caretRect.y >= r.y && caretRect.y + caretRect.height <= r.y + r.height) {
                    int overX = r.width - bounds.width;
                    int overY = r.height - bounds.height;
                    if (overX > 0) {
                        r.x -= overX * (caretRect.x - r.x) / r.width;
                    }
                    if (overY > 0) {
                        r.y -= overY * (caretRect.y - r.y) / r.height;
                    }
                }
                r.height = bounds.height;
                r.width = bounds.width;
                return this.scrollRectToVisibleImpl(r, scrollPolicy, bounds);
            }
            catch (BadLocationException ble) {
                LOG.log(Level.WARNING, null, ble);
            }
        }
        int newX = bounds.x;
        int newY = bounds.y;
        boolean move = false;
        if (r.x < bounds.x) {
            move = true;
            switch (scrollPolicy) {
                case 1: {
                    newX = this.scrollJumpInsets.left < 0 ? bounds.width * (- this.scrollJumpInsets.left) / 100 : this.scrollJumpInsets.left * this.defaultSpaceWidth;
                    newX = Math.min(newX, bounds.x + bounds.width - (r.x + r.width));
                    newX = Math.max(r.x - newX, 0);
                    break;
                }
                default: {
                    newX = r.x;
                }
            }
            this.updateVirtualWidth(newX + bounds.width);
        } else if (r.x + r.width > bounds.x + bounds.width) {
            move = true;
            switch (scrollPolicy) {
                case 2: {
                    newX = r.x + r.width - bounds.width;
                    break;
                }
                default: {
                    newX = this.scrollJumpInsets.right < 0 ? bounds.width * (- this.scrollJumpInsets.right) / 100 : this.scrollJumpInsets.right * this.defaultSpaceWidth;
                    newX = Math.min(newX, bounds.width - r.width);
                    newX = r.x + r.width + newX - bounds.width;
                }
            }
            this.updateVirtualWidth(newX + bounds.width);
        }
        if (r.y < bounds.y) {
            move = true;
            switch (scrollPolicy) {
                case 1: {
                    newY = r.y;
                    newY -= this.scrollJumpInsets.top < 0 ? bounds.height * (- this.scrollJumpInsets.top) / 100 : this.scrollJumpInsets.top * this.getLineHeight();
                    break;
                }
                case 2: {
                    newY = r.y;
                    break;
                }
                default: {
                    newY = r.y - (bounds.height - r.height) / 2;
                }
            }
            newY = Math.max(newY, 0);
        } else if (r.y + r.height > bounds.y + bounds.height) {
            move = true;
            switch (scrollPolicy) {
                case 1: {
                    newY = r.y + r.height - bounds.height;
                    newY += this.scrollJumpInsets.bottom < 0 ? bounds.height * (- this.scrollJumpInsets.bottom) / 100 : this.scrollJumpInsets.bottom * this.getLineHeight();
                    break;
                }
                case 2: {
                    newY = r.y + r.height - bounds.height;
                    break;
                }
                default: {
                    newY = r.y - (bounds.height - r.height) / 2;
                }
            }
            newY = Math.max(newY, 0);
        }
        if (move) {
            this.setExtentPosition(newX, newY);
        }
        return move;
    }

    void setExtentPosition(int x, int y) {
        JViewport port = this.getParentViewport();
        if (port != null) {
            Point p = new Point(Math.max(x, 0), Math.max(y, 0));
            port.setViewPosition(p);
        }
    }

    public void adjustWindow(int caretPercentFromWindowTop) {
        final Rectangle bounds = this.getExtentBounds();
        if (this.component != null) {
            try {
                Rectangle caretRect = this.component.modelToView(this.component.getCaretPosition());
                bounds.y = caretRect.y - caretPercentFromWindowTop * bounds.height / 100 + caretPercentFromWindowTop * this.getLineHeight() / 100;
                Utilities.runInEventDispatchThread(new Runnable(){

                    @Override
                    public void run() {
                        EditorUI.this.scrollRectToVisible(bounds, 2);
                    }
                });
            }
            catch (BadLocationException e) {
                LOG.log(Level.WARNING, null, e);
            }
        }
    }

    public void adjustCaret(int percentFromWindowTop) {
        JTextComponent c = this.component;
        if (c != null) {
            Rectangle bounds = this.getExtentBounds();
            bounds.y += percentFromWindowTop * bounds.height / 100 - percentFromWindowTop * this.getLineHeight() / 100;
            try {
                int offset = ((BaseTextUI)c.getUI()).getPosFromY(bounds.y);
                if (offset >= 0) {
                    this.caretSetDot(offset, null, 2);
                }
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
    }

    public void caretSetDot(int offset, Rectangle scrollRect, int scrollPolicy) {
        if (this.component != null) {
            Caret caret = this.component.getCaret();
            if (caret instanceof BaseCaret) {
                ((BaseCaret)caret).setDot(offset, scrollRect, scrollPolicy);
            } else {
                caret.setDot(offset);
            }
        }
    }

    public void caretMoveDot(int offset, Rectangle scrollRect, int scrollPolicy) {
        if (this.component != null) {
            Caret caret = this.component.getCaret();
            if (caret instanceof BaseCaret) {
                ((BaseCaret)caret).moveDot(offset, scrollRect, scrollPolicy);
            } else {
                caret.moveDot(offset);
            }
        }
    }

    protected void paint(Graphics g) {
        if (this.component != null) {
            this.update(g);
        }
    }

    public Insets getLineNumberMargin() {
        return defaultLineNumberMargin;
    }

    private int computeLineNumberDigitWidth() {
        Coloring dc = this.getDefaultColoring();
        Coloring lnc = this.getCMInternal().get("line-number");
        if (lnc != null) {
            Font lnFont = lnc.getFont();
            if (lnFont == null) {
                lnFont = dc.getFont();
            }
            if (this.component == null) {
                return this.lineNumberDigitWidth;
            }
            FontMetrics lnFM = FontMetricsCache.getFontMetrics(lnFont, this.component);
            if (lnFM == null) {
                return this.lineNumberDigitWidth;
            }
            int maxWidth = 1;
            for (int i = 0; i <= 9; ++i) {
                maxWidth = Math.max(maxWidth, lnFM.charWidth((char)(48 + i)));
            }
            return maxWidth;
        }
        return this.lineNumberDigitWidth;
    }

    public int getLineNumberDigitWidth() {
        return this.lineNumberDigitWidth;
    }

    public boolean isGlyphGutterVisible() {
        return this.glyphGutter != null;
    }

    public final GlyphGutter getGlyphGutter() {
        return this.glyphGutter;
    }

    protected void updateScrollPaneCornerColor() {
        Coloring lineColoring = this.getCMInternal().get("line-number");
        Coloring defaultColoring = this.getDefaultColoring();
        Color backgroundColor = lineColoring != null && lineColoring.getBackColor() != null ? lineColoring.getBackColor() : defaultColoring.getBackColor();
        if (this.glyphCorner != null) {
            this.glyphCorner.setBackground(backgroundColor);
        }
    }

    protected int textLimitWidth() {
        Object textLimitLine;
        int ret = this.textLimitWidth;
        Object object = textLimitLine = this.component == null ? null : this.component.getClientProperty("TextLimitLine");
        if (textLimitLine instanceof Integer) {
            ret = (Integer)textLimitLine;
        }
        return ret;
    }

    Color getTextLimitLineColor() {
        Coloring c = this.getCMInternal().get("text-limit-line-color");
        if (c != null && c.getForeColor() != null) {
            return c.getForeColor();
        }
        return new Color(255, 235, 235);
    }

    private void showPopupMenuForPopupTrigger(final MouseEvent evt) {
        if (this.component != null && evt.isPopupTrigger() && this.popupMenuEnabled) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Caret c;
                    if (EditorUI.this.component != null && (c = EditorUI.this.component.getCaret()) instanceof BaseCaret && !Utilities.isSelectionShowing(c)) {
                        int offset = ((BaseCaret)c).mouse2Offset(evt);
                        EditorUI.this.component.getCaret().setDot(offset);
                    }
                    EditorUI.this.showPopupMenu(evt.getX(), evt.getY());
                }
            });
        }
    }

    @Override
    public void mouseClicked(MouseEvent evt) {
    }

    @Override
    public void mousePressed(MouseEvent evt) {
        this.getWordMatch().clear();
        this.showPopupMenuForPopupTrigger(evt);
    }

    @Override
    public void mouseReleased(MouseEvent evt) {
        this.showPopupMenuForPopupTrigger(evt);
    }

    @Override
    public void mouseEntered(MouseEvent evt) {
    }

    @Override
    public void mouseExited(MouseEvent evt) {
    }

    public ToolTipSupport getToolTipSupport() {
        if (this.toolTipSupport == null) {
            this.toolTipSupport = EditorExtPackageAccessor.get().createToolTipSupport(this);
        }
        return this.toolTipSupport;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public PopupManager getPopupManager() {
        if (this.popupManager == null) {
            Object object = this.getComponentLock();
            synchronized (object) {
                JTextComponent c = this.getComponent();
                if (c != null) {
                    this.popupManager = new PopupManager(c);
                }
            }
        }
        return this.popupManager;
    }

    public void showPopupMenu(int x, int y) {
        JTextComponent c = this.getComponent();
        if (c != null) {
            JPopupMenu pm;
            Action a;
            BaseKit kit = Utilities.getKit(c);
            if (kit != null && (a = kit.getActionByName("build-popup-menu")) != null) {
                a.actionPerformed(new ActionEvent(c, 0, ""));
            }
            if ((pm = this.getPopupMenu()) != null && c.isShowing()) {
                if (!c.isFocusOwner()) {
                    c.requestFocus();
                }
                pm.show(c, x, y);
            }
        }
    }

    public void hidePopupMenu() {
        JPopupMenu pm = this.getPopupMenu();
        if (pm != null) {
            pm.setVisible(false);
        }
    }

    public JPopupMenu getPopupMenu() {
        return this.popupMenu;
    }

    public void setPopupMenu(JPopupMenu popupMenu) {
        this.popupMenu = popupMenu;
    }

    static {
        EditorUiAccessor.register(new Accessor());
    }

    private static final class Accessor
    extends EditorUiAccessor {
        private Accessor() {
        }

        @Override
        public boolean isLineNumberVisible(EditorUI eui) {
            return eui.lineNumberVisible;
        }

        @Override
        public Coloring getColoring(EditorUI eui, String coloringName) {
            return eui.getColoring(coloringName);
        }

        @Override
        public int getLineNumberMaxDigitCount(EditorUI eui) {
            return eui.lineNumberMaxDigitCount;
        }

        @Override
        public int getLineNumberWidth(EditorUI eui) {
            return eui.lineNumberWidth;
        }

        @Override
        public int getLineNumberDigitWidth(EditorUI eui) {
            return eui.lineNumberDigitWidth;
        }

        @Override
        public Insets getLineNumberMargin(EditorUI eui) {
            return eui.getLineNumberMargin();
        }

        @Override
        public int getLineHeight(EditorUI eui) {
            return eui.getLineHeight();
        }

        @Override
        public Coloring getDefaultColoring(EditorUI eui) {
            return eui.getDefaultColoring();
        }

        @Override
        public int getDefaultSpaceWidth(EditorUI eui) {
            return eui.defaultSpaceWidth;
        }

        @Override
        public Map<?, ?> getRenderingHints(EditorUI eui) {
            return eui.renderingHints;
        }

        @Override
        public Rectangle getExtentBounds(EditorUI eui) {
            return eui.getExtentBounds();
        }

        @Override
        public Insets getTextMargin(EditorUI eui) {
            return eui.getTextMargin();
        }

        @Override
        public int getTextLeftMarginWidth(EditorUI eui) {
            return eui.textLeftMarginWidth;
        }

        @Override
        public boolean getTextLimitLineVisible(EditorUI eui) {
            return eui.textLimitLineVisible;
        }

        @Override
        public Color getTextLimitLineColor(EditorUI eui) {
            return eui.getTextLimitLineColor();
        }

        @Override
        public int getTextLimitWidth(EditorUI eui) {
            return eui.textLimitWidth;
        }

        @Override
        public int getLineAscent(EditorUI eui) {
            return eui.getLineAscent();
        }

        @Override
        public void paint(EditorUI eui, Graphics g) {
            eui.paint(g);
        }

        @Override
        public DrawLayerList getDrawLayerList(EditorUI eui) {
            return eui.drawLayerList;
        }
    }

    private class Listener
    implements PreferenceChangeListener {
        private Listener() {
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            BaseDocument doc;
            if (EditorUI.this.prefs == null) {
                EditorUI.this.disableLineNumbers = false;
                return;
            }
            String settingName = evt == null ? null : evt.getKey();
            EditorUI.this.settingsChangeImpl(settingName);
            if ("tab-size".equals(settingName)) {
                EditorUI.this.firePropertyChange("tab-size-changed-prop", null, null);
            }
            if (settingName == null || "line-number-visible".equals(settingName)) {
                EditorUI.this.lineNumberVisibleSetting = EditorUI.this.prefs.getBoolean("line-number-visible", true);
                boolean bl = EditorUI.this.lineNumberVisible = EditorUI.this.lineNumberEnabled && EditorUI.this.lineNumberVisibleSetting;
                if (EditorUI.this.component == null) {
                    EditorUI.this.disableLineNumbers = false;
                }
                if (EditorUI.this.disableLineNumbers) {
                    EditorUI.this.lineNumberVisible = false;
                }
            }
            if (settingName == null || "popup-menu-enabled".equals(settingName)) {
                EditorUI.this.popupMenuEnabled = EditorUI.this.prefs.getBoolean("popup-menu-enabled", true);
            }
            if ((doc = EditorUI.this.getDocument()) != null) {
                float newLineHeightCorrection;
                if (settingName == null || "text-left-margin-width".equals(settingName)) {
                    EditorUI.this.textLeftMarginWidth = 0;
                }
                if ((settingName == null || "line-height-correction".equals(settingName)) && (newLineHeightCorrection = EditorUI.this.prefs.getFloat("line-height-correction", 1.0f)) != EditorUI.this.lineHeightCorrection) {
                    EditorUI.this.lineHeightCorrection = newLineHeightCorrection;
                    EditorUI.this.updateLineHeight(EditorUI.this.getComponent());
                }
                if (settingName == null || "text-limit-line-visible".equals(settingName)) {
                    EditorUI.this.textLimitLineVisible = EditorUI.this.prefs.getBoolean("text-limit-line-visible", true);
                }
                if (settingName == null || "text-limit-width".equals(settingName)) {
                    EditorUI.this.textLimitWidth = EditorUI.this.prefs.getInt("text-limit-width", 80);
                }
                if (EditorUI.this.component != null) {
                    Insets insets;
                    if (settingName == null || "scroll-jump-insets".equals(settingName)) {
                        String value = EditorUI.this.prefs.get("scroll-jump-insets", null);
                        insets = value != null ? SettingsConversions.parseInsets(value) : null;
                        Insets insets2 = EditorUI.this.scrollJumpInsets = insets != null ? insets : EditorPreferencesDefaults.defaultScrollJumpInsets;
                    }
                    if (settingName == null || "scroll-find-insets".equals(settingName)) {
                        String value = EditorUI.this.prefs.get("scroll-find-insets", null);
                        insets = value != null ? SettingsConversions.parseInsets(value) : null;
                        EditorUI.this.scrollFindInsets = insets != null ? insets : EditorPreferencesDefaults.defaultScrollFindInsets;
                    }
                    Utilities.runInEventDispatchThread(new Runnable(){

                        @Override
                        public void run() {
                            JTextComponent c = EditorUI.this.component;
                            if (c != null) {
                                EditorUI.this.updateComponentProperties();
                                ((BaseTextUI)c.getUI()).preferenceChanged(true, true);
                            }
                        }
                    });
                }
            }
        }

    }

    static class ComponentLock {
        ComponentLock() {
        }
    }

}

