/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

public class MultiKeyBinding
extends JTextComponent.KeyBinding
implements Externalizable {
    public KeyStroke[] keys;
    static final long serialVersionUID = -8602816556604003688L;

    public MultiKeyBinding() {
        super(null, null);
    }

    public MultiKeyBinding(KeyStroke[] keys, String actionName) {
        super(null, actionName);
        this.keys = keys;
    }

    public MultiKeyBinding(KeyStroke key, String actionName) {
        super(key, actionName);
    }

    public MultiKeyBinding(JTextComponent.KeyBinding kb) {
        this(kb.key, kb.actionName);
    }

    public boolean equals(Object o) {
        if (o instanceof MultiKeyBinding) {
            MultiKeyBinding kb = (MultiKeyBinding)o;
            if (this.actionName == null ? kb.actionName != null : !this.actionName.equals(kb.actionName)) {
                return false;
            }
            if (this.keys == null) {
                if (kb.keys == null) {
                    return this.key == null && kb.key == null || this.key != null && this.key.equals(kb.key);
                }
                return kb.keys.length == 1 && (this.key == null && kb.keys[0] == null || this.key != null && this.key.equals(kb.keys[0]));
            }
            if (kb.keys != null) {
                return Arrays.equals(this.keys, kb.keys);
            }
            return this.keys.length == 1 && (kb.key == null && this.keys[0] == null || kb.key != null && kb.key.equals(this.keys[0]));
        }
        return false;
    }

    public int hashCode() {
        int hash = this.actionName == null ? 0 : this.actionName.hashCode();
        hash += 7 * (this.key == null ? 0 : this.key.hashCode());
        return hash += 7 * (this.keys == null ? 0 : this.keys.hashCode());
    }

    public static void updateKeyBindings(JTextComponent.KeyBinding[] target, JTextComponent.KeyBinding[] changes) {
        ArrayList<JTextComponent.KeyBinding> tgt = new ArrayList<JTextComponent.KeyBinding>(Arrays.asList(target));
        MultiKeyBinding tmp = new MultiKeyBinding(new KeyStroke[1], null);
        for (int i = 0; i < changes.length; ++i) {
            MultiKeyBinding cur;
            if (changes[i] instanceof MultiKeyBinding) {
                cur = (MultiKeyBinding)changes[i];
                if (cur.keys == null) {
                    tmp.keys[0] = cur.key;
                    tmp.actionName = cur.actionName;
                    cur = tmp;
                }
            } else {
                tmp.keys[0] = changes[i].key;
                tmp.actionName = changes[i].actionName;
                cur = tmp;
            }
            boolean matched = false;
            for (int j = 0; j < tgt.size(); ++j) {
                JTextComponent.KeyBinding kb = tgt.get(j);
                if (kb instanceof MultiKeyBinding) {
                    MultiKeyBinding mkb = (MultiKeyBinding)kb;
                    if (mkb.keys == null) {
                        if (cur.keys.length != 1 || !cur.keys[0].equals(mkb.key)) continue;
                        if (mkb.actionName == null) {
                            tgt.remove(i);
                        } else {
                            tgt.set(i, mkb);
                        }
                        matched = true;
                        break;
                    }
                    if (cur.keys.length != mkb.keys.length) continue;
                    matched = true;
                    for (int k = 0; k < cur.keys.length; ++k) {
                        if (cur.keys[k].equals(mkb.keys[k])) continue;
                        matched = false;
                        break;
                    }
                    if (!matched) continue;
                    if (mkb.actionName == null) {
                        tgt.remove(i);
                        break;
                    }
                    tgt.set(i, mkb);
                    break;
                }
                if (cur.keys.length != 1 || !cur.keys[0].equals(kb.key)) continue;
                if (kb.actionName == null) {
                    tgt.remove(i);
                } else {
                    tgt.set(i, kb);
                }
                matched = true;
                break;
            }
            if (matched) continue;
            tgt.add(changes[tgt.size()]);
        }
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Object obj = in.readObject();
        if (obj instanceof Integer) {
            int len = (Integer)obj;
            if (len >= 0) {
                this.keys = new KeyStroke[len];
                for (int i = 0; i < len; ++i) {
                    this.keys[i] = KeyStroke.getKeyStroke(in.readInt(), in.readInt(), in.readBoolean());
                }
            } else {
                this.keys = null;
            }
            this.key = in.readBoolean() ? KeyStroke.getKeyStroke(in.readInt(), in.readInt(), in.readBoolean()) : null;
            this.actionName = (String)in.readObject();
        } else {
            this.keys = (KeyStroke[])obj;
            this.key = (KeyStroke)in.readObject();
            this.actionName = (String)in.readObject();
        }
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        if (this.keys != null) {
            out.writeObject(new Integer(this.keys.length));
            for (int i = 0; i < this.keys.length; ++i) {
                out.writeInt(this.keys[i].getKeyCode());
                out.writeInt(this.keys[i].getModifiers());
                out.writeBoolean(this.keys[i].isOnKeyRelease());
            }
        } else {
            out.writeObject(new Integer(-1));
        }
        if (this.key != null) {
            out.writeBoolean(true);
            out.writeInt(this.key.getKeyCode());
            out.writeInt(this.key.getModifiers());
            out.writeBoolean(this.key.isOnKeyRelease());
        } else {
            out.writeBoolean(false);
        }
        out.writeObject(this.actionName);
    }

    public String toString() {
        if (this.keys == null) {
            return "key=" + this.key + ", actionName=" + this.actionName;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.keys.length; ++i) {
            sb.append("key");
            sb.append(i);
            sb.append('=');
            sb.append(this.keys[i]);
            sb.append(", ");
        }
        sb.append("actionName=");
        sb.append(this.actionName);
        return sb.toString();
    }
}

