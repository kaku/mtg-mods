/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseKit;

abstract class LocalBaseAction
extends BaseAction {
    public LocalBaseAction() {
    }

    public LocalBaseAction(int updateMask) {
        super(updateMask);
    }

    public LocalBaseAction(String name) {
        super(name);
    }

    public LocalBaseAction(String name, int updateMask) {
        super(name, updateMask);
    }

    @Override
    protected Class getShortDescriptionBundleClass() {
        return BaseKit.class;
    }
}

