/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import org.netbeans.editor.StringMap;

public class StringCache {
    private static final int DEFAULT_MAX_SIZE = 300;
    private static final int DEFAULT_INITIAL_CAPACITY = 701;
    int maxSize;
    int size;
    StringMap strMap;
    private Entry chain;
    private Entry endChain;
    private Entry freeEntry;
    public int statQueries;
    public int statHits;

    public StringCache() {
        this(300, 701);
    }

    public StringCache(int maxSize) {
        this(maxSize, 2 * maxSize);
    }

    public StringCache(int maxSize, int initialMapCapacity) {
        this.maxSize = maxSize;
        this.strMap = new StringMap(initialMapCapacity);
    }

    private void toStart(Entry e) {
        if (e != this.chain) {
            Entry ep = e.prev;
            Entry en = e.next;
            if (en != null) {
                en.prev = ep;
            } else {
                this.endChain = ep;
            }
            ep.next = en;
            if (this.chain != null) {
                e.next = this.chain;
                this.chain.prev = e;
            }
            this.chain = e;
        }
    }

    public String getString(char[] chars, int offset, int len) {
        String ret;
        ++this.statQueries;
        Object o = this.strMap.get(chars, offset, len);
        if (o instanceof Entry) {
            Entry e = (Entry)o;
            this.toStart(e);
            ++this.statHits;
            ret = e.str;
        } else if (o instanceof String) {
            ++this.statHits;
            ret = (String)o;
        } else {
            ret = new String(chars, offset, len);
            this.storeString(ret);
        }
        return ret;
    }

    private void removeString(String s) {
        Object o = this.strMap.remove(s);
        if (o instanceof Entry) {
            Entry e = (Entry)o;
            Entry ep = e.prev;
            Entry en = e.next;
            if (e == this.chain) {
                this.chain = en;
                if (e == this.endChain) {
                    this.endChain = null;
                }
            } else if (en != null) {
                en.prev = ep;
            } else {
                this.endChain = ep;
            }
            this.freeEntry = e;
            --this.size;
        }
    }

    private void storeString(String s) {
        Entry e;
        if (this.size >= this.maxSize) {
            e = this.endChain;
            this.toStart(e);
            this.strMap.remove(e.str);
            e.str = s;
        } else {
            if (this.freeEntry != null) {
                e = this.freeEntry;
                this.freeEntry = null;
                e.str = s;
                e.next = this.chain;
            } else {
                e = new Entry(s, this.chain);
            }
            if (this.chain != null) {
                this.chain.prev = e;
            } else {
                this.endChain = e;
            }
            this.chain = e;
            ++this.size;
        }
        this.strMap.put(s, e);
    }

    public void putSurviveString(String s) {
        this.removeString(s);
        this.strMap.put(s, s);
    }

    public String toString() {
        String ret = "size=" + this.size + ", maxSize=" + this.maxSize + ", statHits=" + this.statHits + ", statQueries=" + this.statQueries;
        if (this.statQueries > 0) {
            ret = ret + ", hit ratio=" + this.statHits * 100 / this.statQueries + "%";
        }
        return ret;
    }

    static class Entry {
        String str;
        Entry next;
        Entry prev;

        Entry(String str, Entry next) {
            this.str = str;
            this.next = next;
        }
    }

}

