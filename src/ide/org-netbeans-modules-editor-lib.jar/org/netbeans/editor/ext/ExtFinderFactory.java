/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import javax.swing.text.BadLocationException;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.Utilities;

public class ExtFinderFactory {

    public static abstract class LineBlocksFinder
    extends FinderFactory.AbstractBlocksFinder {
        private int origStartPos;
        private int origLimitPos;

        public int adjustStartPos(BaseDocument doc, int startPos) {
            this.origStartPos = startPos;
            try {
                return Utilities.getRowStart(doc, startPos);
            }
            catch (BadLocationException e) {
                return startPos;
            }
        }

        public int adjustLimitPos(BaseDocument doc, int limitPos) {
            this.origLimitPos = limitPos;
            try {
                return Utilities.getRowEnd(doc, limitPos);
            }
            catch (BadLocationException e) {
                return limitPos;
            }
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset = reqPos - bufferStartPos;
            do {
                boolean lfFound;
                int lfOffset;
                int lineOffset;
                boolean bl = lfFound = (lfOffset = Analyzer.findFirstLFOffset(buffer, offset, offset2 - offset)) >= 0;
                if (!lfFound) {
                    lfOffset = offset2;
                }
                if ((lineOffset = this.lineFound(buffer, offset, lfOffset, Math.max(this.origStartPos - bufferStartPos, offset), Math.min(this.origLimitPos - bufferStartPos, lfOffset))) >= 0) {
                    this.found = true;
                    return bufferStartPos + offset + lineOffset;
                }
                if (!lfFound) break;
                offset = lfOffset + 1;
            } while (true);
            return bufferStartPos + offset2;
        }

        protected abstract int lineFound(char[] var1, int var2, int var3, int var4, int var5);
    }

    public static abstract class LineBwdFinder
    extends FinderFactory.AbstractFinder {
        private int origStartPos;
        private int origLimitPos;

        public int adjustStartPos(BaseDocument doc, int startPos) {
            this.origStartPos = startPos;
            try {
                return Utilities.getRowEnd(doc, startPos);
            }
            catch (BadLocationException e) {
                return startPos;
            }
        }

        public int adjustLimitPos(BaseDocument doc, int limitPos) {
            this.origLimitPos = limitPos;
            try {
                return Utilities.getRowStart(doc, limitPos);
            }
            catch (BadLocationException e) {
                return limitPos;
            }
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset = reqPos - bufferStartPos + 1;
            do {
                int lineOffset;
                boolean lfFound = false;
                int lfOffsetP1 = offset;
                while (lfOffsetP1 > offset1) {
                    if (buffer[--lfOffsetP1] != '\n') continue;
                    lfFound = true;
                    ++lfOffsetP1;
                    break;
                }
                if (!lfFound) {
                    lfOffsetP1 = offset1;
                }
                if ((lineOffset = this.lineFound(buffer, lfOffsetP1, offset, Math.max(this.origLimitPos - bufferStartPos, lfOffsetP1), Math.min(this.origStartPos - bufferStartPos, offset))) >= 0) {
                    this.found = true;
                    return bufferStartPos + offset + lineOffset;
                }
                if (!lfFound) break;
                offset = lfOffsetP1 - 1;
            } while (true);
            return bufferStartPos + offset1 - 1;
        }

        protected abstract int lineFound(char[] var1, int var2, int var3, int var4, int var5);
    }

    public static abstract class LineFwdFinder
    extends FinderFactory.AbstractFinder {
        private int origStartPos;
        private int origLimitPos;

        public int adjustStartPos(BaseDocument doc, int startPos) {
            this.origStartPos = startPos;
            try {
                return Utilities.getRowStart(doc, startPos);
            }
            catch (BadLocationException e) {
                return startPos;
            }
        }

        public int adjustLimitPos(BaseDocument doc, int limitPos) {
            this.origLimitPos = limitPos;
            try {
                return Utilities.getRowEnd(doc, limitPos);
            }
            catch (BadLocationException e) {
                return limitPos;
            }
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset = reqPos - bufferStartPos;
            do {
                boolean lfFound;
                int lfOffset;
                int lineOffset;
                boolean bl = lfFound = (lfOffset = Analyzer.findFirstLFOffset(buffer, offset, offset2 - offset)) >= 0;
                if (!lfFound) {
                    lfOffset = offset2;
                }
                if ((lineOffset = this.lineFound(buffer, offset, lfOffset, Math.max(this.origStartPos - bufferStartPos, offset), Math.min(this.origLimitPos - bufferStartPos, lfOffset))) >= 0) {
                    this.found = true;
                    return bufferStartPos + offset + lineOffset;
                }
                if (!lfFound) break;
                offset = lfOffset + 1;
            } while (true);
            return bufferStartPos + offset2;
        }

        protected abstract int lineFound(char[] var1, int var2, int var3, int var4, int var5);
    }

}

