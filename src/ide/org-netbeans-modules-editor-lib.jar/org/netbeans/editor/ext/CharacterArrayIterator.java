/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.text.CharacterIterator;

public class CharacterArrayIterator
implements CharacterIterator {
    char[] chars;
    int beginIndex;
    int endIndex;
    int index;

    public CharacterArrayIterator(char[] chars, int beginIndex, int endIndex) {
        this.chars = chars;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
        this.index = beginIndex;
    }

    private char currentChar() {
        return this.index >= this.beginIndex && this.index < this.endIndex ? this.chars[this.index] : '\uffff';
    }

    @Override
    public char first() {
        this.index = this.beginIndex;
        return this.currentChar();
    }

    @Override
    public char last() {
        this.index = this.endIndex - 1;
        return this.currentChar();
    }

    @Override
    public char current() {
        return this.currentChar();
    }

    @Override
    public char next() {
        this.index = Math.min(this.index + 1, this.endIndex);
        return this.currentChar();
    }

    @Override
    public char previous() {
        if (this.index <= this.beginIndex) {
            return '\uffff';
        }
        return this.chars[--this.index];
    }

    @Override
    public char setIndex(int position) {
        if (position < this.beginIndex || position >= this.endIndex) {
            throw new IllegalArgumentException();
        }
        this.index = position;
        return this.currentChar();
    }

    @Override
    public int getBeginIndex() {
        return this.beginIndex;
    }

    @Override
    public int getEndIndex() {
        return this.endIndex;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public Object clone() {
        return new CharacterArrayIterator(this.chars, this.beginIndex, this.endIndex);
    }
}

