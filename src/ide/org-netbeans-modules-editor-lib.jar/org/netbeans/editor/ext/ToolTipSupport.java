/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.PatchedPublic
 */
package org.netbeans.editor.ext;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.TextAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.GlyphGutter;
import org.netbeans.editor.PopupManager;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WeakTimerListener;
import org.netbeans.modules.editor.lib.EditorExtPackageAccessor;
import org.openide.modules.PatchedPublic;

public class ToolTipSupport {
    private static final Logger LOG = Logger.getLogger(ToolTipSupport.class.getName());
    public static final String PROP_TOOL_TIP = "toolTip";
    public static final String PROP_TOOL_TIP_TEXT = "toolTipText";
    public static final String PROP_STATUS = "status";
    public static final String PROP_ENABLED = "enabled";
    public static final String PROP_INITIAL_DELAY = "initialDelay";
    public static final String PROP_DISMISS_DELAY = "dismissDelay";
    private static final String UI_PREFIX = "ToolTip";
    public static final int INITIAL_DELAY = 200;
    public static final int DISMISS_DELAY = 60000;
    public static final int STATUS_HIDDEN = 0;
    public static final int STATUS_VISIBILITY_ENABLED = 1;
    public static final int STATUS_TEXT_VISIBLE = 2;
    public static final int STATUS_COMPONENT_VISIBLE = 3;
    private static final int MOUSE_EXTRA_HEIGHT = 5;
    private static final String HTML_PREFIX_LOWERCASE = "<html";
    private static final String HTML_PREFIX_UPPERCASE = "<HTML";
    private static final String LAST_TOOLTIP_POSITION = "ToolTipSupport.lastToolTipPosition";
    private static final String MOUSE_MOVE_IGNORED_AREA = "ToolTipSupport.mouseMoveIgnoredArea";
    private static final String MOUSE_LISTENER = "ToolTipSupport.noOpMouseListener";
    private static final Action NO_ACTION;
    private final Action HIDE_ACTION;
    private static final MouseListener NO_OP_MOUSE_LISTENER;
    public static final int FLAG_HIDE_ON_MOUSE_MOVE = 1;
    public static final int FLAG_HIDE_ON_TIMER = 2;
    public static final int FLAG_PERMANENT = 4;
    public static final int FLAGS_LIGHTWEIGHT_TOOLTIP = 3;
    public static final int FLAGS_HEAVYWEIGHT_TOOLTIP = 4;
    private static final String ELIPSIS = "...";
    private final EditorUI extEditorUI;
    private final Timer enterTimer;
    private final Timer exitTimer;
    private final PropertyChangeSupport pcs;
    private final Listener listener;
    private boolean enabled;
    private MouseEvent lastMouseEvent;
    private boolean glyphListenerAdded;
    private int status;
    private JComponent toolTip;
    private String toolTipText;
    private PopupManager.HorizontalBounds horizontalBounds;
    private PopupManager.Placement placement;
    private int verticalAdjustment;
    private int horizontalAdjustment;
    private int flags;
    private boolean tooltipFromView;

    @PatchedPublic
    ToolTipSupport(EditorUI extEditorUI) {
        this.HIDE_ACTION = new TextAction("tooltip-hide-action"){

            @Override
            public void actionPerformed(ActionEvent e) {
                ToolTipSupport.this.setToolTipVisible(false);
                JTextComponent jtc = ToolTipSupport.this.extEditorUI.getComponent();
                if (jtc != null) {
                    Utilities.requestFocus(jtc);
                }
            }
        };
        this.pcs = new PropertyChangeSupport(this);
        this.listener = new Listener();
        this.glyphListenerAdded = false;
        this.horizontalBounds = PopupManager.ViewPortBounds;
        this.placement = PopupManager.AbovePreferred;
        this.tooltipFromView = false;
        this.extEditorUI = extEditorUI;
        this.enterTimer = new Timer(200, new WeakTimerListener(this.listener));
        this.enterTimer.setRepeats(false);
        this.exitTimer = new Timer(60000, new WeakTimerListener(this.listener));
        this.exitTimer.setRepeats(false);
        extEditorUI.addPropertyChangeListener(this.listener);
        this.setEnabled(true);
    }

    public final JComponent getToolTip() {
        if (this.toolTip == null) {
            this.setToolTip(this.createDefaultToolTip());
        }
        return this.toolTip;
    }

    public void setToolTip(JComponent toolTip) {
        this.setToolTip(toolTip, PopupManager.ViewPortBounds, PopupManager.AbovePreferred);
    }

    public void setToolTip(JComponent toolTip, PopupManager.HorizontalBounds horizontalBounds, PopupManager.Placement placement) {
        this.setToolTip(toolTip, PopupManager.ViewPortBounds, PopupManager.AbovePreferred, 0, 0);
    }

    public void setToolTip(JComponent toolTip, PopupManager.HorizontalBounds horizontalBounds, PopupManager.Placement placement, int horizontalAdjustment, int verticalAdjustment) {
        this.setToolTip(toolTip, horizontalBounds, placement, horizontalAdjustment, verticalAdjustment, 3);
    }

    public void setToolTip(JComponent toolTip, PopupManager.HorizontalBounds horizontalBounds, Point placeAt, int horizontalAdjustment, int verticalAdjustment, int flags) {
        this.setToolTip(toolTip, horizontalBounds, PopupManager.FixedPoint, placeAt, horizontalAdjustment, verticalAdjustment, flags);
    }

    public void setToolTip(JComponent toolTip, PopupManager.HorizontalBounds horizontalBounds, PopupManager.Placement placement, int horizontalAdjustment, int verticalAdjustment, int flags) {
        this.setToolTip(toolTip, horizontalBounds, placement, null, horizontalAdjustment, verticalAdjustment, flags);
    }

    private void setToolTip(JComponent toolTip, PopupManager.HorizontalBounds horizontalBounds, PopupManager.Placement placement, Point placeAt, int horizontalAdjustment, int verticalAdjustment, int flags) {
        JComponent oldToolTip = this.toolTip;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "setTooltip: {0}, horizontalBounds={1}, placement={2}, horizontalAdjustment={3}, verticalAdjustment={4}, flags={5}", new Object[]{toolTip, horizontalBounds, placement, horizontalAdjustment, verticalAdjustment, flags});
        }
        this.toolTip = toolTip;
        this.horizontalBounds = horizontalBounds;
        this.placement = placement;
        this.horizontalAdjustment = horizontalAdjustment;
        this.verticalAdjustment = verticalAdjustment;
        this.flags = flags;
        if (this.toolTip.getClientProperty("ToolTipSupport.noOpMouseListener") == null) {
            this.toolTip.putClientProperty("ToolTipSupport.noOpMouseListener", NO_OP_MOUSE_LISTENER);
            this.toolTip.addMouseListener(NO_OP_MOUSE_LISTENER);
        }
        if (this.status >= 1) {
            Point pt = placeAt != null ? placeAt : (oldToolTip == this.toolTip && this.toolTip.getClientProperty("ToolTipSupport.lastToolTipPosition") != null ? (Point)this.toolTip.getClientProperty("ToolTipSupport.lastToolTipPosition") : this.getLastMouseEventPoint());
            this.ensureVisibility(pt);
        }
        this.firePropertyChange("toolTip", oldToolTip, this.toolTip);
    }

    protected JComponent createDefaultToolTip() {
        return this.createTextToolTip(false);
    }

    private JEditorPane createHtmlTextToolTip() {
        class HtmlTextToolTip
        extends JEditorPane {
            HtmlTextToolTip() {
            }

            @Override
            public void setSize(int width, int height) {
                Dimension prefSize = this.getPreferredSize();
                if (width >= prefSize.width) {
                    width = prefSize.width;
                } else {
                    super.setSize(width, 10000);
                    prefSize = this.getPreferredSize();
                }
                if (height >= prefSize.height) {
                    height = prefSize.height;
                }
                super.setSize(width, height);
            }

            @Override
            public void setKeymap(Keymap map) {
                super.setKeymap(HtmlTextToolTip.addKeymap(null, map));
            }
        }
        HtmlTextToolTip tt = new HtmlTextToolTip();
        ToolTipSupport.filterBindings(tt.getActionMap());
        tt.getActionMap().put(this.HIDE_ACTION.getValue("Name"), this.HIDE_ACTION);
        tt.getInputMap().put(KeyStroke.getKeyStroke(27, 0), this.HIDE_ACTION.getValue("Name"));
        tt.getKeymap().setDefaultAction(NO_ACTION);
        Font font = UIManager.getFont("ToolTip.font");
        Color backColor = UIManager.getColor("ToolTip.background");
        Color foreColor = UIManager.getColor("ToolTip.foreground");
        if (font != null) {
            tt.setFont(font);
        }
        if (foreColor != null) {
            tt.setForeground(foreColor);
        }
        if (backColor != null) {
            tt.setBackground(backColor);
        }
        tt.setOpaque(true);
        tt.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(tt.getForeground()), BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        tt.setContentType("text/html");
        return tt;
    }

    private JTextArea createTextToolTip(boolean wrapLines) {
        class TextToolTip
        extends JTextArea {
            final /* synthetic */ boolean val$wrapLines;

            TextToolTip() {
                this.val$wrapLines = n;
            }

            @Override
            public void setSize(int width, int height) {
                Dimension prefSize = this.getPreferredSize();
                if (width >= prefSize.width) {
                    width = prefSize.width;
                } else {
                    if (this.val$wrapLines) {
                        this.setLineWrap(true);
                        this.setWrapStyleWord(true);
                    }
                    super.setSize(width, 10000);
                    prefSize = this.getPreferredSize();
                }
                if (height >= prefSize.height) {
                    height = prefSize.height;
                } else {
                    super.setSize(width, 10000);
                    int offset = this.viewToModel(new Point(0, height));
                    Document doc = this.getDocument();
                    try {
                        if (offset > "...".length()) {
                            doc.remove(offset, doc.getLength() - (offset -= "...".length()));
                            doc.insertString(offset, "...", null);
                        }
                    }
                    catch (BadLocationException ble) {
                        // empty catch block
                    }
                    height = Math.min(height, this.getPreferredSize().height);
                }
                super.setSize(width, height);
            }

            @Override
            public void setKeymap(Keymap map) {
                super.setKeymap(TextToolTip.addKeymap(null, map));
            }
        }
        TextToolTip tt = new TextToolTip(this, wrapLines);
        ToolTipSupport.filterBindings(tt.getActionMap());
        tt.getActionMap().put(this.HIDE_ACTION.getValue("Name"), this.HIDE_ACTION);
        tt.getInputMap().put(KeyStroke.getKeyStroke(27, 0), this.HIDE_ACTION.getValue("Name"));
        tt.getKeymap().setDefaultAction(NO_ACTION);
        Font font = UIManager.getFont("ToolTip.font");
        Color backColor = UIManager.getColor("ToolTip.background");
        Color foreColor = UIManager.getColor("ToolTip.foreground");
        if (font != null) {
            tt.setFont(font);
        }
        if (foreColor != null) {
            tt.setForeground(foreColor);
        }
        if (backColor != null) {
            tt.setBackground(backColor);
        }
        tt.setOpaque(true);
        tt.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(tt.getForeground()), BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        return tt;
    }

    private void disableSwingToolTip(final JComponent component) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ToolTipManager.sharedInstance().unregisterComponent(component);
                GlyphGutter gg = ToolTipSupport.this.extEditorUI.getGlyphGutter();
                if (gg != null) {
                    ToolTipManager.sharedInstance().unregisterComponent(gg);
                }
            }
        });
    }

    protected void updateToolTip() {
        EditorUI ui = this.extEditorUI;
        if (ui == null) {
            return;
        }
        JTextComponent comp = ui.getComponent();
        if (comp == null) {
            return;
        }
        JComponent oldTooltip = this.toolTip;
        if (this.isGlyphGutterMouseEvent(this.lastMouseEvent)) {
            this.setToolTipText(this.extEditorUI.getGlyphGutter().getToolTipText(this.lastMouseEvent));
        } else {
            Action a;
            BaseKit kit = Utilities.getKit(comp);
            if (kit != null && (a = kit.getActionByName("build-tool-tip")) != null) {
                a.actionPerformed(new ActionEvent(comp, 0, ""));
            }
        }
        if (this.toolTip != oldTooltip) {
            this.tooltipFromView = true;
        }
    }

    public void setToolTipVisible(boolean visible) {
        this.setToolTipVisible(visible, true);
    }

    public final void setToolTipVisible(boolean visible, boolean updateFromView) {
        LOG.log(Level.FINE, "setToolTipVisible: visible={0}, status={1}, enabled={2}", new Object[]{visible, this.status, this.enabled});
        if (!visible) {
            this.enterTimer.stop();
            this.exitTimer.stop();
        }
        if (visible && (this.status < 1 || this.status >= 1 && updateFromView && !this.tooltipFromView) || !visible && this.status >= 1) {
            if (visible) {
                if (this.enabled) {
                    this.setStatus(1);
                    if (updateFromView) {
                        this.updateToolTip();
                    }
                }
            } else {
                if (this.toolTip != null) {
                    if (this.toolTip.isVisible()) {
                        this.toolTip.setVisible(false);
                        this.toolTip.putClientProperty("ToolTipSupport.lastToolTipPosition", null);
                        this.toolTip.putClientProperty("ToolTipSupport.mouseMoveIgnoredArea", null);
                        PopupManager pm = this.extEditorUI.getPopupManager();
                        if (pm != null) {
                            pm.uninstall(this.toolTip);
                        }
                    }
                    this.toolTip = null;
                }
                this.setStatus(0);
                this.tooltipFromView = false;
            }
        }
    }

    public boolean isToolTipVisible() {
        return this.status >= 1 && this.toolTip != null;
    }

    private boolean isToolTipShowing() {
        return this.toolTip != null && this.toolTip.isShowing();
    }

    public final int getStatus() {
        return this.status;
    }

    private void setStatus(int status) {
        if (this.status != status) {
            int oldStatus = this.status;
            this.status = status;
            this.firePropertyChange("status", new Integer(oldStatus), new Integer(this.status));
        }
    }

    public String getToolTipText() {
        return this.toolTipText;
    }

    private static String makeDisplayable(String str, Font f) {
        if (str == null || f == null) {
            return str;
        }
        StringBuilder buf = new StringBuilder(str.length());
        char[] chars = str.toCharArray();
        block7 : for (int i = 0; i < chars.length; ++i) {
            char c = chars[i];
            switch (c) {
                case '\t': {
                    buf.append(c);
                    continue block7;
                }
                case '\n': {
                    buf.append(c);
                    continue block7;
                }
                case '\r': {
                    buf.append(c);
                    continue block7;
                }
                case '\b': {
                    buf.append("\\b");
                    continue block7;
                }
                case '\f': {
                    buf.append("\\f");
                    continue block7;
                }
                default: {
                    if (f == null || f.canDisplay(c)) {
                        buf.append(c);
                        continue block7;
                    }
                    buf.append("\\u");
                    String hex = Integer.toHexString(c);
                    for (int j = 0; j < 4 - hex.length(); ++j) {
                        buf.append('0');
                    }
                    buf.append(hex);
                }
            }
        }
        return buf.toString();
    }

    public void setToolTipText(String text) {
        final String displayableText = ToolTipSupport.makeDisplayable(text, UIManager.getFont("ToolTip.font"));
        Utilities.runInEventDispatchThread(new Runnable(){

            @Override
            public void run() {
                String oldText = ToolTipSupport.this.toolTipText;
                ToolTipSupport.this.toolTipText = displayableText;
                ToolTipSupport.this.firePropertyChange("toolTipText", oldText, ToolTipSupport.this.toolTipText);
                if (ToolTipSupport.this.toolTipText != null) {
                    if (ToolTipSupport.this.toolTipText.startsWith("<html") || ToolTipSupport.this.toolTipText.startsWith("<HTML")) {
                        JEditorPane jep = ToolTipSupport.this.createHtmlTextToolTip();
                        jep.setText(ToolTipSupport.this.toolTipText);
                        jep.putClientProperty("JEditorPane.honorDisplayProperties", Boolean.TRUE);
                        ToolTipSupport.this.setToolTip(jep);
                    } else {
                        JTextArea ta = ToolTipSupport.this.createTextToolTip(true);
                        ta.setText(ToolTipSupport.this.toolTipText);
                        ToolTipSupport.this.setToolTip(ta);
                    }
                } else if (ToolTipSupport.this.status == 2) {
                    ToolTipSupport.this.setToolTipVisible(false);
                }
            }
        });
    }

    private boolean isGlyphGutterMouseEvent(MouseEvent evt) {
        return evt != null && evt.getSource() == this.extEditorUI.getGlyphGutter();
    }

    private void ensureVisibility(Point toolTipPosition) {
        LOG.log(Level.FINE, "toolTipPosition={0}", toolTipPosition);
        JTextComponent component = this.extEditorUI.getComponent();
        if (component != null) {
            int pos = component.viewToModel(toolTipPosition);
            Rectangle cursorBounds = null;
            if (this.placement != PopupManager.FixedPoint && pos >= 0) {
                try {
                    cursorBounds = component.modelToView(pos);
                    this.extendBounds(cursorBounds);
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
            if (cursorBounds == null) {
                cursorBounds = new Rectangle(toolTipPosition, new Dimension(1, 1));
            }
            PopupManager pm = this.extEditorUI.getPopupManager();
            if (this.toolTip != null && this.toolTip.isVisible()) {
                this.toolTip.setVisible(false);
            }
            LOG.log(Level.FINE, "model-pos={0}, cursorBounds={1}", new Object[]{pos, cursorBounds});
            pm.install(this.toolTip, cursorBounds, this.placement, this.horizontalBounds, this.horizontalAdjustment, this.verticalAdjustment);
            if (this.toolTip != null) {
                this.toolTip.putClientProperty("ToolTipSupport.lastToolTipPosition", toolTipPosition);
                if (this.toolTip.getParent() != null) {
                    Rectangle blockBounds = null;
                    try {
                        int[] offsets = Utilities.getSelectionOrIdentifierBlock(component, pos);
                        if (offsets != null) {
                            Rectangle r1 = component.modelToView(offsets[0]);
                            Rectangle r2 = component.modelToView(offsets[1]);
                            blockBounds = new Rectangle(r1.x, r1.y, r2.x - r1.x, r1.height);
                        }
                    }
                    catch (BadLocationException ble) {
                        // empty catch block
                    }
                    this.toolTip.putClientProperty("ToolTipSupport.mouseMoveIgnoredArea", this.computeMouseMoveIgnoredArea(this.toolTip.getBounds(), blockBounds != null ? SwingUtilities.convertRectangle(component, blockBounds, this.toolTip.getParent()) : null, SwingUtilities.convertRectangle(component, cursorBounds, this.toolTip.getParent())));
                }
                this.toolTip.setVisible(true);
            }
        }
        this.exitTimer.restart();
    }

    private Rectangle extendBounds(Rectangle r) {
        if (this.horizontalBounds != PopupManager.ScrollBarBounds) {
            if (this.placement == PopupManager.AbovePreferred || this.placement == PopupManager.Above) {
                r.y -= 5;
                r.height += 10;
            } else if (this.placement == PopupManager.BelowPreferred || this.placement == PopupManager.Below) {
                r.y -= 5;
                r.height += 10;
            }
        }
        return r;
    }

    private Rectangle computeMouseMoveIgnoredArea(Rectangle toolTipBounds, Rectangle blockBounds, Rectangle cursorBounds) {
        Rectangle _toolTipBounds = new Rectangle(toolTipBounds);
        this.extendBounds(_toolTipBounds);
        Rectangle area = new Rectangle();
        Rectangle.union(_toolTipBounds, cursorBounds, area);
        if (blockBounds != null) {
            area.x = blockBounds.x;
            area.width = blockBounds.width;
        }
        area.x -= cursorBounds.width;
        area.width += 2 * cursorBounds.width;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "toolTip.bounds={0}, blockBounds={1}, cursorBounds={2}, mouseMoveIgnoredArea={3}", new Object[]{_toolTipBounds, blockBounds, cursorBounds, area});
        }
        return area;
    }

    public String getIdentifierUnderCursor() {
        String word = null;
        if (!this.isGlyphGutterMouseEvent(this.lastMouseEvent)) {
            try {
                JTextComponent component = this.extEditorUI.getComponent();
                BaseTextUI ui = (BaseTextUI)component.getUI();
                Point lmePoint = this.getLastMouseEventPoint();
                int pos = ui.viewToModel(component, lmePoint);
                if (pos >= 0) {
                    BaseDocument doc = (BaseDocument)component.getDocument();
                    int eolPos = Utilities.getRowEnd(doc, pos);
                    Rectangle eolRect = ui.modelToView(component, eolPos);
                    int lineHeight = this.extEditorUI.getLineHeight();
                    if (lmePoint.x <= eolRect.x && lmePoint.y <= eolRect.y + lineHeight) {
                        word = Utilities.getIdentifier(doc, pos);
                    }
                }
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        return word;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        if (enabled != this.enabled) {
            this.enabled = enabled;
            this.firePropertyChange("enabled", enabled ? Boolean.FALSE : Boolean.TRUE, enabled ? Boolean.TRUE : Boolean.FALSE);
            if (!enabled) {
                this.setToolTipVisible(false);
            }
        }
    }

    public int getInitialDelay() {
        return this.enterTimer.getDelay();
    }

    public void setInitialDelay(int delay) {
        if (this.enterTimer.getDelay() != delay) {
            int oldDelay = this.enterTimer.getDelay();
            this.enterTimer.setDelay(delay);
            this.firePropertyChange("initialDelay", new Integer(oldDelay), new Integer(this.enterTimer.getDelay()));
        }
    }

    public int getDismissDelay() {
        return this.exitTimer.getDelay();
    }

    public void setDismissDelay(int delay) {
        if (this.exitTimer.getDelay() != delay) {
            int oldDelay = this.exitTimer.getDelay();
            this.exitTimer.setDelay(delay);
            this.firePropertyChange("dismissDelay", new Integer(oldDelay), new Integer(this.exitTimer.getDelay()));
        }
    }

    public final MouseEvent getLastMouseEvent() {
        return this.lastMouseEvent;
    }

    private Point getLastMouseEventPoint() {
        Point p = null;
        MouseEvent lme = this.lastMouseEvent;
        if (lme != null) {
            JTextComponent c;
            p = lme.getPoint();
            if (lme.getSource() == this.extEditorUI.getGlyphGutter() && (c = this.extEditorUI.getComponent()) != null && c.getParent() instanceof JViewport) {
                JViewport vp = (JViewport)c.getParent();
                p = new Point(vp.getViewPosition().x, p.y);
            }
        }
        return p;
    }

    protected void componentToolTipTextChanged(PropertyChangeEvent evt) {
        JComponent component = (JComponent)evt.getSource();
        this.setToolTipText(component.getToolTipText());
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    private static void filterBindings(ActionMap actionMap) {
        for (Object key : actionMap.allKeys()) {
            String actionName = key.toString().toLowerCase(Locale.ENGLISH);
            LOG.log(Level.FINER, "Action-name: {0}", actionName);
            if (!actionName.contains("delete") && !actionName.contains("insert") && !actionName.contains("paste") && !actionName.contains("default") && !actionName.contains("cut")) continue;
            actionMap.put(key, NO_ACTION);
        }
    }

    private static String s2s(Object o) {
        return o == null ? "null" : o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o));
    }

    @PatchedPublic
    private void mouseDragged(MouseEvent evt) {
        this.listener.mouseDragged(evt);
    }

    @PatchedPublic
    private void mouseMoved(MouseEvent evt) {
        this.listener.mouseMoved(evt);
    }

    @PatchedPublic
    private void mouseClicked(MouseEvent evt) {
        this.listener.mouseClicked(evt);
    }

    @PatchedPublic
    private void mousePressed(MouseEvent evt) {
        this.listener.mousePressed(evt);
    }

    @PatchedPublic
    private void mouseReleased(MouseEvent evt) {
        this.listener.mouseReleased(evt);
    }

    @PatchedPublic
    private void mouseEntered(MouseEvent evt) {
        this.listener.mouseEntered(evt);
    }

    @PatchedPublic
    private void mouseExited(MouseEvent evt) {
        this.listener.mouseExited(evt);
    }

    @PatchedPublic
    private void actionPerformed(ActionEvent e) {
        this.listener.actionPerformed(e);
    }

    @PatchedPublic
    private void propertyChange(PropertyChangeEvent evt) {
        this.listener.propertyChange(evt);
    }

    @PatchedPublic
    private void focusGained(FocusEvent e) {
        this.listener.focusGained(e);
    }

    @PatchedPublic
    private void focusLost(FocusEvent e) {
        this.listener.focusLost(e);
    }

    static {
        EditorExtPackageAccessor.register(new Accessor());
        NO_ACTION = new TextAction("tooltip-no-action"){

            @Override
            public void actionPerformed(ActionEvent e) {
            }
        };
        NO_OP_MOUSE_LISTENER = new MouseAdapter(){};
    }

    private static final class Accessor
    extends EditorExtPackageAccessor {
        private Accessor() {
        }

        @Override
        public ToolTipSupport createToolTipSupport(EditorUI eui) {
            return new ToolTipSupport(eui);
        }
    }

    private final class Listener
    extends MouseAdapter
    implements MouseMotionListener,
    ActionListener,
    PropertyChangeListener,
    FocusListener,
    AncestorListener {
        private Listener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propName;
            JComponent component;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "propertyChange: source={0}, property={1}, old={2}, new={3}", new Object[]{ToolTipSupport.s2s(evt.getSource()), evt.getPropertyName(), ToolTipSupport.s2s(evt.getOldValue()), ToolTipSupport.s2s(evt.getNewValue())});
            }
            if ("component".equals(propName = evt.getPropertyName())) {
                component = (JTextComponent)evt.getNewValue();
                if (component != null) {
                    component.addPropertyChangeListener(this);
                    ToolTipSupport.this.disableSwingToolTip(component);
                    component.addAncestorListener(this);
                    component.addFocusListener(this);
                    if (component.hasFocus()) {
                        this.focusGained(new FocusEvent(component, 1004));
                    }
                    component.addMouseListener(this);
                    component.addMouseMotionListener(this);
                    GlyphGutter gg = ToolTipSupport.this.extEditorUI.getGlyphGutter();
                    if (gg != null && !ToolTipSupport.this.glyphListenerAdded) {
                        ToolTipSupport.this.glyphListenerAdded = true;
                        gg.addMouseListener(this);
                        gg.addMouseMotionListener(this);
                    }
                } else {
                    component = (JTextComponent)evt.getOldValue();
                    if (null != component) {
                        component.removeAncestorListener(this);
                        component.removeFocusListener(this);
                        component.removePropertyChangeListener(this);
                        component.removeMouseListener(this);
                        component.removeMouseMotionListener(this);
                        GlyphGutter gg = ToolTipSupport.this.extEditorUI.getGlyphGutter();
                        if (gg != null) {
                            gg.removeMouseListener(this);
                            gg.removeMouseMotionListener(this);
                        }
                        ToolTipSupport.this.setToolTipVisible(false);
                    }
                }
            }
            if ("ToolTipText".equals(propName)) {
                component = (JComponent)evt.getSource();
                ToolTipSupport.this.disableSwingToolTip(component);
                ToolTipSupport.this.componentToolTipTextChanged(evt);
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == ToolTipSupport.this.enterTimer) {
                if (!ToolTipSupport.this.isToolTipShowing() || (ToolTipSupport.this.flags & 4) == 0) {
                    ToolTipSupport.this.setToolTipVisible(true);
                }
            } else if (!(evt.getSource() != ToolTipSupport.this.exitTimer || ToolTipSupport.this.isToolTipShowing() && (ToolTipSupport.this.flags & 2) == 0)) {
                ToolTipSupport.this.setToolTipVisible(false);
            }
        }

        @Override
        public void mouseClicked(MouseEvent evt) {
            ToolTipSupport.this.lastMouseEvent = evt;
            ToolTipSupport.this.setToolTipVisible(false);
        }

        @Override
        public void mousePressed(MouseEvent evt) {
            ToolTipSupport.this.lastMouseEvent = evt;
            ToolTipSupport.this.setToolTipVisible(false);
        }

        @Override
        public void mouseReleased(MouseEvent evt) {
            ToolTipSupport.this.lastMouseEvent = evt;
            ToolTipSupport.this.setToolTipVisible(false);
            EditorUI ui = ToolTipSupport.this.extEditorUI;
            if (ui != null) {
                JTextComponent component = ui.getComponent();
                if (ToolTipSupport.this.enabled && component != null && Utilities.isSelectionShowing(component)) {
                    ToolTipSupport.this.enterTimer.restart();
                }
            }
        }

        @Override
        public void mouseEntered(MouseEvent evt) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "mouseEntered: x=" + evt.getX() + "; y=" + evt.getY());
            }
            ToolTipSupport.this.lastMouseEvent = evt;
        }

        @Override
        public void mouseExited(MouseEvent evt) {
            ToolTipSupport.this.lastMouseEvent = evt;
            if (ToolTipSupport.this.isToolTipShowing()) {
                Rectangle r = new Rectangle(ToolTipSupport.this.toolTip.getLocationOnScreen(), ToolTipSupport.this.toolTip.getSize());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "mouseExited: screen-x=" + evt.getXOnScreen() + "; screen-y=" + evt.getYOnScreen() + "; tooltip=" + r);
                }
                if (r.contains(evt.getLocationOnScreen())) {
                    return;
                }
            }
            if (!ToolTipSupport.this.isToolTipShowing() || (ToolTipSupport.this.flags & 1) != 0) {
                ToolTipSupport.this.setToolTipVisible(false);
            }
        }

        @Override
        public void mouseDragged(MouseEvent evt) {
            ToolTipSupport.this.lastMouseEvent = evt;
            ToolTipSupport.this.setToolTipVisible(false);
        }

        @Override
        public void mouseMoved(MouseEvent evt) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "mouseMoved: x=" + evt.getX() + "; y=" + evt.getY() + "enabled=" + ToolTipSupport.this.enabled + ", status=" + ToolTipSupport.this.status + ", flags=" + ToolTipSupport.this.flags);
            }
            if (ToolTipSupport.this.toolTip != null) {
                Rectangle ignoredArea = (Rectangle)ToolTipSupport.this.toolTip.getClientProperty("ToolTipSupport.mouseMoveIgnoredArea");
                Point mousePosition = SwingUtilities.convertPoint(evt.getComponent(), evt.getPoint(), ToolTipSupport.this.toolTip.getParent());
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Mouse-Move-Ignored-Area=" + ignoredArea + "; mouse=" + mousePosition + "; is-inside=" + (ignoredArea != null ? Boolean.valueOf(ignoredArea.contains(mousePosition)) : null));
                }
                if (ignoredArea != null && ignoredArea.contains(mousePosition)) {
                    return;
                }
            }
            if (!ToolTipSupport.this.isToolTipShowing() || (ToolTipSupport.this.flags & 1) != 0) {
                ToolTipSupport.this.setToolTipVisible(false);
            }
            if (ToolTipSupport.this.enabled) {
                ToolTipSupport.this.enterTimer.restart();
            }
            ToolTipSupport.this.lastMouseEvent = evt;
        }

        @Override
        public void focusGained(FocusEvent e) {
            GlyphGutter gg;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "focusGained: {0}", ToolTipSupport.s2s(e.getComponent()));
            }
            if ((gg = ToolTipSupport.this.extEditorUI.getGlyphGutter()) != null && !ToolTipSupport.this.glyphListenerAdded) {
                ToolTipSupport.this.glyphListenerAdded = true;
                gg.addMouseListener(this);
                gg.addMouseMotionListener(this);
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
        }

        @Override
        public void ancestorAdded(AncestorEvent event) {
        }

        @Override
        public void ancestorRemoved(AncestorEvent event) {
            LOG.log(Level.FINE, "ancestorRemoved: source={0}", ToolTipSupport.s2s(event.getSource()));
            ToolTipSupport.this.setToolTipVisible(false);
        }

        @Override
        public void ancestorMoved(AncestorEvent event) {
        }
    }

}

