/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.netbeans.editor.ext.DataAccessor;

public class FileAccessor
implements DataAccessor {
    private File f;
    private RandomAccessFile file;

    public FileAccessor(File file) {
        this.f = file;
    }

    @Override
    public void append(byte[] buffer, int off, int len) throws IOException {
        this.file.write(buffer, off, len);
    }

    @Override
    public void read(byte[] buffer, int off, int len) throws IOException {
        this.file.readFully(buffer, off, len);
    }

    @Override
    public void open(boolean requestWrite) throws IOException {
        this.file = new RandomAccessFile(this.f, requestWrite ? "rw" : "r");
        if (!this.f.exists()) {
            this.f.createNewFile();
        }
    }

    @Override
    public void close() throws IOException {
        if (this.file != null) {
            this.file.close();
        }
    }

    @Override
    public long getFilePointer() throws IOException {
        return this.file.getFilePointer();
    }

    @Override
    public void resetFile() throws IOException {
        this.file.setLength(0);
    }

    @Override
    public void seek(long pos) throws IOException {
        this.file.seek(pos);
    }

    @Override
    public int getFileLength() {
        return (int)this.f.length();
    }

    public String toString() {
        return this.f.getAbsolutePath();
    }
}

