/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor.ext;

import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtCaret;
import org.netbeans.editor.ext.ExtSyntaxSupport;
import org.netbeans.editor.ext.GotoDialogSupport;
import org.netbeans.editor.ext.KeyEventBlocker;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class ExtKit
extends BaseKit {
    public static final String buildPopupMenuAction = "build-popup-menu";
    public static final String showPopupMenuAction = "show-popup-menu";
    public static final String buildToolTipAction = "build-tool-tip";
    public static final String findAction = "find";
    public static final String replaceAction = "replace";
    public static final String gotoAction = "goto";
    public static final String gotoDeclarationAction = "goto-declaration";
    public static final String gotoSourceAction = "goto-source";
    public static final String gotoSuperImplementationAction = "goto-super-implementation";
    public static final String gotoHelpAction = "goto-help";
    public static final String matchBraceAction = "match-brace";
    public static final String selectionMatchBraceAction = "selection-match-brace";
    public static final String toggleCaseIdentifierBeginAction = "toggle-case-identifier-begin";
    public static final String codeSelectAction = "code-select";
    public static final String escapeAction = "escape";
    public static final String completionShowAction = "completion-show";
    public static final String allCompletionShowAction = "all-completion-show";
    public static final String documentationShowAction = "documentation-show";
    public static final String completionTooltipShowAction = "tooltip-show";
    public static final String commentAction = "comment";
    public static final String uncommentAction = "uncomment";
    public static final String toggleCommentAction = "toggle-comment";
    public static final String toggleToolbarAction = "toggle-toolbar";
    public static final String TRIMMED_TEXT = "trimmed-text";
    private static final String editorBundleHash = "org.netbeans.editor.Bundle#";
    private static final boolean debugPopupMenu = Boolean.getBoolean("netbeans.debug.editor.popup.menu");
    private boolean noExtEditorUIClass = false;

    @Override
    public Caret createCaret() {
        return new ExtCaret();
    }

    @Override
    public SyntaxSupport createSyntaxSupport(BaseDocument doc) {
        return new ExtSyntaxSupport(doc);
    }

    @Override
    protected EditorUI createEditorUI() {
        if (!this.noExtEditorUIClass) {
            try {
                ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                Class extEditorUIClass = loader.loadClass("org.netbeans.editor.ext.ExtEditorUI");
                return (EditorUI)extEditorUIClass.newInstance();
            }
            catch (Exception e) {
                this.noExtEditorUIClass = true;
            }
        }
        return new EditorUI();
    }

    @Override
    protected Action[] createActions() {
        ArrayList<BaseAction> actions = new ArrayList<BaseAction>();
        actions.add(new ExtDefaultKeyTypedAction());
        actions.add(new CommentAction());
        actions.add(new UncommentAction());
        return TextAction.augmentList(super.createActions(), actions.toArray(new Action[actions.size()]));
    }

    public static class ExtDeleteCharAction
    extends BaseKit.DeleteCharAction {
        public ExtDeleteCharAction(String nm, boolean nextChar) {
            super(nm, nextChar);
        }
    }

    public static class CompletionTooltipShowAction
    extends BaseKitLocalizedAction {
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class DocumentationShowAction
    extends BaseKitLocalizedAction {
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class AllCompletionShowAction
    extends BaseKitLocalizedAction {
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class CompletionShowAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = 1050644925893851146L;

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class ExtDefaultKeyTypedAction
    extends BaseKit.DefaultKeyTypedAction {
        static final long serialVersionUID = 5273032708909044812L;

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            String cmd = evt.getActionCommand();
            int mod = evt.getModifiers();
            if (cmd == null || !cmd.equals(" ") || mod != 2) {
                Caret caret = target.getCaret();
                if (caret instanceof ExtCaret) {
                    ((ExtCaret)caret).requestMatchBraceUpdateSync();
                }
                super.actionPerformed(evt, target);
            }
            if (target != null && evt != null && cmd != null && cmd.length() == 1) {
                this.checkIndentHotChars(target, cmd);
                this.checkCompletion(target, cmd);
            }
        }

        protected void checkIndentHotChars(JTextComponent target, String typedText) {
        }

        protected void checkCompletion(JTextComponent target, String typedText) {
        }
    }

    public static class EscapeAction
    extends BaseKitLocalizedAction {
        public EscapeAction() {
            super("escape");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Utilities.getEditorUI(target).hidePopupMenu();
            }
        }
    }

    public static class ToggleCommentAction
    extends BaseAction {
        static final long serialVersionUID = -1;
        private final String lineCommentString;
        private final int lineCommentStringLen;

        public ToggleCommentAction(String lineCommentString) {
            super("toggle-comment");
            this.putValue("ShortDescription", NbBundle.getMessage(ToggleCommentAction.class, (String)"ToggleCommentAction_shortDescription"));
            assert (lineCommentString != null);
            this.lineCommentString = lineCommentString;
            this.lineCommentStringLen = lineCommentString.length();
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/comment.png");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            this.commentUncomment(evt, target, null);
        }

        private void commentUncomment(ActionEvent evt, final JTextComponent target, final Boolean forceComment) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            int startPos;
                            boolean comment;
                            int endPos;
                            if (Utilities.isSelectionShowing(caret)) {
                                startPos = Utilities.getRowStart(doc, target.getSelectionStart());
                                endPos = target.getSelectionEnd();
                                if (endPos > 0 && Utilities.getRowStart(doc, endPos) == endPos) {
                                    --endPos;
                                }
                                endPos = Utilities.getRowEnd(doc, endPos);
                            } else {
                                startPos = Utilities.getRowStart(doc, caret.getDot());
                                endPos = Utilities.getRowEnd(doc, caret.getDot());
                            }
                            int lineCount = Utilities.getRowCount(doc, startPos, endPos);
                            boolean bl = forceComment != null ? forceComment : (comment = !ToggleCommentAction.this.allComments(doc, startPos, lineCount));
                            if (comment) {
                                ToggleCommentAction.this.comment(doc, startPos, lineCount);
                            } else {
                                ToggleCommentAction.this.uncomment(doc, startPos, lineCount);
                            }
                            NavigationHistory.getEdits().markWaypoint(target, startPos, false, true);
                        }
                        catch (BadLocationException e) {
                            target.getToolkit().beep();
                        }
                    }
                });
            }
        }

        private boolean allComments(BaseDocument doc, int startOffset, int lineCount) throws BadLocationException {
            int offset = startOffset;
            while (lineCount > 0) {
                int firstNonWhitePos = Utilities.getRowFirstNonWhite(doc, offset);
                if (firstNonWhitePos == -1) {
                    return false;
                }
                if (Utilities.getRowEnd(doc, firstNonWhitePos) - firstNonWhitePos < this.lineCommentStringLen) {
                    return false;
                }
                CharSequence maybeLineComment = DocumentUtilities.getText((Document)doc, (int)firstNonWhitePos, (int)this.lineCommentStringLen);
                if (!CharSequenceUtilities.textEquals((CharSequence)maybeLineComment, (CharSequence)this.lineCommentString)) {
                    return false;
                }
                offset = Utilities.getRowStart(doc, offset, 1);
                --lineCount;
            }
            return true;
        }

        private void comment(BaseDocument doc, int startOffset, int lineCount) throws BadLocationException {
            int offset = startOffset;
            while (lineCount > 0) {
                doc.insertString(offset, this.lineCommentString, null);
                offset = Utilities.getRowStart(doc, offset, 1);
                --lineCount;
            }
        }

        private void uncomment(BaseDocument doc, int startOffset, int lineCount) throws BadLocationException {
            int offset = startOffset;
            while (lineCount > 0) {
                CharSequence maybeLineComment;
                int firstNonWhitePos = Utilities.getRowFirstNonWhite(doc, offset);
                if (firstNonWhitePos != -1 && Utilities.getRowEnd(doc, firstNonWhitePos) - firstNonWhitePos >= this.lineCommentStringLen && CharSequenceUtilities.textEquals((CharSequence)(maybeLineComment = DocumentUtilities.getText((Document)doc, (int)firstNonWhitePos, (int)this.lineCommentStringLen)), (CharSequence)this.lineCommentString)) {
                    doc.remove(firstNonWhitePos, this.lineCommentStringLen);
                }
                offset = Utilities.getRowStart(doc, offset, 1);
                --lineCount;
            }
        }

    }

    public static class UncommentAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = -7005758666529862034L;
        private ToggleCommentAction delegateAction;

        private UncommentAction() {
            this((String)null);
        }

        public UncommentAction(String lineCommentString) {
            super("uncomment");
            this.delegateAction = lineCommentString != null ? new ToggleCommentAction(lineCommentString) : null;
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/uncomment.png");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            BaseAction action = null;
            if (this.delegateAction != null) {
                action = this.delegateAction;
            } else {
                Action a;
                BaseKit kit = Utilities.getKit(target);
                Action action2 = a = kit == null ? null : kit.getActionByName("toggle-comment");
                if (a instanceof BaseAction) {
                    action = (BaseAction)a;
                }
            }
            if (action instanceof ToggleCommentAction) {
                ((ToggleCommentAction)action).commentUncomment(evt, target, Boolean.FALSE);
            } else {
                if (action != null) {
                    action.putValue("force-uncomment", Boolean.TRUE);
                    try {
                        action.actionPerformed(evt, target);
                    }
                    finally {
                        action.putValue("force-uncomment", null);
                    }
                }
                target.getToolkit().beep();
            }
        }
    }

    public static class CommentAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = -1422954906554289179L;
        private ToggleCommentAction delegateAction;

        private CommentAction() {
            this((String)null);
        }

        public CommentAction(String lineCommentString) {
            super("comment");
            this.delegateAction = lineCommentString != null ? new ToggleCommentAction(lineCommentString) : null;
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/comment.png");
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            BaseAction action = null;
            if (this.delegateAction != null) {
                action = this.delegateAction;
            } else {
                Action a;
                BaseKit kit = Utilities.getKit(target);
                Action action2 = a = kit == null ? null : kit.getActionByName("toggle-comment");
                if (a instanceof BaseAction) {
                    action = (BaseAction)a;
                }
            }
            if (action instanceof ToggleCommentAction) {
                ((ToggleCommentAction)action).commentUncomment(evt, target, Boolean.TRUE);
            } else {
                if (action != null) {
                    action.putValue("force-comment", Boolean.TRUE);
                    try {
                        action.actionPerformed(evt, target);
                    }
                    finally {
                        action.putValue("force-comment", null);
                    }
                }
                target.getToolkit().beep();
            }
        }
    }

    public static class PrefixMakerAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = -2305157963664484920L;
        private String prefix;
        private String[] prefixGroup;

        public PrefixMakerAction(String name, String prefix, String[] prefixGroup) {
            super(name);
            this.prefix = prefix;
            this.prefixGroup = prefixGroup;
            String iconRes = null;
            if ("get".equals(prefix)) {
                iconRes = "org/netbeans/modules/editor/resources/var_get.gif";
            } else if ("set".equals(prefix)) {
                iconRes = "org/netbeans/modules/editor/resources/var_set.gif";
            } else if ("is".equals(prefix)) {
                iconRes = "org/netbeans/modules/editor/resources/var_is.gif";
            }
            if (iconRes != null) {
                this.putValue("IconResource", iconRes);
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                BaseDocument doc = (BaseDocument)target.getDocument();
                int dotPos = target.getCaret().getDot();
                try {
                    int[] block = Utilities.getIdentifierBlock(doc, dotPos);
                    if (block == null) {
                        target.getToolkit().beep();
                        return;
                    }
                    CharSequence identifier = DocumentUtilities.getText((Document)doc, (int)block[0], (int)(block[1] - block[0]));
                    if (CharSequenceUtilities.startsWith((CharSequence)identifier, (CharSequence)this.prefix) && Character.isUpperCase(identifier.charAt(this.prefix.length()))) {
                        return;
                    }
                    for (int i = 0; i < this.prefixGroup.length; ++i) {
                        String actPref = this.prefixGroup[i];
                        if (!CharSequenceUtilities.startsWith((CharSequence)identifier, (CharSequence)actPref) || identifier.length() <= actPref.length() || !Character.isUpperCase(identifier.charAt(actPref.length()))) continue;
                        doc.remove(block[0], actPref.length());
                        doc.insertString(block[0], this.prefix, null);
                        return;
                    }
                    Utilities.changeCase(doc, block[0], 1, 0);
                    doc.insertString(block[0], this.prefix, null);
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class CodeSelectAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = 4033474080778585860L;

        public CodeSelectAction() {
            super("code-select");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class MatchBraceAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = -184887499045886231L;

        public MatchBraceAction(String name, boolean select) {
            super(name, 0);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class ToggleCaseIdentifierBeginAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = 584392193824931979L;

        public ToggleCaseIdentifierBeginAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                try {
                    Caret caret = target.getCaret();
                    BaseDocument doc = (BaseDocument)target.getDocument();
                    int[] idBlk = Utilities.getIdentifierBlock(doc, caret.getDot());
                    if (idBlk != null) {
                        Utilities.changeCase(doc, idBlk[0], 1, 2);
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class GotoDeclarationAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = -6440495023918097760L;

        public GotoDeclarationAction() {
            super("goto-declaration", 142);
            String name = NbBundle.getBundle(BaseKit.class).getString("goto-declaration-trimmed");
            this.putValue("trimmed-text", name);
            this.putValue("PopupMenuText", name);
        }

        public boolean gotoDeclaration(JTextComponent target) {
            BaseDocument doc = Utilities.getDocument(target);
            if (doc == null) {
                return false;
            }
            try {
                int decPos;
                Caret caret = target.getCaret();
                int dotPos = caret.getDot();
                int[] idBlk = Utilities.getIdentifierBlock(doc, dotPos);
                ExtSyntaxSupport extSup = (ExtSyntaxSupport)doc.getSyntaxSupport();
                if (idBlk != null && (decPos = extSup.findDeclarationPosition(doc.getText(idBlk), idBlk[1])) >= 0) {
                    caret.setDot(decPos);
                    return true;
                }
            }
            catch (BadLocationException e) {
                // empty catch block
            }
            return false;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                this.gotoDeclaration(target);
            }
        }
    }

    public static class GotoAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = 8425585413146373256L;

        public GotoAction() {
            super("goto", 14);
            String name = NbBundle.getBundle(BaseKit.class).getString("goto_trimmed");
            this.putValue("trimmed-text", name);
            this.putValue("PopupMenuText", name);
        }

        protected int getOffsetFromLine(BaseDocument doc, int lineOffset) {
            return Utilities.getRowStartFromLineOffset(doc, lineOffset);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                new GotoDialogSupport().showGotoDialog(new KeyEventBlocker(target, false));
            }
        }
    }

    public static class BuildToolTipAction
    extends BaseAction {
        static final long serialVersionUID = -2701131863705941250L;

        public BuildToolTipAction() {
            super("build-tool-tip", 64);
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        public BuildToolTipAction(Map attrs) {
            this();
        }

        protected String buildText(JTextComponent target) {
            ToolTipSupport tts = Utilities.getEditorUI(target).getToolTipSupport();
            return tts != null ? target.getToolTipText(tts.getLastMouseEvent()) : target.getToolTipText();
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            ToolTipSupport tts;
            if (target != null && (tts = Utilities.getEditorUI(target).getToolTipSupport()) != null) {
                tts.setToolTipText(this.buildText(target));
            }
        }
    }

    public static class ShowPopupMenuAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = 4257043398248915291L;

        public ShowPopupMenuAction() {
            super(64);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                try {
                    EditorUI eui;
                    int dotPos = target.getCaret().getDot();
                    Rectangle r = target.getUI().modelToView(target, dotPos);
                    if (r != null && (eui = Utilities.getEditorUI(target)) != null) {
                        eui.showPopupMenu(r.x, r.y + r.height);
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class BuildPopupMenuAction
    extends BaseKitLocalizedAction {
        static final long serialVersionUID = 4257043398248915291L;

        public BuildPopupMenuAction() {
            super("build-popup-menu", 64);
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        public BuildPopupMenuAction(Map attrs) {
            this();
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (debugPopupMenu) {
                    System.err.println("POPUP CREATION START <<<<<");
                }
                JPopupMenu pm = this.buildPopupMenu(target);
                if (debugPopupMenu) {
                    System.err.println("POPUP CREATION END >>>>>");
                }
                Utilities.getEditorUI(target).setPopupMenu(pm);
            }
        }

        protected JPopupMenu createPopupMenu(JTextComponent target) {
            return new JPopupMenu();
        }

        protected JPopupMenu buildPopupMenu(JTextComponent target) {
            JPopupMenu pm = this.createPopupMenu(target);
            EditorUI ui = Utilities.getEditorUI(target);
            String settingName = ui == null || ui.hasExtComponent() ? "popup-menu-action-name-list" : "dialog-popup-menu-action-name-list";
            Preferences prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)target)).lookup(Preferences.class);
            String actionNames = prefs.get(settingName, null);
            if (actionNames != null) {
                StringTokenizer t = new StringTokenizer(actionNames, ",");
                while (t.hasMoreTokens()) {
                    String action = t.nextToken().trim();
                    this.addAction(target, pm, action);
                }
            }
            return pm;
        }

        protected void addAction(JTextComponent target, JPopupMenu popupMenu, String actionName) {
            Action a = Utilities.getKit(target).getActionByName(actionName);
            if (a != null) {
                String itemText;
                JMenuItem item = null;
                if (a instanceof BaseAction) {
                    item = ((BaseAction)a).getPopupMenuItem(target);
                }
                if (item == null && (itemText = this.getItemText(target, actionName, a)) != null) {
                    KeyStroke[] keys;
                    item = new JMenuItem(itemText);
                    item.addActionListener(a);
                    Keymap km = target.getKeymap();
                    if (km != null && (keys = km.getKeyStrokesForAction(a)) != null && keys.length > 0) {
                        item.setAccelerator(keys[0]);
                    }
                    item.setEnabled(a.isEnabled());
                    Object helpID = a.getValue("helpID");
                    if (helpID != null && helpID instanceof String) {
                        item.putClientProperty("HelpID", helpID);
                    }
                }
                if (item != null) {
                    this.debugPopupMenuItem(item, a);
                    popupMenu.add(item);
                }
            } else if (actionName == null && popupMenu.getComponentCount() > 0) {
                this.debugPopupMenuItem(null, null);
                popupMenu.addSeparator();
            }
        }

        protected final void debugPopupMenuItem(JMenuItem item, Action action) {
            if (debugPopupMenu) {
                StringBuilder sb = new StringBuilder("POPUP: ");
                if (item != null) {
                    sb.append('\"');
                    sb.append(item.getText());
                    sb.append('\"');
                    if (!item.isVisible()) {
                        sb.append(", INVISIBLE");
                    }
                    if (action != null) {
                        sb.append(", action=");
                        sb.append(action.getClass().getName());
                    }
                } else {
                    sb.append("--Separator--");
                }
                System.err.println(sb.toString());
            }
        }

        protected String getItemText(JTextComponent target, String actionName, Action a) {
            String itemText;
            if (a instanceof BaseAction) {
                itemText = ((BaseAction)a).getPopupMenuText(target);
            } else {
                itemText = (String)a.getValue("popupText");
                if (itemText == null && (itemText = (String)a.getValue("menuText")) == null) {
                    itemText = actionName;
                }
            }
            return itemText;
        }
    }

    private static abstract class BaseKitLocalizedAction
    extends BaseAction {
        public BaseKitLocalizedAction() {
        }

        public BaseKitLocalizedAction(int updateMask) {
            super(updateMask);
        }

        public BaseKitLocalizedAction(String name) {
            super(name);
        }

        public BaseKitLocalizedAction(String name, int updateMask) {
            super(name, updateMask);
        }

        @Override
        protected Class getShortDescriptionBundleClass() {
            return BaseKit.class;
        }
    }

}

