/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor.ext;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorState;
import org.netbeans.editor.ext.KeyEventBlocker;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class GotoDialogPanel
extends JPanel
implements FocusListener {
    static final long serialVersionUID = -8686958102543713464L;
    private static final String HISTORY_KEY = "GotoDialogPanel.history-goto-line";
    private static final int MAX_ITEMS = 20;
    private boolean dontFire = false;
    private KeyEventBlocker blocker;
    private final ResourceBundle bundle = NbBundle.getBundle(BaseKit.class);
    protected JComboBox gotoCombo;
    protected JLabel gotoLabel;

    public GotoDialogPanel() {
        this.initComponents();
        this.getAccessibleContext().setAccessibleName(this.bundle.getString("goto-title"));
        this.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_goto"));
        this.gotoCombo.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_goto-line"));
        ArrayList history = (ArrayList)EditorState.get("GotoDialogPanel.history-goto-line");
        if (history == null) {
            history = new ArrayList();
        }
        this.updateCombo(history);
    }

    protected void updateCombo(List content) {
        this.dontFire = true;
        this.gotoCombo.setModel(new DefaultComboBoxModel<Object>(content.toArray()));
        this.dontFire = false;
    }

    private void initComponents() {
        this.gotoLabel = new JLabel();
        this.gotoCombo = new JComboBox();
        this.setLayout(new GridBagLayout());
        this.gotoLabel.setLabelFor(this.gotoCombo);
        Mnemonics.setLocalizedText((JLabel)this.gotoLabel, (String)this.bundle.getString("goto-line"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(12, 12, 0, 11);
        this.add((Component)this.gotoLabel, gridBagConstraints);
        this.gotoCombo.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 10);
        this.add((Component)this.gotoCombo, gridBagConstraints);
    }

    public String getValue() {
        return (String)this.gotoCombo.getEditor().getItem();
    }

    public void updateHistory() {
        String value;
        List history = (ArrayList<String>)EditorState.get("GotoDialogPanel.history-goto-line");
        if (history == null) {
            history = new ArrayList<String>();
        }
        if (history.contains(value = this.getValue())) {
            history.remove(value);
            history.add(0, value);
        } else {
            if (history.size() >= 20) {
                history = history.subList(0, 19);
            }
            history.add(0, this.getValue());
        }
        EditorState.put("GotoDialogPanel.history-goto-line", history);
        this.updateCombo(history);
    }

    public void popupNotify(KeyEventBlocker blocker) {
        this.blocker = blocker;
        this.gotoCombo.getEditor().getEditorComponent().addFocusListener(this);
        this.gotoCombo.getEditor().selectAll();
        this.gotoCombo.getEditor().getEditorComponent().requestFocus();
    }

    public JComboBox getGotoCombo() {
        return this.gotoCombo;
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (this.blocker != null) {
            this.blocker.stopBlocking();
        }
        ((JComponent)e.getSource()).removeFocusListener(this);
    }

    @Override
    public void focusLost(FocusEvent e) {
    }
}

