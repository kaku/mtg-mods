/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import org.netbeans.editor.Syntax;

public class MultiSyntax
extends Syntax {
    private SyntaxInfo slaveSyntaxChain;
    private SyntaxInfo slaveSyntaxChainEnd;

    protected void registerSyntax(Syntax slaveSyntax) {
        this.slaveSyntaxChainEnd = new SyntaxInfo(slaveSyntax, this.slaveSyntaxChainEnd);
        if (this.slaveSyntaxChain == null) {
            this.slaveSyntaxChain = this.slaveSyntaxChainEnd;
        }
    }

    @Override
    public void storeState(Syntax.StateInfo stateInfo) {
        super.storeState(stateInfo);
        ((MultiStateInfo)stateInfo).store(this);
    }

    @Override
    public void loadInitState() {
        super.loadInitState();
        SyntaxInfo syntaxItem = this.slaveSyntaxChain;
        while (syntaxItem != null) {
            syntaxItem.syntax.loadInitState();
            syntaxItem = syntaxItem.next;
        }
    }

    @Override
    public void load(Syntax.StateInfo stateInfo, char[] buffer, int offset, int len, boolean lastBuffer, int stopPosition) {
        ((MultiStateInfo)stateInfo).load(this, buffer, offset, len, lastBuffer, stopPosition);
        super.load(stateInfo, buffer, offset, len, lastBuffer, stopPosition);
    }

    @Override
    public Syntax.StateInfo createStateInfo() {
        return new MultiStateInfo();
    }

    @Override
    public int compareState(Syntax.StateInfo stateInfo) {
        int diff = super.compareState(stateInfo);
        if (diff == 0) {
            diff = ((MultiStateInfo)stateInfo).compare(this);
        }
        return diff;
    }

    static class SyntaxInfo {
        Syntax syntax;
        boolean active;
        SyntaxInfo next;
        SyntaxInfo prev;

        SyntaxInfo(Syntax syntax, SyntaxInfo prevChainEnd) {
            this.syntax = syntax;
            if (prevChainEnd != null) {
                this.prev = prevChainEnd;
                prevChainEnd.next = this;
            }
        }
    }

    public static class MultiStateInfo
    extends Syntax.BaseStateInfo {
        private ChainItem stateInfoChain;

        void load(MultiSyntax masterSyntax, char[] buffer, int offset, int len, boolean lastBuffer, int stopPosition) {
            SyntaxInfo syntaxItem = masterSyntax.slaveSyntaxChain;
            while (syntaxItem != null) {
                Syntax.StateInfo loadInfo = null;
                int masterOffsetDelta = 0;
                Syntax s = syntaxItem.syntax;
                if (syntaxItem.active) {
                    Class sc = s.getClass();
                    ChainItem item = this.stateInfoChain;
                    while (item != null) {
                        if (item.syntaxClass == sc && item.valid) {
                            loadInfo = item.stateInfo;
                            masterOffsetDelta = item.masterOffsetDelta;
                            break;
                        }
                        item = item.prev;
                    }
                }
                s.load(loadInfo, buffer, offset + masterOffsetDelta, len - masterOffsetDelta, lastBuffer, stopPosition);
                syntaxItem = syntaxItem.next;
            }
        }

        void store(MultiSyntax masterSyntax) {
            ChainItem item = this.stateInfoChain;
            while (item != null) {
                item.valid = false;
                item = item.prev;
            }
            SyntaxInfo syntaxItem = masterSyntax.slaveSyntaxChain;
            while (syntaxItem != null) {
                if (syntaxItem.active) {
                    Syntax s = syntaxItem.syntax;
                    Class sc = s.getClass();
                    item = this.stateInfoChain;
                    while (item != null && item.syntaxClass != sc) {
                        item = item.prev;
                    }
                    if (item == null) {
                        item = this.stateInfoChain = new ChainItem(s.createStateInfo(), sc, this.stateInfoChain);
                    }
                    s.storeState(item.stateInfo);
                    item.masterOffsetDelta = s.getOffset() - masterSyntax.getOffset();
                    item.valid = true;
                }
                syntaxItem = syntaxItem.next;
            }
        }

        int compare(MultiSyntax masterSyntax) {
            int ret = 0;
            ChainItem item = this.stateInfoChain;
            while (item != null && ret == 0) {
                if (item.valid) {
                    Class sc = item.syntaxClass;
                    SyntaxInfo syntaxItem = masterSyntax.slaveSyntaxChain;
                    while (syntaxItem != null) {
                        if (syntaxItem.syntax.getClass() == sc) {
                            if (syntaxItem.active) {
                                ret = syntaxItem.syntax.compareState(item.stateInfo);
                                break;
                            }
                            ret = 1;
                            break;
                        }
                        syntaxItem = syntaxItem.next;
                    }
                }
                item = item.prev;
            }
            return ret;
        }

        static class ChainItem {
            boolean valid;
            Syntax.StateInfo stateInfo;
            int masterOffsetDelta;
            Class syntaxClass;
            ChainItem prev;

            ChainItem(Syntax.StateInfo stateInfo, Class syntaxClass, ChainItem prev) {
                this.stateInfo = stateInfo;
                this.syntaxClass = syntaxClass;
                this.prev = prev;
            }
        }

    }

}

