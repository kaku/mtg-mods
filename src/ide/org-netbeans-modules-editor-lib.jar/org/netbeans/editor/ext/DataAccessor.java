/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.io.IOException;

public interface DataAccessor {
    public void open(boolean var1) throws IOException;

    public void close() throws IOException;

    public void read(byte[] var1, int var2, int var3) throws IOException;

    public void append(byte[] var1, int var2, int var3) throws IOException;

    public long getFilePointer() throws IOException;

    public void resetFile() throws IOException;

    public void seek(long var1) throws IOException;

    public int getFileLength();
}

