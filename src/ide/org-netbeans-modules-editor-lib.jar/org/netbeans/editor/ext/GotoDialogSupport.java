/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor.ext;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.SwingUtilities;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.DialogSupport;
import org.netbeans.editor.EditorState;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ExtKit;
import org.netbeans.editor.ext.GotoDialogPanel;
import org.netbeans.editor.ext.KeyEventBlocker;
import org.openide.util.NbBundle;

public class GotoDialogSupport
implements ActionListener {
    private static final String BOUNDS_KEY = "GotoDialogSupport.bounds-goto-line";
    private JButton[] gotoButtons;
    private GotoDialogPanel gotoPanel;
    private static Dialog gotoDialog;

    public GotoDialogSupport() {
        ResourceBundle bundle = NbBundle.getBundle(BaseKit.class);
        JButton gotoButton = new JButton(bundle.getString("goto-button-goto"));
        JButton cancelButton = new JButton(bundle.getString("goto-button-cancel"));
        gotoButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_goto-button-goto"));
        cancelButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_goto-button-cancel"));
        this.gotoButtons = new JButton[]{gotoButton, cancelButton};
        this.gotoPanel = new GotoDialogPanel();
        this.gotoPanel.getGotoCombo().getEditor().getEditorComponent().addKeyListener(new KeyListener(){

            @Override
            public void keyPressed(KeyEvent evt) {
            }

            @Override
            public void keyReleased(KeyEvent evt) {
            }

            @Override
            public void keyTyped(KeyEvent evt) {
                if (evt.getKeyChar() == '\n') {
                    GotoDialogSupport.this.actionPerformed(new ActionEvent(GotoDialogSupport.this.gotoButtons[0], 0, null));
                }
            }
        });
    }

    protected synchronized Dialog createGotoDialog() {
        if (gotoDialog == null) {
            gotoDialog = DialogSupport.createDialog(NbBundle.getBundle(BaseKit.class).getString("goto-title"), this.gotoPanel, false, this.gotoButtons, false, 0, 1, this);
            gotoDialog.pack();
            Rectangle lastBounds = (Rectangle)EditorState.get("GotoDialogSupport.bounds-goto-line");
            if (lastBounds != null) {
                gotoDialog.setBounds(lastBounds);
            } else {
                int y;
                int x;
                Window w;
                Dimension dim = gotoDialog.getPreferredSize();
                JTextComponent c = EditorRegistry.lastFocusedComponent();
                Window window = w = c != null ? SwingUtilities.getWindowAncestor(c) : null;
                if (w != null) {
                    x = Math.max(0, w.getX() + (w.getWidth() - dim.width) / 2);
                    y = Math.max(0, w.getY() + (w.getHeight() - dim.height) / 2);
                } else {
                    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                    x = Math.max(0, (screen.width - dim.width) / 2);
                    y = Math.max(0, (screen.height - dim.height) / 2);
                }
                gotoDialog.setLocation(x, y);
            }
            return gotoDialog;
        }
        gotoDialog.setVisible(true);
        gotoDialog.toFront();
        return null;
    }

    protected synchronized void disposeGotoDialog() {
        if (gotoDialog != null) {
            EditorState.put("GotoDialogSupport.bounds-goto-line", gotoDialog.getBounds());
            gotoDialog.dispose();
            Utilities.returnFocus();
        }
        gotoDialog = null;
    }

    public void showGotoDialog(final KeyEventBlocker blocker) {
        Dialog dialog = this.createGotoDialog();
        if (dialog == null) {
            return;
        }
        dialog.setVisible(true);
        this.gotoPanel.popupNotify(blocker);
        WindowAdapter winAdapt = new WindowAdapter(){

            @Override
            public void windowClosing(WindowEvent evt) {
                GotoDialogSupport.this.disposeGotoDialog();
            }

            @Override
            public void windowClosed(WindowEvent evt) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if (blocker != null) {
                            blocker.stopBlocking(false);
                        }
                    }
                });
            }

        };
        dialog.addWindowListener(winAdapt);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object src = evt.getSource();
        if (src == this.gotoButtons[0] || src == this.gotoPanel) {
            if (this.performGoto()) {
                this.gotoPanel.updateHistory();
                this.disposeGotoDialog();
            }
        } else {
            this.disposeGotoDialog();
        }
    }

    protected final String getGotoValueText() {
        return this.gotoPanel.getValue();
    }

    protected boolean performGoto() {
        block7 : {
            JTextComponent c = EditorRegistry.lastFocusedComponent();
            if (c != null) {
                try {
                    Action a;
                    BaseDocument doc;
                    int line = Integer.parseInt(this.getGotoValueText());
                    if (line == 0) {
                        line = 1;
                    }
                    if ((doc = Utilities.getDocument(c)) == null) break block7;
                    int rowCount = Utilities.getRowCount(doc);
                    if (line > rowCount) {
                        line = rowCount;
                    }
                    int pos = Utilities.getRowStartFromLineOffset(doc, line - 1);
                    BaseKit kit = Utilities.getKit(c);
                    if (kit != null && (a = kit.getActionByName("goto")) instanceof ExtKit.GotoAction) {
                        pos = ((ExtKit.GotoAction)a).getOffsetFromLine(doc, line - 1);
                    }
                    if (pos != -1) {
                        Caret caret = c.getCaret();
                        caret.setDot(pos);
                        break block7;
                    }
                    c.getToolkit().beep();
                    return false;
                }
                catch (NumberFormatException e) {
                    c.getToolkit().beep();
                    return false;
                }
            }
        }
        return true;
    }

}

