/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.PrintStream;
import java.util.LinkedList;
import javax.swing.text.JTextComponent;

public class KeyEventBlocker
implements KeyListener {
    private LinkedList blockedEvents = new LinkedList();
    private JTextComponent component;
    private boolean discardKeyTyped = true;
    private static final boolean debugBlockEvent = Boolean.getBoolean("netbeans.debug.editor.blocker");

    public KeyEventBlocker(JTextComponent component, boolean discardFirstKeyTypedEvent) {
        this.component = component;
        this.discardKeyTyped = discardFirstKeyTypedEvent;
        if (debugBlockEvent) {
            System.out.println("");
            System.out.println("attaching listener" + this.component.getClass() + " - " + this.component.hashCode());
        }
        this.component.addKeyListener(this);
    }

    public void stopBlocking(boolean dispatchBlockedEvents) {
        if (debugBlockEvent) {
            System.out.println("removing listener from " + this.component.getClass() + " - " + this.component.hashCode());
        }
        this.component.removeKeyListener(this);
        if (dispatchBlockedEvents) {
            KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            Window focusOwner = kfm.getFocusedWindow();
            while (!this.blockedEvents.isEmpty()) {
                KeyEvent e = (KeyEvent)this.blockedEvents.removeFirst();
                Window src = focusOwner != null ? focusOwner : (Component)e.getSource();
                e = new KeyEvent(src, e.getID(), e.getWhen(), e.getModifiers(), e.getKeyCode(), e.getKeyChar(), e.getKeyLocation());
                kfm.dispatchEvent(e);
            }
        }
    }

    public void stopBlocking() {
        this.stopBlocking(true);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (debugBlockEvent) {
            System.out.println("consuming keyPressed event:" + KeyEvent.getKeyModifiersText(e.getModifiers()) + " + " + KeyEvent.getKeyText(e.getKeyCode()));
        }
        e.consume();
        this.blockedEvents.add(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (debugBlockEvent) {
            System.out.println("consuming keyReleased event:" + KeyEvent.getKeyModifiersText(e.getModifiers()) + " + " + KeyEvent.getKeyText(e.getKeyCode()));
        }
        e.consume();
        this.blockedEvents.add(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (debugBlockEvent) {
            System.out.println("consuming keyTyped event:" + KeyEvent.getKeyModifiersText(e.getModifiers()) + " + " + KeyEvent.getKeyText(e.getKeyCode()));
        }
        e.consume();
        if (this.discardKeyTyped) {
            this.discardKeyTyped = false;
        } else {
            this.blockedEvents.add(e);
        }
    }
}

