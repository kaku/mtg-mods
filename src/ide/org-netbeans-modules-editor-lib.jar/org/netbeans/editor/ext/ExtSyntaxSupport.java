/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.util.HashMap;
import java.util.Map;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Finder;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TextBatchProcessor;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.TokenProcessor;
import org.netbeans.editor.Utilities;

public class ExtSyntaxSupport
extends SyntaxSupport {
    public static final int COMPLETION_POPUP = 0;
    public static final int COMPLETION_CANCEL = 1;
    public static final int COMPLETION_REFRESH = 2;
    public static final int COMPLETION_POST_REFRESH = 3;
    public static final int COMPLETION_HIDE = 4;
    private static final TokenID[] EMPTY_TOKEN_ID_ARRAY = new TokenID[0];
    private DocumentListener docL;
    private HashMap localVarMaps = new HashMap();
    private HashMap globalVarMaps = new HashMap();

    public ExtSyntaxSupport(BaseDocument doc) {
        super(doc);
        this.docL = new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent evt) {
                ExtSyntaxSupport.this.documentModified(evt);
            }

            @Override
            public void removeUpdate(DocumentEvent evt) {
                ExtSyntaxSupport.this.documentModified(evt);
            }

            @Override
            public void changedUpdate(DocumentEvent evt) {
            }
        };
        this.getDocument().addDocumentListener(this.docL);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenItem getTokenChain(int startOffset, int endOffset) throws BadLocationException {
        TokenItem chain;
        if (startOffset < 0) {
            throw new IllegalArgumentException("startOffset=" + startOffset + " < 0");
        }
        if (startOffset > endOffset) {
            throw new IllegalArgumentException("startOffset=" + startOffset + " > endOffset=" + endOffset);
        }
        chain = null;
        BaseDocument doc = this.getDocument();
        doc.readLock();
        try {
            int docLen = doc.getLength();
            endOffset = Math.min(endOffset, docLen);
            if (startOffset < docLen) {
                TokenItemTP tp = new TokenItemTP();
                tp.targetOffset = endOffset;
                this.tokenizeText(tp, startOffset, endOffset, false);
                chain = tp.getTokenChain();
            }
        }
        finally {
            doc.readUnlock();
        }
        return chain;
    }

    protected void documentModified(DocumentEvent evt) {
        if (this.localVarMaps.size() > 0) {
            this.localVarMaps.clear();
        }
        if (this.globalVarMaps.size() > 0) {
            this.globalVarMaps.clear();
        }
    }

    protected BracketFinder getMatchingBracketFinder(char bracketChar) {
        BracketFinder bf = new BracketFinder(bracketChar);
        if (bf.moveCount == 0) {
            bf = null;
        }
        return bf;
    }

    public int[] findMatchingBlock(int offset, boolean simpleSearch) throws BadLocationException {
        int[] arrn;
        char bracketChar = this.getDocument().getChars(offset, 1)[0];
        int foundPos = -1;
        final BracketFinder bf = this.getMatchingBracketFinder(bracketChar);
        if (bf != null) {
            if (!simpleSearch) {
                TokenID tokenID = this.getTokenID(offset);
                TokenID[] bst = this.getBracketSkipTokens();
                for (int i = bst.length - 1; i >= 0; --i) {
                    if (tokenID != bst[i]) continue;
                    simpleSearch = true;
                    break;
                }
            }
            if (simpleSearch) {
                foundPos = bf.isForward() ? this.getDocument().find(bf, offset, -1) : this.getDocument().find(bf, offset + 1, 0);
            } else {
                TextBatchProcessor tbp = new TextBatchProcessor(){

                    @Override
                    public int processTextBatch(BaseDocument doc, int startPos, int endPos, boolean lastBatch) {
                        try {
                            int[] blks = ExtSyntaxSupport.this.getTokenBlocks(startPos, endPos, ExtSyntaxSupport.this.getBracketSkipTokens());
                            return ExtSyntaxSupport.this.findOutsideBlocks(bf, startPos, endPos, blks);
                        }
                        catch (BadLocationException e) {
                            return -1;
                        }
                    }
                };
                foundPos = bf.isForward() ? this.getDocument().processText(tbp, offset, -1) : this.getDocument().processText(tbp, offset + 1, 0);
            }
        }
        if (foundPos != -1) {
            int[] arrn2 = new int[2];
            arrn2[0] = foundPos;
            arrn = arrn2;
            arrn2[1] = foundPos + 1;
        } else {
            arrn = null;
        }
        return arrn;
    }

    protected TokenID[] getBracketSkipTokens() {
        return EMPTY_TOKEN_ID_ARRAY;
    }

    public TokenID getTokenID(int offset) throws BadLocationException {
        FirstTokenTP fttp = new FirstTokenTP();
        this.tokenizeText(fttp, offset, this.getDocument().getLength(), true);
        return fttp.getTokenID();
    }

    public int[] getFunctionBlock(int[] identifierBlock) throws BadLocationException {
        int nwPos;
        if (identifierBlock != null && (nwPos = Utilities.getFirstNonWhiteFwd(this.getDocument(), identifierBlock[1])) >= 0 && this.getDocument().getChars(nwPos, 1)[0] == '(') {
            return new int[]{identifierBlock[0], nwPos + 1};
        }
        return null;
    }

    public int[] getFunctionBlock(int offset) throws BadLocationException {
        return this.getFunctionBlock(Utilities.getIdentifierBlock(this.getDocument(), offset));
    }

    public boolean isWhitespaceToken(TokenID tokenID, char[] buffer, int offset, int tokenLength) {
        return Analyzer.isWhitespace(buffer, offset, tokenLength);
    }

    public boolean isCommentOrWhitespace(int startPos, int endPos) throws BadLocationException {
        CommentOrWhitespaceTP tp = new CommentOrWhitespaceTP(this.getCommentTokens());
        this.tokenizeText(tp, startPos, endPos, true);
        return !tp.nonEmpty;
    }

    public int getRowLastValidChar(int offset) throws BadLocationException {
        return Utilities.getRowLastNonWhite(this.getDocument(), offset);
    }

    public boolean isRowValid(int offset) throws BadLocationException {
        return Utilities.isRowWhite(this.getDocument(), offset);
    }

    public TokenID[] getCommentTokens() {
        return EMPTY_TOKEN_ID_ARRAY;
    }

    public int[] getCommentBlocks(int startPos, int endPos) throws BadLocationException {
        return this.getTokenBlocks(startPos, endPos, this.getCommentTokens());
    }

    public Object findType(String varName, int varPos) {
        Object type = null;
        Map varMap = this.getLocalVariableMap(varPos);
        if (varMap != null) {
            type = varMap.get(varName);
        }
        if (type == null && (varMap = this.getGlobalVariableMap(varPos)) != null) {
            type = varMap.get(varName);
        }
        return type;
    }

    public Map getLocalVariableMap(int offset) {
        Integer posI = new Integer(offset);
        Map varMap = (Map)this.localVarMaps.get(posI);
        if (varMap == null) {
            varMap = this.buildLocalVariableMap(offset);
            this.localVarMaps.put(posI, varMap);
        }
        return varMap;
    }

    protected Map buildLocalVariableMap(int offset) {
        int methodStartPos = this.getMethodStartPosition(offset);
        if (methodStartPos >= 0 && methodStartPos < offset) {
            VariableMapTokenProcessor vmtp = this.createVariableMapTokenProcessor(methodStartPos, offset);
            try {
                this.tokenizeText(vmtp, methodStartPos, offset, true);
                return vmtp.getVariableMap();
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        return null;
    }

    public Map getGlobalVariableMap(int offset) {
        Integer posI = new Integer(offset);
        Map varMap = (Map)this.globalVarMaps.get(posI);
        if (varMap == null) {
            varMap = this.buildGlobalVariableMap(offset);
            this.globalVarMaps.put(posI, varMap);
        }
        return varMap;
    }

    protected Map buildGlobalVariableMap(int offset) {
        int docLen = this.getDocument().getLength();
        VariableMapTokenProcessor vmtp = this.createVariableMapTokenProcessor(0, docLen);
        if (vmtp != null) {
            try {
                this.tokenizeText(vmtp, 0, docLen, true);
                return vmtp.getVariableMap();
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        return null;
    }

    protected int getMethodStartPosition(int offset) {
        return 0;
    }

    public int findDeclarationPosition(String varName, int varPos) {
        int offset = this.findLocalDeclarationPosition(varName, varPos);
        if (offset < 0) {
            offset = this.findGlobalDeclarationPosition(varName, varPos);
        }
        return offset;
    }

    public int findLocalDeclarationPosition(String varName, int varPos) {
        int methodStartPos = this.getMethodStartPosition(varPos);
        if (methodStartPos >= 0 && methodStartPos < varPos) {
            return this.findDeclarationPositionImpl(varName, methodStartPos, varPos);
        }
        return -1;
    }

    public int findGlobalDeclarationPosition(String varName, int varPos) {
        return this.findDeclarationPositionImpl(varName, 0, this.getDocument().getLength());
    }

    private int findDeclarationPositionImpl(String varName, int startPos, int endPos) {
        DeclarationTokenProcessor dtp = this.createDeclarationTokenProcessor(varName, startPos, endPos);
        if (dtp != null) {
            try {
                this.tokenizeText(dtp, startPos, endPos, true);
                return dtp.getDeclarationPosition();
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        return -1;
    }

    protected DeclarationTokenProcessor createDeclarationTokenProcessor(String varName, int startPos, int endPos) {
        return null;
    }

    protected VariableMapTokenProcessor createVariableMapTokenProcessor(int startPos, int endPos) {
        return null;
    }

    public int checkCompletion(JTextComponent target, String typedText, boolean visible) {
        return visible ? 4 : 1;
    }

    static class FirstTokenTP
    implements TokenProcessor {
        private TokenID tokenID;

        FirstTokenTP() {
        }

        public TokenID getTokenID() {
            return this.tokenID;
        }

        @Override
        public boolean token(TokenID tokenID, TokenContextPath tokenContextPath, int offset, int tokenLen) {
            this.tokenID = tokenID;
            return false;
        }

        @Override
        public int eot(int offset) {
            return 0;
        }

        @Override
        public void nextBuffer(char[] buffer, int offset, int len, int startPos, int preScan, boolean lastBuffer) {
        }
    }

    class CommentOrWhitespaceTP
    implements TokenProcessor {
        private char[] buffer;
        private TokenID[] commentTokens;
        boolean nonEmpty;

        CommentOrWhitespaceTP(TokenID[] commentTokens) {
            this.commentTokens = commentTokens;
        }

        @Override
        public boolean token(TokenID tokenID, TokenContextPath tokenContextPath, int offset, int tokenLength) {
            for (int i = 0; i < this.commentTokens.length; ++i) {
                if (tokenID != this.commentTokens[i]) continue;
                return true;
            }
            boolean nonWS = ExtSyntaxSupport.this.isWhitespaceToken(tokenID, this.buffer, offset, tokenLength);
            if (nonWS) {
                this.nonEmpty = true;
            }
            return nonWS;
        }

        @Override
        public int eot(int offset) {
            return 0;
        }

        @Override
        public void nextBuffer(char[] buffer, int offset, int len, int startPos, int preScan, boolean lastBuffer) {
            this.buffer = buffer;
        }
    }

    final class TokenItemTP
    implements TokenProcessor {
        private Item firstItem;
        private Item lastItem;
        private int fwdBatchLineCnt;
        private int bwdBatchLineCnt;
        private char[] buffer;
        private int bufferStartPos;
        int targetOffset;

        TokenItemTP() {
            this.fwdBatchLineCnt = this.bwdBatchLineCnt = ((Integer)ExtSyntaxSupport.this.getDocument().getProperty("line-batch-size")).intValue();
        }

        public TokenItem getTokenChain() {
            return this.firstItem;
        }

        @Override
        public boolean token(TokenID tokenID, TokenContextPath tokenContextPath, int tokenBufferOffset, int tokenLength) {
            if (this.bufferStartPos + tokenBufferOffset >= this.targetOffset) {
                return false;
            }
            this.lastItem = new Item(tokenID, tokenContextPath, this.bufferStartPos + tokenBufferOffset, new String(this.buffer, tokenBufferOffset, tokenLength), this.lastItem);
            if (this.firstItem == null) {
                this.firstItem = this.lastItem;
            }
            return true;
        }

        @Override
        public int eot(int offset) {
            return (Integer)ExtSyntaxSupport.this.getDocument().getProperty("mark-distance");
        }

        @Override
        public void nextBuffer(char[] buffer, int offset, int len, int startPos, int preScan, boolean lastBuffer) {
            this.buffer = buffer;
            this.bufferStartPos = startPos - offset;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Item getNextChunk(Item i) {
            int endPos;
            int docLen;
            BaseDocument doc = ExtSyntaxSupport.this.getDocument();
            int itemEndPos = i.getOffset() + i.getImage().length();
            if (itemEndPos == (docLen = doc.getLength())) {
                return null;
            }
            try {
                endPos = Utilities.getRowStart(doc, itemEndPos, this.fwdBatchLineCnt);
            }
            catch (BadLocationException e) {
                return null;
            }
            if (endPos == -1) {
                endPos = docLen;
            }
            this.fwdBatchLineCnt *= 2;
            Item nextChunkHead = null;
            Item fit = this.firstItem;
            Item lit = this.lastItem;
            try {
                this.firstItem = null;
                this.lastItem = null;
                this.targetOffset = endPos;
                ExtSyntaxSupport.this.tokenizeText(this, itemEndPos, endPos, false);
                nextChunkHead = this.firstItem;
            }
            catch (BadLocationException e) {}
            finally {
                if (this.firstItem != null) {
                    lit.next = this.firstItem;
                    this.firstItem.previous = lit;
                }
                this.firstItem = fit;
                if (this.lastItem == null) {
                    this.lastItem = lit;
                }
            }
            return nextChunkHead;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Item getPreviousChunk(Item i) {
            int startPos;
            BaseDocument doc = ExtSyntaxSupport.this.getDocument();
            int itemStartPos = i.getOffset();
            if (itemStartPos == 0) {
                return null;
            }
            try {
                startPos = Utilities.getRowStart(doc, itemStartPos, - this.bwdBatchLineCnt);
            }
            catch (BadLocationException e) {
                return null;
            }
            if (startPos == -1) {
                startPos = 0;
            }
            this.bwdBatchLineCnt *= 2;
            Item previousChunkLast = null;
            Item fit = this.firstItem;
            Item lit = this.lastItem;
            try {
                this.firstItem = null;
                this.lastItem = null;
                this.targetOffset = itemStartPos;
                ExtSyntaxSupport.this.tokenizeText(this, startPos, itemStartPos, false);
                previousChunkLast = this.lastItem;
            }
            catch (BadLocationException e) {}
            finally {
                if (this.lastItem != null) {
                    fit.previous = this.lastItem;
                    this.lastItem.next = fit;
                }
                this.lastItem = lit;
                if (this.firstItem == null) {
                    this.firstItem = fit;
                }
            }
            return previousChunkLast;
        }

        final class Item
        extends TokenItem.AbstractItem {
            Item previous;
            TokenItem next;

            Item(TokenID tokenID, TokenContextPath tokenContextPath, int offset, String image, Item previous) {
                super(tokenID, tokenContextPath, offset, image);
                if (previous != null) {
                    this.previous = previous;
                    previous.next = this;
                }
            }

            @Override
            public TokenItem getNext() {
                if (this.next == null) {
                    this.next = TokenItemTP.this.getNextChunk(this);
                }
                return this.next;
            }

            @Override
            public TokenItem getPrevious() {
                if (this.previous == null) {
                    this.previous = TokenItemTP.this.getPreviousChunk(this);
                }
                return this.previous;
            }
        }

    }

    public class BracketFinder
    extends FinderFactory.GenericFinder {
        protected char bracketChar;
        protected char matchChar;
        private int depth;
        protected int moveCount;

        public BracketFinder(char bracketChar) {
            this.bracketChar = bracketChar;
            this.updateStatus();
            this.forward = this.moveCount > 0;
        }

        protected boolean updateStatus() {
            boolean valid = true;
            switch (this.bracketChar) {
                case '(': {
                    this.matchChar = 41;
                    this.moveCount = 1;
                    break;
                }
                case ')': {
                    this.matchChar = 40;
                    this.moveCount = -1;
                    break;
                }
                case '{': {
                    this.matchChar = 125;
                    this.moveCount = 1;
                    break;
                }
                case '}': {
                    this.matchChar = 123;
                    this.moveCount = -1;
                    break;
                }
                case '[': {
                    this.matchChar = 93;
                    this.moveCount = 1;
                    break;
                }
                case ']': {
                    this.matchChar = 91;
                    this.moveCount = -1;
                    break;
                }
                case '<': {
                    this.matchChar = 62;
                    this.moveCount = 1;
                    break;
                }
                case '>': {
                    this.matchChar = 60;
                    this.moveCount = -1;
                    break;
                }
                default: {
                    valid = false;
                }
            }
            return valid;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (ch == this.bracketChar) {
                ++this.depth;
            } else if (ch == this.matchChar && --this.depth == 0) {
                this.found = true;
                return 0;
            }
            return this.moveCount;
        }
    }

    public static interface VariableMapTokenProcessor
    extends TokenProcessor {
        public Map getVariableMap();
    }

    public static interface DeclarationTokenProcessor
    extends TokenProcessor {
        public int getDeclarationPosition();
    }

}

