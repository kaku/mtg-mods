/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class KeywordMatchGenerator {
    private static final String USAGE = "Usage: java org.netbeans.editor.ext.KeywordMatchGenerator [options] keyword-file [match-function-name]\n\nOptions:\n  -i Ignore case in matching\n  -s Input is in 'input' String or StringBuffer instead of char buffer\n\nGenerator of method that matches the keywords provided in the file.\nKeywords in the file must be separated by spaces or new-lines and they don't need to be sorted.\n";
    private static final String UNKNOWN_OPTION = " is unknown option.\n";
    public static final String IGNORE_CASE = "-i";
    public static final String USE_STRING = "-s";
    private static final String DEFAULT_METHOD_NAME = "match";
    private static final String[] OPTION_LIST = new String[]{"-i", "-s"};
    private String[] kwds;
    private int maxKwdLen;
    private HashMap options = new HashMap();
    private HashMap kwdConstants = new HashMap();

    private String indent(int cnt) {
        StringBuffer sb = new StringBuffer();
        while (cnt-- > 0) {
            sb.append("  ");
        }
        return sb.toString();
    }

    protected void initScan(String methodName) {
        if (methodName == null) {
            methodName = "match";
        }
        this.appendString("\n");
        for (int i = 0; i < this.kwds.length; ++i) {
            this.appendString(this.indent(1) + "public static final int " + this.kwdConstants.get(this.kwds[i]) + " = " + i + ";\n");
        }
        this.appendString("\n");
        this.appendString(this.indent(1) + "public static int ");
        this.appendString(methodName);
        if (this.options.get("-s") != null) {
            this.appendString("(String buffer, int offset, int len) {\n");
        } else {
            this.appendString("(char[] buffer, int offset, int len) {\n");
        }
        this.appendString(this.indent(2) + "if (len > " + this.maxKwdLen + ")\n");
        this.appendString(this.indent(3) + "return -1;\n");
    }

    public void scan() {
        this.scan(0, this.kwds.length, 0, 2, 0);
    }

    protected void finishScan() {
        this.appendString(this.indent(1) + "}\n\n");
    }

    public void addOption(String option) {
        this.options.put(option, option);
    }

    protected String getKwdConstantPrefix() {
        return "";
    }

    protected String getKwdConstant(String kwd) {
        return (String)this.kwdConstants.get(kwd);
    }

    protected boolean upperCaseKeyConstants() {
        return true;
    }

    private void parseKeywords(String s) {
        ArrayList<String> keyList = new ArrayList<String>();
        StringTokenizer strTok = new StringTokenizer(s);
        try {
            do {
                String key = strTok.nextToken();
                int keyLen = key.length();
                this.maxKwdLen = Math.max(this.maxKwdLen, keyLen);
                keyList.add(key);
                this.kwdConstants.put(key, this.getKwdConstantPrefix() + (this.upperCaseKeyConstants() ? key.toUpperCase() : key));
            } while (true);
        }
        catch (NoSuchElementException e) {
            this.kwds = new String[keyList.size()];
            keyList.toArray(this.kwds);
            Arrays.sort(this.kwds);
            return;
        }
    }

    protected String getCurrentChar() {
        boolean ignoreCase;
        boolean useString = this.options.get("-s") != null;
        boolean bl = ignoreCase = this.options.get("-i") != null;
        if (useString) {
            return ignoreCase ? "Character.toLowerCase(buffer.charAt(offset++))" : "buffer.charAt(offset++)";
        }
        return ignoreCase ? "Character.toLowerCase(buffer[offset++])" : "buffer[offset++]";
    }

    private void appendCheckedReturn(String kwd, int offset, int indent) {
        this.appendString(this.indent(indent) + "return (len == " + kwd.length());
        int kwdLenM1 = kwd.length() - 1;
        for (int k = offset; k <= kwdLenM1; ++k) {
            this.appendString("\n" + this.indent(indent + 1) + "&& ");
            this.appendString(this.getCurrentChar() + " == '" + kwd.charAt(k) + "'");
        }
        this.appendString(")\n" + this.indent(indent + 2) + "? " + this.getKwdConstant(kwd) + " : -1;\n");
    }

    protected void appendString(String s) {
        System.out.print(s);
    }

    private void scan(int indFrom, int indTo, int offset, int indent, int minKwdLen) {
        int minLen;
        int same;
        int i;
        int maxLen = 0;
        for (int i2 = indFrom; i2 < indTo; ++i2) {
            maxLen = Math.max(maxLen, this.kwds[i2].length());
        }
        do {
            int i3;
            minLen = Integer.MAX_VALUE;
            for (i = indFrom; i < indTo; ++i) {
                minLen = Math.min(minLen, this.kwds[i].length());
            }
            if (minLen > minKwdLen) {
                this.appendString(this.indent(indent) + "if (len <= " + (minLen - 1) + ")\n");
                this.appendString(this.indent(indent + 1) + "return -1;\n");
            }
            same = 0;
            boolean stop = false;
            for (i3 = offset; i3 < minLen; ++i3) {
                char c = this.kwds[indFrom].charAt(i3);
                for (int j = indFrom + 1; j < indTo; ++j) {
                    if (this.kwds[j].charAt(i3) == c) continue;
                    stop = true;
                    break;
                }
                if (stop) break;
                ++same;
            }
            if (same > 0) {
                this.appendString(this.indent(indent) + "if (");
                for (i3 = 0; i3 < same; ++i3) {
                    if (i3 > 0) {
                        this.appendString(this.indent(indent + 1) + "|| ");
                    }
                    this.appendString(this.getCurrentChar() + " != '" + this.kwds[indFrom].charAt(offset + i3) + "'");
                    if (i3 >= same - 1) continue;
                    this.appendString("\n");
                }
                this.appendString(")\n" + this.indent(indent + 2) + "return -1;\n");
            }
            if ((offset += same) == this.kwds[indFrom].length()) {
                this.appendString(this.indent(indent) + "if (len == " + offset + ")\n");
                this.appendString(this.indent(indent + 1) + "return " + this.getKwdConstant(this.kwds[indFrom]) + ";\n");
                ++indFrom;
                if (offset >= minLen) {
                    minLen = offset + 1;
                }
            }
            minKwdLen = minLen;
        } while (same > 0 && indFrom < indTo);
        if (offset < maxLen) {
            this.appendString(this.indent(indent) + "switch (" + this.getCurrentChar() + ") {\n");
            i = indFrom;
            while (i < indTo) {
                int subGroupEndInd;
                char actChar = this.kwds[i].charAt(offset);
                this.appendString(this.indent(indent + 1) + "case '" + actChar + "':\n");
                for (subGroupEndInd = i + 1; subGroupEndInd < indTo && this.kwds[subGroupEndInd].length() > offset && this.kwds[subGroupEndInd].charAt(offset) == actChar; ++subGroupEndInd) {
                }
                if (subGroupEndInd > i + 1) {
                    this.scan(i, subGroupEndInd, offset + 1, indent + 2, minLen);
                } else {
                    this.appendCheckedReturn(this.kwds[i], offset + 1, indent + 2);
                }
                i = subGroupEndInd;
            }
            this.appendString(this.indent(indent + 1) + "default:\n");
            this.appendString(this.indent(indent + 2) + "return -1;\n");
            this.appendString(this.indent(indent) + "}\n");
        } else {
            this.appendString(this.indent(indent) + "return -1;\n");
        }
    }

    public static void main(String[] args) {
        int argShift;
        KeywordMatchGenerator km = new KeywordMatchGenerator();
        for (argShift = 0; argShift < args.length && args[argShift].charAt(0) == '-'; ++argShift) {
            int j;
            for (j = 0; j < OPTION_LIST.length; ++j) {
                if (!args[argShift].equals(OPTION_LIST[j])) continue;
                km.addOption(OPTION_LIST[j]);
                break;
            }
            if (j != OPTION_LIST.length) continue;
            System.err.println("'" + args[argShift] + "'" + " is unknown option.\n");
            System.err.println("Usage: java org.netbeans.editor.ext.KeywordMatchGenerator [options] keyword-file [match-function-name]\n\nOptions:\n  -i Ignore case in matching\n  -s Input is in 'input' String or StringBuffer instead of char buffer\n\nGenerator of method that matches the keywords provided in the file.\nKeywords in the file must be separated by spaces or new-lines and they don't need to be sorted.\n");
            return;
        }
        if (args.length - argShift < 1) {
            System.err.println("Usage: java org.netbeans.editor.ext.KeywordMatchGenerator [options] keyword-file [match-function-name]\n\nOptions:\n  -i Ignore case in matching\n  -s Input is in 'input' String or StringBuffer instead of char buffer\n\nGenerator of method that matches the keywords provided in the file.\nKeywords in the file must be separated by spaces or new-lines and they don't need to be sorted.\n");
            return;
        }
        String kwds = null;
        try {
            int count;
            File f = new File(args[argShift]);
            if (!f.exists()) {
                System.err.println("Non-existent file '" + args[argShift] + "'");
                return;
            }
            char[] arr = new char[(int)f.length()];
            FileReader isr = new FileReader(f);
            int n = 0;
            while ((long)n < f.length() && (count = isr.read(arr, n, (int)f.length() - n)) >= 0) {
                n += count;
            }
            kwds = new String(arr);
        }
        catch (IOException e) {
            System.err.println("Cannot read from keyword file '" + args[argShift] + "'");
            return;
        }
        String methodName = null;
        if (args.length - argShift >= 2) {
            methodName = args[argShift + 1];
        }
        km.parseKeywords(kwds);
        km.initScan(methodName);
        km.scan();
        km.finishScan();
    }
}

