/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.ext;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.ext.DataAccessor;
import org.netbeans.editor.ext.FileAccessor;
import org.netbeans.editor.ext.StringCache;

public class FileStorage {
    private static final int MAX_STRING = 60000;
    private static final int BYTES_INCREMENT = 2048;
    private static final byte[] EMPTY_BYTES = new byte[0];
    Thread currentLock;
    protected boolean openedForWrite;
    public boolean fileNotFound = false;
    protected DataAccessor da;
    protected boolean opened = false;
    protected int offset;
    protected byte[] bytes = EMPTY_BYTES;
    char[] chars = Analyzer.EMPTY_CHAR_ARRAY;
    StringCache strCache;
    private int lockDeep;
    private static final String WRITE_LOCK_MISSING = "Unlock file without previous lock file";
    private int version = 1;
    private static final int BIT7 = 128;
    private static final int BIT6 = 64;
    private static final int BIT5 = 32;

    public FileStorage(String fileName) {
        this(fileName, new StringCache());
    }

    public FileStorage(String fileName, StringCache strCache) {
        this.da = new FileAccessor(new File(fileName));
        this.strCache = strCache;
    }

    public FileStorage(DataAccessor da, StringCache strCache) {
        this.da = da;
        this.strCache = strCache;
    }

    public void setVersion(int ver) {
        this.version = ver;
    }

    public void open(boolean requestWrite) throws IOException {
        if (this.openedForWrite == requestWrite) {
            this.ensureOpen(requestWrite);
            this.da.seek(this.getFileLength());
            return;
        }
        this.close();
        this.ensureOpen(requestWrite);
        this.da.seek(this.getFileLength());
        this.openedForWrite = requestWrite;
        this.offset = 0;
    }

    private void ensureOpen(boolean requestWrite) throws IOException {
        if (!this.opened) {
            this.da.open(requestWrite);
            this.opened = true;
        }
    }

    public void close() throws IOException {
        this.opened = false;
        this.da.close();
    }

    protected void checkBytesSize(int len) {
        if (this.bytes.length < len) {
            byte[] newBytes = new byte[len + 2048];
            System.arraycopy(this.bytes, 0, newBytes, 0, this.bytes.length);
            this.bytes = newBytes;
        }
    }

    public void read(int len) throws IOException {
        this.checkBytesSize(len);
        this.da.read(this.bytes, 0, len);
        this.offset = 0;
    }

    public void write() throws IOException {
        if (this.offset > 0) {
            this.da.append(this.bytes, 0, this.offset);
        }
        this.offset = 0;
    }

    public void seek(int filePointer) throws IOException {
        this.da.seek(filePointer);
    }

    public String getFileName() {
        return this.da.toString();
    }

    public int getFilePointer() throws IOException {
        return (int)this.da.getFilePointer();
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return this.offset;
    }

    public int getFileLength() throws IOException {
        return this.da.getFileLength();
    }

    public void resetBytes() {
        this.bytes = EMPTY_BYTES;
    }

    public void resetFile() throws IOException {
        this.open(true);
        this.offset = 0;
        this.da.resetFile();
        this.close();
    }

    public int getInteger() {
        if (this.version == 1) {
            int i = this.bytes[this.offset++];
            i = (i << 8) + (this.bytes[this.offset++] & 255);
            i = (i << 8) + (this.bytes[this.offset++] & 255);
            i = (i << 8) + (this.bytes[this.offset++] & 255);
            return i;
        }
        if (this.version == 2) {
            return this.decodeInteger();
        }
        return 0;
    }

    public String getString() {
        int len = this.getInteger();
        if (len < 0) {
            throw new RuntimeException("Consistency error: read string length=" + len);
        }
        if (len > 60000) {
            throw new RuntimeException("FileStorage: String len is " + len + ". There's probably a corruption in the file '" + this.getFileName() + "'.");
        }
        if (this.version == 1) {
            if (this.chars.length < len) {
                this.chars = new char[2 * len];
            }
            for (int i = 0; i < len; ++i) {
                this.chars[i] = (char)((this.bytes[this.offset] << 8) + (this.bytes[this.offset + 1] & 255));
                this.offset += 2;
            }
            String s = null;
            if (len >= 0) {
                s = this.strCache != null ? this.strCache.getString(this.chars, 0, len) : new String(this.chars, 0, len);
            }
            return s;
        }
        if (this.version == 2) {
            try {
                String s = new String(this.bytes, this.offset, len, this.getEncoding());
                this.offset += len;
                return s;
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }
            catch (ArrayIndexOutOfBoundsException ex) {
                StringBuffer sb = new StringBuffer(len);
                for (int i = 0; i < len; ++i) {
                    sb.append(this.bytes[this.offset + i]);
                }
                String st = sb.toString();
                throw new RuntimeException("Debug of #12932: If this bug occurs, please send the stacktrace as attachment to Issuezilla's #12932.\nhttp://www.netbeans.org/issues/show_bug.cgi?id=12932\ndebug 2\nFile:" + this.toString() + "\n" + "File Version:" + this.version + "\n" + "Offest: " + this.offset + "\n" + "Read length: " + len + "\n" + "bytes.length: " + this.bytes.length + "\n" + "String:" + st + "\n" + "Error:" + ex);
            }
        }
        return "";
    }

    public void putInteger(int i) {
        if (this.version == 1) {
            this.checkBytesSize(this.offset + 4);
            this.bytes[this.offset + 3] = (byte)(i & 255);
            this.bytes[this.offset + 2] = (byte)((i >>>= 8) & 255);
            this.bytes[this.offset + 1] = (byte)((i >>>= 8) & 255);
            this.bytes[this.offset] = (byte)(i >>>= 8);
            this.offset += 4;
        }
        if (this.version == 2) {
            this.encodeInteger(i);
        }
    }

    public void putString(String s) {
        if (s == null) {
            return;
        }
        if (this.version == 1) {
            int len = s.length();
            this.putInteger(len);
            if (len > 0) {
                this.checkBytesSize(this.offset + len * 2);
                for (int i = 0; i < len; ++i) {
                    char ch = s.charAt(i);
                    this.bytes[this.offset + 1] = (byte)(ch & 255);
                    ch = (char)(ch >>> 8);
                    this.bytes[this.offset] = (byte)(ch & 255);
                    this.offset += 2;
                }
            }
        } else if (this.version == 2) {
            byte[] encodedBytes;
            try {
                encodedBytes = s.getBytes(this.getEncoding());
            }
            catch (UnsupportedEncodingException e) {
                return;
            }
            int len = Array.getLength(encodedBytes);
            if (len < 0) {
                return;
            }
            this.putInteger(len);
            this.checkBytesSize(this.offset + len);
            System.arraycopy(encodedBytes, 0, this.bytes, this.offset, len);
            this.offset += len;
        }
    }

    private int decodeInteger() {
        int i;
        if (((i = this.bytes[this.offset++] & 255) & 128) == 0) {
            return i;
        }
        int level = 1;
        if ((i & 64) != 0) {
            level += 2;
        }
        if ((i & 32) != 0) {
            ++level;
        }
        i &= -225;
        for (int j = 1; j <= level; ++j) {
            i = (i << 8) + (this.bytes[this.offset++] & 255);
        }
        return i;
    }

    private void encodeInteger(int y) {
        int level = 0;
        if (y >= 536870912) {
            level += 4;
        } else if (y >= 2097152) {
            level += 3;
        } else if (y >= 8192) {
            level += 2;
        } else if (y >= 128) {
            ++level;
        }
        this.checkBytesSize(this.offset + level + 1);
        for (int j = level; j > 0; --j) {
            this.bytes[this.offset + j] = (byte)(y & 255);
            y >>>= 8;
        }
        this.bytes[this.offset] = (byte)y;
        switch (level) {
            case 2: {
                byte[] arrby = this.bytes;
                int n = this.offset;
                arrby[n] = (byte)(arrby[n] | 32);
                break;
            }
            case 3: {
                byte[] arrby = this.bytes;
                int n = this.offset;
                arrby[n] = (byte)(arrby[n] | 64);
                break;
            }
            case 4: {
                byte[] arrby = this.bytes;
                int n = this.offset;
                arrby[n] = (byte)(arrby[n] | 32);
                byte[] arrby2 = this.bytes;
                int n2 = this.offset;
                arrby2[n2] = (byte)(arrby2[n2] | 64);
            }
        }
        if (level > 0) {
            byte[] arrby = this.bytes;
            int n = this.offset;
            arrby[n] = (byte)(arrby[n] | 128);
        }
        this.offset += ++level;
    }

    private String getEncoding() {
        switch (this.version) {
            case 1: {
                return "UTF-16BE";
            }
            case 2: {
                return "UTF-8";
            }
        }
        return "UTF-16BE";
    }

    public final synchronized void lockFile() {
        if (this.currentLock == null || Thread.currentThread() != this.currentLock) {
            try {
                if (this.currentLock == null) {
                    this.currentLock = Thread.currentThread();
                    this.lockDeep = 0;
                }
                this.wait();
            }
            catch (InterruptedException ie) {
                throw new RuntimeException(ie.toString());
            }
            catch (IllegalMonitorStateException imse) {
                throw new RuntimeException(imse.toString());
            }
        } else {
            ++this.lockDeep;
        }
    }

    public final synchronized void unlockFile() {
        if (Thread.currentThread() != this.currentLock) {
            throw new RuntimeException("Unlock file without previous lock file");
        }
        if (this.lockDeep == 0) {
            this.resetBytes();
            this.notify();
            this.currentLock = null;
        } else {
            --this.lockDeep;
        }
    }

    public String toString() {
        return this.getFileName();
    }
}

