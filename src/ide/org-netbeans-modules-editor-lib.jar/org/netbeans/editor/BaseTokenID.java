/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenID;

public class BaseTokenID
implements TokenID {
    private final String name;
    private final int numericID;
    private final TokenCategory category;

    public BaseTokenID(String name) {
        this(name, 0);
    }

    public BaseTokenID(String name, int numericID) {
        this(name, numericID, null);
    }

    public BaseTokenID(String name, TokenCategory category) {
        this(name, 0, category);
    }

    public BaseTokenID(String name, int numericID, TokenCategory category) {
        this.name = name;
        this.numericID = numericID;
        this.category = category;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getNumericID() {
        return this.numericID;
    }

    @Override
    public TokenCategory getCategory() {
        return this.category;
    }

    public String toString() {
        return this.getName() + (this.getCategory() != null ? new StringBuilder().append(", category=").append(this.getCategory()).toString() : "");
    }
}

