/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.Utilities;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class KeySequenceInputPanel
extends JPanel {
    public static final String PROP_KEYSEQUENCE = "keySequence";
    private Vector strokes = new Vector();
    private StringBuffer text = new StringBuffer();
    private final ResourceBundle bundle = NbBundle.getBundle(BaseKit.class);
    public JTextArea collisionLabel;
    public JTextField keySequenceInputField;
    public JLabel keySequenceLabel;

    public KeySequenceInputPanel() {
        this.initComponents();
        this.keySequenceInputField.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_LBL_KSIP_Sequence"));
        this.getAccessibleContext().setAccessibleName(this.bundle.getString("MSP_AddTitle"));
        this.getAccessibleContext().setAccessibleDescription(this.bundle.getString("ACSD_KSIP"));
        this.keySequenceInputField.setFocusTraversalKeysEnabled(false);
    }

    public void clear() {
        this.strokes.clear();
        this.text.setLength(0);
        this.keySequenceInputField.setText(this.text.toString());
        this.firePropertyChange("keySequence", null, null);
    }

    public void setInfoText(String s) {
        this.collisionLabel.setText(s + ' ');
    }

    public KeyStroke[] getKeySequence() {
        return this.strokes.toArray(new KeyStroke[0]);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dim = super.getPreferredSize();
        if (dim.width < 400) {
            dim.width = 400;
        }
        return dim;
    }

    @Override
    public void requestFocus() {
        this.keySequenceInputField.requestFocus();
    }

    private void initComponents() {
        this.keySequenceLabel = new JLabel();
        this.keySequenceInputField = new JTextField();
        this.collisionLabel = new JTextArea();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 11, 11));
        this.setLayout(new GridBagLayout());
        this.keySequenceLabel.setLabelFor(this.keySequenceInputField);
        Mnemonics.setLocalizedText((JLabel)this.keySequenceLabel, (String)this.bundle.getString("LBL_KSIP_Sequence"));
        this.keySequenceLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 8));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 0, 0, 12);
        this.add((Component)this.keySequenceLabel, gridBagConstraints);
        this.keySequenceInputField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyTyped(KeyEvent evt) {
                KeySequenceInputPanel.this.keySequenceInputFieldKeyTyped(evt);
            }

            @Override
            public void keyPressed(KeyEvent evt) {
                KeySequenceInputPanel.this.keySequenceInputFieldKeyPressed(evt);
            }

            @Override
            public void keyReleased(KeyEvent evt) {
                KeySequenceInputPanel.this.keySequenceInputFieldKeyReleased(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.keySequenceInputField, gridBagConstraints);
        this.collisionLabel.setLineWrap(true);
        this.collisionLabel.setEditable(false);
        this.collisionLabel.setRows(2);
        this.collisionLabel.setForeground(Color.red);
        this.collisionLabel.setBackground(this.getBackground());
        this.collisionLabel.setDisabledTextColor(Color.red);
        this.collisionLabel.setEnabled(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this.add((Component)this.collisionLabel, gridBagConstraints);
    }

    private void keySequenceInputFieldKeyTyped(KeyEvent evt) {
        evt.consume();
    }

    private void keySequenceInputFieldKeyReleased(KeyEvent evt) {
        evt.consume();
        this.keySequenceInputField.setText(this.text.toString());
    }

    private void keySequenceInputFieldKeyPressed(KeyEvent evt) {
        String inputText = this.keySequenceInputField.getText();
        if (evt.getModifiers() == 0 && KeyStroke.getKeyStroke(9, 0).equals(KeyStroke.getKeyStrokeForEvent(evt)) && inputText != null && inputText.length() > 0) {
            this.keySequenceInputField.transferFocus();
            return;
        }
        evt.consume();
        String modif = KeyEvent.getKeyModifiersText(evt.getModifiers());
        if (this.isModifier(evt.getKeyCode())) {
            this.keySequenceInputField.setText(this.text.toString() + modif + '+');
        } else {
            KeyStroke stroke = KeyStroke.getKeyStrokeForEvent(evt);
            this.strokes.add(stroke);
            this.text.append(Utilities.keyStrokeToString(stroke));
            this.text.append(' ');
            this.keySequenceInputField.setText(this.text.toString());
            this.firePropertyChange("keySequence", null, null);
        }
    }

    private boolean isModifier(int keyCode) {
        return keyCode == 18 || keyCode == 65406 || keyCode == 17 || keyCode == 16 || keyCode == 157;
    }

}

