/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.EventListener;
import javax.swing.event.EventListenerList;

public class WeakEventListenerList
extends EventListenerList {
    private int listenerSize;

    @Override
    public synchronized Object[] getListenerList() {
        int tgtInd = 0;
        Object[] ret = new Object[this.listenerSize];
        for (int i = 1; i < this.listenerSize; i += 2) {
            Object l = ((WeakReference)this.listenerList[i]).get();
            if (l != null) {
                ret[tgtInd++] = this.listenerList[i - 1];
                ret[tgtInd++] = l;
                continue;
            }
            System.arraycopy(this.listenerList, i + 1, this.listenerList, i - 1, this.listenerSize - i - 1);
            this.listenerSize -= 2;
            i -= 2;
        }
        if (ret.length != tgtInd) {
            Object[] tmp = new Object[tgtInd];
            System.arraycopy(ret, 0, tmp, 0, tgtInd);
            ret = tmp;
        }
        return ret;
    }

    public synchronized EventListener[] getListeners(Class t) {
        int tgtInd = 0;
        EventListener[] ret = (EventListener[])Array.newInstance(t, this.listenerSize);
        for (int i = 0; i < this.listenerSize; ++i) {
            if (this.listenerList[i++] != t) continue;
            EventListener l = (EventListener)((WeakReference)this.listenerList[i]).get();
            if (l != null) {
                ret[tgtInd++] = l;
                continue;
            }
            System.arraycopy(this.listenerList, i + 1, this.listenerList, i - 1, this.listenerSize - i - 1);
            this.listenerSize -= 2;
            i -= 2;
        }
        if (ret.length != tgtInd) {
            EventListener[] tmp = (EventListener[])Array.newInstance(t, tgtInd);
            System.arraycopy(ret, 0, tmp, 0, tgtInd);
            ret = tmp;
        }
        return ret;
    }

    public synchronized void add(Class t, EventListener l) {
        if (l == null) {
            return;
        }
        if (!t.isInstance(l)) {
            throw new IllegalArgumentException("Listener " + l + " is not of type " + t);
        }
        if (this.listenerSize == 0) {
            this.listenerList = new Object[]{t, new WeakReference<EventListener>(l)};
            this.listenerSize = 2;
        } else {
            if (this.listenerSize == this.listenerList.length) {
                Object[] tmp = new Object[this.listenerSize * 2];
                System.arraycopy(this.listenerList, 0, tmp, 0, this.listenerSize);
                this.listenerList = tmp;
            }
            this.listenerList[this.listenerSize++] = t;
            this.listenerList[this.listenerSize++] = new WeakReference<EventListener>(l);
        }
    }

    public synchronized void remove(Class t, EventListener l) {
        if (l == null) {
            return;
        }
        if (!t.isInstance(l)) {
            throw new IllegalArgumentException("Listener " + l + " is not of type " + t);
        }
        int index = -1;
        for (int i = this.listenerSize - 2; i >= 0; i -= 2) {
            if (this.listenerList[i] != t || l != ((WeakReference)this.listenerList[i + 1]).get()) continue;
            index = i;
            break;
        }
        if (index >= 0) {
            System.arraycopy(this.listenerList, index + 2, this.listenerList, index, this.listenerSize - index - 2);
            this.listenerSize -= 2;
        }
    }

    private synchronized void writeObject(ObjectOutputStream os) throws IOException {
        os.defaultWriteObject();
        for (int i = 0; i < this.listenerSize; i += 2) {
            Class t = (Class)this.listenerList[i];
            EventListener l = (EventListener)((WeakReference)this.listenerList[i + 1]).get();
            if (l == null || !(l instanceof Serializable)) continue;
            os.writeObject(t.getName());
            os.writeObject(l);
        }
        os.writeObject(null);
    }

    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {
        Object listenerTypeOrNull;
        is.defaultReadObject();
        while (null != (listenerTypeOrNull = is.readObject())) {
            EventListener l = (EventListener)is.readObject();
            this.add(Class.forName((String)listenerTypeOrNull), l);
        }
    }

    @Override
    public synchronized String toString() {
        StringBuffer sb = new StringBuffer("WeakEventListenerList: ");
        sb.append(this.listenerSize / 2);
        sb.append(" listeners:\n");
        for (int i = 0; i < this.listenerSize; i += 2) {
            sb.append(" type " + ((Class)this.listenerList[i]).getName());
            sb.append(" listener " + ((WeakReference)this.listenerList[i + 1]).get());
        }
        return sb.toString();
    }
}

