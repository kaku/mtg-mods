/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.TokenID;

public interface ImageTokenID
extends TokenID {
    public String getImage();
}

