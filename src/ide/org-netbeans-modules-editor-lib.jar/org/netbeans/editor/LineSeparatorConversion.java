/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharacterConversions
 */
package org.netbeans.editor;

import java.io.IOException;
import java.io.Reader;
import javax.swing.text.Segment;
import org.netbeans.lib.editor.util.CharacterConversions;

public class LineSeparatorConversion {
    public static final char LS = '\u2028';
    public static final char PS = '\u2029';
    public static final String LS_LS = String.valueOf('\u2028');
    public static final String LS_PS = String.valueOf('\u2029');
    private static final int DEFAULT_CONVERSION_BUFFER_SIZE = 16384;

    private LineSeparatorConversion() {
    }

    public static String convertToLineFeed(String text) {
        return CharacterConversions.lineSeparatorToLineFeed((CharSequence)text);
    }

    public static void convertToLineFeed(String text, int offset, int length, StringBuffer output) {
        String s = CharacterConversions.lineSeparatorToLineFeed((CharSequence)text.subSequence(offset, offset + length));
        output.append(s);
    }

    public static String convertFromLineFeed(String text, String lineFeedReplace) {
        return CharacterConversions.lineFeedToLineSeparator((CharSequence)text, (CharSequence)lineFeedReplace);
    }

    public static void convertFromLineFeed(String text, int offset, int length, String lineFeedReplace, StringBuffer output) {
        String s = CharacterConversions.lineFeedToLineSeparator((CharSequence)text.subSequence(offset, offset + length), (CharSequence)lineFeedReplace);
        output.append(s);
    }

    public static class InitialSeparatorReader
    extends Reader {
        private static final int AFTER_CR_STATUS = -1;
        private static final int INITIAL_STATUS = 0;
        private static final int CR_SEPARATOR = 1;
        private static final int LF_SEPARATOR = 2;
        private static final int CRLF_SEPARATOR = 3;
        private static final int LS_SEPARATOR = 4;
        private static final int PS_SEPARATOR = 5;
        private Reader delegate;
        private int status = 0;

        public InitialSeparatorReader(Reader delegate) {
            this.delegate = delegate;
        }

        public String getInitialSeparator() {
            String separator;
            switch (this.status) {
                case 1: {
                    separator = "\r";
                    break;
                }
                case 2: {
                    separator = "\n";
                    break;
                }
                case 3: {
                    separator = "\r\n";
                    break;
                }
                case -1: {
                    separator = "\r";
                    break;
                }
                case 4: {
                    separator = LineSeparatorConversion.LS_LS;
                    break;
                }
                case 5: {
                    separator = LineSeparatorConversion.LS_PS;
                    break;
                }
                default: {
                    separator = System.getProperty("line.separator");
                }
            }
            return separator;
        }

        private void resolveSeparator(char ch) {
            block0 : switch (this.status) {
                case 0: {
                    switch (ch) {
                        case '\r': {
                            this.status = -1;
                            break block0;
                        }
                        case '\n': {
                            this.status = 2;
                            break block0;
                        }
                        case '\u2028': {
                            this.status = 4;
                            break block0;
                        }
                        case '\u2029': {
                            this.status = 5;
                        }
                    }
                    break;
                }
                case -1: {
                    switch (ch) {
                        case '\n': {
                            this.status = 3;
                            break block0;
                        }
                    }
                    this.status = 1;
                    break;
                }
                default: {
                    switch (ch) {
                        case '\r': {
                            this.status = -1;
                            break block0;
                        }
                        case '\n': {
                            this.status = 2;
                        }
                    }
                }
            }
        }

        private boolean isSeparatorResolved() {
            return this.status > 0;
        }

        @Override
        public void close() throws IOException {
            if (this.delegate == null) {
                return;
            }
            this.delegate.close();
            this.delegate = null;
        }

        @Override
        public int read(char[] cbuf, int off, int len) throws IOException {
            if (this.delegate == null) {
                throw new IOException("Reader already closed.");
            }
            int readLen = this.delegate.read(cbuf, off, len);
            int endOff = off + readLen;
            while (off < endOff && !this.isSeparatorResolved()) {
                this.resolveSeparator(cbuf[off]);
                ++off;
            }
            return readLen;
        }

        @Override
        public int read() throws IOException {
            if (this.delegate == null) {
                throw new IOException("Reader already closed.");
            }
            int r = this.delegate.read();
            if (r != -1 && !this.isSeparatorResolved()) {
                this.resolveSeparator((char)r);
            }
            return r;
        }
    }

    public static class FromLineFeed {
        private Object charArrayOrSequence;
        private int offset;
        private int endOffset;
        private String lineFeedReplace;
        private Segment convertedText;

        public FromLineFeed(char[] source, int offset, int length, String lineFeedReplace) {
            this(source, offset, length, lineFeedReplace, 16384);
        }

        public FromLineFeed(char[] source, int offset, int length, String lineFeedReplace, int conversionSegmentSize) {
            this((Object)source, offset, length, lineFeedReplace, conversionSegmentSize);
        }

        public FromLineFeed(String text, int offset, int length, String lineFeedReplace) {
            this(text, offset, length, lineFeedReplace, 16384);
        }

        public FromLineFeed(String text, int offset, int length, String lineFeedReplace, int conversionSegmentSize) {
            this((Object)text, offset, length, lineFeedReplace, conversionSegmentSize);
        }

        private FromLineFeed(Object charArrayOrSequence, int offset, int length, String lineFeedReplace, int conversionSegmentSize) {
            if (conversionSegmentSize < lineFeedReplace.length()) {
                throw new IllegalArgumentException("conversionSegmentSize=" + conversionSegmentSize + " < lineFeedReplace.length()=" + lineFeedReplace.length());
            }
            this.charArrayOrSequence = charArrayOrSequence;
            this.offset = offset;
            this.endOffset = offset + length;
            this.lineFeedReplace = lineFeedReplace;
            this.convertedText = new Segment();
            this.convertedText.array = new char[conversionSegmentSize];
        }

        public Segment nextConverted() {
            char[] sourceArray;
            String sourceText;
            if (this.offset == this.endOffset) {
                return null;
            }
            char[] convertedArray = this.convertedText.array;
            int convertedArrayLength = convertedArray.length;
            int convertedOffset = 0;
            if (this.charArrayOrSequence instanceof String) {
                sourceText = (String)this.charArrayOrSequence;
                sourceArray = null;
            } else {
                sourceArray = (char[])this.charArrayOrSequence;
                sourceText = null;
            }
            int lineFeedReplaceLength = this.lineFeedReplace.length();
            while (this.offset < this.endOffset && convertedArrayLength - convertedOffset >= lineFeedReplaceLength) {
                char ch;
                char c = ch = sourceText != null ? sourceText.charAt(this.offset++) : sourceArray[this.offset++];
                if (ch == '\n') {
                    for (int i = 0; i < lineFeedReplaceLength; ++i) {
                        convertedArray[convertedOffset++] = this.lineFeedReplace.charAt(i);
                    }
                    continue;
                }
                convertedArray[convertedOffset++] = ch;
            }
            this.convertedText.offset = 0;
            this.convertedText.count = convertedOffset;
            return this.convertedText;
        }
    }

    public static class ToLineFeed {
        private Reader reader;
        private Segment convertedText;
        private boolean lastCharCR;
        private boolean readWholeBuffer;

        public ToLineFeed(Reader reader) {
            this(reader, 16384);
        }

        public ToLineFeed(Reader reader, int convertBufferSize) {
            this.reader = reader;
            this.convertedText = new Segment();
            this.convertedText.array = new char[convertBufferSize];
        }

        public Segment nextConverted() throws IOException {
            this.readWholeBuffer = false;
            if (this.reader == null) {
                return null;
            }
            int readOffset = 0;
            int readSize = ToLineFeed.readBuffer(this.reader, this.convertedText.array, readOffset, true);
            if (readSize == 0) {
                this.reader.close();
                this.reader = null;
                return null;
            }
            boolean bl = this.readWholeBuffer = readSize == this.convertedText.array.length;
            if (this.lastCharCR && readSize > 0 && this.convertedText.array[readOffset] == '\n') {
                ++readOffset;
                --readSize;
            }
            this.convertedText.offset = readOffset;
            this.convertedText.count = readSize;
            this.lastCharCR = ToLineFeed.convertSegmentToLineFeed(this.convertedText);
            return this.convertedText;
        }

        public boolean isReadWholeBuffer() {
            return this.readWholeBuffer;
        }

        private static boolean convertSegmentToLineFeed(Segment text) {
            char[] chars = text.array;
            int storeOffset = text.offset;
            int endOffset = storeOffset + text.count;
            boolean storeChar = false;
            boolean lastCharCR = false;
            for (int offset = storeOffset; offset < endOffset; ++offset) {
                char ch = chars[offset];
                if (lastCharCR && ch == '\n') {
                    lastCharCR = false;
                    storeChar = true;
                    continue;
                }
                if (ch == '\r') {
                    lastCharCR = true;
                    chars[storeOffset++] = 10;
                    continue;
                }
                if (ch == '\u2028' || ch == '\u2029') {
                    lastCharCR = false;
                    chars[storeOffset++] = 10;
                    continue;
                }
                lastCharCR = false;
                if (storeChar) {
                    chars[storeOffset] = ch;
                }
                ++storeOffset;
            }
            text.count = storeOffset - text.offset;
            return lastCharCR;
        }

        private static int readBuffer(Reader reader, char[] buffer, int offset, boolean joinReads) throws IOException {
            int readSize;
            int maxReadSize = buffer.length - offset;
            int totalReadSize = 0;
            do {
                readSize = 0;
                while (readSize == 0) {
                    readSize = reader.read(buffer, offset, maxReadSize);
                }
                if (readSize == -1) break;
                totalReadSize += readSize;
                offset += readSize;
            } while (joinReads && (maxReadSize -= readSize) > 0);
            return totalReadSize;
        }
    }

}

