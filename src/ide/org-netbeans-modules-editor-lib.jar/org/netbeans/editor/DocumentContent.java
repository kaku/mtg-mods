/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.AbstractCharSequence
 *  org.netbeans.lib.editor.util.AbstractCharSequence$StringLike
 */
package org.netbeans.editor;

import java.io.PrintStream;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.CharSeq;
import org.netbeans.editor.GapStart;
import org.netbeans.lib.editor.util.AbstractCharSequence;
import org.netbeans.modules.editor.lib.impl.BasePosition;
import org.netbeans.modules.editor.lib.impl.MarkVector;
import org.netbeans.modules.editor.lib.impl.MultiMark;

final class DocumentContent
implements AbstractDocument.Content,
CharSeq,
GapStart {
    private static final char[] EMPTY_CHAR_ARRAY = new char[0];
    private static final UndoableEdit INVALID_EDIT = new AbstractUndoableEdit();
    private static final boolean debugUndo = Boolean.getBoolean("netbeans.debug.editor.document.undo");
    private final MarkVector markVector = new MarkVector();
    private char[] charArray = EMPTY_CHAR_ARRAY;
    private int gapStart;
    private int gapLength;
    private boolean conservativeReallocation;

    DocumentContent() {
        this.insertText(0, "\n");
    }

    @Override
    public final int getGapStart() {
        return this.gapStart;
    }

    @Override
    public UndoableEdit insertString(int offset, String text) throws BadLocationException {
        this.checkBounds(offset, 0, this.length() - 1);
        return new Edit(this, offset, text);
    }

    @Override
    public UndoableEdit remove(int offset, int length) throws BadLocationException {
        this.checkBounds(offset, length, this.length() - 1);
        return new Edit(this, offset, length);
    }

    @Override
    public Position createPosition(int offset) throws BadLocationException {
        this.checkOffset(offset);
        BasePosition pos = new BasePosition();
        this.markVector.insert(this.markVector.createMark(pos, offset));
        return pos;
    }

    public Position createBiasPosition(int offset, Position.Bias bias) throws BadLocationException {
        this.checkOffset(offset);
        BasePosition pos = new BasePosition();
        this.markVector.insert(this.markVector.createBiasMark(pos, offset, bias));
        return pos;
    }

    MultiMark createBiasMark(int offset, Position.Bias bias) throws BadLocationException {
        this.checkOffset(offset);
        return this.markVector.insert(this.markVector.createBiasMark(offset, bias));
    }

    MultiMark createMark(int offset) throws BadLocationException {
        this.checkOffset(offset);
        return this.markVector.insert(this.markVector.createMark(offset));
    }

    @Override
    public int length() {
        return this.charArray.length - this.gapLength;
    }

    @Override
    public void getChars(int offset, int length, Segment chars) throws BadLocationException {
        this.checkBounds(offset, length, this.length());
        if (offset + length <= this.gapStart) {
            chars.array = this.charArray;
            chars.offset = offset;
        } else if (offset >= this.gapStart) {
            chars.array = this.charArray;
            chars.offset = offset + this.gapLength;
        } else {
            chars.array = this.copySpanChars(offset, length);
            chars.offset = 0;
        }
        chars.count = length;
    }

    @Override
    public String getString(int offset, int length) throws BadLocationException {
        this.checkBounds(offset, length, this.length());
        return this.getText(offset, length);
    }

    String getText(int offset, int length) {
        if (offset < 0 || length < 0) {
            throw new IllegalStateException("offset=" + offset + ", length=" + length);
        }
        String ret = offset + length <= this.gapStart ? new String(this.charArray, offset, length) : (offset >= this.gapStart ? new String(this.charArray, offset + this.gapLength, length) : new String(this.copySpanChars(offset, length)));
        return ret;
    }

    @Override
    public char charAt(int index) {
        return this.charArray[this.getRawIndex(index)];
    }

    public CharSequence createCharSequenceView() {
        return new CharSequenceImpl();
    }

    void compact() {
        if (this.gapLength > 0) {
            int newLength = this.charArray.length - this.gapLength;
            char[] newCharArray = new char[newLength];
            int gapEnd = this.gapStart + this.gapLength;
            System.arraycopy(this.charArray, 0, newCharArray, 0, this.gapStart);
            System.arraycopy(this.charArray, gapEnd, newCharArray, this.gapStart, this.charArray.length - gapEnd);
            this.charArray = newCharArray;
            this.gapStart = this.charArray.length;
            this.gapLength = 0;
        }
        this.markVector.compact();
    }

    boolean isConservativeReallocation() {
        return this.conservativeReallocation;
    }

    void setConservativeReallocation(boolean conservativeReallocation) {
        this.conservativeReallocation = conservativeReallocation;
    }

    private int getRawIndex(int index) {
        return index < this.gapStart ? index : index + this.gapLength;
    }

    private void moveGap(int index) {
        if (index <= this.gapStart) {
            int moveSize = this.gapStart - index;
            System.arraycopy(this.charArray, index, this.charArray, this.gapStart + this.gapLength - moveSize, moveSize);
            this.gapStart = index;
        } else {
            int gapEnd = this.gapStart + this.gapLength;
            int moveSize = index - this.gapStart;
            System.arraycopy(this.charArray, gapEnd, this.charArray, this.gapStart, moveSize);
            this.gapStart += moveSize;
        }
    }

    private void enlargeGap(int extraLength) {
        int newLength = this.conservativeReallocation ? Math.min(4096, this.charArray.length / 10) : this.charArray.length;
        newLength = Math.max(10, this.charArray.length + newLength + extraLength);
        int gapEnd = this.gapStart + this.gapLength;
        int afterGapLength = this.charArray.length - gapEnd;
        int newGapEnd = newLength - afterGapLength;
        char[] newCharArray = new char[newLength];
        System.arraycopy(this.charArray, 0, newCharArray, 0, this.gapStart);
        System.arraycopy(this.charArray, gapEnd, newCharArray, newGapEnd, afterGapLength);
        this.charArray = newCharArray;
        this.gapLength = newGapEnd - this.gapStart;
    }

    private char[] copyChars(int offset, int length) {
        char[] ret;
        if (offset + length <= this.gapStart) {
            ret = new char[length];
            System.arraycopy(this.charArray, offset, ret, 0, length);
        } else if (offset >= this.gapStart) {
            ret = new char[length];
            System.arraycopy(this.charArray, offset + this.gapLength, ret, 0, length);
        } else {
            ret = this.copySpanChars(offset, length);
        }
        return ret;
    }

    private char[] copySpanChars(int offset, int length) {
        char[] ret = new char[length];
        int belowGap = this.gapStart - offset;
        System.arraycopy(this.charArray, offset, ret, 0, belowGap);
        System.arraycopy(this.charArray, this.gapStart + this.gapLength, ret, belowGap, length - belowGap);
        return ret;
    }

    void insertText(int offset, String text) {
        int textLength = text.length();
        int extraLength = textLength - this.gapLength;
        if (extraLength > 0) {
            this.enlargeGap(extraLength);
        }
        if (offset != this.gapStart) {
            this.moveGap(offset);
        }
        text.getChars(0, textLength, this.charArray, this.gapStart);
        this.gapStart += textLength;
        this.gapLength -= textLength;
    }

    void removeText(int offset, int length) {
        if (offset >= this.gapStart) {
            if (offset > this.gapStart) {
                this.moveGap(offset);
            }
        } else {
            int endOffset = offset + length;
            if (endOffset <= this.gapStart) {
                if (endOffset < this.gapStart) {
                    this.moveGap(endOffset);
                }
                this.gapStart -= length;
            } else {
                this.gapStart = offset;
            }
        }
        this.gapLength += length;
    }

    private void checkOffset(int offset) throws BadLocationException {
        if (offset > this.length()) {
            throw new BadLocationException("Invalid offset=" + offset + ", docLength=" + (this.length() - 1), offset);
        }
    }

    private void checkBounds(int offset, int length, int limitOffset) throws BadLocationException {
        if (offset < 0) {
            throw new BadLocationException("Invalid offset=" + offset, offset);
        }
        if (length < 0) {
            throw new BadLocationException("Invalid length" + length, length);
        }
        if (offset + length > limitOffset) {
            throw new BadLocationException("docLength=" + (this.length() - 1) + ":  Invalid offset" + (length != 0 ? "+length" : "") + "=" + (offset + length), offset + length);
        }
    }

    public String toString() {
        return "charArray.length=" + this.charArray.length + ", " + this.markVector.toString();
    }

    class Edit
    extends AbstractUndoableEdit {
        private int offset;
        private int length;
        private String text;
        private MarkVector.Undo markVectorUndo;
        final /* synthetic */ DocumentContent this$0;

        Edit(DocumentContent documentContent, int offset, String text) {
            this.this$0 = documentContent;
            this.offset = offset;
            this.length = text.length();
            this.text = text;
            this.undoOrRedo(this.length, false);
        }

        Edit(DocumentContent documentContent, int offset, int length) {
            this.this$0 = documentContent;
            this.offset = offset;
            this.length = - length;
            this.text = documentContent.getText(offset, length);
            this.undoOrRedo(- length, false);
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            if (debugUndo) {
                System.err.println("UNDO-" + this.dump());
            }
            this.undoOrRedo(- this.length, true);
        }

        @Override
        public void redo() throws CannotRedoException {
            super.redo();
            if (debugUndo) {
                System.err.println("REDO-" + this.dump());
            }
            this.undoOrRedo(this.length, false);
        }

        private String dump() {
            return (this.length >= 0 ? "INSERT" : "REMOVE") + ":offset=" + this.offset + ", length=" + this.length + ", text='" + this.text + '\'';
        }

        private void undoOrRedo(int len, boolean undo) {
            if (len < 0) {
                this.this$0.removeText(this.offset, - len);
            } else {
                this.this$0.insertText(this.offset, this.text);
            }
            this.markVectorUndo = this.this$0.markVector.update(this.offset, len, this.markVectorUndo);
        }

        final String getUndoRedoText() {
            return this.text;
        }
    }

    private final class CharSequenceImpl
    extends AbstractCharSequence.StringLike {
        private CharSequenceImpl() {
        }

        public char charAt(int index) {
            return DocumentContent.this.charAt(index);
        }

        public int length() {
            return DocumentContent.this.length();
        }

        public String toString() {
            return DocumentContent.this.getText(0, this.length());
        }
    }

}

