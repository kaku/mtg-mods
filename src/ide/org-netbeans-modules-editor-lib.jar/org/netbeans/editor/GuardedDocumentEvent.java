/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.event.DocumentEvent;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.GuardedDocument;

public class GuardedDocumentEvent
extends BaseDocumentEvent {
    static final long serialVersionUID = -9204897347010955248L;

    public GuardedDocumentEvent(GuardedDocument doc, int offset, int length, DocumentEvent.EventType type) {
        super(doc, offset, length, type);
    }

    @Override
    public void undo() throws CannotUndoException {
        GuardedDocument gdoc = (GuardedDocument)this.getDocument();
        boolean origBreak = gdoc.breakGuarded;
        gdoc.breakGuarded = true;
        super.undo();
        if (!origBreak) {
            gdoc.breakGuarded = false;
        }
    }

    @Override
    public void redo() throws CannotRedoException {
        GuardedDocument gdoc = (GuardedDocument)this.getDocument();
        boolean origBreak = gdoc.breakGuarded;
        super.redo();
        if (!origBreak) {
            gdoc.breakGuarded = false;
        }
    }
}

