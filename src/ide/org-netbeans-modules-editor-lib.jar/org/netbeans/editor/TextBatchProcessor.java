/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.BaseDocument;

public interface TextBatchProcessor {
    public int processTextBatch(BaseDocument var1, int var2, int var3, boolean var4);
}

