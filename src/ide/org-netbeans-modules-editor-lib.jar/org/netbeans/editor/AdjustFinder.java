/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Finder;

public interface AdjustFinder
extends Finder {
    public int adjustStartPos(BaseDocument var1, int var2);

    public int adjustLimitPos(BaseDocument var1, int var2);
}

