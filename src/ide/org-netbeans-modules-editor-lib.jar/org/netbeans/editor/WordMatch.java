/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Finder;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.StringMap;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.util.WeakListeners;

public class WordMatch
extends FinderFactory.AbstractFinder
implements PropertyChangeListener {
    private static final Object NULL_DOC = new Object();
    private static final HashMap staticWordsDocs = new HashMap();
    char[] baseWord;
    char[] word = new char[20];
    String lastWord;
    String previousWord;
    int wordLen;
    StringMap wordsMap = new StringMap();
    ArrayList wordInfoList = new ArrayList();
    int wordsIndex;
    boolean forwardSearch;
    EditorUI editorUI;
    boolean wrapSearch;
    boolean matchCase;
    boolean smartCase;
    boolean realMatchCase;
    boolean matchOneChar;
    int maxSearchLen;
    BaseDocument startDoc;
    private Preferences prefs = null;
    private final PreferenceChangeListener prefsListener;
    private PreferenceChangeListener weakListener;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public WordMatch(EditorUI editorUI) {
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                if (evt != null) {
                    staticWordsDocs.clear();
                }
                WordMatch.this.maxSearchLen = WordMatch.this.prefs.getInt("word-match-search-len", Integer.MAX_VALUE);
                WordMatch.this.wrapSearch = WordMatch.this.prefs.getBoolean("word-match-wrap-search", true);
                WordMatch.this.matchOneChar = WordMatch.this.prefs.getBoolean("word-match-match-one-char", true);
                WordMatch.this.matchCase = WordMatch.this.prefs.getBoolean("word-match-match-case", false);
                WordMatch.this.smartCase = WordMatch.this.prefs.getBoolean("word-match-smart-case", false);
            }
        };
        this.weakListener = null;
        this.editorUI = editorUI;
        Object object = editorUI.getComponentLock();
        synchronized (object) {
            JTextComponent component = editorUI.getComponent();
            if (component != null) {
                this.propertyChange(new PropertyChangeEvent(editorUI, "component", null, component));
            }
            editorUI.addPropertyChangeListener(this);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if ("component".equals(propName)) {
            JTextComponent newC;
            if (this.prefs != null && this.weakListener != null) {
                this.prefs.removePreferenceChangeListener(this.weakListener);
            }
            if ((newC = (JTextComponent)evt.getNewValue()) != null) {
                String mimeType = DocumentUtilities.getMimeType((JTextComponent)newC);
                this.prefs = (Preferences)MimeLookup.getLookup((String)mimeType).lookup(Preferences.class);
                this.weakListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs);
                this.prefs.addPreferenceChangeListener(this.weakListener);
                this.prefsListener.preferenceChange(null);
            }
        }
    }

    public synchronized void clear() {
        if (this.baseWord != null) {
            this.baseWord = null;
            this.wordsMap.clear();
            this.wordInfoList.clear();
            this.wordsIndex = 0;
        }
    }

    @Override
    public void reset() {
        super.reset();
        this.wordLen = 0;
    }

    public synchronized String getMatchWord(int startPos, boolean forward) {
        int listSize = this.wordInfoList.size();
        boolean searchNext = listSize == 0 || this.wordsIndex == (forward ? listSize - 1 : 0);
        this.startDoc = (BaseDocument)this.editorUI.getComponent().getDocument();
        String ret = null;
        if (this.baseWord == null) {
            try {
                String baseWordString = Utilities.getIdentifierBefore(this.startDoc, startPos);
                if (baseWordString == null) {
                    baseWordString = "";
                }
                this.lastWord = baseWordString;
                this.baseWord = baseWordString.toCharArray();
                WordInfo info = new WordInfo(baseWordString, this.startDoc.createPosition(startPos - this.baseWord.length), this.startDoc);
                this.wordsMap.put(info.word, info);
                this.wordInfoList.add(info);
            }
            catch (BadLocationException e) {
                Utilities.annotateLoggable(e);
            }
            if (this.smartCase && !this.matchCase) {
                this.realMatchCase = false;
                for (int i = 0; i < this.baseWord.length; ++i) {
                    if (!Character.isUpperCase(this.baseWord[i])) continue;
                    this.realMatchCase = true;
                }
            } else {
                this.realMatchCase = this.matchCase;
            }
            if (!this.realMatchCase) {
                for (int i = 0; i < this.baseWord.length; ++i) {
                    this.baseWord[i] = Character.toLowerCase(this.baseWord[i]);
                }
            }
        }
        if (searchNext) {
            try {
                BaseDocument doc;
                int pos;
                if (listSize > 0) {
                    WordInfo info = (WordInfo)this.wordInfoList.get(this.wordsIndex);
                    doc = info.doc;
                    pos = info.pos.getOffset();
                    if (forward) {
                        pos += info.word.length();
                    }
                } else {
                    doc = this.startDoc;
                    pos = startPos;
                }
                while (doc != null) {
                    if (doc.getLength() > 0) {
                        int endPos = doc == this.startDoc ? (forward ? (pos >= startPos ? -1 : startPos) : (pos == -1 || pos > startPos ? startPos : 0)) : -1;
                        this.forwardSearch = forward || doc != this.startDoc;
                        int foundPos = doc.find(this, pos, endPos);
                        if (foundPos != -1) {
                            if (forward) {
                                ++this.wordsIndex;
                            }
                            WordInfo info = new WordInfo(new String(this.word, 0, this.wordLen), doc.createPosition(foundPos), doc);
                            this.wordsMap.put(info.word, info);
                            this.wordInfoList.add(this.wordsIndex, info);
                            this.previousWord = this.lastWord;
                            this.lastWord = info.word;
                            return this.lastWord;
                        }
                        if (doc == this.startDoc) {
                            if (forward) {
                                pos = 0;
                                if (endPos == -1 && this.wrapSearch) continue;
                                doc = this.getNextDoc(doc);
                                continue;
                            }
                            if (pos == -1 || !this.wrapSearch) {
                                doc = this.getNextDoc(doc);
                                pos = 0;
                                continue;
                            }
                            pos = -1;
                            continue;
                        }
                        doc = this.getNextDoc(doc);
                        pos = 0;
                        continue;
                    }
                    doc = this.getNextDoc(doc);
                    pos = 0;
                }
            }
            catch (BadLocationException e) {
                Utilities.annotateLoggable(e);
            }
        } else {
            this.wordsIndex += forward ? 1 : -1;
            this.previousWord = this.lastWord;
            ret = this.lastWord = ((WordInfo)this.wordInfoList.get((int)this.wordsIndex)).word;
        }
        this.startDoc = null;
        return ret;
    }

    public String getPreviousWord() {
        return this.previousWord;
    }

    private void doubleWordSize() {
        char[] tmp = new char[this.word.length * 2];
        System.arraycopy(this.word, 0, tmp, 0, this.word.length);
        this.word = tmp;
    }

    private boolean checkWord() {
        if (!this.matchOneChar && this.wordLen == 1) {
            return false;
        }
        if (this.baseWord.length > 0) {
            if (this.wordLen < this.baseWord.length) {
                return false;
            }
            for (int i = 0; i < this.baseWord.length; ++i) {
                if (!(this.realMatchCase ? this.word[i] != this.baseWord[i] : Character.toLowerCase(this.word[i]) != this.baseWord[i])) continue;
                return false;
            }
        }
        if (this.wordsMap.containsKey(this.word, 0, this.wordLen)) {
            return false;
        }
        return true;
    }

    @Override
    public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
        int offset;
        if (this.forwardSearch) {
            int limitOffset = limitPos - bufferStartPos - 1;
            for (offset = reqPos - bufferStartPos; offset < offset2; ++offset) {
                char ch = buffer[offset];
                boolean wp = this.startDoc.isIdentifierPart(ch);
                if (wp) {
                    if (this.wordLen == this.word.length) {
                        this.doubleWordSize();
                    }
                    this.word[this.wordLen++] = ch;
                }
                if (!wp) {
                    if (this.wordLen <= 0) continue;
                    if (this.checkWord()) {
                        this.found = true;
                        return bufferStartPos + offset - this.wordLen;
                    }
                    this.wordLen = 0;
                    continue;
                }
                if (limitOffset != offset) continue;
                if (this.checkWord()) {
                    this.found = true;
                    return bufferStartPos + offset - this.wordLen + 1;
                }
                this.wordLen = 0;
            }
        } else {
            int limitOffset = limitPos - bufferStartPos;
            while (offset >= offset1) {
                char ch = buffer[offset];
                boolean wp = this.startDoc.isIdentifierPart(ch);
                if (wp) {
                    if (this.wordLen == this.word.length) {
                        this.doubleWordSize();
                    }
                    this.word[this.wordLen++] = ch;
                }
                if (!(wp && limitOffset != offset || this.wordLen <= 0)) {
                    Analyzer.reverse(this.word, this.wordLen);
                    if (this.checkWord()) {
                        this.found = true;
                        return wp ? bufferStartPos + offset + 1 : bufferStartPos + offset;
                    }
                    this.wordLen = 0;
                }
                --offset;
            }
        }
        return bufferStartPos + offset;
    }

    private BaseDocument getNextDoc(BaseDocument doc) {
        if (doc == this.getStaticWordsDoc()) {
            return null;
        }
        BaseDocument nextDoc = null;
        LinkedHashSet<BaseDocument> list = new LinkedHashSet<BaseDocument>();
        for (JTextComponent jtc : EditorRegistry.componentList()) {
            list.add(Utilities.getDocument(jtc));
        }
        Iterator i = list.iterator();
        while (i.hasNext()) {
            if (doc != i.next()) continue;
            if (!i.hasNext()) break;
            nextDoc = (BaseDocument)i.next();
            break;
        }
        if (nextDoc == null) {
            nextDoc = this.getStaticWordsDoc();
        }
        return nextDoc;
    }

    private BaseDocument getStaticWordsDoc() {
        Class kitClass = Utilities.getKitClass(this.editorUI.getComponent());
        Object val = staticWordsDocs.get(kitClass);
        if (val == NULL_DOC) {
            return null;
        }
        BaseDocument doc = (BaseDocument)val;
        if (doc == null && this.prefs != null) {
            String staticWords = this.prefs.get("word-match-static-words", null);
            if (staticWords != null) {
                doc = new BaseDocument(BaseKit.class, false);
                try {
                    doc.insertString(0, staticWords, null);
                }
                catch (BadLocationException e) {
                    Utilities.annotateLoggable(e);
                }
                staticWordsDocs.put(kitClass, doc);
            } else {
                staticWordsDocs.put(kitClass, NULL_DOC);
            }
        }
        return doc;
    }

    public String toString() {
        return "baseWord=" + (this.baseWord != null ? new StringBuilder().append("'").append(this.baseWord.toString()).append("'").toString() : "null") + ", wrapSearch=" + this.wrapSearch + ", matchCase=" + this.matchCase + ", smartCase=" + this.smartCase + ", matchOneChar=" + this.matchOneChar + ", maxSearchLen=" + this.maxSearchLen + ", wordsMap=" + this.wordsMap + "\nwordInfoList=" + this.wordInfoList + "\nwordsIndex=" + this.wordsIndex;
    }

    private static final class WordInfo {
        String word;
        Position pos;
        BaseDocument doc;

        public WordInfo(String word, Position pos, BaseDocument doc) {
            this.word = word;
            this.pos = pos;
            this.doc = doc;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o instanceof WordMatch) {
                WordMatch wm = (WordMatch)o;
                return Analyzer.equals(this.word, wm.word, 0, wm.wordLen);
            }
            if (o instanceof WordInfo) {
                return this.word.equals(((WordInfo)o).word);
            }
            if (o instanceof String) {
                return this.word.equals(o);
            }
            return false;
        }

        public int hashCode() {
            return this.word.hashCode();
        }

        public String toString() {
            return "{word='" + this.word + "', pos=" + this.pos.getOffset() + ", doc=" + this.doc + "}";
        }
    }

}

