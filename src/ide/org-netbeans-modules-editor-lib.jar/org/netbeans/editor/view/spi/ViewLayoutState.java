/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import javax.swing.text.View;

public interface ViewLayoutState {
    public View getView();

    public boolean isFlyweight();

    public int getViewRawIndex();

    public void setViewRawIndex(int var1);

    public ViewLayoutState selectLayoutMajorAxis(int var1);

    public double getLayoutMajorAxisPreferredSpan();

    public double getLayoutMajorAxisRawOffset();

    public void setLayoutMajorAxisRawOffset(double var1);

    public float getLayoutMinorAxisPreferredSpan();

    public float getLayoutMinorAxisMinimumSpan();

    public float getLayoutMinorAxisMaximumSpan();

    public float getLayoutMinorAxisAlignment();

    public void updateLayout();

    public void viewPreferenceChanged(boolean var1, boolean var2);

    public void markViewSizeInvalid();

    public boolean isLayoutValid();

    public static interface Parent {
        public void majorAxisPreferenceChanged(ViewLayoutState var1, double var2);

        public void minorAxisPreferenceChanged(ViewLayoutState var1);

        public void layoutInvalid(ViewLayoutState var1);

        public float getMinorAxisSpan(ViewLayoutState var1);

        public void repaint(ViewLayoutState var1, double var2, double var4, float var6, float var7);
    }

}

