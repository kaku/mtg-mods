/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import javax.swing.text.View;

public interface ViewFragment {
    public View getOriginalView();

    public int getRelativeStartOffset();

    public int getRelativeEndOffset();
}

