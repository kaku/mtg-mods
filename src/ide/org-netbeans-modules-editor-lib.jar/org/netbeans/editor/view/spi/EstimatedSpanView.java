/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

public interface EstimatedSpanView {
    public boolean isEstimatedSpan();

    public void setEstimatedSpan(boolean var1);
}

