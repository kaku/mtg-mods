/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import java.io.Serializable;

public final class ViewInsets
implements Serializable {
    public static final ViewInsets ZERO_INSETS = new ViewInsets(0.0f, 0.0f, 0.0f, 0.0f);
    private float top;
    private float left;
    private float bottom;
    private float right;

    public ViewInsets(float top, float left, float bottom, float right) {
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }

    public float getTop() {
        return this.top;
    }

    public float getLeft() {
        return this.left;
    }

    public float getBottom() {
        return this.bottom;
    }

    public float getRight() {
        return this.right;
    }

    public float getLeftRight() {
        return this.left + this.right;
    }

    public float getTopBottom() {
        return this.top + this.bottom;
    }

    public boolean equals(Object obj) {
        if (obj instanceof ViewInsets) {
            ViewInsets insets = (ViewInsets)obj;
            return this.top == insets.top && this.left == insets.left && this.bottom == insets.bottom && this.right == insets.right;
        }
        return false;
    }

    public int hashCode() {
        float sum1 = this.left + this.bottom;
        float sum2 = this.right + this.top;
        float val1 = sum1 * (sum1 + 1.0f) / 2.0f + this.left;
        float val2 = sum2 * (sum2 + 1.0f) / 2.0f + this.top;
        int sum3 = (int)(val1 + val2);
        return sum3 * (sum3 + 1) / 2 + (int)val2;
    }

    public String toString() {
        return this.getClass().getName() + "[top=" + this.top + ",left=" + this.left + ",bottom=" + this.bottom + ",right=" + this.right + "]";
    }
}

