/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

public class ViewLayoutQueue {
    private static ViewLayoutQueue defaultQueue;
    private static SynchronousQueue synchronousQueue;
    private Runnable[] taskArray = new Runnable[2];
    private int startIndex;
    private int endIndex;
    private Thread worker;

    public static ViewLayoutQueue getDefaultQueue() {
        if (defaultQueue == null) {
            defaultQueue = new ViewLayoutQueue();
        }
        return defaultQueue;
    }

    public static ViewLayoutQueue getSynchronousQueue() {
        if (synchronousQueue == null) {
            synchronousQueue = new SynchronousQueue();
        }
        return synchronousQueue;
    }

    public synchronized Thread getWorkerThread() {
        this.ensureWorkerInited();
        return this.worker;
    }

    private void ensureWorkerInited() {
        if (this.worker == null) {
            this.worker = new LayoutThread();
            this.worker.start();
        }
    }

    public synchronized void addTask(Runnable task) {
        if (task != null) {
            this.ensureWorkerInited();
            if (this.startIndex == this.endIndex && this.taskArray[this.startIndex] != null) {
                Runnable[] newTaskArray = new Runnable[this.taskArray.length << 1];
                int afterStartIndexLength = this.taskArray.length - this.startIndex;
                System.arraycopy(this.taskArray, this.endIndex, newTaskArray, 0, afterStartIndexLength);
                System.arraycopy(this.taskArray, 0, newTaskArray, afterStartIndexLength, this.startIndex);
                this.startIndex = 0;
                this.endIndex = this.taskArray.length;
                this.taskArray = newTaskArray;
            }
            this.taskArray[this.endIndex] = task;
            this.endIndex = this.endIndex + 1 & this.taskArray.length - 1;
            this.notify();
        }
    }

    synchronized Runnable waitForTask() {
        while (this.startIndex == this.endIndex && this.taskArray[this.startIndex] == null) {
            try {
                this.wait();
                continue;
            }
            catch (InterruptedException ie) {
                return null;
            }
        }
        Runnable task = this.taskArray[this.startIndex];
        this.taskArray[this.startIndex] = null;
        int taskArrayLength = this.taskArray.length;
        this.startIndex = this.startIndex + 1 & taskArrayLength - 1;
        if (taskArrayLength >= 128) {
            int indexDiff = this.endIndex - this.startIndex;
            if (indexDiff >= 0) {
                if (indexDiff < taskArrayLength / 8) {
                    Runnable[] smallerTaskArray = new Runnable[taskArrayLength / 4];
                    System.arraycopy(this.taskArray, this.startIndex, smallerTaskArray, 0, indexDiff);
                    this.taskArray = smallerTaskArray;
                    this.startIndex = 0;
                    this.endIndex = indexDiff;
                }
            } else if ((indexDiff = taskArrayLength + indexDiff) < taskArrayLength / 8) {
                Runnable[] smallerTaskArray = new Runnable[taskArrayLength / 4];
                System.arraycopy(this.taskArray, this.startIndex, smallerTaskArray, 0, taskArrayLength - this.startIndex);
                System.arraycopy(this.taskArray, 0, smallerTaskArray, taskArrayLength - this.startIndex, this.endIndex);
                this.taskArray = smallerTaskArray;
                this.startIndex = 0;
                this.endIndex = indexDiff;
            }
        }
        return task;
    }

    private static class SynchronousQueue
    extends ViewLayoutQueue {
        private SynchronousQueue() {
        }

        @Override
        public void addTask(Runnable r) {
            r.run();
        }
    }

    class LayoutThread
    extends Thread {
        LayoutThread() {
            super("Text-Layout");
            this.setPriority(1);
        }

        @Override
        public void run() {
            Runnable task;
            do {
                if ((task = ViewLayoutQueue.this.waitForTask()) == null) continue;
                task.run();
            } while (task != null);
        }
    }

}

