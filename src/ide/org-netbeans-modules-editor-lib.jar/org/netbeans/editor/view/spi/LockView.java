/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.PriorityMutex
 */
package org.netbeans.editor.view.spi;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Shape;
import javax.swing.event.DocumentEvent;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.lib.editor.util.PriorityMutex;

public class LockView
extends View {
    private static final String PROPERTY_VIEW_HIERARCHY_MUTEX = "viewHierarchyMutex";
    private static final String PROPERTY_FOLD_HIERARCHY_MUTEX = "foldHierarchyMutex";
    private View view;
    private PriorityMutex mutex;
    private AbstractDocument doc;

    public static synchronized PriorityMutex getViewHierarchyMutex(JTextComponent component) {
        PriorityMutex mutex = (PriorityMutex)component.getClientProperty("foldHierarchyMutex");
        if (mutex == null) {
            mutex = new PriorityMutex();
            component.putClientProperty("foldHierarchyMutex", (Object)mutex);
        }
        component.putClientProperty("viewHierarchyMutex", (Object)mutex);
        return mutex;
    }

    public static LockView get(View view) {
        while (view != null && !(view instanceof LockView)) {
            view = view.getParent();
        }
        return (LockView)view;
    }

    public LockView(View view) {
        super(null);
        this.view = view;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setParent(View parent) {
        JTextComponent c;
        View origParent = this.getParent();
        if (origParent != null && parent != null) {
            throw new IllegalStateException("Unexpected state occurred when trying to set non-null parent to LockView with non-null parent already set.");
        }
        if (this.mutex == null && parent != null && (c = (JTextComponent)parent.getContainer()) != null) {
            this.mutex = LockView.getViewHierarchyMutex(c);
        }
        if (parent != null) {
            Document maybeAbstractDoc = parent.getDocument();
            if (!(maybeAbstractDoc instanceof AbstractDocument)) {
                throw new IllegalStateException("Currently the LockView is designed to work with AbstractDocument instances only.");
            }
            this.doc = (AbstractDocument)maybeAbstractDoc;
        }
        this.doc.readLock();
        try {
            this.lock();
            try {
                this.setParentLocked(parent);
            }
            finally {
                this.unlock();
            }
        }
        finally {
            this.doc.readUnlock();
        }
    }

    protected void setParentLocked(View parent) {
        if (parent == null && this.view != null) {
            this.view.setParent(null);
        }
        super.setParent(parent);
        if (parent != null && this.view != null) {
            this.view.setParent(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setView(View v) {
        this.lock();
        try {
            if (this.view != null) {
                this.view.setParent(null);
            }
            this.view = v;
            if (this.view != null) {
                this.view.setParent(this);
            }
        }
        finally {
            this.unlock();
        }
    }

    public void lock() {
        if (this.mutex != null) {
            this.mutex.lock();
        }
    }

    public void unlock() {
        this.mutex.unlock();
    }

    public boolean isPriorityThreadWaiting() {
        return this.mutex.isPriorityThreadWaiting();
    }

    public Thread getLockThread() {
        return this.mutex.getLockThread();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void render(Runnable r) {
        this.lock();
        try {
            r.run();
        }
        finally {
            this.unlock();
        }
    }

    @Override
    public AttributeSet getAttributes() {
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public float getPreferredSpan(int axis) {
        this.lock();
        try {
            if (this.view != null) {
                float f = this.view.getPreferredSpan(axis);
                return f;
            }
            float f = 10.0f;
            return f;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public float getMinimumSpan(int axis) {
        this.lock();
        try {
            if (this.view != null) {
                float f = this.view.getMinimumSpan(axis);
                return f;
            }
            float f = 10.0f;
            return f;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public float getMaximumSpan(int axis) {
        this.lock();
        try {
            if (this.view != null) {
                float f = this.view.getMaximumSpan(axis);
                return f;
            }
            float f = 2.14748365E9f;
            return f;
        }
        finally {
            this.unlock();
        }
    }

    @Override
    public void preferenceChanged(View child, boolean width, boolean height) {
        View parent = this.getParent();
        if (parent != null) {
            parent.preferenceChanged(this, width, height);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public float getAlignment(int axis) {
        this.lock();
        try {
            if (this.view != null) {
                float f = this.view.getAlignment(axis);
                return f;
            }
            float f = 0.0f;
            return f;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g, Shape allocation) {
        if (g == null) {
            return;
        }
        this.lock();
        try {
            if (this.view != null) {
                this.view.paint(g, allocation);
            }
        }
        finally {
            this.unlock();
        }
    }

    @Override
    public int getViewCount() {
        return 1;
    }

    @Override
    public View getView(int n) {
        return this.view;
    }

    @Override
    public int getViewIndex(int pos, Position.Bias b) {
        return 0;
    }

    @Override
    public Shape getChildAllocation(int index, Shape a) {
        return a;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
        this.lock();
        try {
            if (this.view != null) {
                Shape shape = this.view.modelToView(pos, a, b);
                return shape;
            }
            Shape shape = null;
            return shape;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Shape modelToView(int p0, Position.Bias b0, int p1, Position.Bias b1, Shape a) throws BadLocationException {
        this.lock();
        try {
            if (this.view != null) {
                Shape shape = this.view.modelToView(p0, b0, p1, b1, a);
                return shape;
            }
            Shape shape = null;
            return shape;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int viewToModel(float x, float y, Shape a, Position.Bias[] bias) {
        this.lock();
        try {
            if (this.view != null) {
                int n = this.view.viewToModel(x, y, a, bias);
                return n;
            }
            int n = -1;
            return n;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        this.lock();
        try {
            if (this.view != null) {
                int n = this.view.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
                return n;
            }
            int n = -1;
            return n;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        this.lock();
        try {
            if (this.view != null) {
                this.view.insertUpdate(e, a, f);
            }
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        this.lock();
        try {
            if (this.view != null) {
                this.view.removeUpdate(e, a, f);
            }
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void changedUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        this.lock();
        try {
            if (this.view != null) {
                this.view.changedUpdate(e, a, f);
            }
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolTipText(float x, float y, Shape allocation) {
        this.lock();
        try {
            String string = this.view != null ? this.view.getToolTipText(x, y, allocation) : null;
            return string;
        }
        finally {
            this.unlock();
        }
    }

    @Override
    public Document getDocument() {
        return this.doc;
    }

    @Override
    public int getStartOffset() {
        if (this.view != null) {
            return this.view.getStartOffset();
        }
        Element elem = this.getElement();
        return elem != null ? elem.getStartOffset() : 0;
    }

    @Override
    public int getEndOffset() {
        if (this.view != null) {
            return this.view.getEndOffset();
        }
        Element elem = this.getElement();
        return elem != null ? elem.getEndOffset() : 0;
    }

    @Override
    public Element getElement() {
        if (this.view != null) {
            return this.view.getElement();
        }
        Document doc = this.getDocument();
        return doc != null ? doc.getDefaultRootElement() : null;
    }

    public View breakView(int axis, float len, Shape a) {
        throw new Error("Can't break lock view");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int getResizeWeight(int axis) {
        this.lock();
        try {
            if (this.view != null) {
                int n = this.view.getResizeWeight(axis);
                return n;
            }
            int n = 0;
            return n;
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSize(float width, float height) {
        this.lock();
        try {
            if (this.view != null) {
                this.view.setSize(width, height);
            }
        }
        finally {
            this.unlock();
        }
    }
}

