/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import org.netbeans.editor.view.spi.ViewLayoutQueue;

public class ViewHierarchyMutex {
    private Thread lockThread;
    private int lockDepth;
    private Thread waitingPriorityThread;

    public synchronized void lock() {
        Thread thread = Thread.currentThread();
        boolean priorityThread = this.isPriorityThread(thread);
        if (thread != this.lockThread) {
            while (this.lockThread != null || this.waitingPriorityThread != null && this.waitingPriorityThread != thread) {
                try {
                    if (this.waitingPriorityThread == null && priorityThread) {
                        this.waitingPriorityThread = thread;
                    }
                    this.wait();
                }
                catch (InterruptedException e) {
                    this.waitingPriorityThread = null;
                }
            }
            this.lockThread = thread;
            if (thread == this.waitingPriorityThread) {
                this.waitingPriorityThread = null;
            }
        }
        ++this.lockDepth;
    }

    public synchronized void unlock() {
        if (Thread.currentThread() != this.lockThread) {
            throw new IllegalStateException("Not locker");
        }
        if (--this.lockDepth == 0) {
            this.lockThread = null;
            this.notifyAll();
        }
    }

    public boolean isPriorityThreadWaiting() {
        return this.waitingPriorityThread != null;
    }

    protected boolean isPriorityThread(Thread thread) {
        return thread != ViewLayoutQueue.getDefaultQueue().getWorkerThread();
    }

    public final synchronized Thread getLockThread() {
        return this.lockThread;
    }
}

