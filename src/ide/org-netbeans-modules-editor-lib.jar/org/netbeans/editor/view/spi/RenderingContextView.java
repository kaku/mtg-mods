/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import javax.swing.text.View;
import org.netbeans.editor.view.spi.ViewRenderingContext;

public interface RenderingContextView {
    public ViewRenderingContext acquireRenderingContext(View var1);

    public void releaseRenderingContext(ViewRenderingContext var1);
}

