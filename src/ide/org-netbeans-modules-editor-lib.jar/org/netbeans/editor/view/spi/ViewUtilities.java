/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import javax.swing.text.Element;
import javax.swing.text.View;
import org.netbeans.lib.editor.view.GapDocumentView;
import org.netbeans.lib.editor.view.ViewUtilitiesImpl;

public class ViewUtilities {
    private ViewUtilities() {
    }

    public static View createDocumentView(Element elem) {
        return new GapDocumentView(elem);
    }

    public static void checkViewHierarchy(View v) {
        ViewUtilitiesImpl.checkViewHierarchy(v);
    }

    public static boolean isAxisValid(int axis) {
        switch (axis) {
            case 0: 
            case 1: {
                return true;
            }
        }
        return false;
    }
}

