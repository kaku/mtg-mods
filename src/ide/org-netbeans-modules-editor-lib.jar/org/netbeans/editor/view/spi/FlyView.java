/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import javax.swing.text.View;

public interface FlyView {
    public FlyView flyInstance(View var1);

    public View regularInstance(View var1, int var2, int var3);

    public CharSequence getText();

    public static interface Parent {
        public int getStartOffset(int var1);

        public int getEndOffset(int var1);
    }

}

