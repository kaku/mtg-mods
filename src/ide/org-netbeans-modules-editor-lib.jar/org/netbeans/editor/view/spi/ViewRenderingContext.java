/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor.view.spi;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Shape;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.TabExpander;
import javax.swing.text.View;

public abstract class ViewRenderingContext {
    public static synchronized ViewRenderingContext obtainContext(View v) {
        return null;
    }

    public abstract float getSpan(View var1, int var2, int var3, TabExpander var4, float var5);

    public abstract float getHeight(View var1);

    public abstract float getAscent(View var1);

    public abstract float getDescent(View var1);

    public abstract void paint(View var1, Graphics var2, Shape var3, int var4, int var5);

    public abstract Shape modelToView(View var1, int var2, Position.Bias var3, Shape var4) throws BadLocationException;

    public abstract int viewToModel(View var1, float var2, float var3, Shape var4, Position.Bias[] var5);

    public abstract int getBoundedPosition(View var1, int var2, float var3, float var4);

    public int getNextVisualPositionFrom(View v, int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        int startOffset = v.getStartOffset();
        int endOffset = v.getEndOffset();
        switch (direction) {
            case 1: 
            case 5: {
                Point magicPoint;
                if (pos != -1) {
                    return -1;
                }
                Container container = v.getContainer();
                if (!(container instanceof JTextComponent)) break;
                Caret c = ((JTextComponent)container).getCaret();
                Point point = magicPoint = c != null ? c.getMagicCaretPosition() : null;
                if (magicPoint == null) {
                    biasRet[0] = Position.Bias.Forward;
                    return startOffset;
                }
                int value = v.viewToModel(magicPoint.x, 0.0f, a, biasRet);
                return value;
            }
            case 3: {
                if (startOffset == v.getDocument().getLength()) {
                    if (pos == -1) {
                        biasRet[0] = Position.Bias.Forward;
                        return startOffset;
                    }
                    return -1;
                }
                if (pos == -1) {
                    biasRet[0] = Position.Bias.Forward;
                    return startOffset;
                }
                if (pos == endOffset) {
                    return -1;
                }
                if (++pos == endOffset) {
                    return -1;
                }
                biasRet[0] = Position.Bias.Forward;
                return pos;
            }
            case 7: {
                if (startOffset == v.getDocument().getLength()) {
                    if (pos == -1) {
                        biasRet[0] = Position.Bias.Forward;
                        return startOffset;
                    }
                    return -1;
                }
                if (pos == -1) {
                    biasRet[0] = Position.Bias.Forward;
                    return endOffset - 1;
                }
                if (pos == startOffset) {
                    return -1;
                }
                biasRet[0] = Position.Bias.Forward;
                return pos - 1;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return pos;
    }
}

