/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Font;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.EditorDebug;
import org.netbeans.editor.GuardedDocumentEvent;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.editor.MarkFactory;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class GuardedDocument
extends BaseDocument
implements StyledDocument {
    public static final String GUARDED_ATTRIBUTE = "guarded";
    public static final SimpleAttributeSet guardedSet = new SimpleAttributeSet();
    public static final SimpleAttributeSet unguardedSet = new SimpleAttributeSet();
    private static final boolean debugAtomic = Boolean.getBoolean("netbeans.debug.editor.atomic");
    private static final boolean debugAtomicStack = Boolean.getBoolean("netbeans.debug.editor.atomic.stack");
    private static final Logger LOG = Logger.getLogger(GuardedDocument.class.getName());
    public static final String FMT_GUARDED_INSERT_LOCALE = "FMT_guarded_insert";
    public static final String FMT_GUARDED_REMOVE_LOCALE = "FMT_guarded_remove";
    MarkBlockChain guardedBlockChain;
    boolean breakGuarded;
    boolean atomicAsUser;
    protected StyleContext styles;
    protected String normalStyleName;

    public GuardedDocument(Class kitClass) {
        this(kitClass, true, new StyleContext());
    }

    public GuardedDocument(String mimeType) {
        this(mimeType, true, new StyleContext());
    }

    public GuardedDocument(Class kitClass, boolean addToRegistry, StyleContext styles) {
        super(kitClass, addToRegistry);
        this.init(styles);
    }

    public GuardedDocument(String mimeType, boolean addToRegistry, StyleContext styles) {
        super(addToRegistry, mimeType);
        this.init(styles);
    }

    private void init(StyleContext styles) {
        this.styles = styles;
        this.guardedBlockChain = new MarkBlockChain(this){

            @Override
            protected Mark createBlockStartMark() {
                MarkFactory.ContextMark startMark = new MarkFactory.ContextMark(Position.Bias.Forward, false);
                return startMark;
            }

            @Override
            protected Mark createBlockEndMark() {
                MarkFactory.ContextMark endMark = new MarkFactory.ContextMark(Position.Bias.Backward, false);
                return endMark;
            }
        };
    }

    public MarkBlockChain getGuardedBlockChain() {
        return this.guardedBlockChain;
    }

    public boolean isPosGuarded(int offset) {
        Object o = this.getProperty("editable");
        if (o != null && !((Boolean)o).booleanValue()) {
            return true;
        }
        if (!this.modifiable) {
            return true;
        }
        int rel = this.guardedBlockChain.compareBlock(offset, offset) & -13;
        return rel == 4129 || rel == 1057 && (offset == 0 || DocumentUtilities.getText((Document)this).charAt(offset - 1) == '\n');
    }

    @Override
    protected void preInsertCheck(int offset, String text, AttributeSet a) throws BadLocationException {
        boolean guarded;
        super.preInsertCheck(offset, text, a);
        int rel = this.guardedBlockChain.compareBlock(offset, offset) & -13;
        if (debugAtomic) {
            System.err.println("GuardedDocument.beforeInsertUpdate() atomicAsUser=" + this.atomicAsUser + ", breakGuarded=" + this.breakGuarded + ", inserting text='" + EditorDebug.debugString(text) + "' at offset=" + Utilities.debugPosition(this, offset));
            if (debugAtomicStack) {
                Thread.dumpStack();
            }
        }
        boolean bl = guarded = (rel & 1) != 0 && rel != 2081 && (text.charAt(text.length() - 1) != '\n' || rel != 1057);
        if (guarded && (!this.breakGuarded || this.atomicAsUser)) {
            CharSequence docText = DocumentUtilities.getText((Document)this);
            boolean insertAtLineBegin = offset == 0 || docText.charAt(offset - 1) == '\n';
            boolean bl2 = guarded = rel != 1057 || insertAtLineBegin;
            if (guarded) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("GuardedDocument.preInsertCheck(): offset:" + Utilities.debugPosition(this, offset) + ", relation: " + rel + "; guardedBlockChain:\n" + this.guardedBlockChain + "\n");
                }
                throw new GuardedException(MessageFormat.format(NbBundle.getBundle(BaseKit.class).getString("FMT_guarded_insert"), new Integer(offset)), offset);
            }
        }
    }

    @Override
    protected void preRemoveCheck(int offset, int len) throws BadLocationException {
        boolean guarded;
        int rel = this.guardedBlockChain.compareBlock(offset, offset + len);
        if (debugAtomic) {
            System.err.println("GuardedDocument.beforeRemoveUpdate() atomicAsUser=" + this.atomicAsUser + ", breakGuarded=" + this.breakGuarded + ", removing text='" + EditorDebug.debugChars(this.getChars(offset, len)) + "'at offset=" + Utilities.debugPosition(this, offset));
            if (debugAtomicStack) {
                Thread.dumpStack();
            }
        }
        boolean bl = guarded = (rel & 1) != 0;
        if (!guarded && rel == 66) {
            CharSequence docText = DocumentUtilities.getText((Document)this);
            int gbStartOffset = offset + len;
            boolean gbStartsAtLineBegin = gbStartOffset == 0 || docText.charAt(gbStartOffset - 1) == '\n';
            boolean bl2 = guarded = gbStartsAtLineBegin && offset != 0 && docText.charAt(offset - 1) != '\n';
        }
        if (guarded && (!this.breakGuarded || this.atomicAsUser)) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("GuardedDocument.preRemoveCheck(): offset:" + Utilities.debugPosition(this, offset) + ", relation: " + rel + "; guardedBlockChain:\n" + this.guardedBlockChain + "\n");
            }
            throw new GuardedException(MessageFormat.format(NbBundle.getBundle(BaseKit.class).getString("FMT_guarded_remove"), new Integer(offset)), offset);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void runAtomic(Runnable r) {
        if (debugAtomic) {
            System.out.println("GuardedDocument.runAtomic() called");
            if (debugAtomicStack) {
                Thread.dumpStack();
            }
        }
        boolean completed = false;
        this.atomicLockImpl();
        boolean origBreakGuarded = this.breakGuarded;
        boolean errorOccurred = false;
        try {
            this.breakGuarded = true;
            r.run();
            completed = true;
        }
        catch (Error e) {
            errorOccurred = true;
            Exceptions.printStackTrace((Throwable)e);
        }
        finally {
            this.breakGuarded = origBreakGuarded;
            try {
                if (!completed && !errorOccurred) {
                    this.breakAtomicLock();
                }
            }
            finally {
                this.atomicUnlockImpl();
            }
            if (debugAtomic) {
                System.out.println("GuardedDocument.runAtomic() finished");
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void runAtomicAsUser(Runnable r) {
        if (debugAtomic) {
            System.out.println("GuardedDocument.runAtomicAsUser() called");
            if (debugAtomicStack) {
                Thread.dumpStack();
            }
        }
        boolean completed = false;
        this.atomicLockImpl();
        boolean origAtomicAsUser = this.atomicAsUser;
        try {
            this.atomicAsUser = true;
            r.run();
            completed = true;
        }
        finally {
            this.atomicAsUser = origAtomicAsUser;
            try {
                if (!completed) {
                    this.breakAtomicLock();
                }
            }
            finally {
                this.atomicUnlockImpl();
            }
            if (debugAtomic) {
                System.out.println("GuardedDocument.runAtomicAsUser() finished");
            }
        }
    }

    @Override
    protected BaseDocumentEvent createDocumentEvent(int offset, int length, DocumentEvent.EventType type) {
        return new GuardedDocumentEvent(this, offset, length, type);
    }

    public void setNormalStyleName(String normalStyleName) {
        this.normalStyleName = normalStyleName;
    }

    public Enumeration getStyleNames() {
        return this.styles.getStyleNames();
    }

    @Override
    public Style addStyle(String styleName, Style parent) {
        Style style = this.styles.addStyle(styleName, parent);
        return style;
    }

    @Override
    public void removeStyle(String styleName) {
        this.styles.removeStyle(styleName);
    }

    @Override
    public Style getStyle(String styleName) {
        return this.styles.getStyle(styleName);
    }

    @Override
    public void setCharacterAttributes(int offset, int length, AttributeSet attribs, boolean replace) {
        if (((Boolean)attribs.getAttribute("guarded")).booleanValue()) {
            this.guardedBlockChain.addBlock(offset, offset + length, false);
            this.fireChangedUpdate(this.getDocumentEvent(offset, length, DocumentEvent.EventType.CHANGE, attribs));
        }
        if (!((Boolean)attribs.getAttribute("guarded")).booleanValue()) {
            this.guardedBlockChain.removeBlock(offset, offset + length);
            this.fireChangedUpdate(this.getDocumentEvent(offset, length, DocumentEvent.EventType.CHANGE, attribs));
        }
    }

    @Override
    public void setParagraphAttributes(int offset, int length, AttributeSet s, boolean replace) {
    }

    @Override
    public void setLogicalStyle(int pos, Style s) {
    }

    @Override
    public Style getLogicalStyle(int pos) {
        return null;
    }

    @Override
    public Element getCharacterElement(int pos) {
        return this.getParagraphElement(pos);
    }

    @Override
    public Color getForeground(AttributeSet attr) {
        return null;
    }

    @Override
    public Color getBackground(AttributeSet attr) {
        return null;
    }

    @Override
    public Font getFont(AttributeSet attr) {
        return new Font("Default", 1, 12);
    }

    @Override
    public String toStringDetail() {
        return super.toStringDetail() + ",\nGUARDED blocks:\n" + this.guardedBlockChain;
    }

    static {
        guardedSet.addAttribute("guarded", Boolean.TRUE);
        unguardedSet.addAttribute("guarded", Boolean.FALSE);
    }

}

