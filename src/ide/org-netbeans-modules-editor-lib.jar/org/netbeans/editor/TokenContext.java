/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;

public class TokenContext {
    private static final TokenContext[] EMPTY_CHILDREN = new TokenContext[0];
    private final String namePrefix;
    private final TokenContext[] children;
    private final HashMap pathCache = new HashMap(37);
    private final ArrayList tokenIDList = new ArrayList();
    private final ArrayList tokenCategoryList = new ArrayList();
    private TokenID[] tokenIDs;
    private TokenCategory[] tokenCategories;
    private TokenContextPath contextPath;
    private TokenContextPath[] allContextPaths;
    private TokenContextPath[] lastContextPathPair;

    public TokenContext(String namePrefix) {
        this(namePrefix, EMPTY_CHILDREN);
    }

    public TokenContext(String namePrefix, TokenContext[] children) {
        if (namePrefix == null) {
            throw new IllegalArgumentException("Name prefix must be non-null.");
        }
        this.namePrefix = namePrefix.intern();
        this.children = children != null ? children : EMPTY_CHILDREN;
        this.contextPath = TokenContextPath.get(new TokenContext[]{this});
    }

    public String getNamePrefix() {
        return this.namePrefix;
    }

    public TokenContext[] getChildren() {
        return this.children;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void addTokenID(TokenID tokenID) {
        ArrayList arrayList = this.tokenIDList;
        synchronized (arrayList) {
            this.tokenIDList.add(tokenID);
            this.tokenIDs = null;
            TokenCategory tcat = tokenID.getCategory();
            if (tcat != null && this.tokenCategoryList.indexOf(tcat) < 0) {
                this.tokenCategoryList.add(tcat);
                this.tokenCategories = null;
            }
        }
    }

    protected void addDeclaredTokenIDs() throws IllegalAccessException, SecurityException {
        Field[] fields = this.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            int flags = 24;
            if ((fields[i].getModifiers() & flags) != flags || !TokenID.class.isAssignableFrom(fields[i].getType())) continue;
            this.addTokenID((TokenID)fields[i].get(null));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenID[] getTokenIDs() {
        if (this.tokenIDs == null) {
            ArrayList arrayList = this.tokenIDList;
            synchronized (arrayList) {
                this.tokenIDs = this.tokenIDList.toArray(new TokenID[this.tokenIDList.size()]);
            }
        }
        return this.tokenIDs;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenCategory[] getTokenCategories() {
        if (this.tokenCategories == null) {
            ArrayList arrayList = this.tokenCategoryList;
            synchronized (arrayList) {
                this.tokenCategories = this.tokenCategoryList.toArray(new TokenCategory[this.tokenCategoryList.size()]);
            }
        }
        return this.tokenCategories;
    }

    public TokenContextPath getContextPath() {
        return this.contextPath;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenContextPath getContextPath(TokenContextPath childPath) {
        if (childPath == null) {
            return this.contextPath;
        }
        TokenContextPath[] lastPair = this.lastContextPathPair;
        if (lastPair == null || lastPair[0] != childPath) {
            HashMap hashMap = this.pathCache;
            synchronized (hashMap) {
                lastPair = (TokenContextPath[])this.pathCache.get(childPath);
                if (lastPair == null) {
                    TokenContext[] origContexts = childPath.getContexts();
                    TokenContext[] contexts = new TokenContext[origContexts.length + 1];
                    System.arraycopy(origContexts, 0, contexts, 0, origContexts.length);
                    contexts[origContexts.length] = this;
                    TokenContextPath path = TokenContextPath.get(contexts);
                    lastPair = new TokenContextPath[]{childPath, path};
                    this.pathCache.put(childPath, lastPair);
                }
                this.lastContextPathPair = lastPair;
            }
        }
        return lastPair[1];
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenContextPath[] getAllContextPaths() {
        ArrayList arrayList = this.tokenIDList;
        synchronized (arrayList) {
            if (this.allContextPaths == null) {
                ArrayList<TokenContextPath> cpList = new ArrayList<TokenContextPath>();
                cpList.add(this.getContextPath());
                for (int i = 0; i < this.children.length; ++i) {
                    TokenContextPath[] childPaths = this.children[i].getAllContextPaths();
                    for (int j = 0; j < childPaths.length; ++j) {
                        cpList.add(this.getContextPath(childPaths[j]));
                    }
                }
                this.allContextPaths = new TokenContextPath[cpList.size()];
                cpList.toArray(this.allContextPaths);
            }
            return this.allContextPaths;
        }
    }
}

