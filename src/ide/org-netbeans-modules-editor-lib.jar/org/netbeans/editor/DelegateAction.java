/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;

public class DelegateAction
extends AbstractAction {
    protected Action delegate;
    private PropertyChangeListener pcl;

    public DelegateAction() {
        this(null);
    }

    public DelegateAction(Action delegate) {
        this.delegate = delegate;
        this.pcl = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt != null) {
                    if ("enabled".equals(evt.getPropertyName())) {
                        DelegateAction.this.setEnabled((Boolean)evt.getNewValue());
                    } else {
                        DelegateAction.this.firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
                    }
                }
            }
        };
    }

    protected final Action getDelegate() {
        return this.delegate;
    }

    protected void setDelegate(Action delegate) {
        if (this.delegate == delegate) {
            return;
        }
        if (delegate == this) {
            throw new IllegalStateException("Cannot delegate on the same action");
        }
        if (this.delegate != null) {
            this.delegate.removePropertyChangeListener(this.pcl);
        }
        if (delegate != null) {
            delegate.addPropertyChangeListener(this.pcl);
        }
        this.delegate = delegate;
        this.setEnabled(delegate != null ? delegate.isEnabled() : false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.delegate != null) {
            this.delegate.actionPerformed(e);
        }
    }

    @Override
    public Object getValue(String key) {
        if (this.delegate != null) {
            return this.delegate.getValue(key);
        }
        return super.getValue(key);
    }

    @Override
    public void putValue(String key, Object value) {
        if (this.delegate != null) {
            this.delegate.putValue(key, value);
        } else {
            super.putValue(key, value);
        }
    }

}

