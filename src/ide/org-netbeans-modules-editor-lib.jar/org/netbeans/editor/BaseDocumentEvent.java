/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.document.ContentEdit
 */
package org.netbeans.editor;

import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import javax.swing.event.DocumentEvent;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.FixLineSyntaxState;
import org.netbeans.modules.editor.lib2.document.ContentEdit;

public class BaseDocumentEvent
extends AbstractDocument.DefaultDocumentEvent {
    private static final boolean debugUndo = Boolean.getBoolean("netbeans.debug.editor.document.undo");
    private ContentEdit modifyUndoEdit;
    private FixLineSyntaxState fixLineSyntaxState;
    private UndoableEdit previous;
    private boolean inUndo;
    private boolean inRedo;
    private boolean hasBeenDone2;
    private boolean alive2;
    private boolean inProgress2;
    private Hashtable changeLookup2;
    private int lfCount;
    private AttributeSet attribs;
    static final long serialVersionUID = -7624299835780414963L;

    public BaseDocumentEvent(BaseDocument doc, int offset, int length, DocumentEvent.EventType type) {
        BaseDocument baseDocument = doc;
        baseDocument.getClass();
        super(baseDocument, offset, length, type);
        this.lfCount = -1;
        this.attribs = null;
        this.hasBeenDone2 = true;
        this.alive2 = true;
        this.inProgress2 = true;
    }

    void attachChangeAttribs(AttributeSet attribs) {
        this.attribs = attribs;
    }

    public final AttributeSet getChangeAttributes() {
        return this.attribs;
    }

    protected UndoableEdit findEdit(Class editClass) {
        for (int i = this.edits.size() - 1; i >= 0; --i) {
            Object edit = this.edits.get(i);
            if (!editClass.isInstance(edit)) continue;
            return (UndoableEdit)edit;
        }
        return null;
    }

    private ContentEdit getModifyUndoEdit() {
        if (this.getType() == DocumentEvent.EventType.CHANGE) {
            throw new IllegalStateException("Cannot be called for CHANGE events.");
        }
        if (this.modifyUndoEdit == null) {
            this.modifyUndoEdit = (ContentEdit)this.findEdit(ContentEdit.class);
        }
        return this.modifyUndoEdit;
    }

    private FixLineSyntaxState getFixLineSyntaxState() {
        if (this.getType() == DocumentEvent.EventType.CHANGE) {
            throw new IllegalStateException("Cannot be called for CHANGE events.");
        }
        if (this.fixLineSyntaxState == null) {
            this.fixLineSyntaxState = ((FixLineSyntaxState.BeforeLineUndo)this.findEdit(FixLineSyntaxState.BeforeLineUndo.class)).getMaster();
        }
        return this.fixLineSyntaxState;
    }

    public char[] getChars() {
        String text = this.getText();
        return text != null ? text.toCharArray() : null;
    }

    public String getText() {
        return this.getModifyUndoEdit() != null ? this.getModifyUndoEdit().getText() : null;
    }

    public int getLine() {
        Element lineRoot = ((BaseDocument)this.getDocument()).getParagraphElement(0).getParentElement();
        int lineIndex = lineRoot.getElementIndex(this.getOffset());
        return lineIndex;
    }

    public int getLFCount() {
        if (this.getType() == DocumentEvent.EventType.CHANGE) {
            throw new IllegalStateException("Not available for CHANGE events");
        }
        if (this.lfCount == -1) {
            String text = this.getText();
            int lfCnt = 0;
            for (int i = text.length() - 1; i >= 0; --i) {
                if (text.charAt(i) != '\n') continue;
                ++lfCnt;
            }
            this.lfCount = lfCnt;
        }
        return this.lfCount;
    }

    public int getSyntaxUpdateOffset() {
        if (this.getType() == DocumentEvent.EventType.CHANGE) {
            throw new IllegalStateException("Not available for CHANGE events");
        }
        return this.getFixLineSyntaxState().getSyntaxUpdateOffset();
    }

    List getSyntaxUpdateTokenList() {
        return this.getFixLineSyntaxState().getSyntaxUpdateTokenList();
    }

    public boolean isInUndo() {
        return this.inUndo;
    }

    public boolean isInRedo() {
        return this.inRedo;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void undo() throws CannotUndoException {
        BaseDocument doc = (BaseDocument)this.getDocument();
        this.inUndo = true;
        doc.atomicLockImpl();
        if (!doc.modifiable) {
            throw new CannotUndoException();
        }
        try {
            doc.incrementDocVersion();
            if (!this.canUndo()) {
                throw new CannotUndoException();
            }
            this.hasBeenDone2 = false;
            doc.lastModifyUndoEdit = null;
            if (debugUndo) {
                System.err.println("UNDO in doc=" + doc);
            }
            int i = this.edits.size();
            while (--i >= 0) {
                UndoableEdit e = (UndoableEdit)this.edits.elementAt(i);
                e.undo();
            }
            if (this.getType() == DocumentEvent.EventType.REMOVE) {
                doc.fireInsertUpdate(this);
            } else if (this.getType() == DocumentEvent.EventType.INSERT) {
                doc.fireRemoveUpdate(this);
            } else {
                doc.fireChangedUpdate(this);
            }
            if (this.previous != null) {
                this.previous.undo();
            }
        }
        finally {
            doc.atomicUnlockImpl(false);
            this.inUndo = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void redo() throws CannotRedoException {
        block10 : {
            BaseDocument doc = (BaseDocument)this.getDocument();
            doc.atomicLockImpl();
            if (!doc.modifiable) {
                throw new CannotRedoException();
            }
            this.inRedo = true;
            try {
                doc.incrementDocVersion();
                if (this.previous != null) {
                    this.previous.redo();
                }
                if (!this.canRedo()) {
                    throw new CannotRedoException();
                }
                this.hasBeenDone2 = true;
                if (debugUndo) {
                    System.err.println("REDO in doc=" + doc);
                }
                Enumeration cursor = this.edits.elements();
                while (cursor.hasMoreElements()) {
                    ((UndoableEdit)cursor.nextElement()).redo();
                }
                if (this.getType() == DocumentEvent.EventType.INSERT) {
                    doc.fireInsertUpdate(this);
                    break block10;
                }
                if (this.getType() == DocumentEvent.EventType.REMOVE) {
                    doc.fireRemoveUpdate(this);
                    break block10;
                }
                doc.fireChangedUpdate(this);
            }
            finally {
                doc.atomicUnlockImpl(false);
            }
        }
        this.inRedo = false;
    }

    @Override
    public boolean addEdit(UndoableEdit anEdit) {
        if (this.changeLookup2 == null && this.edits.size() > 10) {
            this.changeLookup2 = new Hashtable();
            int n = this.edits.size();
            for (int i = 0; i < n; ++i) {
                Object o = this.edits.elementAt(i);
                if (!(o instanceof DocumentEvent.ElementChange)) continue;
                DocumentEvent.ElementChange ec = (DocumentEvent.ElementChange)o;
                this.changeLookup2.put(ec.getElement(), ec);
            }
        }
        if (this.changeLookup2 != null && anEdit instanceof DocumentEvent.ElementChange) {
            DocumentEvent.ElementChange ec = (DocumentEvent.ElementChange)((Object)anEdit);
            this.changeLookup2.put(ec.getElement(), ec);
        }
        if (!this.inProgress2) {
            return false;
        }
        UndoableEdit last = this.lastEdit();
        if (last == null) {
            this.edits.addElement(anEdit);
        } else if (!last.addEdit(anEdit)) {
            if (anEdit.replaceEdit(last)) {
                this.edits.removeElementAt(this.edits.size() - 1);
            }
            this.edits.addElement(anEdit);
        }
        return true;
    }

    private boolean isLastModifyUndoEdit() {
        return true;
    }

    @Override
    public boolean canUndo() {
        return !this.inProgress2 && this.alive2 && this.hasBeenDone2 && this.isLastModifyUndoEdit();
    }

    @Override
    public boolean canRedo() {
        return !this.inProgress2 && this.alive2 && !this.hasBeenDone2;
    }

    @Override
    public boolean isInProgress() {
        return this.inProgress2;
    }

    @Override
    public String getUndoPresentationName() {
        return "";
    }

    @Override
    public String getRedoPresentationName() {
        return "";
    }

    public boolean canMerge(BaseDocumentEvent evt) {
        if (this.getType() == DocumentEvent.EventType.INSERT) {
            if (evt.getType() == DocumentEvent.EventType.INSERT) {
                String text = this.getText();
                String evtText = evt.getText();
                if ((this.getLength() == 1 || this.getLength() > 1 && Analyzer.isSpace(text)) && (evt.getLength() == 1 || evt.getLength() > 1 && Analyzer.isSpace(evtText)) && evt.getOffset() + evt.getLength() == this.getOffset()) {
                    BaseDocument doc = (BaseDocument)this.getDocument();
                    boolean thisWord = doc.isIdentifierPart(text.charAt(0));
                    boolean lastWord = doc.isIdentifierPart(evtText.charAt(0));
                    if (thisWord && lastWord) {
                        return true;
                    }
                    boolean thisWhite = doc.isWhitespace(text.charAt(0));
                    boolean lastWhite = doc.isWhitespace(evtText.charAt(0));
                    if (lastWhite && thisWhite || !lastWhite && !lastWord && !thisWhite && !thisWord) {
                        return true;
                    }
                }
            }
        } else if (evt.getType() == DocumentEvent.EventType.REMOVE && evt.getType() != DocumentEvent.EventType.INSERT && evt.getType() == DocumentEvent.EventType.REMOVE) {
            String text = this.getText();
            String evtText = evt.getText();
            if ((this.getLength() == 1 || this.getLength() > 1 && Analyzer.isSpace(text)) && (evt.getLength() == 1 || evt.getLength() > 1 && Analyzer.isSpace(evtText)) && (evt.getOffset() - evt.getLength() == this.getOffset() || evt.getOffset() == this.getOffset())) {
                BaseDocument doc = (BaseDocument)this.getDocument();
                boolean thisWord = doc.isIdentifierPart(text.charAt(0));
                boolean lastWord = doc.isIdentifierPart(evtText.charAt(0));
                if (thisWord && lastWord) {
                    return true;
                }
                boolean thisWhite = doc.isWhitespace(text.charAt(0));
                boolean lastWhite = doc.isWhitespace(evtText.charAt(0));
                if (lastWhite && thisWhite || !lastWhite && !lastWord && !thisWhite && !thisWord) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean replaceEdit(UndoableEdit anEdit) {
        BaseDocument doc = (BaseDocument)this.getDocument();
        if (anEdit instanceof BaseDocument.AtomicCompoundEdit) {
            UndoableEdit edit;
            BaseDocument.AtomicCompoundEdit compEdit = (BaseDocument.AtomicCompoundEdit)((Object)anEdit);
            if (!doc.undoMergeReset && compEdit.getEdits().size() == 1 && (edit = (UndoableEdit)compEdit.getEdits().get(0)) instanceof BaseDocumentEvent && this.canMerge((BaseDocumentEvent)edit)) {
                this.previous = anEdit;
                return true;
            }
        } else if (anEdit instanceof BaseDocumentEvent) {
            BaseDocumentEvent evt = (BaseDocumentEvent)anEdit;
            if (!doc.undoMergeReset && this.canMerge(evt)) {
                this.previous = anEdit;
                return true;
            }
        }
        doc.undoMergeReset = false;
        return false;
    }

    @Override
    public void die() {
        int size = this.edits.size();
        for (int i = size - 1; i >= 0; --i) {
            UndoableEdit e = (UndoableEdit)this.edits.elementAt(i);
            e.die();
        }
        this.alive2 = false;
        if (this.previous != null) {
            this.previous.die();
            this.previous = null;
        }
    }

    @Override
    public void end() {
        this.inProgress2 = false;
    }

    @Override
    public DocumentEvent.ElementChange getChange(Element elem) {
        if (this.changeLookup2 != null) {
            return (DocumentEvent.ElementChange)this.changeLookup2.get(elem);
        }
        int n = this.edits.size();
        for (int i = 0; i < n; ++i) {
            DocumentEvent.ElementChange c;
            Object o = this.edits.elementAt(i);
            if (!(o instanceof DocumentEvent.ElementChange) || (c = (DocumentEvent.ElementChange)o).getElement() != elem) continue;
            return c;
        }
        return null;
    }

    @Override
    public String toString() {
        return "" + System.identityHashCode(this) + " " + super.toString() + ", type=" + this.getType() + (this.getType() != DocumentEvent.EventType.CHANGE ? new StringBuilder().append("text='").append(this.getText()).append("'").toString() : "");
    }
}

