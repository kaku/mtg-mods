/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Mark;

public abstract class BaseElement
implements Element {
    public static final String ElementNameAttribute = "$ename";
    protected BaseDocument doc;
    protected BaseElement parent;
    protected AttributeSet attrs;

    public BaseElement(BaseDocument doc, BaseElement parent, AttributeSet attrs) {
        this.doc = doc;
        this.parent = parent;
        this.attrs = attrs;
    }

    @Override
    public Document getDocument() {
        return this.doc;
    }

    @Override
    public Element getParentElement() {
        return this.parent;
    }

    @Override
    public String getName() {
        AttributeSet as = this.attrs;
        if (as != null && as.isDefined("$ename")) {
            return (String)as.getAttribute("$ename");
        }
        return null;
    }

    @Override
    public AttributeSet getAttributes() {
        AttributeSet as = this.attrs;
        return as == null ? SimpleAttributeSet.EMPTY : as;
    }

    @Override
    public abstract int getStartOffset();

    public abstract Mark getStartMark();

    @Override
    public abstract int getEndOffset();

    public abstract Mark getEndMark();

    @Override
    public abstract Element getElement(int var1);

    @Override
    public abstract int getElementIndex(int var1);

    @Override
    public abstract int getElementCount();

    @Override
    public abstract boolean isLeaf();
}

