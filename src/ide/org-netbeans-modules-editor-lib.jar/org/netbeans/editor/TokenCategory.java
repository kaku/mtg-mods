/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

public interface TokenCategory {
    public String getName();

    public int getNumericID();
}

