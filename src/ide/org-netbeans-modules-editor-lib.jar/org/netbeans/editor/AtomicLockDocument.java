/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.Document;
import org.netbeans.editor.AtomicLockListener;

public interface AtomicLockDocument
extends Document {
    public void atomicLock();

    public void atomicUnlock();

    public void atomicUndo();

    public void addAtomicLockListener(AtomicLockListener var1);

    public void removeAtomicLockListener(AtomicLockListener var1);
}

