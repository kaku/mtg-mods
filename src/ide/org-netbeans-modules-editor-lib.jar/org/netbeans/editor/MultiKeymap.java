/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package org.netbeans.editor;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.netbeans.editor.MultiKeyBinding;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public class MultiKeymap
implements Keymap {
    private static final boolean compatibleIgnoreNextTyped = Boolean.getBoolean("netbeans.editor.keymap.compatible");
    private static final Logger LOG = Logger.getLogger(MultiKeymap.class.getName());
    public static final Action EMPTY_ACTION = new AbstractAction(){

        @Override
        public void actionPerformed(ActionEvent evt) {
        }
    };
    public static final Action BEEP_ACTION = new DefaultEditorKit.BeepAction();
    private Keymap delegate;
    private Keymap context;
    private boolean ignoreNextTyped = false;
    private Action contextKeyNotFoundAction = BEEP_ACTION;
    private List contextKeys;

    public MultiKeymap(String name) {
        this.delegate = JTextComponent.addKeymap(name, null);
        this.contextKeys = new ArrayList();
    }

    void setContext(Keymap contextKeymap) {
        this.context = contextKeymap;
    }

    private static String getKeyText(KeyStroke keyStroke) {
        if (keyStroke == null) {
            return "";
        }
        String modifText = KeyEvent.getKeyModifiersText(keyStroke.getModifiers());
        String suffix = Utilities.keyToString((KeyStroke)KeyStroke.getKeyStroke(keyStroke.getKeyCode(), 0));
        if (suffix == null) {
            return "";
        }
        if ("".equals(modifText)) {
            return suffix;
        }
        return modifText + "+" + suffix;
    }

    public void resetContext() {
        this.context = null;
        this.contextKeys.clear();
    }

    private void shiftGlobalContext(KeyStroke key) {
        List globalContextList = this.getGlobalContextList();
        if (globalContextList != null) {
            globalContextList.add(key);
            StringBuffer text = new StringBuffer();
            Iterator it = globalContextList.iterator();
            while (it.hasNext()) {
                text.append(MultiKeymap.getKeyText((KeyStroke)it.next())).append(' ');
            }
            StatusDisplayer.getDefault().setStatusText(text.toString());
        }
        this.contextKeys.add(key);
    }

    private void resetGlobalContext() {
        List globalContextList = this.getGlobalContextList();
        if (globalContextList != null) {
            globalContextList.clear();
            StatusDisplayer.getDefault().setStatusText("");
        }
    }

    private List getGlobalContextList() {
        List globalContextList;
        try {
            ClassLoader sysCL = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            Class nbKeymapClass = Class.forName("org.netbeans.core.NbKeymap", true, sysCL);
            Field contextField = nbKeymapClass.getDeclaredField("context");
            contextField.setAccessible(true);
            globalContextList = (List)contextField.get(null);
        }
        catch (Exception e) {
            globalContextList = null;
        }
        return globalContextList;
    }

    public void setContextKeyNotFoundAction(Action a) {
        this.contextKeyNotFoundAction = a;
    }

    public void load(JTextComponent.KeyBinding[] bindings, Action[] actions) {
        HashMap<String, Action> h = new HashMap<String, Action>(bindings.length);
        for (int i = 0; i < actions.length; ++i) {
            Action a = actions[i];
            String value = (String)a.getValue("Name");
            h.put(value != null ? value : "", a);
        }
        this.load(bindings, h);
    }

    public void load(JTextComponent.KeyBinding[] bindings, Map actions) {
        for (int i = 0; i < bindings.length; ++i) {
            Action a = (Action)actions.get(bindings[i].actionName);
            if (a == null) continue;
            boolean added = false;
            if (bindings[i] instanceof MultiKeyBinding) {
                MultiKeyBinding mb = (MultiKeyBinding)bindings[i];
                if (mb.keys != null) {
                    Keymap cur = this.delegate;
                    for (int j = 0; j < mb.keys.length; ++j) {
                        if (j == mb.keys.length - 1) {
                            cur.addActionForKeyStroke(mb.keys[j], a);
                            continue;
                        }
                        Action sca = cur.getAction(mb.keys[j]);
                        if (!(sca instanceof KeymapSetContextAction)) {
                            sca = new KeymapSetContextAction(JTextComponent.addKeymap(null, null));
                            cur.addActionForKeyStroke(mb.keys[j], sca);
                        }
                        cur = ((KeymapSetContextAction)sca).contextKeymap;
                    }
                    added = true;
                }
            }
            if (added) continue;
            if (bindings[i].key != null) {
                this.delegate.addActionForKeyStroke(bindings[i].key, a);
                continue;
            }
            this.setDefaultAction(a);
        }
    }

    @Override
    public String getName() {
        return this.context != null ? this.context.getName() : this.delegate.getName();
    }

    @Override
    public Action getDefaultAction() {
        return this.delegate.getDefaultAction();
    }

    @Override
    public void setDefaultAction(Action a) {
        if (this.context != null) {
            this.context.setDefaultAction(a);
        } else {
            this.delegate.setDefaultAction(a);
        }
    }

    private Action getActionImpl(KeyStroke key) {
        Action a = null;
        a = this.context != null ? this.context.getAction(key) : this.delegate.getAction(key);
        if (LOG.isLoggable(Level.FINE)) {
            String msg = "MultiKeymap.getActionImpl():\n  KEY=" + key + "\n  ACTION=" + a;
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.INFO, msg, new Exception());
            } else {
                LOG.fine(msg + "\n\n");
            }
        }
        return a;
    }

    private boolean contextKeysEqual(List keys) {
        if (keys.size() != this.contextKeys.size()) {
            return false;
        }
        for (int i = keys.size() - 1; i >= 0; --i) {
            if (this.contextKeys.get(i).equals(keys.get(i))) continue;
            return false;
        }
        return true;
    }

    @Override
    public Action getAction(KeyStroke key) {
        Action ret = null;
        List globalContextList = this.getGlobalContextList();
        if (globalContextList != null && globalContextList.size() > 0 && !this.contextKeysEqual(globalContextList)) {
            int i;
            this.resetContext();
            for (i = 0; i < globalContextList.size(); ++i) {
                Action a = this.getActionImpl((KeyStroke)globalContextList.get(i));
                if (!(a instanceof KeymapSetContextAction)) {
                    this.resetContext();
                    break;
                }
                a.actionPerformed(null);
            }
            if (i != globalContextList.size()) {
                return null;
            }
        }
        if (this.ignoreNextTyped) {
            if (key.isOnKeyRelease()) {
                ret = EMPTY_ACTION;
            } else {
                this.ignoreNextTyped = false;
            }
            if (key.getKeyChar() != '\u0000' && key.getKeyChar() != '\uffff') {
                ret = EMPTY_ACTION;
            }
        }
        if (ret == null) {
            ret = this.getActionImpl(key);
            if (ret instanceof KeymapSetContextAction) {
                this.shiftGlobalContext(key);
            } else {
                if (this.context != null) {
                    this.ignoreNextTyped = true;
                } else if (compatibleIgnoreNextTyped) {
                    if ((key.getModifiers() & 8) != 0 && (key.getModifiers() & 2) == 0) {
                        boolean patch = true;
                        if (key.getKeyChar() == '\u0000' || key.getKeyChar() == '\uffff') {
                            switch (key.getKeyCode()) {
                                case 18: 
                                case 25: 
                                case 96: 
                                case 97: 
                                case 98: 
                                case 99: 
                                case 100: 
                                case 101: 
                                case 102: 
                                case 103: 
                                case 104: 
                                case 105: 
                                case 241: 
                                case 242: 
                                case 259: 
                                case 260: 
                                case 263: {
                                    patch = false;
                                }
                            }
                        }
                        if (patch) {
                            this.ignoreNextTyped = true;
                        }
                    } else if ((key.getModifiers() & 4) != 0) {
                        this.ignoreNextTyped = true;
                    } else if ((key.getModifiers() & 2) != 0 && (key.getModifiers() & 1) != 0 && (key.getKeyCode() == 107 || key.getKeyCode() == 109)) {
                        this.ignoreNextTyped = true;
                    }
                }
                this.resetContext();
            }
            if (this.context == null || ret == null) {
                // empty if block
            }
        }
        if (ret != null && !(ret instanceof KeymapSetContextAction) && ret != EMPTY_ACTION) {
            StringBuilder command = new StringBuilder();
            List list = this.getGlobalContextList();
            if (list != null) {
                for (KeyStroke ks : list) {
                    command.append(MultiKeymap.getKeyText(ks)).append(" ");
                }
            }
            command.append(MultiKeymap.getKeyText(key));
            ret.putValue("ActionCommandKey", command.toString());
            this.resetGlobalContext();
        }
        if (compatibleIgnoreNextTyped && key == KeyStroke.getKeyStroke(32, 2)) {
            this.ignoreNextTyped = true;
        }
        return ret;
    }

    @Override
    public KeyStroke[] getBoundKeyStrokes() {
        return this.context != null ? this.context.getBoundKeyStrokes() : this.delegate.getBoundKeyStrokes();
    }

    @Override
    public Action[] getBoundActions() {
        return this.context != null ? this.context.getBoundActions() : this.delegate.getBoundActions();
    }

    @Override
    public KeyStroke[] getKeyStrokesForAction(Action a) {
        return this.context != null ? this.context.getKeyStrokesForAction(a) : this.delegate.getKeyStrokesForAction(a);
    }

    @Override
    public boolean isLocallyDefined(KeyStroke key) {
        return this.context != null ? this.context.isLocallyDefined(key) : this.delegate.isLocallyDefined(key);
    }

    @Override
    public void addActionForKeyStroke(KeyStroke key, Action a) {
        if (this.context != null) {
            this.context.addActionForKeyStroke(key, a);
        } else {
            this.delegate.addActionForKeyStroke(key, a);
        }
    }

    @Override
    public void removeKeyStrokeBinding(KeyStroke key) {
        if (this.context != null) {
            this.context.removeKeyStrokeBinding(key);
        } else {
            this.delegate.removeKeyStrokeBinding(key);
        }
    }

    @Override
    public void removeBindings() {
        if (this.context != null) {
            this.context.removeBindings();
        } else {
            this.delegate.removeBindings();
        }
    }

    @Override
    public Keymap getResolveParent() {
        return this.context != null ? this.context.getResolveParent() : this.delegate.getResolveParent();
    }

    @Override
    public void setResolveParent(Keymap parent) {
        if (this.context != null) {
            this.context.setResolveParent(parent);
        } else {
            this.delegate.setResolveParent(parent);
        }
    }

    public String toString() {
        return "MK: name=" + this.getName();
    }

    class KeymapSetContextAction
    extends AbstractAction {
        Keymap contextKeymap;
        static final long serialVersionUID = 1034848289049566148L;

        KeymapSetContextAction(Keymap contextKeymap) {
            this.contextKeymap = contextKeymap;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            MultiKeymap.this.setContext(this.contextKeymap);
        }
    }

}

