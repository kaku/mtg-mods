/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.util.HashMap;
import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenContext;

public final class TokenContextPath {
    private static final HashMap registry = new HashMap(199);
    private final TokenContext[] contexts;
    private TokenContextPath parent;
    private TokenContextPath base;
    private String namePrefix;
    private final HashMap tokenNameCache = new HashMap();
    private final HashMap replaceStartCache = new HashMap();

    static synchronized TokenContextPath get(TokenContext[] contexts) {
        if (contexts == null || contexts.length == 0) {
            throw new IllegalArgumentException("Contexts must be valid and non-empty.");
        }
        ArrayMatcher am = new ArrayMatcher(contexts);
        TokenContextPath path = (TokenContextPath)registry.get(am);
        if (path == null) {
            path = new TokenContextPath(contexts);
            registry.put(am, path);
        }
        return path;
    }

    private TokenContextPath(TokenContext[] contexts) {
        this.contexts = contexts;
        if (contexts.length == 1) {
            this.base = this;
        }
    }

    public TokenContext[] getContexts() {
        return this.contexts;
    }

    public int length() {
        return this.contexts.length;
    }

    public TokenContextPath getParent() {
        if (this.parent == null && this.contexts.length > 1) {
            TokenContext[] parentContexts = new TokenContext[this.contexts.length - 1];
            System.arraycopy(this.contexts, 0, parentContexts, 0, this.contexts.length - 1);
            this.parent = TokenContextPath.get(parentContexts);
        }
        return this.parent;
    }

    public TokenContextPath getBase() {
        if (this.base == null) {
            this.base = this.getParent().getBase();
        }
        return this.base;
    }

    public boolean contains(TokenContextPath tcp) {
        if (tcp == this) {
            return true;
        }
        if (this.contexts.length > 1) {
            return this.getParent().contains(tcp);
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TokenContextPath replaceStart(TokenContextPath byPath) {
        if (this.contexts.length < byPath.contexts.length) {
            throw new IllegalArgumentException("byPath=" + byPath + " is too long.");
        }
        HashMap hashMap = this.replaceStartCache;
        synchronized (hashMap) {
            TokenContextPath ret = (TokenContextPath)this.replaceStartCache.get(byPath);
            if (ret == null) {
                TokenContext[] targetContexts = (TokenContext[])this.contexts.clone();
                for (int i = byPath.contexts.length - 1; i >= 0; --i) {
                    targetContexts[i] = byPath.contexts[i];
                }
                ret = TokenContextPath.get(targetContexts);
                this.replaceStartCache.put(byPath, ret);
            }
            return ret;
        }
    }

    public String getNamePrefix() {
        if (this.namePrefix == null) {
            this.namePrefix = this.contexts.length == 1 ? this.contexts[0].getNamePrefix() : (this.contexts[this.contexts.length - 1].getNamePrefix() + this.getParent().getNamePrefix()).intern();
        }
        return this.namePrefix;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getFullTokenName(TokenCategory tokenIDOrCategory) {
        String fullName;
        String tokenName = tokenIDOrCategory.getName();
        HashMap hashMap = this.tokenNameCache;
        synchronized (hashMap) {
            fullName = (String)this.tokenNameCache.get(tokenName);
            if (fullName == null) {
                fullName = (this.getNamePrefix() + tokenName).intern();
                this.tokenNameCache.put(tokenName, fullName);
            }
        }
        return fullName;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("|");
        for (int i = 0; i < this.contexts.length; ++i) {
            String shortName = this.contexts[i].getClass().getName();
            shortName = shortName.substring(shortName.lastIndexOf(46) + 1);
            sb.append('<');
            sb.append(shortName);
            sb.append('>');
        }
        sb.append('|');
        return sb.toString();
    }

    private static final class ArrayMatcher {
        private int hash;
        private TokenContext[] contexts;

        ArrayMatcher(TokenContext[] contexts) {
            this.contexts = contexts;
        }

        public int hashCode() {
            int h = this.hash;
            if (h == 0) {
                for (int i = this.contexts.length - 1; i >= 0; --i) {
                    h = h * 31 + this.contexts[i].hashCode();
                }
                this.hash = h;
            }
            return h;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o instanceof ArrayMatcher) {
                ArrayMatcher am = (ArrayMatcher)o;
                if (this.contexts.length == am.contexts.length) {
                    for (int i = this.contexts.length - 1; i >= 0; --i) {
                        if (this.contexts[i].equals(am.contexts[i])) continue;
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }
    }

}

