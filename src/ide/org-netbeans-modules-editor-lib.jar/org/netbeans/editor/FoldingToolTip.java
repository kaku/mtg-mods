/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.PopupManager;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;

@Deprecated
public class FoldingToolTip
extends JPanel {
    View view;
    EditorUI editorUI;
    public static final int BORDER_WIDTH = 2;

    public FoldingToolTip(View view, EditorUI editorUI) {
        this.view = view;
        this.editorUI = editorUI;
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)editorUI.getComponent())).lookup(FontColorSettings.class);
        AttributeSet attribs = fcs.getFontColors("default");
        Color foreColor = (Color)attribs.getAttribute(StyleConstants.Foreground);
        if (foreColor == null) {
            foreColor = Color.black;
        }
        this.setBorder(new LineBorder(foreColor));
        this.setOpaque(true);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension prefSize = this.editorUI.getComponent().getPreferredSize();
        int viewHeight = (int)this.view.getPreferredSpan(1);
        prefSize.height = viewHeight + 4;
        prefSize.width += this.editorUI.getSideBarWidth() + 4;
        return prefSize;
    }

    @Override
    public void setSize(Dimension d) {
        this.setSize(d.width, d.height);
    }

    @Override
    public void setSize(int width, int height) {
        int viewHeight = (int)this.view.getPreferredSpan(1);
        int viewWidth = (int)this.view.getPreferredSpan(0);
        if (height < 30) {
            this.putClientProperty(PopupManager.Placement.class, null);
        } else {
            height = Math.min(height, viewHeight);
        }
        width = Math.min(width, viewWidth);
        super.setSize(width, height += 4);
    }

    private void updateRenderingHints(Graphics g) {
        FontColorSettings fcs;
        Map renderingHints;
        String mimeType;
        JTextComponent comp = this.editorUI.getComponent();
        if (comp != null && (renderingHints = (Map)(fcs = (FontColorSettings)MimeLookup.getLookup((String)(mimeType = DocumentUtilities.getMimeType((JTextComponent)comp))).lookup(FontColorSettings.class)).getFontColors("default").getAttribute(EditorStyleConstants.RenderingHints)) != null) {
            ((Graphics2D)g).addRenderingHints(renderingHints);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        this.updateRenderingHints(g);
        Rectangle shape = new Rectangle(this.getSize());
        Rectangle clip = g.getClipBounds();
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)this.editorUI.getComponent())).lookup(FontColorSettings.class);
        AttributeSet attribs = fcs.getFontColors("default");
        Color backColor = (Color)attribs.getAttribute(StyleConstants.Background);
        if (backColor == null) {
            backColor = Color.white;
        }
        g.setColor(backColor);
        g.fillRect(clip.x, clip.y, clip.width, clip.height);
        g.translate(2, 2);
        JTextComponent component = this.editorUI.getComponent();
        if (component == null) {
            return;
        }
        int sideBarWidth = this.editorUI.getSideBarWidth();
        g.translate(-2, -2);
        g.setColor(backColor);
        for (int i = 1; i <= 2; ++i) {
            g.drawRect(clip.x + i, clip.y + i, clip.width - i * 2 - 1, clip.height - i * 2 - 1);
        }
    }
}

