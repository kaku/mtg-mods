/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.editor;

import java.util.Comparator;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;

public class Mark {
    private static final MarkComparator MARK_COMPARATOR = new MarkComparator();
    private BaseDocument doc;
    private Position pos;
    private Position.Bias bias;

    public Mark() {
        this(Position.Bias.Forward);
    }

    public Mark(Position.Bias bias) {
        this.bias = bias;
    }

    public Mark(boolean backwardBias) {
        this(backwardBias ? Position.Bias.Backward : Position.Bias.Forward);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void insert(BaseDocument doc, int offset) throws InvalidMarkException, BadLocationException {
        BaseDocument ldoc = this.doc;
        if (ldoc != null) {
            throw new InvalidMarkException("Mark already inserted: mark=" + this + ", class=" + this.getClass());
        }
        BaseDocument baseDocument = ldoc = (this.doc = doc);
        synchronized (baseDocument) {
            if (this.pos != null) {
                throw new IllegalStateException("Mark already inserted: mark=" + this + ", class=" + this.getClass());
            }
            if (offset < 0 || offset > ldoc.getLength() + 1) {
                throw new BadLocationException("Invalid offset", offset);
            }
            if (offset <= ldoc.getLength() && Character.isLowSurrogate(DocumentUtilities.getText((Document)ldoc).charAt(offset))) {
                if (this.bias == Position.Bias.Forward && offset < ldoc.getLength()) {
                    ++offset;
                } else if (this.bias == Position.Bias.Backward && offset > 0) {
                    --offset;
                }
            }
            this.pos = doc.createPosition(offset, this.bias);
        }
    }

    void move(BaseDocument doc, int newOffset) throws InvalidMarkException, BadLocationException {
        this.dispose();
        this.insert(doc, newOffset);
    }

    public final int getOffset() throws InvalidMarkException {
        BaseDocument ldoc = this.doc;
        if (ldoc != null) {
            BaseDocument baseDocument = ldoc;
            synchronized (baseDocument) {
                if (this.pos != null) {
                    return this.pos.getOffset();
                }
                throw new InvalidMarkException();
            }
        }
        throw new InvalidMarkException();
    }

    public final int getLine() throws InvalidMarkException {
        BaseDocument ldoc = this.doc;
        if (ldoc != null) {
            BaseDocument baseDocument = ldoc;
            synchronized (baseDocument) {
                if (this.pos != null) {
                    int offset = this.pos.getOffset();
                    Element lineRoot = ldoc.getParagraphElement(0).getParentElement();
                    return lineRoot.getElementIndex(offset);
                }
                throw new InvalidMarkException();
            }
        }
        throw new InvalidMarkException();
    }

    public final boolean getInsertAfter() {
        return this.bias == Position.Bias.Backward;
    }

    public final boolean getBackwardBias() {
        return this.getInsertAfter();
    }

    public final Position.Bias getBias() {
        return this.bias;
    }

    int getBiasAsInt() {
        return this.bias == Position.Bias.Backward ? -1 : 1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void dispose() {
        BaseDocument ldoc = this.doc;
        if (ldoc != null) {
            BaseDocument baseDocument = ldoc;
            synchronized (baseDocument) {
                if (this.pos != null) {
                    this.pos = null;
                    this.doc = null;
                    return;
                }
            }
        }
        throw new IllegalStateException("Mark already disposed: mark=" + this + ", class=" + this.getClass());
    }

    public final void remove() throws InvalidMarkException {
        this.dispose();
    }

    public final int compare(int pos) throws InvalidMarkException {
        return this.getOffset() - pos;
    }

    protected void removeUpdateAction(int pos, int len) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final boolean isValid() {
        BaseDocument ldoc = this.doc;
        if (ldoc != null) {
            BaseDocument baseDocument = ldoc;
            synchronized (baseDocument) {
                return this.pos != null;
            }
        }
        return false;
    }

    public String toString() {
        return "offset=" + (this.isValid() ? Integer.toString(this.pos.getOffset()) : "<invalid>") + ", bias=" + this.bias;
    }

    private static final class MarkComparator
    implements Comparator {
        private MarkComparator() {
        }

        public int compare(Object o1, Object o2) {
            Mark m1 = (Mark)o1;
            Mark m2 = (Mark)o2;
            try {
                int offDiff = m1.getOffset() - m2.getOffset();
                if (offDiff != 0) {
                    return offDiff;
                }
                return m1.getBiasAsInt() - m2.getBiasAsInt();
            }
            catch (InvalidMarkException e) {
                throw new IllegalStateException(e.toString());
            }
        }
    }

}

