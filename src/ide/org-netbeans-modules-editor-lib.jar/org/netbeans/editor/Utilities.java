/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.editor.indent.spi.CodeStylePreferences
 *  org.netbeans.modules.editor.lib2.view.DocumentView
 *  org.netbeans.modules.editor.lib2.view.EditorView
 *  org.openide.util.Exceptions
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.TextAction;
import javax.swing.text.View;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Finder;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.ImplementationProvider;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.StatusBar;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.indent.spi.CodeStylePreferences;
import org.netbeans.modules.editor.lib.BeforeSaveTasks;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.openide.util.Exceptions;
import org.openide.util.Mutex;
import org.openide.util.NbBundle;

public class Utilities {
    private static final String WRONG_POSITION_LOCALE = "wrong_position";
    public static final int CASE_UPPER = 0;
    public static final int CASE_LOWER = 1;
    public static final int CASE_SWITCH = 2;
    private static TextAction focusedComponentAction;
    private static final Logger logger;
    private static Position.Bias[] discardBias;
    private static final String NO_ACTION = "no-action";

    private Utilities() {
    }

    public static int getRowStart(JTextComponent c, int offset) throws BadLocationException {
        Rectangle2D r = Utilities.modelToView(c, offset);
        if (r == null) {
            return -1;
        }
        EditorUI eui = Utilities.getEditorUI(c);
        if (eui != null) {
            return Utilities.viewToModel(c, eui.textLeftMarginWidth, r.getY());
        }
        return -1;
    }

    public static int getRowStart(BaseDocument doc, int offset) throws BadLocationException {
        return Utilities.getRowStart(doc, offset, 0);
    }

    public static int getRowStart(BaseDocument doc, int offset, int lineShift) throws BadLocationException {
        Utilities.checkOffsetValid(doc, offset);
        if (lineShift != 0) {
            Element lineRoot = doc.getParagraphElement(0).getParentElement();
            int line = lineRoot.getElementIndex(offset);
            if ((line += lineShift) < 0 || line >= lineRoot.getElementCount()) {
                return -1;
            }
            return lineRoot.getElement(line).getStartOffset();
        }
        return doc.getParagraphElement(offset).getStartOffset();
    }

    public static int getRowFirstNonWhite(BaseDocument doc, int offset) throws BadLocationException {
        Utilities.checkOffsetValid(doc, offset);
        Element lineElement = doc.getParagraphElement(offset);
        return Utilities.getFirstNonWhiteFwd(doc, lineElement.getStartOffset(), lineElement.getEndOffset() - 1);
    }

    public static int getRowLastNonWhite(BaseDocument doc, int offset) throws BadLocationException {
        Utilities.checkOffsetValid(doc, offset);
        Element lineElement = doc.getParagraphElement(offset);
        return Utilities.getFirstNonWhiteBwd(doc, lineElement.getEndOffset() - 1, lineElement.getStartOffset());
    }

    public static int getRowIndent(BaseDocument doc, int offset) throws BadLocationException {
        if ((offset = Utilities.getRowFirstNonWhite(doc, offset)) == -1) {
            return -1;
        }
        return doc.getVisColFromPos(offset);
    }

    public static int getRowIndent(BaseDocument doc, int offset, boolean downDir) throws BadLocationException {
        int p = Utilities.getRowFirstNonWhite(doc, offset);
        if (p == -1) {
            p = Utilities.getFirstNonWhiteRow(doc, offset, downDir);
            if (p == -1) {
                return -1;
            }
            if ((p = Utilities.getRowFirstNonWhite(doc, p)) == -1) {
                return -1;
            }
        }
        return doc.getVisColFromPos(p);
    }

    public static int getRowEnd(JTextComponent c, int offset) throws BadLocationException {
        Rectangle2D r = Utilities.modelToView(c, offset);
        if (r == null) {
            return -1;
        }
        return Utilities.viewToModel(c, 2.147483647E9, r.getY());
    }

    public static int getRowEnd(BaseDocument doc, int offset) throws BadLocationException {
        Utilities.checkOffsetValid(doc, offset);
        return doc.getParagraphElement(offset).getEndOffset() - 1;
    }

    private static int findBestSpan(JTextComponent c, int lineBegin, int lineEnd, int x) throws BadLocationException {
        if (lineBegin == lineEnd) {
            return lineEnd;
        }
        int low = lineBegin;
        int high = lineEnd;
        while (low <= high) {
            if (high - low < 3) {
                int bestSpan = Integer.MAX_VALUE;
                int bestPos = -1;
                for (int i = low; i <= high; ++i) {
                    Rectangle tempRect = c.modelToView(i);
                    if (Math.abs(x - tempRect.x) >= bestSpan) continue;
                    bestSpan = Math.abs(x - tempRect.x);
                    bestPos = i;
                }
                return bestPos;
            }
            int mid = (low + high) / 2;
            Rectangle tempRect = c.modelToView(mid);
            if (tempRect.x > x) {
                high = mid;
                continue;
            }
            if (tempRect.x < x) {
                low = mid;
                continue;
            }
            return mid;
        }
        return lineBegin;
    }

    public static int getPositionAbove(JTextComponent c, int offset, int x) throws BadLocationException {
        offset = c.getUI().getNextVisualPositionFrom(c, offset, Position.Bias.Forward, 1, null);
        return offset;
    }

    public static int getPositionBelow(JTextComponent c, int offset, int x) throws BadLocationException {
        offset = c.getUI().getNextVisualPositionFrom(c, offset, Position.Bias.Forward, 5, null);
        return offset;
    }

    public static int getWordStart(JTextComponent c, int offset) throws BadLocationException {
        return Utilities.getWordStart((BaseDocument)c.getDocument(), offset);
    }

    public static int getWordStart(BaseDocument doc, int offset) throws BadLocationException {
        return doc.find(new FinderFactory.PreviousWordBwdFinder(doc, false, true), offset, 0);
    }

    public static int getWordEnd(JTextComponent c, int offset) throws BadLocationException {
        return Utilities.getWordEnd((BaseDocument)c.getDocument(), offset);
    }

    public static int getWordEnd(BaseDocument doc, int offset) throws BadLocationException {
        int ret = doc.find(new FinderFactory.NextWordFwdFinder(doc, false, true), offset, -1);
        return ret > 0 ? ret : doc.getLength();
    }

    public static int getNextWord(JTextComponent c, int offset) throws BadLocationException {
        int nextWordOffset = Utilities.getNextWord((BaseDocument)c.getDocument(), offset);
        int nextVisualPosition = -1;
        if (nextWordOffset > 0) {
            nextVisualPosition = c.getUI().getNextVisualPositionFrom(c, nextWordOffset - 1, Position.Bias.Forward, 3, null);
        }
        return nextVisualPosition == -1 ? nextWordOffset : nextVisualPosition;
    }

    public static int getNextWord(BaseDocument doc, int offset) throws BadLocationException {
        Finder nextWordFinder = (Finder)doc.getProperty("next-word-finder");
        offset = doc.find(nextWordFinder, offset, -1);
        if (offset < 0) {
            offset = doc.getLength();
        }
        return offset;
    }

    public static int getPreviousWord(JTextComponent c, int offset) throws BadLocationException {
        int prevWordOffset = Utilities.getPreviousWord((BaseDocument)c.getDocument(), offset);
        int nextVisualPosition = c.getUI().getNextVisualPositionFrom(c, prevWordOffset, Position.Bias.Forward, 7, null);
        if (nextVisualPosition == 0 && prevWordOffset == 0) {
            return 0;
        }
        return nextVisualPosition + 1 == prevWordOffset ? prevWordOffset : nextVisualPosition + 1;
    }

    public static int getPreviousWord(BaseDocument doc, int offset) throws BadLocationException {
        Finder prevWordFinder = (Finder)doc.getProperty("previous-word-finder");
        offset = doc.find(prevWordFinder, offset, 0);
        if (offset < 0) {
            offset = 0;
        }
        return offset;
    }

    public static int getFirstWhiteFwd(BaseDocument doc, int offset) throws BadLocationException {
        return Utilities.getFirstWhiteFwd(doc, offset, -1);
    }

    public static int getFirstWhiteFwd(BaseDocument doc, int offset, int limitPos) throws BadLocationException {
        return doc.find(new FinderFactory.WhiteFwdFinder(doc), offset, limitPos);
    }

    public static int getFirstNonWhiteFwd(BaseDocument doc, int offset) throws BadLocationException {
        return Utilities.getFirstNonWhiteFwd(doc, offset, -1);
    }

    public static int getFirstNonWhiteFwd(BaseDocument doc, int offset, int limitPos) throws BadLocationException {
        return doc.find(new FinderFactory.NonWhiteFwdFinder(doc), offset, limitPos);
    }

    public static int getFirstWhiteBwd(BaseDocument doc, int offset) throws BadLocationException {
        return Utilities.getFirstWhiteBwd(doc, offset, 0);
    }

    public static int getFirstWhiteBwd(BaseDocument doc, int offset, int limitPos) throws BadLocationException {
        return doc.find(new FinderFactory.WhiteBwdFinder(doc), offset, limitPos);
    }

    public static int getFirstNonWhiteBwd(BaseDocument doc, int offset) throws BadLocationException {
        return Utilities.getFirstNonWhiteBwd(doc, offset, 0);
    }

    public static int getFirstNonWhiteBwd(BaseDocument doc, int offset, int limitPos) throws BadLocationException {
        return doc.find(new FinderFactory.NonWhiteBwdFinder(doc), offset, limitPos);
    }

    public static int getLineOffset(BaseDocument doc, int offset) throws BadLocationException {
        Utilities.checkOffsetValid(doc, offset);
        Element lineRoot = doc.getParagraphElement(0).getParentElement();
        return lineRoot.getElementIndex(offset);
    }

    public static int getRowStartFromLineOffset(BaseDocument doc, int lineIndex) {
        Element lineRoot = doc.getParagraphElement(0).getParentElement();
        if (lineIndex < 0 || lineIndex >= lineRoot.getElementCount()) {
            return -1;
        }
        return lineRoot.getElement(lineIndex).getStartOffset();
    }

    public static int getVisualColumn(BaseDocument doc, int offset) throws BadLocationException {
        int docLen = doc.getLength();
        if (offset == docLen + 1) {
            offset = docLen;
        }
        return doc.getVisColFromPos(offset);
    }

    public static String getIdentifier(BaseDocument doc, int offset) throws BadLocationException {
        int[] blk = Utilities.getIdentifierBlock(doc, offset);
        return blk != null ? doc.getText(blk[0], blk[1] - blk[0]) : null;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static int[] getIdentifierBlock(JTextComponent c, int offset) throws BadLocationException {
        CharSequence id = null;
        int[] ret = null;
        Document doc = c.getDocument();
        int idStart = javax.swing.text.Utilities.getWordStart(c, offset);
        if (idStart < 0) return ret;
        int idEnd = javax.swing.text.Utilities.getWordEnd(c, idStart);
        if (idEnd < 0) return ret;
        id = DocumentUtilities.getText((Document)doc, (int)idStart, (int)(idEnd - idStart));
        ret = new int[]{idStart, idEnd};
        CharSequence trim = CharSequenceUtilities.trim((CharSequence)id);
        if (trim.length() == 0 || trim.length() == 1 && !Character.isJavaIdentifierPart(trim.charAt(0))) {
            int prevWordStart = javax.swing.text.Utilities.getPreviousWord(c, offset);
            if (offset != javax.swing.text.Utilities.getWordEnd(c, prevWordStart)) return null;
            return new int[]{prevWordStart, offset};
        }
        if (id == null) return ret;
        if (id.length() == 0) return ret;
        if (CharSequenceUtilities.indexOf((CharSequence)id, (int)46) == -1) return ret;
        int index = offset - idStart;
        int begin = CharSequenceUtilities.lastIndexOf((CharSequence)id.subSequence(0, index), (int)46);
        begin = begin == -1 ? 0 : begin + 1;
        int end = CharSequenceUtilities.indexOf((CharSequence)id, (int)46, (int)index);
        end = end == -1 ? id.length() : end;
        return new int[]{idStart + begin, idStart + end};
    }

    public static int[] getIdentifierBlock(BaseDocument doc, int offset) throws BadLocationException {
        int idEnd;
        int[] ret = null;
        int idStart = Utilities.getWordStart(doc, offset);
        if (idStart >= 0 && (idEnd = Utilities.getWordEnd(doc, idStart)) >= 0) {
            String id = doc.getText(idStart, idEnd - idStart);
            if (doc.getSyntaxSupport().isIdentifier(id)) {
                ret = new int[]{idStart, idEnd};
            } else {
                id = Utilities.getWord(doc, offset);
                if (doc.getSyntaxSupport().isIdentifier(id)) {
                    ret = new int[]{offset, offset + id.length()};
                }
            }
        }
        return ret;
    }

    public static String getWord(JTextComponent c, int offset) throws BadLocationException {
        int[] blk = Utilities.getIdentifierBlock(c, offset);
        Document doc = c.getDocument();
        return blk != null ? doc.getText(blk[0], blk[1] - blk[0]) : null;
    }

    public static int[] getSelectionOrIdentifierBlock(JTextComponent c, int offset) throws BadLocationException {
        Document doc = c.getDocument();
        Caret caret = c.getCaret();
        int[] ret = Utilities.isSelectionShowing(caret) ? new int[]{c.getSelectionStart(), c.getSelectionEnd()} : (doc instanceof BaseDocument ? Utilities.getIdentifierBlock((BaseDocument)doc, offset) : Utilities.getIdentifierBlock(c, offset));
        return ret;
    }

    public static int[] getSelectionOrIdentifierBlock(JTextComponent c) {
        try {
            return Utilities.getSelectionOrIdentifierBlock(c, c.getCaret().getDot());
        }
        catch (BadLocationException e) {
            return null;
        }
    }

    public static String getIdentifierBefore(BaseDocument doc, int offset) throws BadLocationException {
        int wordStart = Utilities.getWordStart(doc, offset);
        if (wordStart != -1) {
            String word = new String(doc.getChars(wordStart, offset - wordStart), 0, offset - wordStart);
            if (doc.getSyntaxSupport().isIdentifier(word)) {
                return word;
            }
        }
        return null;
    }

    public static String getSelectionOrIdentifier(JTextComponent c, int offset) throws BadLocationException {
        String ret;
        Document doc = c.getDocument();
        Caret caret = c.getCaret();
        if (Utilities.isSelectionShowing(caret) && (ret = c.getSelectedText()) != null) {
            return ret;
        }
        ret = doc instanceof BaseDocument ? Utilities.getIdentifier((BaseDocument)doc, offset) : Utilities.getWord(c, offset);
        return ret;
    }

    public static String getSelectionOrIdentifier(JTextComponent c) {
        try {
            return Utilities.getSelectionOrIdentifier(c, c.getCaret().getDot());
        }
        catch (BadLocationException e) {
            return null;
        }
    }

    public static String getWord(BaseDocument doc, int offset) throws BadLocationException {
        int wordEnd = Utilities.getWordEnd(doc, offset);
        if (wordEnd != -1) {
            return new String(doc.getChars(offset, wordEnd - offset), 0, wordEnd - offset);
        }
        return null;
    }

    public static boolean changeCase(final BaseDocument doc, final int offset, int len, int type) throws BadLocationException {
        int i;
        final char[] orig = doc.getChars(offset, len);
        final char[] changed = (char[])orig.clone();
        block5 : for (i = 0; i < orig.length; ++i) {
            switch (type) {
                case 0: {
                    changed[i] = Character.toUpperCase(orig[i]);
                    continue block5;
                }
                case 1: {
                    changed[i] = Character.toLowerCase(orig[i]);
                    continue block5;
                }
                case 2: {
                    if (Character.isUpperCase(orig[i])) {
                        changed[i] = Character.toLowerCase(orig[i]);
                        continue block5;
                    }
                    if (!Character.isLowerCase(orig[i])) continue block5;
                    changed[i] = Character.toUpperCase(orig[i]);
                }
            }
        }
        for (i = 0; i < orig.length; ++i) {
            if (orig[i] == changed[i]) continue;
            final BadLocationException[] badLocationExceptions = new BadLocationException[1];
            doc.runAtomicAsUser(new Runnable(){

                @Override
                public void run() {
                    try {
                        Position pos = doc.createPosition(offset);
                        doc.remove(pos.getOffset(), orig.length);
                        doc.insertString(pos.getOffset(), new String(changed), null);
                    }
                    catch (BadLocationException ex) {
                        badLocationExceptions[0] = ex;
                    }
                }
            });
            if (badLocationExceptions[0] != null) {
                throw badLocationExceptions[0];
            }
            return true;
        }
        return false;
    }

    public static boolean isRowEmpty(BaseDocument doc, int offset) throws BadLocationException {
        Element lineElement = doc.getParagraphElement(offset);
        return lineElement.getStartOffset() + 1 == lineElement.getEndOffset();
    }

    public static int getFirstNonEmptyRow(BaseDocument doc, int offset, boolean downDir) throws BadLocationException {
        while (offset != -1 && Utilities.isRowEmpty(doc, offset)) {
            offset = Utilities.getRowStart(doc, offset, downDir ? 1 : -1);
        }
        return offset;
    }

    public static boolean isRowWhite(BaseDocument doc, int offset) throws BadLocationException {
        Element lineElement = doc.getParagraphElement(offset);
        offset = doc.find(new FinderFactory.NonWhiteFwdFinder(doc), lineElement.getStartOffset(), lineElement.getEndOffset() - 1);
        return offset == -1;
    }

    public static int getFirstNonWhiteRow(BaseDocument doc, int offset, boolean downDir) throws BadLocationException {
        if (Utilities.isRowWhite(doc, offset)) {
            offset = downDir ? Utilities.getFirstNonWhiteFwd(doc, offset) : Utilities.getFirstNonWhiteBwd(doc, offset);
        }
        return offset;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static int reformat(final BaseDocument doc, final int startOffset, final int endOffset) throws BadLocationException {
        final Reformat formatter = Reformat.get((Document)doc);
        formatter.lock();
        try {
            final Object[] result = new Object[1];
            doc.runAtomicAsUser(new Runnable(){

                @Override
                public void run() {
                    try {
                        Position endPos = doc.createPosition(endOffset);
                        formatter.reformat(startOffset, endOffset);
                        result[0] = Math.max(endPos.getOffset() - startOffset, 0);
                    }
                    catch (BadLocationException ex) {
                        result[0] = ex;
                    }
                }
            });
            if (result[0] instanceof BadLocationException) {
                throw (BadLocationException)result[0];
            }
            int n = (Integer)result[0];
            return n;
        }
        finally {
            formatter.unlock();
        }
    }

    public static void reformatLine(BaseDocument doc, int pos) throws BadLocationException {
        int lineStart = Utilities.getRowStart(doc, pos);
        int lineEnd = Utilities.getRowEnd(doc, pos);
        Utilities.reformat(doc, lineStart, lineEnd);
    }

    public static int getRowCount(BaseDocument doc, int startPos, int endPos) throws BadLocationException {
        if (startPos > endPos) {
            return 0;
        }
        Element lineRoot = doc.getParagraphElement(0).getParentElement();
        return lineRoot.getElementIndex(endPos) - lineRoot.getElementIndex(startPos) + 1;
    }

    public static int getRowCount(BaseDocument doc) {
        return doc.getParagraphElement(0).getParentElement().getElementCount();
    }

    public static String getTabInsertString(BaseDocument doc, int offset) throws BadLocationException {
        int col = Utilities.getVisualColumn(doc, offset);
        Preferences prefs = CodeStylePreferences.get((Document)doc).getPreferences();
        boolean expandTabs = prefs.getBoolean("expand-tabs", true);
        if (expandTabs) {
            int spacesPerTab = prefs.getInt("spaces-per-tab", 4);
            if (spacesPerTab <= 0) {
                return "";
            }
            int len = (col + spacesPerTab) / spacesPerTab * spacesPerTab - col;
            return new String(Analyzer.getSpacesBuffer(len), 0, len);
        }
        return "\t";
    }

    public static int getNextTabColumn(BaseDocument doc, int offset) throws BadLocationException {
        int col = Utilities.getVisualColumn(doc, offset);
        Preferences prefs = CodeStylePreferences.get((Document)doc).getPreferences();
        int tabSize = prefs.getInt("spaces-per-tab", 4);
        return tabSize <= 0 ? col : (col + tabSize) / tabSize * tabSize;
    }

    public static void setStatusText(JTextComponent c, String text) {
        StatusBar sb;
        EditorUI eui = Utilities.getEditorUI(c);
        StatusBar statusBar = sb = eui == null ? null : eui.getStatusBar();
        if (sb != null) {
            sb.setText("main", text);
        }
    }

    public static void setStatusText(JTextComponent c, String text, int importance) {
        StatusBar sb;
        EditorUI eui = Utilities.getEditorUI(c);
        StatusBar statusBar = sb = eui == null ? null : eui.getStatusBar();
        if (sb != null) {
            sb.setText(text, importance);
        }
    }

    public static void setStatusText(JTextComponent c, String text, Coloring extraColoring) {
        StatusBar sb;
        EditorUI eui = Utilities.getEditorUI(c);
        StatusBar statusBar = sb = eui == null ? null : eui.getStatusBar();
        if (sb != null) {
            sb.setText("main", text, extraColoring);
        }
    }

    public static void setStatusBoldText(JTextComponent c, String text) {
        StatusBar sb;
        EditorUI eui = Utilities.getEditorUI(c);
        StatusBar statusBar = sb = eui == null ? null : eui.getStatusBar();
        if (sb != null) {
            sb.setBoldText("main", text);
        }
    }

    public static String getStatusText(JTextComponent c) {
        EditorUI eui = Utilities.getEditorUI(c);
        StatusBar sb = eui == null ? null : eui.getStatusBar();
        return sb != null ? sb.getText("main") : null;
    }

    public static void clearStatusText(JTextComponent c) {
        Utilities.setStatusText(c, "");
    }

    public static void insertMark(BaseDocument doc, Mark mark, int offset) throws BadLocationException, InvalidMarkException {
        mark.insert(doc, offset);
    }

    public static void moveMark(BaseDocument doc, Mark mark, int newOffset) throws BadLocationException, InvalidMarkException {
        mark.move(doc, newOffset);
    }

    public static void returnFocus() {
        JTextComponent c = Utilities.getLastActiveComponent();
        if (c != null) {
            Utilities.requestFocus(c);
        }
    }

    public static void requestFocus(JTextComponent c) {
        if (c != null && !ImplementationProvider.getDefault().activateComponent(c)) {
            Frame f = EditorUI.getParentFrame(c);
            if (f != null) {
                f.requestFocus();
            }
            c.requestFocus();
        }
    }

    public static void runInEventDispatchThread(Runnable r) {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }

    public static String debugPosition(BaseDocument doc, int offset) {
        return Utilities.debugPosition(doc, offset, ":");
    }

    public static String debugPosition(BaseDocument doc, int offset, String separator) {
        String ret;
        if (offset >= 0) {
            try {
                int line = Utilities.getLineOffset(doc, offset) + 1;
                int col = Utilities.getVisualColumn(doc, offset) + 1;
                ret = String.valueOf(line) + separator + String.valueOf(col);
            }
            catch (BadLocationException e) {
                ret = NbBundle.getBundle(BaseKit.class).getString("wrong_position") + ' ' + offset + " > " + doc.getLength();
            }
        } else {
            ret = String.valueOf(offset);
        }
        return ret;
    }

    public static String offsetToLineColumnString(BaseDocument doc, int offset) {
        return String.valueOf(offset) + "[" + Utilities.debugPosition(doc, offset) + "]";
    }

    public static String debugDocument(Document doc) {
        return "<" + System.identityHashCode(doc) + ", title='" + doc.getProperty("title") + "', stream='" + doc.getProperty("stream") + ", " + doc.toString() + ">";
    }

    public static void performAction(Action a, ActionEvent evt, JTextComponent target) {
        if (a instanceof BaseAction) {
            ((BaseAction)a).actionPerformed(evt, target);
        } else {
            a.actionPerformed(evt);
        }
    }

    public static JTextComponent getLastActiveComponent() {
        return EditorRegistry.lastFocusedComponent();
    }

    public static JTextComponent getFocusedComponent() {
        if (focusedComponentAction == null) {
            class FocusedComponentAction
            extends TextAction {
                FocusedComponentAction() {
                    super("focused-component");
                }

                JTextComponent getFocusedComponent2() {
                    return this.getFocusedComponent();
                }

                @Override
                public void actionPerformed(ActionEvent evt) {
                }
            }
            focusedComponentAction = new FocusedComponentAction();
        }
        return ((FocusedComponentAction)focusedComponentAction).getFocusedComponent2();
    }

    public static EditorUI getEditorUI(JTextComponent target) {
        TextUI ui = target.getUI();
        return ui instanceof BaseTextUI ? ((BaseTextUI)ui).getEditorUI() : null;
    }

    public static BaseKit getKit(JTextComponent target) {
        if (target == null) {
            return null;
        }
        EditorKit ekit = target.getUI().getEditorKit(target);
        return ekit instanceof BaseKit ? (BaseKit)ekit : null;
    }

    public static Class getKitClass(JTextComponent target) {
        EditorKit kit = target != null ? target.getUI().getEditorKit(target) : null;
        return kit != null ? kit.getClass() : null;
    }

    public static BaseDocument getDocument(JTextComponent target) {
        Document doc = target.getDocument();
        return doc instanceof BaseDocument ? (BaseDocument)doc : null;
    }

    public static SyntaxSupport getSyntaxSupport(JTextComponent target) {
        Document doc = target.getDocument();
        return doc instanceof BaseDocument ? ((BaseDocument)doc).getSyntaxSupport() : null;
    }

    public static View getRootView(JTextComponent component, Class rootViewClass) {
        View view = null;
        TextUI textUI = component.getUI();
        if (textUI != null) {
            for (view = textUI.getRootView((JTextComponent)component); view != null && !rootViewClass.isInstance(view) && view.getViewCount() == 1; view = view.getView((int)0)) {
            }
        }
        return view;
    }

    public static View getDocumentView(JTextComponent component) {
        return Utilities.getRootView(component, DocumentView.class);
    }

    public static void runViewHierarchyTransaction(final JTextComponent component, boolean readLockDocument, final Runnable r) {
        Document doc;
        Runnable wrapRun = new Runnable(){

            @Override
            public void run() {
                View documentView = Utilities.getDocumentView(component);
                if (documentView != null) {
                    ((DocumentView)documentView).runTransaction(r);
                }
            }
        };
        if (readLockDocument && (doc = component.getDocument()) != null) {
            doc.render(wrapRun);
        } else {
            wrapRun.run();
        }
    }

    public static String keySequenceToString(KeyStroke[] seq) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < seq.length; ++i) {
            if (i > 0) {
                sb.append(' ');
            }
            sb.append(Utilities.keyStrokeToString(seq[i]));
        }
        return sb.toString();
    }

    public static String keyStrokeToString(KeyStroke stroke) {
        String keyText;
        String modifText = KeyEvent.getKeyModifiersText(stroke.getModifiers());
        String string = keyText = stroke.getKeyCode() == 0 ? String.valueOf(stroke.getKeyChar()) : Utilities.getKeyText(stroke.getKeyCode());
        if (modifText.length() > 0) {
            return modifText + '+' + keyText;
        }
        return keyText;
    }

    private static String getKeyText(int keyCode) {
        String ret = KeyEvent.getKeyText(keyCode);
        if (ret != null) {
            switch (keyCode) {
                case 225: {
                    ret = Utilities.prefixNumpad(ret, 40);
                    break;
                }
                case 226: {
                    ret = Utilities.prefixNumpad(ret, 37);
                    break;
                }
                case 227: {
                    ret = Utilities.prefixNumpad(ret, 39);
                    break;
                }
                case 224: {
                    ret = Utilities.prefixNumpad(ret, 38);
                }
            }
        }
        return ret;
    }

    private static String prefixNumpad(String key, int testKeyCode) {
        if (key.equals(KeyEvent.getKeyText(testKeyCode))) {
            key = NbBundle.getBundle(BaseKit.class).getString("key-prefix-numpad") + key;
        }
        return key;
    }

    private static void checkOffsetValid(Document doc, int offset) throws BadLocationException {
        Utilities.checkOffsetValid(offset, doc.getLength() + 1);
    }

    private static void checkOffsetValid(int offset, int limitOffset) throws BadLocationException {
        if (offset < 0 || offset > limitOffset) {
            throw new BadLocationException("Invalid offset=" + offset + " not within <0, " + limitOffset + ">", offset);
        }
    }

    public static void annotateLoggable(Throwable t) {
        Logger.getLogger("org.netbeans.editor").log(Level.INFO, null, t);
    }

    public static boolean isSelectionShowing(Caret caret) {
        return caret.isSelectionVisible() && caret.getDot() != caret.getMark();
    }

    public static boolean isSelectionShowing(JTextComponent component) {
        Caret caret = component.getCaret();
        return caret != null && Utilities.isSelectionShowing(caret);
    }

    static String getMimeType(Document doc) {
        return (String)doc.getProperty("mimeType");
    }

    static String getMimeType(JTextComponent component) {
        EditorKit kit;
        Document doc = component.getDocument();
        String mimeType = Utilities.getMimeType(doc);
        if (mimeType == null && (kit = component.getUI().getEditorKit(component)) != null) {
            mimeType = kit.getContentType();
        }
        return mimeType;
    }

    static Rectangle2D modelToView(JTextComponent tc, int pos) throws BadLocationException {
        return Utilities.modelToView(tc, pos, Position.Bias.Forward);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static Rectangle2D modelToView(JTextComponent tc, int pos, Position.Bias bias) throws BadLocationException {
        Document doc = tc.getDocument();
        if (doc instanceof AbstractDocument) {
            ((AbstractDocument)doc).readLock();
        }
        try {
            Rectangle alloc = Utilities.getVisibleEditorRect(tc);
            if (alloc != null) {
                View rootView = tc.getUI().getRootView(tc);
                rootView.setSize(alloc.width, alloc.height);
                Shape s = rootView.modelToView(pos, alloc, bias);
                if (s != null) {
                    Rectangle2D rectangle2D = s.getBounds2D();
                    return rectangle2D;
                }
            }
        }
        finally {
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument)doc).readUnlock();
            }
        }
        return null;
    }

    static int viewToModel(JTextComponent tc, double x, double y) {
        return Utilities.viewToModel(tc, x, y, discardBias);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static int viewToModel(JTextComponent tc, double x, double y, Position.Bias[] biasReturn) {
        int offs;
        block6 : {
            offs = -1;
            Document doc = tc.getDocument();
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument)doc).readLock();
            }
            try {
                Rectangle alloc = Utilities.getVisibleEditorRect(tc);
                if (alloc == null) break block6;
                View rootView = tc.getUI().getRootView(tc);
                View documentView = rootView.getView(0);
                if (documentView instanceof EditorView) {
                    documentView.setSize(alloc.width, alloc.height);
                    offs = ((EditorView)documentView).viewToModelChecked(x, y, (Shape)alloc, biasReturn);
                    break block6;
                }
                rootView.setSize(alloc.width, alloc.height);
                offs = rootView.viewToModel((float)x, (float)y, alloc, biasReturn);
            }
            finally {
                if (doc instanceof AbstractDocument) {
                    ((AbstractDocument)doc).readUnlock();
                }
            }
        }
        return offs;
    }

    private static Rectangle getVisibleEditorRect(JTextComponent tc) {
        Rectangle alloc = tc.getBounds();
        if (alloc.width > 0 && alloc.height > 0) {
            alloc.y = 0;
            alloc.x = 0;
            Insets insets = tc.getInsets();
            alloc.x += insets.left;
            alloc.y += insets.top;
            alloc.width -= insets.left + insets.right;
            alloc.height -= insets.top + insets.bottom;
            return alloc;
        }
        return null;
    }

    public static JComponent[] createSingleLineEditor(String mimeType) {
        assert (SwingUtilities.isEventDispatchThread());
        EditorKit kit = (EditorKit)MimeLookup.getLookup((String)mimeType).lookup(EditorKit.class);
        if (kit == null) {
            throw new IllegalArgumentException("No EditorKit for '" + mimeType + "' mimetype.");
        }
        final JEditorPane editorPane = new JEditorPane();
        editorPane.putClientProperty("HighlightsLayerExcludes", ".*(?<!TextSelectionHighlighting)$");
        editorPane.putClientProperty("AsTextField", Boolean.TRUE);
        editorPane.setEditorKit(kit);
        Utilities.getEditorUI((JTextComponent)editorPane).textLimitLineVisible = false;
        KeyStroke enterKs = KeyStroke.getKeyStroke(10, 0);
        KeyStroke escKs = KeyStroke.getKeyStroke(27, 0);
        KeyStroke tabKs = KeyStroke.getKeyStroke(9, 0);
        InputMap im = editorPane.getInputMap();
        im.put(enterKs, "no-action");
        im.put(escKs, "no-action");
        im.put(tabKs, "no-action");
        editorPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        JTextField referenceTextField = new JTextField("M");
        Set<AWTKeyStroke> tfkeys = referenceTextField.getFocusTraversalKeys(0);
        editorPane.setFocusTraversalKeys(0, tfkeys);
        tfkeys = referenceTextField.getFocusTraversalKeys(1);
        editorPane.setFocusTraversalKeys(1, tfkeys);
        final Insets margin = referenceTextField.getMargin();
        Insets borderInsets = referenceTextField.getBorder().getBorderInsets(referenceTextField);
        final JScrollPane sp = new JScrollPane(21, 31){

            @Override
            public void setViewportView(Component view) {
                if (view instanceof JComponent) {
                    ((JComponent)view).setBorder(new EmptyBorder(margin));
                }
                if (view instanceof JEditorPane) {
                    Utilities.adjustScrollPaneSize(this, (JEditorPane)view);
                }
                super.setViewportView(view);
            }
        };
        editorPane.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("editorKit".equals(evt.getPropertyName())) {
                    Utilities.adjustScrollPaneSize(sp, editorPane);
                }
            }
        });
        sp.setBorder(new DelegatingBorder(referenceTextField.getBorder(), borderInsets));
        sp.setBackground(referenceTextField.getBackground());
        int preferredHeight = referenceTextField.getPreferredSize().height;
        Dimension spDim = sp.getPreferredSize();
        spDim.height = preferredHeight;
        spDim.height += margin.bottom + margin.top;
        sp.setPreferredSize(spDim);
        sp.setMinimumSize(spDim);
        sp.setMaximumSize(spDim);
        sp.setViewportView(editorPane);
        final ManageViewPositionListener manageViewListener = new ManageViewPositionListener(editorPane, sp);
        DocumentUtilities.addDocumentListener((Document)editorPane.getDocument(), (DocumentListener)manageViewListener, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
        editorPane.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ("document".equals(evt.getPropertyName())) {
                    Document newDoc;
                    Document oldDoc = (Document)evt.getOldValue();
                    if (oldDoc != null) {
                        DocumentUtilities.removeDocumentListener((Document)oldDoc, (DocumentListener)manageViewListener, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
                    }
                    if ((newDoc = (Document)evt.getNewValue()) != null) {
                        DocumentUtilities.addDocumentListener((Document)newDoc, (DocumentListener)manageViewListener, (DocumentListenerPriority)DocumentListenerPriority.AFTER_CARET_UPDATE);
                    }
                }
            }
        });
        return new JComponent[]{sp, editorPane};
    }

    public static <T> T runWithOnSaveTasksDisabled(Mutex.Action<T> run) {
        return BeforeSaveTasks.runWithOnSaveTasksDisabled(run);
    }

    private static void adjustScrollPaneSize(JScrollPane sp, JEditorPane editorPane) {
        int height;
        Dimension prefSize = sp.getPreferredSize();
        Insets borderInsets = sp.getBorder().getBorderInsets(sp);
        int vBorder = borderInsets.bottom + borderInsets.top;
        EditorUI eui = Utilities.getEditorUI(editorPane);
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("createSingleLineEditor(): editor UI = " + eui);
            if (eui != null) {
                logger.fine("createSingleLineEditor(): editor UI's line height = " + eui.getLineHeight());
                logger.fine("createSingleLineEditor(): editor UI's line ascent = " + eui.getLineAscent());
            }
        }
        if (eui != null) {
            height = eui.getLineHeight();
            if (height < eui.getLineAscent()) {
                height = eui.getLineAscent() * 4 / 3;
            }
        } else {
            Font font = editorPane.getFont();
            FontMetrics fontMetrics = editorPane.getFontMetrics(font);
            height = fontMetrics.getHeight();
        }
        height += vBorder + Utilities.getLFHeightAdjustment();
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("createSingleLineEditor(): border vertical insets = " + borderInsets.bottom + " + " + borderInsets.top);
            logger.fine("createSingleLineEditor(): computed height = " + height + ", prefSize = " + prefSize);
        }
        if (prefSize.height < height) {
            prefSize.height = height;
            sp.setPreferredSize(prefSize);
            sp.setMinimumSize(prefSize);
            sp.setMaximumSize(prefSize);
            Container c = sp.getParent();
            logger.fine("createSingleLineEditor(): setting a new height of ScrollPane = " + height);
            if (c instanceof JComponent) {
                ((JComponent)c).revalidate();
            }
        }
    }

    private static int getLFHeightAdjustment() {
        LookAndFeel lf = UIManager.getLookAndFeel();
        String lfID = lf.getID();
        logger.fine("createSingleLineEditor(): current L&F = '" + lfID + "'");
        if ("Metal".equals(lfID)) {
            return 0;
        }
        if ("GTK".equals(lfID)) {
            return 2;
        }
        if ("Motif".equals(lfID)) {
            return 3;
        }
        if ("Nimbus".equals(lfID)) {
            return 0;
        }
        if ("Aqua".equals(lfID)) {
            return -2;
        }
        return 0;
    }

    static {
        logger = Logger.getLogger(Utilities.class.getName());
        discardBias = new Position.Bias[1];
    }

    private static final class DelegatingBorder
    implements Border {
        private Border delegate;
        private Insets insets;

        public DelegatingBorder(Border delegate, Insets insets) {
            this.delegate = delegate;
            this.insets = insets;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            this.delegate.paintBorder(c, g, x, y, width, height);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return this.insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return this.delegate.isBorderOpaque();
        }
    }

    private static final class ManageViewPositionListener
    implements DocumentListener {
        private JEditorPane editorPane;
        private JScrollPane sp;

        public ManageViewPositionListener(JEditorPane editorPane, JScrollPane sp) {
            this.editorPane = editorPane;
            this.sp = sp;
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.changed();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.changed();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.changed();
        }

        private void changed() {
            JViewport viewport = this.sp.getViewport();
            Point viewPosition = viewport.getViewPosition();
            if (viewPosition.x > 0) {
                try {
                    Rectangle textRect = this.editorPane.getUI().modelToView(this.editorPane, this.editorPane.getDocument().getLength());
                    int textLength = textRect.x + textRect.width;
                    int viewLength = viewport.getExtentSize().width;
                    if (textLength < viewPosition.x + viewLength) {
                        viewPosition.x = Math.max(textLength - viewLength, 0);
                        viewport.setViewPosition(viewPosition);
                    }
                }
                catch (BadLocationException blex) {
                    Exceptions.printStackTrace((Throwable)blex);
                }
            }
        }
    }

}

