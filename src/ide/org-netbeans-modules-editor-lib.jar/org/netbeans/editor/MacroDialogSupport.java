/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.editor;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.accessibility.AccessibleContext;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import org.netbeans.editor.DialogSupport;
import org.netbeans.editor.MacroSavePanel;
import org.openide.util.NbBundle;

public class MacroDialogSupport
implements ActionListener {
    JButton okButton;
    JButton cancelButton;
    MacroSavePanel panel;
    Dialog macroDialog;
    Class kitClass;

    public MacroDialogSupport(Class kitClass) {
        this.kitClass = kitClass;
        this.panel = new MacroSavePanel(kitClass);
        ResourceBundle bundle = NbBundle.getBundle(MacroDialogSupport.class);
        this.okButton = new JButton(bundle.getString("MDS_ok"));
        this.cancelButton = new JButton(bundle.getString("MDS_cancel"));
        this.okButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_MDS_ok"));
        this.cancelButton.getAccessibleContext().setAccessibleDescription(bundle.getString("ACSD_MDS_cancel"));
    }

    public void setBody(String body) {
        this.panel.setMacroBody(body);
    }

    public void showMacroDialog() {
        this.macroDialog = DialogSupport.createDialog(NbBundle.getBundle(MacroDialogSupport.class).getString("MDS_title"), this.panel, true, new JButton[]{this.okButton, this.cancelButton}, false, 0, 1, this);
        this.macroDialog.pack();
        this.panel.popupNotify();
        this.macroDialog.requestFocus();
        this.macroDialog.show();
    }

    protected int showConfirmDialog(String macroName) {
        return JOptionPane.showConfirmDialog(this.panel, MessageFormat.format(NbBundle.getBundle(MacroDialogSupport.class).getString("MDS_Overwrite"), this.panel.getMacroName()), NbBundle.getBundle(MacroDialogSupport.class).getString("MDS_Warning"), 1, 2);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        this.macroDialog.setVisible(false);
        this.macroDialog.dispose();
    }
}

