/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.ListenerList
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.editor.lib2.document.ContentEdit
 *  org.netbeans.modules.editor.lib2.document.EditorDocumentContent
 *  org.netbeans.modules.editor.lib2.document.EditorDocumentHandler
 *  org.netbeans.modules.editor.lib2.document.EditorDocumentServices
 *  org.netbeans.modules.editor.lib2.document.LineRootElement
 *  org.netbeans.modules.editor.lib2.document.ListUndoableEdit
 *  org.netbeans.modules.editor.lib2.document.ModRootElement
 *  org.netbeans.modules.editor.lib2.document.ReadWriteBuffer
 *  org.netbeans.modules.editor.lib2.document.ReadWriteUtils
 *  org.netbeans.modules.editor.lib2.document.StableCompoundEdit
 *  org.netbeans.spi.editor.document.UndoableEditWrapper
 *  org.netbeans.spi.lexer.MutableTextInput
 *  org.netbeans.spi.lexer.TokenHierarchyControl
 *  org.openide.util.WeakListeners
 */
package org.netbeans.editor;

import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JEditorPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.WrappedPlainView;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.Acceptor;
import org.netbeans.editor.AcceptorFactory;
import org.netbeans.editor.ActionFactory;
import org.netbeans.editor.AdjustFinder;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.AtomicLockDocument;
import org.netbeans.editor.AtomicLockEvent;
import org.netbeans.editor.AtomicLockListener;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.CharSeq;
import org.netbeans.editor.DocumentUtilities;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Finder;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.FixLineSyntaxState;
import org.netbeans.editor.GapStart;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.PrintContainer;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TextBatchProcessor;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.ListenerList;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.lib.BaseDocument_PropertyHandler;
import org.netbeans.modules.editor.lib.BeforeSaveTasks;
import org.netbeans.modules.editor.lib.EditorPackageAccessor;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.netbeans.modules.editor.lib.drawing.DrawEngine;
import org.netbeans.modules.editor.lib.drawing.DrawGraphics;
import org.netbeans.modules.editor.lib.impl.MarkVector;
import org.netbeans.modules.editor.lib.impl.MultiMark;
import org.netbeans.modules.editor.lib2.document.ContentEdit;
import org.netbeans.modules.editor.lib2.document.EditorDocumentContent;
import org.netbeans.modules.editor.lib2.document.EditorDocumentHandler;
import org.netbeans.modules.editor.lib2.document.EditorDocumentServices;
import org.netbeans.modules.editor.lib2.document.LineRootElement;
import org.netbeans.modules.editor.lib2.document.ListUndoableEdit;
import org.netbeans.modules.editor.lib2.document.ModRootElement;
import org.netbeans.modules.editor.lib2.document.ReadWriteBuffer;
import org.netbeans.modules.editor.lib2.document.ReadWriteUtils;
import org.netbeans.modules.editor.lib2.document.StableCompoundEdit;
import org.netbeans.spi.editor.document.UndoableEditWrapper;
import org.netbeans.spi.lexer.MutableTextInput;
import org.netbeans.spi.lexer.TokenHierarchyControl;
import org.openide.util.WeakListeners;

public class BaseDocument
extends AbstractDocument
implements AtomicLockDocument {
    private static final Logger LOG;
    private static final Logger LOG_LISTENER;
    public static final String MIME_TYPE_PROP = "mimeType";
    public static final String ID_PROP = "id";
    private static final String VERSION_PROP = "version";
    private static final String LAST_MODIFICATION_TIMESTAMP_PROP = "last-modification-timestamp";
    public static final String READ_LINE_SEPARATOR_PROP = "__EndOfLine__";
    public static final String WRITE_LINE_SEPARATOR_PROP = "write-line-separator";
    public static final String FILE_NAME_PROP = "file-name";
    public static final String WRAP_SEARCH_MARK_PROP = "wrap-search-mark";
    public static final String UNDO_MANAGER_PROP = "undo-manager";
    public static final String KIT_CLASS_PROP = "kit-class";
    public static final String STRING_FINDER_PROP = "string-finder";
    public static final String STRING_BWD_FINDER_PROP = "string-bwd-finder";
    public static final String BLOCKS_FINDER_PROP = "blocks-finder";
    public static final String LINE_LIMIT_PROP = "line-limit";
    static final String EDITABLE_PROP = "editable";
    public static final String LINE_BATCH_SIZE = "line-batch-size";
    public static final String LS_CR = "\r";
    public static final String LS_LF = "\n";
    public static final String LS_CRLF = "\r\n";
    public static final String FORMATTER = "formatter";
    private static final String SUPPORTS_MODIFICATION_LISTENER_PROP = "supportsModificationListener";
    private static final String MODIFICATION_LISTENER_PROP = "modificationListener";
    private static final int DEACTIVATE_LEXER_THRESHOLD = 30;
    private static final Object annotationsLock;
    private static final Object getVisColFromPosLock;
    private static final Object getOffsetFromVisColLock;
    private static final boolean debugStack;
    private static final boolean debugNoText;
    private static final boolean debugRead;
    private int atomicDepth;
    boolean modifiable = true;
    protected boolean inited;
    protected boolean modified;
    protected Element defaultRootElem;
    private SyntaxSupport syntaxSupport;
    boolean undoMergeReset;
    private final Class deprecatedKitClass;
    private String mimeType;
    private AtomicCompoundEdit atomicEdits;
    private Acceptor identifierAcceptor;
    private Acceptor whitespaceAcceptor;
    LineRootElement lineRootElement;
    UndoableEdit lastModifyUndoEdit;
    private Annotations annotations;
    private boolean composedText = false;
    private FinderFactory.VisColPosFwdFinder visColPosFwdFinder;
    private FinderFactory.PosVisColFwdFinder posVisColFwdFinder;
    private AtomicLockEvent atomicLockEventInstance;
    private FixLineSyntaxState fixLineSyntaxState;
    private Object[] atomicLockListenerList;
    private DocumentListener postModificationDocumentListener;
    private ListenerList<DocumentListener> postModificationDocumentListenerList;
    private ListenerList<DocumentListener> updateDocumentListenerList;
    private Position lastPositionEditedByTyping;
    private int shiftWidth;
    private int tabSize;
    private CharSequence text;
    private UndoableEdit removeUpdateLineUndo;
    private Collection<? extends UndoableEditWrapper> undoEditWrappers;
    private DocumentFilter.FilterBypass filterBypass;
    private int runExclusiveDepth;
    private Preferences prefs;
    private final PreferenceChangeListener prefsListener;
    private PreferenceChangeListener weakPrefsListener;

    public BaseDocument(Class kitClass, boolean addToRegistry) {
        super((AbstractDocument.Content)new EditorDocumentContent());
        this.atomicLockEventInstance = new AtomicLockEvent(this);
        this.postModificationDocumentListenerList = new ListenerList();
        this.updateDocumentListenerList = new ListenerList();
        this.lastPositionEditedByTyping = null;
        this.shiftWidth = -1;
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                Finder finder;
                String key;
                String string = key = evt == null ? null : evt.getKey();
                if (key == null || "tab-size".equals(key)) {
                    BaseDocument.this.tabSize = BaseDocument.this.prefs.getInt("tab-size", 8);
                }
                if (key == null || "indent-shift-width".equals(key)) {
                    BaseDocument.this.shiftWidth = BaseDocument.this.prefs.getInt("indent-shift-width", -1);
                }
                if ((key == null || "spaces-per-tab".equals(key)) && BaseDocument.this.shiftWidth == -1) {
                    BaseDocument.this.shiftWidth = BaseDocument.this.prefs.getInt("spaces-per-tab", 4);
                }
                if (key == null || "read-buffer-size".equals(key)) {
                    int readBufferSize = BaseDocument.this.prefs.getInt("read-buffer-size", -1);
                    if (readBufferSize <= 0) {
                        readBufferSize = 16384;
                    }
                    BaseDocument.this.putProperty("read-buffer-size", new Integer(readBufferSize));
                }
                if (key == null || "write-buffer-size".equals(key)) {
                    int writeBufferSize = BaseDocument.this.prefs.getInt("write-buffer-size", -1);
                    if (writeBufferSize <= 0) {
                        writeBufferSize = 16384;
                    }
                    BaseDocument.this.putProperty("write-buffer-size", new Integer(writeBufferSize));
                }
                if (key == null || "mark-distance".equals(key)) {
                    int markDistance = BaseDocument.this.prefs.getInt("mark-distance", -1);
                    if (markDistance <= 0) {
                        markDistance = 100;
                    }
                    BaseDocument.this.putProperty("mark-distance", new Integer(markDistance));
                }
                if (key == null || "max-mark-distance".equals(key)) {
                    int maxMarkDistance = BaseDocument.this.prefs.getInt("max-mark-distance", -1);
                    if (maxMarkDistance <= 0) {
                        maxMarkDistance = 150;
                    }
                    BaseDocument.this.putProperty("max-mark-distance", new Integer(maxMarkDistance));
                }
                if (key == null || "min-mark-distance".equals(key)) {
                    int minMarkDistance = BaseDocument.this.prefs.getInt("min-mark-distance", -1);
                    if (minMarkDistance <= 0) {
                        minMarkDistance = 50;
                    }
                    BaseDocument.this.putProperty("min-mark-distance", new Integer(minMarkDistance));
                }
                if (key == null || "read-mark-distance".equals(key)) {
                    int readMarkDistance = BaseDocument.this.prefs.getInt("read-mark-distance", -1);
                    if (readMarkDistance <= 0) {
                        readMarkDistance = 180;
                    }
                    BaseDocument.this.putProperty("read-mark-distance", new Integer(readMarkDistance));
                }
                if (key == null || "syntax-update-batch-size".equals(key)) {
                    int syntaxUpdateBatchSize = BaseDocument.this.prefs.getInt("syntax-update-batch-size", -1);
                    if (syntaxUpdateBatchSize <= 0) {
                        syntaxUpdateBatchSize = 7 * (Integer)BaseDocument.this.getProperty("mark-distance");
                    }
                    BaseDocument.this.putProperty("syntax-update-batch-size", new Integer(syntaxUpdateBatchSize));
                }
                if (key == null || "line-batch-size".equals(key)) {
                    int lineBatchSize = BaseDocument.this.prefs.getInt("line-batch-size", -1);
                    if (lineBatchSize <= 0) {
                        lineBatchSize = 2;
                    }
                    BaseDocument.this.putProperty("line-batch-size", new Integer(lineBatchSize));
                }
                if (key == null || "identifier-acceptor".equals(key)) {
                    BaseDocument.this.identifierAcceptor = (Acceptor)SettingsConversions.callFactory(BaseDocument.this.prefs, MimePath.parse((String)BaseDocument.this.mimeType), "identifier-acceptor", AcceptorFactory.LETTER_DIGIT);
                }
                if (key == null || "whitespace-acceptor".equals(key)) {
                    BaseDocument.this.whitespaceAcceptor = (Acceptor)SettingsConversions.callFactory(BaseDocument.this.prefs, MimePath.parse((String)BaseDocument.this.mimeType), "whitespace-acceptor", AcceptorFactory.WHITESPACE);
                }
                boolean stopOnEOL = BaseDocument.this.prefs.getBoolean("word-move-newline-stop", true);
                if (key == null || "next-word-finder".equals(key)) {
                    finder = (Finder)SettingsConversions.callFactory(BaseDocument.this.prefs, MimePath.parse((String)BaseDocument.this.mimeType), "next-word-finder", null);
                    BaseDocument.this.putProperty("next-word-finder", finder != null ? finder : new FinderFactory.NextWordFwdFinder(BaseDocument.this, stopOnEOL, false));
                }
                if (key == null || "previous-word-finder".equals(key)) {
                    finder = (Finder)SettingsConversions.callFactory(BaseDocument.this.prefs, MimePath.parse((String)BaseDocument.this.mimeType), "previous-word-finder", null);
                    BaseDocument.this.putProperty("previous-word-finder", finder != null ? finder : new FinderFactory.PreviousWordBwdFinder(BaseDocument.this, stopOnEOL, false));
                }
                SettingsConversions.callSettingsChange(BaseDocument.this);
            }
        };
        if (LOG.isLoggable(Level.FINE) || LOG.isLoggable(Level.WARNING)) {
            String msg = "Using deprecated document construction for " + this.getClass().getName() + ", " + "see http://www.netbeans.org/nonav/issues/show_bug.cgi?id=114747. " + "Use -J-Dorg.netbeans.editor.BaseDocument.level=500 to see the stacktrace.";
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, null, new Throwable(msg));
            } else {
                LOG.warning(msg);
            }
        }
        this.deprecatedKitClass = kitClass;
        this.mimeType = BaseKit.getKit(kitClass).getContentType();
        this.init(addToRegistry);
    }

    public BaseDocument(boolean addToRegistry, String mimeType) {
        super((AbstractDocument.Content)new EditorDocumentContent());
        this.atomicLockEventInstance = new AtomicLockEvent(this);
        this.postModificationDocumentListenerList = new ListenerList();
        this.updateDocumentListenerList = new ListenerList();
        this.lastPositionEditedByTyping = null;
        this.shiftWidth = -1;
        this.prefsListener = new ;
        this.deprecatedKitClass = null;
        this.mimeType = mimeType;
        this.init(addToRegistry);
    }

    private void init(boolean addToRegistry) {
        this.setDocumentProperties(this.createDocumentProperties(this.getDocumentProperties()));
        super.addDocumentListener(org.netbeans.lib.editor.util.swing.DocumentUtilities.initPriorityListening((Document)this));
        this.text = ((EditorDocumentContent)this.getContent()).getText();
        this.putProperty(CharSequence.class, this.text);
        this.putProperty(GapStart.class, new GapStart(){

            @Override
            public int getGapStart() {
                return ((EditorDocumentContent)BaseDocument.this.getContent()).getCharContentGapStart();
            }
        });
        this.putProperty("supportsModificationListener", Boolean.TRUE);
        this.putProperty("mimeType", new MimeTypePropertyEvaluator(this));
        this.putProperty("version", new AtomicLong());
        this.putProperty("last-modification-timestamp", new AtomicLong());
        this.putProperty("tab-size", new BaseDocument_PropertyHandler(){

            @Override
            public Object setValue(Object value) {
                return null;
            }

            @Override
            public Object getValue() {
                return BaseDocument.this.getTabSize();
            }
        });
        this.putProperty(PropertyChangeSupport.class, new PropertyChangeSupport(this));
        this.lineRootElement = new LineRootElement((Document)this);
        this.putProperty("__EndOfLine__", ReadWriteUtils.getSystemLineSeparator());
        this.prefs = (Preferences)MimeLookup.getLookup((String)this.mimeType).lookup(Preferences.class);
        this.prefsListener.preferenceChange(null);
        EditorKit kit = this.getEditorKit();
        if (kit instanceof BaseKit) {
            ((BaseKit)kit).initDocument(this);
        }
        ModRootElement modElementRoot = new ModRootElement((Document)this);
        this.addUpdateDocumentListener((DocumentListener)modElementRoot);
        modElementRoot.setEnabled(true);
        BeforeSaveTasks.get(this);
        this.undoEditWrappers = MimeLookup.getLookup((String)this.mimeType).lookupAll(UndoableEditWrapper.class);
        if (this.undoEditWrappers != null && this.undoEditWrappers.isEmpty()) {
            this.undoEditWrappers = null;
        }
        if (this.weakPrefsListener == null) {
            this.weakPrefsListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs);
            this.prefs.addPreferenceChangeListener(this.weakPrefsListener);
        }
    }

    public CharSeq getText() {
        return new CharSeq(){

            @Override
            public int length() {
                return BaseDocument.this.text.length();
            }

            @Override
            public char charAt(int index) {
                return BaseDocument.this.text.charAt(index);
            }
        };
    }

    Syntax getFreeSyntax() {
        EditorKit kit = this.getEditorKit();
        if (kit instanceof BaseKit) {
            return ((BaseKit)kit).createSyntax(this);
        }
        return new BaseKit.DefaultSyntax();
    }

    void releaseSyntax(Syntax syntax) {
    }

    public SyntaxSupport getSyntaxSupport() {
        if (this.syntaxSupport == null) {
            EditorKit kit = this.getEditorKit();
            this.syntaxSupport = kit instanceof BaseKit ? ((BaseKit)kit).createSyntaxSupport(this) : new SyntaxSupport(this);
        }
        return this.syntaxSupport;
    }

    public int processText(TextBatchProcessor tbp, int startPos, int endPos) throws BadLocationException {
        if (endPos == -1) {
            endPos = this.getLength();
        }
        int batchLineCnt = (Integer)this.getProperty("line-batch-size");
        int batchStart = startPos;
        int ret = -1;
        if (startPos < endPos) {
            while (ret < 0 && batchStart < endPos) {
                int batchEnd = Math.min(Utilities.getRowStart(this, batchStart, batchLineCnt), endPos);
                if (batchEnd == -1) {
                    batchEnd = endPos;
                }
                ret = tbp.processTextBatch(this, batchStart, batchEnd, batchEnd == endPos);
                batchLineCnt *= 2;
                batchStart = batchEnd;
            }
        } else {
            while (ret < 0 && batchStart > endPos) {
                int batchEnd;
                ret = tbp.processTextBatch(this, batchStart, batchEnd, (batchEnd = Math.max(Utilities.getRowStart(this, batchStart, - batchLineCnt), endPos)) == endPos);
                batchLineCnt *= 2;
                batchStart = batchEnd;
            }
        }
        return ret;
    }

    public boolean isIdentifierPart(char ch) {
        return this.identifierAcceptor.accept(ch);
    }

    public boolean isWhitespace(char ch) {
        return this.whitespaceAcceptor.accept(ch);
    }

    public boolean isModifiable() {
        return this.modifiable;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertString(int offset, String text, AttributeSet attrs) throws BadLocationException {
        block4 : {
            this.atomicLockImpl();
            try {
                this.checkModifiable(offset);
                DocumentFilter filter = this.getDocumentFilter();
                if (filter != null) {
                    filter.insertString(this.getFilterBypass(), offset, text, attrs);
                    break block4;
                }
                this.handleInsertString(offset, text, attrs);
            }
            finally {
                this.atomicUnlockImpl(true);
            }
        }
    }

    void handleInsertString(int offset, String text, AttributeSet attrs) throws BadLocationException {
        boolean isComposedText;
        if (text == null || text.length() == 0) {
            return;
        }
        if (offset < 0 || offset > this.getLength()) {
            throw new BadLocationException("Wrong insert position " + offset, offset);
        }
        text = ReadWriteUtils.convertToNewlines((CharSequence)text);
        this.incrementDocVersion();
        this.preInsertCheck(offset, text, attrs);
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("insertString(): doc=");
            this.appendInfoTerse(sb);
            sb.append(this.modified ? "" : " - first modification").append(", offset=").append(Utilities.offsetToLineColumnString(this, offset));
            if (!debugNoText) {
                sb.append(" \"");
                this.appendContext(sb, offset);
                sb.append("\" + \"");
                if (this.modified) {
                    CharSequenceUtilities.debugText((StringBuilder)sb, (CharSequence)text);
                } else {
                    sb.append(text);
                }
                sb.append("\"");
            }
            if (debugStack) {
                LOG.log(Level.FINE, sb.toString(), new Throwable("Insert stack"));
            } else {
                LOG.log(Level.FINE, sb.toString());
            }
        }
        UndoableEdit edit = this.getContent().insertString(offset, text);
        BaseDocumentEvent evt = this.getDocumentEvent(offset, text.length(), DocumentEvent.EventType.INSERT, attrs);
        this.preInsertUpdate(evt, attrs);
        if (edit != null) {
            evt.addEdit(edit);
            this.lastModifyUndoEdit = edit;
        }
        this.modified = true;
        if (this.atomicDepth > 0) {
            this.ensureAtomicEditsInited();
            this.atomicEdits.addEdit((UndoableEdit)evt);
        }
        this.insertUpdate(evt, attrs);
        evt.end();
        this.fireInsertUpdate(evt);
        boolean bl = isComposedText = attrs != null && attrs.isDefined(StyleConstants.ComposedTextAttribute);
        if (this.composedText && !isComposedText) {
            this.composedText = false;
        }
        if (!this.composedText && isComposedText) {
            this.composedText = true;
        }
        if (this.atomicDepth == 0 && !isComposedText) {
            this.fireUndoableEditUpdate(new UndoableEditEvent(this, evt));
        }
        if (this.postModificationDocumentListener != null) {
            this.postModificationDocumentListener.insertUpdate(evt);
        }
        if (this.postModificationDocumentListenerList.getListenerCount() > 0) {
            for (DocumentListener listener : this.postModificationDocumentListenerList.getListeners()) {
                listener.insertUpdate(evt);
            }
        }
    }

    private void appendContext(StringBuilder sb, int offset) {
        int startOffset;
        int contextLen;
        CharSequence docText = org.netbeans.lib.editor.util.swing.DocumentUtilities.getText((Document)this);
        int back = contextLen = 20;
        int endOffset = offset;
        for (startOffset = offset; back > 0 && startOffset > 0 && docText.charAt(startOffset) != '\n'; --startOffset, --back) {
        }
        int docTextLen = docText.length();
        for (int forward = contextLen; forward > 0 && endOffset < docTextLen && docText.charAt(endOffset++) != '\n'; --forward) {
        }
        if (startOffset > 0) {
            sb.append("...");
        }
        CharSequenceUtilities.debugText((StringBuilder)sb, (CharSequence)docText.subSequence(startOffset, offset));
        sb.append("|");
        CharSequenceUtilities.debugText((StringBuilder)sb, (CharSequence)docText.subSequence(offset, endOffset));
        if (endOffset < docTextLen) {
            sb.append("...");
        }
    }

    public void checkTrailingSpaces(int offset) {
        try {
            int lastEditedLine;
            int lineNum = Utilities.getLineOffset(this, offset);
            int n = lastEditedLine = this.lastPositionEditedByTyping != null ? Utilities.getLineOffset(this, this.lastPositionEditedByTyping.getOffset()) : -1;
            if (lastEditedLine != -1 && lastEditedLine != lineNum) {
                int end;
                int startIndex;
                Element root = this.getDefaultRootElement();
                Element elem = root.getElement(lastEditedLine);
                int start = elem.getStartOffset();
                String line = this.getText(start, (end = elem.getEndOffset()) - start);
                int endIndex = line.length() - 1;
                if (endIndex >= 0 && line.charAt(endIndex) == '\n' && --endIndex >= 0 && line.charAt(endIndex) == '\r') {
                    --endIndex;
                }
                for (startIndex = endIndex; startIndex >= 0 && Character.isWhitespace(line.charAt(startIndex)) && line.charAt(startIndex) != '\n' && line.charAt(startIndex) != '\r'; --startIndex) {
                }
                if (++startIndex >= 0 && startIndex <= endIndex) {
                    this.remove(start + startIndex, endIndex - startIndex + 1);
                }
            }
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, null, e);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void remove(int offset, int length) throws BadLocationException {
        block4 : {
            this.atomicLockImpl();
            try {
                this.checkModifiable(offset);
                DocumentFilter filter = this.getDocumentFilter();
                if (filter != null) {
                    filter.remove(this.getFilterBypass(), offset, length);
                    break block4;
                }
                this.handleRemove(offset, length);
            }
            finally {
                this.atomicUnlockImpl(true);
            }
        }
    }

    void handleRemove(int offset, int length) throws BadLocationException {
        if (length == 0) {
            return;
        }
        if (length < 0) {
            throw new IllegalArgumentException("len=" + length + " < 0");
        }
        if (offset < 0) {
            throw new BadLocationException("Wrong remove position " + offset + " < 0", offset);
        }
        if (offset + length > this.getLength()) {
            throw new BadLocationException("Wrong (offset+length)=" + (offset + length) + " > getLength()=" + this.getLength(), offset + length);
        }
        this.incrementDocVersion();
        int docLen = this.getLength();
        if (offset < 0 || offset > docLen) {
            throw new BadLocationException("Wrong remove position " + offset, offset);
        }
        if (offset + length > docLen) {
            throw new BadLocationException("End offset of removed text " + (offset + length) + " > getLength()=" + docLen, offset + length);
        }
        this.preRemoveCheck(offset, length);
        BaseDocumentEvent evt = this.getDocumentEvent(offset, length, DocumentEvent.EventType.REMOVE, null);
        org.netbeans.lib.editor.util.swing.DocumentUtilities.addEventPropertyStorage((DocumentEvent)evt);
        String removedText = this.getText(offset, length);
        org.netbeans.lib.editor.util.swing.DocumentUtilities.putEventProperty((DocumentEvent)evt, String.class, (Object)removedText);
        this.removeUpdate(evt);
        UndoableEdit edit = ((EditorDocumentContent)this.getContent()).remove(offset, removedText);
        if (edit != null) {
            evt.addEdit(edit);
            this.lastModifyUndoEdit = edit;
        }
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("remove(): doc=");
            this.appendInfoTerse(sb);
            sb.append(",origDocLen=").append(docLen);
            sb.append(", offset=").append(Utilities.offsetToLineColumnString(this, offset));
            sb.append(",len=").append(length);
            if (!debugNoText) {
                sb.append(" \"");
                this.appendContext(sb, offset);
                sb.append("\" - \"");
                CharSequenceUtilities.debugText((StringBuilder)sb, (CharSequence)((ContentEdit)edit).getText());
                sb.append("\"");
            }
            if (debugStack) {
                LOG.log(Level.FINE, sb.toString(), new Throwable("Remove text"));
            } else {
                LOG.log(Level.FINE, sb.toString());
            }
        }
        if (this.atomicDepth > 0) {
            this.ensureAtomicEditsInited();
            this.atomicEdits.addEdit((UndoableEdit)evt);
        }
        this.postRemoveUpdate(evt);
        evt.end();
        this.fireRemoveUpdate(evt);
        if (this.atomicDepth == 0 && !this.composedText) {
            this.fireUndoableEditUpdate(new UndoableEditEvent(this, evt));
        }
        if (this.postModificationDocumentListener != null) {
            this.postModificationDocumentListener.removeUpdate(evt);
        }
        if (this.postModificationDocumentListenerList.getListenerCount() > 0) {
            for (DocumentListener listener : this.postModificationDocumentListenerList.getListeners()) {
                listener.removeUpdate(evt);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
        block4 : {
            this.atomicLockImpl();
            try {
                this.checkModifiable(offset);
                DocumentFilter filter = this.getDocumentFilter();
                if (filter != null) {
                    filter.replace(this.getFilterBypass(), offset, length, text, attrs);
                    break block4;
                }
                this.handleRemove(offset, length);
                this.handleInsertString(offset, text, attrs);
            }
            finally {
                this.atomicUnlockImpl(true);
            }
        }
    }

    private void checkModifiable(int offset) throws BadLocationException {
        if (!this.modifiable) {
            throw new GuardedException("Modification prohibited", offset);
        }
    }

    private DocumentFilter.FilterBypass getFilterBypass() {
        if (this.filterBypass == null) {
            this.filterBypass = new FilterBypassImpl();
        }
        return this.filterBypass;
    }

    protected void preInsertCheck(int offset, String text, AttributeSet a) throws BadLocationException {
    }

    protected void preRemoveCheck(int offset, int len) throws BadLocationException {
    }

    @Override
    protected void insertUpdate(AbstractDocument.DefaultDocumentEvent chng, AttributeSet attr) {
        super.insertUpdate(chng, attr);
        org.netbeans.lib.editor.util.swing.DocumentUtilities.addEventPropertyStorage((DocumentEvent)chng);
        org.netbeans.lib.editor.util.swing.DocumentUtilities.putEventProperty((DocumentEvent)chng, String.class, (Object)((BaseDocumentEvent)chng).getText());
        this.lineRootElement.insertUpdate(chng, attr);
        this.fixLineSyntaxState.update(false);
        chng.addEdit(this.fixLineSyntaxState.createAfterLineUndo());
        this.fixLineSyntaxState = null;
        for (DocumentListener listener : this.updateDocumentListenerList.getListeners()) {
            listener.insertUpdate(chng);
        }
    }

    protected void preInsertUpdate(AbstractDocument.DefaultDocumentEvent chng, AttributeSet attr) {
        this.fixLineSyntaxState = new FixLineSyntaxState(chng);
        chng.addEdit(this.fixLineSyntaxState.createBeforeLineUndo());
    }

    @Override
    protected void removeUpdate(AbstractDocument.DefaultDocumentEvent chng) {
        super.removeUpdate(chng);
        for (DocumentListener listener : this.updateDocumentListenerList.getListeners()) {
            listener.removeUpdate(chng);
        }
        this.removeUpdateLineUndo = this.lineRootElement.legacyRemoveUpdate(chng);
        this.fixLineSyntaxState = new FixLineSyntaxState(chng);
        chng.addEdit(this.fixLineSyntaxState.createBeforeLineUndo());
    }

    @Override
    protected void postRemoveUpdate(AbstractDocument.DefaultDocumentEvent chng) {
        super.postRemoveUpdate(chng);
        if (this.removeUpdateLineUndo != null) {
            chng.addEdit(this.removeUpdateLineUndo);
            this.removeUpdateLineUndo = null;
        }
        this.fixLineSyntaxState.update(false);
        chng.addEdit(this.fixLineSyntaxState.createAfterLineUndo());
        this.fixLineSyntaxState = null;
    }

    public String getText(int[] block) throws BadLocationException {
        return this.getText(block[0], block[1] - block[0]);
    }

    public char[] getChars(int pos, int len) throws BadLocationException {
        char[] chars = new char[len];
        this.getChars(pos, chars, 0, len);
        return chars;
    }

    public char[] getChars(int[] block) throws BadLocationException {
        return this.getChars(block[0], block[1] - block[0]);
    }

    public void getChars(int pos, char[] ret, int offset, int len) throws BadLocationException {
        DocumentUtilities.copyText(this, pos, pos + len, ret, offset);
    }

    public int find(Finder finder, int startPos, int limitPos) throws BadLocationException {
        boolean fwdSearch;
        int docLen = this.getLength();
        if (limitPos == -1) {
            limitPos = docLen;
        }
        if (startPos == -1) {
            startPos = docLen;
        }
        if (finder instanceof AdjustFinder) {
            boolean voidSearch;
            if (startPos == limitPos) {
                finder.reset();
                return -1;
            }
            boolean forwardAdjustedSearch = startPos < limitPos;
            startPos = ((AdjustFinder)finder).adjustStartPos(this, startPos);
            limitPos = ((AdjustFinder)finder).adjustLimitPos(this, limitPos);
            boolean bl = forwardAdjustedSearch ? startPos >= limitPos : (voidSearch = startPos <= limitPos);
            if (voidSearch) {
                finder.reset();
                return -1;
            }
        }
        finder.reset();
        if (startPos == limitPos) {
            return -1;
        }
        Segment text = new Segment();
        int gapStart = ((EditorDocumentContent)this.getContent()).getCharContentGapStart();
        if (gapStart == -1) {
            throw new IllegalStateException("Cannot get gapStart");
        }
        int pos = startPos;
        boolean bl = fwdSearch = startPos <= limitPos;
        if (fwdSearch) {
            while (pos >= startPos && pos < limitPos) {
                int p0;
                int p1;
                if (pos < gapStart) {
                    p0 = startPos;
                    p1 = Math.min(gapStart, limitPos);
                } else {
                    p0 = Math.max(gapStart, startPos);
                    p1 = limitPos;
                }
                this.getText(p0, p1 - p0, text);
                pos = finder.find(p0 - text.offset, text.array, text.offset, text.offset + text.count, pos, limitPos);
                if (!finder.isFound()) continue;
                return pos;
            }
        } else {
            --pos;
            while (limitPos <= pos && pos <= startPos) {
                int p0;
                int p1;
                if (pos < gapStart) {
                    p0 = limitPos;
                    p1 = Math.min(gapStart, startPos);
                } else {
                    p0 = Math.max(gapStart, limitPos);
                    p1 = startPos;
                }
                this.getText(p0, p1 - p0, text);
                pos = finder.find(p0 - text.offset, text.array, text.offset, text.offset + text.count, pos, limitPos);
                if (!finder.isFound()) continue;
                return pos;
            }
        }
        return -1;
    }

    public void repaintBlock(int startOffset, int endOffset) {
        BaseDocumentEvent evt = this.getDocumentEvent(startOffset, endOffset - startOffset, DocumentEvent.EventType.CHANGE, null);
        this.fireChangedUpdate(evt);
    }

    public void print(PrintContainer container) {
        this.print(container, true, true, 0, this.getLength());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void print(PrintContainer container, boolean usePrintColoringMap, boolean lineNumberEnabled, int startOffset, int endOffset) {
        this.readLock();
        try {
            EditorKit kit = this.getEditorKit();
            EditorUI editorUI = kit instanceof BaseKit ? ((BaseKit)kit).createPrintEditorUI(this, usePrintColoringMap, lineNumberEnabled) : new EditorUI(this, usePrintColoringMap, lineNumberEnabled);
            DrawGraphics.PrintDG printDG = new DrawGraphics.PrintDG(container);
            DrawEngine.getDrawEngine().draw(printDG, editorUI, startOffset, endOffset, 0, 0, Integer.MAX_VALUE);
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, null, e);
        }
        finally {
            this.readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void print(PrintContainer container, boolean usePrintColoringMap, Boolean lineNumberEnabled, int startOffset, int endOffset) {
        this.readLock();
        try {
            EditorKit kit;
            boolean lineNumberEnabledPar = true;
            boolean forceLineNumbers = false;
            if (lineNumberEnabled != null) {
                lineNumberEnabledPar = lineNumberEnabled;
                forceLineNumbers = lineNumberEnabled;
            }
            EditorUI editorUI = (kit = this.getEditorKit()) instanceof BaseKit ? ((BaseKit)kit).createPrintEditorUI(this, usePrintColoringMap, lineNumberEnabledPar) : new EditorUI(this, usePrintColoringMap, lineNumberEnabledPar);
            if (forceLineNumbers) {
                editorUI.setLineNumberVisibleSetting(true);
                editorUI.setLineNumberEnabled(true);
                editorUI.updateLineNumberWidth(0);
            }
            DrawGraphics.PrintDG printDG = new DrawGraphics.PrintDG(container);
            DrawEngine.getDrawEngine().draw(printDG, editorUI, startOffset, endOffset, 0, 0, Integer.MAX_VALUE);
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, null, e);
        }
        finally {
            this.readUnlock();
        }
    }

    public Position createPosition(int offset, Position.Bias bias) throws BadLocationException {
        EditorDocumentContent content = (EditorDocumentContent)this.getContent();
        Position pos = bias == Position.Bias.Forward ? content.createPosition(offset) : content.createBackwardBiasPosition(offset);
        return pos;
    }

    @Override
    public Element[] getRootElements() {
        Element[] elems = new Element[]{this.getDefaultRootElement()};
        return elems;
    }

    @Override
    public Element getDefaultRootElement() {
        if (this.defaultRootElem == null) {
            this.defaultRootElem = this.lineRootElement;
        }
        return this.defaultRootElem;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void render(Runnable r) {
        this.readLock();
        try {
            r.run();
        }
        finally {
            this.readUnlock();
        }
    }

    public void runAtomic(Runnable r) {
        this.runAtomicAsUser(r);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void runAtomicAsUser(Runnable r) {
        boolean completed = false;
        this.atomicLockImpl();
        try {
            r.run();
            completed = true;
        }
        finally {
            try {
                if (!completed) {
                    this.breakAtomicLock();
                }
            }
            finally {
                this.atomicUnlockImpl();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void read(Reader reader, int pos) throws IOException, BadLocationException {
        this.extWriteLock();
        try {
            ModRootElement modElementRoot;
            Boolean inPaste;
            if (pos < 0 || pos > this.getLength()) {
                throw new BadLocationException("BaseDocument.read()", pos);
            }
            ReadWriteBuffer buffer = ReadWriteUtils.read((Reader)reader);
            if (!this.inited) {
                String lineSeparator = ReadWriteUtils.findFirstLineSeparator((ReadWriteBuffer)buffer);
                if (lineSeparator == null && (lineSeparator = (String)this.getProperty("default-line-separator")) == null) {
                    lineSeparator = ReadWriteUtils.getSystemLineSeparator();
                }
                this.putProperty("__EndOfLine__", lineSeparator);
            }
            this.insertString(pos, buffer.toString(), null);
            this.inited = true;
            if (debugRead) {
                LOG.log(Level.FINE, "BaseDocument.read(): StreamDescriptionProperty: {0}", this.getProperty("stream"));
            }
            if (!((inPaste = BaseKit.IN_PASTE.get()) != null && inPaste.booleanValue() || (modElementRoot = ModRootElement.get((Document)this)) == null)) {
                modElementRoot.resetMods(null);
            }
            this.lastModifyUndoEdit = null;
        }
        finally {
            this.extWriteUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void write(Writer writer, int pos, int len) throws IOException, BadLocationException {
        this.readLock();
        try {
            if (pos < 0 || pos + len > this.getLength()) {
                throw new BadLocationException("BaseDocument.write()", pos);
            }
            String lineSeparator = (String)this.getProperty("write-line-separator");
            if (lineSeparator == null && (lineSeparator = (String)this.getProperty("__EndOfLine__")) == null && (lineSeparator = (String)this.getProperty("default-line-separator")) == null) {
                lineSeparator = ReadWriteUtils.getSystemLineSeparator();
            }
            CharSequence docText = (CharSequence)this.getProperty(CharSequence.class);
            ReadWriteBuffer buffer = ReadWriteUtils.convertFromNewlines((CharSequence)docText, (int)pos, (int)(pos + len), (String)lineSeparator);
            ReadWriteUtils.write((Writer)writer, (ReadWriteBuffer)buffer);
            writer.flush();
        }
        finally {
            this.readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void invalidateSyntaxMarks() {
        this.extWriteLock();
        try {
            FixLineSyntaxState.invalidateAllSyntaxStateInfos(this);
            BaseDocumentEvent evt = this.getDocumentEvent(0, this.getLength(), DocumentEvent.EventType.CHANGE, null);
            this.fireChangedUpdate(evt);
        }
        finally {
            this.extWriteUnlock();
        }
    }

    public int getTabSize() {
        return this.tabSize;
    }

    public int getShiftWidth() {
        return this.shiftWidth;
    }

    public final Class getKitClass() {
        return this.getEditorKit().getClass();
    }

    private EditorKit getEditorKit() {
        EditorKit editorKit = (EditorKit)MimeLookup.getLookup((String)this.mimeType).lookup(EditorKit.class);
        if (editorKit == null) {
            LOG.log(Level.CONFIG, "No registered editor kit for ''{0}'', trying ''text/plain''.", this.mimeType);
            editorKit = (EditorKit)MimeLookup.getLookup((String)"text/plain").lookup(EditorKit.class);
            if (editorKit == null) {
                LOG.config("No registered editor kit for 'text/plain', using default.");
                editorKit = new PlainEditorKit();
            }
        }
        return editorKit;
    }

    private void setMimeType(String mimeType) {
        if (!this.mimeType.equals(mimeType)) {
            this.mimeType = mimeType;
            if (this.prefs != null && this.weakPrefsListener != null) {
                try {
                    this.prefs.removePreferenceChangeListener(this.weakPrefsListener);
                }
                catch (IllegalArgumentException e) {
                    // empty catch block
                }
                this.weakPrefsListener = null;
            }
            this.prefs = (Preferences)MimeLookup.getLookup((String)this.mimeType).lookup(Preferences.class);
            this.prefsListener.preferenceChange(null);
            EditorKit kit = this.getEditorKit();
            if (kit instanceof BaseKit) {
                ((BaseKit)kit).initDocument(this);
            }
            if (this.weakPrefsListener == null) {
                this.weakPrefsListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs);
                this.prefs.addPreferenceChangeListener(this.weakPrefsListener);
            }
        }
    }

    public void resetUndoMerge() {
        this.undoMergeReset = true;
    }

    @Override
    protected void fireChangedUpdate(DocumentEvent e) {
        super.fireChangedUpdate(e);
    }

    @Override
    protected void fireInsertUpdate(DocumentEvent e) {
        super.fireInsertUpdate(e);
    }

    @Override
    protected void fireRemoveUpdate(DocumentEvent e) {
        super.fireRemoveUpdate(e);
    }

    @Override
    protected void fireUndoableEditUpdate(UndoableEditEvent e) {
        if (this.undoEditWrappers != null) {
            UndoableEdit edit = e.getEdit();
            ListUndoableEdit listEdit = null;
            for (UndoableEditWrapper wrapper : this.undoEditWrappers) {
                UndoableEdit wrapEdit = wrapper.wrap(edit, (Document)this);
                if (wrapEdit == edit) continue;
                if (listEdit == null) {
                    listEdit = new ListUndoableEdit(edit, wrapEdit);
                } else {
                    listEdit.setDelegate(wrapEdit);
                }
                edit = wrapEdit;
            }
            if (listEdit != null) {
                e = new UndoableEditEvent(this, (UndoableEdit)listEdit);
            }
        }
        Object[] listeners = this.atomicLockListenerList != null ? this.atomicLockListenerList : this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != UndoableEditListener.class) continue;
            ((UndoableEditListener)listeners[i + 1]).undoableEditHappened(e);
        }
        this.undoMergeReset = false;
    }

    public final void extWriteLock() {
        super.writeLock();
    }

    public final void extWriteUnlock() {
        super.writeUnlock();
    }

    @Override
    public final void atomicLock() {
        if (LOG.isLoggable(Level.FINER)) {
            LOG.log(Level.FINER, "Use runAtomic() instead of atomicLock()", new Exception());
        }
        this.atomicLockImpl();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final synchronized void atomicLockImpl() {
        boolean alreadyAtomicLocker;
        if (this.runExclusiveDepth > 0) {
            throw new IllegalStateException("Document modifications or atomic locking not allowed in runExclusive()");
        }
        boolean bl = alreadyAtomicLocker = Thread.currentThread() == this.getCurrentWriter() && this.atomicDepth > 0;
        if (alreadyAtomicLocker) {
            ++this.atomicDepth;
        }
        if (!alreadyAtomicLocker) {
            BaseDocument baseDocument = this;
            synchronized (baseDocument) {
                this.extWriteLock();
                ++this.atomicDepth;
                if (this.atomicDepth == 1) {
                    this.fireAtomicLock(this.atomicLockEventInstance);
                    this.atomicLockListenerList = this.listenerList.getListenerList();
                }
            }
        }
    }

    @Override
    public final synchronized void atomicUnlock() {
        this.atomicUnlockImpl();
    }

    final void atomicUnlockImpl() {
        this.atomicUnlockImpl(true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    final void atomicUnlockImpl(boolean notifyUnmodifyIfNoMods) {
        VetoableChangeListener l;
        boolean noModsAndOuterUnlock = false;
        BaseDocument baseDocument = this;
        synchronized (baseDocument) {
            if (this.atomicDepth <= 0) {
                throw new IllegalStateException("atomicUnlock() without atomicLock()");
            }
            if (--this.atomicDepth == 0) {
                this.fireAtomicUnlock(this.atomicLockEventInstance);
                noModsAndOuterUnlock = !this.checkAndFireAtomicEdits();
                this.atomicLockListenerList = null;
                this.extWriteUnlock();
            }
        }
        if (notifyUnmodifyIfNoMods && noModsAndOuterUnlock && (l = (VetoableChangeListener)this.getProperty("modificationListener")) != null) {
            try {
                l.vetoableChange(new PropertyChangeEvent(this, "modified", null, Boolean.FALSE));
            }
            catch (PropertyVetoException ex) {
                // empty catch block
            }
        }
    }

    public final boolean isAtomicLock() {
        return this.atomicDepth > 0;
    }

    public final void breakAtomicLock() {
        this.undoAtomicEdits();
    }

    @Override
    public void atomicUndo() {
        this.breakAtomicLock();
    }

    @Override
    public void addAtomicLockListener(AtomicLockListener l) {
        this.listenerList.add(AtomicLockListener.class, l);
    }

    @Override
    public void removeAtomicLockListener(AtomicLockListener l) {
        this.listenerList.remove(AtomicLockListener.class, l);
    }

    private void fireAtomicLock(AtomicLockEvent evt) {
        EventListener[] listeners = this.listenerList.getListeners(AtomicLockListener.class);
        int cnt = listeners.length;
        for (int i = 0; i < cnt; ++i) {
            ((AtomicLockListener)listeners[i]).atomicLock(evt);
        }
    }

    private void fireAtomicUnlock(AtomicLockEvent evt) {
        EventListener[] listeners = this.listenerList.getListeners(AtomicLockListener.class);
        int cnt = listeners.length;
        for (int i = 0; i < cnt; ++i) {
            ((AtomicLockListener)listeners[i]).atomicUnlock(evt);
        }
    }

    protected final int getAtomicDepth() {
        return this.atomicDepth;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void runExclusive(Runnable r) {
        boolean writeLockDone = false;
        BaseDocument baseDocument = this;
        synchronized (baseDocument) {
            Thread currentWriter = this.getCurrentWriter();
            if (currentWriter != Thread.currentThread()) {
                assert (this.runExclusiveDepth == 0);
                this.writeLock();
                writeLockDone = true;
            }
            ++this.runExclusiveDepth;
        }
        try {
            r.run();
        }
        finally {
            --this.runExclusiveDepth;
            if (writeLockDone) {
                this.writeUnlock();
                assert (this.runExclusiveDepth == 0);
            }
        }
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
        if (LOG_LISTENER.isLoggable(Level.FINE)) {
            LOG_LISTENER.fine("ADD DocumentListener to " + org.netbeans.lib.editor.util.swing.DocumentUtilities.getDocumentListenerCount((Document)this) + " present: " + listener + '\n');
            if (LOG_LISTENER.isLoggable(Level.FINER)) {
                LOG_LISTENER.log(Level.FINER, "    StackTrace:\n", new Exception());
            }
        }
        if (!org.netbeans.lib.editor.util.swing.DocumentUtilities.addPriorityDocumentListener((Document)this, (DocumentListener)listener, (DocumentListenerPriority)DocumentListenerPriority.DEFAULT)) {
            super.addDocumentListener(listener);
        }
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
        if (LOG_LISTENER.isLoggable(Level.FINE)) {
            LOG_LISTENER.fine("REMOVE DocumentListener from " + org.netbeans.lib.editor.util.swing.DocumentUtilities.getDocumentListenerCount((Document)this) + " present: " + listener + '\n');
            if (LOG_LISTENER.isLoggable(Level.FINER)) {
                LOG_LISTENER.log(Level.FINER, "    StackTrace:\n", new Exception());
            }
        }
        if (!org.netbeans.lib.editor.util.swing.DocumentUtilities.removePriorityDocumentListener((Document)this, (DocumentListener)listener, (DocumentListenerPriority)DocumentListenerPriority.DEFAULT)) {
            super.removeDocumentListener(listener);
        }
    }

    protected BaseDocumentEvent createDocumentEvent(int pos, int length, DocumentEvent.EventType type) {
        return new BaseDocumentEvent(this, pos, length, type);
    }

    final BaseDocumentEvent getDocumentEvent(int pos, int length, DocumentEvent.EventType type, AttributeSet attribs) {
        BaseDocumentEvent bde = this.createDocumentEvent(pos, length, type);
        bde.attachChangeAttribs(attribs);
        return bde;
    }

    public void setPostModificationDocumentListener(DocumentListener listener) {
        this.postModificationDocumentListener = listener;
    }

    public void addPostModificationDocumentListener(DocumentListener listener) {
        this.postModificationDocumentListenerList.add((EventListener)listener);
    }

    public void removePostModificationDocumentListener(DocumentListener listener) {
        this.postModificationDocumentListenerList.remove((EventListener)listener);
    }

    public void addUpdateDocumentListener(DocumentListener listener) {
        this.updateDocumentListenerList.add((EventListener)listener);
    }

    public void removeUpdateDocumentListener(DocumentListener listener) {
        this.updateDocumentListenerList.remove((EventListener)listener);
    }

    @Override
    public void addUndoableEditListener(UndoableEditListener listener) {
        UndoableEditListener[] listeners;
        super.addUndoableEditListener(listener);
        if (LOG.isLoggable(Level.FINE) && (listeners = this.getUndoableEditListeners()).length > 1) {
            LOG.log(Level.INFO, "Two or more UndoableEditListeners attached", new Exception());
        }
    }

    public void addUndoableEdit(UndoableEdit edit) {
        if (!this.isAtomicLock()) {
            throw new IllegalStateException("This method can only be called under atomic-lock.");
        }
        this.ensureAtomicEditsInited();
        this.atomicEdits.addEdit(edit);
    }

    public boolean isModified() {
        return this.modified;
    }

    @Override
    public Element getParagraphElement(int pos) {
        return this.lineRootElement.getElement(this.lineRootElement.getElementIndex(pos));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Annotations getAnnotations() {
        Object object = annotationsLock;
        synchronized (object) {
            if (this.annotations == null) {
                this.annotations = new Annotations(this);
            }
            return this.annotations;
        }
    }

    void prepareSyntax(Segment text, Syntax syntax, int reqPos, int reqLen, boolean forceLastBuffer, boolean forceNotLastBuffer) throws BadLocationException {
        FixLineSyntaxState.prepareSyntax(this, text, syntax, reqPos, reqLen, forceLastBuffer, forceNotLastBuffer);
    }

    int getTokenSafeOffset(int offset) {
        return FixLineSyntaxState.getTokenSafeOffset(this, offset);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int getOffsetFromVisCol(int visCol, int startLinePos) throws BadLocationException {
        Object object = getOffsetFromVisColLock;
        synchronized (object) {
            if (startLinePos < 0 || startLinePos >= this.getLength()) {
                throw new BadLocationException("Invalid start line offset", startLinePos);
            }
            if (visCol <= 0) {
                return startLinePos;
            }
            if (this.visColPosFwdFinder == null) {
                this.visColPosFwdFinder = new FinderFactory.VisColPosFwdFinder();
            }
            this.visColPosFwdFinder.setVisCol(visCol);
            this.visColPosFwdFinder.setTabSize(this.getTabSize());
            int pos = this.find(this.visColPosFwdFinder, startLinePos, -1);
            return pos != -1 ? pos : Utilities.getRowEnd(this, startLinePos);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int getVisColFromPos(int pos) throws BadLocationException {
        Object object = getVisColFromPosLock;
        synchronized (object) {
            if (pos < 0 || pos > this.getLength()) {
                throw new BadLocationException("Invalid offset", pos);
            }
            if (this.posVisColFwdFinder == null) {
                this.posVisColFwdFinder = new FinderFactory.PosVisColFwdFinder();
            }
            int startLinePos = Utilities.getRowStart(this, pos);
            this.posVisColFwdFinder.setTabSize(this.getTabSize());
            this.find(this.posVisColFwdFinder, startLinePos, pos);
            return this.posVisColFwdFinder.getVisCol();
        }
    }

    protected Dictionary createDocumentProperties(Dictionary origDocumentProperties) {
        return new LazyPropertyMap(origDocumentProperties);
    }

    private void ensureAtomicEditsInited() {
        if (this.atomicEdits == null) {
            this.atomicEdits = new AtomicCompoundEdit();
        }
    }

    private boolean checkAndFireAtomicEdits() {
        if (this.atomicEdits != null && this.atomicEdits.size() > 0) {
            this.atomicEdits.end();
            AtomicCompoundEdit nonEmptyAtomicEdits = this.atomicEdits;
            this.atomicEdits = null;
            this.fireUndoableEditUpdate(new UndoableEditEvent(this, (UndoableEdit)((Object)nonEmptyAtomicEdits)));
            return true;
        }
        return false;
    }

    private void undoAtomicEdits() {
        if (this.atomicEdits != null && this.atomicEdits.size() > 0) {
            this.atomicEdits.end();
            if (this.atomicEdits.canUndo()) {
                this.atomicEdits.undo();
            } else {
                LOG.log(Level.WARNING, "Cannot UNDO: " + this.atomicEdits.toString() + " Edits: " + this.atomicEdits.getEdits(), new CannotUndoException());
            }
            this.atomicEdits = null;
        }
    }

    void clearAtomicEdits() {
        this.atomicEdits = null;
    }

    UndoableEdit startOnSaveTasks() {
        assert (this.atomicDepth > 0);
        this.checkAndFireAtomicEdits();
        this.ensureAtomicEditsInited();
        return this.atomicEdits;
    }

    void endOnSaveTasks(boolean success) {
        if (success) {
            this.checkAndFireAtomicEdits();
        } else {
            this.undoAtomicEdits();
        }
    }

    UndoableEdit markAtomicEditsNonSignificant() {
        assert (this.atomicDepth > 0);
        this.ensureAtomicEditsInited();
        this.atomicEdits.setSignificant(false);
        return this.atomicEdits;
    }

    void appendInfoTerse(StringBuilder sb) {
        sb.append(this.getClass().getSimpleName()).append("@").append(System.identityHashCode(this));
        sb.append(",version=").append(org.netbeans.lib.editor.util.swing.DocumentUtilities.getDocumentVersion((Document)this));
        sb.append(",StreamDesc=").append(this.getProperty("stream"));
    }

    public String toString() {
        return Object.super.toString() + ", mimeType='" + this.mimeType + "'" + ", kitClass=" + this.deprecatedKitClass + ", length=" + this.getLength() + ", version=" + org.netbeans.lib.editor.util.swing.DocumentUtilities.getDocumentVersion((Document)this) + ", file=" + this.getProperty("stream");
    }

    public String toStringDetail() {
        return this.toString() + ", content:\n  " + this.getContent().toString();
    }

    void incrementDocVersion() {
        ((AtomicLong)this.getProperty("version")).incrementAndGet();
        ((AtomicLong)this.getProperty("last-modification-timestamp")).set(System.currentTimeMillis());
    }

    static {
        EditorPackageAccessor.register(new Accessor());
        EditorDocumentHandler.setEditorDocumentServices(BaseDocument.class, (EditorDocumentServices)BaseDocumentServices.INSTANCE);
        LOG = Logger.getLogger(BaseDocument.class.getName());
        LOG_LISTENER = Logger.getLogger(BaseDocument.class.getName() + "-listener");
        annotationsLock = new Object();
        getVisColFromPosLock = new Object();
        getOffsetFromVisColLock = new Object();
        debugStack = Boolean.getBoolean("netbeans.debug.editor.document.stack");
        debugNoText = Boolean.getBoolean("netbeans.debug.editor.document.notext");
        debugRead = Boolean.getBoolean("netbeans.debug.editor.document.read");
    }

    private static final class BaseDocumentServices
    implements EditorDocumentServices {
        static final EditorDocumentServices INSTANCE = new BaseDocumentServices();

        private BaseDocumentServices() {
        }

        public void runExclusive(Document doc, Runnable r) {
            BaseDocument bDoc = (BaseDocument)doc;
            bDoc.runExclusive(r);
        }

        public void resetUndoMerge(Document doc) {
            BaseDocument bDoc = (BaseDocument)doc;
            bDoc.resetUndoMerge();
        }

        public UndoableEdit startOnSaveTasks(Document doc) {
            BaseDocument bDoc = (BaseDocument)doc;
            return bDoc.startOnSaveTasks();
        }

        public void endOnSaveTasks(Document doc, boolean success) {
            BaseDocument bDoc = (BaseDocument)doc;
            bDoc.endOnSaveTasks(success);
        }
    }

    class FilterBypassImpl
    extends DocumentFilter.FilterBypass {
        FilterBypassImpl() {
        }

        @Override
        public Document getDocument() {
            return BaseDocument.this;
        }

        @Override
        public void remove(int offset, int length) throws BadLocationException {
            BaseDocument.this.handleRemove(offset, length);
        }

        @Override
        public void insertString(int offset, String string, AttributeSet attrs) throws BadLocationException {
            BaseDocument.this.handleInsertString(offset, string, attrs);
        }

        @Override
        public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            BaseDocument.this.handleRemove(offset, length);
            BaseDocument.this.handleInsertString(offset, text, attrs);
        }
    }

    private static final class PlainEditorKit
    extends DefaultEditorKit
    implements ViewFactory {
        static final long serialVersionUID = 1;

        PlainEditorKit() {
        }

        @Override
        public Object clone() {
            return new PlainEditorKit();
        }

        @Override
        public ViewFactory getViewFactory() {
            return this;
        }

        @Override
        public View create(Element elem) {
            return new WrappedPlainView(elem);
        }

        @Override
        public void install(JEditorPane pane) {
            super.install(pane);
            pane.setFont(new Font("Monospaced", 0, pane.getFont().getSize() + 1));
        }
    }

    private static final class Accessor
    extends EditorPackageAccessor {
        private Accessor() {
        }

        @Override
        public UndoableEdit BaseDocument_markAtomicEditsNonSignificant(BaseDocument doc) {
            return doc.markAtomicEditsNonSignificant();
        }

        @Override
        public void BaseDocument_clearAtomicEdits(BaseDocument doc) {
            doc.clearAtomicEdits();
        }

        @Override
        public MarkVector BaseDocument_getMarksStorage(BaseDocument doc) {
            return null;
        }

        @Override
        public Mark BaseDocument_getMark(BaseDocument doc, MultiMark multiMark) {
            return null;
        }

        @Override
        public void Mark_insert(Mark mark, BaseDocument doc, int pos) throws InvalidMarkException, BadLocationException {
            mark.insert(doc, pos);
        }

        @Override
        public void ActionFactory_reformat(Reformat formatter, Document doc, int startPos, int endPos, AtomicBoolean canceled) throws BadLocationException {
            ActionFactory.reformat(formatter, doc, startPos, endPos, canceled);
        }
    }

    protected static class LazyPropertyMap
    extends Hashtable {
        private PropertyChangeSupport pcs = null;

        protected LazyPropertyMap(Dictionary dict) {
            super(5);
            Enumeration en = dict.keys();
            while (en.hasMoreElements()) {
                Object key = en.nextElement();
                super.put(key, dict.get(key));
            }
        }

        @Override
        public Object get(Object key) {
            Object val = super.get(key);
            if (val instanceof PropertyEvaluator) {
                val = ((PropertyEvaluator)val).getValue();
            }
            return val;
        }

        @Override
        public Object put(Object key, Object value) {
            Object val;
            if (key == PropertyChangeSupport.class && value instanceof PropertyChangeSupport) {
                this.pcs = (PropertyChangeSupport)value;
            }
            Object old = null;
            boolean usePlainPut = true;
            if (key != null && (val = super.get(key)) instanceof BaseDocument_PropertyHandler) {
                old = ((BaseDocument_PropertyHandler)val).setValue(value);
                usePlainPut = false;
            }
            if (usePlainPut) {
                old = super.put(key, value);
            }
            if (key instanceof String && this.pcs != null) {
                this.pcs.firePropertyChange((String)key, old, value);
            }
            return old;
        }
    }

    private static final class MimeTypePropertyEvaluator
    implements BaseDocument_PropertyHandler {
        private final BaseDocument doc;
        private String hackMimeType = null;

        public MimeTypePropertyEvaluator(BaseDocument baseDocument) {
            this.doc = baseDocument;
        }

        @Override
        public Object getValue() {
            if (this.hackMimeType == null) {
                return this.doc.mimeType;
            }
            return this.hackMimeType;
        }

        @Override
        public Object setValue(Object value) {
            String mimeType;
            String string = mimeType = value == null ? null : value.toString();
            assert (MimePath.validate((CharSequence)mimeType));
            boolean hackNewMimeType = mimeType != null && mimeType.startsWith("test");
            String oldValue = (String)this.getValue();
            if (hackNewMimeType) {
                this.hackMimeType = mimeType;
            } else {
                this.doc.setMimeType(mimeType);
            }
            return oldValue;
        }
    }

    public static interface PropertyEvaluator {
        public Object getValue();
    }

    class AtomicCompoundEdit
    extends StableCompoundEdit {
        private UndoableEdit previousEdit;
        private boolean nonSignificant;

        AtomicCompoundEdit() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void undo() throws CannotUndoException {
            BaseDocument.this.atomicLockImpl();
            try {
                TokenHierarchyControl thcInactive = this.thcInactive();
                try {
                    super.undo();
                }
                finally {
                    if (thcInactive != null) {
                        thcInactive.setActive(true);
                    }
                }
            }
            finally {
                BaseDocument.this.atomicUnlockImpl();
            }
            if (this.previousEdit != null) {
                this.previousEdit.undo();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void redo() throws CannotRedoException {
            if (this.previousEdit != null) {
                this.previousEdit.redo();
            }
            BaseDocument.this.atomicLockImpl();
            try {
                TokenHierarchyControl thcInactive = this.thcInactive();
                try {
                    super.redo();
                }
                finally {
                    if (thcInactive != null) {
                        thcInactive.setActive(true);
                    }
                }
            }
            finally {
                BaseDocument.this.atomicUnlockImpl();
            }
        }

        private TokenHierarchyControl<?> thcInactive() {
            TokenHierarchyControl thc = null;
            if (this.getEdits().size() > 30) {
                MutableTextInput input = (MutableTextInput)BaseDocument.this.getProperty(MutableTextInput.class);
                if (input != null && (thc = input.tokenHierarchyControl()) != null && thc.isActive()) {
                    thc.setActive(false);
                } else {
                    thc = null;
                }
            }
            return thc;
        }

        public void die() {
            super.die();
            if (this.previousEdit != null) {
                if (this.previousEdit != this) {
                    this.previousEdit.die();
                }
                this.previousEdit = null;
            }
        }

        public int size() {
            return this.getEdits().size();
        }

        public boolean replaceEdit(UndoableEdit anEdit) {
            UndoableEdit childEdit;
            if (this.nonSignificant) {
                this.previousEdit = anEdit;
                this.nonSignificant = false;
                return true;
            }
            if (this.size() == 1 && (childEdit = (UndoableEdit)this.getEdits().get(0)) instanceof BaseDocumentEvent) {
                BaseDocumentEvent childEvt = (BaseDocumentEvent)childEdit;
                if (anEdit instanceof AtomicCompoundEdit) {
                    UndoableEdit edit;
                    AtomicCompoundEdit compEdit = (AtomicCompoundEdit)((Object)anEdit);
                    if (!BaseDocument.this.undoMergeReset && compEdit.getEdits().size() == 1 && (edit = (UndoableEdit)compEdit.getEdits().get(0)) instanceof BaseDocumentEvent && childEvt.canMerge((BaseDocumentEvent)edit)) {
                        this.previousEdit = anEdit;
                        return true;
                    }
                } else if (anEdit instanceof BaseDocumentEvent) {
                    BaseDocumentEvent evt = (BaseDocumentEvent)anEdit;
                    if (!BaseDocument.this.undoMergeReset && childEvt.canMerge(evt)) {
                        this.previousEdit = anEdit;
                        return true;
                    }
                }
            }
            return false;
        }

        public boolean isSignificant() {
            return !this.nonSignificant;
        }

        public void setSignificant(boolean significant) {
            this.nonSignificant = !significant;
        }
    }

}

