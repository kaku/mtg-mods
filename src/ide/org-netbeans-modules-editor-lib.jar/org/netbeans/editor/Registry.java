/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.openide.modules.PatchedPublic
 */
package org.netbeans.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WeakEventListenerList;
import org.openide.modules.PatchedPublic;

public class Registry {
    private static final WeakEventListenerList listenerList = new WeakEventListenerList();
    private static PropertyChangeListener editorRegistryListener = null;

    public static void addChangeListener(ChangeListener l) {
        Registry.registerListener();
        listenerList.add(ChangeListener.class, l);
    }

    public static void removeChangeListener(ChangeListener l) {
        Registry.registerListener();
        listenerList.remove(ChangeListener.class, l);
    }

    @PatchedPublic
    private static int getID(BaseDocument doc) {
        Registry.registerListener();
        return -1;
    }

    @PatchedPublic
    private static int getID(JTextComponent c) {
        Registry.registerListener();
        return -1;
    }

    @PatchedPublic
    private static BaseDocument getDocument(int docID) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static JTextComponent getComponent(int compID) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static int addDocument(BaseDocument doc) {
        Registry.registerListener();
        return -1;
    }

    @PatchedPublic
    private static int addComponent(JTextComponent c) {
        Registry.registerListener();
        return -1;
    }

    @PatchedPublic
    private static int removeComponent(JTextComponent c) {
        Registry.registerListener();
        return -1;
    }

    @PatchedPublic
    private static void activate(JTextComponent c) {
        Registry.registerListener();
    }

    @PatchedPublic
    private static void activate(BaseDocument doc) {
        Registry.registerListener();
    }

    public static BaseDocument getMostActiveDocument() {
        Registry.registerListener();
        JTextComponent jtc = Registry.getMostActiveComponent();
        return jtc == null ? null : Utilities.getDocument(jtc);
    }

    public static BaseDocument getLeastActiveDocument() {
        Registry.registerListener();
        JTextComponent jtc = Registry.getLeastActiveComponent();
        return jtc == null ? null : Utilities.getDocument(jtc);
    }

    @PatchedPublic
    private static BaseDocument getLessActiveDocument(BaseDocument doc) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static BaseDocument getLessActiveDocument(int docID) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static BaseDocument getMoreActiveDocument(BaseDocument doc) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static BaseDocument getMoreActiveDocument(int docID) {
        Registry.registerListener();
        return null;
    }

    public static Iterator<? extends Document> getDocumentIterator() {
        Registry.registerListener();
        final Iterator<? extends JTextComponent> cIt = Registry.getComponentIterator();
        return new Iterator<Document>(){

            @Override
            public boolean hasNext() {
                return cIt.hasNext();
            }

            @Override
            public Document next() {
                return ((JTextComponent)cIt.next()).getDocument();
            }

            @Override
            public void remove() {
                cIt.remove();
            }
        };
    }

    public static JTextComponent getMostActiveComponent() {
        Registry.registerListener();
        return EditorRegistry.focusedComponent();
    }

    public static JTextComponent getLeastActiveComponent() {
        Registry.registerListener();
        List l = EditorRegistry.componentList();
        return l.size() > 0 ? (JTextComponent)l.get(l.size() - 1) : null;
    }

    @PatchedPublic
    private static JTextComponent getLessActiveComponent(JTextComponent c) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static JTextComponent getLessActiveComponent(int compID) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static JTextComponent getMoreActiveComponent(JTextComponent c) {
        Registry.registerListener();
        return null;
    }

    @PatchedPublic
    private static JTextComponent getMoreActiveComponent(int compID) {
        Registry.registerListener();
        return null;
    }

    public static Iterator<? extends JTextComponent> getComponentIterator() {
        Registry.registerListener();
        return EditorRegistry.componentList().iterator();
    }

    private static synchronized void registerListener() {
        if (editorRegistryListener == null) {
            editorRegistryListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    Registry.fireChange();
                }
            };
            EditorRegistry.addPropertyChangeListener((PropertyChangeListener)editorRegistryListener);
        }
    }

    private static void fireChange() {
        ChangeListener[] listeners = (ChangeListener[])listenerList.getListeners(ChangeListener.class);
        ChangeEvent evt = new ChangeEvent(Registry.class);
        for (int i = 0; i < listeners.length; ++i) {
            listeners[i].stateChanged(evt);
        }
    }

    public static String registryToString() {
        Registry.registerListener();
        try {
            Method m = EditorRegistry.class.getDeclaredMethod("dumpItemList", new Class[0]);
            return (String)m.invoke(null, new Object[0]);
        }
        catch (Exception e) {
            return "";
        }
    }

}

