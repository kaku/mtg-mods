/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.BaseTokenID;
import org.netbeans.editor.ImageTokenID;
import org.netbeans.editor.TokenCategory;

public class BaseImageTokenID
extends BaseTokenID
implements ImageTokenID {
    private final String image;

    public BaseImageTokenID(String nameAndImage) {
        this(nameAndImage, nameAndImage);
    }

    public BaseImageTokenID(String name, String image) {
        super(name);
        this.image = image;
    }

    public BaseImageTokenID(String nameAndImage, int numericID) {
        this(nameAndImage, numericID, nameAndImage);
    }

    public BaseImageTokenID(String name, int numericID, String image) {
        super(name, numericID);
        this.image = image;
    }

    public BaseImageTokenID(String nameAndImage, TokenCategory category) {
        this(nameAndImage, category, nameAndImage);
    }

    public BaseImageTokenID(String name, TokenCategory category, String image) {
        super(name, category);
        this.image = image;
    }

    public BaseImageTokenID(String nameAndImage, int numericID, TokenCategory category) {
        this(nameAndImage, numericID, category, nameAndImage);
    }

    public BaseImageTokenID(String name, int numericID, TokenCategory category, String image) {
        super(name, numericID, category);
        this.image = image;
    }

    @Override
    public String getImage() {
        return this.image;
    }

    @Override
    public String toString() {
        return super.toString() + ", image='" + this.getImage() + "'";
    }
}

