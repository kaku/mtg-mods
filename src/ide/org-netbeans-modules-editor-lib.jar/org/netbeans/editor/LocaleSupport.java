/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.MissingResourceException;

public class LocaleSupport {
    private static final boolean debug = Boolean.getBoolean("netbeans.debug.editor.localesupport");
    private static Localizer[] localizers = new Localizer[0];

    public static void addLocalizer(Localizer localizer) {
        ArrayList<Localizer> ll = new ArrayList<Localizer>(Arrays.asList(localizers));
        ll.add(localizer);
        Localizer[] la = new Localizer[ll.size()];
        ll.toArray(la);
        localizers = la;
    }

    public static void removeLocalizer(Localizer localizer) {
        ArrayList<Localizer> ll = new ArrayList<Localizer>(Arrays.asList(localizers));
        ll.remove(localizer);
        Localizer[] la = new Localizer[ll.size()];
        ll.toArray(la);
        localizers = la;
    }

    public static synchronized String getString(String key) {
        int i;
        String ret = null;
        for (i = LocaleSupport.localizers.length - 1; i >= 0; --i) {
            try {
                ret = localizers[i].getString(key);
            }
            catch (MissingResourceException e) {
                ret = null;
            }
            if (ret != null) break;
        }
        if (debug) {
            String inLocalizerString = i >= 0 ? " found in localizer=" + localizers[i] : "";
            System.err.println("LocaleSupport.getString(): key=\"" + key + "\", value=\"" + ret + "\"" + inLocalizerString);
            Thread.dumpStack();
        }
        return ret;
    }

    public static String getString(String key, String defaultValue) {
        String ret = LocaleSupport.getString(key);
        return ret != null ? ret : defaultValue;
    }

    public static char getChar(String key, char defaultValue) {
        String value = LocaleSupport.getString(key);
        if (value == null || value.length() < 1) {
            return defaultValue;
        }
        return value.charAt(0);
    }

    public static interface Localizer {
        public String getString(String var1);
    }

}

