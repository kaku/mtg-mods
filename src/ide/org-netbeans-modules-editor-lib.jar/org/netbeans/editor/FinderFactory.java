/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.Acceptor;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Finder;
import org.netbeans.modules.editor.lib.WcwdithUtil;

public class FinderFactory {

    public static interface BlocksFinder
    extends Finder {
        public void setBlocks(int[] var1);

        public int[] getBlocks();
    }

    public static interface StringFinder
    extends Finder {
        public int getFoundLength();
    }

    public static final class WholeWordsBlocksFinder
    extends AbstractBlocksFinder {
        char[] chars;
        int stringInd;
        boolean matchCase;
        boolean insideWord;
        boolean firstCharWordPart;
        boolean wordFound;
        BaseDocument doc;

        public WholeWordsBlocksFinder(BaseDocument doc, String s, boolean matchCase) {
            this.doc = doc;
            this.matchCase = matchCase;
            this.chars = (matchCase ? s : s.toLowerCase()).toCharArray();
            this.firstCharWordPart = doc.isIdentifierPart(this.chars[0]);
        }

        @Override
        public void reset() {
            super.reset();
            this.insideWord = false;
            this.wordFound = false;
            this.stringInd = 0;
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset = reqPos - bufferStartPos;
            int limitOffset = limitPos - bufferStartPos - 1;
            while (offset >= offset1 && offset < offset2) {
                int blkEnd;
                char ch = buffer[offset];
                if (!this.matchCase) {
                    ch = Character.toLowerCase(ch);
                }
                if (this.wordFound) {
                    if (this.doc.isIdentifierPart(ch)) {
                        this.insideWord = this.firstCharWordPart;
                        offset -= this.chars.length - 1;
                    } else {
                        blkEnd = bufferStartPos + offset;
                        this.addBlock(blkEnd - this.chars.length, blkEnd);
                        this.insideWord = false;
                        ++offset;
                    }
                    this.wordFound = false;
                    this.stringInd = 0;
                    continue;
                }
                if (this.stringInd == 0) {
                    if (ch != this.chars[0] || this.insideWord) {
                        this.insideWord = this.doc.isIdentifierPart(ch);
                        ++offset;
                        continue;
                    }
                    this.stringInd = 1;
                    if (this.chars.length == 1) {
                        if (offset == limitOffset) {
                            int blkStart = bufferStartPos + offset;
                            this.addBlock(blkStart, blkStart + 1);
                        } else {
                            this.wordFound = true;
                        }
                    }
                    ++offset;
                    continue;
                }
                if (ch == this.chars[this.stringInd]) {
                    ++this.stringInd;
                    if (this.stringInd == this.chars.length) {
                        if (offset == limitOffset) {
                            blkEnd = bufferStartPos + 1;
                            this.addBlock(blkEnd - this.stringInd, blkEnd);
                        } else {
                            this.wordFound = true;
                        }
                    }
                    ++offset;
                    continue;
                }
                offset += 1 - this.stringInd;
                this.stringInd = 0;
                this.insideWord = this.firstCharWordPart;
            }
            reqPos = bufferStartPos + offset;
            return reqPos;
        }
    }

    public static final class StringBlocksFinder
    extends AbstractBlocksFinder {
        char[] chars;
        int stringInd;
        boolean matchCase;

        public StringBlocksFinder(String s, boolean matchCase) {
            this.matchCase = matchCase;
            this.chars = (matchCase ? s : s.toLowerCase()).toCharArray();
        }

        @Override
        public void reset() {
            super.reset();
            this.stringInd = 0;
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset = reqPos - bufferStartPos;
            while (offset >= offset1 && offset < offset2) {
                char ch = buffer[offset];
                if (!this.matchCase) {
                    ch = Character.toLowerCase(ch);
                }
                if (ch == this.chars[this.stringInd]) {
                    ++this.stringInd;
                    if (this.stringInd == this.chars.length) {
                        int blkEnd = bufferStartPos + offset + 1;
                        this.addBlock(blkEnd - this.stringInd, blkEnd);
                        this.stringInd = 0;
                    }
                    ++offset;
                    continue;
                }
                offset += 1 - this.stringInd;
                this.stringInd = 0;
            }
            reqPos = bufferStartPos + offset;
            return reqPos;
        }
    }

    public static final class FalseBlocksFinder
    extends AbstractBlocksFinder {
        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            return -1;
        }
    }

    public static abstract class AbstractBlocksFinder
    extends AbstractFinder
    implements BlocksFinder {
        private static int[] EMPTY_INT_ARRAY = new int[0];
        private int[] blocks = EMPTY_INT_ARRAY;
        private int blocksInd;
        private boolean closed;

        @Override
        public void reset() {
            this.blocksInd = 0;
            this.closed = false;
        }

        @Override
        public int[] getBlocks() {
            if (!this.closed) {
                this.closeBlocks();
                this.closed = true;
            }
            return this.blocks;
        }

        @Override
        public void setBlocks(int[] blocks) {
            this.blocks = blocks;
            this.closed = false;
        }

        protected void addBlock(int blkStartPos, int blkEndPos) {
            if (this.blocksInd == this.blocks.length) {
                int[] dbl = new int[this.blocks.length * 2];
                System.arraycopy(this.blocks, 0, dbl, 0, this.blocks.length);
                this.blocks = dbl;
            }
            this.blocks[this.blocksInd++] = blkStartPos;
            this.blocks[this.blocksInd++] = blkEndPos;
        }

        protected void closeBlocks() {
            this.addBlock(-1, -1);
        }

        public String debugBlocks() {
            StringBuffer buf = new StringBuffer();
            int ind = 0;
            while (this.blocks[ind] != -1) {
                buf.append("" + (ind / 2 + 1) + ": [" + this.blocks[ind] + ", " + this.blocks[ind + 1] + "]\n");
                ind += 2;
            }
            return buf.toString();
        }
    }

    public static final class WholeWordsBwdFinder
    extends GenericBwdFinder
    implements StringFinder {
        char[] chars;
        int stringInd;
        boolean matchCase;
        boolean insideWord;
        boolean lastCharWordPart;
        boolean wordFound;
        int endInd;
        BaseDocument doc;

        public WholeWordsBwdFinder(BaseDocument doc, String s, boolean matchCase) {
            this.doc = doc;
            this.matchCase = matchCase;
            this.chars = (matchCase ? s : s.toLowerCase()).toCharArray();
            this.endInd = this.chars.length - 1;
            doc.isIdentifierPart(this.chars[this.endInd]);
        }

        @Override
        public int getFoundLength() {
            return this.chars.length;
        }

        @Override
        public void reset() {
            super.reset();
            this.insideWord = false;
            this.wordFound = false;
            this.stringInd = this.endInd;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.matchCase) {
                ch = Character.toLowerCase(ch);
            }
            if (this.wordFound) {
                if (this.doc.isIdentifierPart(ch)) {
                    this.wordFound = false;
                    this.insideWord = this.lastCharWordPart;
                    this.stringInd = this.endInd;
                    return this.endInd;
                }
                this.found = true;
                return 1;
            }
            if (this.stringInd == this.endInd) {
                if (ch != this.chars[this.endInd] || this.insideWord) {
                    this.insideWord = this.doc.isIdentifierPart(ch);
                    return -1;
                }
                this.stringInd = this.endInd - 1;
                if (this.chars.length == 1) {
                    if (lastChar) {
                        this.found = true;
                        return 0;
                    }
                    this.wordFound = true;
                    return -1;
                }
                return -1;
            }
            if (ch == this.chars[this.stringInd]) {
                --this.stringInd;
                if (this.stringInd == -1) {
                    if (lastChar) {
                        this.found = true;
                        return 0;
                    }
                    this.wordFound = true;
                    return -1;
                }
                return -1;
            }
            int back = this.chars.length - 2 - this.stringInd;
            this.stringInd = this.endInd;
            this.insideWord = this.lastCharWordPart;
            return back;
        }
    }

    public static final class WholeWordsFwdFinder
    extends GenericFwdFinder
    implements StringFinder {
        char[] chars;
        int stringInd;
        boolean matchCase;
        BaseDocument doc;
        boolean insideWord;
        boolean firstCharWordPart;
        boolean wordFound;

        public WholeWordsFwdFinder(BaseDocument doc, String s, boolean matchCase) {
            this.doc = doc;
            this.matchCase = matchCase;
            this.chars = (matchCase ? s : s.toLowerCase()).toCharArray();
            this.firstCharWordPart = doc.isIdentifierPart(this.chars[0]);
        }

        @Override
        public int getFoundLength() {
            return this.chars.length;
        }

        @Override
        public void reset() {
            super.reset();
            this.insideWord = false;
            this.wordFound = false;
            this.stringInd = 0;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.matchCase) {
                ch = Character.toLowerCase(ch);
            }
            if (this.wordFound) {
                if (this.doc.isIdentifierPart(ch)) {
                    this.wordFound = false;
                    this.insideWord = this.firstCharWordPart;
                    this.stringInd = 0;
                    return 1 - this.chars.length;
                }
                this.found = true;
                return - this.chars.length;
            }
            if (this.stringInd == 0) {
                if (ch != this.chars[0] || this.insideWord) {
                    this.insideWord = this.doc.isIdentifierPart(ch);
                    return 1;
                }
                this.stringInd = 1;
                if (this.chars.length == 1) {
                    if (lastChar) {
                        this.found = true;
                        return 0;
                    }
                    this.wordFound = true;
                    return 1;
                }
                return 1;
            }
            if (ch == this.chars[this.stringInd]) {
                ++this.stringInd;
                if (this.stringInd == this.chars.length) {
                    if (lastChar) {
                        this.found = true;
                        return 1 - this.chars.length;
                    }
                    this.wordFound = true;
                    return 1;
                }
                return 1;
            }
            int back = 1 - this.stringInd;
            this.stringInd = 0;
            this.insideWord = this.firstCharWordPart;
            return back;
        }
    }

    public static class StringBwdFinder
    extends GenericBwdFinder
    implements StringFinder {
        char[] chars;
        int stringInd;
        boolean matchCase;
        int endInd;

        public StringBwdFinder(String s, boolean matchCase) {
            this.matchCase = matchCase;
            this.chars = (matchCase ? s : s.toLowerCase()).toCharArray();
            this.endInd = this.chars.length - 1;
        }

        @Override
        public int getFoundLength() {
            return this.chars.length;
        }

        @Override
        public void reset() {
            super.reset();
            this.stringInd = this.endInd;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.matchCase) {
                ch = Character.toLowerCase(ch);
            }
            if (ch == this.chars[this.stringInd]) {
                --this.stringInd;
                if (this.stringInd == -1) {
                    this.found = true;
                    return 0;
                }
                return -1;
            }
            if (this.stringInd == this.endInd) {
                return -1;
            }
            int back = this.chars.length - 2 - this.stringInd;
            this.stringInd = this.endInd;
            return back;
        }
    }

    public static final class StringFwdFinder
    extends GenericFwdFinder
    implements StringFinder {
        char[] chars;
        int stringInd;
        boolean matchCase;

        public StringFwdFinder(String s, boolean matchCase) {
            this.matchCase = matchCase;
            this.chars = (matchCase ? s : s.toLowerCase()).toCharArray();
        }

        @Override
        public int getFoundLength() {
            return this.chars.length;
        }

        @Override
        public void reset() {
            super.reset();
            this.stringInd = 0;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.matchCase) {
                ch = Character.toLowerCase(ch);
            }
            if (ch == this.chars[this.stringInd]) {
                ++this.stringInd;
                if (this.stringInd == this.chars.length) {
                    this.found = true;
                    return 1 - this.stringInd;
                }
                return 1;
            }
            if (this.stringInd == 0) {
                return 1;
            }
            int back = 1 - this.stringInd;
            this.stringInd = 0;
            return back;
        }
    }

    public static class NonWhiteBwdFinder
    extends GenericBwdFinder {
        BaseDocument doc;
        private char foundChar;

        public NonWhiteBwdFinder(BaseDocument doc) {
            this.doc = doc;
        }

        public char getFoundChar() {
            return this.foundChar;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.doc.isWhitespace(ch)) {
                this.found = true;
                this.foundChar = ch;
                return 0;
            }
            return -1;
        }
    }

    public static class NonWhiteFwdFinder
    extends GenericFwdFinder {
        BaseDocument doc;
        private char foundChar;

        public NonWhiteFwdFinder(BaseDocument doc) {
            this.doc = doc;
        }

        public char getFoundChar() {
            return this.foundChar;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.doc.isWhitespace(ch)) {
                this.found = true;
                this.foundChar = ch;
                return 0;
            }
            return 1;
        }
    }

    public static class WhiteBwdFinder
    extends GenericBwdFinder {
        BaseDocument doc;
        private char foundChar;

        public WhiteBwdFinder(BaseDocument doc) {
            this.doc = doc;
        }

        public char getFoundChar() {
            return this.foundChar;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (this.doc.isWhitespace(ch)) {
                this.found = true;
                this.foundChar = ch;
                return 0;
            }
            return -1;
        }
    }

    public static class WhiteFwdFinder
    extends GenericFwdFinder {
        BaseDocument doc;
        private char foundChar;

        public WhiteFwdFinder(BaseDocument doc) {
            this.doc = doc;
        }

        public char getFoundChar() {
            return this.foundChar;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (this.doc.isWhitespace(ch)) {
                this.found = true;
                this.foundChar = ch;
                return 0;
            }
            return 1;
        }
    }

    public static class PreviousWordBwdFinder
    extends GenericBwdFinder {
        BaseDocument doc;
        boolean inIdentifier;
        boolean inPunct;
        boolean stopOnEOL;
        boolean stopOnWhitespace;
        boolean firstChar;

        public PreviousWordBwdFinder(BaseDocument doc, boolean stopOnEOL, boolean stopOnWhitespace) {
            this.doc = doc;
            this.stopOnEOL = stopOnEOL;
            this.stopOnWhitespace = stopOnWhitespace;
        }

        @Override
        public void reset() {
            super.reset();
            this.inIdentifier = false;
            this.inPunct = false;
            this.firstChar = true;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (this.stopOnEOL) {
                if (ch == '\n') {
                    this.found = true;
                    return this.firstChar ? 0 : 1;
                }
                this.firstChar = false;
            }
            if (this.inIdentifier) {
                if (this.doc.isIdentifierPart(ch)) {
                    if (lastChar) {
                        this.found = true;
                        return 0;
                    }
                    return -1;
                }
                this.found = true;
                return 1;
            }
            if (this.inPunct) {
                if (this.doc.isIdentifierPart(ch) || this.doc.isWhitespace(ch) || lastChar) {
                    this.found = true;
                    return 1;
                }
                return -1;
            }
            if (this.doc.isWhitespace(ch)) {
                if (this.stopOnWhitespace) {
                    this.found = true;
                    return 1;
                }
                return -1;
            }
            if (this.doc.isIdentifierPart(ch)) {
                this.inIdentifier = true;
                if (lastChar) {
                    this.found = true;
                    return 0;
                }
                return -1;
            }
            this.inPunct = true;
            return -1;
        }
    }

    public static class NextWordFwdFinder
    extends GenericFwdFinder {
        BaseDocument doc;
        boolean inWhitespace;
        boolean inIdentifier;
        boolean inPunct;
        boolean firstChar;
        boolean stopOnEOL;
        boolean stopOnWhitespace;

        public NextWordFwdFinder(BaseDocument doc, boolean stopOnEOL, boolean stopOnWhitespace) {
            this.doc = doc;
            this.stopOnEOL = stopOnEOL;
            this.stopOnWhitespace = stopOnWhitespace;
        }

        @Override
        public void reset() {
            super.reset();
            this.inWhitespace = false;
            this.inIdentifier = false;
            this.inPunct = false;
            this.firstChar = true;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (this.stopOnEOL) {
                if (ch == '\n') {
                    this.found = true;
                    return this.firstChar ? 1 : 0;
                }
                this.firstChar = false;
            }
            if (this.doc.isWhitespace(ch)) {
                if (this.stopOnWhitespace) {
                    this.found = true;
                    return 0;
                }
                this.inWhitespace = true;
                return 1;
            }
            if (this.inWhitespace) {
                this.found = true;
                return 0;
            }
            if (this.inIdentifier) {
                if (this.doc.isIdentifierPart(ch)) {
                    return 1;
                }
                this.found = true;
                return 0;
            }
            if (this.inPunct) {
                if (this.doc.isIdentifierPart(ch)) {
                    this.found = true;
                    return 0;
                }
                return 1;
            }
            if (this.doc.isIdentifierPart(ch)) {
                this.inIdentifier = true;
                return 1;
            }
            this.inPunct = true;
            return 1;
        }
    }

    public static class AcceptorBwdFinder
    extends GenericBwdFinder {
        Acceptor a;

        public AcceptorBwdFinder(Acceptor a) {
            this.a = a;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.a.accept(ch)) {
                this.found = true;
                return 0;
            }
            return -1;
        }
    }

    public static class CharArrayBwdFinder
    extends GenericBwdFinder {
        char[] searchChars;
        char foundChar;

        public CharArrayBwdFinder(char[] searchChars) {
            this.searchChars = searchChars;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            for (int i = 0; i < this.searchChars.length; ++i) {
                if (ch != this.searchChars[i]) continue;
                this.foundChar = this.searchChars[i];
                this.found = true;
                return 0;
            }
            return -1;
        }

        public char getFoundChar() {
            return this.foundChar;
        }
    }

    public static class AcceptorFwdFinder
    extends GenericFwdFinder {
        Acceptor a;

        public AcceptorFwdFinder(Acceptor a) {
            this.a = a;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (!this.a.accept(ch)) {
                this.found = true;
                return 0;
            }
            return 1;
        }
    }

    public static class CharArrayFwdFinder
    extends GenericFwdFinder {
        char[] searchChars;
        char foundChar;

        public CharArrayFwdFinder(char[] searchChars) {
            this.searchChars = searchChars;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            for (int i = 0; i < this.searchChars.length; ++i) {
                if (ch != this.searchChars[i]) continue;
                this.foundChar = this.searchChars[i];
                this.found = true;
                return 0;
            }
            return 1;
        }

        public char getFoundChar() {
            return this.foundChar;
        }
    }

    public static class CharBwdFinder
    extends GenericBwdFinder {
        char searchChar;

        public CharBwdFinder(char searchChar) {
            this.searchChar = searchChar;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (ch == this.searchChar) {
                this.found = true;
                return 0;
            }
            return -1;
        }
    }

    public static class CharFwdFinder
    extends GenericFwdFinder {
        char searchChar;

        public CharFwdFinder(char searchChar) {
            this.searchChar = searchChar;
        }

        @Override
        protected int scan(char ch, boolean lastChar) {
            if (ch == this.searchChar) {
                this.found = true;
                return 0;
            }
            return 1;
        }
    }

    public static abstract class GenericFinder
    extends AbstractFinder {
        protected boolean forward;

        public boolean isForward() {
            return this.forward;
        }

        @Override
        public final int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset = reqPos - bufferStartPos;
            int limitOffset = limitPos - bufferStartPos;
            if (this.forward) {
                --limitOffset;
            }
            while (offset >= offset1 && offset < offset2) {
                offset += this.scan(buffer[offset], offset == limitOffset);
                if (!this.found) continue;
            }
            return bufferStartPos + offset;
        }

        protected abstract int scan(char var1, boolean var2);
    }

    public static abstract class GenericBwdFinder
    extends AbstractFinder {
        @Override
        public final int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset;
            int limitOffset = limitPos - bufferStartPos;
            for (offset = reqPos - bufferStartPos; offset >= offset1 && offset < offset2; offset += this.scan((char)buffer[offset], (boolean)(offset == limitOffset))) {
                if (!this.found) continue;
            }
            return bufferStartPos + offset;
        }

        protected abstract int scan(char var1, boolean var2);
    }

    public static abstract class GenericFwdFinder
    extends AbstractFinder {
        @Override
        public final int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset;
            int limitOffset = limitPos - bufferStartPos - 1;
            for (offset = reqPos - bufferStartPos; offset >= offset1 && offset < offset2; offset += this.scan((char)buffer[offset], (boolean)(offset == limitOffset))) {
                if (!this.found) continue;
            }
            return bufferStartPos + offset;
        }

        protected abstract int scan(char var1, boolean var2);
    }

    public static final class VisColPosFwdFinder
    extends AbstractFinder {
        int visCol;
        int curVisCol;
        int tabSize;
        EditorUI editorUI;

        public void setVisCol(int visCol) {
            this.visCol = visCol;
        }

        public void setTabSize(int tabSize) {
            this.tabSize = tabSize;
        }

        @Override
        public void reset() {
            super.reset();
            this.curVisCol = 0;
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset;
            block4 : for (offset = reqPos - bufferStartPos; offset < offset2; ++offset) {
                if (this.curVisCol >= this.visCol) {
                    this.found = true;
                    return bufferStartPos + offset;
                }
                switch (buffer[offset]) {
                    case '\t': {
                        this.curVisCol = (this.curVisCol + this.tabSize) / this.tabSize * this.tabSize;
                        continue block4;
                    }
                    case '\n': {
                        this.found = true;
                        return bufferStartPos + offset;
                    }
                    default: {
                        int codePoint;
                        if (Character.isHighSurrogate(buffer[offset])) {
                            codePoint = Character.toCodePoint(buffer[offset], buffer[offset + 1]);
                            ++offset;
                        } else {
                            codePoint = buffer[offset];
                        }
                        int w = WcwdithUtil.wcwidth(codePoint);
                        this.curVisCol += w > 0 ? w : 0;
                    }
                }
            }
            return bufferStartPos + offset;
        }
    }

    public static final class PosVisColFwdFinder
    extends AbstractFinder {
        int visCol;
        int tabSize;

        public int getVisCol() {
            return this.visCol;
        }

        public void setTabSize(int tabSize) {
            this.tabSize = tabSize;
        }

        @Override
        public void reset() {
            super.reset();
            this.visCol = 0;
        }

        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            int offset;
            for (offset = reqPos - bufferStartPos; offset < offset2; ++offset) {
                int codePoint;
                if (buffer[offset] == '\t') {
                    this.visCol = (this.visCol + this.tabSize) / this.tabSize * this.tabSize;
                    continue;
                }
                if (Character.isHighSurrogate(buffer[offset])) {
                    codePoint = Character.toCodePoint(buffer[offset], buffer[offset + 1]);
                    ++offset;
                } else {
                    codePoint = buffer[offset];
                }
                int w = WcwdithUtil.wcwidth(codePoint);
                this.visCol += w > 0 ? w : 0;
            }
            return bufferStartPos + offset;
        }
    }

    public static class FalseFinder
    extends AbstractFinder
    implements StringFinder {
        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            return -1;
        }

        @Override
        public int getFoundLength() {
            return 0;
        }
    }

    public static class TrueFinder
    extends AbstractFinder {
        @Override
        public int find(int bufferStartPos, char[] buffer, int offset1, int offset2, int reqPos, int limitPos) {
            this.found = true;
            return reqPos;
        }
    }

    public static abstract class AbstractFinder
    implements Finder {
        protected boolean found;

        @Override
        public final boolean isFound() {
            return this.found;
        }

        @Override
        public void reset() {
            this.found = false;
        }
    }

}

