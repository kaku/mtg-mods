/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;
import javax.swing.Timer;

public class WeakTimerListener
implements ActionListener {
    private WeakReference ref;
    private boolean stopTimer;

    public WeakTimerListener(ActionListener source) {
        this(source, true);
    }

    public WeakTimerListener(ActionListener source, boolean stopTimer) {
        this.ref = new WeakReference<ActionListener>(source);
        this.stopTimer = stopTimer;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        ActionListener src = (ActionListener)this.ref.get();
        if (src != null) {
            src.actionPerformed(evt);
        } else if (evt.getSource() instanceof Timer) {
            Timer timer = (Timer)evt.getSource();
            timer.removeActionListener(this);
            if (this.stopTimer) {
                timer.stop();
            }
        }
    }
}

