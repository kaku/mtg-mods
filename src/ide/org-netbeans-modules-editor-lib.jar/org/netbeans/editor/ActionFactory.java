/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.NavigationHistory
 *  org.netbeans.api.editor.NavigationHistory$Waypoint
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.progress.ProgressUtils
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.netbeans.modules.editor.indent.api.Reformat
 *  org.netbeans.modules.editor.lib2.RectangularSelectionUtils
 *  org.netbeans.modules.editor.lib2.view.DocumentView
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Toolbar
 */
package org.netbeans.editor;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.NavigationHistory;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.editor.Abbrev;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Finder;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.FixLineSyntaxState;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.JumpList;
import org.netbeans.editor.LocalBaseAction;
import org.netbeans.editor.LocaleSupport;
import org.netbeans.editor.MacroDialogSupport;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WordMatch;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.lib2.RectangularSelectionUtils;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.openide.util.ContextAwareAction;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.Presenter;

public class ActionFactory {
    private static final Logger LOG = Logger.getLogger(ActionFactory.class.getName());

    private ActionFactory() {
    }

    static void removeLineByLine(Document doc, int startPosition, int length) throws BadLocationException {
        String text = doc.getText(startPosition, length);
        BadLocationException ble = null;
        int notDeleted = 0;
        int deleted = 0;
        int line = 0;
        while ((line = text.indexOf(10, line + 1)) != -1) {
            try {
                doc.remove(startPosition + notDeleted, line + 1 - deleted - notDeleted);
                deleted = line + 1 - notDeleted;
            }
            catch (BadLocationException blee) {
                ble = blee;
                notDeleted = line + 1 - deleted;
            }
        }
        doc.remove(startPosition + notDeleted, length - deleted - notDeleted);
        if (ble != null) {
            throw ble;
        }
    }

    static void reformat(Reformat formatter, Document doc, int startPos, int endPos, AtomicBoolean canceled) throws BadLocationException {
        List<PositionRegion> regions = ActionFactory.collectRegions(doc, startPos, endPos);
        if (canceled.get()) {
            return;
        }
        for (PositionRegion region : regions) {
            formatter.reformat(region.getStartOffset(), region.getEndOffset());
        }
    }

    private static List<PositionRegion> collectRegions(Document doc, int startPos, int endPos) throws BadLocationException {
        GuardedDocument gdoc = doc instanceof GuardedDocument ? (GuardedDocument)doc : null;
        int pos = startPos;
        if (gdoc != null) {
            pos = gdoc.getGuardedBlockChain().adjustToBlockEnd(pos);
        }
        LinkedList<PositionRegion> regions = new LinkedList<PositionRegion>();
        while (pos < endPos) {
            int stopPos = endPos;
            if (gdoc != null && ((stopPos = gdoc.getGuardedBlockChain().adjustToNextBlockStart(pos) - 1) < 0 || stopPos > endPos)) {
                stopPos = endPos;
            }
            if (pos < stopPos) {
                regions.addFirst(new PositionRegion(doc, pos, stopPos));
                pos = stopPos;
            } else {
                ++pos;
            }
            if (gdoc == null) continue;
            pos = gdoc.getGuardedBlockChain().adjustToBlockEnd(pos);
        }
        return regions;
    }

    public static class CutToLineBeginOrEndAction
    extends LocalBaseAction {
        public CutToLineBeginOrEndAction() {
            super(14);
        }

        @Override
        public void actionPerformed(final ActionEvent evt, final JTextComponent target) {
            if (!target.isEditable() || !target.isEnabled()) {
                target.getToolkit().beep();
                return;
            }
            final BaseDocument doc = (BaseDocument)target.getDocument();
            doc.runAtomicAsUser(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                    try {
                        Action cutAction;
                        ActionMap actionMap = target.getActionMap();
                        if (actionMap != null && (cutAction = actionMap.get("cut-to-clipboard")) != null) {
                            int boundOffset;
                            Caret caret = target.getCaret();
                            int caretOffset = caret.getDot();
                            boolean toLineEnd = "cut-to-line-end".equals(CutToLineBeginOrEndAction.this.getValue("Name"));
                            int n = boundOffset = toLineEnd ? Utilities.getRowEnd(target, caretOffset) : Utilities.getRowStart(target, caretOffset);
                            if (toLineEnd) {
                                String text = target.getText(caretOffset, boundOffset - caretOffset);
                                if (boundOffset < doc.getLength() && text != null && text.matches("^[\\s]*$")) {
                                    ++boundOffset;
                                }
                            }
                            caret.moveDot(boundOffset);
                            cutAction.actionPerformed(evt);
                        }
                    }
                    catch (BadLocationException ex) {
                        ex.printStackTrace();
                    }
                    finally {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                    }
                }
            });
        }

    }

    public static class StartNewLine
    extends LocalBaseAction {
        public StartNewLine() {
            super(14);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (!target.isEditable() || !target.isEnabled()) {
                target.getToolkit().beep();
                return;
            }
            final BaseDocument doc = (BaseDocument)target.getDocument();
            final Indent indenter = Indent.get((Document)doc);
            indenter.lock();
            doc.runAtomicAsUser(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    try {
                        Caret caret = target.getCaret();
                        int eolDot = Utilities.getRowEnd(target, caret.getDot());
                        doc.insertString(eolDot, "\n", null);
                        Position newDotPos = doc.createPosition(eolDot + 1);
                        indenter.reindent(eolDot + 1);
                        caret.setDot(newDotPos.getOffset());
                    }
                    catch (BadLocationException ex) {
                        ex.printStackTrace();
                    }
                    finally {
                        indenter.unlock();
                    }
                }
            });
        }

    }

    public static class DumpViewHierarchyAction
    extends LocalBaseAction {
        public DumpViewHierarchyAction() {
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            AbstractDocument adoc = (AbstractDocument)target.getDocument();
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            adoc.readLock();
            try {
                hierarchy.lock();
                try {
                    System.err.println("FOLD HIERARCHY DUMP:\n" + (Object)hierarchy);
                    TokenHierarchy th = TokenHierarchy.get((Document)adoc);
                    System.err.println("TOKEN HIERARCHY DUMP:\n" + (Object)(th != null ? th : "<NULL-TH>"));
                }
                finally {
                    hierarchy.unlock();
                }
            }
            finally {
                adoc.readUnlock();
            }
            View rootView = null;
            TextUI textUI = target.getUI();
            if (textUI != null && (rootView = textUI.getRootView(target)) != null && rootView.getViewCount() == 1) {
                rootView = rootView.getView(0);
            }
            if (rootView != null) {
                String rootViewDump = rootView instanceof DocumentView ? ((DocumentView)rootView).toStringDetail() : rootView.toString();
                System.err.println("DOCUMENT VIEW: " + System.identityHashCode(rootView) + "\n" + rootViewDump);
                int caretOffset = target.getCaretPosition();
                int caretViewIndex = rootView.getViewIndex(caretOffset, Position.Bias.Forward);
                System.err.println("caretOffset=" + caretOffset + ", caretViewIndex=" + caretViewIndex);
                if (caretViewIndex >= 0 && caretViewIndex < rootView.getViewCount()) {
                    View caretView = rootView.getView(caretViewIndex);
                    System.err.println("caretView: " + caretView);
                }
                System.err.println(FixLineSyntaxState.lineInfosToString(adoc));
            }
            if (adoc instanceof BaseDocument) {
                System.err.println("DOCUMENT:\n" + ((BaseDocument)adoc).toStringDetail());
            }
        }
    }

    @Deprecated
    public static class ExpandAllFolds
    extends DeprecatedFoldAction {
        public ExpandAllFolds() {
            super("expand-all-folds");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            super.actionPerformed(evt, target);
        }
    }

    @Deprecated
    public static class CollapseAllFolds
    extends DeprecatedFoldAction {
        public CollapseAllFolds() {
            super("collapse-all-folds");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            super.actionPerformed(evt, target);
        }
    }

    public static class ExpandFold
    extends DeprecatedFoldAction {
        public ExpandFold() {
            super("expand-fold");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            super.actionPerformed(evt, target);
        }
    }

    @Deprecated
    public static class CollapseFold
    extends DeprecatedFoldAction {
        public CollapseFold() {
            super("collapse-fold");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            super.actionPerformed(evt, target);
        }
    }

    static abstract class DeprecatedFoldAction
    extends LocalBaseAction {
        private String delegateId;

        DeprecatedFoldAction(String id) {
            this.delegateId = id;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            BaseKit kit;
            BaseKit baseKit = kit = target == null ? BaseKit.getKit(BaseKit.class) : Utilities.getKit(target);
            if (kit != null) {
                Action a = kit.getActionByName(this.delegateId);
                if (a instanceof BaseAction && a != this) {
                    ((BaseAction)a).actionPerformed(evt, target);
                    return;
                }
                a.actionPerformed(evt);
                return;
            }
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public static class AnnotationsCyclingAction
    extends LocalBaseAction {
        public AnnotationsCyclingAction() {
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                try {
                    Caret caret = target.getCaret();
                    BaseDocument doc = Utilities.getDocument(target);
                    int caretLine = Utilities.getLineOffset(doc, caret.getDot());
                    AnnotationDesc aDesc = doc.getAnnotations().activateNextAnnotation(caretLine);
                }
                catch (BadLocationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class ToggleLineNumbersAction
    extends LocalBaseAction {
        static final long serialVersionUID = -3502499718130556526L;
        private JCheckBoxMenuItem item = null;

        public ToggleLineNumbersAction() {
            super("toggle-line-numbers");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            this.toggleLineNumbers();
        }

        @Override
        public JMenuItem getPopupMenuItem(JTextComponent target) {
            this.item = new JCheckBoxMenuItem(NbBundle.getBundle(BaseKit.class).getString("line-numbers-menuitem"), this.isLineNumbersVisible());
            this.item.addItemListener(new ItemListener(){

                @Override
                public void itemStateChanged(ItemEvent e) {
                    ToggleLineNumbersAction.this.actionPerformed(null, null);
                }
            });
            return this.item;
        }

        protected boolean isLineNumbersVisible() {
            return false;
        }

        protected void toggleLineNumbers() {
        }

    }

    public static class GenerateGutterPopupAction
    extends LocalBaseAction {
        static final long serialVersionUID = -3502499718130556525L;

        public GenerateGutterPopupAction() {
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }

        @Override
        public JMenuItem getPopupMenuItem(JTextComponent target) {
            EditorUI ui = Utilities.getEditorUI(target);
            try {
                return ui.getDocument().getAnnotations().createMenu(Utilities.getKit(target), Utilities.getLineOffset(ui.getDocument(), target.getCaret().getDot()));
            }
            catch (BadLocationException ex) {
                return null;
            }
        }
    }

    public static class InsertDateTimeAction
    extends LocalBaseAction {
        static final long serialVersionUID = 2865619897402L;

        public InsertDateTimeAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                try {
                    Caret caret = target.getCaret();
                    BaseDocument doc = (BaseDocument)target.getDocument();
                    SimpleDateFormat formatter = new SimpleDateFormat();
                    Date currentTime = new Date();
                    String dateString = formatter.format(currentTime);
                    doc.insertString(caret.getDot(), dateString, null);
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class ScrollDownAction
    extends LocalBaseAction {
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                EditorUI editorUI = Utilities.getEditorUI(target);
                Rectangle bounds = editorUI.getExtentBounds();
                bounds.y -= editorUI.getLineHeight();
                bounds.x += editorUI.getTextMargin().left;
                editorUI.scrollRectToVisible(bounds, 2);
            }
        }
    }

    public static class ScrollUpAction
    extends LocalBaseAction {
        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                EditorUI editorUI = Utilities.getEditorUI(target);
                Rectangle bounds = editorUI.getExtentBounds();
                bounds.y += editorUI.getLineHeight();
                bounds.x += editorUI.getTextMargin().left;
                editorUI.scrollRectToVisible(bounds, 2);
            }
        }
    }

    public static class JumpListPrevComponentAction
    extends LocalBaseAction {
        static final long serialVersionUID = 2032230534727849525L;

        public JumpListPrevComponentAction() {
            super("jump-list-prev-component");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                JumpList.jumpPrevComponent(target);
            }
        }
    }

    public static class JumpListNextComponentAction
    extends LocalBaseAction {
        static final long serialVersionUID = -2059070050865876892L;

        public JumpListNextComponentAction() {
            super("jump-list-next-component");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                JumpList.jumpNextComponent(target);
            }
        }
    }

    public static class JumpListPrevAction
    extends LocalBaseAction {
        static final long serialVersionUID = 7174907031986424265L;
        PropertyChangeListener pcl;

        public JumpListPrevAction() {
            super("jump-list-prev");
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/edit_previous.png");
            this.pcl = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    JumpListPrevAction.this.setEnabled(JumpList.hasPrev());
                }
            };
            JumpList.addPropertyChangeListener(this.pcl);
            this.setEnabled(JumpList.hasPrev());
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                JumpList.jumpPrev(target);
            }
        }

    }

    public static class JumpListNextAction
    extends LocalBaseAction {
        static final long serialVersionUID = 6891721278404990446L;
        PropertyChangeListener pcl;

        public JumpListNextAction() {
            super("jump-list-next");
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/edit_next.png");
            this.pcl = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    JumpListNextAction.this.setEnabled(JumpList.hasNext());
                }
            };
            JumpList.addPropertyChangeListener(this.pcl);
            this.setEnabled(JumpList.hasNext());
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                JumpList.jumpNext(target);
            }
        }

    }

    public static class SelectNextParameterAction
    extends LocalBaseAction {
        static final long serialVersionUID = 8045372985336370934L;

        public SelectNextParameterAction() {
            super("select-next-parameter", 34);
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                BaseDocument doc = (BaseDocument)target.getDocument();
                int dotPos = caret.getDot();
                int selectStartPos = -1;
                try {
                    int selectEndPos;
                    if (dotPos > 0 && doc.getChars(dotPos - 1, 1)[0] == ',') {
                        selectStartPos = dotPos;
                    }
                    if (dotPos < doc.getLength()) {
                        char dotChar = doc.getChars(dotPos, 1)[0];
                        if (dotChar == ',') {
                            selectStartPos = dotPos + 1;
                        } else if (dotChar == ')') {
                            caret.setDot(dotPos + 1);
                        }
                    }
                    if (selectStartPos >= 0 && (selectEndPos = doc.find(new FinderFactory.CharArrayFwdFinder(new char[]{',', ')'}), selectStartPos, -1)) >= 0) {
                        target.select(selectStartPos, selectEndPos);
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class SelectIdentifierAction
    extends LocalBaseAction {
        static final long serialVersionUID = -7288216961333147873L;

        public SelectIdentifierAction() {
            super(2);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    if (Utilities.isSelectionShowing(caret)) {
                        caret.setDot(caret.getDot());
                    } else {
                        int[] block = Utilities.getIdentifierBlock((BaseDocument)target.getDocument(), caret.getDot());
                        if (block != null) {
                            caret.setDot(block[0]);
                            caret.moveDot(block[1]);
                        }
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class LastNonWhiteAction
    extends LocalBaseAction {
        static final long serialVersionUID = 4503533041729712917L;

        public LastNonWhiteAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    int pos = Utilities.getRowLastNonWhite((BaseDocument)target.getDocument(), caret.getDot());
                    if (pos >= 0) {
                        boolean select = "selection-last-non-white".equals(this.getValue("Name"));
                        if (select) {
                            caret.moveDot(pos);
                        } else {
                            caret.setDot(pos);
                        }
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class FirstNonWhiteAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5888439539790901158L;

        public FirstNonWhiteAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Caret caret = target.getCaret();
                try {
                    int pos = Utilities.getRowFirstNonWhite((BaseDocument)target.getDocument(), caret.getDot());
                    if (pos >= 0) {
                        boolean select = "selection-first-non-white".equals(this.getValue("Name"));
                        if (select) {
                            caret.moveDot(pos);
                        } else {
                            caret.setDot(pos);
                        }
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class FormatAction
    extends LocalBaseAction {
        static final long serialVersionUID = -7666172828961171865L;
        private boolean indentOnly;

        public FormatAction() {
            super(14);
        }

        @Override
        protected void actionNameUpdate(String actionName) {
            super.actionNameUpdate(actionName);
            this.indentOnly = "indent".equals(actionName);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = Utilities.getDocument(target);
                if (doc == null) {
                    return;
                }
                Cursor origCursor = target.getCursor();
                target.setCursor(Cursor.getPredefinedCursor(3));
                try {
                    final AtomicBoolean canceled = new AtomicBoolean();
                    ProgressUtils.runOffEventDispatchThread((Runnable)new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            Indent indenter;
                            if (canceled.get()) {
                                return;
                            }
                            final Reformat formatter = FormatAction.this.indentOnly ? null : Reformat.get((Document)doc);
                            Indent indent = indenter = FormatAction.this.indentOnly ? Indent.get((Document)doc) : null;
                            if (FormatAction.this.indentOnly) {
                                indenter.lock();
                            } else {
                                formatter.lock();
                            }
                            try {
                                if (canceled.get()) {
                                    return;
                                }
                                doc.runAtomicAsUser(new Runnable(){

                                    @Override
                                    public void run() {
                                        try {
                                            int endPos;
                                            int startPos;
                                            if (Utilities.isSelectionShowing(caret)) {
                                                startPos = target.getSelectionStart();
                                                endPos = target.getSelectionEnd();
                                            } else {
                                                startPos = 0;
                                                endPos = doc.getLength();
                                            }
                                            List regions = ActionFactory.collectRegions(doc, startPos, endPos);
                                            if (canceled.get()) {
                                                return;
                                            }
                                            for (PositionRegion region : regions) {
                                                if (FormatAction.this.indentOnly) {
                                                    indenter.reindent(region.getStartOffset(), region.getEndOffset());
                                                    continue;
                                                }
                                                formatter.reformat(region.getStartOffset(), region.getEndOffset());
                                            }
                                        }
                                        catch (GuardedException e) {
                                            target.getToolkit().beep();
                                        }
                                        catch (BadLocationException e) {
                                            Utilities.annotateLoggable(e);
                                        }
                                    }
                                });
                            }
                            finally {
                                if (FormatAction.this.indentOnly) {
                                    indenter.unlock();
                                } else {
                                    formatter.unlock();
                                }
                            }
                        }

                    }, (String)NbBundle.getMessage(FormatAction.class, (String)(this.indentOnly ? "Indent_in_progress" : "Format_in_progress")), (AtomicBoolean)canceled, (boolean)false);
                }
                catch (Exception e) {
                    Logger.getLogger(FormatAction.class.getName()).log(Level.FINE, null, e);
                }
                finally {
                    target.setCursor(origCursor);
                }
            }
        }

    }

    public static class AdjustCaretAction
    extends LocalBaseAction {
        int percentFromWindowTop;
        static final long serialVersionUID = 3223383913531191066L;

        public static AdjustCaretAction createAdjustTop() {
            return new AdjustCaretAction(0);
        }

        public static AdjustCaretAction createAdjustCenter() {
            return new AdjustCaretAction(50);
        }

        public static AdjustCaretAction createAdjustBottom() {
            return new AdjustCaretAction(100);
        }

        public AdjustCaretAction(int percentFromWindowTop) {
            this.percentFromWindowTop = percentFromWindowTop;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Utilities.getEditorUI(target).adjustCaret(this.percentFromWindowTop);
            }
        }
    }

    public static class AdjustWindowAction
    extends LocalBaseAction {
        int percentFromWindowTop;
        static final long serialVersionUID = 8864278998999643292L;

        public static AdjustWindowAction createAdjustTop() {
            return new AdjustWindowAction(0);
        }

        public static AdjustWindowAction createAdjustCenter() {
            return new AdjustWindowAction(50);
        }

        public static AdjustWindowAction createAdjustBottom() {
            return new AdjustWindowAction(100);
        }

        public AdjustWindowAction(int percentFromWindowTop) {
            this.percentFromWindowTop = percentFromWindowTop;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                Utilities.getEditorUI(target).adjustWindow(this.percentFromWindowTop);
            }
        }
    }

    public static class ReindentLineAction
    extends LocalBaseAction {
        private boolean reindent;
        static final long serialVersionUID = 1;

        public ReindentLineAction() {
            super(14);
        }

        @Override
        protected void actionNameUpdate(String actionName) {
            super.actionNameUpdate(actionName);
            this.reindent = "reindent-line".equals(actionName);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                Reformat reformat;
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                final GuardedDocument gdoc = doc instanceof GuardedDocument ? (GuardedDocument)doc : null;
                final Indent indenter = this.reindent ? Indent.get((Document)doc) : null;
                Reformat reformat2 = reformat = this.reindent ? null : Reformat.get((Document)doc);
                if (this.reindent) {
                    indenter.lock();
                } else {
                    reformat.lock();
                }
                try {
                    doc.runAtomicAsUser(new Runnable(){

                        @Override
                        public void run() {
                            try {
                                int startPos;
                                Position endPosition;
                                if (Utilities.isSelectionShowing(caret)) {
                                    startPos = target.getSelectionStart();
                                    endPosition = doc.createPosition(target.getSelectionEnd());
                                } else {
                                    startPos = Utilities.getRowStart(doc, caret.getDot());
                                    endPosition = doc.createPosition(Utilities.getRowEnd(doc, caret.getDot()));
                                }
                                int pos = startPos;
                                if (gdoc != null) {
                                    pos = gdoc.getGuardedBlockChain().adjustToBlockEnd(pos);
                                }
                                while (pos <= endPosition.getOffset()) {
                                    int stopPos = endPosition.getOffset();
                                    if (gdoc != null && ((stopPos = gdoc.getGuardedBlockChain().adjustToNextBlockStart(pos)) == -1 || stopPos > endPosition.getOffset())) {
                                        stopPos = endPosition.getOffset();
                                    }
                                    Position stopPosition = doc.createPosition(stopPos);
                                    if (ReindentLineAction.this.reindent) {
                                        indenter.reindent(pos, stopPos);
                                    } else {
                                        reformat.reformat(pos, stopPos);
                                    }
                                    pos = stopPosition.getOffset() + 1;
                                    if (gdoc == null) continue;
                                    pos = gdoc.getGuardedBlockChain().adjustToBlockEnd(pos);
                                }
                            }
                            catch (GuardedException e) {
                                target.getToolkit().beep();
                            }
                            catch (BadLocationException e) {
                                Utilities.annotateLoggable(e);
                            }
                        }
                    });
                }
                finally {
                    if (this.reindent) {
                        indenter.unlock();
                    } else {
                        reformat.unlock();
                    }
                }
            }
        }

    }

    public static class ShiftLineAction
    extends LocalBaseAction {
        static final long serialVersionUID = -5124732597493699582L;

        public ShiftLineAction() {
            super(10);
        }

        @Override
        protected void actionNameUpdate(String actionName) {
            super.actionNameUpdate(actionName);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = Utilities.getDocument(target);
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        block8 : {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                boolean right = "shift-line-right".equals(ShiftLineAction.this.getValue("Name"));
                                if (Utilities.isSelectionShowing(caret)) {
                                    BaseKit.shiftBlock(doc, target.getSelectionStart(), target.getSelectionEnd(), right);
                                    break block8;
                                }
                                BaseKit.shiftLine(doc, caret.getDot(), right);
                            }
                            catch (GuardedException e) {
                                target.getToolkit().beep();
                            }
                            catch (BadLocationException e) {
                                e.printStackTrace();
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    }
                });
            }
        }

    }

    public static class WordMatchAction
    extends LocalBaseAction {
        private boolean matchNext;
        static final long serialVersionUID = 595571114685133170L;

        public WordMatchAction() {
            super(14);
        }

        @Override
        protected void actionNameUpdate(String actionName) {
            super.actionNameUpdate(actionName);
            this.matchNext = "word-match-next".equals(actionName);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                EditorUI editorUI = Utilities.getEditorUI(target);
                Caret caret = target.getCaret();
                final BaseDocument doc = Utilities.getDocument(target);
                if (Utilities.isSelectionShowing(caret)) {
                    target.replaceSelection(null);
                }
                final int caretOffset = caret.getDot();
                final String s = editorUI.getWordMatch().getMatchWord(caretOffset, this.matchNext);
                final String prevWord = editorUI.getWordMatch().getPreviousWord();
                if (s != null) {
                    doc.runAtomicAsUser(new Runnable(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        @Override
                        public void run() {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                boolean removePrevWord;
                                int offset = caretOffset;
                                boolean bl = removePrevWord = prevWord != null && prevWord.length() > 0;
                                if (removePrevWord) {
                                    offset -= prevWord.length();
                                }
                                Position pos = doc.createPosition(offset);
                                doc.remove(offset, prevWord.length());
                                doc.insertString(pos.getOffset(), s, null);
                            }
                            catch (BadLocationException e) {
                                target.getToolkit().beep();
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    });
                }
            }
        }

    }

    public static class RedoAction
    extends LocalBaseAction {
        static final long serialVersionUID = 6048125996333769202L;

        public RedoAction() {
            super("redo", 30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (!target.isEditable() || !target.isEnabled()) {
                target.getToolkit().beep();
                return;
            }
            Document doc = target.getDocument();
            UndoableEdit undoMgr = (UndoableEdit)doc.getProperty("undo-manager");
            if (undoMgr == null) {
                undoMgr = (UndoableEdit)doc.getProperty(UndoManager.class);
            }
            if (target != null && undoMgr != null) {
                try {
                    undoMgr.redo();
                }
                catch (CannotRedoException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class UndoAction
    extends LocalBaseAction {
        static final long serialVersionUID = 8628586205035497612L;

        public UndoAction() {
            super("undo", 30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (!target.isEditable() || !target.isEnabled()) {
                target.getToolkit().beep();
                return;
            }
            Document doc = target.getDocument();
            UndoableEdit undoMgr = (UndoableEdit)doc.getProperty("undo-manager");
            if (undoMgr == null) {
                undoMgr = (UndoableEdit)doc.getProperty(UndoManager.class);
            }
            if (target != null && undoMgr != null) {
                try {
                    undoMgr.undo();
                }
                catch (CannotUndoException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class ToggleRectangularSelectionAction
    extends LocalBaseAction
    implements Presenter.Toolbar,
    ContextAwareAction,
    PropertyChangeListener,
    DocumentListener {
        static final long serialVersionUID = 0;
        private JEditorPane pane;
        private JToggleButton toggleButton;

        public ToggleRectangularSelectionAction() {
            super("toggle-rectangular-selection");
            this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"org/netbeans/modules/editor/resources/rect_select_16x16.png", (boolean)false));
            this.putValue("noIconInMenu", Boolean.TRUE);
        }

        void setPane(JEditorPane pane) {
            assert (pane != null);
            this.pane = pane;
            pane.addPropertyChangeListener(this);
            pane.getDocument().addDocumentListener(this);
            this.updateState();
        }

        void updateState() {
            if (this.pane != null) {
                boolean rectangleSelection = RectangularSelectionUtils.isRectangularSelection((JComponent)this.pane);
                if (this.toggleButton != null) {
                    this.toggleButton.setSelected(rectangleSelection);
                    this.toggleButton.setContentAreaFilled(rectangleSelection);
                    this.toggleButton.setBorderPainted(rectangleSelection);
                }
            }
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null && !Boolean.TRUE.equals(target.getClientProperty("AsTextField"))) {
                boolean newRectSel = !RectangularSelectionUtils.isRectangularSelection((JComponent)target);
                RectangularSelectionUtils.setRectangularSelection((JComponent)target, (boolean)newRectSel);
            }
        }

        public Component getToolbarPresenter() {
            this.toggleButton = new JToggleButton();
            this.toggleButton.putClientProperty("hideActionText", Boolean.TRUE);
            this.toggleButton.setIcon((Icon)this.getValue("SmallIcon"));
            this.toggleButton.setAction(this);
            return this.toggleButton;
        }

        public Action createContextAwareInstance(Lookup actionContext) {
            JEditorPane pane = (JEditorPane)actionContext.lookup(JEditorPane.class);
            if (pane != null) {
                ToggleRectangularSelectionAction action = new ToggleRectangularSelectionAction();
                action.setPane(pane);
                return action;
            }
            return this;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (this.pane == evt.getSource() && RectangularSelectionUtils.getRectangularSelectionProperty().equals(evt.getPropertyName())) {
                this.updateState();
            }
        }

        private void documentUpdate(DocumentEvent e) {
            if (RectangularSelectionUtils.isRectangularSelection((JComponent)this.pane) && !Boolean.TRUE.equals(e.getDocument().getProperty("rectangular-document-change-allowed"))) {
                RectangularSelectionUtils.resetRectangularSelection((JTextComponent)this.pane);
                e.getDocument().putProperty("rectangular-document-change-allowed", Boolean.FALSE);
            }
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            this.documentUpdate(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            this.documentUpdate(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            this.documentUpdate(e);
        }
    }

    public static class ChangeCaseAction
    extends LocalBaseAction {
        int changeCaseMode;
        static final long serialVersionUID = 5680212865619897402L;

        public static ChangeCaseAction createToUpperCase() {
            return new ChangeCaseAction(0);
        }

        public static ChangeCaseAction createToLowerCase() {
            return new ChangeCaseAction(1);
        }

        public static ChangeCaseAction createSwitchCase() {
            return new ChangeCaseAction(2);
        }

        private ChangeCaseAction(int changeCaseMode) {
            super(30);
            this.changeCaseMode = changeCaseMode;
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                try {
                    Caret caret = target.getCaret();
                    BaseDocument doc = (BaseDocument)target.getDocument();
                    int dotPos = caret.getDot();
                    if (RectangularSelectionUtils.isRectangularSelection((JComponent)target)) {
                        List positions = RectangularSelectionUtils.regionsCopy((JComponent)target);
                        for (int i = 0; i < positions.size(); i += 2) {
                            int b;
                            int a = ((Position)positions.get(i)).getOffset();
                            if (a == (b = ((Position)positions.get(i + 1)).getOffset())) continue;
                            Utilities.changeCase(doc, a, b - a, this.changeCaseMode);
                        }
                    } else if (Utilities.isSelectionShowing(caret)) {
                        int startPos = target.getSelectionStart();
                        int endPos = target.getSelectionEnd();
                        Utilities.changeCase(doc, startPos, endPos - startPos, this.changeCaseMode);
                        caret.setDot(dotPos == startPos ? endPos : startPos);
                        caret.moveDot(dotPos == startPos ? startPos : endPos);
                    } else {
                        Utilities.changeCase(doc, dotPos, 1, this.changeCaseMode);
                        caret.setDot(dotPos + 1);
                    }
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class AbbrevResetAction
    extends LocalBaseAction {
        static final long serialVersionUID = -2807497346060448395L;

        public AbbrevResetAction() {
            super("abbrev-reset", 4);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
        }
    }

    public static class AbbrevExpandAction
    extends LocalBaseAction {
        static final long serialVersionUID = -2124569510083544403L;

        public AbbrevExpandAction() {
            super("abbrev-expand", 26);
            this.putValue("no-keybinding", Boolean.TRUE);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                EditorUI editorUI = ((BaseTextUI)target.getUI()).getEditorUI();
                try {
                    editorUI.getAbbrev().checkAndExpand(evt);
                }
                catch (BadLocationException e) {
                    target.getToolkit().beep();
                }
            }
        }
    }

    public static class StopMacroRecordingAction
    extends LocalBaseAction {
        static final long serialVersionUID = 1;

        public StopMacroRecordingAction() {
            super("stop-macro-recording", 64);
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/stop_macro_recording.png");
        }

        protected MacroDialogSupport getMacroDialogSupport(Class kitClass) {
            return new MacroDialogSupport(kitClass);
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                String macro = this.stopRecording(target);
                if (macro == null) {
                    target.getToolkit().beep();
                } else {
                    BaseKit kit = Utilities.getKit(target);
                    MacroDialogSupport support = this.getMacroDialogSupport(kit.getClass());
                    support.setBody(macro);
                    support.showMacroDialog();
                }
            }
        }
    }

    public static class StartMacroRecordingAction
    extends LocalBaseAction {
        static final long serialVersionUID = 1;

        public StartMacroRecordingAction() {
            super("start-macro-recording", 64);
            this.putValue("IconResource", "org/netbeans/modules/editor/resources/start_macro_recording.png");
        }

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null && !this.startRecording(target)) {
                target.getToolkit().beep();
            }
        }
    }

    public static class RunMacroAction
    extends BaseAction {
        static final long serialVersionUID = 1;
        static HashSet runningActions = new HashSet();
        private String macroName;

        public RunMacroAction(String name) {
            super("macro-" + name);
            this.macroName = name;
        }

        protected void error(JTextComponent target, String messageKey) {
            Utilities.setStatusText(target, LocaleSupport.getString(messageKey, "Error in macro: " + messageKey));
            Toolkit.getDefaultToolkit().beep();
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (!runningActions.add(this.macroName)) {
                this.error(target, "loop");
                return;
            }
            if (target == null) {
                return;
            }
            final BaseKit kit = Utilities.getKit(target);
            if (kit == null) {
                return;
            }
            String commandString = null;
            if (commandString == null) {
                this.error(target, "macro-not-found");
                runningActions.remove(this.macroName);
                return;
            }
            final StringBuffer actionName = new StringBuffer();
            final char[] command = commandString.toCharArray();
            final int len = command.length;
            BaseDocument doc = (BaseDocument)target.getDocument();
            doc.runAtomicAsUser(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    try {
                        for (int i = 0; i < len; ++i) {
                            char ch;
                            if (Character.isWhitespace(command[i])) continue;
                            if (command[i] == '\"') {
                                while (++i < len && command[i] != '\"') {
                                    Action a;
                                    ch = command[i];
                                    if (ch == '\\') {
                                        if (++i >= len) {
                                            RunMacroAction.this.error(target, "macro-malformed");
                                            return;
                                        }
                                        ch = command[i];
                                        if (ch != '\"' && ch != '\\') {
                                            RunMacroAction.this.error(target, "macro-malformed");
                                            return;
                                        }
                                    }
                                    if ((a = target.getKeymap().getDefaultAction()) == null) continue;
                                    ActionEvent newEvt = new ActionEvent(target, 0, new String(new char[]{ch}));
                                    if (a instanceof BaseAction) {
                                        ((BaseAction)a).updateComponent(target);
                                        ((BaseAction)a).actionPerformed(newEvt, target);
                                        continue;
                                    }
                                    a.actionPerformed(newEvt);
                                }
                                continue;
                            }
                            actionName.setLength(0);
                            while (i < len && !Character.isWhitespace(command[i])) {
                                if ((ch = command[i++]) == '\\') {
                                    if (i >= len) {
                                        RunMacroAction.this.error(target, "macro-malformed");
                                        return;
                                    }
                                    if ((ch = command[i++]) != '\\' && !Character.isWhitespace(ch)) {
                                        RunMacroAction.this.error(target, "macro-malformed");
                                        return;
                                    }
                                }
                                actionName.append(ch);
                            }
                            Action a = kit.getActionByName(actionName.toString());
                            if (a != null) {
                                ActionEvent fakeEvt = new ActionEvent(target, 0, "");
                                if (a instanceof BaseAction) {
                                    ((BaseAction)a).updateComponent(target);
                                    ((BaseAction)a).actionPerformed(fakeEvt, target);
                                } else {
                                    a.actionPerformed(fakeEvt);
                                }
                                if (!"insert-break".equals(actionName.toString())) continue;
                                Action def = target.getKeymap().getDefaultAction();
                                ActionEvent fakeEvt10 = new ActionEvent(target, 0, new String(new byte[]{10}));
                                if (def instanceof BaseAction) {
                                    ((BaseAction)def).updateComponent(target);
                                    ((BaseAction)def).actionPerformed(fakeEvt10, target);
                                    continue;
                                }
                                def.actionPerformed(fakeEvt10);
                                continue;
                            }
                            RunMacroAction.this.error(target, "macro-unknown-action");
                            return;
                        }
                    }
                    finally {
                        RunMacroAction.runningActions.remove(RunMacroAction.this.macroName);
                    }
                }
            });
        }

    }

    public static class ToggleTypingModeAction
    extends LocalBaseAction {
        static final long serialVersionUID = -2431132686507799723L;

        @Override
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            if (target != null) {
                EditorUI editorUI = Utilities.getEditorUI(target);
                Boolean overwriteMode = (Boolean)editorUI.getProperty("overwriteMode");
                overwriteMode = overwriteMode == null || overwriteMode == false ? Boolean.TRUE : Boolean.FALSE;
                editorUI.putProperty("overwriteMode", overwriteMode);
            }
        }
    }

    public static class RemoveSelectionAction
    extends LocalBaseAction {
        static final long serialVersionUID = -1419424594746686573L;

        public RemoveSelectionAction() {
            super("remove-selection", 30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            target.replaceSelection(null);
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class CopySelectionElseLineDownAction
    extends LocalBaseAction {
        static final long serialVersionUID = 1;

        public CopySelectionElseLineDownAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled() || Boolean.TRUE.equals(target.getClientProperty("AsTextField"))) {
                    target.getToolkit().beep();
                    return;
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int start;
                            Element rootElement = doc.getDefaultRootElement();
                            Caret caret = target.getCaret();
                            boolean selection = false;
                            boolean backwardSelection = false;
                            int end = start = target.getCaretPosition();
                            if (Utilities.isSelectionShowing(caret)) {
                                int selStart = caret.getDot();
                                int selEnd = caret.getMark();
                                start = Math.min(selStart, selEnd);
                                end = Math.max(selStart, selEnd) - 1;
                                selection = true;
                                backwardSelection = selStart >= selEnd;
                            }
                            int zeroBaseStartLineNumber = rootElement.getElementIndex(start);
                            int zeroBaseEndLineNumber = rootElement.getElementIndex(end);
                            if (zeroBaseEndLineNumber == -1) {
                                target.getToolkit().beep();
                                return;
                            }
                            try {
                                Element startLineElement = rootElement.getElement(zeroBaseStartLineNumber);
                                int startLineStartOffset = startLineElement.getStartOffset();
                                Element endLineElement = rootElement.getElement(zeroBaseEndLineNumber);
                                int endLineEndOffset = endLineElement.getEndOffset();
                                String linesText = doc.getText(startLineStartOffset, endLineEndOffset - startLineStartOffset);
                                int column = start - startLineStartOffset;
                                try {
                                    if (endLineEndOffset == doc.getLength() + 1) {
                                        NavigationHistory.getEdits().markWaypoint(target, endLineEndOffset - 1, false, true);
                                    } else {
                                        NavigationHistory.getEdits().markWaypoint(target, endLineEndOffset, false, true);
                                    }
                                }
                                catch (BadLocationException e) {
                                    LOG.log(Level.WARNING, "Can't add position to the history of edits.", e);
                                }
                                if (endLineEndOffset == doc.getLength() + 1) {
                                    assert (linesText.charAt(linesText.length() - 1) == '\n');
                                    doc.insertString(endLineEndOffset - 1, "\n" + linesText.substring(0, linesText.length() - 1), null);
                                } else {
                                    doc.insertString(endLineEndOffset, linesText, null);
                                }
                                if (selection) {
                                    if (backwardSelection) {
                                        caret.setDot(endLineEndOffset + column);
                                        caret.moveDot(endLineEndOffset + (endLineEndOffset - startLineStartOffset) - (endLineEndOffset - end - 1));
                                    } else {
                                        caret.setDot(endLineEndOffset + (endLineEndOffset - startLineStartOffset) - (endLineEndOffset - end - 1));
                                        caret.moveDot(endLineEndOffset + column);
                                    }
                                } else {
                                    target.setCaretPosition(Math.min(doc.getLength() - 1, endLineEndOffset + column));
                                }
                            }
                            catch (BadLocationException ex) {
                                target.getToolkit().beep();
                            }
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class CopySelectionElseLineUpAction
    extends LocalBaseAction {
        static final long serialVersionUID = 1;

        public CopySelectionElseLineUpAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int start;
                            Element rootElement = doc.getDefaultRootElement();
                            Caret caret = target.getCaret();
                            boolean selection = false;
                            boolean backwardSelection = false;
                            int end = start = target.getCaretPosition();
                            if (Utilities.isSelectionShowing(caret)) {
                                int selStart = caret.getDot();
                                int selEnd = caret.getMark();
                                start = Math.min(selStart, selEnd);
                                end = Math.max(selStart, selEnd) - 1;
                                selection = true;
                                backwardSelection = selStart >= selEnd;
                            }
                            int zeroBaseStartLineNumber = rootElement.getElementIndex(start);
                            int zeroBaseEndLineNumber = rootElement.getElementIndex(end);
                            if (zeroBaseStartLineNumber == -1) {
                                target.getToolkit().beep();
                                return;
                            }
                            try {
                                Element startLineElement = rootElement.getElement(zeroBaseStartLineNumber);
                                int startLineStartOffset = startLineElement.getStartOffset();
                                Element endLineElement = rootElement.getElement(zeroBaseEndLineNumber);
                                int endLineEndOffset = endLineElement.getEndOffset();
                                String linesText = doc.getText(startLineStartOffset, endLineEndOffset - startLineStartOffset);
                                int column = start - startLineStartOffset;
                                try {
                                    NavigationHistory.getEdits().markWaypoint(target, startLineStartOffset, false, true);
                                }
                                catch (BadLocationException e) {
                                    LOG.log(Level.WARNING, "Can't add position to the history of edits.", e);
                                }
                                doc.insertString(startLineStartOffset, linesText, null);
                                if (selection) {
                                    if (backwardSelection) {
                                        caret.setDot(startLineStartOffset + column);
                                        caret.moveDot(startLineStartOffset + (endLineEndOffset - startLineStartOffset) - (endLineEndOffset - end - 1));
                                    } else {
                                        caret.setDot(startLineStartOffset + (endLineEndOffset - startLineStartOffset) - (endLineEndOffset - end - 1));
                                        caret.moveDot(startLineStartOffset + column);
                                    }
                                } else {
                                    target.setCaretPosition(startLineStartOffset + column);
                                }
                            }
                            catch (BadLocationException ex) {
                                target.getToolkit().beep();
                            }
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class MoveSelectionElseLineDownAction
    extends LocalBaseAction {
        static final long serialVersionUID = 1;

        public MoveSelectionElseLineDownAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                if (doc instanceof GuardedDocument && ((GuardedDocument)doc).isPosGuarded(target.getCaretPosition())) {
                    target.getToolkit().beep();
                    return;
                }
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        block15 : {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                boolean selection;
                                int zeroBaseEndLineNumber;
                                int endLineEndOffset;
                                int end;
                                Element rootElement;
                                int start;
                                Caret caret;
                                int startLineStartOffset;
                                boolean backwardSelection;
                                block16 : {
                                    rootElement = doc.getDefaultRootElement();
                                    caret = target.getCaret();
                                    selection = false;
                                    backwardSelection = false;
                                    end = start = target.getCaretPosition();
                                    if (Utilities.isSelectionShowing(caret)) {
                                        int selStart = caret.getDot();
                                        int selEnd = caret.getMark();
                                        start = Math.min(selStart, selEnd);
                                        end = Math.max(selStart, selEnd) - 1;
                                        selection = true;
                                        backwardSelection = selStart >= selEnd;
                                    }
                                    int zeroBaseStartLineNumber = rootElement.getElementIndex(start);
                                    zeroBaseEndLineNumber = rootElement.getElementIndex(end);
                                    if (zeroBaseEndLineNumber == -1) {
                                        target.getToolkit().beep();
                                        break block15;
                                    }
                                    try {
                                        Element startLineElement = rootElement.getElement(zeroBaseStartLineNumber);
                                        startLineStartOffset = startLineElement.getStartOffset();
                                        Element endLineElement = rootElement.getElement(zeroBaseEndLineNumber);
                                        endLineEndOffset = endLineElement.getEndOffset();
                                        if (endLineEndOffset <= doc.getLength()) break block16;
                                        return;
                                    }
                                    catch (BadLocationException ex) {
                                        target.getToolkit().beep();
                                    }
                                }
                                String linesText = doc.getText(startLineStartOffset, endLineEndOffset - startLineStartOffset);
                                Element nextLineElement = rootElement.getElement(zeroBaseEndLineNumber + 1);
                                int nextLineEndOffset = nextLineElement.getEndOffset();
                                int column = start - startLineStartOffset;
                                if (nextLineEndOffset > doc.getLength()) {
                                    doc.insertString(doc.getLength(), "\n" + linesText.substring(0, linesText.length() - 1), null);
                                } else {
                                    doc.insertString(nextLineEndOffset, linesText, null);
                                }
                                ActionFactory.removeLineByLine(doc, startLineStartOffset, endLineEndOffset - startLineStartOffset);
                                if (selection) {
                                    if (backwardSelection) {
                                        caret.setDot(nextLineEndOffset - (endLineEndOffset - startLineStartOffset) + column);
                                        caret.moveDot(nextLineEndOffset - (endLineEndOffset - end - 1));
                                    } else {
                                        caret.setDot(nextLineEndOffset - (endLineEndOffset - end - 1));
                                        caret.moveDot(nextLineEndOffset - (endLineEndOffset - startLineStartOffset) + column);
                                    }
                                } else {
                                    target.setCaretPosition(Math.min(doc.getLength(), nextLineEndOffset + column - (endLineEndOffset - startLineStartOffset)));
                                }
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    }
                });
            }
        }

    }

    public static class MoveSelectionElseLineUpAction
    extends LocalBaseAction {
        static final long serialVersionUID = 1;

        public MoveSelectionElseLineUpAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final BaseDocument doc = (BaseDocument)target.getDocument();
                if (doc instanceof GuardedDocument && ((GuardedDocument)doc).isPosGuarded(target.getCaretPosition())) {
                    target.getToolkit().beep();
                    return;
                }
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        block13 : {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                int start;
                                Element rootElement = doc.getDefaultRootElement();
                                Caret caret = target.getCaret();
                                boolean selection = false;
                                boolean backwardSelection = false;
                                int end = start = target.getCaretPosition();
                                if (Utilities.isSelectionShowing(caret)) {
                                    int selStart = caret.getDot();
                                    int selEnd = caret.getMark();
                                    start = Math.min(selStart, selEnd);
                                    end = Math.max(selStart, selEnd) - 1;
                                    selection = true;
                                    backwardSelection = selStart >= selEnd;
                                }
                                int zeroBaseStartLineNumber = rootElement.getElementIndex(start);
                                int zeroBaseEndLineNumber = rootElement.getElementIndex(end);
                                if (zeroBaseStartLineNumber == -1) {
                                    target.getToolkit().beep();
                                    break block13;
                                }
                                if (zeroBaseStartLineNumber == 0) break block13;
                                try {
                                    Element startLineElement = rootElement.getElement(zeroBaseStartLineNumber);
                                    int startLineStartOffset = startLineElement.getStartOffset();
                                    Element endLineElement = rootElement.getElement(zeroBaseEndLineNumber);
                                    int endLineEndOffset = endLineElement.getEndOffset();
                                    String linesText = doc.getText(startLineStartOffset, endLineEndOffset - startLineStartOffset);
                                    Element previousLineElement = rootElement.getElement(zeroBaseStartLineNumber - 1);
                                    int previousLineStartOffset = previousLineElement.getStartOffset();
                                    int column = start - startLineStartOffset;
                                    doc.insertString(previousLineStartOffset, linesText, null);
                                    if (endLineEndOffset + linesText.length() > doc.getLength()) {
                                        ActionFactory.removeLineByLine(doc, startLineStartOffset + linesText.length() - 1, endLineEndOffset - startLineStartOffset);
                                    } else {
                                        ActionFactory.removeLineByLine(doc, startLineStartOffset + linesText.length(), endLineEndOffset - startLineStartOffset);
                                    }
                                    if (selection) {
                                        if (backwardSelection) {
                                            caret.setDot(previousLineStartOffset + column);
                                            caret.moveDot(previousLineStartOffset + (endLineEndOffset - startLineStartOffset) - (endLineEndOffset - end - 1));
                                        } else {
                                            caret.setDot(previousLineStartOffset + (endLineEndOffset - startLineStartOffset) - (endLineEndOffset - end - 1));
                                            caret.moveDot(previousLineStartOffset + column);
                                        }
                                    } else {
                                        target.setCaretPosition(previousLineStartOffset + column);
                                    }
                                }
                                catch (BadLocationException ex) {
                                    target.getToolkit().beep();
                                }
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    }
                });
            }
        }

    }

    public static class RemoveLineAction
    extends LocalBaseAction {
        static final long serialVersionUID = -536315497241419877L;

        public RemoveLineAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int bolPos = Utilities.getRowStart(target, target.getSelectionStart());
                            int eolPos = Utilities.getRowEnd(target, target.getSelectionEnd());
                            if (eolPos == doc.getLength()) {
                                if (bolPos > 0) {
                                    --bolPos;
                                }
                            } else {
                                ++eolPos;
                            }
                            doc.remove(bolPos, eolPos - bolPos);
                        }
                        catch (BadLocationException e) {
                            target.getToolkit().beep();
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class RemoveLineBeginAction
    extends LocalBaseAction {
        static final long serialVersionUID = 9193117196412195554L;

        public RemoveLineBeginAction() {
            super(30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int dotPos = caret.getDot();
                            int bolPos = Utilities.getRowStart(doc, dotPos);
                            if (dotPos == bolPos) {
                                if (dotPos > 0) {
                                    doc.remove(dotPos - 1, 1);
                                }
                            } else {
                                char[] chars = doc.getChars(bolPos, dotPos - bolPos);
                                if (Analyzer.isWhitespace(chars, 0, chars.length)) {
                                    doc.remove(bolPos, dotPos - bolPos);
                                } else {
                                    int firstNW = Utilities.getRowFirstNonWhite(doc, bolPos);
                                    if (firstNW >= 0 && firstNW < dotPos) {
                                        doc.remove(firstNW, dotPos - firstNW);
                                    }
                                }
                            }
                        }
                        catch (BadLocationException e) {
                            target.getToolkit().beep();
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class RemoveWordNextAction
    extends LocalBaseAction {
        public RemoveWordNextAction() {
            super("remove-word-next", 30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int dotPos = caret.getDot();
                            int eolPos = Utilities.getRowEnd(doc, dotPos);
                            int wsPos = Utilities.getNextWord(target, dotPos);
                            wsPos = dotPos == eolPos ? wsPos : Math.min(eolPos, wsPos);
                            doc.remove(dotPos, wsPos - dotPos);
                        }
                        catch (BadLocationException e) {
                            target.getToolkit().beep();
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class RemoveWordPreviousAction
    extends LocalBaseAction {
        public RemoveWordPreviousAction() {
            super("remove-word-previous", 30);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                        try {
                            int dotPos = caret.getDot();
                            int bolPos = Utilities.getRowStart(doc, dotPos);
                            int wsPos = Utilities.getPreviousWord(target, dotPos);
                            wsPos = dotPos == bolPos ? wsPos : Math.max(bolPos, wsPos);
                            doc.remove(wsPos, dotPos - wsPos);
                        }
                        catch (BadLocationException e) {
                            target.getToolkit().beep();
                        }
                        finally {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                        }
                    }
                });
            }
        }

    }

    public static class RemoveTabAction
    extends LocalBaseAction {
        static final long serialVersionUID = -1537748600593395706L;

        public RemoveTabAction() {
            super("remove-tab", 22);
        }

        @Override
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            if (target != null) {
                if (!target.isEditable() || !target.isEnabled()) {
                    target.getToolkit().beep();
                    return;
                }
                final Caret caret = target.getCaret();
                final BaseDocument doc = (BaseDocument)target.getDocument();
                doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        block15 : {
                            DocumentUtilities.setTypingModification((Document)doc, (boolean)true);
                            try {
                                if (Utilities.isSelectionShowing(caret)) {
                                    try {
                                        BaseKit.changeBlockIndent(doc, target.getSelectionStart(), target.getSelectionEnd(), -1);
                                    }
                                    catch (GuardedException e) {
                                        target.getToolkit().beep();
                                    }
                                    catch (BadLocationException e) {
                                        e.printStackTrace();
                                    }
                                    break block15;
                                }
                                try {
                                    int dot = caret.getDot();
                                    int lineStartOffset = Utilities.getRowStart(doc, dot);
                                    int firstNW = Utilities.getRowFirstNonWhite(doc, dot);
                                    if (firstNW != -1 && dot <= firstNW) {
                                        int lineEndOffset = Utilities.getRowEnd(doc, dot);
                                        BaseKit.changeBlockIndent(doc, lineStartOffset, lineEndOffset, -1);
                                    } else {
                                        int endNW;
                                        int shiftWidth;
                                        int n = endNW = firstNW == -1 ? lineStartOffset : Utilities.getRowLastNonWhite(doc, dot) + 1;
                                        if (dot > endNW && (shiftWidth = doc.getShiftWidth()) > 0) {
                                            int insertLen;
                                            int dotColumn = Utilities.getVisualColumn(doc, dot);
                                            int targetColumn = Math.max(0, (dotColumn - 1) / shiftWidth * shiftWidth);
                                            while (dotColumn > targetColumn && --dot >= endNW) {
                                                doc.remove(dot, 1);
                                                dotColumn = Utilities.getVisualColumn(doc, dot);
                                            }
                                            if (dot >= endNW && (insertLen = targetColumn - dotColumn) > 0) {
                                                char[] spaceChars = new char[insertLen];
                                                Arrays.fill(spaceChars, ' ');
                                                String spaces = new String(spaceChars);
                                                doc.insertString(dot, spaces, null);
                                            }
                                        }
                                    }
                                }
                                catch (GuardedException e) {
                                    target.getToolkit().beep();
                                }
                                catch (BadLocationException e) {
                                    e.printStackTrace();
                                }
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)doc, (boolean)false);
                            }
                        }
                    }
                });
            }
        }

    }

}

