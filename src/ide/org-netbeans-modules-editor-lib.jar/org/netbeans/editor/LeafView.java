/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseElement;
import org.netbeans.editor.BaseView;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.LeafElement;
import org.netbeans.editor.Mark;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.lib.drawing.DrawContext;
import org.netbeans.modules.editor.lib.drawing.DrawEngine;
import org.netbeans.modules.editor.lib.drawing.DrawGraphics;

public class LeafView
extends BaseView {
    protected int mainHeight;
    final ModelToViewDG modelToViewDG = new ModelToViewDG();
    final ViewToModelDG viewToModelDG = new ViewToModelDG();

    public LeafView(Element elem) {
        super(elem);
    }

    @Override
    public void setParent(View parent) {
        super.setParent(parent);
        if (this.getParent() != null) {
            this.updateMainHeight();
        }
    }

    @Override
    protected int getPaintAreas(Graphics g, int clipY, int clipHeight) {
        if (clipHeight <= 0) {
            return 0;
        }
        int clipEndY = clipY + clipHeight;
        int startY = this.getStartY();
        if (this.insets != null) {
            int mainAreaY = startY + this.insets.top;
            if (clipEndY <= mainAreaY) {
                return 1;
            }
            int bottomInsetsY = mainAreaY + this.mainHeight;
            if (clipEndY <= bottomInsetsY) {
                if (clipY <= mainAreaY) {
                    return 3;
                }
                return 2;
            }
            if (clipY <= mainAreaY) {
                return 7;
            }
            if (clipY <= bottomInsetsY) {
                return 6;
            }
            if (clipY <= bottomInsetsY + this.insets.bottom) {
                return 4;
            }
            return 0;
        }
        if (clipEndY <= startY || clipY >= startY + this.getHeight()) {
            return 0;
        }
        return 2;
    }

    @Override
    protected void paintAreas(Graphics g, int clipY, int clipHeight, int paintAreas) {
        if ((paintAreas & 2) == 2) {
            EditorUI editorUI = this.getEditorUI();
            int paintY = Math.max(clipY, 0);
            int startPos = this.getPosFromY(paintY);
            if (clipHeight > 0) {
                BaseDocument doc = (BaseDocument)this.getDocument();
                try {
                    int pos = this.getPosFromY(clipY + clipHeight - 1);
                    int endPos = Utilities.getRowEnd(doc, pos);
                    int baseY = this.getYFromPos(startPos);
                    DrawEngine.getDrawEngine().draw(new DrawGraphics.GraphicsDG(g), editorUI, startPos, endPos, this.getBaseX(baseY), baseY, Integer.MAX_VALUE);
                }
                catch (BadLocationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int getHeight() {
        if (this.insets != null) {
            return this.insets.top + this.mainHeight + this.insets.bottom;
        }
        return this.mainHeight;
    }

    @Override
    public void updateMainHeight() {
        LeafElement elem = (LeafElement)this.getElement();
        try {
            int lineDiff = elem.getEndMark().getLine() - elem.getStartMark().getLine() + 1;
            this.mainHeight = lineDiff * this.getEditorUI().getLineHeight();
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            this.mainHeight = 0;
        }
    }

    @Override
    protected int getPosFromY(int y) {
        int relY = y - this.getStartY() - (this.insets != null ? this.insets.top : 0);
        if (relY < 0) {
            return this.getStartOffset();
        }
        if (relY >= this.mainHeight) {
            return this.getEndOffset();
        }
        int line = 0;
        try {
            line = ((BaseElement)this.getElement()).getStartMark().getLine();
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
        int startOffset = this.getStartOffset();
        int pos = Utilities.getRowStartFromLineOffset((BaseDocument)this.getDocument(), line += relY / this.getEditorUI().getLineHeight());
        if (pos == -1) {
            pos = startOffset;
        }
        return Math.max(pos, startOffset);
    }

    @Override
    public int getBaseX(int y) {
        return this.getEditorUI().getTextMargin().left + (this.insets != null ? this.insets.left : 0);
    }

    @Override
    public final int getViewCount() {
        return 0;
    }

    @Override
    public final View getView(int n) {
        return null;
    }

    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        if (biasRet != null) {
            biasRet[0] = Position.Bias.Forward;
        }
        switch (direction) {
            case 1: {
                try {
                    BaseDocument doc = (BaseDocument)this.getDocument();
                    int visCol = doc.getVisColFromPos(pos);
                    pos = doc.getOffsetFromVisCol(visCol, Utilities.getRowStart(doc, pos, -1));
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
                return pos;
            }
            case 5: {
                try {
                    BaseDocument doc = (BaseDocument)this.getDocument();
                    int visCol = doc.getVisColFromPos(pos);
                    pos = doc.getOffsetFromVisCol(visCol, Utilities.getRowStart(doc, pos, 1));
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
                return pos;
            }
            case 7: {
                return pos == -1 ? this.getStartOffset() : pos - 1;
            }
            case 3: {
                return pos == -1 ? this.getEndOffset() : pos + 1;
            }
        }
        throw new IllegalArgumentException("Bad direction: " + direction);
    }

    @Override
    protected int getYFromPos(int pos) throws BadLocationException {
        int relLine = 0;
        try {
            relLine = Utilities.getLineOffset((BaseDocument)this.getDocument(), pos) - ((BaseElement)this.getElement()).getStartMark().getLine();
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
        return this.getStartY() + (this.insets != null ? this.insets.top : 0) + relLine * this.getEditorUI().getLineHeight();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
        Rectangle ret;
        EditorUI editorUI = this.getEditorUI();
        ret = new Rectangle();
        BaseDocument doc = (BaseDocument)this.getDocument();
        if (pos < 0 || pos > doc.getLength()) {
            throw new BadLocationException("Invalid offset", pos);
        }
        ret.y = this.getYFromPos(pos);
        try {
            ModelToViewDG modelToViewDG = this.modelToViewDG;
            synchronized (modelToViewDG) {
                this.modelToViewDG.r = ret;
                Element lineElement = doc.getParagraphElement(pos);
                int bolPos = lineElement.getStartOffset();
                int eolPos = lineElement.getEndOffset() - 1;
                DrawEngine.getDrawEngine().draw(this.modelToViewDG, editorUI, bolPos, eolPos, this.getBaseX(ret.y), ret.y, pos);
                this.modelToViewDG.r = null;
            }
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
        }
        return ret;
    }

    @Override
    public Shape modelToView(int p0, Position.Bias b0, int p1, Position.Bias b1, Shape a) throws BadLocationException {
        Rectangle r0 = (Rectangle)this.modelToView(p0, a, b0);
        Rectangle r1 = (Rectangle)this.modelToView(p1, a, b1);
        if (r0.y != r1.y) {
            r0.x = this.getComponent().getX();
            r0.width = this.getComponent().getWidth();
        }
        r0.add(r1);
        return r0;
    }

    @Override
    void modelToViewDG(int pos, DrawGraphics dg) throws BadLocationException {
        EditorUI editorUI = this.getEditorUI();
        BaseDocument doc = (BaseDocument)this.getDocument();
        if (pos < 0 || pos > doc.getLength()) {
            throw new BadLocationException("Invalid offset", pos);
        }
        int y = this.getYFromPos(pos);
        Element lineElement = doc.getParagraphElement(pos);
        DrawEngine.getDrawEngine().draw(dg, editorUI, lineElement.getStartOffset(), lineElement.getEndOffset() - 1, this.getBaseX(y), y, pos);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int viewToModel(float x, float y, Shape a, Position.Bias[] biasReturn) {
        int pos;
        int begMainY;
        int intX = (int)x;
        int intY = (int)y;
        if (biasReturn != null) {
            biasReturn[0] = Position.Bias.Forward;
        }
        if (intY < (begMainY = this.getStartY() + (this.insets != null ? this.insets.top : 0))) {
            return -1;
        }
        if (intY > begMainY + this.mainHeight) {
            return this.getEndOffset();
        }
        pos = this.getPosFromY(intY);
        EditorUI editorUI = this.getEditorUI();
        try {
            int eolPos = Utilities.getRowEnd((BaseDocument)this.getDocument(), pos);
            ViewToModelDG viewToModelDG = this.viewToModelDG;
            synchronized (viewToModelDG) {
                this.viewToModelDG.setTargetX(intX);
                this.viewToModelDG.setEOLOffset(eolPos);
                DrawEngine.getDrawEngine().draw(this.viewToModelDG, editorUI, pos, eolPos, this.getBaseX(intY), 0, -1);
                pos = this.viewToModelDG.getOffset();
            }
        }
        catch (BadLocationException e) {
            // empty catch block
        }
        return pos;
    }

    @Override
    public void insertUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        try {
            BaseDocumentEvent bevt = (BaseDocumentEvent)evt;
            EditorUI editorUI = this.getEditorUI();
            int y = this.getYFromPos(evt.getOffset());
            int lineHeight = editorUI.getLineHeight();
            if (bevt.getLFCount() > 0) {
                int addHeight = bevt.getLFCount() * lineHeight;
                this.mainHeight += addHeight;
                editorUI.repaint(y);
            } else {
                int syntaxY = this.getYFromPos(bevt.getSyntaxUpdateOffset());
                if (bevt.getSyntaxUpdateOffset() == evt.getDocument().getLength()) {
                    syntaxY += lineHeight;
                }
                if (this.getComponent().isShowing()) {
                    editorUI.repaint(y, Math.max(lineHeight, syntaxY - y));
                }
            }
        }
        catch (BadLocationException ex) {
            Utilities.annotateLoggable(ex);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        try {
            BaseDocumentEvent bevt = (BaseDocumentEvent)evt;
            EditorUI editorUI = this.getEditorUI();
            int y = this.getYFromPos(evt.getOffset());
            int lineHeight = editorUI.getLineHeight();
            if (bevt.getLFCount() > 0) {
                int removeHeight = bevt.getLFCount() * lineHeight;
                this.mainHeight -= removeHeight;
                editorUI.repaint(y);
            } else {
                int syntaxY = this.getYFromPos(bevt.getSyntaxUpdateOffset());
                if (bevt.getSyntaxUpdateOffset() == evt.getDocument().getLength()) {
                    syntaxY += lineHeight;
                }
                if (this.getComponent().isShowing()) {
                    editorUI.repaint(y, Math.max(lineHeight, syntaxY - y));
                }
            }
        }
        catch (BadLocationException ex) {
            Utilities.annotateLoggable(ex);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        try {
            if (this.getComponent().isShowing()) {
                this.getEditorUI().repaintBlock(evt.getOffset(), evt.getOffset() + evt.getLength());
            }
        }
        catch (BadLocationException ex) {
            Utilities.annotateLoggable(ex);
        }
    }

    @Override
    protected int getViewStartY(BaseView view, int helperInd) {
        return 0;
    }

    static final class ViewToModelDG
    extends DrawGraphics.SimpleDG {
        int targetX;
        int offset;
        int eolOffset;

        ViewToModelDG() {
        }

        void setTargetX(int targetX) {
            this.targetX = targetX;
        }

        void setEOLOffset(int eolOffset) {
            this.eolOffset = eolOffset;
            this.offset = eolOffset;
        }

        int getOffset() {
            return this.offset;
        }

        @Override
        public boolean targetOffsetReached(int offset, char ch, int x, int charWidth, DrawContext ctx) {
            if (offset <= this.eolOffset) {
                if (x + charWidth < this.targetX) {
                    this.offset = offset;
                    return true;
                }
                this.offset = offset;
                if (this.targetX > x + charWidth / 2) {
                    BaseDocument doc = ctx.getEditorUI().getDocument();
                    if (ch != '\n' && doc != null && offset < doc.getLength()) {
                        ++this.offset;
                    }
                }
                return false;
            }
            return false;
        }
    }

    static final class ModelToViewDG
    extends DrawGraphics.SimpleDG {
        Rectangle r;

        ModelToViewDG() {
        }

        @Override
        public boolean targetOffsetReached(int pos, char ch, int x, int charWidth, DrawContext ctx) {
            this.r.x = x;
            this.r.y = this.getY();
            this.r.width = charWidth;
            this.r.height = this.getLineHeight();
            return false;
        }
    }

}

