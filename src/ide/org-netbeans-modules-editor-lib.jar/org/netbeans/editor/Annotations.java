/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.awt.DynamicMenuContent
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.editor;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.BaseAction;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.Mark;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.lib.drawing.ChainDrawMark;
import org.netbeans.modules.editor.lib.drawing.MarkChain;
import org.openide.awt.Actions;
import org.openide.awt.DynamicMenuContent;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.Presenter;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class Annotations
implements DocumentListener {
    private final Map<Mark, LineAnnotations> lineAnnotationsByMark = new HashMap<Mark, LineAnnotations>(30);
    private final List<LineAnnotations> lineAnnotationsArray = new ArrayList<LineAnnotations>(20);
    private final MarkChain markChain;
    private final BaseDocument doc;
    private final EventListenerList listenerList = new EventListenerList();
    private final PropertyChangeListener l;
    private boolean glyphColumn = false;
    private boolean glyphButtonColumn = false;
    private boolean menuInitialized = false;
    public static final Comparator<JMenu> MENU_COMPARATOR = new MenuComparator();
    private int lastGetLineAnnotationsIdx = -1;
    private int lastGetLineAnnotationsLine = -1;
    private LineAnnotations lastGetLineAnnotationsResult = null;

    public Annotations(BaseDocument doc) {
        this.doc = doc;
        this.markChain = new MarkChain(doc, null);
        this.doc.addDocumentListener(this);
        this.l = new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName() == null || "annotationType".equals(evt.getPropertyName())) {
                    LineAnnotations lineAnnos;
                    List list = Annotations.this.lineAnnotationsArray;
                    synchronized (list) {
                        AnnotationDesc anno = (AnnotationDesc)evt.getSource();
                        lineAnnos = (LineAnnotations)Annotations.this.lineAnnotationsByMark.get(anno.getMark());
                        if (lineAnnos != null) {
                            lineAnnos.refreshAnnotations();
                        }
                    }
                    if (lineAnnos != null) {
                        Annotations.this.refreshLine(lineAnnos.getLine());
                    }
                }
                if (evt.getPropertyName() == null || "moveToFront".equals(evt.getPropertyName())) {
                    AnnotationDesc anno = (AnnotationDesc)evt.getSource();
                    Annotations.this.frontAnnotation(anno);
                }
            }
        };
        AnnotationTypes.getTypes().addPropertyChangeListener(new PropertyChangeListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                List list;
                if (evt.getPropertyName() == null || "combineGlyphs".equals(evt.getPropertyName())) {
                    list = Annotations.this.lineAnnotationsArray;
                    synchronized (list) {
                        for (LineAnnotations lineAnnos : Annotations.this.lineAnnotationsArray) {
                            lineAnnos.refreshAnnotations();
                        }
                    }
                }
                if (evt.getPropertyName() == null || "annotationTypes".equals(evt.getPropertyName())) {
                    list = Annotations.this.lineAnnotationsArray;
                    synchronized (list) {
                        for (LineAnnotations lineAnnos : Annotations.this.lineAnnotationsArray) {
                            Iterator<AnnotationDesc> it2 = lineAnnos.getAnnotations();
                            while (it2.hasNext()) {
                                AnnotationDesc anno = it2.next();
                                anno.updateAnnotationType();
                            }
                        }
                    }
                }
                Annotations.this.fireChangedAll();
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addAnnotation(AnnotationDesc anno) {
        LineAnnotations lineAnnos;
        List<LineAnnotations> list = this.lineAnnotationsArray;
        synchronized (list) {
            MarkChain chain = this.markChain;
            try {
                chain.addMark(anno.getOffset(), true);
            }
            catch (BadLocationException e) {
                throw new IllegalStateException("offset=" + anno.getOffset() + ", docLen=" + this.doc.getLength());
            }
            ChainDrawMark annoMark = chain.getAddedMark();
            if (annoMark == null) {
                throw new NullPointerException();
            }
            anno.setMark(annoMark);
            lineAnnos = this.lineAnnotationsByMark.get(annoMark);
            if (lineAnnos == null) {
                lineAnnos = this.getLineAnnotations(anno.getLine());
                if (lineAnnos == null) {
                    lineAnnos = new LineAnnotations();
                    lineAnnos.addAnnotation(anno);
                    if (this.lineAnnotationsByMark.put(anno.getMark(), lineAnnos) != null) {
                        throw new IllegalStateException("Mark already in the map.");
                    }
                    int pos = Collections.binarySearch(this.lineAnnotationsArray, lineAnnos, new LineAnnotationsComparator());
                    this.lineAnnotationsArray.add(- pos - 1, lineAnnos);
                    this.lastGetLineAnnotationsIdx = -1;
                    this.lastGetLineAnnotationsLine = -1;
                    this.lastGetLineAnnotationsResult = null;
                } else {
                    lineAnnos.addAnnotation(anno);
                    if (this.lineAnnotationsByMark.get(anno.getMark()) == null) {
                        this.lineAnnotationsByMark.put(anno.getMark(), lineAnnos);
                    }
                }
            } else {
                lineAnnos.addAnnotation(anno);
            }
            anno.addPropertyChangeListener(this.l);
            if (anno.isVisible() && (!anno.isDefaultGlyph() || anno.isDefaultGlyph() && lineAnnos.getCount() > 1)) {
                this.glyphColumn = true;
            }
            if (lineAnnos.getCount() > 1) {
                this.glyphButtonColumn = true;
            }
        }
        this.refreshLine(lineAnnos.getLine());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeAnnotation(AnnotationDesc anno) {
        int line;
        List<LineAnnotations> list = this.lineAnnotationsArray;
        synchronized (list) {
            int index;
            ChainDrawMark annoMark = (ChainDrawMark)anno.getMark();
            if (annoMark == null) {
                return;
            }
            LineAnnotations lineAnnos = this.lineAnnotationsByMark.get(annoMark);
            line = lineAnnos.getLine();
            lineAnnos.removeAnnotation(anno);
            if (!lineAnnos.isMarkStillReferenced(annoMark)) {
                if (this.lineAnnotationsByMark.remove(annoMark) != lineAnnos) {
                    throw new IllegalStateException();
                }
                MarkChain chain = this.markChain;
                if (!chain.removeMark(annoMark)) {
                    throw new IllegalStateException("Mark not removed");
                }
            }
            if (lineAnnos.getCount() == 0 && (index = this.lineAnnotationsArray.indexOf(lineAnnos)) != -1) {
                this.lastGetLineAnnotationsIdx = -1;
                this.lastGetLineAnnotationsLine = -1;
                this.lastGetLineAnnotationsResult = null;
                this.lineAnnotationsArray.remove(index);
            }
            anno.setMark(null);
            anno.removePropertyChangeListener(this.l);
        }
        this.refreshLine(line);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public AnnotationDesc getActiveAnnotation(Mark mark) {
        List<LineAnnotations> list = this.lineAnnotationsArray;
        synchronized (list) {
            LineAnnotations annos = this.lineAnnotationsByMark.get(mark);
            if (annos == null) {
                return null;
            }
            AnnotationDesc anno = annos.getActive();
            if (anno == null || anno.getMark() != mark) {
                return null;
            }
            return anno;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    AnnotationDesc getLineActiveAnnotation(Mark mark) {
        List<LineAnnotations> list = this.lineAnnotationsArray;
        synchronized (list) {
            LineAnnotations annos = this.lineAnnotationsByMark.get(mark);
            if (annos == null) {
                return null;
            }
            AnnotationDesc anno = annos.getActive();
            return anno;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected LineAnnotations getLineAnnotations(int line) {
        List<LineAnnotations> list = this.lineAnnotationsArray;
        synchronized (list) {
            LineAnnotations annos;
            if (this.lastGetLineAnnotationsIdx != -1 && this.lastGetLineAnnotationsLine != -1 && this.lastGetLineAnnotationsResult != null) {
                if (this.lastGetLineAnnotationsLine == line) {
                    return this.lastGetLineAnnotationsResult;
                }
                if (this.lastGetLineAnnotationsLine + 1 == line && this.lastGetLineAnnotationsIdx + 1 < this.lineAnnotationsArray.size() && (annos = this.lineAnnotationsArray.get(this.lastGetLineAnnotationsIdx + 1)).getLine() == line) {
                    ++this.lastGetLineAnnotationsIdx;
                    this.lastGetLineAnnotationsLine = annos.getLine();
                    this.lastGetLineAnnotationsResult = annos;
                    return annos;
                }
            }
            int low = 0;
            int high = this.lineAnnotationsArray.size() - 1;
            while (low <= high) {
                int mid = (low + high) / 2;
                annos = this.lineAnnotationsArray.get(mid);
                int annosLine = annos.getLine();
                if (line < annosLine) {
                    high = mid - 1;
                    continue;
                }
                if (line > annosLine) {
                    low = mid + 1;
                    continue;
                }
                this.lastGetLineAnnotationsIdx = mid;
                this.lastGetLineAnnotationsLine = annosLine;
                this.lastGetLineAnnotationsResult = annos;
                return annos;
            }
            this.lastGetLineAnnotationsIdx = -1;
            this.lastGetLineAnnotationsLine = -1;
            this.lastGetLineAnnotationsResult = null;
            return null;
        }
    }

    public AnnotationDesc getActiveAnnotation(int line) {
        LineAnnotations annos = this.getLineAnnotations(line);
        if (annos == null) {
            return null;
        }
        return annos.getActive();
    }

    public void frontAnnotation(AnnotationDesc anno) {
        int line = anno.getLine();
        LineAnnotations annos = this.getLineAnnotations(line);
        if (annos == null) {
            return;
        }
        annos.activate(anno);
        this.refreshLine(line);
    }

    public AnnotationDesc activateNextAnnotation(int line) {
        LineAnnotations annos = this.getLineAnnotations(line);
        if (annos == null) {
            return null;
        }
        AnnotationDesc aa = annos.activateNext();
        this.refreshLine(line);
        return aa;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int getNextLineWithAnnotation(int line) {
        List<LineAnnotations> list = this.lineAnnotationsArray;
        synchronized (list) {
            LineAnnotations annos;
            int annosLine;
            if (this.lastGetLineAnnotationsIdx != -1 && this.lastGetLineAnnotationsLine != -1 && this.lastGetLineAnnotationsResult != null) {
                if (this.lastGetLineAnnotationsLine == line) {
                    return this.lastGetLineAnnotationsLine;
                }
                if (this.lastGetLineAnnotationsLine + 1 == line && this.lastGetLineAnnotationsIdx + 1 < this.lineAnnotationsArray.size()) {
                    LineAnnotations annos2 = this.lineAnnotationsArray.get(this.lastGetLineAnnotationsIdx + 1);
                    ++this.lastGetLineAnnotationsIdx;
                    this.lastGetLineAnnotationsLine = annos2.getLine();
                    this.lastGetLineAnnotationsResult = annos2;
                    return this.lastGetLineAnnotationsLine;
                }
            }
            int low = 0;
            int high = this.lineAnnotationsArray.size() - 1;
            while (low <= high) {
                int mid = (low + high) / 2;
                annos = this.lineAnnotationsArray.get(mid);
                annosLine = annos.getLine();
                if (line < annosLine) {
                    high = mid - 1;
                    continue;
                }
                if (line > annosLine) {
                    low = mid + 1;
                    continue;
                }
                this.lastGetLineAnnotationsIdx = mid;
                this.lastGetLineAnnotationsLine = annosLine;
                this.lastGetLineAnnotationsResult = annos;
                return annosLine;
            }
            for (int i = low; i < this.lineAnnotationsArray.size(); ++i) {
                annos = this.lineAnnotationsArray.get(i);
                annosLine = annos.getLine();
                if (annosLine < line) continue;
                this.lastGetLineAnnotationsIdx = i;
                this.lastGetLineAnnotationsLine = annosLine;
                this.lastGetLineAnnotationsResult = annos;
                return annosLine;
            }
            this.lastGetLineAnnotationsIdx = -1;
            this.lastGetLineAnnotationsLine = -1;
            this.lastGetLineAnnotationsResult = null;
            return -1;
        }
    }

    public AnnotationDesc getAnnotation(int line, String type) {
        LineAnnotations lineAnnotations = this.getLineAnnotations(line);
        if (lineAnnotations != null) {
            return lineAnnotations.getType(type);
        }
        return null;
    }

    public AnnotationDesc[] getPasiveAnnotations(int line) {
        return this.getPassiveAnnotationsForLine(line);
    }

    public AnnotationDesc[] getPassiveAnnotationsForLine(int line) {
        LineAnnotations annos = this.getLineAnnotations(line);
        if (annos == null) {
            return null;
        }
        return annos.getPassive();
    }

    public AnnotationDesc[] getPassiveAnnotations(int offset) {
        int lineIndex = this.doc.getDefaultRootElement().getElementIndex(offset);
        return lineIndex >= 0 ? this.getPassiveAnnotationsForLine(lineIndex) : null;
    }

    public int getNumberOfAnnotations(int line) {
        LineAnnotations annos = this.getLineAnnotations(line);
        if (annos == null) {
            return 0;
        }
        return annos.getCount();
    }

    protected void refreshLine(int line) {
        this.fireChangedLine(line);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        BaseDocumentEvent be = (BaseDocumentEvent)e;
        int countOfDeletedLines = be.getLFCount();
        if (countOfDeletedLines == 0) {
            return;
        }
        this.fireChangedAll();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        BaseDocumentEvent be = (BaseDocumentEvent)e;
        int countOfInsertedLines = be.getLFCount();
        if (countOfInsertedLines == 0) {
            return;
        }
        this.fireChangedAll();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    public void addAnnotationsListener(AnnotationsListener listener) {
        this.listenerList.add(AnnotationsListener.class, listener);
    }

    public void removeAnnotationsListener(AnnotationsListener listener) {
        this.listenerList.remove(AnnotationsListener.class, listener);
    }

    protected void fireChangedLine(int line) {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != AnnotationsListener.class) continue;
            ((AnnotationsListener)listeners[i + 1]).changedLine(line);
        }
    }

    protected void fireChangedAll() {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] != AnnotationsListener.class) continue;
            ((AnnotationsListener)listeners[i + 1]).changedAll();
        }
    }

    public boolean isGlyphColumn() {
        return this.glyphColumn;
    }

    public boolean isGlyphButtonColumn() {
        return this.glyphButtonColumn;
    }

    private void addAcceleretors(Action a, JMenuItem item, BaseKit kit) {
        JTextComponent target = Utilities.getFocusedComponent();
        if (target == null) {
            return;
        }
        Keymap km = target.getKeymap();
        if (km != null) {
            KeyStroke[] keys = km.getKeyStrokesForAction(a);
            if (keys != null && keys.length > 0) {
                item.setAccelerator(keys[0]);
            } else {
                String actionName = (String)a.getValue("Name");
                if (actionName == null) {
                    return;
                }
                BaseAction action = (BaseAction)kit.getActionByName(actionName);
                if (action == null) {
                    return;
                }
                keys = km.getKeyStrokesForAction(action);
                if (keys != null && keys.length > 0) {
                    item.setAccelerator(keys[0]);
                }
            }
        }
    }

    private Action getAction(AnnotationDesc anno, Action action) {
        if (action instanceof ContextAwareAction && anno instanceof Lookup.Provider) {
            Lookup lookup = ((Lookup.Provider)anno).getLookup();
            action = ((ContextAwareAction)action).createContextAwareInstance(lookup);
        }
        return action;
    }

    private JMenuItem createMenuItem(Action action, BaseKit kit) {
        JMenuItem item;
        if (action instanceof BaseAction) {
            item = new JMenuItem(((BaseAction)action).getPopupMenuText(null));
            item.addActionListener(action);
            this.addAcceleretors(action, item, kit);
        } else if (action instanceof Presenter.Popup) {
            item = ((Presenter.Popup)action).getPopupPresenter();
        } else {
            item = new JMenuItem((String)action.getValue("Name"));
            Actions.connect((JMenuItem)item, (Action)action, (boolean)true);
            this.addAcceleretors(action, item, kit);
        }
        return item;
    }

    public JPopupMenu createPopupMenu(BaseKit kit, int line) {
        return this.createMenu(kit, line, false).getPopupMenu();
    }

    private JMenu createSubMenu(AnnotationDesc anno, BaseKit kit) {
        JMenu subMenu = null;
        Action[] actions = anno.getActions();
        if (actions != null) {
            subMenu = new JMenu(anno.getAnnotationTypeInstance().getDescription());
            for (int j = 0; j < actions.length; ++j) {
                JMenuItem item = this.createMenuItem(this.getAction(anno, actions[j]), kit);
                if (item instanceof DynamicMenuContent) {
                    JComponent[] cs;
                    for (JComponent c : cs = ((DynamicMenuContent)item).getMenuPresenters()) {
                        subMenu.add(c);
                    }
                    continue;
                }
                subMenu.add(item);
            }
            if (subMenu.getItemCount() == 0) {
                subMenu = null;
            }
        }
        return subMenu;
    }

    private List<JMenu> createSubMenus(AnnotationDesc anno, BaseKit kit) {
        JMenu subMenu = this.createSubMenu(anno, kit);
        if (subMenu != null) {
            return Collections.singletonList(subMenu);
        }
        if (anno instanceof AnnotationCombination) {
            List<AnnotationDesc> subAnnotations = ((AnnotationCombination)anno).getCombinedAnnotations();
            ArrayList<JMenu> subMenus = new ArrayList<JMenu>(subAnnotations.size());
            for (AnnotationDesc ad : subAnnotations) {
                subMenu = this.createSubMenu(ad, kit);
                if (subMenu == null) continue;
                subMenus.add(subMenu);
            }
            return subMenus;
        }
        return Collections.emptyList();
    }

    private void initMenu(JMenu pm, BaseKit kit, int line) {
        JMenuItem popupItem;
        Action action;
        LineAnnotations annos = this.getLineAnnotations(line);
        HashMap<String, String> types = new HashMap<String, String>(AnnotationTypes.getTypes().getVisibleAnnotationTypeNamesCount() * 4 / 3);
        TreeSet<JMenu> orderedSubMenus = new TreeSet<JMenu>(MENU_COMPARATOR);
        if (annos != null) {
            AnnotationDesc[] pasiveAnnos;
            AnnotationDesc anno = annos.getActive();
            if (anno != null) {
                List<JMenu> subMenus = this.createSubMenus(anno, kit);
                orderedSubMenus.addAll(subMenus);
                types.put(anno.getAnnotationType(), anno.getAnnotationType());
            }
            if ((pasiveAnnos = annos.getPassive()) != null) {
                for (int i = 0; i < pasiveAnnos.length; ++i) {
                    List<JMenu> subMenus = this.createSubMenus(pasiveAnnos[i], kit);
                    orderedSubMenus.addAll(subMenus);
                    types.put(pasiveAnnos[i].getAnnotationType(), pasiveAnnos[i].getAnnotationType());
                }
            }
        }
        Iterator<String> i = AnnotationTypes.getTypes().getAnnotationTypeNames();
        while (i.hasNext()) {
            Action[] actions;
            AnnotationType type = AnnotationTypes.getTypes().getType(i.next());
            if (type == null || !type.isVisible() || types.get(type.getName()) != null || (actions = type.getActions()) == null) continue;
            JMenu subMenu = new JMenu(type.getDescription());
            for (int j = 0; j < actions.length; ++j) {
                if (!actions[j].isEnabled()) continue;
                subMenu.add(this.createMenuItem(actions[j], kit));
            }
            if (subMenu.getItemCount() <= 0) continue;
            orderedSubMenus.add(subMenu);
        }
        if (!orderedSubMenus.isEmpty()) {
            for (JMenu subMenu : orderedSubMenus) {
                pm.add(subMenu);
            }
            pm.addSeparator();
        }
        if ((popupItem = Annotations.getPopupMenuItem(action = kit.getActionByName("toggle-line-numbers"))) != null) {
            pm.add(popupItem);
        }
        if ((action = kit.getActionByName("toggle-toolbar")) != null && (popupItem = Annotations.getPopupMenuItem(action)) != null) {
            pm.add(popupItem);
        }
        this.menuInitialized = true;
    }

    private static JMenuItem getPopupMenuItem(Action action) {
        JMenuItem popupItem = null;
        if (action instanceof BaseAction) {
            popupItem = ((BaseAction)action).getPopupMenuItem(null);
        }
        if (popupItem == null && action instanceof Presenter.Popup) {
            popupItem = ((Presenter.Popup)action).getPopupPresenter();
        }
        return popupItem;
    }

    private JMenu createMenu(BaseKit kit, int line, boolean backgroundInit) {
        final DelayedMenu pm = new DelayedMenu(NbBundle.getBundle(BaseKit.class).getString("generate-gutter-popup"));
        final BaseKit fKit = kit;
        final int fLine = line;
        if (backgroundInit) {
            RequestProcessor rp = RequestProcessor.getDefault();
            RequestProcessor.Task task = rp.create(new Runnable(){

                @Override
                public void run() {
                    Annotations.this.initMenu(pm, fKit, fLine);
                    pm.clearTask();
                }
            });
            pm.setTask(task);
            task.schedule(0);
        } else {
            this.initMenu(pm, fKit, fLine);
        }
        return pm;
    }

    public JMenu createMenu(BaseKit kit, int line) {
        boolean bkgInit = this.menuInitialized;
        this.menuInitialized = true;
        return this.createMenu(kit, line, !bkgInit);
    }

    private String dumpAnnotaionDesc(AnnotationDesc ad) {
        return "offset=" + ad.getOffset() + "(ls=" + this.doc.getParagraphElement(ad.getOffset()).getStartOffset() + "), line=" + ad.getLine() + ", type=" + ad.getAnnotationType();
    }

    private String dumpLineAnnotationsArray() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.lineAnnotationsArray.size(); ++i) {
            LineAnnotations la = this.lineAnnotationsArray.get(i);
            LinkedList annos = la.annos;
            sb.append("[" + i + "]: line=" + la.getLine() + ", anos:");
            for (AnnotationDesc ad : annos) {
                sb.append("\n    " + this.dumpAnnotaionDesc(ad));
            }
            sb.append('\n');
        }
        return sb.toString();
    }

    public String toString() {
        return this.dumpLineAnnotationsArray() + ", lastGetLineAnnotationsIdx = " + this.lastGetLineAnnotationsIdx + ", lastGetLineAnnotationsLine = " + this.lastGetLineAnnotationsLine;
    }

    private static class LineAnnotationsComparator
    implements Comparator<LineAnnotations> {
        private LineAnnotationsComparator() {
        }

        @Override
        public int compare(LineAnnotations o1, LineAnnotations o2) {
            return o1.getLine() - o2.getLine();
        }
    }

    public static final class MenuComparator
    implements Comparator<JMenu> {
        @Override
        public int compare(JMenu menuOne, JMenu menuTwo) {
            if (menuTwo == null || menuOne == null) {
                return 0;
            }
            String menuOneText = menuOne.getText();
            String menuTwoText = menuTwo.getText();
            if (menuTwoText == null || menuOneText == null) {
                return 0;
            }
            return menuOneText.compareTo(menuTwoText);
        }
    }

    static final class AnnotationCombination
    extends AnnotationDesc
    implements Lookup.Provider {
        private AnnotationDesc delegate;
        private String type;
        private LinkedList<AnnotationDesc> list;

        public AnnotationCombination(String type, AnnotationDesc delegate) {
            super(delegate.getOffset(), delegate.getLength());
            this.delegate = delegate;
            this.type = type;
            this.updateAnnotationType();
            this.list = new LinkedList();
            this.list.add(delegate);
        }

        @Override
        public int getOffset() {
            return this.delegate.getOffset();
        }

        @Override
        public int getLine() {
            return this.delegate.getLine();
        }

        @Override
        public String getShortDescription() {
            return this.getAnnotationTypeInstance().getDescription();
        }

        @Override
        public String getAnnotationType() {
            return this.type;
        }

        public void addCombinedAnnotation(AnnotationDesc anno) {
            this.list.add(anno);
        }

        public boolean isAnnotationCombined(AnnotationDesc anno) {
            if (this.list.indexOf(anno) == -1) {
                return false;
            }
            return true;
        }

        List<AnnotationDesc> getCombinedAnnotations() {
            return this.list;
        }

        @Override
        Mark getMark() {
            return this.delegate.getMark();
        }

        public Lookup getLookup() {
            Lookup l = null;
            for (AnnotationDesc ad : this.list) {
                if (!(ad instanceof Lookup.Provider)) continue;
                Lookup adl = ((Lookup.Provider)ad).getLookup();
                if (l == null) {
                    l = adl;
                    continue;
                }
                l = new ProxyLookup(new Lookup[]{l, adl});
            }
            if (l == null) {
                l = Lookups.fixed((Object[])new Object[0]);
            }
            return l;
        }
    }

    public static interface AnnotationsListener
    extends EventListener {
        public void changedLine(int var1);

        public void changedAll();
    }

    public static class LineAnnotations {
        private final LinkedList<AnnotationDesc> annos = new LinkedList();
        private LinkedList<AnnotationDesc> annosVisible = new LinkedList();
        private AnnotationDesc active;

        protected LineAnnotations() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void addAnnotation(AnnotationDesc anno) {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                this.annos.add(anno);
                this.refreshAnnotations();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void removeAnnotation(AnnotationDesc anno) {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                this.annos.remove(anno);
                this.refreshAnnotations();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public AnnotationDesc getActive() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                return this.active;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        AnnotationDesc getType(String type) {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                for (AnnotationDesc ad : this.annosVisible) {
                    if (!ad.getAnnotationType().equals(type)) continue;
                    return ad;
                }
                return null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getLine() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                return this.annos.size() > 0 ? this.annos.peek().getLine() : 0;
            }
        }

        public void setLine(int line) {
            throw new IllegalStateException("Setting of line number not allowed");
        }

        public AnnotationDesc[] getPasive() {
            return this.getPassive();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public AnnotationDesc[] getPassive() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                int startIndex;
                if (this.getCount() <= 1) {
                    return null;
                }
                AnnotationDesc[] pasives = new AnnotationDesc[this.getCount() - 1];
                int index = startIndex = this.annosVisible.indexOf(this.getActive());
                int i = 0;
                do {
                    if (++index >= this.annosVisible.size()) {
                        index = 0;
                    }
                    if (index == startIndex) break;
                    pasives[i] = this.annosVisible.get(index);
                    ++i;
                } while (true);
                return pasives;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean activate(AnnotationDesc anno) {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                int i = this.annosVisible.indexOf(anno);
                if (i == -1) {
                    for (int j = 0; j < this.annosVisible.size(); ++j) {
                        if (!(this.annosVisible.get(j) instanceof AnnotationCombination) || !((AnnotationCombination)this.annosVisible.get(j)).isAnnotationCombined(anno)) continue;
                        i = j;
                        anno = (AnnotationCombination)this.annosVisible.get(j);
                        break;
                    }
                }
                if (i == -1) {
                    return false;
                }
                if (this.annosVisible.get(i) == null) {
                    return false;
                }
                if (anno == this.active || !anno.isVisible()) {
                    return false;
                }
                this.active = anno;
                return true;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public int getCount() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                return this.annosVisible.size();
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public AnnotationDesc activateNext() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                if (this.getCount() <= 1) {
                    return this.active;
                }
                int current = this.annosVisible.indexOf(this.active);
                if (++current >= this.getCount()) {
                    current = 0;
                }
                this.active = this.annosVisible.get(current);
                return this.active;
            }
        }

        private static void fillInCombinationsAndOrderThem(LinkedList<AnnotationType> combTypes) {
            AnnotationTypes types = AnnotationTypes.getTypes();
            Iterator<String> it = types.getAnnotationTypeNames();
            while (it.hasNext()) {
                AnnotationType.CombinationMember[] combs;
                String typeName = it.next();
                AnnotationType type = types.getType(typeName);
                if (type == null || (combs = type.getCombinations()) == null || !type.isWholeLine() || combs.length < 2 && (combs.length != 1 || !combs[0].isAbsorbAll())) continue;
                if (type.getCombinationOrder() == 0) {
                    combTypes.add(type);
                    continue;
                }
                boolean inserted = false;
                for (int i = 0; i < combTypes.size(); ++i) {
                    if (combTypes.get(i).getCombinationOrder() <= type.getCombinationOrder()) continue;
                    combTypes.add(i, type);
                    inserted = true;
                    break;
                }
                if (inserted) continue;
                combTypes.add(type);
            }
        }

        private boolean combineType(AnnotationType combType, LinkedList<AnnotationDesc> annosDupl) {
            int i;
            int countOfAnnos = 0;
            int valid_optional_count = 0;
            LinkedList<AnnotationDesc> combinedAnnos = new LinkedList<AnnotationDesc>();
            AnnotationType.CombinationMember[] combs = combType.getCombinations();
            boolean matchedComb = true;
            for (i = 0; i < combs.length; ++i) {
                AnnotationType.CombinationMember comb = combs[i];
                boolean matchedType = false;
                for (int j = 0; j < annosDupl.size(); ++j) {
                    int k;
                    AnnotationDesc anno = annosDupl.get(j);
                    if (anno == null || !comb.getName().equals(anno.getAnnotationType())) continue;
                    ++countOfAnnos;
                    if (comb.getMinimumCount() == 0) {
                        matchedType = true;
                        ++countOfAnnos;
                        combinedAnnos.add(anno);
                        if (comb.isAbsorbAll()) continue;
                        break;
                    }
                    int requiredCount = comb.getMinimumCount() - 1;
                    for (k = j + 1; k < annosDupl.size() && requiredCount > 0; ++k) {
                        if (annosDupl.get(k) == null || !comb.getName().equals(annosDupl.get(k).getAnnotationType())) continue;
                        --requiredCount;
                    }
                    if (requiredCount != 0) break;
                    matchedType = true;
                    combinedAnnos.add(anno);
                    for (k = j + 1; k < annosDupl.size(); ++k) {
                        if (annosDupl.get(k) == null || !comb.getName().equals(annosDupl.get(k).getAnnotationType())) continue;
                        ++countOfAnnos;
                        combinedAnnos.add(annosDupl.get(k));
                    }
                    break;
                }
                if (matchedType) {
                    if (!comb.isOptional()) continue;
                    ++valid_optional_count;
                    continue;
                }
                if (comb.isOptional()) continue;
                matchedComb = false;
                break;
            }
            if (combType.getMinimumOptionals() > valid_optional_count) {
                matchedComb = false;
            }
            AnnotationCombination annoComb = null;
            if (matchedComb) {
                boolean activateComb = false;
                for (i = 0; i < combinedAnnos.size(); ++i) {
                    if (combinedAnnos.get(i) == this.active) {
                        activateComb = true;
                    }
                    if (annoComb == null) {
                        annoComb = new AnnotationCombination(combType.getName(), (AnnotationDesc)combinedAnnos.get(i));
                        annosDupl.remove(combinedAnnos.get(i));
                        annosDupl.add(annoComb);
                        continue;
                    }
                    annoComb.addCombinedAnnotation((AnnotationDesc)combinedAnnos.get(i));
                    annosDupl.remove(combinedAnnos.get(i));
                }
                if (activateComb) {
                    this.active = annoComb;
                }
                return true;
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void refreshAnnotations() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                if (!AnnotationTypes.getTypes().isCombineGlyphs().booleanValue()) {
                    this.annosVisible = new LinkedList();
                    for (AnnotationDesc ad : this.annos) {
                        if (!ad.isVisible()) continue;
                        this.annosVisible.add(ad);
                    }
                } else {
                    LinkedList<AnnotationDesc> annosDupl = new LinkedList<AnnotationDesc>(this.annos);
                    LinkedList<AnnotationType> combTypes = new LinkedList<AnnotationType>();
                    LineAnnotations.fillInCombinationsAndOrderThem(combTypes);
                    for (int ct = 0; ct < combTypes.size(); ++ct) {
                        this.combineType(combTypes.get(ct), annosDupl);
                    }
                    this.annosVisible = new LinkedList();
                    for (AnnotationDesc ad : annosDupl) {
                        if (!ad.isVisible()) continue;
                        this.annosVisible.add(ad);
                    }
                }
                Collections.sort(this.annosVisible);
                if (this.annosVisible.size() > 0) {
                    this.active = this.annosVisible.get(0);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isMarkStillReferenced(Mark mark) {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                for (AnnotationDesc ad : this.annos) {
                    if (ad.getMark() != mark) continue;
                    return true;
                }
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public Iterator<AnnotationDesc> getAnnotations() {
            LinkedList<AnnotationDesc> linkedList = this.annos;
            synchronized (linkedList) {
                return this.annos.iterator();
            }
        }
    }

    private static final class DelayedMenu
    extends JMenu {
        RequestProcessor.Task task;

        public DelayedMenu(String s) {
            super(s);
        }

        @Override
        public JPopupMenu getPopupMenu() {
            RequestProcessor.Task t = this.task;
            if (t != null && !t.isFinished()) {
                t.waitFinished();
            }
            return super.getPopupMenu();
        }

        void setTask(RequestProcessor.Task task) {
            this.task = task;
        }

        void clearTask() {
            this.task = null;
        }
    }

}

