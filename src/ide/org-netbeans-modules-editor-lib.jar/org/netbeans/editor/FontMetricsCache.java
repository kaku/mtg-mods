/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.util.HashMap;

public class FontMetricsCache {
    private static HashMap font2FM = new HashMap();
    private static HashMap font2Info = new HashMap();

    public static synchronized FontMetrics getFontMetrics(Font f, Component c) {
        Object fm = font2FM.get(f);
        if (fm == null) {
            fm = c.getFontMetrics(f);
            font2FM.put(f, fm);
        }
        return (FontMetrics)fm;
    }

    public static synchronized FontMetrics getFontMetrics(Font f, Graphics g) {
        Object fm = font2FM.get(f);
        if (fm == null) {
            fm = g.getFontMetrics(f);
            font2FM.put(f, fm);
        }
        return (FontMetrics)fm;
    }

    public static synchronized Info getInfo(Font f) {
        Info info = (Info)font2Info.get(f);
        if (info == null) {
            info = new InfoImpl(f);
            font2Info.put(f, info);
        }
        return info;
    }

    public static synchronized void clear() {
        font2FM.clear();
        font2Info.clear();
    }

    private static class InfoImpl
    implements Info {
        private static final int SW_INITED = 1;
        private static final int ST_INITED = 2;
        private static final int UL_INITED = 4;
        private Font font;
        private int inited;
        private int spaceWidth;
        private float strikethroughOffset;
        private float strikethroughThickness;
        private float underlineOffset;
        private float underlineThickness;

        InfoImpl(Font font) {
            this.font = font;
        }

        private synchronized void initSpaceWidth(Graphics g, Component c) {
            FontMetrics fm = g != null ? FontMetricsCache.getFontMetrics(this.font, g) : FontMetricsCache.getFontMetrics(this.font, c);
            this.spaceWidth = fm.stringWidth(" ");
            if (this.spaceWidth <= 0) {
                this.spaceWidth = fm.stringWidth("A") / 3;
            }
            this.inited |= 1;
        }

        private synchronized void initStrikethrough(Graphics g) {
            LineMetrics lm = this.font.getLineMetrics("aAyY", ((Graphics2D)g).getFontRenderContext());
            this.strikethroughOffset = lm.getStrikethroughOffset();
            this.strikethroughThickness = lm.getStrikethroughThickness();
            this.inited |= 2;
        }

        private synchronized void initUnderline(Graphics g) {
            LineMetrics lm = this.font.getLineMetrics("aAyY", ((Graphics2D)g).getFontRenderContext());
            this.underlineOffset = lm.getUnderlineOffset();
            this.underlineThickness = lm.getUnderlineThickness();
            this.inited |= 4;
        }

        @Override
        public int getSpaceWidth(Graphics g) {
            if ((this.inited & 1) == 0) {
                this.initSpaceWidth(g, null);
            }
            return this.spaceWidth;
        }

        @Override
        public int getSpaceWidth(Component c) {
            if ((this.inited & 1) == 0) {
                this.initSpaceWidth(null, c);
            }
            return this.spaceWidth;
        }

        @Override
        public float getStrikethroughOffset(Graphics g) {
            if ((this.inited & 2) == 0) {
                this.initStrikethrough(g);
            }
            return this.strikethroughOffset;
        }

        @Override
        public float getStrikethroughOffset(Component c) {
            if ((this.inited & 2) == 0) {
                this.initStrikethrough(c.getGraphics());
            }
            return this.strikethroughOffset;
        }

        @Override
        public float getStrikethroughThickness(Graphics g) {
            if ((this.inited & 2) == 0) {
                this.initStrikethrough(g);
            }
            return this.strikethroughThickness;
        }

        @Override
        public float getStrikethroughThickness(Component c) {
            if ((this.inited & 2) == 0) {
                this.initStrikethrough(c.getGraphics());
            }
            return this.strikethroughThickness;
        }

        @Override
        public float getUnderlineOffset(Graphics g) {
            if ((this.inited & 4) == 0) {
                this.initUnderline(g);
            }
            return this.underlineOffset;
        }

        @Override
        public float getUnderlineOffset(Component c) {
            if ((this.inited & 4) == 0) {
                this.initUnderline(c.getGraphics());
            }
            return this.underlineOffset;
        }

        @Override
        public float getUnderlineThickness(Graphics g) {
            if ((this.inited & 4) == 0) {
                this.initUnderline(g);
            }
            return this.underlineThickness;
        }

        @Override
        public float getUnderlineThickness(Component c) {
            if ((this.inited & 4) == 0) {
                this.initUnderline(c.getGraphics());
            }
            return this.underlineThickness;
        }
    }

    public static interface Info {
        public int getSpaceWidth(Graphics var1);

        public int getSpaceWidth(Component var1);

        public float getStrikethroughOffset(Graphics var1);

        public float getStrikethroughOffset(Component var1);

        public float getStrikethroughThickness(Graphics var1);

        public float getStrikethroughThickness(Component var1);

        public float getUnderlineOffset(Graphics var1);

        public float getUnderlineOffset(Component var1);

        public float getUnderlineThickness(Graphics var1);

        public float getUnderlineThickness(Component var1);
    }

}

