/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.BadLocationException;

public class GuardedException
extends BadLocationException {
    static final long serialVersionUID = -8139460534188487509L;

    public GuardedException(String s, int offs) {
        super(s, offs);
    }
}

