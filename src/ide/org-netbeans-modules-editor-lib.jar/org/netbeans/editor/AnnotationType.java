/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.ImplementationProvider;
import org.netbeans.editor.Utilities;

public class AnnotationType {
    private static final Logger LOG = Logger.getLogger(AnnotationType.class.getName());
    public static final String PROP_NAME = "name";
    public static final String PROP_DESCRIPTION = "description";
    public static final String PROP_VISIBLE = "visible";
    public static final String PROP_GLYPH_URL = "glyph";
    public static final String PROP_HIGHLIGHT_COLOR = "highlight";
    public static final String PROP_FOREGROUND_COLOR = "foreground";
    public static final String PROP_WAVEUNDERLINE_COLOR = "waveunderline";
    public static final String PROP_WHOLE_LINE = "wholeline";
    public static final String PROP_CONTENT_TYPE = "contenttype";
    public static final String PROP_ACTIONS = "actions";
    public static final String PROP_TOOLTIP_TEXT = "tooltipText";
    public static final String PROP_INHERIT_FOREGROUND_COLOR = "inheritForegroundColor";
    public static final String PROP_USE_HIGHLIGHT_COLOR = "useHighlightColor";
    public static final String PROP_USE_WAVEUNDERLINE_COLOR = "useWaveUnderlineColor";
    public static final String PROP_USE_CUSTOM_SIDEBAR_COLOR = "useCustomSidebarColor";
    public static final String PROP_CUSTOM_SIDEBAR_COLOR = "customSidebarColor";
    public static final String PROP_SEVERITY = "severity";
    public static final String PROP_BROWSEABLE = "browseable";
    public static final String PROP_PRIORITY = "priority";
    public static final String PROP_COMBINATIONS = "combinations";
    public static final String PROP_COMBINATION_ORDER = "combinationOrder";
    public static final String PROP_COMBINATION_MINIMUM_OPTIONALS = "combinationMinimumOptionals";
    public static final String PROP_FILE = "file";
    public static final String PROP_LOCALIZING_BUNDLE = "bundle";
    public static final String PROP_DESCRIPTION_KEY = "desciptionKey";
    public static final String PROP_ACTIONS_FOLDER = "actionsFolder";
    public static final String PROP_COMBINATION_TOOLTIP_TEXT_KEY = "tooltipTextKey";
    private Map properties = new HashMap(20);
    private PropertyChangeSupport support;
    private Image img = null;
    private Coloring col;

    public AnnotationType() {
        this.support = new PropertyChangeSupport(this);
    }

    public URL getGlyph() {
        URL u = (URL)this.getProp("glyph");
        if (u == null) {
            u = AnnotationTypes.getDefaultGlyphURL();
        }
        return u;
    }

    public void setGlyph(URL glyph) {
        this.putProp("glyph", glyph);
    }

    public Image getGlyphImage() {
        if (this.img == null) {
            this.img = Toolkit.getDefaultToolkit().createImage(this.getGlyph());
            final boolean[] waiting = new boolean[]{true};
            if (!Toolkit.getDefaultToolkit().prepareImage(this.img, -1, -1, new ImageObserver(){

                @Override
                public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                    if ((infoflags & 32) == 32) {
                        waiting[0] = false;
                        return false;
                    }
                    return true;
                }
            })) {
                long tm = System.currentTimeMillis();
                while (waiting[0] && System.currentTimeMillis() - tm < 1000) {
                    try {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException e) {}
                }
            }
        }
        return this.img;
    }

    public boolean isDefaultGlyph() {
        if (this.getProp("glyph") == null) {
            return true;
        }
        return false;
    }

    public Color getHighlight() {
        return (Color)this.getProp("highlight");
    }

    public void setHighlight(Color highlight) {
        this.col = null;
        this.putProp("highlight", highlight);
        this.firePropertyChange("highlight", null, null);
        this.processChange();
    }

    public boolean isUseHighlightColor() {
        Boolean b = (Boolean)this.getProp("useHighlightColor");
        if (b == null) {
            return true;
        }
        return b;
    }

    public void setUseHighlightColor(boolean use) {
        if (this.isUseHighlightColor() != use) {
            this.col = null;
            this.putProp("useHighlightColor", use ? Boolean.TRUE : Boolean.FALSE);
            this.firePropertyChange("useHighlightColor", null, null);
            this.processChange();
        }
    }

    public Color getForegroundColor() {
        return (Color)this.getProp("foreground");
    }

    public void setForegroundColor(Color foregroundColor) {
        this.col = null;
        this.putProp("foreground", foregroundColor);
        this.firePropertyChange("foreground", null, null);
        this.processChange();
    }

    public boolean isInheritForegroundColor() {
        Boolean b = (Boolean)this.getProp("inheritForegroundColor");
        if (b == null) {
            return true;
        }
        return b;
    }

    public void setInheritForegroundColor(boolean inherit) {
        if (this.isInheritForegroundColor() != inherit) {
            this.col = null;
            this.putProp("inheritForegroundColor", inherit ? Boolean.TRUE : Boolean.FALSE);
            this.firePropertyChange("inheritForegroundColor", null, null);
            this.processChange();
        }
    }

    public Color getWaveUnderlineColor() {
        return (Color)this.getProp("waveunderline");
    }

    public void setWaveUnderlineColor(Color waveunderline) {
        this.col = null;
        this.putProp("waveunderline", waveunderline);
        this.firePropertyChange("waveunderline", null, null);
        this.processChange();
    }

    public boolean isUseWaveUnderlineColor() {
        Boolean b = (Boolean)this.getProp("useWaveUnderlineColor");
        if (b == null) {
            return true;
        }
        return b;
    }

    public void setUseWaveUnderlineColor(boolean use) {
        if (this.isUseWaveUnderlineColor() != use) {
            this.col = null;
            this.putProp("useWaveUnderlineColor", use ? Boolean.TRUE : Boolean.FALSE);
            this.firePropertyChange("useWaveUnderlineColor", null, null);
            this.processChange();
        }
    }

    private void processChange() {
        if (this.getProp("file") == null) {
            return;
        }
        AnnotationTypes.getTypes().saveType(this);
    }

    public Coloring getColoring() {
        if (this.col == null) {
            this.col = new Coloring(null, 7, this.isInheritForegroundColor() ? null : this.getForegroundColor(), this.isUseHighlightColor() ? this.getHighlight() : null, null, null, this.isUseWaveUnderlineColor() ? this.getWaveUnderlineColor() : null);
        }
        return this.col;
    }

    public Action[] getActions() {
        return (Action[])this.getProp("actions");
    }

    public void setActions(Action[] actions) {
        this.putProp("actions", actions);
    }

    public CombinationMember[] getCombinations() {
        return (CombinationMember[])this.getProp("combinations");
    }

    public void setCombinations(CombinationMember[] combs) {
        this.putProp("combinations", combs);
    }

    public String getName() {
        return (String)this.getProp("name");
    }

    public void setName(String name) {
        this.putProp("name", name);
    }

    public String getDescription() {
        String desc = (String)this.getProp("description");
        if (desc == null) {
            String localizer = (String)this.getProp("bundle");
            String key = (String)this.getProp("desciptionKey");
            if (localizer != null && key != null) {
                try {
                    ResourceBundle bundle = ImplementationProvider.getDefault().getResourceBundle(localizer);
                    desc = bundle.getString(key);
                }
                catch (MissingResourceException mre) {
                    if (LOG.isLoggable(Level.WARNING)) {
                        LOG.warning("Can't find '" + key + "' in " + localizer + " for AnnotationType '" + this.getName() + "'.");
                    }
                    desc = key;
                }
            }
            this.setDescription(desc);
        }
        if (desc == null) {
            desc = (String)this.getProp("name");
        }
        if (desc == null) {
            desc = "null";
        }
        return desc;
    }

    public void setDescription(String name) {
        this.putProp("description", name);
    }

    public String getTooltipText() {
        String text = (String)this.getProp("tooltipText");
        if (text == null) {
            String localizer = (String)this.getProp("bundle");
            String key = (String)this.getProp("tooltipTextKey");
            ResourceBundle bundle = ImplementationProvider.getDefault().getResourceBundle(localizer);
            text = bundle.getString(key);
            this.setTooltipText(text);
        }
        return text;
    }

    public void setTooltipText(String text) {
        this.putProp("tooltipText", text);
    }

    public int getCombinationOrder() {
        if (this.getProp("combinationOrder") == null) {
            return 0;
        }
        return (Integer)this.getProp("combinationOrder");
    }

    public void setCombinationOrder(int order) {
        this.putProp("combinationOrder", new Integer(order));
    }

    public void setCombinationOrder(String ord) {
        int order;
        try {
            order = Integer.parseInt(ord);
        }
        catch (NumberFormatException ex) {
            Utilities.annotateLoggable(ex);
            return;
        }
        this.putProp("combinationOrder", new Integer(order));
    }

    public int getMinimumOptionals() {
        if (this.getProp("combinationMinimumOptionals") == null) {
            return 0;
        }
        return (Integer)this.getProp("combinationMinimumOptionals");
    }

    public void setMinimumOptionals(int min) {
        this.putProp("combinationMinimumOptionals", new Integer(min));
    }

    public void setMinimumOptionals(String m) {
        int min;
        try {
            min = Integer.parseInt(m);
        }
        catch (NumberFormatException ex) {
            Utilities.annotateLoggable(ex);
            return;
        }
        this.putProp("combinationMinimumOptionals", new Integer(min));
    }

    public boolean isVisible() {
        Boolean b = (Boolean)this.getProp("visible");
        if (b == null) {
            return false;
        }
        return b;
    }

    public void setVisible(boolean vis) {
        this.putProp("visible", vis ? Boolean.TRUE : Boolean.FALSE);
    }

    public void setVisible(String vis) {
        this.putProp("visible", Boolean.valueOf(vis));
    }

    public boolean isWholeLine() {
        Boolean b = (Boolean)this.getProp("wholeline");
        if (b == null) {
            return true;
        }
        return b;
    }

    public void setWholeLine(boolean wl) {
        this.putProp("wholeline", wl ? Boolean.TRUE : Boolean.FALSE);
    }

    public void setWholeLine(String wl) {
        this.putProp("wholeline", Boolean.valueOf(wl));
    }

    public String getContentType() {
        return (String)this.getProp("contenttype");
    }

    public void setContentType(String ct) {
        this.putProp("contenttype", ct);
    }

    public boolean isUseCustomSidebarColor() {
        return (Boolean)this.getProp("useCustomSidebarColor");
    }

    public void setUseCustomSidebarColor(boolean value) {
        this.putProp("useCustomSidebarColor", value);
    }

    public Color getCustomSidebarColor() {
        return (Color)this.getProp("customSidebarColor");
    }

    public void setCustomSidebarColor(Color customSidebarColor) {
        this.putProp("customSidebarColor", customSidebarColor);
    }

    public Severity getSeverity() {
        return (Severity)this.getProp("severity");
    }

    public void setSeverity(Severity severity) {
        this.putProp("severity", severity);
    }

    public int getPriority() {
        return (Integer)this.getProp("priority");
    }

    public void setPriority(int priority) {
        this.putProp("priority", new Integer(priority));
    }

    public boolean isBrowseable() {
        return (Boolean)this.getProp("browseable");
    }

    public void setBrowseable(boolean browseable) {
        this.putProp("browseable", browseable);
    }

    public Object getProp(String prop) {
        return this.properties.get(prop);
    }

    public void putProp(Object key, Object value) {
        if (value == null) {
            this.properties.remove(key);
            return;
        }
        this.properties.put(key, value);
    }

    public String toString() {
        return "AnnotationType: name='" + this.getName() + "', description='" + this.getDescription() + "', visible=" + this.isVisible() + ", wholeline=" + this.isWholeLine() + ", glyph=" + this.getGlyph() + ", highlight=" + this.getHighlight() + ", foreground=" + this.getForegroundColor() + "', inheritForeground=" + this.isInheritForegroundColor() + ", contenttype=" + this.getContentType();
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.support.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.support.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.support.firePropertyChange(propertyName, oldValue, newValue);
    }

    public static final class Severity
    implements Comparable {
        private static final int STATUS_NONE_NUMBER = 0;
        private static final int STATUS_OK_NUMBER = 1;
        private static final int STATUS_WARNING_NUMBER = 2;
        private static final int STATUS_ERROR_NUMBER = 3;
        public static final Severity STATUS_NONE = new Severity(0);
        public static final Severity STATUS_OK = new Severity(1);
        public static final Severity STATUS_WARNING = new Severity(2);
        public static final Severity STATUS_ERROR = new Severity(3);
        private static final Severity[] VALUES = new Severity[]{STATUS_NONE, STATUS_OK, STATUS_WARNING, STATUS_ERROR};
        private static final Color[] DEFAULT_STATUS_COLORS = new Color[]{Color.WHITE, Color.GREEN, Color.YELLOW, Color.RED};
        private int status;
        private static String[] STATUS_NAMES = new String[]{"none", "ok", "warning", "error"};

        private Severity(int status) throws IllegalArgumentException {
            if (status != 0 && status != 3 && status != 2 && status != 1) {
                throw new IllegalArgumentException("Invalid status provided: " + status);
            }
            this.status = status;
        }

        private int getStatus() {
            return this.status;
        }

        public int compareTo(Object o) {
            Severity remote = (Severity)o;
            if (this.status > remote.status) {
                return 1;
            }
            if (this.status < remote.status) {
                return -1;
            }
            return 0;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Severity)) {
                return false;
            }
            Severity remote = (Severity)o;
            return this.status == remote.status;
        }

        public int hashCode() {
            return 43 ^ this.status;
        }

        public String toString() {
            return "[Status: " + STATUS_NAMES[this.getStatus()] + "]";
        }

        public static Severity getCompoundStatus(Severity first, Severity second) throws IllegalArgumentException {
            if (first != STATUS_ERROR && first != STATUS_WARNING && first != STATUS_OK) {
                throw new IllegalArgumentException("Invalid status provided: " + first);
            }
            if (second != STATUS_ERROR && second != STATUS_WARNING && second != STATUS_OK) {
                throw new IllegalArgumentException("Invalid status provided: " + second);
            }
            return VALUES[Math.max(first.getStatus(), second.getStatus())];
        }

        public static Color getDefaultColor(Severity s) {
            return DEFAULT_STATUS_COLORS[s.getStatus()];
        }

        public static Severity valueOf(String severity) {
            Severity severityValue = STATUS_NONE;
            if (severity != null) {
                if ("ok".equals(severity)) {
                    severityValue = STATUS_OK;
                } else if ("warning".equals(severity)) {
                    severityValue = STATUS_WARNING;
                } else if ("error".equals(severity)) {
                    severityValue = STATUS_ERROR;
                }
            }
            return severityValue;
        }

        public String getName() {
            return STATUS_NAMES[this.status];
        }
    }

    public static final class CombinationMember {
        private String type;
        private boolean absorbAll;
        private boolean optional;
        private int minimumCount;

        public CombinationMember(String type, boolean absorbAll, boolean optional, int minimumCount) {
            this.type = type;
            this.absorbAll = absorbAll;
            this.optional = optional;
            this.minimumCount = minimumCount;
        }

        public CombinationMember(String type, boolean absorbAll, boolean optional, String minimumCount) {
            this.type = type;
            this.absorbAll = absorbAll;
            this.optional = optional;
            if (minimumCount != null && minimumCount.length() > 0) {
                try {
                    this.minimumCount = Integer.parseInt(minimumCount);
                }
                catch (NumberFormatException ex) {
                    Utilities.annotateLoggable(ex);
                    this.minimumCount = 0;
                }
            } else {
                this.minimumCount = 0;
            }
        }

        public String getName() {
            return this.type;
        }

        public boolean isAbsorbAll() {
            return this.absorbAll;
        }

        public boolean isOptional() {
            return this.optional;
        }

        public int getMinimumCount() {
            return this.minimumCount;
        }
    }

}

