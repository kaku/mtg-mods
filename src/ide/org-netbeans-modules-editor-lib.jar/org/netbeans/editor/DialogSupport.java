/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.DialogFactory
 *  org.netbeans.modules.editor.lib2.DialogSupport
 */
package org.netbeans.editor;

import java.awt.Dialog;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

public class DialogSupport {
    private DialogSupport() {
    }

    public static Dialog createDialog(String title, JPanel panel, boolean modal, JButton[] buttons, boolean sidebuttons, int defaultIndex, int cancelIndex, ActionListener listener) {
        return org.netbeans.modules.editor.lib2.DialogSupport.getInstance().createDialog(title, panel, modal, buttons, sidebuttons, defaultIndex, cancelIndex, listener);
    }

    public static void setDialogFactory(DialogFactory factory) {
        org.netbeans.modules.editor.lib2.DialogSupport.getInstance().setExternalDialogFactory((org.netbeans.modules.editor.lib2.DialogFactory)new Wrapper(factory));
    }

    private static final class Wrapper
    implements org.netbeans.modules.editor.lib2.DialogFactory {
        private DialogFactory origFactory;

        public Wrapper(DialogFactory origFactory) {
            this.origFactory = origFactory;
        }

        public Dialog createDialog(String title, JPanel panel, boolean modal, JButton[] buttons, boolean sidebuttons, int defaultIndex, int cancelIndex, ActionListener listener) {
            return this.origFactory.createDialog(title, panel, modal, buttons, sidebuttons, defaultIndex, cancelIndex, listener);
        }
    }

    public static interface DialogFactory {
        public Dialog createDialog(String var1, JPanel var2, boolean var3, JButton[] var4, boolean var5, int var6, int var7, ActionListener var8);
    }

}

