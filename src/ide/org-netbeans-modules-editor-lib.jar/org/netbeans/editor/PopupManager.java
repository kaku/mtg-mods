/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.editor;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ToolTipSupport;
import org.openide.util.Parameters;

public class PopupManager {
    private static final Logger LOG = Logger.getLogger(PopupManager.class.getName());
    private JComponent popup = null;
    private final JTextComponent textComponent;
    public static final Placement Above = new Placement("Above");
    public static final Placement Below = new Placement("Below");
    public static final Placement Largest = new Placement("Largest");
    public static final Placement AbovePreferred = new Placement("AbovePreferred");
    public static final Placement BelowPreferred = new Placement("BelowPreferred");
    public static final Placement FixedPoint = new Placement("TopLeft");
    public static final HorizontalBounds ViewPortBounds = new HorizontalBounds("ViewPort");
    public static final HorizontalBounds ScrollBarBounds = new HorizontalBounds("ScrollBar");
    private final KeyListener keyListener;
    private final TextComponentListener componentListener;

    public PopupManager(JTextComponent textComponent) {
        this.textComponent = textComponent;
        this.keyListener = new PopupKeyListener();
        textComponent.addKeyListener(this.keyListener);
        this.componentListener = new TextComponentListener();
        textComponent.addComponentListener(this.componentListener);
    }

    public void install(JComponent popup) {
        if (this.textComponent == null) {
            return;
        }
        int caretPos = this.textComponent.getCaret().getDot();
        try {
            Rectangle caretBounds = this.textComponent.modelToView(caretPos);
            this.install(popup, caretBounds, Largest);
        }
        catch (BadLocationException e) {
            // empty catch block
        }
    }

    public void uninstall(JComponent popup) {
        JComponent oldPopup = this.popup;
        if (oldPopup != null) {
            if (oldPopup.isVisible()) {
                oldPopup.setVisible(false);
            }
            this.removeFromRootPane(oldPopup);
            this.popup = null;
        }
        if (popup != null && popup != oldPopup) {
            if (popup.isVisible()) {
                popup.setVisible(false);
            }
            this.removeFromRootPane(popup);
        }
    }

    public void install(JComponent popup, Rectangle cursorBounds, Placement placement, HorizontalBounds horizontalBounds, int horizontalAdjustment, int verticalAdjustment) {
        if (this.popup != null && this.popup != popup) {
            this.uninstall(null);
        }
        assert (this.popup == null || this.popup == popup);
        if (popup != null) {
            if (this.popup == null) {
                this.popup = popup;
                this.installToRootPane(this.popup);
            }
            Rectangle bounds = PopupManager.computeBounds(this.popup, this.textComponent, cursorBounds, placement, horizontalBounds);
            LOG.log(Level.FINE, "computed-bounds={0}", bounds);
            if (bounds != null) {
                JRootPane rp;
                if (horizontalBounds == ScrollBarBounds && placement != FixedPoint) {
                    bounds.x = 0;
                }
                if ((rp = this.textComponent.getRootPane()) != null) {
                    bounds = SwingUtilities.convertRectangle(this.textComponent, bounds, rp.getLayeredPane());
                    if (bounds.y < 0) {
                        bounds.y = 0;
                    }
                }
                if (horizontalBounds == ScrollBarBounds && this.textComponent.getParent() instanceof JViewport) {
                    int shift = this.textComponent.getParent().getX();
                    Rectangle viewBounds = ((JViewport)this.textComponent.getParent()).getViewRect();
                    bounds.x += viewBounds.x;
                    bounds.x -= shift;
                    bounds.width += shift;
                }
                bounds.x += horizontalAdjustment;
                bounds.y += verticalAdjustment;
                bounds.width -= horizontalAdjustment;
                bounds.height -= verticalAdjustment;
                LOG.log(Level.FINE, "setting bounds={0} on {1}", new Object[]{bounds, this.popup});
                this.popup.setBounds(bounds);
            } else {
                this.popup.setVisible(false);
            }
        }
    }

    public void install(JComponent popup, Rectangle cursorBounds, Placement placement, HorizontalBounds horizontalBounds) {
        this.install(popup, cursorBounds, placement, horizontalBounds, 0, 0);
    }

    public void install(JComponent popup, Rectangle cursorBounds, Placement placement) {
        this.install(popup, cursorBounds, placement, ViewPortBounds);
    }

    public JComponent get() {
        return this.popup;
    }

    private void installToRootPane(JComponent c) {
        JRootPane rp = this.textComponent.getRootPane();
        if (rp != null) {
            rp.getLayeredPane().add(c, JLayeredPane.POPUP_LAYER, 0);
        }
    }

    private void removeFromRootPane(JComponent c) {
        JRootPane rp = c.getRootPane();
        if (rp != null) {
            rp.getLayeredPane().remove(c);
        }
    }

    protected static Rectangle computeBounds(JComponent popup, JComponent view, Rectangle cursorBounds, Placement placement, HorizontalBounds horizontalBounds) {
        Rectangle ret;
        Container viewParent;
        if (horizontalBounds == null) {
            horizontalBounds = ViewPortBounds;
        }
        if ((viewParent = view.getParent()) instanceof JViewport) {
            Rectangle viewBounds = ((JViewport)viewParent).getViewRect();
            Rectangle translatedCursorBounds = (Rectangle)cursorBounds.clone();
            if (placement != FixedPoint) {
                translatedCursorBounds.translate(- viewBounds.x, - viewBounds.y);
            }
            if ((ret = PopupManager.computeBounds(popup, viewBounds.width, viewBounds.height, translatedCursorBounds, placement, horizontalBounds)) != null) {
                ret.translate(viewBounds.x, viewBounds.y);
            }
        } else {
            ret = PopupManager.computeBounds(popup, view.getWidth(), view.getHeight(), cursorBounds, placement);
        }
        return ret;
    }

    protected static Rectangle computeBounds(JComponent popup, JComponent view, Rectangle cursorBounds, Placement placement) {
        return PopupManager.computeBounds(popup, view, cursorBounds, placement, ViewPortBounds);
    }

    protected static Rectangle computeBounds(JComponent popup, int viewWidth, int viewHeight, Rectangle cursorBounds, Placement originalPlacement, HorizontalBounds horizontalBounds) {
        Placement updatedPlacement;
        Parameters.notNull((CharSequence)"popup", (Object)popup);
        Parameters.notNull((CharSequence)"cursorBounds", (Object)cursorBounds);
        Parameters.notNull((CharSequence)"originalPlacement", (Object)originalPlacement);
        int aboveCursorHeight = cursorBounds.y;
        int belowCursorY = cursorBounds.y + cursorBounds.height;
        int belowCursorHeight = viewHeight - belowCursorY;
        Dimension prefSize = popup.getPreferredSize();
        int width = Math.min(viewWidth, prefSize.width);
        popup.setSize(width, Integer.MAX_VALUE);
        prefSize = popup.getPreferredSize();
        Placement placement = PopupManager.determinePlacement(originalPlacement, prefSize, aboveCursorHeight, belowCursorHeight);
        Rectangle popupBounds = null;
        do {
            popup.putClientProperty(Placement.class, placement);
            int maxHeight = placement == Above || placement == AbovePreferred ? aboveCursorHeight : belowCursorHeight;
            int height = Math.min(prefSize.height, maxHeight);
            popup.setSize(width, height);
            popupBounds = popup.getBounds();
            updatedPlacement = (Placement)popup.getClientProperty(Placement.class);
            if (updatedPlacement == placement) break;
            if (placement == AbovePreferred && updatedPlacement == null) {
                placement = Below;
                continue;
            }
            if (placement != BelowPreferred || updatedPlacement != null) break;
            placement = Above;
        } while (true);
        if (updatedPlacement == null) {
            popupBounds = null;
        }
        if (popupBounds != null) {
            if (placement == FixedPoint) {
                popupBounds.x = cursorBounds.x;
                popupBounds.y = cursorBounds.y;
            } else {
                popupBounds.x = Math.min(cursorBounds.x, viewWidth - popupBounds.width);
                popupBounds.y = placement == Above || placement == AbovePreferred ? aboveCursorHeight - popupBounds.height : belowCursorY;
            }
        }
        return popupBounds;
    }

    protected static Rectangle computeBounds(JComponent popup, int viewWidth, int viewHeight, Rectangle cursorBounds, Placement placement) {
        return PopupManager.computeBounds(popup, viewWidth, viewHeight, cursorBounds, placement, ViewPortBounds);
    }

    private static Placement determinePlacement(Placement placement, Dimension prefSize, int aboveCursorHeight, int belowCursorHeight) {
        if (placement == AbovePreferred) {
            placement = prefSize.height <= aboveCursorHeight ? Above : Largest;
        } else if (placement == BelowPreferred) {
            Placement placement2 = placement = prefSize.height <= belowCursorHeight ? Below : Largest;
        }
        if (placement == Largest) {
            placement = aboveCursorHeight < belowCursorHeight ? Below : Above;
        }
        return placement;
    }

    public static final class HorizontalBounds {
        private final String representation;

        private HorizontalBounds(String representation) {
            this.representation = representation;
        }

        public String toString() {
            return this.representation;
        }
    }

    public static final class Placement {
        private final String representation;

        private Placement(String representation) {
            this.representation = representation;
        }

        public String toString() {
            return this.representation;
        }
    }

    private final class TextComponentListener
    extends ComponentAdapter {
        private TextComponentListener() {
        }

        @Override
        public void componentHidden(ComponentEvent evt) {
            PopupManager.this.install(null);
        }
    }

    private final class PopupKeyListener
    implements KeyListener {
        private PopupKeyListener() {
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e != null && PopupManager.this.popup != null && PopupManager.this.popup.isShowing()) {
                ActionMap am = PopupManager.this.popup.getActionMap();
                InputMap im = PopupManager.this.popup.getInputMap();
                KeyStroke ks = KeyStroke.getKeyStrokeForEvent(e);
                Object obj = im.get(ks);
                LOG.log(Level.FINE, "Keystroke for event {0}: {1}; action-map-key={2}", new Object[]{e, ks, obj});
                if (obj != null && !obj.equals("tooltip-no-action") && obj.equals("tooltip-hide-action")) {
                    Action action = am.get(obj);
                    Object[] arrobject = new Object[2];
                    arrobject[0] = action;
                    arrobject[1] = action != null ? action.getValue("Name") : null;
                    LOG.log(Level.FINE, "Popup component''s action: {0}, {1}", arrobject);
                    if (action != null) {
                        action.actionPerformed(null);
                        e.consume();
                        return;
                    }
                }
                if (e.getKeyCode() != 17 && e.getKeyCode() != 16 && e.getKeyCode() != 18 && e.getKeyCode() != 65406 && e.getKeyCode() != 157) {
                    Utilities.getEditorUI(PopupManager.this.textComponent).getToolTipSupport().setToolTipVisible(false);
                }
            }
        }
    }

}

