/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.io.PrintStream;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.modules.editor.lib.drawing.DrawGraphics;

public abstract class BaseView
extends View {
    protected static final int INSETS_TOP = 1;
    protected static final int MAIN_AREA = 2;
    protected static final int INSETS_BOTTOM = 4;
    protected boolean packed;
    protected int helperInd;
    private JTextComponent component;
    protected Insets insets;
    private int startY = -1;

    BaseView(Element elem) {
        super(elem);
    }

    public boolean isPacked() {
        return this.packed;
    }

    public void setPacked(boolean packed) {
        this.packed = packed;
    }

    @Override
    public float getAlignment(int axis) {
        return 0.0f;
    }

    abstract void modelToViewDG(int var1, DrawGraphics var2) throws BadLocationException;

    protected abstract int getYFromPos(int var1) throws BadLocationException;

    protected abstract int getPosFromY(int var1);

    protected abstract int getBaseX(int var1);

    protected abstract int getPaintAreas(Graphics var1, int var2, int var3);

    protected abstract void paintAreas(Graphics var1, int var2, int var3, int var4);

    @Override
    public void paint(Graphics g, Shape allocation) {
        Rectangle clip = g.getClipBounds();
        if (clip.height < 0 || clip.width < 0) {
            return;
        }
        int paintAreas = this.getPaintAreas(g, clip.y, clip.height);
        if (paintAreas != 0) {
            this.paintAreas(g, clip.y, clip.height, paintAreas);
        }
    }

    public JTextComponent getComponent() {
        if (this.component == null) {
            this.component = (JTextComponent)this.getContainer();
        }
        return this.component;
    }

    public Insets getInsets() {
        return this.insets;
    }

    protected void setHelperInd(int ind) {
        this.helperInd = ind;
    }

    protected abstract int getViewStartY(BaseView var1, int var2);

    protected void invalidateStartY() {
        this.startY = -1;
    }

    protected int getStartY() {
        BaseView v;
        if (this.startY == -1 && (v = (BaseView)this.getParent()) != null) {
            this.startY = v.getViewStartY(this, this.helperInd);
        }
        return this.startY;
    }

    public abstract int getHeight();

    public abstract void updateMainHeight();

    @Override
    public float getPreferredSpan(int axis) {
        switch (axis) {
            case 1: {
                return this.getHeight();
            }
        }
        return 0.0f;
    }

    protected EditorUI getEditorUI() {
        return ((BaseTextUI)this.getComponent().getUI()).getEditorUI();
    }

    public void displayHierarchy() {
        BaseView v = this;
        while (v.getParent() != null) {
            v = (BaseView)v.getParent();
        }
        v.displayHierarchyHelper(this, 0, 0);
    }

    private void displayHierarchyHelper(View origView, int col, int index) {
        StringBuffer buf = new StringBuffer();
        buf.append(this == origView ? "*" : " ");
        for (int i = 0; i < col; ++i) {
            buf.append(' ');
        }
        buf.append('[');
        buf.append(Integer.toString(index));
        buf.append("] ");
        buf.append(this.toString());
        System.out.println(buf);
        int childrenCnt = this.getViewCount();
        if (childrenCnt > 0) {
            for (int i2 = 0; i2 < childrenCnt; ++i2) {
                ((BaseView)this.getView(i2)).displayHierarchyHelper(origView, col + 1, i2);
            }
        }
    }

    public String toString() {
        return "BaseView=" + System.identityHashCode(this) + ", elem=" + this.getElement() + ", parent=" + System.identityHashCode(this.getParent());
    }
}

