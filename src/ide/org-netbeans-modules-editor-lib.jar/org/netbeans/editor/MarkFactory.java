/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.Position;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.TokenItem;

public class MarkFactory {
    private MarkFactory() {
    }

    public static class ContextMark
    extends Mark {
        public ContextMark(boolean stayBOL) {
            this(false, stayBOL);
        }

        public ContextMark(boolean insertAfter, boolean stayBOL) {
            this(insertAfter ? Position.Bias.Backward : Position.Bias.Forward, stayBOL);
        }

        public ContextMark(Position.Bias bias, boolean stayBOL) {
            super(bias);
        }
    }

    public static class SyntaxMark
    extends Mark {
        private Syntax.StateInfo stateInfo;
        private TokenItem tokenItem;

        public Syntax.StateInfo getStateInfo() {
            return this.stateInfo;
        }

        public void updateStateInfo(Syntax syntax) {
            if (this.stateInfo == null) {
                this.stateInfo = syntax.createStateInfo();
            }
            syntax.storeState(this.stateInfo);
        }

        void setStateInfo(Syntax.StateInfo stateInfo) {
            this.stateInfo = stateInfo;
        }

        public TokenItem getTokenItem() {
            return this.tokenItem;
        }

        void setTokenItem(TokenItem tokenItem) {
            this.tokenItem = tokenItem;
        }

        @Override
        protected void removeUpdateAction(int pos, int len) {
            try {
                this.remove();
            }
            catch (InvalidMarkException e) {
                // empty catch block
            }
        }
    }

}

