/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.Reformat
 */
package org.netbeans.editor;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Action;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.Acceptor;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.Reformat;

public class Abbrev
implements PropertyChangeListener {
    private StringBuffer abbrevSB = new StringBuffer();
    private boolean checkDocText;
    private boolean checkTextDelimiter;
    protected EditorUI editorUI;
    private Acceptor doExpandAcceptor;
    private Acceptor addTypedAcceptor;
    private Acceptor resetAcceptor;
    private HashMap abbrevMap;

    public static boolean isAbbrevDisabled(JTextComponent component) {
        BaseDocument bdoc;
        SyntaxSupport sup;
        Document doc = component.getDocument();
        if (doc instanceof BaseDocument && (sup = (bdoc = (BaseDocument)doc).getSyntaxSupport()) != null) {
            return sup.isAbbrevDisabled(component.getCaretPosition());
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Abbrev(EditorUI editorUI, boolean checkDocText, boolean checkTextDelimiter) {
        this.editorUI = editorUI;
        this.checkDocText = checkDocText;
        this.checkTextDelimiter = checkTextDelimiter;
        Object object = editorUI.getComponentLock();
        synchronized (object) {
            JTextComponent component = editorUI.getComponent();
            if (component != null) {
                this.propertyChange(new PropertyChangeEvent(editorUI, "component", null, component));
            }
            editorUI.addPropertyChangeListener(this);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JTextComponent component;
        String propName = evt.getPropertyName();
        if (!"component".equals(propName) || (component = (JTextComponent)evt.getNewValue()) != null) {
            // empty if block
        }
    }

    public void reset() {
        this.abbrevSB.setLength(0);
    }

    public void addChar(char ch) {
        this.abbrevSB.append(ch);
    }

    public String getAbbrevString() {
        return this.abbrevSB.toString();
    }

    public Map getAbbrevMap() {
        return this.abbrevMap;
    }

    public Object translateAbbrev(String abbrev) {
        String abbStr = abbrev != null ? abbrev : this.abbrevSB.toString();
        return this.getAbbrevMap().get(abbStr);
    }

    public String getExpandString(char typedChar) {
        return this.doExpandAcceptor.accept(typedChar) ? this.getExpandString() : null;
    }

    public String getExpandString() {
        BaseDocument doc = this.editorUI.getDocument();
        String abbrevStr = this.getAbbrevString();
        int abbrevStrLen = abbrevStr.length();
        Object expansion = this.translateAbbrev(abbrevStr);
        Caret caret = this.editorUI.getComponent().getCaret();
        int dotPos = caret.getDot();
        if (abbrevStr != null && expansion != null && dotPos >= abbrevStrLen && this.checkDocText) {
            try {
                CharSequence prevChars = DocumentUtilities.getText((Document)doc, (int)(dotPos - abbrevStrLen), (int)abbrevStrLen);
                if (CharSequenceUtilities.textEquals((CharSequence)prevChars, (CharSequence)abbrevStr) && (!this.checkTextDelimiter || dotPos == abbrevStrLen || this.resetAcceptor.accept(doc.getChars(dotPos - abbrevStrLen - 1, 1)[0]))) {
                    return abbrevStr;
                }
            }
            catch (BadLocationException e) {
                // empty catch block
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected boolean doExpansion(int dotPos, String expandStr, ActionEvent evt) throws BadLocationException {
        Object expansion = this.translateAbbrev(expandStr);
        boolean expanded = false;
        if (expansion instanceof String) {
            BaseDocument doc = this.editorUI.getDocument();
            String ins = (String)expansion;
            int offset = ins.indexOf(124);
            if (offset >= 0) {
                if (offset > 0) {
                    doc.insertString(dotPos, ins.substring(0, offset), null);
                }
                if (offset + 1 < ins.length()) {
                    doc.insertString(dotPos + offset, ins.substring(offset + 1), null);
                }
                Caret caret = this.editorUI.getComponent().getCaret();
                caret.setDot(dotPos + offset);
            } else {
                doc.insertString(dotPos, ins, null);
            }
            if (ins.indexOf("\n") != -1) {
                Reformat formatter = Reformat.get((Document)doc);
                formatter.lock();
                try {
                    doc.atomicLock();
                    try {
                        formatter.reformat(dotPos, dotPos + ins.length());
                    }
                    finally {
                        doc.atomicUnlock();
                    }
                }
                finally {
                    formatter.unlock();
                }
            }
            expanded = true;
        } else if (expansion instanceof Action) {
            ((Action)expansion).actionPerformed(evt);
            expanded = true;
        }
        return expanded;
    }

    public boolean expandString(char typedChar, String expandStr, ActionEvent evt) throws BadLocationException {
        if (this.expandString(expandStr, evt)) {
            if (this.addTypedAcceptor.accept(typedChar)) {
                int dotPos = this.editorUI.getComponent().getCaret().getDot();
                this.editorUI.getDocument().insertString(dotPos, String.valueOf(typedChar), null);
            }
            return true;
        }
        return false;
    }

    public boolean expandString(String expandStr, ActionEvent evt) throws BadLocationException {
        this.reset();
        return true;
    }

    public boolean checkReset(char typedChar) {
        if (this.resetAcceptor.accept(typedChar)) {
            this.reset();
            return true;
        }
        return false;
    }

    public boolean checkAndExpand(char typedChar, ActionEvent evt) throws BadLocationException {
        boolean doInsert = true;
        boolean disableAbbrev = false;
        JTextComponent component = this.editorUI.getComponent();
        Document doc = component.getDocument();
        if (doc instanceof BaseDocument) {
            BaseDocument bdoc = (BaseDocument)doc;
            SyntaxSupport sup = bdoc.getSyntaxSupport();
            disableAbbrev = sup.isAbbrevDisabled(component.getCaretPosition());
        }
        if (disableAbbrev) {
            this.reset();
        } else {
            String expandStr = this.getExpandString(typedChar);
            if (expandStr != null) {
                doInsert = false;
                this.expandString(typedChar, expandStr, evt);
            } else {
                this.addChar(typedChar);
            }
            this.checkReset(typedChar);
        }
        return doInsert;
    }

    public void checkAndExpand(ActionEvent evt) throws BadLocationException {
        String expandStr = this.getExpandString();
        if (expandStr != null) {
            this.expandString(expandStr, evt);
        }
    }

}

