/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.EditorDebug;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;

public interface TokenItem {
    public TokenID getTokenID();

    public TokenContextPath getTokenContextPath();

    public int getOffset();

    public String getImage();

    public TokenItem getNext();

    public TokenItem getPrevious();

    public static class FilterItem
    implements TokenItem {
        protected TokenItem delegate;

        public FilterItem(TokenItem delegate) {
            this.delegate = delegate;
        }

        @Override
        public TokenID getTokenID() {
            return this.delegate.getTokenID();
        }

        @Override
        public TokenContextPath getTokenContextPath() {
            return this.delegate.getTokenContextPath();
        }

        @Override
        public int getOffset() {
            return this.delegate.getOffset();
        }

        @Override
        public String getImage() {
            return this.delegate.getImage();
        }

        @Override
        public TokenItem getNext() {
            return this.delegate.getNext();
        }

        @Override
        public TokenItem getPrevious() {
            return this.delegate.getPrevious();
        }

        public String toString() {
            return this.delegate.toString();
        }
    }

    public static abstract class AbstractItem
    implements TokenItem {
        private TokenID tokenID;
        private TokenContextPath tokenContextPath;
        private String image;
        private int offset;

        public AbstractItem(TokenID tokenID, TokenContextPath tokenContextPath, int offset, String image) {
            this.tokenID = tokenID;
            this.tokenContextPath = tokenContextPath;
            this.offset = offset;
            this.image = image;
        }

        @Override
        public TokenID getTokenID() {
            return this.tokenID;
        }

        @Override
        public TokenContextPath getTokenContextPath() {
            return this.tokenContextPath;
        }

        @Override
        public int getOffset() {
            return this.offset;
        }

        @Override
        public String getImage() {
            return this.image;
        }

        public String toString() {
            return "'" + EditorDebug.debugString(this.getImage()) + "', tokenID=" + this.getTokenID() + ", tcp=" + this.getTokenContextPath() + ", offset=" + this.getOffset();
        }
    }

}

