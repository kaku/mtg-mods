/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.util.Comparator;
import org.netbeans.editor.ObjectArray;

public class ObjectArrayUtilities {
    private ObjectArrayUtilities() {
    }

    public static int binarySearch(ObjectArray objectArray, Object key) {
        int low = 0;
        int high = objectArray.getItemCount() - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            Object midVal = objectArray.getItem(mid);
            int cmp = ((Comparable)midVal).compareTo(key);
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    public static int binarySearch(ObjectArray objectArray, Object key, Comparator c) {
        int low = 0;
        int high = objectArray.getItemCount() - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            Object midVal = objectArray.getItem(mid);
            int cmp = c.compare(midVal, key);
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return - low + 1;
    }

    public static int findIndex(ObjectArray objectArray, Object item) {
        int index = ObjectArrayUtilities.binarySearch(objectArray, item);
        if (index < 0) {
            return -1;
        }
        if (objectArray.getItem(index) == item) {
            return index;
        }
        int cnt = objectArray.getItemCount();
        for (int upIndex = index + 1; upIndex < cnt; ++upIndex) {
            Object indexItem = objectArray.getItem(upIndex);
            if (indexItem == item) {
                return upIndex;
            }
            if (((Comparable)item).compareTo(indexItem) != 0) break;
        }
        while (--index >= 0) {
            Object indexItem = objectArray.getItem(index);
            if (indexItem == item) {
                return index;
            }
            if (((Comparable)item).compareTo(indexItem) == 0) continue;
            break;
        }
        return -1;
    }

    public static int findIndex(ObjectArray objectArray, Object item, Comparator c) {
        int index = ObjectArrayUtilities.binarySearch(objectArray, item, c);
        if (index < 0) {
            return -1;
        }
        if (objectArray.getItem(index) == item) {
            return index;
        }
        int cnt = objectArray.getItemCount();
        for (int upIndex = index + 1; upIndex < cnt; ++upIndex) {
            Object indexItem = objectArray.getItem(upIndex);
            if (indexItem == item) {
                return upIndex;
            }
            if (c.compare(item, indexItem) != 0) break;
        }
        while (--index >= 0) {
            Object indexItem = objectArray.getItem(index);
            if (indexItem == item) {
                return index;
            }
            if (c.compare(item, indexItem) == 0) continue;
            break;
        }
        return -1;
    }

    public static Object[] toArray(ObjectArray objectArray) {
        return ObjectArrayUtilities.toArray(objectArray, 0, objectArray.getItemCount());
    }

    public static Object[] toArray(ObjectArray objectArray, int startIndex, int endIndex) {
        Object[] dest = new Object[endIndex - startIndex];
        ObjectArrayUtilities.copyItems(objectArray, startIndex, endIndex, dest, 0);
        return dest;
    }

    public static void copyItems(ObjectArray srcObjectArray, Object[] dest) {
        ObjectArrayUtilities.copyItems(srcObjectArray, 0, srcObjectArray.getItemCount(), dest, 0);
    }

    public static void copyItems(ObjectArray srcObjectArray, int srcStartIndex, int srcEndIndex, Object[] dest, int destIndex) {
        if (srcObjectArray instanceof ObjectArray.CopyItems) {
            ((ObjectArray.CopyItems)((Object)srcObjectArray)).copyItems(srcStartIndex, srcEndIndex, dest, destIndex);
        } else {
            while (srcStartIndex < srcEndIndex) {
                dest[destIndex++] = srcObjectArray.getItem(srcStartIndex++);
            }
        }
    }

    public static void reverse(Object[] array) {
        int i = 0;
        for (int j = array.length - 1; i < j; ++i, --j) {
            Object o = array[i];
            array[i] = array[j];
            array[j] = o;
        }
    }

    public static ObjectArray.Modification mergeModifications(final ObjectArray.Modification mod1, ObjectArray.Modification mod2) {
        int index;
        int removedItemsCount;
        Object[] addedItems;
        if (mod1.getArray() != mod2.getArray()) {
            throw new IllegalArgumentException("Cannot merge modifications to different object arrays.");
        }
        if (mod1.getIndex() <= mod2.getIndex()) {
            index = mod1.getIndex();
            int overlap = Math.min(mod1.getIndex() + mod1.getAddedItems().length - mod2.getIndex(), mod2.getRemovedItemsCount());
            addedItems = new Object[mod1.getAddedItems().length + mod2.getAddedItems().length - overlap];
            if (overlap < 0) {
                System.arraycopy(mod1.getAddedItems(), 0, addedItems, 0, mod1.getAddedItems().length);
                ObjectArrayUtilities.copyItems(mod1.getArray(), mod1.getIndex() + mod1.getRemovedItemsCount(), mod1.getIndex() + mod1.getRemovedItemsCount() - overlap, addedItems, mod1.getAddedItems().length);
                System.arraycopy(mod2.getAddedItems(), 0, addedItems, mod2.getIndex() - mod1.getIndex(), mod2.getAddedItems().length);
            } else {
                System.arraycopy(mod1.getAddedItems(), 0, addedItems, 0, mod2.getIndex() - mod1.getIndex());
                System.arraycopy(mod2.getAddedItems(), 0, addedItems, mod2.getIndex() - mod1.getIndex(), mod2.getAddedItems().length);
                System.arraycopy(mod1.getAddedItems(), mod2.getIndex() - mod1.getIndex() + overlap, addedItems, mod2.getIndex() - mod1.getIndex() + mod2.getAddedItems().length, mod1.getAddedItems().length - mod2.getIndex() + mod1.getIndex() - overlap);
            }
            removedItemsCount = mod1.getRemovedItemsCount() + mod2.getRemovedItemsCount() - overlap;
        } else {
            index = mod2.getIndex();
            int overlap = Math.min(mod2.getIndex() + mod2.getRemovedItemsCount() - mod1.getIndex(), mod1.getAddedItems().length);
            addedItems = new Object[mod1.getAddedItems().length + mod2.getAddedItems().length - overlap];
            System.arraycopy(mod2.getAddedItems(), 0, addedItems, 0, mod2.getAddedItems().length);
            if (overlap < 0) {
                ObjectArrayUtilities.copyItems(mod1.getArray(), mod2.getIndex() + mod2.getRemovedItemsCount(), mod1.getIndex(), addedItems, mod2.getAddedItems().length);
                System.arraycopy(mod1.getAddedItems(), 0, addedItems, mod2.getAddedItems().length - overlap, mod1.getAddedItems().length);
            } else {
                System.arraycopy(mod1.getAddedItems(), overlap, addedItems, mod2.getAddedItems().length, mod1.getAddedItems().length - overlap);
            }
            removedItemsCount = mod1.getRemovedItemsCount() + mod2.getRemovedItemsCount() - overlap;
        }
        return new ObjectArray.Modification(){

            @Override
            public ObjectArray getArray() {
                return mod1.getArray();
            }

            @Override
            public int getIndex() {
                return index;
            }

            @Override
            public Object[] getAddedItems() {
                return addedItems;
            }

            @Override
            public int getRemovedItemsCount() {
                return removedItemsCount;
            }
        };
    }

    public static ObjectArray.Modification mergeModifications(ObjectArray.Modification[] mods) {
        ObjectArray.Modification ret = mods.length > 0 ? mods[0] : null;
        for (int i = 1; i < mods.length; ++i) {
            ret = ObjectArrayUtilities.mergeModifications(ret, mods[i]);
        }
        return ret;
    }

}

