/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WeakPropertyChangeSupport;

public class AnnotationTypes {
    public static final String PROP_BACKGROUND_DRAWING = "backgroundDrawing";
    public static final String PROP_BACKGROUND_GLYPH_ALPHA = "backgroundGlyphAlpha";
    public static final String PROP_COMBINE_GLYPHS = "combineGlyphs";
    public static final String PROP_GLYPHS_OVER_LINE_NUMBERS = "glyphsOverLineNumbers";
    public static final String PROP_SHOW_GLYPH_GUTTER = "showGlyphGutter";
    public static final String PROP_ANNOTATION_TYPES = "annotationTypes";
    private Map properties = new HashMap(6);
    private WeakPropertyChangeSupport support = new WeakPropertyChangeSupport();
    private Map<String, AnnotationType> allTypes = null;
    private boolean loadedTypes = false;
    private boolean loadedSettings = false;
    private boolean loadingInProgress = false;
    private Loader loader = null;
    private static URL defaultGlyphIcon = null;
    private static AnnotationTypes annoTypes = null;

    private AnnotationTypes() {
    }

    public static AnnotationTypes getTypes() {
        AnnotationTypes types = annoTypes;
        if (types == null) {
            annoTypes = types = new AnnotationTypes();
        }
        return types;
    }

    public static URL getDefaultGlyphURL() {
        if (defaultGlyphIcon == null) {
            defaultGlyphIcon = AnnotationTypes.class.getClassLoader().getResource("org/netbeans/editor/resources/defaultglyph.gif");
        }
        return defaultGlyphIcon;
    }

    public Boolean isBackgroundDrawing() {
        this.loadSettings();
        Boolean b = (Boolean)this.getProp("backgroundDrawing");
        if (b == null) {
            return Boolean.FALSE;
        }
        return b;
    }

    public void setBackgroundDrawing(Boolean drawing) {
        if (!this.isBackgroundDrawing().equals(drawing)) {
            this.putProp("backgroundDrawing", drawing);
            this.firePropertyChange("backgroundDrawing", null, null);
            this.saveSetting("backgroundDrawing", drawing);
        }
    }

    public Boolean isCombineGlyphs() {
        this.loadSettings();
        Boolean b = (Boolean)this.getProp("combineGlyphs");
        if (b == null) {
            return Boolean.TRUE;
        }
        return b;
    }

    public void setCombineGlyphs(Boolean combine) {
        if (!this.isCombineGlyphs().equals(combine)) {
            this.putProp("combineGlyphs", combine);
            this.firePropertyChange("combineGlyphs", null, null);
            this.saveSetting("combineGlyphs", combine);
        }
    }

    public Integer getBackgroundGlyphAlpha() {
        this.loadSettings();
        if (this.getProp("backgroundGlyphAlpha") == null) {
            return new Integer(40);
        }
        return (Integer)this.getProp("backgroundGlyphAlpha");
    }

    public void setBackgroundGlyphAlpha(int alpha) {
        if (alpha < 0 || alpha > 100) {
            return;
        }
        Integer i = new Integer(alpha);
        this.putProp("backgroundGlyphAlpha", i);
        this.firePropertyChange("backgroundGlyphAlpha", null, null);
        this.saveSetting("backgroundGlyphAlpha", i);
    }

    public Boolean isGlyphsOverLineNumbers() {
        this.loadSettings();
        Boolean b = (Boolean)this.getProp("glyphsOverLineNumbers");
        if (b == null) {
            return Boolean.TRUE;
        }
        return b;
    }

    public void setGlyphsOverLineNumbers(Boolean over) {
        if (!this.isGlyphsOverLineNumbers().equals(over)) {
            this.putProp("glyphsOverLineNumbers", over);
            this.firePropertyChange("glyphsOverLineNumbers", null, null);
            this.saveSetting("glyphsOverLineNumbers", over);
        }
    }

    public Boolean isShowGlyphGutter() {
        this.loadSettings();
        Boolean b = (Boolean)this.getProp("showGlyphGutter");
        if (b == null) {
            return Boolean.TRUE;
        }
        return b;
    }

    public void setShowGlyphGutter(Boolean gutter) {
        if (!this.isShowGlyphGutter().equals(gutter)) {
            this.putProp("showGlyphGutter", gutter);
            this.firePropertyChange("showGlyphGutter", null, null);
            this.saveSetting("showGlyphGutter", gutter);
        }
    }

    private Object getProp(String prop) {
        return this.properties.get(prop);
    }

    private void putProp(Object key, Object value) {
        if (value == null) {
            this.properties.remove(key);
            return;
        }
        this.properties.put(key, value);
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.support.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.support.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.support.firePropertyChange(this, propertyName, oldValue, newValue);
    }

    public final void setTypes(Map map) {
        if (this.allTypes != null) {
            this.allTypes = map;
            SwingUtilities.invokeLater(new FirePropertyChange());
        } else {
            this.allTypes = map;
        }
    }

    public final void removeType(String name) {
        this.allTypes.remove(name);
        SwingUtilities.invokeLater(new FirePropertyChange());
    }

    public final AnnotationType getType(String name) {
        this.loadTypes();
        AnnotationType ret = null;
        if (this.allTypes != null) {
            ret = this.allTypes.get(name);
        }
        if (ret == null) {
            Utilities.annotateLoggable(new NullPointerException("null AnnotationType for:" + name));
        }
        return ret;
    }

    public Iterator<String> getAnnotationTypeNames() {
        this.loadTypes();
        HashSet<String> temp = new HashSet<String>();
        if (this.allTypes != null) {
            temp.addAll(this.allTypes.keySet());
        }
        return temp.iterator();
    }

    public int getAnnotationTypeNamesCount() {
        this.loadTypes();
        return this.allTypes.keySet().size();
    }

    public int getVisibleAnnotationTypeNamesCount() {
        this.loadTypes();
        Iterator<String> i = this.getAnnotationTypeNames();
        int count = 0;
        while (i.hasNext()) {
            AnnotationType type = this.getType(i.next());
            if (type == null || !type.isVisible()) continue;
            ++count;
        }
        return count;
    }

    public void registerLoader(Loader l) {
        this.loader = l;
        this.loadedTypes = false;
        this.loadedSettings = false;
    }

    private void loadTypes() {
        if (this.loadedTypes || this.loader == null) {
            return;
        }
        this.loader.loadTypes();
        this.loadedTypes = true;
    }

    public void saveType(AnnotationType type) {
        if (!this.loadedTypes || this.loader == null) {
            return;
        }
        this.loader.saveType(type);
    }

    private void loadSettings() {
        if (this.loadedSettings || this.loader == null || this.loadingInProgress) {
            return;
        }
        this.loadingInProgress = true;
        this.loader.loadSettings();
        this.loadingInProgress = false;
        this.loadedSettings = true;
    }

    public void saveSetting(String settingName, Object value) {
        if (!this.loadedSettings || this.loader == null) {
            return;
        }
        this.loader.saveSetting(settingName, value);
    }

    private class FirePropertyChange
    implements Runnable {
        FirePropertyChange() {
        }

        @Override
        public void run() {
            AnnotationTypes.this.firePropertyChange("annotationTypes", null, null);
        }
    }

    public static interface Loader {
        public void loadTypes();

        public void loadSettings();

        public void saveType(AnnotationType var1);

        public void saveSetting(String var1, Object var2);
    }

}

