/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.Action;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.Mark;

public abstract class AnnotationDesc
implements Comparable<AnnotationDesc> {
    public static final String PROP_SHORT_DESCRIPTION = "shortDescription";
    public static final String PROP_ANNOTATION_TYPE = "annotationType";
    public static final String PROP_MOVE_TO_FRONT = "moveToFront";
    private PropertyChangeSupport support;
    private int order = ++counter;
    private static int counter = 0;
    private int length;
    private Mark mark;
    private AnnotationType type = null;

    public AnnotationDesc(int offset, int length) {
        this.length = length;
        this.support = new PropertyChangeSupport(this);
    }

    public Coloring getColoring() {
        if (this.type == null) {
            this.updateAnnotationType();
        }
        return this.type != null ? this.type.getColoring() : new Coloring(null, 7, null, null, null, null, null);
    }

    public Image getGlyph() {
        if (this.type == null) {
            this.updateAnnotationType();
        }
        return this.type != null ? this.type.getGlyphImage() : null;
    }

    private int getPriority() {
        if (this.type == null) {
            this.updateAnnotationType();
        }
        return this.type.getPriority();
    }

    public boolean isDefaultGlyph() {
        if (this.type == null) {
            this.updateAnnotationType();
        }
        return this.type != null ? this.type.isDefaultGlyph() : false;
    }

    public boolean isVisible() {
        if (this.type == null) {
            this.updateAnnotationType();
        }
        return this.type != null ? this.type.isVisible() : false;
    }

    public int getOrderNumber() {
        return this.order;
    }

    public Action[] getActions() {
        if (this.type == null) {
            this.updateAnnotationType();
        }
        return this.type != null ? this.type.getActions() : new Action[]{};
    }

    public boolean isWholeLine() {
        return this.length == -1;
    }

    public int getLength() {
        return this.length;
    }

    void setMark(Mark mark) {
        this.mark = mark;
    }

    Mark getMark() {
        return this.mark;
    }

    public AnnotationType getAnnotationTypeInstance() {
        return this.type;
    }

    public abstract String getAnnotationType();

    public abstract String getShortDescription();

    public abstract int getOffset();

    public abstract int getLine();

    public void updateAnnotationType() {
        this.type = AnnotationTypes.getTypes().getType(this.getAnnotationType());
    }

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.support.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.support.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        this.support.firePropertyChange(propertyName, oldValue, newValue);
    }

    @Override
    public int compareTo(AnnotationDesc o) {
        int p2;
        int p1 = this.getPriority();
        return p1 > (p2 = o.getPriority()) ? -1 : (p1 == p2 ? 0 : 1);
    }

    public String toString() {
        return "Annotation: type='" + this.getAnnotationType() + "', line=" + this.getLine() + ", offset=" + this.getOffset() + ", length=" + this.length + ", coloring=" + this.getColoring();
    }
}

