/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.document.LineElement
 */
package org.netbeans.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Segment;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.SyntaxUpdateTokens;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.modules.editor.lib2.document.LineElement;

final class FixLineSyntaxState {
    private static final boolean debug = false;
    private final DocumentEvent evt;
    private int syntaxUpdateOffset;
    private List syntaxUpdateTokenList = Collections.EMPTY_LIST;

    FixLineSyntaxState(DocumentEvent evt) {
        this.evt = evt;
    }

    final int getSyntaxUpdateOffset() {
        return this.syntaxUpdateOffset;
    }

    final List getSyntaxUpdateTokenList() {
        return this.syntaxUpdateTokenList;
    }

    static void invalidateAllSyntaxStateInfos(BaseDocument doc) {
        Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
        int elemCount = lineRoot.getElementCount();
        for (int i = elemCount - 1; i >= 0; --i) {
            LineElement line = (LineElement)lineRoot.getElement(i);
            line.legacySetAttributesObject((Object)null);
        }
    }

    static void prepareSyntax(BaseDocument doc, Segment text, Syntax syntax, int reqPos, int reqLen, boolean forceLastBuffer, boolean forceNotLastBuffer) throws BadLocationException {
        int preScan;
        if (reqPos < 0 || reqLen < 0 || reqPos + reqLen > doc.getLength()) {
            throw new BadLocationException("reqPos=" + reqPos + ", reqLen=" + reqLen + ", doc.getLength()=" + doc.getLength(), -1);
        }
        Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
        int reqPosLineIndex = lineRoot.getElementIndex(reqPos);
        Element reqPosLineElem = lineRoot.getElement(reqPosLineIndex);
        Syntax.StateInfo stateInfo = FixLineSyntaxState.getValidSyntaxStateInfo(doc, reqPosLineIndex);
        int lineStartOffset = reqPosLineElem.getStartOffset();
        int n = preScan = stateInfo != null ? stateInfo.getPreScan() : 0;
        if (preScan > lineStartOffset) {
            preScan = lineStartOffset;
        }
        int intraLineLength = reqPos - lineStartOffset;
        doc.getText(lineStartOffset - preScan, preScan + intraLineLength + reqLen, text);
        text.offset += preScan;
        text.count -= preScan;
        syntax.load(stateInfo, text.array, text.offset, intraLineLength, false, reqPos);
        while (syntax.nextToken() != null) {
        }
        text.offset += intraLineLength;
        text.count -= intraLineLength;
        boolean forceLB = forceNotLastBuffer ? false : forceLastBuffer || reqPos + reqLen >= doc.getLength();
        syntax.relocate(text.array, text.offset, text.count, forceLB, reqPos + reqLen);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static Syntax.StateInfo getValidSyntaxStateInfo(BaseDocument doc, int lineIndex) throws BadLocationException {
        LineElement lineElem;
        if (lineIndex == 0) {
            return null;
        }
        Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
        lineElem = (LineElement)lineRoot.getElement(lineIndex);
        Syntax.StateInfo stateInfo = (Syntax.StateInfo)lineElem.legacyGetAttributesObject();
        if (lineIndex > 0 && stateInfo == null) {
            int validLineIndex;
            LineElement validLineElem = null;
            for (validLineIndex = lineIndex - 1; validLineIndex > 0 && (stateInfo = (Syntax.StateInfo)(validLineElem = (LineElement)lineRoot.getElement(validLineIndex)).legacyGetAttributesObject()) == null; --validLineIndex) {
            }
            Segment text = new Segment();
            Syntax syntax = doc.getFreeSyntax();
            try {
                int validLineOffset;
                int lineElemOffset = lineElem.getStartOffset();
                int preScan = 0;
                if (validLineIndex > 0) {
                    validLineOffset = validLineElem.getStartOffset();
                    preScan = stateInfo.getPreScan();
                } else {
                    validLineOffset = 0;
                    stateInfo = null;
                }
                doc.getText(validLineOffset - preScan, lineElemOffset - validLineOffset + preScan, text);
                text.offset += preScan;
                text.count -= preScan;
                syntax.load(stateInfo, text.array, text.offset, text.count, false, lineElemOffset);
                int textEndOffset = text.offset + text.count;
                do {
                    validLineElem = (LineElement)lineRoot.getElement(++validLineIndex);
                    int scanLength = validLineOffset;
                    validLineOffset = validLineElem.getStartOffset();
                    scanLength = validLineOffset - scanLength;
                    syntax.relocate(text.array, syntax.getOffset(), scanLength, false, validLineOffset);
                    while (syntax.nextToken() != null) {
                    }
                    FixLineSyntaxState.updateSyntaxStateInfo(syntax, validLineElem);
                } while (validLineIndex != lineIndex);
            }
            finally {
                doc.releaseSyntax(syntax);
            }
        }
        return (Syntax.StateInfo)lineElem.legacyGetAttributesObject();
    }

    static void updateSyntaxStateInfo(Syntax syntax, LineElement lineElement) {
        Syntax.StateInfo syntaxStateInfo = (Syntax.StateInfo)lineElement.legacyGetAttributesObject();
        if (syntaxStateInfo == null) {
            syntaxStateInfo = syntax.createStateInfo();
            assert (syntaxStateInfo != null);
            lineElement.legacySetAttributesObject((Object)syntaxStateInfo);
        }
        syntax.storeState(syntaxStateInfo);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void update(boolean undo) {
        SyntaxUpdateTokens suTokens = (SyntaxUpdateTokens)this.evt.getDocument().getProperty(SyntaxUpdateTokens.class);
        if (suTokens != null) {
            suTokens.syntaxUpdateStart();
        }
        try {
            this.syntaxUpdateOffset = this.fixSyntaxStateInfos(undo);
        }
        finally {
            if (suTokens != null) {
                this.syntaxUpdateTokenList = Collections.unmodifiableList(new ArrayList(suTokens.syntaxUpdateEnd()));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private int fixSyntaxStateInfos(boolean undo) {
        int offset = this.evt.getOffset();
        if (offset < 0) {
            throw new IllegalStateException("offset=" + offset);
        }
        BaseDocument doc = (BaseDocument)this.evt.getDocument();
        Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
        int lineCount = lineRoot.getElementCount();
        DocumentEvent.ElementChange lineChange = this.evt.getChange(lineRoot);
        int lineIndex = lineChange != null ? lineChange.getIndex() : lineRoot.getElementIndex(offset);
        int addedLinesCount = lineChange != null ? lineChange.getChildrenAdded().length : 0;
        int maybeMatchLineIndex = lineIndex + addedLinesCount + 1;
        if (lineIndex > 0) {
            --lineIndex;
        }
        if (lineIndex + 1 == lineCount) {
            return doc.getLength();
        }
        LineElement lineElem = (LineElement)lineRoot.getElement(lineIndex);
        Segment text = new Segment();
        try {
            Syntax.StateInfo stateInfo = FixLineSyntaxState.getValidSyntaxStateInfo(doc, lineIndex);
            int lineStartOffset = lineElem.getStartOffset();
            int preScan = stateInfo != null ? stateInfo.getPreScan() : 0;
            Syntax syntax = doc.getFreeSyntax();
            try {
                LineElement nextLineElem = (LineElement)lineRoot.getElement(++lineIndex);
                int nextLineStartOffset = nextLineElem.getStartOffset();
                int len = nextLineStartOffset - lineStartOffset + preScan;
                if (len < 0) {
                    throw new IndexOutOfBoundsException("len=" + len + " < 0: nextLineStartOffset=" + nextLineStartOffset + ", lineStartOffset=" + lineStartOffset + ", preScan=" + preScan);
                }
                doc.getText(lineStartOffset - preScan, len, text);
                text.offset += preScan;
                text.count -= preScan;
                syntax.load(stateInfo, text.array, text.offset, text.count, false, nextLineStartOffset);
                SyntaxUpdateTokens suTokens = (SyntaxUpdateTokens)doc.getProperty(SyntaxUpdateTokens.class);
                int textLength = -1;
                int textStartOffset = -1;
                int textBufferStartOffset = -1;
                do {
                    int tbStartOffset = lineStartOffset - text.offset;
                    TokenID tokenID = syntax.nextToken();
                    while (tokenID != null) {
                        if (suTokens != null) {
                            suTokens.syntaxUpdateToken(tokenID, syntax.getTokenContextPath(), tbStartOffset + syntax.getTokenOffset(), syntax.getTokenLength());
                        }
                        tokenID = syntax.nextToken();
                    }
                    stateInfo = (Syntax.StateInfo)nextLineElem.legacyGetAttributesObject();
                    if (lineIndex >= maybeMatchLineIndex && stateInfo != null && syntax.compareState(stateInfo) == 0) {
                        lineStartOffset = nextLineStartOffset;
                        tbStartOffset = lineStartOffset;
                        return tbStartOffset;
                    }
                    FixLineSyntaxState.updateSyntaxStateInfo(syntax, nextLineElem);
                    if (++lineIndex >= lineCount) {
                        int n = doc.getLength();
                        return n;
                    }
                    lineElem = nextLineElem;
                    lineStartOffset = nextLineStartOffset;
                    nextLineElem = (LineElement)lineRoot.getElement(lineIndex);
                    nextLineStartOffset = nextLineElem.getStartOffset();
                    preScan = syntax.getPreScan();
                    int requestedTextLength = nextLineStartOffset - lineStartOffset + preScan;
                    if (textLength == -1) {
                        textStartOffset = lineStartOffset - preScan;
                        textLength = requestedTextLength;
                        if (textLength < 0) {
                            throw new IndexOutOfBoundsException("len=" + textLength + " < 0: nextLineStartOffset=" + nextLineStartOffset + ", lineStartOffset=" + lineStartOffset + ", preScan=" + preScan);
                        }
                        doc.getText(textStartOffset, textLength, text);
                        textBufferStartOffset = textStartOffset - text.offset;
                    } else {
                        if (lineStartOffset - preScan < textStartOffset || nextLineStartOffset > textStartOffset + textLength) {
                            textLength = Math.max(textLength, requestedTextLength);
                            textLength *= 2;
                            textStartOffset = lineStartOffset - preScan;
                            textLength = Math.min(textStartOffset + textLength, doc.getLength()) - textStartOffset;
                            doc.getText(textStartOffset, textLength, text);
                            textBufferStartOffset = textStartOffset - text.offset;
                        } else {
                            text.offset = lineStartOffset - preScan - textBufferStartOffset;
                        }
                        text.count = requestedTextLength;
                    }
                    text.offset += preScan;
                    text.count -= preScan;
                    syntax.relocate(text.array, text.offset, text.count, false, nextLineStartOffset);
                } while (true);
            }
            finally {
                doc.releaseSyntax(syntax);
            }
        }
        catch (BadLocationException e) {
            throw new IllegalStateException(e);
        }
    }

    static int getTokenSafeOffset(BaseDocument doc, int offset) {
        if (offset == 0) {
            return offset;
        }
        try {
            Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
            int lineIndex = lineRoot.getElementIndex(offset);
            Element lineElem = lineRoot.getElement(lineIndex);
            int lineStartOffset = lineElem.getStartOffset();
            Syntax.StateInfo stateInfo = FixLineSyntaxState.getValidSyntaxStateInfo(doc, lineIndex);
            if (offset == lineStartOffset && stateInfo.getPreScan() == 0) {
                return offset;
            }
            int lineCount = lineRoot.getElementCount();
            while (++lineIndex < lineCount) {
                lineElem = lineRoot.getElement(lineIndex);
                stateInfo = FixLineSyntaxState.getValidSyntaxStateInfo(doc, lineIndex);
                lineStartOffset = lineElem.getStartOffset();
                if (lineStartOffset - stateInfo.getPreScan() < offset) continue;
                return lineStartOffset;
            }
        }
        catch (BadLocationException e) {
            throw new IllegalStateException(e.toString());
        }
        return doc.getLength();
    }

    private static Element getLineRoot(Document doc) {
        return doc.getDefaultRootElement();
    }

    private static void checkConsistency(Document doc) {
        Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
        int lineCount = lineRoot.getElementCount();
        for (int i = 1; i < lineCount; ++i) {
            LineElement elem = (LineElement)lineRoot.getElement(i);
            assert ((Syntax.StateInfo)elem.legacyGetAttributesObject() != null);
        }
    }

    public static String lineInfosToString(Document doc) {
        StringBuffer sb = new StringBuffer();
        Element lineRoot = FixLineSyntaxState.getLineRoot(doc);
        int lineCount = lineRoot.getElementCount();
        for (int i = 0; i < lineCount; ++i) {
            LineElement elem = (LineElement)lineRoot.getElement(i);
            sb.append("[" + i + "]: lineStartOffset=" + elem.getStartOffset() + ", info: " + (Syntax.StateInfo)elem.legacyGetAttributesObject() + "\n");
        }
        return sb.toString();
    }

    UndoableEdit createBeforeLineUndo() {
        return new BeforeLineUndo();
    }

    UndoableEdit createAfterLineUndo() {
        return new AfterLineUndo();
    }

    final class AfterLineUndo
    extends AbstractUndoableEdit {
        AfterLineUndo() {
        }

        @Override
        public void redo() throws CannotRedoException {
            FixLineSyntaxState.this.update(false);
            super.redo();
        }
    }

    final class BeforeLineUndo
    extends AbstractUndoableEdit {
        BeforeLineUndo() {
        }

        FixLineSyntaxState getMaster() {
            return FixLineSyntaxState.this;
        }

        @Override
        public void undo() throws CannotUndoException {
            FixLineSyntaxState.this.update(true);
            super.undo();
        }
    }

}

