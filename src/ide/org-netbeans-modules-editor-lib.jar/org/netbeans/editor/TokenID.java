/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import org.netbeans.editor.TokenCategory;

public interface TokenID
extends TokenCategory {
    public TokenCategory getCategory();
}

