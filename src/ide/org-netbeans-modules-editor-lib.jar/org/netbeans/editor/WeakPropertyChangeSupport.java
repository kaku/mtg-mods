/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WeakPropertyChangeSupport {
    private transient ArrayList listeners = new ArrayList();
    private transient ArrayList interestNames = new ArrayList();

    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        this.addLImpl(null, l);
    }

    public synchronized void addPropertyChangeListener(String propertyName, PropertyChangeListener l) {
        this.addLImpl(propertyName, l);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
        int cnt = this.listeners.size();
        for (int i = 0; i < cnt; ++i) {
            Object o = ((WeakReference)this.listeners.get(i)).get();
            if (o != null && o != l) continue;
            this.listeners.remove(i);
            this.interestNames.remove(i);
            --i;
            --cnt;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void firePropertyChange(Object source, String propertyName, Object oldValue, Object newValue) {
        int i;
        String[] isa;
        int cnt;
        PropertyChangeListener[] la;
        if (oldValue != null && newValue != null && oldValue.equals(newValue)) {
            return;
        }
        WeakPropertyChangeSupport weakPropertyChangeSupport = this;
        synchronized (weakPropertyChangeSupport) {
            cnt = this.listeners.size();
            la = new PropertyChangeListener[cnt];
            for (i = 0; i < cnt; ++i) {
                PropertyChangeListener l = (PropertyChangeListener)((WeakReference)this.listeners.get(i)).get();
                if (l == null) {
                    this.listeners.remove(i);
                    this.interestNames.remove(i);
                    --i;
                    --cnt;
                    continue;
                }
                la[i] = l;
            }
            isa = this.interestNames.toArray(new String[cnt]);
        }
        PropertyChangeEvent evt = new PropertyChangeEvent(source, propertyName, oldValue, newValue);
        for (i = 0; i < cnt; ++i) {
            if (isa[i] != null && propertyName != null && !isa[i].equals(propertyName)) continue;
            la[i].propertyChange(evt);
        }
    }

    private void addLImpl(String sn, PropertyChangeListener l) {
        this.listeners.add(new WeakReference<PropertyChangeListener>(l));
        this.interestNames.add(sn);
    }
}

