/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import java.util.HashMap;
import javax.swing.text.BadLocationException;
import javax.swing.text.Segment;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Finder;
import org.netbeans.editor.Syntax;
import org.netbeans.editor.SyntaxSeg;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenItem;
import org.netbeans.editor.TokenProcessor;

public class SyntaxSupport {
    private static final int[] EMPTY_INT_ARRAY = new int[0];
    private static final int MATCH_ARRAY_CACHE_SIZE = 3;
    private HashMap supMap;
    private BaseDocument doc;
    protected boolean tokenNumericIDsValid;
    private int[] tokenBlocks = EMPTY_INT_ARRAY;
    private TokenID[][] lastTokenIDArrays = new TokenID[3][];
    private boolean[][] lastMatchArrays = new boolean[3][];

    public SyntaxSupport(BaseDocument doc) {
        this.doc = doc;
    }

    public final BaseDocument getDocument() {
        return this.doc;
    }

    public synchronized SyntaxSupport get(Class syntaxSupportClass) {
        SyntaxSupport sup;
        if (this.supMap == null) {
            this.supMap = new HashMap(11);
        }
        if ((sup = (SyntaxSupport)this.supMap.get(syntaxSupportClass)) == null) {
            sup = this.createSyntaxSupport(syntaxSupportClass);
            this.supMap.put(syntaxSupportClass, sup);
        }
        return sup;
    }

    protected SyntaxSupport createSyntaxSupport(Class syntaxSupportClass) {
        if (syntaxSupportClass.isInstance(this)) {
            return this;
        }
        return null;
    }

    private boolean[] getMatchArray(TokenID[] tokenIDArray) {
        int ind;
        boolean[] matchArray = null;
        for (ind = 0; ind < 3; ++ind) {
            if (tokenIDArray != this.lastTokenIDArrays[ind]) continue;
            matchArray = this.lastMatchArrays[ind];
            break;
        }
        if (matchArray == null) {
            int i;
            int maxTokenNumericID = -1;
            if (tokenIDArray != null) {
                for (i = 0; i < tokenIDArray.length; ++i) {
                    if (tokenIDArray[i].getNumericID() <= maxTokenNumericID) continue;
                    maxTokenNumericID = tokenIDArray[i].getNumericID();
                }
            }
            matchArray = new boolean[maxTokenNumericID + 1];
            for (i = 0; i < tokenIDArray.length; ++i) {
                matchArray[tokenIDArray[i].getNumericID()] = true;
            }
        }
        if (ind > 0) {
            ind = Math.min(ind, 2);
            System.arraycopy(this.lastTokenIDArrays, 0, this.lastTokenIDArrays, 1, ind);
            System.arraycopy(this.lastMatchArrays, 0, this.lastMatchArrays, 1, ind);
            this.lastTokenIDArrays[0] = tokenIDArray;
            this.lastMatchArrays[0] = matchArray;
        }
        return matchArray;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int[] getTokenBlocks(int startPos, int endPos, TokenID[] tokenIDArray) throws BadLocationException {
        this.doc.readLock();
        try {
            SyntaxSupport syntaxSupport = this;
            synchronized (syntaxSupport) {
                int blkInd;
                boolean[] matchArray = this.tokenNumericIDsValid ? this.getMatchArray(tokenIDArray) : null;
                blkInd = 0;
                if (startPos > endPos) {
                    int tmp = startPos;
                    startPos = endPos;
                    endPos = tmp;
                }
                SyntaxSeg.Slot slot = SyntaxSeg.getFreeSlot();
                Syntax syntax = this.doc.getFreeSyntax();
                try {
                    this.doc.prepareSyntax(slot, syntax, startPos, endPos - startPos, true, false);
                    int preScan = syntax.getPreScan();
                    int pos = startPos - preScan;
                    int blkStart = -1;
                    boolean cont = true;
                    while (cont) {
                        boolean matches;
                        TokenID tokenID = syntax.nextToken();
                        if (tokenID == null) {
                            cont = false;
                            continue;
                        }
                        boolean bl = matches = tokenID != null && pos + syntax.getTokenLength() > startPos;
                        if (matches) {
                            if (matchArray != null) {
                                int numID = tokenID.getNumericID();
                                matches = numID < matchArray.length && matchArray[numID];
                            } else {
                                matches = false;
                                for (int i = 0; i < tokenIDArray.length; ++i) {
                                    if (tokenID != tokenIDArray[i]) continue;
                                    matches = true;
                                    break;
                                }
                            }
                        }
                        if (matches) {
                            if (blkStart < 0) {
                                blkStart = Math.max(pos, startPos);
                            }
                        } else if (blkStart >= 0) {
                            this.tokenBlocks = this.addTokenBlock(this.tokenBlocks, blkInd, blkStart, pos);
                            blkInd += 2;
                            blkStart = -1;
                        }
                        pos += syntax.getTokenLength();
                    }
                    if (blkStart >= 0) {
                        this.tokenBlocks = this.addTokenBlock(this.tokenBlocks, blkInd, blkStart, endPos);
                        blkInd += 2;
                    }
                }
                finally {
                    this.doc.releaseSyntax(syntax);
                    SyntaxSeg.releaseSlot(slot);
                }
                int[] ret = new int[blkInd];
                System.arraycopy(this.tokenBlocks, 0, ret, 0, blkInd);
                int[] pos = ret;
                return pos;
            }
        }
        finally {
            this.doc.readUnlock();
        }
    }

    private int[] addTokenBlock(int[] blks, int blkInd, int blkStartPos, int blkEndPos) {
        if (blks.length < blkInd + 2) {
            int[] tmp = new int[Math.max(2, blks.length * 2)];
            System.arraycopy(blks, 0, tmp, 0, blkInd);
            blks = tmp;
        }
        blks[blkInd++] = blkStartPos;
        blks[blkInd] = blkEndPos;
        return blks;
    }

    public int findInsideBlocks(Finder finder, int startPos, int endPos, int[] blocks) throws BadLocationException {
        boolean fwd;
        boolean bl = fwd = startPos <= endPos;
        if (fwd) {
            for (int i = 0; i < blocks.length; i += 2) {
                int pos = this.doc.find(finder, blocks[i], blocks[i + 1]);
                if (pos < 0) continue;
                return pos;
            }
        } else {
            for (int i = blocks.length - 2; i >= 0; i -= 2) {
                int pos = this.doc.find(finder, blocks[i + 1], blocks[i]);
                if (pos < 0) continue;
                return pos;
            }
        }
        return -1;
    }

    public int findOutsideBlocks(Finder finder, int startPos, int endPos, int[] blocks) throws BadLocationException {
        boolean fwd;
        boolean bl = fwd = startPos <= endPos;
        if (fwd) {
            int pos = this.doc.find(finder, startPos, blocks.length > 0 ? blocks[0] : endPos);
            if (pos >= 0) {
                return pos;
            }
            for (int ind = 2; ind <= blocks.length; ind += 2) {
                pos = this.doc.find(finder, blocks[ind - 1], ind >= blocks.length ? endPos : blocks[ind]);
                if (pos < 0) continue;
                return pos;
            }
        } else {
            int pos = this.doc.find(finder, startPos, blocks.length > 0 ? blocks[blocks.length - 1] : endPos);
            if (pos >= 0) {
                return pos;
            }
            for (int ind = blocks.length - 2; ind >= 0; ind -= 2) {
                pos = this.doc.find(finder, blocks[ind], ind == 0 ? endPos : blocks[ind - 1]);
                if (pos < 0) continue;
                return pos;
            }
        }
        return -1;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void initSyntax(Syntax syntax, int startPos, int endPos, boolean forceLastBuffer, boolean forceNotLastBuffer) throws BadLocationException {
        this.doc.readLock();
        try {
            Segment text = new Segment();
            int docLen = this.doc.getLength();
            this.doc.prepareSyntax(text, syntax, startPos, 0, forceLastBuffer, forceNotLastBuffer);
            int preScan = syntax.getPreScan();
            char[] buffer = this.doc.getChars(startPos - preScan, endPos - startPos + preScan);
            boolean lastBuffer = forceNotLastBuffer ? false : forceLastBuffer || endPos == docLen;
            syntax.relocate(buffer, preScan, endPos - startPos, lastBuffer, endPos);
        }
        finally {
            this.doc.readUnlock();
        }
    }

    public boolean isIdentifier(String word) {
        if (word == null || word.length() == 0) {
            return false;
        }
        for (int i = 0; i < word.length(); ++i) {
            if (this.doc.isIdentifierPart(word.charAt(i))) continue;
            return false;
        }
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void tokenizeText(TokenProcessor tp, int startOffset, int endOffset, boolean forceLastBuffer) throws BadLocationException {
        Syntax syntax = null;
        this.doc.readLock();
        try {
            Segment text = new Segment();
            syntax = this.doc.getFreeSyntax();
            int docLen = this.doc.getLength();
            this.doc.prepareSyntax(text, syntax, startOffset, endOffset - startOffset, forceLastBuffer, false);
            int preScan = syntax.getPreScan();
            tp.nextBuffer(text.array, syntax.getOffset(), endOffset - startOffset, startOffset, preScan, syntax.lastBuffer);
            int bufferStartOffset = startOffset - syntax.getOffset();
            boolean cont = true;
            while (cont) {
                TokenID tokenID = syntax.nextToken();
                TokenContextPath tcp = syntax.getTokenContextPath();
                if (tokenID == null) {
                    int nextLen = tp.eot(syntax.tokenOffset);
                    if ((nextLen = Math.min(nextLen, docLen - endOffset)) == 0) {
                        cont = false;
                        continue;
                    }
                    preScan = syntax.getPreScan();
                    this.doc.getText(endOffset - preScan, preScan + nextLen, text);
                    boolean lastBuffer = forceLastBuffer || endOffset + nextLen >= docLen;
                    syntax.relocate(text.array, text.offset + preScan, nextLen, lastBuffer, endOffset + nextLen);
                    tp.nextBuffer(text.array, syntax.getOffset(), nextLen, endOffset, preScan, lastBuffer);
                    bufferStartOffset = endOffset - syntax.getOffset();
                    endOffset += nextLen;
                    continue;
                }
                int tokenLen = syntax.getTokenLength();
                int tokenOffset = syntax.getTokenOffset();
                if (bufferStartOffset + tokenOffset + tokenLen <= startOffset || tp.token(tokenID, tcp, tokenOffset, tokenLen)) continue;
                cont = false;
            }
        }
        finally {
            if (syntax != null) {
                this.doc.releaseSyntax(syntax);
            }
            this.doc.readUnlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void tokenizeText(TokenProcessor tp, String text) {
        Syntax syntax = null;
        try {
            syntax = this.doc.getFreeSyntax();
            char[] buf = text.toCharArray();
            syntax.load(null, buf, 0, buf.length, true, -1);
            boolean cont = true;
            while (cont) {
                TokenID tokenID = syntax.nextToken();
                TokenContextPath tcp = syntax.getTokenContextPath();
                if (tokenID == null) {
                    tp.eot(syntax.tokenOffset);
                    cont = false;
                    continue;
                }
                if (tp.token(tokenID, tcp, syntax.getTokenOffset(), syntax.getTokenLength())) continue;
                cont = false;
            }
        }
        finally {
            if (syntax != null) {
                this.doc.releaseSyntax(syntax);
            }
        }
    }

    public TokenItem getTokenChain(int offset) throws BadLocationException {
        if (this.doc.getLength() <= offset) {
            return null;
        }
        return null;
    }

    protected boolean isAbbrevDisabled(int offset) {
        return false;
    }
}

