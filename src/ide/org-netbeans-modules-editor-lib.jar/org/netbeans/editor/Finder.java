/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

public interface Finder {
    public void reset();

    public int find(int var1, char[] var2, int var3, int var4, int var5, int var6);

    public boolean isFound();
}

