/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.Segment;

public class SegmentCache {
    private static final SegmentCache SHARED = new SegmentCache();

    public static SegmentCache getSharedInstance() {
        return SHARED;
    }

    public Segment getSegment() {
        return new Segment();
    }

    public void releaseSegment(Segment segment) {
    }
}

