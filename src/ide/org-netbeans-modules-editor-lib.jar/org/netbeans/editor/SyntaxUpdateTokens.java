/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 */
package org.netbeans.editor;

import java.util.Collections;
import java.util.List;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.SyntaxSupport;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.TokenProcessor;
import org.openide.ErrorManager;

public abstract class SyntaxUpdateTokens {
    public static List getTokenInfoList(DocumentEvent evt) {
        if (!(evt instanceof BaseDocumentEvent)) {
            return Collections.EMPTY_LIST;
        }
        return ((BaseDocumentEvent)evt).getSyntaxUpdateTokenList();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static List getTokenInfoList(Document doc) {
        List tokenList;
        SyntaxUpdateTokens suTokens = (SyntaxUpdateTokens)doc.getProperty(SyntaxUpdateTokens.class);
        if (suTokens == null || !(doc instanceof BaseDocument)) {
            return Collections.EMPTY_LIST;
        }
        BaseDocument bdoc = (BaseDocument)doc;
        bdoc.readLock();
        try {
            suTokens.syntaxUpdateStart();
            try {
                bdoc.getSyntaxSupport().tokenizeText(new AllTokensProcessor(suTokens), 0, bdoc.getLength(), true);
            }
            catch (BadLocationException e) {
                ErrorManager.getDefault().notify(1, (Throwable)e);
            }
            finally {
                tokenList = suTokens.syntaxUpdateEnd();
            }
        }
        finally {
            bdoc.readUnlock();
        }
        return tokenList;
    }

    public abstract void syntaxUpdateStart();

    public abstract List syntaxUpdateEnd();

    public abstract void syntaxUpdateToken(TokenID var1, TokenContextPath var2, int var3, int var4);

    static final class AllTokensProcessor
    implements TokenProcessor {
        private SyntaxUpdateTokens suTokens;
        private int bufferStartOffset;

        AllTokensProcessor(SyntaxUpdateTokens suTokens) {
            this.suTokens = suTokens;
        }

        @Override
        public void nextBuffer(char[] buffer, int offset, int len, int startPos, int preScan, boolean lastBuffer) {
            this.bufferStartOffset = startPos - offset;
        }

        @Override
        public boolean token(TokenID tokenID, TokenContextPath tokenContextPath, int tokenBufferOffset, int tokenLength) {
            this.suTokens.syntaxUpdateToken(tokenID, tokenContextPath, tokenBufferOffset, tokenLength);
            return true;
        }

        @Override
        public int eot(int offset) {
            return 0;
        }
    }

    public class TokenInfo {
        private final TokenID id;
        private final TokenContextPath contextPath;
        private final int offset;
        private final int length;

        public TokenInfo(TokenID id, TokenContextPath contextPath, int offset, int length) {
            this.id = id;
            this.contextPath = contextPath;
            this.offset = offset;
            this.length = length;
        }

        public final TokenID getID() {
            return this.id;
        }

        public final TokenContextPath getContextPath() {
            return this.contextPath;
        }

        public final int getOffset() {
            return this.offset;
        }

        public int getLength() {
            return this.length;
        }

        public String toString() {
            return "id=" + this.id + ", ctx=" + this.contextPath + ", off=" + this.offset + ", len=" + this.length;
        }
    }

}

