/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseElement;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.Utilities;

public class LeafElement
extends BaseElement {
    protected Mark startMark;
    protected Mark endMark;
    protected boolean bol;
    protected boolean eol;

    public LeafElement(BaseDocument doc, BaseElement parent, AttributeSet attrs, int startOffset, int endOffset, boolean bol, boolean eol) {
        super(doc, parent, attrs);
        this.bol = bol;
        this.eol = eol;
        try {
            this.startMark = new Mark(true);
            this.endMark = new Mark(false);
            this.startMark.insert(doc, startOffset);
            this.endMark.insert(doc, endOffset);
        }
        catch (BadLocationException e) {
            Utilities.annotateLoggable(e);
        }
        catch (InvalidMarkException e) {
            throw new IllegalStateException(e.toString());
        }
    }

    protected void finalize() throws Throwable {
        try {
            this.startMark.remove();
            this.endMark.remove();
        }
        catch (InvalidMarkException e) {
            // empty catch block
        }
        Object.super.finalize();
    }

    @Override
    public final Mark getStartMark() {
        return this.startMark;
    }

    @Override
    public final int getStartOffset() {
        try {
            return this.startMark.getOffset();
        }
        catch (InvalidMarkException e) {
            return 0;
        }
    }

    @Override
    public final Mark getEndMark() {
        return this.endMark;
    }

    @Override
    public final int getEndOffset() {
        try {
            return this.endMark.getOffset();
        }
        catch (InvalidMarkException e) {
            return 0;
        }
    }

    public final boolean isBOL() {
        return this.bol;
    }

    public final boolean isEOL() {
        return this.eol;
    }

    @Override
    public int getElementIndex(int offset) {
        return -1;
    }

    @Override
    public int getElementCount() {
        return 0;
    }

    @Override
    public Element getElement(int index) {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    public String toString() {
        return "startOffset=" + this.getStartOffset() + ", endOffset=" + this.getEndMark();
    }
}

