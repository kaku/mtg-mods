/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.editor;

import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import org.netbeans.editor.LineRootElement;
import org.netbeans.editor.Syntax;

final class LineElement
implements Element,
Position {
    private final LineRootElement root;
    private final Position startPos;
    private final Position endPos;
    private AttributeSet attributes = null;
    private Syntax.StateInfo syntaxStateInfo;

    LineElement(LineRootElement root, Position startPos, Position endPos) {
        assert (startPos != null);
        assert (endPos != null);
        this.root = root;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    @Override
    public Document getDocument() {
        return this.root.getDocument();
    }

    @Override
    public int getOffset() {
        return this.getStartOffset();
    }

    @Override
    public int getStartOffset() {
        return this.startPos.getOffset();
    }

    Position getStartPosition() {
        return this.startPos;
    }

    @Override
    public int getEndOffset() {
        return this.endPos.getOffset();
    }

    Position getEndPosition() {
        return this.endPos;
    }

    @Override
    public Element getParentElement() {
        return this.root;
    }

    @Override
    public String getName() {
        return "paragraph";
    }

    @Override
    public AttributeSet getAttributes() {
        AttributeSet as = this.attributes;
        return as == null ? SimpleAttributeSet.EMPTY : as;
    }

    public void setAttributes(AttributeSet attributes) {
        this.attributes = attributes;
    }

    @Override
    public int getElementIndex(int offset) {
        return -1;
    }

    @Override
    public int getElementCount() {
        return 0;
    }

    @Override
    public Element getElement(int index) {
        return null;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    Syntax.StateInfo getSyntaxStateInfo() {
        return this.syntaxStateInfo;
    }

    void updateSyntaxStateInfo(Syntax syntax) {
        if (this.syntaxStateInfo == null) {
            this.syntaxStateInfo = syntax.createStateInfo();
            assert (this.syntaxStateInfo != null);
        }
        syntax.storeState(this.syntaxStateInfo);
    }

    void clearSyntaxStateInfo() {
        this.syntaxStateInfo = null;
    }

    public String toString() {
        return "getStartOffset()=" + this.getStartOffset() + ", getEndOffset()=" + this.getEndOffset() + ", syntaxStateInfo=" + this.getSyntaxStateInfo();
    }
}

