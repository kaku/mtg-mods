/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.text.View;
import org.netbeans.editor.view.spi.EstimatedSpanView;
import org.netbeans.editor.view.spi.ViewInsets;
import org.netbeans.editor.view.spi.ViewLayoutState;
import org.netbeans.lib.editor.view.GapBoxView;
import org.netbeans.lib.editor.view.GapObjectArray;

class GapBoxViewChildren
extends GapObjectArray
implements GapObjectArray.RemoveUpdater {
    private static final int INITIAL_INDEX_GAP_LENGTH = 1073741823;
    private static final double INITIAL_MAJOR_AXIS_OFFSET_GAP_LENGTH = 0.0;
    protected final GapBoxView view;
    private double majorAxisOffsetGapLength;
    private int majorAxisOffsetGapIndex;
    private int indexGapLength;
    private int firstUpdateLayoutChildIndex;
    private int updateLayoutChildCount;
    private float minorAxisPreferredSpan;
    private int maxMinorAxisPreferredSpanChildIndex = -1;
    private int firstRepaintChildIndex = -1;

    GapBoxViewChildren(GapBoxView view) {
        this.view = view;
        this.indexGapLength = 1073741823;
        this.majorAxisOffsetGapLength = 0.0;
    }

    public int getChildCount() {
        return this.getItemCount();
    }

    public ViewLayoutState getChild(int index) {
        return (ViewLayoutState)this.getItem(index);
    }

    public int getChildIndex(ViewLayoutState child) {
        int childIndex = this.getChildIndexNoCheck(child);
        if (childIndex >= this.getChildCount() || this.getChild(childIndex) != child) {
            childIndex = -1;
        }
        return childIndex;
    }

    public int getChildIndexNoCheck(ViewLayoutState child) {
        return this.getTranslatedChildIndex(child.getViewRawIndex());
    }

    private int getTranslatedChildIndex(int rawIndex) {
        if (rawIndex >= this.indexGapLength) {
            rawIndex -= this.indexGapLength;
        }
        return rawIndex;
    }

    public void replace(int index, int length, View[] views) {
        int insertLength;
        this.checkConsistency();
        int n = insertLength = views != null ? views.length : 0;
        if (this.isReplaceRemovingIndexes(index, length)) {
            int neighborIndexAfterReplace;
            int neighborIndex;
            ViewLayoutState neighbor;
            int endRemoveIndex = index + length;
            if (index > 0) {
                neighborIndexAfterReplace = neighborIndex = index - 1;
                neighbor = this.getChild(neighborIndex);
            } else if (endRemoveIndex < this.getChildCount()) {
                neighborIndex = endRemoveIndex;
                neighborIndexAfterReplace = index + insertLength;
                neighbor = this.getChild(neighborIndex);
            } else {
                neighborIndex = -1;
                neighborIndexAfterReplace = -1;
                neighbor = null;
            }
            this.replaceUpdateIndexes(index, length, insertLength, neighborIndex, neighborIndexAfterReplace, neighbor);
        } else {
            this.replaceUpdateIndexes(index, length, insertLength);
        }
        double childMajorAxisOffset = 0.0;
        if (length == 0) {
            if (this.getChildCount() == 0) {
                this.ensureCapacity(insertLength);
            }
        } else {
            int endIndex = index + length;
            this.removeInvalidChildIndexesArea(index, length);
            this.moveMajorAxisOffsetGap(endIndex);
            this.moveIndexGap(endIndex);
            childMajorAxisOffset = this.getMajorAxisOffset(index);
            double majorAxisRemovedSpan = this.getMajorAxisOffset(endIndex) - childMajorAxisOffset;
            this.majorAxisOffsetGapIndex = index;
            this.majorAxisOffsetGapLength += majorAxisRemovedSpan;
            this.indexGapLength += length;
            this.remove(index, length, this);
        }
        if (insertLength > 0) {
            boolean childEstimatedSpan;
            boolean insertLengthAboveThreshold;
            if (length == 0) {
                this.moveIndexGap(index);
                this.moveMajorAxisOffsetGap(index);
                childMajorAxisOffset = this.getMajorAxisOffset(index) - this.majorAxisOffsetGapLength;
            }
            int majorAxis = this.view.getMajorAxis();
            boolean estimatedSpan = this.view.isEstimatedSpan();
            if (estimatedSpan) {
                childEstimatedSpan = true;
                insertLengthAboveThreshold = false;
            } else {
                childEstimatedSpan = insertLengthAboveThreshold = insertLength >= this.view.getReplaceEstimatedThreshold();
            }
            for (int i = 0; i < insertLength; ++i) {
                ViewLayoutState child = this.view.createChild(views[i]);
                View childView = child.getView();
                int childIndex = index + i;
                if (childEstimatedSpan && childView instanceof EstimatedSpanView) {
                    ((EstimatedSpanView)((Object)childView)).setEstimatedSpan(childEstimatedSpan);
                }
                child.selectLayoutMajorAxis(majorAxis);
                this.insertItem(childIndex, child);
                --this.indexGapLength;
                ++this.majorAxisOffsetGapIndex;
                if (!child.isFlyweight()) {
                    child.setViewRawIndex(childIndex);
                    child.setLayoutMajorAxisRawOffset(childMajorAxisOffset);
                    childView.setParent(this.view);
                    child.viewPreferenceChanged(true, true);
                    this.moveMajorAxisOffsetGap(childIndex + 1);
                    childMajorAxisOffset = child.getLayoutMajorAxisRawOffset() + child.getLayoutMajorAxisPreferredSpan();
                    continue;
                }
                childMajorAxisOffset += child.getLayoutMajorAxisPreferredSpan();
            }
            if (childEstimatedSpan && !estimatedSpan) {
                this.view.resetEstimatedSpan(index, insertLength);
            }
        }
        this.view.markMajorAxisPreferenceChanged();
        this.markLayoutInvalid(index, insertLength);
        this.view.markRepaint(index, true);
        this.checkConsistency();
    }

    @Override
    public void removeUpdate(Object removedItem) {
        this.releaseChild((ViewLayoutState)removedItem);
    }

    protected void releaseChild(ViewLayoutState child) {
        if (!child.isFlyweight()) {
            child.getView().setParent(null);
        }
    }

    protected boolean isReplaceRemovingIndexes(int index, int removeLength) {
        int ind = this.getMaxMinorAxisPreferredSpanChildIndex();
        return !this.view.isChildrenLayoutNecessary() && ind >= index && ind < index + removeLength;
    }

    protected void replaceUpdateIndexes(int index, int removeLength, int insertLength) {
        if (!this.view.isChildrenLayoutNecessary()) {
            int endRemoveIndex = index + removeLength;
            int ind = this.getMaxMinorAxisPreferredSpanChildIndex();
            if (ind >= endRemoveIndex) {
                this.setMaxMinorAxisPreferredSpanChildIndex(ind + insertLength - removeLength);
            }
        }
    }

    protected void replaceUpdateIndexes(int index, int removeLength, int insertLength, int neighborIndex, int neighborIndexAfterReplace, ViewLayoutState neighbor) {
        if (!this.view.isChildrenLayoutNecessary()) {
            int endRemoveIndex = index + removeLength;
            int ind = this.getMaxMinorAxisPreferredSpanChildIndex();
            if (ind >= endRemoveIndex) {
                this.setMaxMinorAxisPreferredSpanChildIndex(ind + insertLength - removeLength);
            } else if (ind >= index) {
                if (neighbor == null || neighbor.getLayoutMinorAxisPreferredSpan() < this.view.getMinorAxisPreferredSpan()) {
                    this.view.markChildrenLayoutNecessary();
                }
                this.setMaxMinorAxisPreferredSpanChildIndex(neighborIndexAfterReplace);
            }
        }
    }

    public double getMajorAxisOffset(int childIndex) {
        ViewLayoutState child;
        if (childIndex < this.getChildCount() && !(child = this.getChild(childIndex)).isFlyweight()) {
            double offset = child.getLayoutMajorAxisRawOffset();
            if (childIndex >= this.majorAxisOffsetGapIndex) {
                offset -= this.majorAxisOffsetGapLength;
            }
            return offset;
        }
        double majorAxisOffset = 0.0;
        while (--childIndex >= 0) {
            ViewLayoutState child2 = this.getChild(childIndex);
            majorAxisOffset += child2.getLayoutMajorAxisPreferredSpan();
            if (child2.isFlyweight()) continue;
            double offset = child2.getLayoutMajorAxisRawOffset();
            if (childIndex >= this.majorAxisOffsetGapIndex) {
                offset -= this.majorAxisOffsetGapLength;
            }
            majorAxisOffset += offset;
            break;
        }
        return majorAxisOffset;
    }

    protected double getMajorAxisPreferredSpan() {
        return 0.0 - this.majorAxisOffsetGapLength;
    }

    protected final float getMinorAxisPreferredSpan() {
        return this.minorAxisPreferredSpan;
    }

    protected void setMinorAxisPreferredSpan(float minorAxisPreferredSpan) {
        this.minorAxisPreferredSpan = minorAxisPreferredSpan;
    }

    protected float getMinorAxisOffset(ViewLayoutState child) {
        float minorAxisAssignedSpan = this.view.getMinorAxisAssignedSpan();
        float childPreferredSpan = child.getLayoutMinorAxisPreferredSpan();
        if (childPreferredSpan < minorAxisAssignedSpan) {
            float align = child.getLayoutMinorAxisAlignment();
            return (minorAxisAssignedSpan - childPreferredSpan) * align;
        }
        return 0.0f;
    }

    protected float getMinorAxisSpan(ViewLayoutState child) {
        float minorAxisAssignedSpan = this.view.getMinorAxisAssignedSpan();
        float childPreferredSpan = child.getLayoutMinorAxisPreferredSpan();
        return childPreferredSpan < minorAxisAssignedSpan ? childPreferredSpan : minorAxisAssignedSpan;
    }

    protected void majorAxisPreferenceChanged(ViewLayoutState child, int childIndex, double majorAxisSpanDelta) {
        this.moveMajorAxisOffsetGap(childIndex + 1);
        this.majorAxisOffsetGapLength -= majorAxisSpanDelta;
        this.view.markMajorAxisPreferenceChanged();
    }

    protected void minorAxisPreferenceChanged(ViewLayoutState child, int childIndex) {
        if (!this.view.isChildrenLayoutNecessary()) {
            float preferredSpan = child.getLayoutMinorAxisPreferredSpan();
            float minorAxisPreferredSpan = this.getMinorAxisPreferredSpan();
            int maxPreferredSpanIndex = this.getMaxMinorAxisPreferredSpanChildIndex();
            if (maxPreferredSpanIndex == -1 || preferredSpan > minorAxisPreferredSpan) {
                this.setMinorAxisPreferredSpan(preferredSpan);
                this.view.markMinorAxisPreferenceChanged();
                this.setMaxMinorAxisPreferredSpanChildIndex(childIndex);
            } else if (childIndex == maxPreferredSpanIndex && preferredSpan < minorAxisPreferredSpan) {
                this.view.markChildrenLayoutNecessary();
            }
        }
    }

    public int getChildStartOffset(int childIndex) {
        ViewLayoutState child = this.getChild(childIndex);
        if (!child.isFlyweight()) {
            return child.getView().getStartOffset();
        }
        int startOffset = 0;
        while (--childIndex >= 0) {
            child = this.getChild(childIndex);
            startOffset += child.getView().getEndOffset();
            if (child.isFlyweight()) continue;
        }
        return startOffset;
    }

    public int getChildEndOffset(int childIndex) {
        int endOffset = 0;
        while (childIndex >= 0) {
            ViewLayoutState child = this.getChild(childIndex--);
            endOffset += child.getView().getEndOffset();
            if (child.isFlyweight()) continue;
            break;
        }
        return endOffset;
    }

    public Rectangle getChildCoreAllocation(int childIndex, Rectangle targetRect) {
        this.childrenUpdateLayout(childIndex);
        if (targetRect == null) {
            targetRect = new Rectangle();
        }
        ViewLayoutState child = this.getChild(childIndex);
        int majorAxisOffsetInt = (int)this.getMajorAxisOffset(childIndex);
        int minorAxisOffsetInt = (int)this.getMinorAxisOffset(child);
        int majorAxisSpanInt = (int)child.getLayoutMajorAxisPreferredSpan();
        int minorAxisSpanInt = (int)this.getMinorAxisSpan(child);
        if (this.view.isXMajorAxis()) {
            targetRect.x = majorAxisOffsetInt;
            targetRect.y = minorAxisOffsetInt;
            targetRect.width = majorAxisSpanInt;
            targetRect.height = minorAxisSpanInt;
        } else {
            targetRect.x = minorAxisOffsetInt;
            targetRect.y = majorAxisOffsetInt;
            targetRect.width = minorAxisSpanInt;
            targetRect.height = majorAxisSpanInt;
        }
        ViewInsets insets = this.view.getInsets();
        if (insets != null) {
            targetRect.x += (int)insets.getLeft();
            targetRect.y += (int)insets.getTop();
        }
        return targetRect;
    }

    public int getChildIndexAtCorePoint(float x, float y) {
        int childCount = this.getChildCount();
        int low = 0;
        int high = childCount - 1;
        if (high == -1) {
            return -1;
        }
        double majorAxisOffset = this.view.isXMajorAxis() ? (double)x : (double)y;
        int luChildCount = this.getUpdateLayoutChildCount();
        if (luChildCount > 0) {
            int firstLUChildIndex = this.getFirstUpdateLayoutChildIndex();
            if (this.getMajorAxisOffset(firstLUChildIndex) <= majorAxisOffset) {
                this.childrenUpdateLayout(high);
                low = firstLUChildIndex;
            } else {
                high = firstLUChildIndex - 1;
            }
        }
        while (low <= high) {
            int mid = (low + high) / 2;
            ViewLayoutState child = this.getChild(mid);
            double midMajorAxisOffset = this.getMajorAxisOffset(mid);
            if (midMajorAxisOffset < majorAxisOffset) {
                low = mid + 1;
                continue;
            }
            if (midMajorAxisOffset > majorAxisOffset) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        if (high < 0) {
            high = 0;
        }
        return high;
    }

    protected void paintChildren(Graphics g, Rectangle alloc) {
        int viewAllocX = alloc.x;
        int viewAllocY = alloc.y;
        if ((alloc = g.getClipBounds(alloc)) == null) {
            return;
        }
        int clipX = alloc.x;
        int clipY = alloc.y;
        boolean isXMajorAxis = this.view.isXMajorAxis();
        int clipEnd = isXMajorAxis ? clipX + alloc.width : clipY + alloc.height;
        int childIndex = this.getChildIndexAtCorePoint(clipX, clipY);
        int childCount = this.getChildCount();
        for (int i = Math.max((int)childIndex, (int)0); i < childCount; ++i) {
            int allocStart;
            ViewLayoutState child = this.getChild(i);
            alloc = this.getChildCoreAllocation(i, alloc);
            alloc.x += viewAllocX;
            alloc.y += viewAllocY;
            int n = allocStart = isXMajorAxis ? alloc.x : alloc.y;
            if (allocStart >= clipEnd) break;
            View v = child.getView();
            v.paint(g, alloc);
        }
    }

    protected final int getFirstRepaintChildIndex() {
        return this.firstRepaintChildIndex;
    }

    final void setFirstRepaintChildIndex(int firstRepaintChildIndex) {
        this.firstRepaintChildIndex = firstRepaintChildIndex;
    }

    final int getMaxMinorAxisPreferredSpanChildIndex() {
        return this.maxMinorAxisPreferredSpanChildIndex;
    }

    void setMaxMinorAxisPreferredSpanChildIndex(int maxMinorAxisPreferredSpanChildIndex) {
        this.maxMinorAxisPreferredSpanChildIndex = maxMinorAxisPreferredSpanChildIndex;
    }

    final int getFirstUpdateLayoutChildIndex() {
        return this.firstUpdateLayoutChildIndex;
    }

    final int getUpdateLayoutChildCount() {
        return this.updateLayoutChildCount;
    }

    void markLayoutInvalid(int firstChildIndex, int count) {
        if (count > 0) {
            if (this.updateLayoutChildCount > 0) {
                int endInvalid = this.firstUpdateLayoutChildIndex + this.updateLayoutChildCount;
                this.firstUpdateLayoutChildIndex = Math.min(this.firstUpdateLayoutChildIndex, firstChildIndex);
                this.updateLayoutChildCount = Math.max(endInvalid, firstChildIndex + count) - this.firstUpdateLayoutChildIndex;
            } else {
                this.firstUpdateLayoutChildIndex = firstChildIndex;
                this.updateLayoutChildCount = count;
                this.view.markLayoutInvalid();
            }
        }
    }

    void removeInvalidChildIndexesArea(int firstChildIndex, int count) {
        int endInvalid;
        int endChildIndex = firstChildIndex + count;
        if (this.updateLayoutChildCount > 0 && (endInvalid = this.firstUpdateLayoutChildIndex + this.updateLayoutChildCount) > firstChildIndex) {
            if (this.firstUpdateLayoutChildIndex >= endChildIndex) {
                this.firstUpdateLayoutChildIndex -= count;
            } else if (this.firstUpdateLayoutChildIndex < firstChildIndex) {
                this.updateLayoutChildCount -= Math.min(endInvalid, endChildIndex) - firstChildIndex;
            } else {
                this.updateLayoutChildCount -= Math.min(endInvalid, endChildIndex) - this.firstUpdateLayoutChildIndex;
                this.firstUpdateLayoutChildIndex = endChildIndex;
                while (this.updateLayoutChildCount > 0 && this.getChild(this.firstUpdateLayoutChildIndex).isFlyweight()) {
                    ++this.firstUpdateLayoutChildIndex;
                    --this.updateLayoutChildCount;
                }
            }
        }
    }

    protected final void childrenUpdateLayout(int tillChildIndex) {
        while (this.updateLayoutChildCount > 0 && this.firstUpdateLayoutChildIndex <= tillChildIndex) {
            --this.updateLayoutChildCount;
            ViewLayoutState child = this.getChild(this.firstUpdateLayoutChildIndex++);
            child.updateLayout();
        }
    }

    protected final void childrenUpdateLayout() {
        this.childrenUpdateLayout(Integer.MAX_VALUE);
    }

    protected void childrenLayout() {
        int childCount = this.getChildCount();
        int maxPreferredSpanChildIndex = -1;
        float maxPreferredSpan = 0.0f;
        for (int i = 0; i < childCount; ++i) {
            ViewLayoutState child = this.getChild(i);
            float span = child.getLayoutMinorAxisPreferredSpan();
            if (span <= maxPreferredSpan) continue;
            maxPreferredSpanChildIndex = i;
            maxPreferredSpan = span;
        }
        this.setMaxMinorAxisPreferredSpanChildIndex(maxPreferredSpanChildIndex);
        if (maxPreferredSpan != this.getMinorAxisPreferredSpan()) {
            this.setMinorAxisPreferredSpan(maxPreferredSpan);
            this.view.markMinorAxisPreferenceChanged();
        }
    }

    protected void unload() {
        int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            this.releaseChild(this.getChild(i));
        }
    }

    private void moveIndexGap(int index) {
        this.checkConsistency();
        int gapLen = this.indexGapLength;
        int belowIndex = index;
        boolean updated = false;
        while (--belowIndex >= 0) {
            ViewLayoutState child = this.getChild(belowIndex);
            if (child.isFlyweight()) continue;
            int rawIndex = child.getViewRawIndex();
            if (rawIndex < gapLen) break;
            child.setViewRawIndex(rawIndex - gapLen);
            updated = true;
        }
        if (!updated) {
            int childCount = this.getChildCount();
            while (index < childCount) {
                ViewLayoutState child;
                if ((child = this.getChild(index++)).isFlyweight()) continue;
                int rawIndex = child.getViewRawIndex();
                if (rawIndex >= gapLen) break;
                child.setViewRawIndex(rawIndex + gapLen);
            }
        }
        this.checkConsistency();
    }

    private void moveMajorAxisOffsetGap(int index) {
        if (index == this.majorAxisOffsetGapIndex) {
            return;
        }
        this.checkConsistency();
        if (index < this.majorAxisOffsetGapIndex) {
            while (--this.majorAxisOffsetGapIndex >= index) {
                ViewLayoutState child = this.getChild(this.majorAxisOffsetGapIndex);
                if (child.isFlyweight()) continue;
                child.setLayoutMajorAxisRawOffset(child.getLayoutMajorAxisRawOffset() + this.majorAxisOffsetGapLength);
            }
            ++this.majorAxisOffsetGapIndex;
        } else {
            while (this.majorAxisOffsetGapIndex < index) {
                ViewLayoutState child = this.getChild(this.majorAxisOffsetGapIndex);
                if (!child.isFlyweight()) {
                    child.setLayoutMajorAxisRawOffset(child.getLayoutMajorAxisRawOffset() - this.majorAxisOffsetGapLength);
                }
                ++this.majorAxisOffsetGapIndex;
            }
        }
        this.checkConsistency();
    }

    private void checkConsistency() {
    }

    private int computeIndexGapStart() {
        int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            if (this.getChild(i).getViewRawIndex() < this.indexGapLength) continue;
            return i;
        }
        return childCount;
    }

    @Override
    public String toStringDetail() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.view.toString());
        sb.append(", indexGapStart=");
        sb.append(this.computeIndexGapStart());
        sb.append(", majorAxisOffsetGapIndex=");
        sb.append(this.majorAxisOffsetGapIndex);
        sb.append(", majorAxisOffsetGapLength=");
        sb.append(this.majorAxisOffsetGapLength);
        sb.append(this.view.childrenToString());
        return sb.toString();
    }
}

