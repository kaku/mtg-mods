/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.netbeans.editor.view.spi.FlyView;

public class ViewUtilitiesImpl {
    private ViewUtilitiesImpl() {
    }

    public static int findViewIndexBounded(View view, int offset) {
        int viewStartOffset = view.getStartOffset();
        if (offset == viewStartOffset) {
            return 0;
        }
        if (offset >= viewStartOffset && offset < view.getEndOffset()) {
            return ViewUtilitiesImpl.findViewIndex(view, offset);
        }
        return -1;
    }

    public static int findViewIndex(View view, int offset) {
        return ViewUtilitiesImpl.findViewIndex(view, offset, null);
    }

    private static int findViewIndex(View view, int offset, OffsetMatchUpdater updater) {
        FlyView.Parent flyParent = view instanceof FlyView.Parent ? (FlyView.Parent)((Object)view) : null;
        int low = 0;
        int high = view.getViewCount() - 1;
        if (high == -1) {
            return -1;
        }
        while (low <= high) {
            int midStartOffset;
            int mid = (low + high) / 2;
            int n = midStartOffset = flyParent != null ? flyParent.getStartOffset(mid) : view.getView(mid).getStartOffset();
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            if (updater != null) {
                mid = updater.updateIndex(mid, offset, view, flyParent);
            }
            return mid;
        }
        if (high < 0) {
            high = 0;
        }
        return high;
    }

    public static int findLowerViewIndex(View view, int offset, boolean lowerAdjacent) {
        LowerOffsetMatchUpdater updater = lowerAdjacent ? LowerOffsetMatchUpdater.adjacent : LowerOffsetMatchUpdater.normal;
        return ViewUtilitiesImpl.findViewIndex(view, offset, updater);
    }

    public static int findUpperViewIndex(View view, int offset, boolean excludeAtOffset) {
        UpperOffsetMatchUpdater updater = excludeAtOffset ? UpperOffsetMatchUpdater.exclude : UpperOffsetMatchUpdater.normal;
        return ViewUtilitiesImpl.findViewIndex(view, offset, updater);
    }

    public static Rectangle maybeNew(Rectangle r, int x, int y, int width, int height) {
        if (r == null || r.x != x || r.y != y || r.width != width || r.height != height) {
            return new Rectangle(x, y, width, height);
        }
        return r;
    }

    public static Rectangle maybeNew(Rectangle origRect, Rectangle testRect) {
        if (origRect == null || !origRect.equals(testRect)) {
            origRect = new Rectangle(testRect);
        }
        return origRect;
    }

    public static String axisToString(int axis) {
        switch (axis) {
            case 0: {
                return "x";
            }
            case 1: {
                return "y";
            }
        }
        return "<invalid-axis-value=" + axis + ">";
    }

    static int getNextVisualPositionFrom(View v, int pos, Position.Bias b, Shape alloc, int direction, Position.Bias[] biasRet) throws BadLocationException {
        int retValue;
        boolean top;
        if (v.getViewCount() == 0) {
            return pos;
        }
        boolean bl = top = direction == 1 || direction == 7;
        if (pos == -1) {
            Shape childBounds;
            int childIndex = top ? v.getViewCount() - 1 : 0;
            View child = v.getView(childIndex);
            retValue = child.getNextVisualPositionFrom(pos, b, childBounds = v.getChildAllocation(childIndex, alloc), direction, biasRet);
            if (retValue == -1 && !top && v.getViewCount() > 1) {
                child = v.getView(1);
                childBounds = v.getChildAllocation(1, alloc);
                retValue = child.getNextVisualPositionFrom(-1, biasRet[0], childBounds, direction, biasRet);
            }
        } else {
            Shape childBounds;
            int increment = top ? -1 : 1;
            int childIndex = b == Position.Bias.Backward && pos > 0 ? v.getViewIndex(pos - 1, Position.Bias.Forward) : v.getViewIndex(pos, Position.Bias.Forward);
            View child = v.getView(childIndex);
            retValue = child.getNextVisualPositionFrom(pos, b, childBounds = v.getChildAllocation(childIndex, alloc), direction, biasRet);
            if (retValue == -1 && childIndex >= 0 && (childIndex += increment) < v.getViewCount()) {
                child = v.getView(childIndex);
                retValue = child.getNextVisualPositionFrom(-1, b, childBounds = v.getChildAllocation(childIndex, alloc), direction, biasRet);
                if (retValue == pos && biasRet[0] != b) {
                    return ViewUtilitiesImpl.getNextVisualPositionFrom(v, pos, biasRet[0], alloc, direction, biasRet);
                }
            } else if (retValue != -1 && biasRet[0] != b && (increment == 1 && child.getEndOffset() == retValue || increment == -1 && child.getStartOffset() == retValue) && childIndex >= 0 && childIndex < v.getViewCount()) {
                child = v.getView(childIndex);
                childBounds = v.getChildAllocation(childIndex, alloc);
                Position.Bias originalBias = biasRet[0];
                int nextPos = child.getNextVisualPositionFrom(-1, b, childBounds, direction, biasRet);
                if (biasRet[0] == b) {
                    retValue = nextPos;
                } else {
                    biasRet[0] = originalBias;
                }
            }
        }
        return retValue;
    }

    public static void checkViewHierarchy(View v) {
        ViewUtilitiesImpl.checkChildrenParent(v);
    }

    private static void checkChildrenParent(View v) {
        int cnt = v.getViewCount();
        for (int i = 0; i < cnt; ++i) {
            View child = v.getView(i);
            View childParent = child.getParent();
            if (childParent != v) {
                throw new IllegalStateException("child=" + child + " has parent=" + childParent + " instead of " + v);
            }
            ViewUtilitiesImpl.checkChildrenParent(child);
        }
    }

    static class UpperOffsetMatchUpdater
    implements OffsetMatchUpdater {
        static final UpperOffsetMatchUpdater normal = new UpperOffsetMatchUpdater(false);
        static final UpperOffsetMatchUpdater exclude = new UpperOffsetMatchUpdater(true);
        private final boolean excludeAtOffset;

        UpperOffsetMatchUpdater(boolean excludeAtOffset) {
            this.excludeAtOffset = excludeAtOffset;
        }

        @Override
        public int updateIndex(int viewIndex, int offset, View view, FlyView.Parent flyParent) {
            int lastViewIndex = view.getViewCount() - 1;
            do {
                int endOffset;
                int n = endOffset = flyParent != null ? flyParent.getEndOffset(viewIndex) : view.getView(viewIndex).getEndOffset();
                if (endOffset != offset) {
                    if (!this.excludeAtOffset) break;
                    --viewIndex;
                    break;
                }
                if (viewIndex == lastViewIndex) break;
                ++viewIndex;
            } while (true);
            return viewIndex;
        }
    }

    static class LowerOffsetMatchUpdater
    implements OffsetMatchUpdater {
        static final LowerOffsetMatchUpdater normal = new LowerOffsetMatchUpdater(false);
        static final LowerOffsetMatchUpdater adjacent = new LowerOffsetMatchUpdater(true);
        private final boolean lowerAdjacent;

        LowerOffsetMatchUpdater(boolean lowerAdjacent) {
            this.lowerAdjacent = lowerAdjacent;
        }

        @Override
        public int updateIndex(int viewIndex, int offset, View view, FlyView.Parent flyParent) {
            while (--viewIndex >= 0) {
                int startOffset = flyParent != null ? flyParent.getStartOffset(viewIndex) : view.getView(viewIndex).getStartOffset();
                if (startOffset == offset) continue;
                if (!this.lowerAdjacent) break;
                --viewIndex;
                break;
            }
            return viewIndex + 1;
        }
    }

    static interface OffsetMatchUpdater {
        public int updateIndex(int var1, int var2, View var3, FlyView.Parent var4);
    }

}

