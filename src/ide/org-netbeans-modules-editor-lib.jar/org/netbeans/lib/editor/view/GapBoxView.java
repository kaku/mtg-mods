/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.ElementUtilities
 */
package org.netbeans.lib.editor.view;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.view.spi.EstimatedSpanView;
import org.netbeans.editor.view.spi.ViewInsets;
import org.netbeans.editor.view.spi.ViewLayoutState;
import org.netbeans.lib.editor.util.swing.ElementUtilities;
import org.netbeans.lib.editor.view.GapBoxViewChildren;
import org.netbeans.lib.editor.view.SimpleViewLayoutState;
import org.netbeans.lib.editor.view.ViewUtilitiesImpl;

public class GapBoxView
extends View
implements ViewLayoutState.Parent,
ViewLayoutState,
EstimatedSpanView {
    private static final Logger LOG = Logger.getLogger(GapBoxView.class.getName());
    private static final int X_MAJOR_AXIS_BIT = 1;
    private static final int MAJOR_AXES_ORTHOGONAL_BIT = 2;
    private static final int MAJOR_AXIS_PREFERENCE_CHANGED_BIT = 4;
    private static final int MINOR_AXIS_PREFERENCE_CHANGED_BIT = 8;
    private static final int CHILDREN_LAYOUT_NECESSARY_BIT = 16;
    private static final int REPAINT_PENDING_BIT = 32;
    private static final int REPAINT_TILL_END_BIT = 64;
    private static final int ESTIMATED_SPAN_BIT = 128;
    private static final int UPDATE_LAYOUT_IN_PROGRESS = 256;
    private static final int ACTIVE_LAYOUT_STATE = 512;
    private static final int LAYOUT_STATE_X_MAJOR_AXIS_BIT = 1024;
    private static final int LAYOUT_STATE_VIEW_SIZE_INVALID_BIT = 2048;
    protected static final int GAP_BOX_VIEW_LAST_USED_STATUS_BIT = 2048;
    private static final int LAYOUT_STATE_ANY_INVALID = 2092;
    private int statusBits;
    private GapBoxViewChildren children;
    private double layoutStateMajorAxisRawOffset;
    private int viewRawIndex;
    private float lastMajorAxisPreferredSpan;
    private float lastMinorAxisPreferredSpan;
    private float minorAxisAssignedSpan;

    public GapBoxView(Element elem, int majorAxis) {
        super(elem);
        if (majorAxis == 0) {
            this.setStatusBits(1);
        }
    }

    @Override
    public float getPreferredSpan(int axis) {
        return axis == this.getMajorAxis() ? (float)this.getMajorAxisPreferredSpan() : this.getMinorAxisPreferredSpan();
    }

    @Override
    public float getMinimumSpan(int axis) {
        return this.getPreferredSpan(axis);
    }

    @Override
    public float getMaximumSpan(int axis) {
        return this.getPreferredSpan(axis);
    }

    @Override
    public float getAlignment(int axis) {
        return 0.0f;
    }

    public ViewInsets getInsets() {
        return null;
    }

    final double getMajorAxisPreferredSpan() {
        return this.children != null ? this.children.getMajorAxisPreferredSpan() + (double)this.getMajorAxisInsetSpan() : (double)this.lastMajorAxisPreferredSpan;
    }

    final float getMinorAxisPreferredSpan() {
        return this.children != null ? this.children.getMinorAxisPreferredSpan() + this.getMinorAxisInsetSpan() : this.lastMinorAxisPreferredSpan;
    }

    final float getMinorAxisAssignedSpan() {
        return this.minorAxisAssignedSpan;
    }

    public final int getMajorAxis() {
        return this.isXMajorAxis() ? 0 : 1;
    }

    public final int getMinorAxis() {
        return this.isXMajorAxis() ? 1 : 0;
    }

    final boolean isXMajorAxis() {
        return this.isStatusBitsNonZero(1);
    }

    final boolean isMajorAxesOrthogonal() {
        return this.isStatusBitsNonZero(2);
    }

    @Override
    public int getViewCount() {
        return this.getChildren().getChildCount();
    }

    @Override
    public View getView(int index) {
        return this.getChild(index).getView();
    }

    @Override
    public void replace(int index, int length, View[] views) {
        if (length < 0) {
            throw new IllegalArgumentException("length=" + length + " < 0");
        }
        if (length == 0 && (views == null || views.length == 0)) {
            return;
        }
        GapBoxViewChildren ch = this.getChildren();
        ch.replace(index, length, views);
    }

    protected int getReplaceEstimatedThreshold() {
        return Integer.MAX_VALUE;
    }

    @Override
    public final boolean isEstimatedSpan() {
        return this.isStatusBitsNonZero(128);
    }

    @Override
    public void setEstimatedSpan(boolean estimatedSpan) {
        if (this.isEstimatedSpan() != estimatedSpan) {
            if (estimatedSpan) {
                this.setStatusBits(128);
            } else {
                int viewCount;
                this.clearStatusBits(128);
                if (this.children != null && (viewCount = this.getViewCount()) > 0) {
                    this.resetEstimatedSpan(0, viewCount);
                }
            }
        }
    }

    protected void resetEstimatedSpan(int childIndex, int count) {
        while (--count >= 0) {
            ViewLayoutState child = this.getChild(childIndex);
            View childView = child.getView();
            if (childView instanceof EstimatedSpanView) {
                ((EstimatedSpanView)((Object)childView)).setEstimatedSpan(false);
            }
            ++childIndex;
        }
    }

    public void rebuild(int index, int count) {
        if (count != 0) {
            int startOffset = index == 0 ? -1 : this.getView(index - 1).getEndOffset();
            int viewCount = this.getViewCount();
            int endIndex = Math.min(index + count, viewCount);
            int endOffset = endIndex == viewCount ? -1 : this.getView(endIndex).getStartOffset();
            boolean loggable = LOG.isLoggable(Level.FINE);
            long tm = 0;
            if (loggable) {
                tm = System.currentTimeMillis();
            }
            this.reloadChildren(index, count, startOffset, endOffset);
            if (loggable) {
                LOG.fine("GapBoxView.rebuild(): " + (System.currentTimeMillis() - tm) + "ms; index=" + index + ", count=" + count + ", <" + startOffset + ", " + endOffset + ">\n");
            }
        }
    }

    public void offsetRebuild(int startOffset, int endOffset) {
        int count;
        int index = ViewUtilitiesImpl.findLowerViewIndex(this, startOffset, false);
        if (index == -1) {
            index = 0;
            count = 0;
        } else {
            count = ViewUtilitiesImpl.findUpperViewIndex(this, endOffset, true) - index + 1;
        }
        this.rebuild(index, count);
    }

    @Override
    public void setParent(View parent) {
        super.setParent(parent);
        if (parent != null) {
            if (parent instanceof ViewLayoutState.Parent) {
                this.setStatusBits(512);
            } else {
                this.clearStatusBits(512);
            }
            this.getChildren();
        } else {
            this.releaseChildren();
            this.clearStatusBits(512);
        }
    }

    public final boolean isActiveLayoutState() {
        return this.isStatusBitsNonZero(512);
    }

    GapBoxViewChildren getChildren() {
        if (this.children == null) {
            this.children = this.createChildren();
            View parent = this.getParent();
            if (parent != null) {
                this.reloadChildren(0, 0, -1, -1);
            }
        }
        return this.children;
    }

    final GapBoxViewChildren getChildrenNull() {
        return this.children;
    }

    public void releaseChildren() {
        if (this.children != null) {
            this.unloadChildren();
            this.children.unload();
            this.children = null;
        }
    }

    @Override
    public final View getView() {
        return this;
    }

    @Override
    public ViewLayoutState selectLayoutMajorAxis(int axis) {
        if (axis == 0) {
            this.setStatusBits(1024);
        } else {
            this.clearStatusBits(1024);
        }
        if (axis == this.getMajorAxis()) {
            this.clearStatusBits(2);
        } else {
            this.setStatusBits(2);
        }
        return this;
    }

    @Override
    public boolean isFlyweight() {
        return false;
    }

    @Override
    public void updateLayout() {
        double delta;
        if (this.isLayoutValid()) {
            return;
        }
        if (this.isStatusBitsNonZero(256)) {
            return;
        }
        this.setStatusBits(256);
        View parent = this.getParent();
        if (parent == null) {
            return;
        }
        ViewLayoutState.Parent lsParent = parent instanceof ViewLayoutState.Parent ? (ViewLayoutState.Parent)((Object)parent) : null;
        this.children.childrenUpdateLayout();
        if (this.isChildrenLayoutNecessary()) {
            this.resetChildrenLayoutNecessary();
            this.children.childrenLayout();
        }
        boolean parentWillRepaint = false;
        boolean majorAxisPreferenceChanged = this.isMajorAxisPreferenceChanged();
        boolean minorAxisPreferenceChanged = this.isMinorAxisPreferenceChanged();
        this.resetAxesPreferenceChanged();
        if (majorAxisPreferenceChanged && this.children != null && (delta = this.updateLastMajorAxisPreferredSpan()) != 0.0 && lsParent != null) {
            if (this.isMajorAxesOrthogonal()) {
                lsParent.minorAxisPreferenceChanged(this);
            } else {
                lsParent.majorAxisPreferenceChanged(this, delta);
                parentWillRepaint = true;
            }
        }
        if (minorAxisPreferenceChanged && this.children != null && (delta = this.updateLastMinorAxisPreferredSpan()) != 0.0 && lsParent != null) {
            if (this.isMajorAxesOrthogonal()) {
                lsParent.majorAxisPreferenceChanged(this, delta);
                parentWillRepaint = true;
            } else {
                lsParent.minorAxisPreferenceChanged(this);
            }
        }
        if (majorAxisPreferenceChanged || minorAxisPreferenceChanged || !this.isActiveLayoutState()) {
            boolean horizontalChange = false;
            boolean verticalChange = false;
            if (this.isXMajorAxis()) {
                horizontalChange = majorAxisPreferenceChanged;
                verticalChange = minorAxisPreferenceChanged;
            } else {
                horizontalChange = minorAxisPreferenceChanged;
                verticalChange = majorAxisPreferenceChanged;
            }
            parent.preferenceChanged(this, horizontalChange, verticalChange);
        }
        if (this.isStatusBitsNonZero(2048)) {
            this.clearStatusBits(2048);
            if (lsParent != null) {
                float height;
                float width;
                float layoutStateMajorAxisSpan = this.getPreferredSpan(this.getLayoutStateMajorAxis());
                float layoutStateMinorAxisSpan = lsParent.getMinorAxisSpan(this);
                if (this.isXLayoutStateMajorAxis()) {
                    width = layoutStateMajorAxisSpan;
                    height = layoutStateMinorAxisSpan;
                } else {
                    width = layoutStateMinorAxisSpan;
                    height = layoutStateMajorAxisSpan;
                }
                this.setSize(width, height);
            }
        }
        if (this.children != null && this.isRepaintPending()) {
            if (!parentWillRepaint) {
                this.processRepaint(lsParent);
            }
            this.resetRepaintPending();
        }
        this.clearStatusBits(256);
        this.updateLayout();
    }

    @Override
    protected void updateLayout(DocumentEvent.ElementChange ec, DocumentEvent e, Shape a) {
    }

    @Override
    public void layoutInvalid(ViewLayoutState child) {
        int childIndex = this.children.getChildIndexNoCheck(child);
        this.children.markLayoutInvalid(childIndex, 1);
    }

    protected void markLayoutInvalid() {
        if (this.isActiveLayoutState()) {
            ((ViewLayoutState.Parent)((Object)this.getParent())).layoutInvalid(this);
        } else {
            this.directUpdateLayout();
        }
    }

    protected void directUpdateLayout() {
        this.updateLayout();
    }

    protected void processRepaint(ViewLayoutState.Parent lsParent) {
        if (lsParent != null) {
            float repaintMinorSpan;
            double repaintMajorOffset;
            float repaintMinorOffset;
            double repaintMajorSpan;
            int firstRepaintChildIndex = this.children.getFirstRepaintChildIndex();
            double majorAxisOffset = this.children.getMajorAxisOffset(firstRepaintChildIndex);
            if (this.isRepaintTillEnd() || firstRepaintChildIndex >= this.getViewCount()) {
                if (this.isMajorAxesOrthogonal()) {
                    repaintMajorOffset = 0.0;
                    repaintMajorSpan = 0.0;
                    repaintMinorOffset = (float)majorAxisOffset;
                    repaintMinorSpan = 0.0f;
                } else {
                    repaintMajorOffset = majorAxisOffset;
                    repaintMajorSpan = 0.0;
                    repaintMinorOffset = 0.0f;
                    repaintMinorSpan = 0.0f;
                }
            } else {
                double majorAxisSpan = this.getChild(firstRepaintChildIndex).getLayoutMajorAxisPreferredSpan();
                if (this.isMajorAxesOrthogonal()) {
                    repaintMajorOffset = 0.0;
                    repaintMajorSpan = 0.0;
                    repaintMinorOffset = (float)majorAxisOffset;
                    repaintMinorSpan = (float)majorAxisSpan;
                } else {
                    repaintMajorOffset = majorAxisOffset;
                    repaintMajorSpan = majorAxisSpan;
                    repaintMinorOffset = 0.0f;
                    repaintMinorSpan = 0.0f;
                }
            }
            lsParent.repaint(this, repaintMajorOffset, repaintMajorSpan, repaintMinorOffset, repaintMinorSpan);
        } else {
            Container c = this.getContainer();
            if (c != null) {
                c.repaint();
            }
        }
    }

    protected boolean markRepaint(int childIndex, boolean repaintTillEnd) {
        boolean lowerIndexMarked = false;
        if (this.children != null) {
            int firstRepaintChildIndex = this.children.getFirstRepaintChildIndex();
            if (!this.isRepaintTillEnd()) {
                if (firstRepaintChildIndex == -1) {
                    lowerIndexMarked = true;
                    this.markRepaintPending();
                    this.children.setFirstRepaintChildIndex(childIndex);
                    if (repaintTillEnd) {
                        this.setStatusBits(64);
                    }
                } else if (firstRepaintChildIndex != childIndex) {
                    if (childIndex < firstRepaintChildIndex) {
                        lowerIndexMarked = true;
                        this.children.setFirstRepaintChildIndex(childIndex);
                    }
                    this.setStatusBits(64);
                } else if (repaintTillEnd) {
                    this.setStatusBits(64);
                }
            } else if (childIndex < firstRepaintChildIndex) {
                lowerIndexMarked = true;
                this.children.setFirstRepaintChildIndex(childIndex);
            }
        }
        return lowerIndexMarked;
    }

    public final boolean isRepaintPending() {
        return this.isStatusBitsNonZero(32);
    }

    protected final void markRepaintPending() {
        this.setStatusBits(32);
    }

    protected void resetRepaintPending() {
        if (this.children != null) {
            this.children.setFirstRepaintChildIndex(-1);
        }
        this.clearStatusBits(96);
    }

    public final boolean isRepaintTillEnd() {
        return this.isStatusBitsNonZero(64);
    }

    protected boolean isLayoutMinorAxisPreferenceChanged(boolean majorAxesOrthogonal) {
        double delta = majorAxesOrthogonal ? this.updateLastMajorAxisPreferredSpan() : this.updateLastMinorAxisPreferredSpan();
        return delta != 0.0;
    }

    private double updateLastMinorAxisPreferredSpan() {
        float currentMinorAxisPreferredSpan = this.children.getMinorAxisPreferredSpan();
        double delta = currentMinorAxisPreferredSpan - this.lastMinorAxisPreferredSpan;
        this.lastMinorAxisPreferredSpan = currentMinorAxisPreferredSpan;
        return delta;
    }

    private double updateLastMajorAxisPreferredSpan() {
        double currentMajorAxisPreferredSpan = this.children.getMajorAxisPreferredSpan();
        double delta = currentMajorAxisPreferredSpan - (double)this.lastMajorAxisPreferredSpan;
        this.lastMajorAxisPreferredSpan = (float)currentMajorAxisPreferredSpan;
        return delta;
    }

    @Override
    public boolean isLayoutValid() {
        return !this.isStatusBitsNonZero(2092) && (this.children == null || this.children.getUpdateLayoutChildCount() == 0);
    }

    @Override
    public double getLayoutMajorAxisPreferredSpan() {
        return this.isMajorAxesOrthogonal() ? (double)this.lastMinorAxisPreferredSpan : (double)this.lastMajorAxisPreferredSpan;
    }

    @Override
    public float getLayoutMinorAxisPreferredSpan() {
        return this.isMajorAxesOrthogonal() ? this.lastMajorAxisPreferredSpan : this.lastMinorAxisPreferredSpan;
    }

    @Override
    public float getLayoutMinorAxisMinimumSpan() {
        return this.getLayoutMinorAxisPreferredSpan();
    }

    @Override
    public float getLayoutMinorAxisMaximumSpan() {
        return this.getLayoutMinorAxisPreferredSpan();
    }

    @Override
    public float getLayoutMinorAxisAlignment() {
        return this.getAlignment(this.getLayoutStateMinorAxis());
    }

    @Override
    public double getLayoutMajorAxisRawOffset() {
        return this.layoutStateMajorAxisRawOffset;
    }

    @Override
    public void setLayoutMajorAxisRawOffset(double majorAxisRawOffset) {
        this.layoutStateMajorAxisRawOffset = majorAxisRawOffset;
    }

    protected final ViewLayoutState.Parent getLayoutStateParent() {
        View parent = this.getParent();
        return parent instanceof ViewLayoutState.Parent ? (ViewLayoutState.Parent)((Object)parent) : null;
    }

    protected final boolean isXLayoutStateMajorAxis() {
        return this.isStatusBitsNonZero(1024);
    }

    protected final int getLayoutStateMajorAxis() {
        return this.isStatusBitsNonZero(1024) ? 0 : 1;
    }

    protected final int getLayoutStateMinorAxis() {
        return this.isStatusBitsNonZero(1024) ? 1 : 0;
    }

    @Override
    public int getViewRawIndex() {
        return this.viewRawIndex;
    }

    @Override
    public void setViewRawIndex(int viewRawIndex) {
        this.viewRawIndex = viewRawIndex;
    }

    @Override
    public void viewPreferenceChanged(boolean width, boolean height) {
        this.markViewSizeInvalid();
    }

    @Override
    public void markViewSizeInvalid() {
        this.setStatusBits(2048);
    }

    @Override
    public void majorAxisPreferenceChanged(ViewLayoutState child, double majorAxisSpanDelta) {
        int childIndex = this.getChildIndexNoCheck(child);
        if (majorAxisSpanDelta != 0.0) {
            this.markRepaint(childIndex, true);
            this.children.majorAxisPreferenceChanged(child, childIndex, majorAxisSpanDelta);
        } else {
            this.markRepaint(childIndex, false);
        }
    }

    @Override
    public void minorAxisPreferenceChanged(ViewLayoutState child) {
        int childIndex = this.getChildIndexNoCheck(child);
        this.markRepaint(childIndex, false);
        this.children.minorAxisPreferenceChanged(child, childIndex);
    }

    @Override
    public float getMinorAxisSpan(ViewLayoutState child) {
        return this.getChildren().getMinorAxisSpan(child);
    }

    @Override
    public void repaint(ViewLayoutState child, double majorAxisOffset, double majorAxisSpan, float minorAxisOffset, float minorAxisSpan) {
        int childIndex = this.getChildIndexNoCheck(child);
        this.markRepaint(childIndex, false);
    }

    public final boolean isChildrenLayoutNecessary() {
        return this.isStatusBitsNonZero(16);
    }

    public final void markChildrenLayoutNecessary() {
        this.setStatusBits(16);
    }

    final void resetChildrenLayoutNecessary() {
        this.clearStatusBits(16);
    }

    @Override
    public void preferenceChanged(View childView, boolean width, boolean height) {
        if (childView == null) {
            this.getParent().preferenceChanged(this, width, height);
        } else {
            int index = childView instanceof ViewLayoutState ? this.getChildIndexNoCheck((ViewLayoutState)((Object)childView)) : this.getViewIndex(childView.getStartOffset());
            ViewLayoutState child = this.getChild(index);
            if (child.getView() != childView) {
                int ind;
                for (ind = this.getViewCount() - 1; ind >= 0 && this.getView(ind) != childView; --ind) {
                }
                if (ind == -1) {
                    throw new IllegalArgumentException("childView=" + childView + " not child of view " + this);
                }
                throw new IllegalStateException("Internal error. Child expected at index=" + index + " but found at index=" + ind);
            }
            child.viewPreferenceChanged(width, height);
            this.children.markLayoutInvalid(index, 1);
        }
    }

    @Override
    public void setSize(float width, float height) {
        float targetMinorAxisSpan;
        float targetMajorAxisSpan;
        if (this.isXMajorAxis()) {
            targetMajorAxisSpan = width;
            targetMinorAxisSpan = height;
        } else {
            targetMajorAxisSpan = height;
            targetMinorAxisSpan = width;
        }
        this.setSpanOnMajorAxis(targetMajorAxisSpan);
        this.setSpanOnMinorAxis(targetMinorAxisSpan);
    }

    protected void setSpanOnMajorAxis(float targetMajorAxisSpan) {
    }

    protected void setSpanOnMinorAxis(float targetMinorAxisSpan) {
        if (targetMinorAxisSpan != this.minorAxisAssignedSpan) {
            int viewCount;
            this.minorAxisAssignedSpan = targetMinorAxisSpan;
            if (!this.isEstimatedSpan() && !this.isChildrenResizeDisabled() && (viewCount = this.getViewCount()) != 0) {
                this.markSizeInvalid(0, viewCount);
            }
        }
    }

    protected void markSizeInvalid(int childIndex, int count) {
        while (--count >= 0) {
            ViewLayoutState child = this.getChild(childIndex);
            if (!child.isFlyweight()) {
                child.markViewSizeInvalid();
            }
            ++childIndex;
        }
        this.children.markLayoutInvalid(childIndex, count);
    }

    protected boolean isChildrenResizeDisabled() {
        return false;
    }

    @Override
    public Shape getChildAllocation(int index, Shape a) {
        if (a == null) {
            return null;
        }
        Rectangle alloc = this.reallocate(a);
        int thisViewAllocX = alloc.x;
        int thisViewAllocY = alloc.y;
        this.getChildren().getChildCoreAllocation(index, alloc);
        alloc.x += thisViewAllocX;
        alloc.y += thisViewAllocY;
        ViewInsets insets = this.getInsets();
        if (insets != null) {
            alloc.x = (int)((float)alloc.x + insets.getLeft());
            alloc.y = (int)((float)alloc.y + insets.getRight());
        }
        return alloc;
    }

    public int getViewIndexAtPoint(float x, float y, Shape a) {
        Rectangle alloc = this.reallocate(a);
        x -= (float)alloc.x;
        y -= (float)alloc.y;
        ViewInsets insets = this.getInsets();
        if (insets != null) {
            x -= insets.getLeft();
            y -= insets.getRight();
        }
        return this.getChildren().getChildIndexAtCorePoint(x, y);
    }

    @Override
    public int getViewIndex(int offset, Position.Bias b) {
        if (b == Position.Bias.Backward) {
            --offset;
        }
        return this.getViewIndex(offset);
    }

    public int getViewIndex(int offset) {
        return ViewUtilitiesImpl.findViewIndexBounded(this, offset);
    }

    @Override
    public void paint(Graphics g, Shape a) {
        Rectangle alloc = this.reallocate(a);
        this.getChildren().paintChildren(g, alloc);
    }

    @Override
    public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
        int index = this.getViewIndex(pos, b);
        if (index >= 0) {
            Shape ca = this.getChildAllocation(index, a);
            ViewLayoutState child = this.getChild(index);
            View cv = child.getView();
            return cv.modelToView(pos, ca, b);
        }
        Document doc = this.getDocument();
        int docLen = doc != null ? doc.getLength() : -1;
        throw new BadLocationException("Offset " + pos + " with bias " + b + " is outside of the view" + ", children = " + this.getViewCount() + (this.getViewCount() > 0 ? new StringBuilder().append(" covering offsets <").append(this.getView(0).getStartOffset()).append(", ").append(this.getView(this.getViewCount() - 1).getEndOffset()).append(">").toString() : "") + ", docLen=" + docLen, pos);
    }

    @Override
    public int viewToModel(float x, float y, Shape a, Position.Bias[] biasReturn) {
        int pos;
        int index = this.getViewIndexAtPoint(x, y, a);
        if ((index = Math.max(index, 0)) < this.getViewCount()) {
            Shape ca = this.getChildAllocation(index, a);
            ViewLayoutState child = this.getChild(index);
            View v = child.getView();
            pos = v.viewToModel(x, y, ca, biasReturn);
        } else {
            int endOff = this.getEndOffset();
            Document doc = this.getDocument();
            pos = doc != null && doc.getLength() < endOff ? doc.getLength() : endOff;
        }
        return pos;
    }

    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        return ViewUtilitiesImpl.getNextVisualPositionFrom(this, pos, b, a, direction, biasRet);
    }

    protected final ViewLayoutState getChild(int index) {
        return this.getChildren().getChild(index);
    }

    protected final int getChildIndex(ViewLayoutState child) {
        return this.getChildren().getChildIndex(child);
    }

    protected final int getChildIndexNoCheck(ViewLayoutState child) {
        return this.getChildren().getChildIndexNoCheck(child);
    }

    GapBoxViewChildren createChildren() {
        return new GapBoxViewChildren(this);
    }

    protected boolean useCustomReloadChildren() {
        return this.getElement() == null;
    }

    @Override
    public void insertUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        if (this.children == null && this.getParent() == null) {
            return;
        }
        if (this.useCustomReloadChildren()) {
            this.customInsertUpdate(evt, a, f);
        } else {
            super.insertUpdate(evt, a, f);
        }
    }

    protected void customInsertUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        int[] offsetRange = this.getInsertUpdateRebuildOffsetRange(evt);
        if (offsetRange != null) {
            this.offsetRebuild(offsetRange[0], offsetRange[1]);
        } else {
            this.forwardUpdate(null, evt, a, f);
        }
    }

    protected int[] getInsertUpdateRebuildOffsetRange(DocumentEvent evt) {
        DocumentEvent.ElementChange lineChange = evt.getChange(evt.getDocument().getDefaultRootElement());
        if (lineChange == null) {
            return null;
        }
        int startOffset = evt.getOffset();
        int endOffset = startOffset + evt.getLength();
        int[] offsetRange = new int[]{startOffset, endOffset};
        Element[] addedLines = lineChange.getChildrenAdded();
        ElementUtilities.updateOffsetRange((Element[])addedLines, (int[])offsetRange);
        Element[] removedLines = lineChange.getChildrenRemoved();
        ElementUtilities.updateOffsetRange((Element[])removedLines, (int[])offsetRange);
        return offsetRange;
    }

    @Override
    public void removeUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        if (this.children == null && this.getParent() == null) {
            return;
        }
        if (this.useCustomReloadChildren()) {
            this.customRemoveUpdate(evt, a, f);
        } else {
            super.removeUpdate(evt, a, f);
        }
    }

    protected void customRemoveUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        int[] offsetRange = this.getRemoveUpdateRebuildOffsetRange(evt);
        if (offsetRange != null) {
            this.offsetRebuild(offsetRange[0], offsetRange[1]);
        } else {
            this.forwardUpdate(null, evt, a, f);
        }
    }

    protected int[] getRemoveUpdateRebuildOffsetRange(DocumentEvent evt) {
        int startOffset;
        DocumentEvent.ElementChange lineChange = evt.getChange(evt.getDocument().getDefaultRootElement());
        if (lineChange == null) {
            return null;
        }
        int endOffset = startOffset = evt.getOffset();
        int[] offsetRange = new int[]{startOffset, endOffset};
        Element[] addedLines = lineChange.getChildrenAdded();
        ElementUtilities.updateOffsetRange((Element[])addedLines, (int[])offsetRange);
        Element[] removedLines = lineChange.getChildrenRemoved();
        ElementUtilities.updateOffsetRange((Element[])removedLines, (int[])offsetRange);
        return offsetRange;
    }

    @Override
    public void changedUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        if (this.children == null && this.getParent() == null) {
            return;
        }
        super.changedUpdate(e, a, f);
    }

    protected void reloadChildren(int index, int removeLength, int startOffset, int endOffset) {
        if (this.useCustomReloadChildren()) {
            if (startOffset == -1) {
                startOffset = this.getStartOffset();
            }
            if (endOffset == -1) {
                endOffset = this.getEndOffset();
            }
            this.customReloadChildren(index, removeLength, startOffset, endOffset);
        } else {
            int startIndex;
            Element elem = this.getElement();
            if (startOffset == -1) {
                startIndex = 0;
            } else {
                if (index == 0 ? startOffset != this.getStartOffset() : startOffset != this.getView(index - 1).getEndOffset()) {
                    throw new IllegalArgumentException("Invalid startOffset=" + startOffset);
                }
                startIndex = index;
            }
            int endIndex = endOffset == -1 ? elem.getElementCount() : elem.getElementIndex(endOffset - 1) + 1;
            this.elementReloadChildren(index, removeLength, endIndex - startIndex);
        }
    }

    protected void elementReloadChildren(int index, int removeLength, int elementCount) {
        Element e = this.getElement();
        View[] added = null;
        ViewFactory f = this.getViewFactory();
        if (f != null) {
            added = new View[elementCount];
            for (int i = 0; i < elementCount; ++i) {
                added[i] = f.create(e.getElement(index + i));
            }
        }
        this.replace(index, removeLength, added);
    }

    protected void customReloadChildren(int index, int removeLength, int startOffset, int endOffset) {
        View[] added = null;
        ViewFactory f = this.getViewFactory();
        if (f != null) {
            int elementIndex;
            Element elem = this.getElement();
            int elementCount = elem.getElementCount();
            int n = elementIndex = elem != null ? elem.getElementIndex(startOffset) : -1;
            if (elementIndex >= elementCount) {
                return;
            }
            ArrayList<View> childViews = new ArrayList<View>();
            int viewCount = this.getViewCount();
            block0 : while (startOffset < endOffset) {
                View childView = this.createCustomView(f, startOffset, endOffset, elementIndex);
                if (childView == null) {
                    throw new IllegalStateException("No view created for area (" + startOffset + ", " + endOffset + ")");
                }
                childViews.add(childView);
                int childViewEndOffset = childView.getEndOffset();
                while (childViewEndOffset > endOffset && index + removeLength < viewCount) {
                    endOffset = this.getView(index + removeLength).getEndOffset();
                    ++removeLength;
                    if (!LOG.isLoggable(Level.FINE)) continue;
                    LOG.fine("GapBoxView.customReloadChildren(): Increased removeLength to " + removeLength + ", eo=" + endOffset);
                }
                Element childElem = elem.getElement(elementIndex);
                while (childElem.getEndOffset() <= childViewEndOffset) {
                    if (++elementIndex == elementCount) break block0;
                    childElem = elem.getElement(elementIndex);
                }
                startOffset = childViewEndOffset;
            }
            added = new View[childViews.size()];
            childViews.toArray(added);
        }
        this.replace(index, removeLength, added);
    }

    protected View createCustomView(ViewFactory f, int startOffset, int maxEndOffset, int elementIndex) {
        return null;
    }

    protected void unloadChildren() {
    }

    protected ViewLayoutState createChild(View v) {
        ViewLayoutState child = v instanceof ViewLayoutState ? (ViewLayoutState)((Object)v) : this.createDefaultChild(v);
        return child;
    }

    protected ViewLayoutState createDefaultChild(View v) {
        return new SimpleViewLayoutState(v);
    }

    protected final boolean isMajorAxisPreferenceChanged() {
        return this.isStatusBitsNonZero(4);
    }

    protected void markMajorAxisPreferenceChanged() {
        this.setStatusBits(4);
    }

    protected final boolean isMinorAxisPreferenceChanged() {
        return this.isStatusBitsNonZero(8);
    }

    protected void markMinorAxisPreferenceChanged() {
        this.setStatusBits(8);
    }

    protected final void resetAxesPreferenceChanged() {
        this.clearStatusBits(12);
    }

    protected final float getInsetSpan(int axis) {
        ViewInsets insets = this.getInsets();
        return insets != null ? (axis == 0 ? insets.getLeftRight() : insets.getTopBottom()) : 0.0f;
    }

    protected final float getMajorAxisInsetSpan() {
        ViewInsets insets = this.getInsets();
        return insets != null ? (this.isXMajorAxis() ? insets.getLeftRight() : insets.getTopBottom()) : 0.0f;
    }

    protected final float getMinorAxisInsetSpan() {
        ViewInsets insets = this.getInsets();
        return insets != null ? (this.isXMajorAxis() ? insets.getTopBottom() : insets.getLeftRight()) : 0.0f;
    }

    protected final int getStatusBits(int bits) {
        return this.statusBits & bits;
    }

    protected final boolean isStatusBitsNonZero(int bits) {
        return this.getStatusBits(bits) != 0;
    }

    protected final void setStatusBits(int bits) {
        this.statusBits |= bits;
    }

    protected final void clearStatusBits(int bits) {
        this.statusBits &= ~ bits;
    }

    protected Rectangle reallocate(Shape a) {
        Rectangle alloc = a.getBounds();
        this.setSize(alloc.width, alloc.height);
        return alloc;
    }

    public int getStartOffset(int childViewIndex) {
        return this.getChildren().getChildStartOffset(childViewIndex);
    }

    public int getEndOffset(int childViewIndex) {
        return this.getChildren().getChildEndOffset(childViewIndex);
    }

    public String childToString(int childIndex) {
        StringBuffer sb = new StringBuffer();
        this.appendChildToStringBuffer(sb, childIndex, 0);
        return sb.toString();
    }

    public void appendChildToStringBuffer(StringBuffer sb, int childIndex, int indent) {
        ViewLayoutState child = this.getChild(childIndex);
        View childView = child.getView();
        Document doc = this.getDocument();
        boolean isFly = child.isFlyweight();
        boolean isEstimated = childView instanceof EstimatedSpanView && ((EstimatedSpanView)((Object)childView)).isEstimatedSpan();
        boolean layoutValid = child.isLayoutValid();
        double offset = this.children.getMajorAxisOffset(childIndex);
        boolean indexesDiffer = !isFly && this.getChildIndexNoCheck(child) != childIndex;
        boolean showRaw = false;
        sb.append(isFly ? 'F' : 'R');
        sb.append(':');
        if (indexesDiffer) {
            sb.append(" WRONG-INDEX=" + this.getChildIndexNoCheck(child));
        }
        if (showRaw) {
            sb.append("rI=" + child.getViewRawIndex());
        }
        sb.append('<');
        GapBoxView.appendOffsetInfo(sb, doc, childView.getStartOffset());
        sb.append(',');
        GapBoxView.appendOffsetInfo(sb, doc, childView.getEndOffset());
        sb.append('>');
        sb.append(", major=").append(child.getLayoutMajorAxisPreferredSpan());
        sb.append("(off=").append(offset);
        if (showRaw) {
            sb.append('(').append(child.getLayoutMajorAxisRawOffset()).append(')');
        }
        sb.append("), minor[pref=").append(child.getLayoutMinorAxisPreferredSpan());
        sb.append(", min=").append(child.getLayoutMinorAxisMinimumSpan());
        sb.append(", max=").append(child.getLayoutMinorAxisMaximumSpan());
        sb.append("] ");
        sb.append(isEstimated ? "E" : "");
        sb.append(layoutValid ? "" : "I");
        if (childView instanceof GapBoxView) {
            sb.append("\n");
            GapBoxView.appendSpaces(sb, indent + 4);
            sb.append("VIEW: ");
            sb.append(childView.toString());
            sb.append(((GapBoxView)childView).childrenToString(indent + 4));
        }
    }

    private static void appendOffsetInfo(StringBuffer sb, Document doc, int offset) {
        sb.append(offset);
        sb.append('[');
        sb.append(Utilities.debugPosition((BaseDocument)doc, offset));
        sb.append(']');
    }

    private static void appendSpaces(StringBuffer sb, int spaceCount) {
        while (--spaceCount >= 0) {
            sb.append(' ');
        }
    }

    public String childrenToString() {
        return this.childrenToString(0);
    }

    public String childrenToString(int indent) {
        StringBuffer sb = new StringBuffer();
        int viewCount = this.getViewCount();
        int totalDigitCount = Integer.toString(viewCount).length();
        for (int i = 0; i < viewCount; ++i) {
            sb.append('\n');
            String iToString = Integer.toString(i);
            GapBoxView.appendSpaces(sb, indent + (totalDigitCount - iToString.length()));
            sb.append('[');
            sb.append(iToString);
            sb.append("]: ");
            this.appendChildToStringBuffer(sb, i, indent);
        }
        return sb.toString();
    }

    public String toString() {
        return "lastMajorAxisPreferredSpan=" + this.lastMajorAxisPreferredSpan + ", lastMinorAxisPreferredSpan=" + this.lastMinorAxisPreferredSpan + ", minorAxisAssignedSpan=" + this.getMinorAxisAssignedSpan();
    }
}

