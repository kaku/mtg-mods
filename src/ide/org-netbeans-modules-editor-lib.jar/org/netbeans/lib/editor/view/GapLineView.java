/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import javax.swing.text.Element;
import org.netbeans.lib.editor.view.GapBoxView;
import org.netbeans.lib.editor.view.GapBoxViewChildren;
import org.netbeans.lib.editor.view.GapLineViewChildren;

public class GapLineView
extends GapBoxView {
    public GapLineView(Element lineElement) {
        super(lineElement, 0);
    }

    @Override
    GapBoxViewChildren createChildren() {
        return new GapLineViewChildren(this);
    }
}

