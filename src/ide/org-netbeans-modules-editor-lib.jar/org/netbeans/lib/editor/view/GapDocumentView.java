/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.lib.editor.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;
import java.io.PrintStream;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.editor.view.spi.EstimatedSpanView;
import org.netbeans.editor.view.spi.LockView;
import org.netbeans.editor.view.spi.ViewLayoutQueue;
import org.netbeans.editor.view.spi.ViewLayoutState;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.view.GapBoxView;
import org.netbeans.lib.editor.view.GapBoxViewChildren;
import org.netbeans.lib.editor.view.GapDocumentViewChildren;
import org.openide.util.WeakListeners;

public class GapDocumentView
extends GapBoxView {
    private static final boolean debugPaint = Boolean.getBoolean("netbeans.debug.editor.view.paint");
    private static final boolean debugRepaint = Boolean.getBoolean("netbeans.debug.editor.view.repaint");
    private static final int ASYNC_CHILDREN_UPDATE_COUNT = 20;
    private static final int CHILDREN_UPDATE_SUBTASK_COUNT = 50;
    private ChildrenUpdateTask childrenUpdateTask;
    private int lastAllocationX;
    private int lastAllocationY;
    private int lastAllocationWidth;
    private int lastAllocationHeight;
    private double firstRepaintChildYSubOffset;
    private double firstRepaintChildYSubSpan;
    private float firstRepaintChildXSubOffset;
    private int layoutLockDepth;
    private boolean pendingUpdate;
    private DocumentListener earlyDocListener;
    private int damageRangeStartOffset;
    private int damageRangeEndOffset;
    private final boolean hideBottomPadding;
    private JViewport viewport;

    public GapDocumentView(Element elem) {
        this(elem, false);
    }

    public GapDocumentView(Element elem, boolean hideBottomPadding) {
        super(elem, 1);
        this.hideBottomPadding = hideBottomPadding;
        this.clearDamageRangeBounds();
    }

    @Override
    public float getPreferredSpan(int axis) {
        float span = super.getPreferredSpan(axis);
        if (!this.hideBottomPadding && axis == 1) {
            Container c;
            if (this.viewport == null && (c = this.getContainer()) != null) {
                this.viewport = (JViewport)SwingUtilities.getAncestorOfClass(JViewport.class, c);
            }
            if (this.viewport != null) {
                int viewportHeight = this.viewport.getExtentSize().height;
                span += (float)(viewportHeight / 3);
            }
        }
        return span;
    }

    @Override
    GapBoxViewChildren createChildren() {
        return new GapDocumentViewChildren(this);
    }

    @Override
    protected Rectangle reallocate(Shape a) {
        Rectangle alloc = super.reallocate(a);
        this.lastAllocationX = alloc.x;
        this.lastAllocationY = alloc.y;
        this.lastAllocationWidth = alloc.width;
        this.lastAllocationHeight = alloc.height;
        return alloc;
    }

    @Override
    protected void directUpdateLayout() {
        if (this.layoutLockDepth == 0) {
            super.directUpdateLayout();
        }
    }

    protected final void layoutLock() {
        ++this.layoutLockDepth;
    }

    protected final void layoutUnlock() {
        --this.layoutLockDepth;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void renderWithUpdateLayout(Runnable r) {
        ++this.layoutLockDepth;
        try {
            r.run();
        }
        finally {
            this.updateLayout();
            --this.layoutLockDepth;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setParent(View parent) {
        ++this.layoutLockDepth;
        try {
            Document doc;
            super.setParent(parent);
            if (parent != null && (doc = this.getDocument()) != null) {
                this.earlyDocListener = new DocumentListener(){

                    @Override
                    public void insertUpdate(DocumentEvent e) {
                        GapDocumentView.this.markPendingUpdate();
                    }

                    @Override
                    public void removeUpdate(DocumentEvent e) {
                        GapDocumentView.this.markPendingUpdate();
                    }

                    @Override
                    public void changedUpdate(DocumentEvent e) {
                    }
                };
                if (!DocumentUtilities.addPriorityDocumentListener((Document)doc, (DocumentListener)WeakListeners.document((DocumentListener)this.earlyDocListener, (Object)doc), (DocumentListenerPriority)DocumentListenerPriority.FIRST)) {
                    this.earlyDocListener = null;
                }
            }
        }
        finally {
            this.updateLayout();
            --this.layoutLockDepth;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setSize(float width, float height) {
        ++this.layoutLockDepth;
        try {
            super.setSize(width, height);
        }
        finally {
            this.updateLayout();
            --this.layoutLockDepth;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        ++this.layoutLockDepth;
        try {
            this.pendingUpdate = false;
            super.insertUpdate(evt, a, f);
        }
        finally {
            this.updateLayout();
            this.checkPendingDamageRange();
            --this.layoutLockDepth;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeUpdate(DocumentEvent evt, Shape a, ViewFactory f) {
        ++this.layoutLockDepth;
        try {
            this.pendingUpdate = false;
            super.removeUpdate(evt, a, f);
        }
        finally {
            this.updateLayout();
            this.checkPendingDamageRange();
            --this.layoutLockDepth;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void changedUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        ++this.layoutLockDepth;
        try {
            super.changedUpdate(e, a, f);
        }
        finally {
            this.updateLayout();
            --this.layoutLockDepth;
        }
    }

    public boolean checkDamageRange(int startOffset, int endOffset, Position.Bias startBias, Position.Bias endBias) {
        if (this.pendingUpdate) {
            this.damageRangeStartOffset = Math.min(this.damageRangeStartOffset, startOffset);
            this.damageRangeEndOffset = Math.max(this.damageRangeEndOffset, endOffset);
            return false;
        }
        return true;
    }

    public boolean isPendingUpdate() {
        return this.pendingUpdate;
    }

    void markPendingUpdate() {
        this.pendingUpdate = true;
    }

    void checkPendingDamageRange() {
        Document doc;
        if (this.damageRangeStartOffset != Integer.MAX_VALUE && (doc = this.getDocument()) != null) {
            this.damageRangeStartOffset = Math.max(this.damageRangeStartOffset, doc.getLength());
            this.damageRangeEndOffset = Math.max(this.damageRangeEndOffset, doc.getLength());
            JTextComponent component = (JTextComponent)this.getContainer();
            if (component != null) {
                component.getUI().damageRange(component, this.damageRangeStartOffset, this.damageRangeEndOffset);
            }
        }
        this.clearDamageRangeBounds();
    }

    private void clearDamageRangeBounds() {
        this.damageRangeStartOffset = Integer.MAX_VALUE;
        this.damageRangeEndOffset = -1;
        this.pendingUpdate = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paint(Graphics g, Shape a) {
        if (debugPaint) {
            System.err.println("VIEW-PAINT: clip=" + g.getClipBounds() + ", alloc=" + a);
        }
        ++this.layoutLockDepth;
        try {
            super.paint(g, a);
        }
        finally {
            this.updateLayout();
            --this.layoutLockDepth;
        }
    }

    @Override
    public void repaint(ViewLayoutState child, double majorAxisOffset, double majorAxisSpan, float minorAxisOffset, float minorAxisSpan) {
        int childIndex = this.getChildIndexNoCheck(child);
        if (this.markRepaint(childIndex, false)) {
            this.firstRepaintChildYSubOffset = majorAxisOffset;
            this.firstRepaintChildXSubOffset = minorAxisOffset;
        }
    }

    @Override
    protected boolean markRepaint(int childIndex, boolean repaintTillEnd) {
        boolean lowerIndexMarked = super.markRepaint(childIndex, repaintTillEnd);
        if (lowerIndexMarked) {
            this.firstRepaintChildYSubOffset = 0.0;
            this.firstRepaintChildXSubOffset = 0.0f;
        }
        return lowerIndexMarked;
    }

    @Override
    protected void processRepaint(ViewLayoutState.Parent lsParent) {
        int firstRepaintChildIndex = this.getChildren().getFirstRepaintChildIndex();
        if (firstRepaintChildIndex >= 0 && firstRepaintChildIndex < this.getViewCount()) {
            Container c;
            int repaintX;
            int repaintHeight;
            double repY = this.getChildren().getMajorAxisOffset(firstRepaintChildIndex);
            int repaintY = (int)Math.floor(repY += this.firstRepaintChildYSubOffset);
            if (this.isRepaintTillEnd()) {
                repaintX = 0;
                repaintHeight = this.lastAllocationHeight;
            } else {
                repaintX = (int)Math.floor(this.firstRepaintChildXSubOffset);
                double repYEnd = repY + this.getChild(firstRepaintChildIndex).getLayoutMajorAxisPreferredSpan();
                repaintHeight = (int)Math.ceil(repYEnd) - repaintY;
            }
            int repaintWidth = this.lastAllocationWidth - repaintX;
            repaintX += this.lastAllocationX;
            if (debugRepaint) {
                System.err.println("REPAINT(childIndex=" + firstRepaintChildIndex + ", rect(" + repaintX + ", " + repaintY + ", " + repaintWidth + ", " + repaintHeight + "))");
            }
            if ((c = this.getContainer()) != null) {
                c.repaint(repaintX, repaintY, repaintWidth, repaintHeight);
            }
        }
    }

    ChildrenUpdateTask getChildrenUpdateTask() {
        if (this.childrenUpdateTask == null) {
            this.childrenUpdateTask = new ChildrenUpdateTask();
        }
        return this.childrenUpdateTask;
    }

    @Override
    protected void resetEstimatedSpan(int childIndex, int count) {
        if (count >= 20) {
            ChildrenUpdateTask updateTask = this.getChildrenUpdateTask();
            updateTask.markResetChildEstimatedSpan();
            updateTask.setChildIndex(childIndex);
            if (!updateTask.isRunning()) {
                updateTask.start();
            }
        } else {
            super.resetEstimatedSpan(childIndex, count);
        }
    }

    @Override
    protected void markSizeInvalid(int childIndex, int count) {
        if (count >= 20) {
            ChildrenUpdateTask updateTask = this.getChildrenUpdateTask();
            updateTask.markUpdateChildSize();
            updateTask.setChildIndex(0);
            if (!updateTask.isRunning()) {
                updateTask.start();
            }
        } else {
            super.markSizeInvalid(childIndex, count);
        }
    }

    protected final int getLastAllocationX() {
        return this.lastAllocationX;
    }

    protected final int getLastAllocationY() {
        return this.lastAllocationY;
    }

    protected final int getLastAllocationWidth() {
        return this.lastAllocationWidth;
    }

    protected final int getLastAllocationHeight() {
        return this.lastAllocationHeight;
    }

    protected ViewLayoutQueue getLayoutQueue() {
        return ViewLayoutQueue.getDefaultQueue();
    }

    final class ChildrenUpdateTask
    implements Runnable {
        private int childIndex;
        private boolean running;
        private boolean updateChildSize;
        private boolean resetChildEstimatedSpan;

        ChildrenUpdateTask() {
            this.childIndex = Integer.MAX_VALUE;
        }

        void markUpdateChildSize() {
            this.updateChildSize = true;
        }

        void markResetChildEstimatedSpan() {
            this.resetChildEstimatedSpan = true;
        }

        void start() {
            this.running = true;
            GapDocumentView.this.getLayoutQueue().addTask(this);
        }

        boolean isRunning() {
            return this.running;
        }

        private void finish() {
            this.running = false;
            this.updateChildSize = false;
            this.resetChildEstimatedSpan = false;
            this.childIndex = Integer.MAX_VALUE;
        }

        void setChildIndex(int childIndex) {
            if (childIndex < this.childIndex) {
                this.childIndex = childIndex;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            AbstractDocument doc = (AbstractDocument)GapDocumentView.this.getDocument();
            if (doc != null) {
                doc.readLock();
                try {
                    LockView lockView = LockView.get(GapDocumentView.this);
                    if (lockView != null) {
                        lockView.lock();
                        try {
                            GapDocumentView.this.layoutLock();
                            try {
                                this.updateView(lockView);
                            }
                            finally {
                                GapDocumentView.this.updateLayout();
                                GapDocumentView.this.layoutUnlock();
                            }
                        }
                        finally {
                            lockView.unlock();
                        }
                    }
                }
                finally {
                    doc.readUnlock();
                }
            }
        }

        private void updateView(LockView lockView) {
            if (GapDocumentView.this.getContainer() == null) {
                this.finish();
                return;
            }
            int viewCount = GapDocumentView.this.getViewCount();
            int updateCount = Math.max(1, viewCount / 50);
            while (updateCount > 0 && this.childIndex < viewCount && !lockView.isPriorityThreadWaiting()) {
                ViewLayoutState child = GapDocumentView.this.getChild(this.childIndex);
                if (!child.isFlyweight()) {
                    View childView = child.getView();
                    if (this.resetChildEstimatedSpan && childView instanceof EstimatedSpanView) {
                        ((EstimatedSpanView)((Object)childView)).setEstimatedSpan(false);
                    }
                    if (this.updateChildSize) {
                        child.markViewSizeInvalid();
                    }
                    child.updateLayout();
                    --updateCount;
                }
                ++this.childIndex;
            }
            if (this.childIndex < viewCount) {
                GapDocumentView.this.getLayoutQueue().addTask(this);
            } else {
                this.finish();
            }
        }
    }

}

