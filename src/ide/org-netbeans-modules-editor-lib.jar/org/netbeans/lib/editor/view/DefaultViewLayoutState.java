/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import javax.swing.text.View;
import org.netbeans.lib.editor.view.SimpleViewLayoutState;

public class DefaultViewLayoutState
extends SimpleViewLayoutState {
    private float layoutMinorAxisMinimumSpan;
    private float layoutMinorAxisMaximumSpan;

    public DefaultViewLayoutState(View v) {
        super(v);
    }

    @Override
    protected boolean minorAxisUpdateLayout(int minorAxis) {
        View view = this.getView();
        boolean minorAxisPreferenceChanged = false;
        float val = view.getMaximumSpan(minorAxis);
        if (val != this.getLayoutMinorAxisMaximumSpan()) {
            this.setLayoutMinorAxisMaximumSpan(val);
            minorAxisPreferenceChanged = true;
        }
        if ((val = view.getMinimumSpan(minorAxis)) != this.getLayoutMinorAxisMinimumSpan()) {
            this.setLayoutMinorAxisMinimumSpan(val);
            minorAxisPreferenceChanged = true;
        }
        return minorAxisPreferenceChanged;
    }

    @Override
    public float getLayoutMinorAxisMaximumSpan() {
        return this.layoutMinorAxisMaximumSpan;
    }

    public void setLayoutMinorAxisMaximumSpan(float layoutMinorAxisMaximumSpan) {
        this.layoutMinorAxisMaximumSpan = layoutMinorAxisMaximumSpan;
    }

    @Override
    public float getLayoutMinorAxisMinimumSpan() {
        return this.layoutMinorAxisMinimumSpan;
    }

    public void setLayoutMinorAxisMinimumSpan(float layoutMinorAxisMinimumSpan) {
        this.layoutMinorAxisMinimumSpan = layoutMinorAxisMinimumSpan;
    }
}

