/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import javax.swing.text.Element;
import org.netbeans.lib.editor.view.GapBoxView;

public class GapMultiLineView
extends GapBoxView {
    private Element lastLineElement;

    public GapMultiLineView(Element firstLineElement, Element lastLineElement) {
        super(firstLineElement, 0);
        this.setLastLineElement(lastLineElement);
    }

    public GapMultiLineView(Element firstLineElement) {
        this(firstLineElement, firstLineElement);
    }

    protected final void setLastLineElement(Element lastLineElement) {
        this.lastLineElement = lastLineElement;
    }

    protected Element getLastLineElement() {
        return this.lastLineElement;
    }

    @Override
    public int getEndOffset() {
        return this.lastLineElement.getEndOffset();
    }
}

