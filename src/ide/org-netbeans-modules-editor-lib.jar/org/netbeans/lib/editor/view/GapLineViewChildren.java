/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import java.util.ArrayList;
import org.netbeans.editor.view.spi.ViewLayoutState;
import org.netbeans.lib.editor.view.GapBoxView;
import org.netbeans.lib.editor.view.GapBoxViewChildren;

class GapLineViewChildren
extends GapBoxViewChildren {
    private float maxMinorAxisPrefAscent;
    private int maxMinorAxisPrefDescentChildIndex = -1;
    private float maxMinorAxisPrefDescent;
    private RowList rowList;

    GapLineViewChildren(GapBoxView view) {
        super(view);
    }

    public void minorAxisLayout() {
        int childCount = this.getChildCount();
        int maxMinorPrefAscentIndex = -1;
        int maxMinorPrefDescentIndex = -1;
        float maxMinorPrefAscentValue = 0.0f;
        float maxMinorPrefDescentValue = 0.0f;
        for (int i = 0; i < childCount; ++i) {
            ViewLayoutState child = this.getChild(i);
            float span = child.getLayoutMinorAxisPreferredSpan();
            float alignment = child.getLayoutMinorAxisAlignment();
            float ascent = span * alignment;
            float descent = span - ascent;
            if (ascent > maxMinorPrefAscentValue) {
                maxMinorPrefAscentIndex = i;
                maxMinorPrefAscentValue = ascent;
            }
            if (descent <= maxMinorPrefDescentValue) continue;
            maxMinorPrefDescentIndex = i;
            maxMinorPrefDescentValue = descent;
        }
        float maxMinorPrefSpanValue = maxMinorPrefAscentValue + maxMinorPrefDescentValue;
        this.setMinorAxisPreferredSpan(maxMinorPrefSpanValue);
        this.setMaxMinorAxisPrefAscent(maxMinorPrefAscentValue);
        this.setMaxMinorAxisPrefDescent(maxMinorPrefDescentValue);
        this.setMaxMinorAxisPrefAscentChildIndex(maxMinorPrefAscentIndex);
        this.setMaxMinorAxisPrefDescentChildIndex(maxMinorPrefDescentIndex);
    }

    private float getPrefAscent(ViewLayoutState child) {
        float span = child.getLayoutMinorAxisPreferredSpan();
        float alignment = child.getLayoutMinorAxisAlignment();
        return span * alignment;
    }

    private float getMaxMinorAxisPrefAscent() {
        return this.maxMinorAxisPrefAscent;
    }

    private void setMaxMinorAxisPrefAscent(float maxMinorAxisPrefAscent) {
        this.maxMinorAxisPrefAscent = maxMinorAxisPrefAscent;
    }

    private int getMaxMinorAxisPrefAscentChildIndex() {
        return super.getMaxMinorAxisPreferredSpanChildIndex();
    }

    private void setMaxMinorAxisPrefAscentChildIndex(int maxMinorAxisPrefAscentChildIndex) {
        super.setMaxMinorAxisPreferredSpanChildIndex(maxMinorAxisPrefAscentChildIndex);
    }

    private float getPrefDescent(ViewLayoutState child) {
        float span = child.getLayoutMinorAxisPreferredSpan();
        float alignment = child.getLayoutMinorAxisAlignment();
        return span * (1.0f - alignment);
    }

    private float getMaxMinorAxisPrefDescent() {
        return this.maxMinorAxisPrefDescent;
    }

    private void setMaxMinorAxisPrefDescent(float maxMinorAxisPrefDescent) {
        this.maxMinorAxisPrefDescent = maxMinorAxisPrefDescent;
    }

    private int getMaxMinorAxisPrefDescentChildIndex() {
        return this.maxMinorAxisPrefDescentChildIndex;
    }

    private void setMaxMinorAxisPrefDescentChildIndex(int maxMinorAxisPrefDescentChildIndex) {
        this.maxMinorAxisPrefDescentChildIndex = maxMinorAxisPrefDescentChildIndex;
    }

    int getMaxMinorAxisPrefSpanChildIndex() {
        throw new IllegalStateException("Should never be called");
    }

    void setMaxMinorAxisPrefSpanChildIndex(int maxMinorAxisPrefSpanChildIndex) {
        throw new IllegalStateException("Should never be called");
    }

    @Override
    protected void replaceUpdateIndexes(int index, int removeLength, int insertLength, int neighborIndex, int neighborIndexAfterReplace, ViewLayoutState neighbor) {
        boolean minorAxisChanged = false;
        int endRemoveIndex = index + removeLength;
        int ind = this.getMaxMinorAxisPrefAscentChildIndex();
        if (ind >= endRemoveIndex) {
            this.setMaxMinorAxisPrefAscentChildIndex(ind + insertLength - removeLength);
        } else if (ind >= index) {
            float neighborAscent = this.getPrefAscent(neighbor);
            if (neighbor == null || neighborAscent < this.getMaxMinorAxisPrefAscent()) {
                minorAxisChanged = true;
            }
            this.setMaxMinorAxisPrefAscentChildIndex(neighborIndexAfterReplace);
        }
        ind = this.getMaxMinorAxisPrefDescentChildIndex();
        if (ind >= endRemoveIndex) {
            this.setMaxMinorAxisPrefDescentChildIndex(ind + insertLength - removeLength);
        } else if (ind >= index) {
            float neighborDescent = this.getPrefDescent(neighbor);
            if (neighbor == null || neighborDescent < this.getMaxMinorAxisPrefDescent()) {
                minorAxisChanged = true;
            }
            this.setMaxMinorAxisPrefDescentChildIndex(neighborIndexAfterReplace);
        }
        if (minorAxisChanged) {
            this.view.markMinorAxisPreferenceChanged();
        }
    }

    @Override
    protected void minorAxisPreferenceChanged(ViewLayoutState child, int childIndex) {
        boolean minorAxisChanged = false;
        float span = child.getLayoutMinorAxisPreferredSpan();
        float alignment = child.getLayoutMinorAxisAlignment();
        float ascent = span * alignment;
        float descent = span - ascent;
        if (this.getMaxMinorAxisPrefAscentChildIndex() == -1 || ascent > this.getMaxMinorAxisPrefAscent()) {
            this.setMaxMinorAxisPrefAscent(ascent);
            this.setMaxMinorAxisPrefAscentChildIndex(childIndex);
            minorAxisChanged = true;
        }
        if (this.getMaxMinorAxisPrefDescentChildIndex() == -1 || descent > this.getMaxMinorAxisPrefDescent()) {
            this.setMaxMinorAxisPrefDescent(descent);
            this.setMaxMinorAxisPrefDescentChildIndex(childIndex);
            minorAxisChanged = true;
        }
        if (minorAxisChanged) {
            this.setMinorAxisPreferredSpan(this.getMaxMinorAxisPrefAscent() + this.getMaxMinorAxisPrefDescent());
            this.view.markMinorAxisPreferenceChanged();
        }
    }

    public float getChildMinorAxisOffset(int childIndex) {
        ViewLayoutState child = this.getChild(childIndex);
        float minorAxisSpan = this.view.getMinorAxisAssignedSpan();
        float childMinorMaxSpan = child.getLayoutMinorAxisMaximumSpan();
        if (childMinorMaxSpan < minorAxisSpan) {
            float align = child.getLayoutMinorAxisAlignment();
            float baseline = this.getMaxMinorAxisPrefAscent();
            float span = child.getLayoutMinorAxisPreferredSpan();
            float alignment = child.getLayoutMinorAxisAlignment();
            float ascent = span * alignment;
            return baseline - ascent;
        }
        return 0.0f;
    }

    private int getRowCount() {
        return this.rowList != null ? this.rowList.size() : 0;
    }

    private Row getRow(int rowIndex) {
        return (Row)this.rowList.get(rowIndex);
    }

    private int getRowIndex(int childIndex) {
        int low = 0;
        int high = this.rowList.lastValidRowIndex;
        while (low <= high) {
            int mid = (low + high) / 2;
            Row midRow = this.getRow(mid);
            if (midRow.endChildIndex <= childIndex) {
                low = mid + 1;
                continue;
            }
            if (midRow.startChildIndex > childIndex) {
                high = mid - 1;
                continue;
            }
            return mid;
        }
        return low;
    }

    class RowList
    extends ArrayList {
        int lastValidRowIndex;

        RowList() {
        }
    }

    class Row {
        int startChildIndex;
        int endChildIndex;
        float beforeStartChildMajorOffset;
        float minorAxisOffset;
        float minorAxisSpan;

        Row() {
        }
    }

}

