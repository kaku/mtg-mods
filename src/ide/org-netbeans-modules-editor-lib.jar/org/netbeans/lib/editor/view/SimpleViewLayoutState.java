/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

import javax.swing.text.View;
import org.netbeans.editor.view.spi.ViewLayoutState;

public class SimpleViewLayoutState
implements ViewLayoutState {
    private static final int X_MAJOR_AXIS_BIT = 1;
    private static final int MAJOR_AXIS_PREFERENCE_CHANGED_BIT = 2;
    private static final int MINOR_AXIS_PREFERENCE_CHANGED_BIT = 4;
    private static final int VIEW_SIZE_INVALID_BIT = 8;
    protected static final int LAST_USED_BIT = 8;
    private static final int ANY_INVALID = 14;
    private int statusBits;
    private View view;
    private double layoutMajorAxisRawOffset;
    private int viewRawIndex;
    private float layoutMajorAxisPreferredSpan;
    private float layoutMinorAxisPreferredSpan;
    private float layoutMinorAxisAlignment;

    public SimpleViewLayoutState(View v) {
        this.view = v;
    }

    @Override
    public final View getView() {
        return this.view;
    }

    @Override
    public boolean isFlyweight() {
        return false;
    }

    @Override
    public ViewLayoutState selectLayoutMajorAxis(int axis) {
        if (axis == 0) {
            this.setStatusBits(1);
        } else {
            this.clearStatusBits(1);
        }
        return this;
    }

    protected final boolean isXMajorAxis() {
        return this.isStatusBitsNonZero(1);
    }

    protected final int getMajorAxis() {
        return this.isXMajorAxis() ? 0 : 1;
    }

    protected final int getMinorAxis() {
        return this.isXMajorAxis() ? 1 : 0;
    }

    protected final ViewLayoutState.Parent getLayoutStateParent() {
        View parent = this.getView().getParent();
        return parent instanceof ViewLayoutState.Parent ? (ViewLayoutState.Parent)((Object)parent) : null;
    }

    @Override
    public void updateLayout() {
        View parent = this.view.getParent();
        if (parent == null) {
            return;
        }
        while (!this.isLayoutValid()) {
            this.doUpdateLayout(parent);
        }
    }

    protected void doUpdateLayout(View parent) {
        ViewLayoutState.Parent lsParent;
        ViewLayoutState.Parent parent2 = lsParent = parent instanceof ViewLayoutState.Parent ? (ViewLayoutState.Parent)((Object)parent) : null;
        if (this.isStatusBitsNonZero(4)) {
            this.clearStatusBits(4);
            int minorAxis = this.getMinorAxis();
            if (this.minorAxisUpdateLayout(minorAxis) && lsParent != null) {
                lsParent.minorAxisPreferenceChanged(this);
            }
        }
        if (this.isStatusBitsNonZero(2)) {
            this.clearStatusBits(2);
            float oldSpan = this.getLayoutMajorAxisPreferredSpanFloat();
            float newSpan = this.view.getPreferredSpan(this.getMajorAxis());
            this.setLayoutMajorAxisPreferredSpan(newSpan);
            double majorAxisSpanDelta = newSpan - oldSpan;
            if (majorAxisSpanDelta != 0.0 && lsParent != null) {
                lsParent.majorAxisPreferenceChanged(this, majorAxisSpanDelta);
            }
        }
        if (this.isStatusBitsNonZero(8)) {
            this.clearStatusBits(8);
            if (lsParent != null) {
                float height;
                float width;
                float majorAxisSpan = (float)this.getLayoutMajorAxisPreferredSpan();
                float minorAxisSpan = lsParent.getMinorAxisSpan(this);
                if (this.isXMajorAxis()) {
                    width = majorAxisSpan;
                    height = minorAxisSpan;
                } else {
                    width = minorAxisSpan;
                    height = majorAxisSpan;
                }
                this.view.setSize(width, height);
            }
        }
        this.updateLayout();
    }

    protected boolean minorAxisUpdateLayout(int minorAxis) {
        boolean minorAxisPreferenceChanged = false;
        float val = this.view.getPreferredSpan(minorAxis);
        if (val != this.getLayoutMinorAxisPreferredSpan()) {
            this.setLayoutMinorAxisPreferredSpan(val);
            minorAxisPreferenceChanged = true;
        }
        if ((val = this.view.getAlignment(this.getMinorAxis())) != this.getLayoutMinorAxisAlignment()) {
            this.setLayoutMinorAxisAlignment(val);
            minorAxisPreferenceChanged = true;
        }
        return minorAxisPreferenceChanged;
    }

    @Override
    public int getViewRawIndex() {
        return this.viewRawIndex;
    }

    @Override
    public void setViewRawIndex(int viewRawIndex) {
        this.viewRawIndex = viewRawIndex;
    }

    @Override
    public double getLayoutMajorAxisRawOffset() {
        return this.layoutMajorAxisRawOffset;
    }

    @Override
    public void setLayoutMajorAxisRawOffset(double layoutMajorAxisRawOffset) {
        this.layoutMajorAxisRawOffset = layoutMajorAxisRawOffset;
    }

    @Override
    public double getLayoutMajorAxisPreferredSpan() {
        return this.layoutMajorAxisPreferredSpan;
    }

    public float getLayoutMajorAxisPreferredSpanFloat() {
        return this.layoutMajorAxisPreferredSpan;
    }

    protected void setLayoutMajorAxisPreferredSpan(float layoutMajorAxisPreferredSpan) {
        this.layoutMajorAxisPreferredSpan = layoutMajorAxisPreferredSpan;
    }

    @Override
    public float getLayoutMinorAxisPreferredSpan() {
        return this.layoutMinorAxisPreferredSpan;
    }

    protected void setLayoutMinorAxisPreferredSpan(float layoutMinorAxisPreferredSpan) {
        this.layoutMinorAxisPreferredSpan = layoutMinorAxisPreferredSpan;
    }

    @Override
    public float getLayoutMinorAxisMinimumSpan() {
        return this.getLayoutMinorAxisPreferredSpan();
    }

    @Override
    public float getLayoutMinorAxisMaximumSpan() {
        return this.getLayoutMinorAxisPreferredSpan();
    }

    @Override
    public float getLayoutMinorAxisAlignment() {
        return this.layoutMinorAxisAlignment;
    }

    public void setLayoutMinorAxisAlignment(float layoutMinorAxisAlignment) {
        this.layoutMinorAxisAlignment = layoutMinorAxisAlignment;
    }

    @Override
    public void viewPreferenceChanged(boolean width, boolean height) {
        if (this.isXMajorAxis()) {
            if (width) {
                this.setStatusBits(2);
            }
            if (height) {
                this.setStatusBits(4);
            }
        } else {
            if (width) {
                this.setStatusBits(4);
            }
            if (height) {
                this.setStatusBits(2);
            }
        }
        this.setStatusBits(8);
    }

    @Override
    public void markViewSizeInvalid() {
        this.setStatusBits(8);
    }

    @Override
    public boolean isLayoutValid() {
        return !this.isStatusBitsNonZero(14);
    }

    protected final int getStatusBits(int bits) {
        return this.statusBits & bits;
    }

    protected final boolean isStatusBitsNonZero(int bits) {
        return this.getStatusBits(bits) != 0;
    }

    protected final void setStatusBits(int bits) {
        this.statusBits |= bits;
    }

    protected final void clearStatusBits(int bits) {
        this.statusBits &= ~ bits;
    }
}

