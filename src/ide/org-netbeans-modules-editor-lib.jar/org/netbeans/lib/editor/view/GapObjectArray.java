/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.view;

class GapObjectArray {
    private static final Object[] EMPTY_ARRAY = new Object[0];
    private Object[] array;
    private int gapStart;
    private int gapLength;

    public GapObjectArray() {
        this.array = EMPTY_ARRAY;
    }

    public GapObjectArray(Object[] array, int length) {
        this.array = array;
        this.gapStart = length;
        this.gapLength = array.length - length;
    }

    public int getItemCount() {
        return this.array.length - this.gapLength;
    }

    public Object getItem(int index) {
        return this.array[index < this.gapStart ? index : index + this.gapLength];
    }

    public void copyItems(int srcStartIndex, int srcEndIndex, Object[] dest, int destIndex) {
        this.rangeCheck(srcStartIndex, srcEndIndex - srcStartIndex);
        if (srcEndIndex < this.gapStart) {
            System.arraycopy(this.array, srcStartIndex, dest, destIndex, srcEndIndex - srcStartIndex);
        } else if (srcStartIndex >= this.gapStart) {
            System.arraycopy(this.array, srcStartIndex + this.gapLength, dest, destIndex, srcEndIndex - srcStartIndex);
        } else {
            int beforeGap = this.gapStart - srcStartIndex;
            System.arraycopy(this.array, srcStartIndex, dest, destIndex, beforeGap);
            System.arraycopy(this.array, this.gapStart + this.gapLength, dest, destIndex + beforeGap, srcEndIndex - srcStartIndex - beforeGap);
        }
    }

    public final int getGapStart() {
        return this.gapStart;
    }

    public void replace(int index, int removeCount, Object[] newItems) {
        this.remove(index, removeCount);
        this.insertAll(index, newItems);
    }

    public void insertItem(int index, Object item) {
        this.indexCheck(index);
        if (this.gapLength == 0) {
            this.enlargeGap(1);
        }
        if (index != this.gapStart) {
            this.moveGap(index);
        }
        this.array[this.gapStart++] = item;
        --this.gapLength;
    }

    public void insertAll(int index, Object[] items) {
        this.insertAll(index, items, 0, items.length);
    }

    public void insertAll(int index, Object[] items, int off, int len) {
        this.indexCheck(index);
        if (items.length == 0) {
            return;
        }
        int extraLength = len - this.gapLength;
        if (extraLength > 0) {
            this.enlargeGap(extraLength);
        }
        if (index != this.gapStart) {
            this.moveGap(index);
        }
        System.arraycopy(items, off, this.array, this.gapStart, len);
        this.gapStart += len;
        this.gapLength -= len;
    }

    public void ensureCapacity(int minCapacity) {
        if (minCapacity < 0) {
            throw new IllegalArgumentException("minCapacity=" + minCapacity + " < 0");
        }
        int capacity = this.array.length;
        if (capacity == 0) {
            this.setCapacity(minCapacity);
        } else if (minCapacity > capacity) {
            this.enlargeGap(minCapacity - capacity);
        }
    }

    public void remove(int index, int count) {
        this.remove(index, count, null);
    }

    public void remove(int index, int count, RemoveUpdater removeUpdater) {
        this.rangeCheck(index, count);
        if (count == 0) {
            return;
        }
        if (index >= this.gapStart) {
            if (index > this.gapStart) {
                this.moveGap(index);
            }
            int endIndex = (index += this.gapLength) + count;
            while (index < endIndex) {
                if (removeUpdater != null) {
                    removeUpdater.removeUpdate(this.array[index]);
                }
                this.array[index] = null;
                ++index;
            }
        } else {
            int endIndex = index + count;
            if (endIndex <= this.gapStart) {
                if (endIndex < this.gapStart) {
                    this.moveGap(endIndex);
                }
                this.gapStart = index;
            } else {
                for (int clearIndex = index; clearIndex < this.gapStart; ++clearIndex) {
                    if (removeUpdater != null) {
                        removeUpdater.removeUpdate(this.array[clearIndex]);
                    }
                    this.array[clearIndex] = null;
                }
                index = this.gapStart + this.gapLength;
                this.gapStart = endIndex - count;
                endIndex += this.gapLength;
            }
            while (index < endIndex) {
                if (removeUpdater != null) {
                    removeUpdater.removeUpdate(this.array[index]);
                }
                this.array[index++] = null;
            }
        }
        this.gapLength += count;
    }

    protected void unoptimizedRemove(int index, int count, RemoveUpdater removeUpdater) {
        this.rangeCheck(index, count);
        int endIndex = index + count;
        if (this.gapStart != endIndex) {
            this.moveGap(endIndex);
        }
        for (int i = endIndex - 1; i >= index; --i) {
            if (removeUpdater != null) {
                removeUpdater.removeUpdate(this.array[i]);
            }
            this.array[i] = null;
        }
        this.gapStart = index;
    }

    public void compact() {
        this.setCapacity(this.getItemCount());
    }

    protected void movedAboveGapUpdate(Object[] array, int index, int count) {
    }

    protected void movedBelowGapUpdate(Object[] array, int index, int count) {
    }

    private void moveGap(int index) {
        if (index <= this.gapStart) {
            int moveSize = this.gapStart - index;
            System.arraycopy(this.array, index, this.array, this.gapStart + this.gapLength - moveSize, moveSize);
            this.clearEmpty(index, Math.min(moveSize, this.gapLength));
            this.gapStart = index;
            this.movedAboveGapUpdate(this.array, this.gapStart + this.gapLength, moveSize);
        } else {
            int gapEnd = this.gapStart + this.gapLength;
            int moveSize = index - this.gapStart;
            System.arraycopy(this.array, gapEnd, this.array, this.gapStart, moveSize);
            if (index < gapEnd) {
                this.clearEmpty(gapEnd, moveSize);
            } else {
                this.clearEmpty(index, this.gapLength);
            }
            this.movedBelowGapUpdate(this.array, this.gapStart, moveSize);
            this.gapStart += moveSize;
        }
    }

    private void clearEmpty(int index, int length) {
        while (--length >= 0) {
            this.array[index++] = null;
        }
    }

    private void enlargeGap(int extraLength) {
        int newLength = Math.max(4, Math.max(this.array.length * 2, this.array.length + extraLength));
        this.setCapacity(newLength);
    }

    private void setCapacity(int newCapacity) {
        int gapEnd = this.gapStart + this.gapLength;
        int afterGapLength = this.array.length - gapEnd;
        int newGapEnd = newCapacity - afterGapLength;
        Object[] newArray = new Object[newCapacity];
        if (newCapacity < this.gapStart + afterGapLength) {
            throw new IllegalArgumentException("newCapacity=" + newCapacity + " < itemCount=" + (this.gapStart + afterGapLength));
        }
        System.arraycopy(this.array, 0, newArray, 0, this.gapStart);
        System.arraycopy(this.array, gapEnd, newArray, newGapEnd, afterGapLength);
        this.array = newArray;
        this.gapLength = newGapEnd - this.gapStart;
    }

    private void rangeCheck(int index, int count) {
        if (index < 0 || count < 0 || index + count > this.getItemCount()) {
            throw new IndexOutOfBoundsException("index=" + index + ", count=" + count + ", getItemCount()=" + this.getItemCount());
        }
    }

    private void indexCheck(int index) {
        if (index > this.getItemCount()) {
            throw new IndexOutOfBoundsException("index=" + index + ", getItemCount()=" + this.getItemCount());
        }
    }

    void check() {
        if (this.gapStart < 0 || this.gapLength < 0 || this.gapStart + this.gapLength > this.array.length) {
            throw new IllegalStateException();
        }
        for (int i = this.gapStart + this.gapLength - 1; i >= this.gapStart; --i) {
            if (this.array[i] == null) continue;
            throw new IllegalStateException();
        }
    }

    public String toStringDetail() {
        return "gapStart=" + this.gapStart + ", gapLength=" + this.gapLength + ", array.length=" + this.array.length;
    }

    public static interface RemoveUpdater {
        public void removeUpdate(Object var1);
    }

}

