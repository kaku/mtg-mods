/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.hyperlink.spi;

public enum HyperlinkType {
    GO_TO_DECLARATION,
    ALT_HYPERLINK;
    

    private HyperlinkType() {
    }
}

