/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightAttributeValue
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 *  org.openide.util.Utilities
 */
package org.netbeans.lib.editor.hyperlink;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.JumpList;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProviderExt;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.highlighting.HighlightAttributeValue;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;
import org.openide.util.Utilities;

public class HyperlinkOperation
implements MouseListener,
MouseMotionListener,
PropertyChangeListener,
KeyListener {
    private static Logger LOG = Logger.getLogger(HyperlinkOperation.class.getName());
    private static final String KEY = "hyperlink-operation";
    private JTextComponent component;
    private Document currentDocument;
    private String operationMimeType;
    private Cursor oldComponentsMouseCursor;
    private boolean hyperlinkUp;
    private boolean listenersSetUp;
    private boolean hyperlinkEnabled;
    private int actionKeyMask;
    private int altActionKeyMask;
    private static Object BAG_KEY = new Object();
    private static AttributeSet defaultHyperlinksHighlight = AttributesUtilities.createImmutable((Object[])new Object[]{StyleConstants.Foreground, Color.BLUE, StyleConstants.Underline, Color.BLUE});

    public static void ensureRegistered(JTextComponent component, String mimeType) {
        if (component.getClientProperty("hyperlink-operation") == null) {
            component.putClientProperty("hyperlink-operation", new HyperlinkOperation(component, mimeType));
        }
    }

    private static synchronized Cursor getMouseCursor(HyperlinkType type) {
        switch (type) {
            case GO_TO_DECLARATION: 
            case ALT_HYPERLINK: {
                return Cursor.getPredefinedCursor(12);
            }
        }
        throw new UnsupportedOperationException();
    }

    private static synchronized boolean isHyperlinkMouseCursor(Cursor c) {
        return c == Cursor.getPredefinedCursor(12) || c == Cursor.getPredefinedCursor(1) || c == Cursor.getPredefinedCursor(13);
    }

    private HyperlinkOperation(JTextComponent component, String mimeType) {
        this.component = component;
        this.operationMimeType = mimeType;
        this.oldComponentsMouseCursor = null;
        this.hyperlinkUp = false;
        this.listenersSetUp = false;
        this.readSettings();
        if (this.hyperlinkEnabled) {
            component.addPropertyChangeListener("document", this);
        }
    }

    private void documentUpdated() {
        if (!this.hyperlinkEnabled) {
            return;
        }
        this.currentDocument = this.component.getDocument();
        if (this.currentDocument instanceof BaseDocument && !this.listenersSetUp) {
            this.component.addMouseListener(this);
            this.component.addMouseMotionListener(this);
            this.component.addKeyListener(this);
            this.listenersSetUp = true;
        }
    }

    private void readSettings() {
        String hyperlinkActivationKeyPropertyValue = System.getProperty("org.netbeans.lib.editor.hyperlink.HyperlinkOperation.activationKey");
        if (hyperlinkActivationKeyPropertyValue != null) {
            if ("off".equals(hyperlinkActivationKeyPropertyValue)) {
                this.hyperlinkEnabled = false;
                this.actionKeyMask = -1;
            } else {
                this.hyperlinkEnabled = true;
                this.actionKeyMask = -1;
                for (int cntr = 0; cntr < hyperlinkActivationKeyPropertyValue.length(); ++cntr) {
                    int localMask = 0;
                    switch (hyperlinkActivationKeyPropertyValue.charAt(cntr)) {
                        case 'S': {
                            localMask = 64;
                            break;
                        }
                        case 'C': {
                            localMask = 128;
                            break;
                        }
                        case 'A': {
                            localMask = 512;
                            break;
                        }
                        case 'M': {
                            localMask = 256;
                            break;
                        }
                        default: {
                            LOG.warning("Incorrect value of org.netbeans.lib.editor.hyperlink.HyperlinkOperation.activationKey property (only letters CSAM are allowed): " + hyperlinkActivationKeyPropertyValue.charAt(cntr));
                        }
                    }
                    if (localMask == 0) {
                        this.actionKeyMask = -1;
                        break;
                    }
                    if (this.actionKeyMask == -1) {
                        this.actionKeyMask = localMask;
                        continue;
                    }
                    this.actionKeyMask |= localMask;
                }
                if (this.actionKeyMask == -1) {
                    LOG.warning("Some problem with property org.netbeans.lib.editor.hyperlink.HyperlinkOperation.activationKey, more information might be given above. Falling back to the default behaviour.");
                } else {
                    return;
                }
            }
        }
        this.hyperlinkEnabled = true;
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)this.component)).lookup(Preferences.class);
        this.actionKeyMask = prefs.getInt("hyperlink-activation-modifiers", 128);
        this.altActionKeyMask = prefs.getInt("alt-hyperlink-activation-modifiers", 640);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        HyperlinkType type = this.getHyperlinkType(e);
        if (type != null) {
            int position = this.component.viewToModel(e.getPoint());
            if (position < 0) {
                this.unHyperlink(true);
                return;
            }
            this.performHyperlinking(position, type);
        } else {
            this.unHyperlink(true);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    private HyperlinkType getHyperlinkType(InputEvent e) {
        int modifiers = e.getModifiers() | e.getModifiersEx();
        if ((modifiers & this.altActionKeyMask) == this.altActionKeyMask) {
            return HyperlinkType.ALT_HYPERLINK;
        }
        if ((modifiers & this.actionKeyMask) == this.actionKeyMask) {
            return HyperlinkType.GO_TO_DECLARATION;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void performHyperlinking(int position, HyperlinkType type) {
        block5 : {
            BaseDocument doc = (BaseDocument)this.component.getDocument();
            doc.readLock();
            try {
                HyperlinkProviderExt provider = this.findProvider(position, type);
                if (provider != null) {
                    int[] offsets = provider.getHyperlinkSpan(doc, position, type);
                    if (offsets != null) {
                        this.makeHyperlink(type, provider, offsets[0], offsets[1], position);
                    }
                    break block5;
                }
                this.unHyperlink(true);
            }
            finally {
                doc.readUnlock();
            }
        }
    }

    private void performAction(int position, HyperlinkType type) {
        HyperlinkProviderExt provider = this.findProvider(position, type);
        if (provider != null) {
            this.unHyperlink(true);
            this.component.getCaret().setDot(position);
            JumpList.checkAddEntry(this.component, position);
            provider.performClickAction(this.component.getDocument(), position, type);
        }
    }

    private HyperlinkProviderExt findProvider(int position, HyperlinkType type) {
        Object mimeTypeObj = this.component.getDocument().getProperty("mimeType");
        String mimeType = mimeTypeObj instanceof String ? (String)mimeTypeObj : this.operationMimeType;
        Collection<? extends HyperlinkProviderExt> extProviders = HyperlinkOperation.getHyperlinkProviderExts(mimeType);
        for (HyperlinkProviderExt provider : extProviders) {
            if (!provider.getSupportedHyperlinkTypes().contains((Object)type) || !provider.isHyperlinkPoint(this.component.getDocument(), position, type)) continue;
            return provider;
        }
        if (type != HyperlinkType.GO_TO_DECLARATION) {
            return null;
        }
        Collection<? extends HyperlinkProvider> providers = HyperlinkOperation.getHyperlinkProviders(mimeType);
        for (final HyperlinkProvider provider2 : providers) {
            if (!provider2.isHyperlinkPoint(this.component.getDocument(), position)) continue;
            return new HyperlinkProviderExt(){

                @Override
                public Set<HyperlinkType> getSupportedHyperlinkTypes() {
                    return EnumSet.of(HyperlinkType.GO_TO_DECLARATION);
                }

                @Override
                public boolean isHyperlinkPoint(Document doc, int offset, HyperlinkType type) {
                    return provider2.isHyperlinkPoint(doc, offset);
                }

                @Override
                public int[] getHyperlinkSpan(Document doc, int offset, HyperlinkType type) {
                    return provider2.getHyperlinkSpan(doc, offset);
                }

                @Override
                public void performClickAction(Document doc, int offset, HyperlinkType type) {
                    provider2.performClickAction(doc, offset);
                }

                @Override
                public String getTooltipText(Document doc, int offset, HyperlinkType type) {
                    return null;
                }
            };
        }
        return null;
    }

    private synchronized void makeHyperlink(HyperlinkType type, HyperlinkProviderExt provider, int start, int end, int offset) {
        boolean makeCursorSnapshot = true;
        if (this.hyperlinkUp) {
            this.unHyperlink(false);
            makeCursorSnapshot = false;
        }
        OffsetsBag prepare = new OffsetsBag(this.component.getDocument());
        FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FontColorSettings.class);
        AttributeSet hyperlinksHighlight = fcs.getFontColors("hyperlinks");
        AttributeSet[] arrattributeSet = new AttributeSet[2];
        arrattributeSet[0] = hyperlinksHighlight != null ? hyperlinksHighlight : defaultHyperlinksHighlight;
        arrattributeSet[1] = AttributesUtilities.createImmutable((Object[])new Object[]{EditorStyleConstants.Tooltip, new TooltipResolver(provider, offset, type)});
        prepare.addHighlight(start, end, AttributesUtilities.createComposite((AttributeSet[])arrattributeSet));
        HyperlinkOperation.getBag(this.currentDocument).setHighlights(prepare);
        this.hyperlinkUp = true;
        if (makeCursorSnapshot) {
            this.oldComponentsMouseCursor = this.component.isCursorSet() ? this.component.getCursor() : null;
            this.component.setCursor(HyperlinkOperation.getMouseCursor(type));
        }
    }

    private synchronized void unHyperlink(boolean removeCursor) {
        if (!this.hyperlinkUp) {
            return;
        }
        HyperlinkOperation.getBag(this.currentDocument).clear();
        if (removeCursor) {
            if (this.component.isCursorSet() && HyperlinkOperation.isHyperlinkMouseCursor(this.component.getCursor())) {
                this.component.setCursor(this.oldComponentsMouseCursor);
            }
            this.oldComponentsMouseCursor = null;
        }
        this.hyperlinkUp = false;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.currentDocument != this.component.getDocument()) {
            this.documentUpdated();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (this.getHyperlinkType(e) == null) {
            this.unHyperlink(true);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        HyperlinkType type = this.getHyperlinkType(e);
        Point mousePos = null;
        try {
            mousePos = this.component.getMousePosition();
        }
        catch (NullPointerException npe) {
            // empty catch block
        }
        if (type != null && mousePos != null) {
            int position = this.component.viewToModel(mousePos);
            if (position < 0) {
                this.unHyperlink(true);
                return;
            }
            this.performHyperlinking(position, type);
        } else {
            this.unHyperlink(true);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        boolean activate = false;
        HyperlinkType type = this.getHyperlinkType(e);
        if (type != null) {
            activate = !e.isPopupTrigger() && e.getClickCount() == 1 && SwingUtilities.isLeftMouseButton(e);
        } else if (Utilities.isWindows() && e.getClickCount() == 1 && SwingUtilities.isMiddleMouseButton(e)) {
            activate = true;
            type = HyperlinkType.GO_TO_DECLARATION;
        }
        if (activate) {
            int position = this.component.viewToModel(e.getPoint());
            if (position < 0) {
                return;
            }
            this.performAction(position, type);
        }
    }

    private static OffsetsBag getBag(Document doc) {
        OffsetsBag bag = (OffsetsBag)doc.getProperty(BAG_KEY);
        if (bag == null) {
            bag = new OffsetsBag(doc);
            doc.putProperty(BAG_KEY, (Object)bag);
        }
        return bag;
    }

    public static Collection<? extends HyperlinkProvider> getHyperlinkProviders(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        return MimeLookup.getLookup((MimePath)mimePath).lookupAll(HyperlinkProvider.class);
    }

    public static Collection<? extends HyperlinkProviderExt> getHyperlinkProviderExts(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        return MimeLookup.getLookup((MimePath)mimePath).lookupAll(HyperlinkProviderExt.class);
    }

    public static final class TooltipInfo
    implements CharSequence {
        private final String tooltipText;
        private final HyperlinkListener listener;

        private TooltipInfo(String tooltipText, HyperlinkListener listener) {
            this.tooltipText = tooltipText;
            this.listener = listener;
        }

        public HyperlinkListener getListener() {
            return this.listener;
        }

        @Override
        public int length() {
            return this.tooltipText.length();
        }

        @Override
        public char charAt(int index) {
            return this.tooltipText.charAt(index);
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return this.tooltipText.subSequence(start, end);
        }

        @Override
        public String toString() {
            return this.tooltipText;
        }
    }

    private static final class TooltipResolver
    implements HighlightAttributeValue<CharSequence> {
        private static final String HYPERLINK_LISTENER = "TooltipResolver.hyperlinkListener";
        private HyperlinkProviderExt provider;
        private int offset;
        private HyperlinkType type;

        public TooltipResolver(HyperlinkProviderExt provider, int offset, HyperlinkType type) {
            this.provider = provider;
            this.offset = offset;
            this.type = type;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public CharSequence getValue(JTextComponent component, Document document, Object attributeKey, int startOffset, int endOffset) {
            try {
                CharSequence tooltipText = this.provider.getTooltipText(document, this.offset, this.type);
                HyperlinkListener hl = (HyperlinkListener)document.getProperty("TooltipResolver.hyperlinkListener");
                CharSequence charSequence = hl != null ? new TooltipInfo((String)tooltipText, hl) : tooltipText;
                return charSequence;
            }
            finally {
                document.putProperty("TooltipResolver.hyperlinkListener", null);
            }
        }
    }

    public static final class HighlightFactoryImpl
    implements HighlightsLayerFactory {
        public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
            return new HighlightsLayer[]{HighlightsLayer.create((String)HyperlinkOperation.class.getName(), (ZOrder)ZOrder.SHOW_OFF_RACK.forPosition(450), (boolean)true, (HighlightsContainer)HyperlinkOperation.getBag(context.getDocument()))};
        }
    }

}

