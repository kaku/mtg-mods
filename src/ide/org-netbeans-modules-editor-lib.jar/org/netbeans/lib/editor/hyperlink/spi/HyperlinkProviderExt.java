/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.lib.editor.hyperlink.spi;

import java.util.Set;
import javax.swing.text.Document;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkType;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="HyperlinkProviders")
public interface HyperlinkProviderExt {
    public Set<HyperlinkType> getSupportedHyperlinkTypes();

    public boolean isHyperlinkPoint(Document var1, int var2, HyperlinkType var3);

    public int[] getHyperlinkSpan(Document var1, int var2, HyperlinkType var3);

    public void performClickAction(Document var1, int var2, HyperlinkType var3);

    public String getTooltipText(Document var1, int var2, HyperlinkType var3);
}

