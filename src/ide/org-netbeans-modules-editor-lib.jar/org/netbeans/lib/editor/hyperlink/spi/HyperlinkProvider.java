/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.lib.editor.hyperlink.spi;

import javax.swing.text.Document;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="HyperlinkProviders")
public interface HyperlinkProvider {
    public boolean isHyperlinkPoint(Document var1, int var2);

    public int[] getHyperlinkSpan(Document var1, int var2);

    public void performClickAction(Document var1, int var2);
}

