/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.image.ImageObserver;
import java.io.PrintStream;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.AnnotationTypes;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.FontMetricsCache;
import org.netbeans.editor.PrintContainer;
import org.netbeans.modules.editor.lib.drawing.DrawContext;
import org.netbeans.modules.editor.lib.drawing.EditorUiAccessor;

public interface DrawGraphics {
    public void setForeColor(Color var1);

    public void setBackColor(Color var1);

    public void setDefaultBackColor(Color var1);

    public void setStrikeThroughColor(Color var1);

    public void setUnderlineColor(Color var1);

    public void setWaveUnderlineColor(Color var1);

    public void setTopBorderLineColor(Color var1);

    public void setRightBorderLineColor(Color var1);

    public void setBottomBorderLineColor(Color var1);

    public void setLeftBorderLineColor(Color var1);

    public void setFont(Font var1);

    public void setX(int var1);

    public void setY(int var1);

    public void setLineHeight(int var1);

    public void setLineAscent(int var1);

    public Graphics getGraphics();

    public boolean supportsLineNumbers();

    public void init(DrawContext var1);

    public void finish();

    public void fillRect(int var1);

    public void drawChars(int var1, int var2, int var3);

    public void drawTabs(int var1, int var2, int var3, int var4);

    public void setBuffer(char[] var1);

    public boolean targetOffsetReached(int var1, char var2, int var3, int var4, DrawContext var5);

    public void eol();

    public void setView(View var1);

    public static final class PrintDG
    extends SimpleDG {
        PrintContainer container;
        boolean lineInited;

        public PrintDG(PrintContainer container) {
            this.container = container;
        }

        @Override
        public boolean supportsLineNumbers() {
            return true;
        }

        @Override
        public void drawChars(int offset, int length, int width) {
            if (length > 0) {
                this.lineInited = true;
                char[] chars = new char[length];
                System.arraycopy(this.buffer, offset, chars, 0, length);
                this.container.add(chars, this.font, this.foreColor, this.backColor);
            }
        }

        private void printSpaces(int spaceCount) {
            char[] chars = new char[spaceCount];
            System.arraycopy(Analyzer.getSpacesBuffer(spaceCount), 0, chars, 0, spaceCount);
            this.container.add(chars, this.font, this.foreColor, this.backColor);
        }

        @Override
        public void drawTabs(int offset, int length, int spaceCount, int width) {
            this.lineInited = true;
            this.printSpaces(spaceCount);
        }

        @Override
        public void eol() {
            if (!this.lineInited && this.container.initEmptyLines()) {
                this.printSpaces(1);
            }
            this.container.eol();
            this.lineInited = false;
        }
    }

    public static final class GraphicsDG
    extends SimpleDG {
        private static final boolean debug = Boolean.getBoolean("netbeans.debug.editor.draw.graphics");
        private Graphics graphics;
        private int startOffset = -1;
        private int endOffset;
        private int startX;
        private int startY;
        private int width;
        private Color strikeThroughColor;
        private Color underlineColor;
        private Color waveUnderlineColor;
        private Color topBorderLineColor;
        private Color rightBorderLineColor;
        private Color bottomBorderLineColor;
        private Color leftBorderLineColor;
        private int lastDrawnAnnosY;
        private int lastDrawnAnnosX;
        private AnnotationDesc[] passiveAnnosAtY;
        private AlphaComposite alpha = null;
        private Annotations annos = null;
        private boolean drawTextLimitLine;
        private int textLimitWidth;
        private int defaultSpaceWidth;
        private Color textLimitLineColor;
        private int absoluteX;
        private int maxWidth;
        private View view;
        private int bufferStartOffset;
        private JComponent component;

        public GraphicsDG(Graphics graphics) {
            this.graphics = graphics;
            this.y = -1;
        }

        @Override
        public void setForeColor(Color foreColor) {
            if (!foreColor.equals(this.foreColor)) {
                this.flush();
                this.foreColor = foreColor;
            }
        }

        @Override
        public void setBackColor(Color backColor) {
            if (!backColor.equals(this.backColor)) {
                this.flush();
                this.backColor = backColor;
            }
        }

        @Override
        public void setStrikeThroughColor(Color strikeThroughColor) {
            if (!(strikeThroughColor == this.strikeThroughColor || strikeThroughColor != null && strikeThroughColor.equals(this.strikeThroughColor))) {
                this.flush();
                this.strikeThroughColor = strikeThroughColor;
            }
        }

        @Override
        public void setUnderlineColor(Color underlineColor) {
            if (!(underlineColor == this.underlineColor || underlineColor != null && underlineColor.equals(this.underlineColor))) {
                this.flush();
                this.underlineColor = underlineColor;
            }
        }

        @Override
        public void setWaveUnderlineColor(Color waveUnderlineColor) {
            if (!(waveUnderlineColor == this.waveUnderlineColor || waveUnderlineColor != null && waveUnderlineColor.equals(this.waveUnderlineColor))) {
                this.flush();
                this.waveUnderlineColor = waveUnderlineColor;
            }
        }

        @Override
        public void setTopBorderLineColor(Color color) {
            if (!(color == this.topBorderLineColor || color != null && color.equals(this.topBorderLineColor))) {
                this.flush();
                this.topBorderLineColor = color;
            }
        }

        @Override
        public void setRightBorderLineColor(Color color) {
            if (!(color == this.rightBorderLineColor || color != null && color.equals(this.rightBorderLineColor))) {
                this.flush();
                this.rightBorderLineColor = color;
            }
        }

        @Override
        public void setBottomBorderLineColor(Color color) {
            if (!(color == this.bottomBorderLineColor || color != null && color.equals(this.bottomBorderLineColor))) {
                this.flush();
                this.bottomBorderLineColor = color;
            }
        }

        @Override
        public void setLeftBorderLineColor(Color color) {
            if (!(color == this.leftBorderLineColor || color != null && color.equals(this.leftBorderLineColor))) {
                this.flush();
                this.leftBorderLineColor = color;
            }
        }

        @Override
        public void setFont(Font font) {
            if (!font.equals(this.font)) {
                this.flush();
                this.font = font;
            }
        }

        @Override
        public void setX(int x) {
            if (x != this.x) {
                this.flush();
                this.x = x;
            }
        }

        @Override
        public void setY(int y) {
            if (y != this.y) {
                this.flush();
                this.y = y;
            }
        }

        @Override
        public void init(DrawContext ctx) {
            EditorUiAccessor accessor = EditorUiAccessor.get();
            this.annos = ctx.getEditorUI().getDocument().getAnnotations();
            this.drawTextLimitLine = accessor.getTextLimitLineVisible(ctx.getEditorUI());
            this.textLimitWidth = accessor.getTextLimitWidth(ctx.getEditorUI());
            this.defaultSpaceWidth = accessor.getDefaultSpaceWidth(ctx.getEditorUI());
            this.textLimitLineColor = accessor.getTextLimitLineColor(ctx.getEditorUI());
            this.absoluteX = ctx.getEditorUI().getTextMargin().left;
            this.maxWidth = ctx.getEditorUI().getExtentBounds().width;
            this.component = ctx.getEditorUI().getComponent();
        }

        @Override
        public void finish() {
        }

        @Override
        public void setView(View view) {
            this.view = view;
        }

        private void flush() {
            this.flush(false);
        }

        private void flush(boolean atEOL) {
            FontMetricsCache.Info fmcInfo;
            int i;
            if (this.y < 0) {
                return;
            }
            if (this.startOffset >= 0 && this.startOffset != this.endOffset) {
                this.fillRectImpl(this.startX, this.startY, this.x - this.startX);
            }
            if (this.lastDrawnAnnosY != this.y) {
                this.lastDrawnAnnosY = this.y;
                this.lastDrawnAnnosX = 0;
                if (AnnotationTypes.getTypes().isBackgroundDrawing().booleanValue()) {
                    if (this.alpha == null) {
                        this.alpha = AlphaComposite.getInstance(3, (float)AnnotationTypes.getTypes().getBackgroundGlyphAlpha().intValue() / 100.0f);
                    }
                    if (this.view != null) {
                        this.passiveAnnosAtY = this.annos.getPassiveAnnotations(this.view.getStartOffset());
                    }
                } else {
                    this.passiveAnnosAtY = null;
                }
            }
            int glyphX = 2;
            if (this.passiveAnnosAtY != null) {
                Graphics2D g2d = (Graphics2D)this.graphics;
                Shape shape = this.graphics.getClip();
                Composite origin = g2d.getComposite();
                g2d.setComposite(this.alpha);
                int clipX = atEOL ? Integer.MAX_VALUE : this.x;
                int clipY = Math.min(this.lastDrawnAnnosX, this.startX);
                Rectangle clip = new Rectangle(clipY, this.y, clipX - clipY, this.lineHeight);
                this.lastDrawnAnnosX = clipX;
                clip = clip.intersection(shape.getBounds());
                this.graphics.setClip(clip);
                for (i = 0; i < this.passiveAnnosAtY.length; ++i) {
                    g2d.drawImage(this.passiveAnnosAtY[i].getGlyph(), glyphX, this.y, null);
                    glyphX += this.passiveAnnosAtY[i].getGlyph().getWidth(null) + 1;
                }
                this.graphics.setClip(shape);
                g2d.setComposite(origin);
            }
            if (this.startOffset < 0 || this.startOffset >= this.endOffset || this.startOffset - this.endOffset > this.buffer.length) {
                this.startOffset = -1;
                return;
            }
            if (this.drawTextLimitLine && this.textLimitWidth > 0) {
                Rectangle clip = this.graphics.getClipBounds();
                int lineX = this.absoluteX + this.textLimitWidth * this.defaultSpaceWidth;
                if (lineX >= this.startX && lineX <= this.x) {
                    Color bakColor = this.graphics.getColor();
                    this.graphics.setColor(this.textLimitLineColor);
                    this.graphics.drawLine(lineX, this.startY, lineX, this.startY + this.lineHeight);
                    this.graphics.setColor(bakColor);
                }
            }
            if (this.topBorderLineColor != null) {
                this.graphics.setColor(this.topBorderLineColor);
                this.graphics.drawLine(this.startX, this.startY, this.x - 1, this.startY);
            }
            if (this.rightBorderLineColor != null) {
                this.graphics.setColor(this.rightBorderLineColor);
                this.graphics.drawLine(this.x - 1, this.startY, this.x - 1, this.startY + this.lineHeight - 1);
            }
            if (this.bottomBorderLineColor != null) {
                this.graphics.setColor(this.bottomBorderLineColor);
                this.graphics.drawLine(this.startX, this.startY + this.lineHeight - 1, this.x - 1, this.startY + this.lineHeight - 1);
            }
            if (this.leftBorderLineColor != null) {
                this.graphics.setColor(this.leftBorderLineColor);
                this.graphics.drawLine(this.startX, this.startY, this.startX, this.startY + this.lineHeight - 1);
            }
            this.graphics.setColor(this.foreColor);
            this.graphics.setFont(this.font);
            if (debug) {
                String text = new String(this.buffer, this.startOffset, this.endOffset - this.startOffset);
                System.out.println("DrawGraphics: text='" + text + "', text.length=" + text.length() + ", x=" + this.startX + ", y=" + this.startY + ", ascent=" + this.lineAscent + ", clip=" + this.graphics.getClipBounds() + ", color=" + this.graphics.getColor());
            }
            GraphicsDG.drawStringTextLayout(this.component, this.graphics, new String(this.buffer, this.startOffset, this.endOffset - this.startOffset), this.startX, this.startY + this.lineAscent);
            if (this.strikeThroughColor != null) {
                fmcInfo = FontMetricsCache.getInfo(this.font);
                this.graphics.setColor(this.strikeThroughColor);
                this.graphics.fillRect(this.startX, (int)((float)this.startY + fmcInfo.getStrikethroughOffset(this.graphics) + (float)this.lineAscent), this.x - this.startX, Math.max(1, Math.round(fmcInfo.getStrikethroughThickness(this.graphics))));
            }
            if (this.waveUnderlineColor != null && this.bottomBorderLineColor == null) {
                fmcInfo = FontMetricsCache.getInfo(this.font);
                this.graphics.setColor(this.waveUnderlineColor);
                int waveLength = this.x - this.startX;
                if (waveLength > 0) {
                    int[] wf = new int[]{0, 0, -1, -1};
                    int[] xArray = new int[waveLength + 1];
                    int[] yArray = new int[waveLength + 1];
                    int yBase = (int)((double)((float)this.startY + fmcInfo.getUnderlineOffset(this.graphics) + (float)this.lineAscent) + 0.5);
                    for (i = 0; i <= waveLength; ++i) {
                        xArray[i] = this.startX + i;
                        yArray[i] = yBase + wf[xArray[i] % 4];
                    }
                    this.graphics.drawPolyline(xArray, yArray, waveLength);
                }
            }
            if (this.underlineColor != null && this.bottomBorderLineColor == null) {
                fmcInfo = FontMetricsCache.getInfo(this.font);
                this.graphics.setColor(this.underlineColor);
                this.graphics.fillRect(this.startX, (int)((double)((float)this.startY + fmcInfo.getUnderlineOffset(this.graphics) + (float)this.lineAscent) + 0.5), this.x - this.startX, Math.max(1, Math.round(fmcInfo.getUnderlineThickness(this.graphics))));
            }
            this.startOffset = -1;
        }

        @Override
        public Graphics getGraphics() {
            return this.graphics;
        }

        @Override
        public boolean supportsLineNumbers() {
            return true;
        }

        @Override
        public void fillRect(int width) {
            this.fillRectImpl(this.x, this.y, width);
            this.x += width;
        }

        private void fillRectImpl(int rx, int ry, int width) {
            if (width > 0 && !this.backColor.equals(this.defaultBackColor)) {
                this.graphics.setColor(this.backColor);
                this.graphics.fillRect(rx, ry, width, this.lineHeight);
            }
        }

        @Override
        public void drawChars(int offset, int length, int width) {
            if (length >= 0) {
                if (this.startOffset < 0) {
                    this.startOffset = offset;
                    this.endOffset = offset + length;
                    this.startX = this.x;
                    this.startY = this.y;
                    this.width = width;
                } else {
                    this.endOffset += length;
                }
            }
            this.x += width;
        }

        @Override
        public void drawTabs(int offset, int length, int spaceCount, int width) {
            if (width > 0) {
                this.flush();
                this.fillRectImpl(this.x, this.y, width);
                this.x += width;
            }
        }

        @Override
        public void setBuffer(char[] buffer) {
            this.flush();
            this.buffer = buffer;
            this.startOffset = -1;
            this.bufferStartOffset = -1;
        }

        void setBufferStartOffset(int bufferStartOffset) {
            this.bufferStartOffset = bufferStartOffset;
        }

        @Override
        public void eol() {
            int lineX;
            if (this.drawTextLimitLine && this.textLimitWidth > 0 && (lineX = this.absoluteX + this.textLimitWidth * this.defaultSpaceWidth) >= this.x - this.defaultSpaceWidth) {
                Color bakColor = this.graphics.getColor();
                this.graphics.setColor(this.textLimitLineColor);
                Rectangle clipB = this.graphics.getClipBounds();
                if (clipB.width + clipB.x <= lineX && clipB.x < this.maxWidth) {
                    this.graphics.setClip(clipB.x, clipB.y, this.maxWidth - clipB.x, clipB.height);
                    this.graphics.drawLine(lineX, this.y, lineX, this.y + this.lineHeight);
                    this.graphics.setClip(clipB.x, clipB.y, clipB.width, clipB.height);
                } else {
                    this.graphics.drawLine(lineX, this.y, lineX, this.y + this.lineHeight);
                }
                this.graphics.setColor(bakColor);
            }
            this.flush(true);
        }

        private static void drawStringTextLayout(JComponent c, Graphics g, String text, int x, int baselineY) {
            if (!(g instanceof Graphics2D)) {
                g.drawString(text, x, baselineY);
            } else {
                Graphics2D g2d = (Graphics2D)g;
                FontRenderContext frc = g2d.getFontRenderContext();
                TextLayout layout = new TextLayout(text, g2d.getFont(), frc);
                layout.draw(g2d, x, baselineY);
            }
        }
    }

    public static class SimpleDG
    extends AbstractDG {
        @Override
        public Graphics getGraphics() {
            return null;
        }

        @Override
        public boolean supportsLineNumbers() {
            return false;
        }

        @Override
        public void init(DrawContext ctx) {
        }

        @Override
        public void finish() {
        }

        @Override
        public void fillRect(int width) {
        }

        @Override
        public boolean targetOffsetReached(int offset, char ch, int x, int charWidth, DrawContext ctx) {
            return true;
        }

        @Override
        public void eol() {
        }
    }

    public static abstract class AbstractDG
    implements DrawGraphics {
        Color foreColor;
        Color backColor;
        Color defaultBackColor;
        Font font;
        char[] buffer;
        int x;
        int y;
        int lineHeight;
        int lineAscent;

        public Color getForeColor() {
            return this.foreColor;
        }

        @Override
        public void setForeColor(Color foreColor) {
            this.foreColor = foreColor;
        }

        public Color getBackColor() {
            return this.backColor;
        }

        @Override
        public void setBackColor(Color backColor) {
            this.backColor = backColor;
        }

        public Color getDefaultBackColor() {
            return this.defaultBackColor;
        }

        @Override
        public void setDefaultBackColor(Color defaultBackColor) {
            this.defaultBackColor = defaultBackColor;
        }

        public Font getFont() {
            return this.font;
        }

        @Override
        public void setFont(Font font) {
            this.font = font;
        }

        public int getX() {
            return this.x;
        }

        @Override
        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return this.y;
        }

        @Override
        public void setY(int y) {
            this.y = y;
        }

        public int getLineHeight() {
            return this.lineHeight;
        }

        @Override
        public void setLineHeight(int lineHeight) {
            this.lineHeight = lineHeight;
        }

        public int getLineAscent() {
            return this.lineAscent;
        }

        @Override
        public void setLineAscent(int lineAscent) {
            this.lineAscent = lineAscent;
        }

        public char[] getBuffer() {
            return this.buffer;
        }

        @Override
        public void setBuffer(char[] buffer) {
            this.buffer = buffer;
        }

        @Override
        public void drawChars(int offset, int length, int width) {
            this.x += width;
        }

        @Override
        public void drawTabs(int offset, int length, int spaceCount, int width) {
            this.x += width;
        }

        @Override
        public void setStrikeThroughColor(Color strikeThroughColor) {
        }

        @Override
        public void setUnderlineColor(Color underlineColor) {
        }

        @Override
        public void setWaveUnderlineColor(Color waveUnderlineColor) {
        }

        @Override
        public void setView(View view) {
        }

        @Override
        public void setTopBorderLineColor(Color topBorderLineColor) {
        }

        @Override
        public void setRightBorderLineColor(Color rightBorderLineColor) {
        }

        @Override
        public void setBottomBorderLineColor(Color bottomBorderLineColor) {
        }

        @Override
        public void setLeftBorderLineColor(Color leftBorderLineColor) {
        }
    }

}

