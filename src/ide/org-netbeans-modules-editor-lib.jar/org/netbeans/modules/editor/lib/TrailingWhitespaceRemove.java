/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.document.ModRootElement
 *  org.netbeans.modules.editor.lib2.document.TrailingWhitespaceRemoveProcessor
 *  org.netbeans.spi.editor.document.OnSaveTask
 *  org.netbeans.spi.editor.document.OnSaveTask$Context
 *  org.netbeans.spi.editor.document.OnSaveTask$Factory
 */
package org.netbeans.modules.editor.lib;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.text.Document;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.document.ModRootElement;
import org.netbeans.modules.editor.lib2.document.TrailingWhitespaceRemoveProcessor;
import org.netbeans.spi.editor.document.OnSaveTask;

public final class TrailingWhitespaceRemove
implements OnSaveTask {
    static final Logger LOG = Logger.getLogger(TrailingWhitespaceRemove.class.getName());
    private final Document doc;
    private AtomicBoolean canceled = new AtomicBoolean();

    TrailingWhitespaceRemove(Document doc) {
        this.doc = doc;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void performTask() {
        ModRootElement modRootElement;
        String policy;
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((Document)this.doc)).lookup(Preferences.class);
        if (prefs.getBoolean("on-save-use-global-settings", Boolean.TRUE)) {
            prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        }
        if (!"never".equals(policy = prefs.get("on-save-remove-trailing-whitespace", "never")) && (modRootElement = ModRootElement.get((Document)this.doc)) != null) {
            boolean origEnabled = modRootElement.isEnabled();
            modRootElement.setEnabled(false);
            try {
                new TrailingWhitespaceRemoveProcessor(this.doc, "modified-lines".equals(policy), this.canceled).removeWhitespace();
            }
            finally {
                modRootElement.setEnabled(origEnabled);
            }
        }
    }

    public void runLocked(Runnable run) {
        run.run();
    }

    public boolean cancel() {
        this.canceled.set(true);
        return true;
    }

    public static final class FactoryImpl
    implements OnSaveTask.Factory {
        public OnSaveTask createTask(OnSaveTask.Context context) {
            return new TrailingWhitespaceRemove(context.getDocument());
        }
    }

}

