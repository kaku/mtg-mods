/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.Map;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.modules.editor.lib.drawing.DrawLayerList;

public abstract class EditorUiAccessor {
    private static EditorUiAccessor ACCESSOR = null;

    public static synchronized void register(EditorUiAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized EditorUiAccessor get() {
        try {
            Class clazz = Class.forName(EditorUI.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected EditorUiAccessor() {
    }

    public abstract boolean isLineNumberVisible(EditorUI var1);

    public abstract Coloring getColoring(EditorUI var1, String var2);

    public abstract int getLineNumberMaxDigitCount(EditorUI var1);

    public abstract int getLineNumberWidth(EditorUI var1);

    public abstract int getLineNumberDigitWidth(EditorUI var1);

    public abstract Insets getLineNumberMargin(EditorUI var1);

    public abstract int getLineHeight(EditorUI var1);

    public abstract Coloring getDefaultColoring(EditorUI var1);

    public abstract int getDefaultSpaceWidth(EditorUI var1);

    public abstract Map<?, ?> getRenderingHints(EditorUI var1);

    public abstract Rectangle getExtentBounds(EditorUI var1);

    public abstract Insets getTextMargin(EditorUI var1);

    public abstract int getTextLeftMarginWidth(EditorUI var1);

    public abstract boolean getTextLimitLineVisible(EditorUI var1);

    public abstract Color getTextLimitLineColor(EditorUI var1);

    public abstract int getTextLimitWidth(EditorUI var1);

    public abstract int getLineAscent(EditorUI var1);

    public abstract void paint(EditorUI var1, Graphics var2);

    public abstract DrawLayerList getDrawLayerList(EditorUI var1);
}

