/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Logger;
import org.netbeans.editor.EditorDebug;
import org.netbeans.modules.editor.lib.drawing.DrawLayer;

public final class DrawLayerList {
    private static final Logger LOG = Logger.getLogger(DrawLayerList.class.getName());
    private static final DrawLayer[] EMPTY = new DrawLayer[0];
    private DrawLayer[] layers = EMPTY;
    private final ArrayList visibilityList = new ArrayList();

    synchronized boolean add(DrawLayer layer, int visibility) {
        if (this.indexOf(layer.getName()) >= 0) {
            return false;
        }
        int indAdd = this.layers.length;
        for (int i = 0; i < this.layers.length; ++i) {
            if ((Integer)this.visibilityList.get(i) <= visibility) continue;
            indAdd = i;
            break;
        }
        ArrayList<DrawLayer> l = new ArrayList<DrawLayer>(Arrays.asList(this.layers));
        l.add(indAdd, layer);
        this.layers = new DrawLayer[this.layers.length + 1];
        l.toArray(this.layers);
        this.visibilityList.add(indAdd, new Integer(visibility));
        return true;
    }

    synchronized void add(DrawLayerList l) {
        DrawLayer[] lta = l.layers;
        for (int i = 0; i < lta.length; ++i) {
            this.add(lta[i], (Integer)l.visibilityList.get(i));
        }
    }

    synchronized DrawLayer remove(String layerName) {
        int ind = this.indexOf(layerName);
        DrawLayer removed = null;
        if (ind >= 0) {
            removed = this.layers[ind];
            ArrayList<DrawLayer> l = new ArrayList<DrawLayer>(Arrays.asList(this.layers));
            l.remove(ind);
            this.layers = new DrawLayer[this.layers.length - 1];
            l.toArray(this.layers);
            this.visibilityList.remove(ind);
        }
        return removed;
    }

    synchronized void remove(DrawLayerList l) {
        DrawLayer[] lta = l.layers;
        for (int i = 0; i < lta.length; ++i) {
            this.remove(lta[i].getName());
        }
    }

    synchronized DrawLayer findLayer(String layerName) {
        int ind = this.indexOf(layerName);
        return ind >= 0 ? this.layers[ind] : null;
    }

    synchronized DrawLayer[] currentLayers() {
        return (DrawLayer[])this.layers.clone();
    }

    private int indexOf(String layerName) {
        for (int i = 0; i < this.layers.length; ++i) {
            if (!layerName.equals(this.layers[i].getName())) continue;
            return i;
        }
        return -1;
    }

    public String toString() {
        switch (this.layers.length) {
            case 0: {
                return "No layers";
            }
            case 1: {
                return "Standalone " + this.layers[0];
            }
        }
        return "Layers:\n" + EditorDebug.debugArray(this.layers);
    }
}

