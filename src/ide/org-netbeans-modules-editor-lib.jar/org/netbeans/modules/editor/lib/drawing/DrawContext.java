/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Color;
import java.awt.Font;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;

public interface DrawContext {
    public Color getForeColor();

    public void setForeColor(Color var1);

    public Color getBackColor();

    public void setBackColor(Color var1);

    public Color getUnderlineColor();

    public void setUnderlineColor(Color var1);

    public Color getWaveUnderlineColor();

    public void setWaveUnderlineColor(Color var1);

    public Color getStrikeThroughColor();

    public void setStrikeThroughColor(Color var1);

    public Color getTopBorderLineColor();

    public void setTopBorderLineColor(Color var1);

    public Color getRightBorderLineColor();

    public void setRightBorderLineColor(Color var1);

    public Color getBottomBorderLineColor();

    public void setBottomBorderLineColor(Color var1);

    public Color getLeftBorderLineColor();

    public void setLeftBorderLineColor(Color var1);

    public Font getFont();

    public void setFont(Font var1);

    public int getStartOffset();

    public int getEndOffset();

    public boolean isBOL();

    public boolean isEOL();

    public EditorUI getEditorUI();

    public TokenID getTokenID();

    public TokenContextPath getTokenContextPath();

    public int getTokenOffset();

    public int getTokenLength();

    public int getFragmentOffset();

    public int getFragmentLength();

    public char[] getBuffer();

    public int getBufferStartOffset();
}

