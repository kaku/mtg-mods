/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import javax.swing.text.Position;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.modules.editor.lib.drawing.DrawMark;

public final class ChainDrawMark
extends DrawMark {
    protected ChainDrawMark next;
    protected ChainDrawMark prev;

    public ChainDrawMark(String layerName, EditorUI editorUI) {
        this(layerName, editorUI, Position.Bias.Forward);
    }

    public ChainDrawMark(String layerName, EditorUI editorUI, Position.Bias bias) {
        super(layerName, editorUI, bias);
    }

    public final ChainDrawMark getNext() {
        return this.next;
    }

    public final void setNext(ChainDrawMark mark) {
        this.next = mark;
    }

    public void setNextChain(ChainDrawMark mark) {
        this.next = mark;
        if (mark != null) {
            mark.prev = this;
        }
    }

    public final ChainDrawMark getPrev() {
        return this.prev;
    }

    public final void setPrev(ChainDrawMark mark) {
        this.prev = mark;
    }

    public void setPrevChain(ChainDrawMark mark) {
        this.prev = mark;
        if (mark != null) {
            mark.next = this;
        }
    }

    public ChainDrawMark insertChain(ChainDrawMark mark) {
        ChainDrawMark thisPrev;
        mark.prev = thisPrev = this.prev;
        mark.next = this;
        if (thisPrev != null) {
            thisPrev.next = mark;
        }
        this.prev = mark;
        return mark;
    }

    public ChainDrawMark removeChain() {
        ChainDrawMark thisNext = this.next;
        ChainDrawMark thisPrev = this.prev;
        if (thisPrev != null) {
            thisPrev.next = thisNext;
            this.prev = null;
        }
        if (thisNext != null) {
            thisNext.prev = thisPrev;
            this.next = null;
        }
        try {
            this.remove();
        }
        catch (InvalidMarkException e) {
            // empty catch block
        }
        return thisNext;
    }

    public String toStringChain() {
        return this.toString() + (this.next != null ? new StringBuilder().append("\n").append(this.next.toStringChain()).toString() : "");
    }

    @Override
    public String toString() {
        return super.toString() + ", " + (this.prev != null ? (this.next != null ? "chain member" : "last member") : (this.next != null ? "first member" : "standalone member"));
    }
}

