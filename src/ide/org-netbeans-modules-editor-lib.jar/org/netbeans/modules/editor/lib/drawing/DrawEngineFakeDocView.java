/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import javax.swing.text.Element;
import org.netbeans.modules.editor.lib.drawing.DrawEngineDocView;

class DrawEngineFakeDocView
extends DrawEngineDocView {
    private boolean useCollapsing = true;
    private int fakeStartOffset;
    private int fakeEndOffset;

    DrawEngineFakeDocView(Element elem, int startOffset, int endOffset, boolean useCollapsing) {
        this(elem, startOffset, endOffset, useCollapsing, false);
    }

    DrawEngineFakeDocView(Element elem, int startOffset, int endOffset, boolean useCollapsing, boolean hideBottomPadding) {
        super(elem, hideBottomPadding);
        this.useCollapsing = useCollapsing;
        this.fakeStartOffset = startOffset;
        this.fakeEndOffset = endOffset;
        this.setEstimatedSpan(false);
    }

    @Override
    public int getStartOffset() {
        return this.fakeStartOffset;
    }

    @Override
    public int getEndOffset() {
        return this.fakeEndOffset;
    }

    @Override
    protected void attachListeners() {
    }
}

