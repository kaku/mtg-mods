/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import org.netbeans.editor.Coloring;
import org.netbeans.modules.editor.lib.drawing.DrawContext;

public abstract class ColoringAccessor {
    private static ColoringAccessor ACCESSOR = null;

    public static synchronized void register(ColoringAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized ColoringAccessor get() {
        if (ACCESSOR == null) {
            try {
                Class clazz = Class.forName(Coloring.class.getName());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected ColoringAccessor() {
    }

    public abstract void apply(Coloring var1, DrawContext var2);
}

