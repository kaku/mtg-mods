/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.document.DocumentSpiPackageAccessor
 *  org.netbeans.modules.editor.lib2.document.EditorDocumentHandler
 *  org.netbeans.modules.editor.lib2.document.ModRootElement
 *  org.netbeans.spi.editor.document.OnSaveTask
 *  org.netbeans.spi.editor.document.OnSaveTask$Context
 *  org.netbeans.spi.editor.document.OnSaveTask$Factory
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 */
package org.netbeans.modules.editor.lib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.text.Document;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.document.DocumentSpiPackageAccessor;
import org.netbeans.modules.editor.lib2.document.EditorDocumentHandler;
import org.netbeans.modules.editor.lib2.document.ModRootElement;
import org.netbeans.spi.editor.document.OnSaveTask;
import org.openide.util.Mutex;

public final class BeforeSaveTasks {
    private static final Logger LOG = Logger.getLogger(BeforeSaveTasks.class.getName());
    private static final ThreadLocal<Boolean> ignoreOnSaveTasks = new ThreadLocal<Boolean>(){

        @Override
        protected Boolean initialValue() {
            return false;
        }
    };
    private final BaseDocument doc;

    public static synchronized BeforeSaveTasks get(BaseDocument doc) {
        BeforeSaveTasks beforeSaveTasks = (BeforeSaveTasks)doc.getProperty(BeforeSaveTasks.class);
        if (beforeSaveTasks == null) {
            beforeSaveTasks = new BeforeSaveTasks(doc);
            doc.putProperty(BeforeSaveTasks.class, beforeSaveTasks);
        }
        return beforeSaveTasks;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> T runWithOnSaveTasksDisabled(Mutex.Action<T> run) {
        Boolean originalIgnore = ignoreOnSaveTasks.get();
        ignoreOnSaveTasks.set(true);
        try {
            Object object = run.run();
            return (T)object;
        }
        finally {
            ignoreOnSaveTasks.set(originalIgnore);
        }
    }

    private BeforeSaveTasks(BaseDocument doc) {
        this.doc = doc;
        Runnable beforeSaveRunnable = (Runnable)doc.getProperty("beforeSaveRunnable");
        if (beforeSaveRunnable != null) {
            throw new IllegalStateException("\"beforeSaveRunnable\" property of document " + doc + " is already occupied by " + beforeSaveRunnable);
        }
        beforeSaveRunnable = new Runnable(){

            @Override
            public void run() {
                BeforeSaveTasks.this.runTasks();
            }
        };
        doc.putProperty("beforeSaveRunnable", beforeSaveRunnable);
    }

    void runTasks() {
        if (ignoreOnSaveTasks.get() == Boolean.TRUE) {
            return;
        }
        String mimeType = DocumentUtilities.getMimeType((Document)this.doc);
        Collection factories = MimeLookup.getLookup((String)mimeType).lookupAll(OnSaveTask.Factory.class);
        OnSaveTask.Context context = DocumentSpiPackageAccessor.get().createContext((Document)this.doc);
        ArrayList<OnSaveTask> tasks = new ArrayList<OnSaveTask>(factories.size());
        for (OnSaveTask.Factory factory : factories) {
            OnSaveTask task = factory.createTask(context);
            if (task == null) continue;
            tasks.add(task);
        }
        new TaskRunnable(this.doc, tasks, context).run();
    }

    private static final class TaskRunnable
    implements Runnable {
        final BaseDocument doc;
        final List<OnSaveTask> tasks;
        final OnSaveTask.Context context;
        int lockedTaskIndex;

        public TaskRunnable(BaseDocument doc, List<OnSaveTask> tasks, OnSaveTask.Context context) {
            this.doc = doc;
            this.tasks = tasks;
            this.context = context;
        }

        @Override
        public void run() {
            if (this.lockedTaskIndex < this.tasks.size()) {
                OnSaveTask task = this.tasks.get(this.lockedTaskIndex++);
                task.runLocked((Runnable)this);
            } else {
                this.doc.runAtomicAsUser(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        Runnable beforeSaveStart = (Runnable)TaskRunnable.this.doc.getProperty("beforeSaveStart");
                        if (beforeSaveStart != null) {
                            beforeSaveStart.run();
                        }
                        UndoableEdit atomicEdit = EditorDocumentHandler.startOnSaveTasks((Document)TaskRunnable.this.doc);
                        assert (atomicEdit != null);
                        boolean success = false;
                        try {
                            DocumentSpiPackageAccessor.get().setUndoEdit(TaskRunnable.this.context, atomicEdit);
                            for (int i = 0; i < TaskRunnable.this.tasks.size(); ++i) {
                                OnSaveTask task = TaskRunnable.this.tasks.get(i);
                                DocumentSpiPackageAccessor.get().setTaskStarted(TaskRunnable.this.context, true);
                                task.performTask();
                            }
                            ModRootElement modRootElement = ModRootElement.get((Document)TaskRunnable.this.doc);
                            if (modRootElement != null) {
                                modRootElement.resetMods(atomicEdit);
                            }
                            success = true;
                        }
                        finally {
                            EditorDocumentHandler.endOnSaveTasks((Document)TaskRunnable.this.doc, (boolean)success);
                            Runnable beforeSaveEnd = (Runnable)TaskRunnable.this.doc.getProperty("beforeSaveEnd");
                            if (beforeSaveEnd != null) {
                                beforeSaveEnd.run();
                            }
                        }
                    }
                });
            }
        }

    }

}

