/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.view.spi.LockView;
import org.netbeans.lib.editor.view.GapDocumentView;
import org.netbeans.modules.editor.lib.drawing.EditorUiAccessor;

public class DrawEngineDocView
extends GapDocumentView
implements PropertyChangeListener {
    private static final boolean debugRebuild = Boolean.getBoolean("netbeans.debug.editor.view.rebuild");
    private EditorUI editorUI;
    private int collapsedFoldStartOffset;
    private boolean collapsedFoldsInPresentViews;
    private boolean estimatedSpanResetInitiated;

    public DrawEngineDocView(Element elem) {
        this(elem, false);
    }

    public DrawEngineDocView(Element elem, boolean hideBottomPadding) {
        super(elem, hideBottomPadding);
        this.setEstimatedSpan(true);
    }

    @Override
    public void setParent(View parent) {
        JTextComponent component;
        TextUI tui;
        if (parent != null && (tui = (component = (JTextComponent)parent.getContainer()).getUI()) instanceof BaseTextUI) {
            this.editorUI = ((BaseTextUI)tui).getEditorUI();
            if (this.editorUI != null) {
                this.editorUI.addPropertyChangeListener(this);
            }
        }
        super.setParent(parent);
        if (parent == null && this.editorUI != null) {
            this.editorUI.removePropertyChangeListener(this);
            this.editorUI = null;
        }
    }

    protected void attachListeners() {
    }

    @Override
    protected boolean useCustomReloadChildren() {
        return true;
    }

    @Override
    protected View createCustomView(ViewFactory f, int startOffset, int maxEndOffset, int elementIndex) {
        if (elementIndex == -1) {
            throw new IllegalStateException("Need underlying line element structure");
        }
        View view = null;
        Element elem = this.getElement();
        Element lineElem = elem.getElement(elementIndex);
        view = f.create(lineElem);
        return view;
    }

    @Override
    public void paint(Graphics g, Shape allocation) {
        TextUI textUI;
        Container c = this.getContainer();
        if (c instanceof JTextComponent && (textUI = ((JTextComponent)c).getUI()) instanceof BaseTextUI) {
            EditorUiAccessor.get().paint(((BaseTextUI)textUI).getEditorUI(), g);
        }
        super.paint(g, allocation);
        if (c != null) {
            g.setColor(c.getForeground());
        }
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
    }

    @Override
    protected boolean isChildrenResizeDisabled() {
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JTextComponent component = (JTextComponent)this.getContainer();
        if (component == null || evt == null || !"line-height-changed-prop".equals(evt.getPropertyName()) && !"tab-size-changed-prop".equals(evt.getPropertyName())) {
            return;
        }
        AbstractDocument doc = (AbstractDocument)this.getDocument();
        if (doc != null) {
            doc.readLock();
            try {
                LockView lockView = LockView.get(this);
                lockView.lock();
                try {
                    this.rebuild(0, this.getViewCount());
                }
                finally {
                    lockView.unlock();
                }
            }
            finally {
                doc.readUnlock();
            }
            component.revalidate();
        }
    }

    public int getYFromPos(int offset, Shape a) {
        int index = this.getViewIndex(offset);
        if (index >= 0) {
            Shape ca = this.getChildAllocation(index, a);
            return ca instanceof Rectangle ? ((Rectangle)ca).y : (ca != null ? ca.getBounds().y : 0);
        }
        return 0;
    }
}

