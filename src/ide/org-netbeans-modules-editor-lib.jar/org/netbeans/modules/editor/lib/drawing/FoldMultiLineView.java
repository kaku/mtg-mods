/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.lib.editor.view.GapMultiLineView;

class FoldMultiLineView
extends GapMultiLineView {
    private List foldAndEndLineElemList;

    FoldMultiLineView(Element firstLineElement, List foldAndEndLineElemList) {
        super(firstLineElement);
        this.foldAndEndLineElemList = foldAndEndLineElemList;
        int foldAndEndLineElemListSize = foldAndEndLineElemList.size();
        this.setLastLineElement((Element)foldAndEndLineElemList.get(foldAndEndLineElemList.size() - 1));
    }

    private JTextComponent getTextComponent() {
        return (JTextComponent)this.getContainer();
    }

    @Override
    protected boolean useCustomReloadChildren() {
        return true;
    }

    @Override
    protected void reloadChildren(int index, int removeLength, int startOffset, int endOffset) {
        index = 0;
        removeLength = this.getViewCount();
        Element lineElem = this.getElement();
        View[] added = null;
        ViewFactory f = this.getViewFactory();
        if (f != null) {
            int lineElemEndOffset = lineElem.getEndOffset();
            int lastViewEndOffset = lineElem.getStartOffset();
            ArrayList<View> childViews = new ArrayList<View>();
            if (lastViewEndOffset < lineElemEndOffset) {
                View lineView = f.create(lineElem);
                View endingFrag = lineView.createFragment(lastViewEndOffset, lineElemEndOffset);
                childViews.add(endingFrag);
            }
            added = new View[childViews.size()];
            childViews.toArray(added);
        }
        this.replace(index, removeLength, added);
    }
}

