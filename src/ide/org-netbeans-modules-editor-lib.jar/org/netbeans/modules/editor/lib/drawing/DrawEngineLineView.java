/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.view.spi.EstimatedSpanView;
import org.netbeans.editor.view.spi.LockView;
import org.netbeans.editor.view.spi.ViewLayoutState;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.view.GapDocumentView;
import org.netbeans.modules.editor.lib.drawing.DrawContext;
import org.netbeans.modules.editor.lib.drawing.DrawEngine;
import org.netbeans.modules.editor.lib.drawing.DrawGraphics;
import org.netbeans.modules.editor.lib.drawing.FoldMultiLineView;

public class DrawEngineLineView
extends View
implements ViewLayoutState,
EstimatedSpanView {
    private static final Logger LOG = Logger.getLogger(DrawEngineLineView.class.getName());
    private static final boolean loggable = LOG.isLoggable(Level.FINEST);
    private static final long PERF_TRESHOLD = Long.getLong("DrawEngineLineView.PERF_TRESHOLD", -1);
    private static final int X_MAJOR_AXIS_BIT = 1;
    private static final int MAJOR_AXIS_PREFERENCE_CHANGED_BIT = 2;
    private static final int MINOR_AXIS_PREFERENCE_CHANGED_BIT = 4;
    private static final int VIEW_SIZE_INVALID_BIT = 8;
    private static final int UPDATE_LAYOUT_PENDING_BIT = 16;
    private static final int ESTIMATED_SPAN_BIT = 32;
    protected static final int LAST_USED_BIT = 32;
    private static final int ANY_INVALID = 14;
    private int statusBits;
    private int viewRawIndex;
    private double layoutMajorAxisRawOffset;
    private float layoutMajorAxisPreferredSpan;
    private float layoutMinorAxisPreferredSpan;
    private ViewToModelDG viewToModelDG;
    private static final int MARKERS_DIST;
    private int[] markers = new int[]{0};
    private int markersLength = this.markers.length;

    public DrawEngineLineView(Element elem) {
        super(elem);
    }

    private int getBaseX(int orig) {
        return orig + this.getEditorUI().getTextMargin().left;
    }

    private JTextComponent getComponent() {
        return (JTextComponent)this.getContainer();
    }

    private BaseTextUI getBaseTextUI() {
        return (BaseTextUI)this.getComponent().getUI();
    }

    private EditorUI getEditorUI() {
        return this.getBaseTextUI().getEditorUI();
    }

    private ModelToViewDG getModelToViewDG() {
        return new ModelToViewDG();
    }

    private ViewToModelDG getViewToModelDG() {
        if (this.viewToModelDG == null) {
            this.viewToModelDG = new ViewToModelDG();
        }
        return this.viewToModelDG;
    }

    @Override
    public boolean isEstimatedSpan() {
        return this.isStatusBitsNonZero(32);
    }

    @Override
    public void setEstimatedSpan(boolean estimatedSpan) {
        if (this.isEstimatedSpan() != estimatedSpan) {
            if (estimatedSpan) {
                this.setStatusBits(32);
            } else {
                this.clearStatusBits(32);
                this.getParent().preferenceChanged(this, true, true);
            }
        }
    }

    protected boolean isFragment() {
        return false;
    }

    private int getEOLffset() {
        return super.getEndOffset() - 1;
    }

    private int getAdjustedEOLOffset() {
        return Math.min(this.getEndOffset(), this.getEOLffset());
    }

    @Override
    public void insertUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        this.resetMarkers(e.getOffset());
        this.preferenceChanged(this, true, false);
    }

    @Override
    public void removeUpdate(DocumentEvent e, Shape a, ViewFactory f) {
        this.resetMarkers(e.getOffset());
        this.preferenceChanged(this, true, false);
    }

    @Override
    public float getAlignment(int axis) {
        return 0.0f;
    }

    @Override
    public void paint(Graphics g, Shape a) {
        if (!(this.getDocument() instanceof BaseDocument)) {
            return;
        }
        this.setEstimatedSpan(false);
        Rectangle allocReadOnly = a instanceof Rectangle ? (Rectangle)a : a.getBounds();
        int startOffset = this.getStartOffset();
        int endOffset = this.getAdjustedEOLOffset();
        try {
            if (this.isFragment()) {
                Rectangle oldClipRect = g.getClipBounds();
                Rectangle newClip = new Rectangle(oldClipRect);
                Rectangle startOffsetClip = this.modelToView(startOffset, a, Position.Bias.Forward).getBounds();
                Rectangle endOffsetClip = this.modelToView(endOffset, a, Position.Bias.Forward).getBounds();
                View parent = this.getParent();
                if (parent instanceof FoldMultiLineView && !this.equals(parent.getView(parent.getViewCount() - 1))) {
                    newClip.width = Math.min(oldClipRect.width, endOffsetClip.x);
                    if (newClip.width + newClip.x > endOffsetClip.x) {
                        newClip.width -= newClip.width + newClip.x - endOffsetClip.x;
                    }
                    g.setClip(newClip);
                }
                int shift = startOffsetClip.x - this.getEditorUI().getTextMargin().left - allocReadOnly.x;
                g.translate(- shift, 0);
                DrawEngine.getDrawEngine().draw(this, new DrawGraphics.GraphicsDG(g), this.getEditorUI(), startOffset, endOffset, this.getBaseX(allocReadOnly.x), allocReadOnly.y, Integer.MAX_VALUE);
                g.translate(shift, 0);
                g.setClip(oldClipRect);
            } else {
                JTextComponent component = this.getComponent();
                if (component != null) {
                    long ts1 = 0;
                    long ts2 = 0;
                    if (loggable) {
                        ts1 = System.currentTimeMillis();
                    }
                    Rectangle clip = g.getClipBounds();
                    int fromOffset = this.viewToModel(clip.x, clip.y, allocReadOnly, null);
                    int toOffset = this.viewToModel(clip.x + clip.width, clip.y, allocReadOnly, null);
                    fromOffset = Math.max(fromOffset - 1, this.getStartOffset());
                    toOffset = Math.min(toOffset + 1, this.getAdjustedEOLOffset());
                    Rectangle rr = this.modelToView(fromOffset, allocReadOnly, Position.Bias.Forward).getBounds();
                    DrawEngine.getDrawEngine().draw(this, new DrawGraphics.GraphicsDG(g), this.getEditorUI(), fromOffset, toOffset, rr.x, rr.y, Integer.MAX_VALUE);
                    if (loggable && (ts2 = System.currentTimeMillis()) - ts1 > PERF_TRESHOLD) {
                        LOG.finest("paint: <" + fromOffset + ", " + toOffset + ">, " + "DrawEngine.startX = " + rr.x + ", DrawEngine.startY = " + rr.y + ", " + "shape = [" + allocReadOnly.x + ", " + allocReadOnly.y + ", " + allocReadOnly.width + ", " + allocReadOnly.height + "], " + "clip = [" + clip.x + ", " + clip.y + ", " + clip.width + ", " + clip.height + "] " + "took " + (ts2 - ts1) + " msec");
                    }
                }
            }
        }
        catch (BadLocationException ble) {
            LOG.log(Level.INFO, "Painting the view failed", ble);
        }
    }

    @Override
    public float getPreferredSpan(int axis) {
        switch (axis) {
            case 1: {
                return this.getEditorUI().getLineHeight();
            }
            case 0: {
                int offset = Math.max(0, this.getEndOffset() - 1);
                Shape retShape = this.modelToView(offset, new Rectangle(), Position.Bias.Forward, false);
                int ret = retShape.getBounds().x + retShape.getBounds().width;
                return Math.max((float)ret, 1.0f);
            }
        }
        return 1.0f;
    }

    public void highlightsChanged(int changeStart, int changeEnd) {
        this.checkViewAccess();
        this.resetMarkers(changeStart);
        this.preferenceChanged(this, true, false);
    }

    private void resetMarkers(int offset) {
        this.markersLength = offset < this.getStartOffset() || offset > this.getEndOffset() ? 1 : Math.min(this.markersLength, (offset - this.getStartOffset()) / MARKERS_DIST + 1);
        if (loggable) {
            LOG.finest("resetMarkers: <" + this.getStartOffset() + ", " + this.getEndOffset() + ">, offset = " + offset + " -> markersLength = " + this.markersLength);
        }
    }

    private Rectangle getModel2ViewRect(int startOffset, int endOffset, int startX, int startY, int targetOffset) {
        Rectangle ret;
        long ts1 = 0;
        long ts2 = 0;
        EditorUI eui = this.getEditorUI();
        View parent = this.getParent();
        if (parent instanceof GapDocumentView && ((GapDocumentView)parent).isPendingUpdate() || this.isEstimatedSpan()) {
            ret = new Rectangle(this.getBaseX(startX), startY, 1, eui.getLineHeight());
        } else {
            if (loggable) {
                ts1 = System.currentTimeMillis();
            }
            int targetMarkerIdx = (targetOffset - startOffset) / MARKERS_DIST;
            int markerIdx = Math.min(targetMarkerIdx, this.markersLength - 1);
            int markerX = this.markers[markerIdx];
            int markerOffset = startOffset + markerIdx * MARKERS_DIST;
            ret = new Rectangle(this.getBaseX(markerX), startY, 1, eui.getLineHeight());
            try {
                ModelToViewDG g = this.getModelToViewDG();
                g.setRectangle(ret);
                if (this.markers.length <= targetMarkerIdx) {
                    int[] arr = new int[targetMarkerIdx + 1];
                    System.arraycopy(this.markers, 0, arr, 0, this.markers.length);
                    this.markers = arr;
                }
                while (markerIdx < targetMarkerIdx) {
                    DrawEngine.getDrawEngine().draw(this, g, eui, markerOffset, markerOffset + MARKERS_DIST, markerX, startY, markerOffset + MARKERS_DIST);
                    markerOffset += MARKERS_DIST;
                    this.markers[markerIdx + 1] = markerX = ret.x;
                    ++markerIdx;
                }
                if (targetMarkerIdx >= this.markersLength) {
                    this.markersLength = targetMarkerIdx + 1;
                }
                DrawEngine.getDrawEngine().draw(this, g, eui, markerOffset, endOffset, this.getBaseX(markerX + startX), startY, targetOffset);
                g.setRectangle(null);
            }
            catch (BadLocationException ble) {
                LOG.log(Level.INFO, "Model-to-view translation failed", ble);
                ret = new Rectangle(this.getBaseX(startX), startY, 1, eui.getLineHeight());
            }
            if (loggable) {
                ts2 = System.currentTimeMillis();
            }
        }
        if (loggable && ts2 - ts1 > PERF_TRESHOLD) {
            LOG.finest("m2v: <" + startOffset + ", " + endOffset + ">, targetOffset = " + targetOffset + ", " + "[" + startX + ", " + startY + "] " + "-> [" + ret.getBounds().x + ", " + ret.getBounds().y + ", " + ret.getBounds().width + ", " + ret.getBounds().height + "]" + " took " + (ts2 - ts1) + " msec");
        }
        return ret;
    }

    @Override
    public Shape modelToView(int pos, Shape shape, Position.Bias b) {
        return this.modelToView(pos, shape, b, true);
    }

    public Shape modelToView(int pos, Shape shape, Position.Bias bias, boolean exactSpan) {
        assert (shape != null);
        this.checkViewAccess();
        if (!(this.getDocument() instanceof BaseDocument)) {
            return new Rectangle();
        }
        if (exactSpan) {
            this.setEstimatedSpan(false);
        }
        if (bias == Position.Bias.Forward && (pos < super.getStartOffset() || pos >= super.getEndOffset()) || bias == Position.Bias.Backward && (pos <= super.getStartOffset() || pos > super.getEndOffset())) {
            BadLocationException ble = new BadLocationException("Invalid offset = " + pos + ", bias = " + bias + ", outside of the view <" + super.getStartOffset() + ", " + super.getEndOffset() + ">" + ", isFragment = " + this.isFragment() + (this.isFragment() ? new StringBuilder().append(", fragment boundaries <").append(this.getStartOffset()).append(", ").append(this.getEndOffset()).append(">").toString() : ""), pos);
            LOG.log(Level.INFO, null, ble);
            return new Rectangle(this.getBaseX(shape.getBounds().x), shape.getBounds().y, 1, this.getEditorUI().getLineHeight());
        }
        if (this.isFragment() && (pos < this.getStartOffset() || pos > this.getEndOffset())) {
            BadLocationException ble = new BadLocationException("Invalid offset = " + pos + ", bias = " + bias + ", outside of the fragment view" + " <" + this.getStartOffset() + ", " + this.getEndOffset() + ">", pos);
            LOG.log(Level.INFO, null, ble);
            return new Rectangle(this.getBaseX(shape.getBounds().x), shape.getBounds().y, 1, this.getEditorUI().getLineHeight());
        }
        if (bias == Position.Bias.Backward) {
            --pos;
        }
        Rectangle ret = this.getModel2ViewRect(this.getStartOffset(), this.getEndOffset(), shape.getBounds().x, shape.getBounds().y, pos);
        return ret;
    }

    @Override
    public int viewToModel(float x, float y, Shape shape, Position.Bias[] biasReturn) {
        assert (shape != null);
        this.checkViewAccess();
        if (!(this.getDocument() instanceof BaseDocument)) {
            return 0;
        }
        long ts1 = 0;
        long ts2 = 0;
        int pos = this.getStartOffset();
        if (biasReturn != null) {
            biasReturn[0] = Position.Bias.Forward;
        }
        if (!this.isEstimatedSpan() && x > (float)shape.getBounds().x) {
            if (loggable) {
                ts1 = System.currentTimeMillis();
            }
            EditorUI eui = this.getEditorUI();
            int xx = Math.max(0, (int)x - shape.getBounds().x - eui.getTextMargin().left);
            int markerIdx = ArrayUtilities.binarySearch((int[])this.markers, (int)0, (int)(this.markersLength - 1), (int)xx);
            if (markerIdx >= 0) {
                pos = this.getStartOffset() + markerIdx * MARKERS_DIST;
            } else {
                markerIdx = - markerIdx - 2;
                int markerX = this.markers[markerIdx];
                int markerOffset = this.getStartOffset() + markerIdx * MARKERS_DIST;
                try {
                    ViewToModelDG g = this.getViewToModelDG();
                    do {
                        int nextOffset = Math.min(this.getAdjustedEOLOffset(), markerOffset + MARKERS_DIST - 1);
                        g.setTargetX(xx);
                        g.setEOLOffset(nextOffset);
                        DrawEngine.getDrawEngine().draw(this, g, eui, markerOffset, nextOffset, markerX, shape.getBounds().y, -1);
                        if (g.getX() >= xx || g.getOffset() >= this.getAdjustedEOLOffset()) break;
                        markerOffset += MARKERS_DIST;
                        markerX = g.getX();
                        if (markerIdx + 1 >= this.markers.length) {
                            int[] arr = new int[this.markers.length + 10];
                            System.arraycopy(this.markers, 0, arr, 0, this.markers.length);
                            this.markers = arr;
                        }
                        this.markers[++markerIdx] = markerX;
                        this.markersLength = markerIdx + 1;
                    } while (true);
                    pos = Math.min(g.getOffset(), this.getAdjustedEOLOffset());
                }
                catch (BadLocationException ble) {
                    LOG.log(Level.INFO, "View-to-model translation failed", ble);
                }
            }
            if (loggable) {
                ts2 = System.currentTimeMillis();
            }
        }
        if (loggable && ts2 - ts1 > PERF_TRESHOLD) {
            LOG.finest("v2m: [" + x + ", " + y + "], " + "[" + shape.getBounds().x + ", " + shape.getBounds().y + ", " + shape.getBounds().width + ", " + shape.getBounds().height + "] " + "-> " + pos + " took " + (ts2 - ts1) + " msec");
        }
        return pos;
    }

    private void checkViewAccess() {
        LockView view = LockView.get(this);
        if (view != null && view.getLockThread() != Thread.currentThread()) {
            throw new IllegalStateException("View access without view lock");
        }
    }

    @Override
    public View createFragment(int p0, int p1) {
        Element elem = this.getElement();
        return p0 >= 0 && p0 >= elem.getStartOffset() && p0 < elem.getEndOffset() && p1 > 0 && p1 <= elem.getEndOffset() && p1 > elem.getStartOffset() && (p0 != elem.getStartOffset() || p1 != elem.getEndOffset()) ? new FragmentView(this.getElement(), p0 - elem.getStartOffset(), p1 - p0) : this;
    }

    @Override
    public double getLayoutMajorAxisPreferredSpan() {
        return this.layoutMajorAxisPreferredSpan;
    }

    public float getLayoutMajorAxisPreferredSpanFloat() {
        return this.layoutMajorAxisPreferredSpan;
    }

    protected void setLayoutMajorAxisPreferredSpan(float layoutMajorAxisPreferredSpan) {
        this.layoutMajorAxisPreferredSpan = layoutMajorAxisPreferredSpan;
    }

    @Override
    public double getLayoutMajorAxisRawOffset() {
        return this.layoutMajorAxisRawOffset;
    }

    @Override
    public void setLayoutMajorAxisRawOffset(double layoutMajorAxisRawOffset) {
        this.layoutMajorAxisRawOffset = layoutMajorAxisRawOffset;
    }

    @Override
    public float getLayoutMinorAxisAlignment() {
        return this.getAlignment(this.getMinorAxis());
    }

    @Override
    public float getLayoutMinorAxisMaximumSpan() {
        return this.getLayoutMinorAxisPreferredSpan();
    }

    @Override
    public float getLayoutMinorAxisMinimumSpan() {
        return this.getLayoutMinorAxisPreferredSpan();
    }

    @Override
    public float getLayoutMinorAxisPreferredSpan() {
        return this.layoutMinorAxisPreferredSpan;
    }

    protected void setLayoutMinorAxisPreferredSpan(float layoutMinorAxisPreferredSpan) {
        this.layoutMinorAxisPreferredSpan = layoutMinorAxisPreferredSpan;
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public int getViewRawIndex() {
        return this.viewRawIndex;
    }

    @Override
    public void setViewRawIndex(int viewRawIndex) {
        this.viewRawIndex = viewRawIndex;
    }

    @Override
    public boolean isFlyweight() {
        return false;
    }

    @Override
    public ViewLayoutState selectLayoutMajorAxis(int majorAxis) {
        if (majorAxis == 0) {
            this.setStatusBits(1);
        } else {
            this.clearStatusBits(1);
        }
        return this;
    }

    protected final ViewLayoutState.Parent getLayoutStateParent() {
        View parent = this.getView().getParent();
        return parent instanceof ViewLayoutState.Parent ? (ViewLayoutState.Parent)((Object)parent) : null;
    }

    @Override
    public void updateLayout() {
        if (this.isLayoutValid()) {
            return;
        }
        ViewLayoutState.Parent lsParent = this.getLayoutStateParent();
        if (lsParent == null) {
            return;
        }
        if (this.isStatusBitsNonZero(4)) {
            this.clearStatusBits(4);
            int minorAxis = this.getMinorAxis();
            if (this.minorAxisUpdateLayout(minorAxis)) {
                lsParent.minorAxisPreferenceChanged(this);
            }
        }
        if (this.isStatusBitsNonZero(2)) {
            this.clearStatusBits(2);
            float oldSpan = this.getLayoutMajorAxisPreferredSpanFloat();
            float newSpan = this.getPreferredSpan(this.getMajorAxis());
            this.setLayoutMajorAxisPreferredSpan(newSpan);
            double majorAxisSpanDelta = newSpan - oldSpan;
            if (majorAxisSpanDelta != 0.0) {
                lsParent.majorAxisPreferenceChanged(this, majorAxisSpanDelta);
            }
        }
        if (this.isStatusBitsNonZero(8)) {
            float height;
            float width;
            this.clearStatusBits(8);
            float majorAxisSpan = (float)this.getLayoutMajorAxisPreferredSpan();
            float minorAxisSpan = lsParent.getMinorAxisSpan(this);
            if (this.isXMajorAxis()) {
                width = majorAxisSpan;
                height = minorAxisSpan;
            } else {
                width = minorAxisSpan;
                height = majorAxisSpan;
            }
            this.setSize(width, height);
        }
        this.updateLayout();
    }

    protected boolean minorAxisUpdateLayout(int minorAxis) {
        boolean minorAxisPreferenceChanged = false;
        float val = this.getPreferredSpan(minorAxis);
        if (val != this.getLayoutMinorAxisPreferredSpan()) {
            this.setLayoutMinorAxisPreferredSpan(val);
            minorAxisPreferenceChanged = true;
        }
        return minorAxisPreferenceChanged;
    }

    @Override
    public void viewPreferenceChanged(boolean width, boolean height) {
        if (this.isXMajorAxis()) {
            if (width) {
                this.setStatusBits(2);
            }
            if (height) {
                this.setStatusBits(4);
            }
        } else {
            if (width) {
                this.setStatusBits(4);
            }
            if (height) {
                this.setStatusBits(2);
            }
        }
        this.setStatusBits(8);
    }

    @Override
    public void markViewSizeInvalid() {
        this.setStatusBits(8);
    }

    @Override
    public boolean isLayoutValid() {
        return !this.isStatusBitsNonZero(14);
    }

    protected final boolean isXMajorAxis() {
        return this.isStatusBitsNonZero(1);
    }

    protected final int getMajorAxis() {
        return this.isXMajorAxis() ? 0 : 1;
    }

    protected final int getMinorAxis() {
        return this.isXMajorAxis() ? 1 : 0;
    }

    protected final int getStatusBits(int bits) {
        return this.statusBits & bits;
    }

    protected final boolean isStatusBitsNonZero(int bits) {
        return this.getStatusBits(bits) != 0;
    }

    protected final void setStatusBits(int bits) {
        this.statusBits |= bits;
    }

    protected final void clearStatusBits(int bits) {
        this.statusBits &= ~ bits;
    }

    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        switch (direction) {
            case 7: {
                pos = super.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
                if (!Character.isLowSurrogate(DocumentUtilities.getText((Document)this.getDocument()).charAt(pos))) break;
                return super.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
            }
            case 3: {
                pos = super.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
                if (!Character.isLowSurrogate(DocumentUtilities.getText((Document)this.getDocument()).charAt(pos))) break;
                return super.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
            }
            default: {
                pos = super.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
            }
        }
        return pos;
    }

    static {
        int markersDist = 128;
        try {
            markersDist = (Integer)Class.forName("org.netbeans.editor.DrawEngineTest").getField("TEST_MARKERS_DIST").get(null);
        }
        catch (Exception e) {
            // empty catch block
        }
        MARKERS_DIST = markersDist;
        LOG.fine("DrawEngineLineView.MARKERS_DIST = " + MARKERS_DIST);
    }

    private static final class FragmentView
    extends DrawEngineLineView {
        private Position startPos;
        private Position endPos;

        public FragmentView(Element elem, int offset, int length) {
            super(elem);
            try {
                Document doc = elem.getDocument();
                this.startPos = doc.createPosition(super.getStartOffset() + offset);
                this.endPos = doc.createPosition(this.startPos.getOffset() + length);
            }
            catch (BadLocationException ble) {
                LOG.log(Level.INFO, "Can't create fragment view, offset = " + offset + ", length = " + length, ble);
            }
        }

        @Override
        protected boolean isFragment() {
            return true;
        }

        @Override
        public int getStartOffset() {
            return this.startPos.getOffset();
        }

        @Override
        public int getEndOffset() {
            return this.endPos.getOffset();
        }
    }

    private final class ModelToViewDG
    extends DrawGraphics.SimpleDG {
        private Rectangle r;

        private ModelToViewDG() {
        }

        public Rectangle getRectangle() {
            return this.r;
        }

        public void setRectangle(Rectangle r) {
            this.r = r;
        }

        @Override
        public boolean targetOffsetReached(int pos, char ch, int x, int charWidth, DrawContext ctx) {
            this.r.x = x;
            this.r.y = this.getY();
            this.r.width = charWidth;
            this.r.height = DrawEngineLineView.this.getEditorUI().getLineHeight();
            return false;
        }
    }

    private final class ViewToModelDG
    extends DrawGraphics.SimpleDG {
        private int targetX;
        private int offset;
        private int eolOffset;

        private ViewToModelDG() {
        }

        public void setTargetX(int targetX) {
            this.targetX = targetX;
        }

        public void setEOLOffset(int eolOffset) {
            this.eolOffset = eolOffset;
            this.offset = eolOffset;
        }

        public int getOffset() {
            return this.offset;
        }

        @Override
        public boolean targetOffsetReached(int offset, char ch, int x, int charWidth, DrawContext ctx) {
            if (offset <= this.eolOffset) {
                if (x + charWidth < this.targetX) {
                    this.offset = offset;
                    return true;
                }
                this.offset = offset;
                if (this.targetX > x + charWidth / 2) {
                    Document doc = DrawEngineLineView.this.getDocument();
                    if (ch != '\n' && doc != null && offset < doc.getLength()) {
                        ++this.offset;
                    }
                }
                return false;
            }
            return false;
        }
    }

}

