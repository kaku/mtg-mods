/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.lib;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.openide.util.Lookup;

public abstract class KitsTracker {
    private static final Logger LOG = Logger.getLogger(KitsTracker.class.getName());
    private static final Set<String> ALREADY_LOGGED = Collections.synchronizedSet(new HashSet<E>(10));
    private static KitsTracker instance = null;
    private final PropertyChangeSupport PCS;

    public static synchronized KitsTracker getInstance() {
        if (instance == null && (KitsTracker.instance = (KitsTracker)Lookup.getDefault().lookup(KitsTracker.class)) == null) {
            instance = new KitsTracker(){
                private final ThreadLocal<String> context = new ThreadLocal();

                @Override
                public List<String> getMimeTypesForKitClass(Class kitClass) {
                    String mimeType = this.findMimeType(kitClass);
                    if (mimeType != null) {
                        return Collections.singletonList(mimeType);
                    }
                    return Collections.emptyList();
                }

                @Override
                public String findMimeType(Class kitClass) {
                    if (kitClass != null) {
                        return this.context.get();
                    }
                    return "";
                }

                @Override
                public Class<?> findKitClass(String mimeType) {
                    return null;
                }

                @Override
                public Set<String> getMimeTypes() {
                    return Collections.emptySet();
                }

                @Override
                public String setContextMimeType(String mimeType) {
                    if (mimeType != null && MimePath.validate((CharSequence)mimeType) && !$assertionsDisabled) {
                        throw new AssertionError((Object)("Invalid mimeType: '" + mimeType + "'"));
                    }
                    String previous = this.context.get();
                    this.context.set(mimeType);
                    return previous;
                }
            };
        }
        return instance;
    }

    public static String getGenericPartOfCompoundMimeType(String mimeType) {
        int plusIdx = mimeType.lastIndexOf(43);
        if (plusIdx != -1 && plusIdx < mimeType.length() - 1) {
            int slashIdx = mimeType.indexOf(47);
            String prefix = mimeType.substring(0, slashIdx + 1);
            String suffix = mimeType.substring(plusIdx + 1);
            if (suffix.equals("xml")) {
                prefix = "text/";
            }
            return prefix + suffix;
        }
        return null;
    }

    public abstract List<String> getMimeTypesForKitClass(Class var1);

    public abstract String findMimeType(Class var1);

    public abstract Class findKitClass(String var1);

    public abstract Set<String> getMimeTypes();

    public abstract String setContextMimeType(String var1);

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.PCS.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.PCS.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String name, Object oldValue, Object newValue) {
        this.PCS.firePropertyChange(name, oldValue, newValue);
    }

    protected KitsTracker() {
        this.PCS = new PropertyChangeSupport(this);
    }

}

