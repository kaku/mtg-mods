/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.lib;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.openide.util.Lookup;

public final class SettingsConversions {
    private static final Logger LOG = Logger.getLogger(SettingsConversions.class.getName());
    private static volatile boolean noSettingsChangeCalls = false;
    private static final Map<Class, Boolean> settingsChangeAvailable = Collections.synchronizedMap(new WeakHashMap());

    public static String insetsToString(Insets ins) {
        StringBuilder sb = new StringBuilder();
        sb.append(ins.top);
        sb.append(',');
        sb.append(ins.left);
        sb.append(',');
        sb.append(ins.bottom);
        sb.append(',');
        sb.append(ins.right);
        return sb.toString();
    }

    public static Insets parseInsets(String s) {
        StringTokenizer st = new StringTokenizer(s, ",");
        int[] arr = new int[4];
        int i = 0;
        while (st.hasMoreElements()) {
            if (i > 3) {
                return null;
            }
            try {
                arr[i] = Integer.parseInt(st.nextToken());
            }
            catch (NumberFormatException nfe) {
                LOG.log(Level.WARNING, null, nfe);
                return null;
            }
            ++i;
        }
        if (i != 4) {
            return null;
        }
        return new Insets(arr[0], arr[1], arr[2], arr[3]);
    }

    public static String dimensionToString(Dimension dim) {
        StringBuilder sb = new StringBuilder();
        sb.append(dim.width);
        sb.append(',');
        sb.append(dim.height);
        return sb.toString();
    }

    public static Dimension parseDimension(String s) {
        StringTokenizer st = new StringTokenizer(s, ",");
        int[] arr = new int[2];
        int i = 0;
        while (st.hasMoreElements()) {
            if (i > 1) {
                return null;
            }
            try {
                arr[i] = Integer.parseInt(st.nextToken());
            }
            catch (NumberFormatException nfe) {
                LOG.log(Level.WARNING, null, nfe);
                return null;
            }
            ++i;
        }
        if (i != 2) {
            return null;
        }
        return new Dimension(arr[0], arr[1]);
    }

    private static String wrap(String s) {
        return s.length() == 1 ? "0" + s : s;
    }

    public static String color2String(Color c) {
        StringBuilder sb = new StringBuilder();
        sb.append('#');
        sb.append(SettingsConversions.wrap(Integer.toHexString(c.getRed()).toUpperCase()));
        sb.append(SettingsConversions.wrap(Integer.toHexString(c.getGreen()).toUpperCase()));
        sb.append(SettingsConversions.wrap(Integer.toHexString(c.getBlue()).toUpperCase()));
        return sb.toString();
    }

    public static Color parseColor(String s) {
        try {
            return Color.decode(s);
        }
        catch (NumberFormatException nfe) {
            LOG.log(Level.WARNING, null, nfe);
            return null;
        }
    }

    public static Object callFactory(Preferences prefs, MimePath mimePath, String settingName, Object defaultValue) {
        String factoryRef = prefs.get(settingName, null);
        if (factoryRef != null) {
            int lastDot = factoryRef.lastIndexOf(46);
            assert (lastDot != -1);
            String classFqn = factoryRef.substring(0, lastDot);
            String methodName = factoryRef.substring(lastDot + 1);
            ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            try {
                Method factoryMethod;
                Class factoryClass = loader.loadClass(classFqn);
                try {
                    factoryMethod = factoryClass.getDeclaredMethod(methodName, MimePath.class, String.class);
                }
                catch (NoSuchMethodException nsme) {
                    try {
                        factoryMethod = factoryClass.getDeclaredMethod(methodName, new Class[0]);
                    }
                    catch (NoSuchMethodException nsme2) {
                        throw nsme;
                    }
                }
                Object value = factoryMethod.getParameterTypes().length == 2 ? factoryMethod.invoke(null, new Object[]{mimePath, settingName}) : factoryMethod.invoke(null, new Object[0]);
                if (value != null) {
                    return value;
                }
            }
            catch (Exception e) {
                LOG.log(Level.INFO, null, e);
            }
        }
        return defaultValue;
    }

    public static void callSettingsChange(Object instance) {
        Class eventClass;
        assert (instance != null);
        if (noSettingsChangeCalls) {
            return;
        }
        try {
            ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            if (loader == null) {
                loader = SettingsConversions.class.getClassLoader();
            }
            eventClass = loader.loadClass("org.netbeans.editor.SettingsChangeEvent");
        }
        catch (ClassNotFoundException e) {
            noSettingsChangeCalls = true;
            return;
        }
        Class clazz = instance.getClass();
        Boolean hasMethod = settingsChangeAvailable.get(clazz);
        if (hasMethod == null || hasMethod.booleanValue()) {
            AccessibleObject method = null;
            try {
                method = clazz.getMethod("settingsChange", eventClass);
            }
            catch (NoSuchMethodException e) {
                // empty catch block
            }
            if (method == null || !method.isAccessible()) {
                settingsChangeAvailable.put(clazz, false);
                return;
            }
            settingsChangeAvailable.put(clazz, true);
            try {
                method.invoke(instance, new Object[]{null});
            }
            catch (InvocationTargetException ite) {
                throw new RuntimeException(ite.getCause());
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, null, e);
            }
        }
    }

    private SettingsConversions() {
    }
}

