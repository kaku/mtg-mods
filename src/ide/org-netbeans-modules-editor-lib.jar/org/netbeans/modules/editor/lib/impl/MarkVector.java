/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.impl;

import java.util.List;
import javax.swing.text.Position;
import org.netbeans.modules.editor.lib.impl.BasePosition;
import org.netbeans.modules.editor.lib.impl.MultiMark;

public final class MarkVector {
    private static final MultiMark[] EMPTY = new MultiMark[0];
    private static final int INITIAL_OFFSET_GAP_SIZE = 1073741823;
    private MultiMark[] markArray = EMPTY;
    private int gapStart;
    private int gapLength;
    private int offsetGapStart;
    private int offsetGapLength = 1073741823;
    private int disposedMarkCount;

    public MultiMark createBiasMark(int offset, Position.Bias bias) {
        return new MultiMark(null, this, offset, bias);
    }

    public MultiMark createBiasMark(BasePosition pos, int offset, Position.Bias bias) {
        return new MultiMark(pos, this, offset, bias);
    }

    public MultiMark createMark(int offset) {
        return new MultiMark(null, this, offset);
    }

    public MultiMark createMark(BasePosition pos, int offset) {
        return new MultiMark(pos, this, offset);
    }

    public synchronized int getMarkCount() {
        return this.markArray.length - this.gapLength;
    }

    public synchronized MultiMark getMark(int index) {
        return this.markArray[this.getRawIndex(index)];
    }

    public synchronized int getMarkOffsetInternal(int index) {
        return this.getOffset(this.getMark((int)index).rawOffset);
    }

    public synchronized MultiMark insert(MultiMark mark) {
        int flags = mark.flags;
        if ((flags & 2) != 0) {
            throw new IllegalStateException();
        }
        int offset = mark.rawOffset;
        int index = this.findInsertIndex(offset);
        if (this.gapLength == 0) {
            this.enlargeGap(1);
        }
        if (index != this.gapStart) {
            this.moveGap(index);
        }
        if (offset > this.offsetGapStart || offset == this.offsetGapStart && (flags & 1) == 0) {
            mark.rawOffset += this.offsetGapLength;
        }
        this.markArray[this.gapStart++] = mark;
        --this.gapLength;
        mark.flags |= 2;
        return mark;
    }

    synchronized void insertList(List markList) {
        int lastOffset = Integer.MAX_VALUE;
        boolean lastBackwardBias = true;
        int upperOffset = 0;
        boolean upperBackwardBias = false;
        int markCount = this.getMarkCount();
        int insertMarkCount = markList.size();
        if (this.gapLength < insertMarkCount) {
            this.enlargeGap(insertMarkCount);
        }
        for (int i = 0; i < insertMarkCount; ++i) {
            MultiMark mark = (MultiMark)markList.get(i);
            int flags = mark.flags;
            if ((flags & 2) != 0) {
                throw new IllegalStateException();
            }
            boolean backwardBias = (flags & 1) != 0;
            int offset = mark.rawOffset;
            if (offset < lastOffset || offset == lastOffset && backwardBias && !lastBackwardBias || offset > upperOffset || offset == upperOffset && !backwardBias && upperBackwardBias) {
                int index = this.findInsertIndex(offset);
                if (index != this.gapStart) {
                    this.moveGap(index);
                }
                if (index < markCount) {
                    MultiMark m = this.markArray[this.getRawIndex(index)];
                    upperOffset = this.getOffset(m.rawOffset);
                    upperBackwardBias = (m.flags & 1) != 0;
                } else {
                    upperOffset = Integer.MAX_VALUE;
                    upperBackwardBias = false;
                }
            }
            if (offset > this.offsetGapStart || offset == this.offsetGapStart && (flags & 1) == 0) {
                mark.rawOffset += this.offsetGapLength;
            }
            this.markArray[this.gapStart++] = mark;
            --this.gapLength;
            mark.flags |= 2;
            lastOffset = offset;
            lastBackwardBias = backwardBias;
            ++markCount;
        }
    }

    synchronized void notifyMarkDisposed() {
        ++this.disposedMarkCount;
        if (this.disposedMarkCount > Math.max(5, this.getMarkCount() / 10)) {
            this.removeDisposedMarks();
        }
    }

    public synchronized void compact() {
        if (this.gapLength > 0) {
            int newLength = this.markArray.length - this.gapLength;
            MultiMark[] newMarkArray = new MultiMark[newLength];
            int gapEnd = this.gapStart + this.gapLength;
            System.arraycopy(this.markArray, 0, newMarkArray, 0, this.gapStart);
            System.arraycopy(this.markArray, gapEnd, newMarkArray, this.gapStart, this.markArray.length - gapEnd);
            this.markArray = newMarkArray;
            this.gapStart = this.markArray.length;
            this.gapLength = 0;
        }
    }

    public synchronized Undo update(int offset, int length, Undo undo) {
        if (length < 0) {
            offset -= length;
        }
        int offsetGapIndex = this.findInsertIndex(offset);
        this.moveOffsetGap(offsetGapIndex, offset);
        this.offsetGapStart += length;
        this.offsetGapLength -= length;
        if (length >= 0) {
            if (undo != null) {
                UndoItem dirFirstItem = undo.fbItem;
                int fbUndoMarkCount = 0;
                while (dirFirstItem != null) {
                    if ((dirFirstItem.mark.flags & 4) == 0) {
                        ++fbUndoMarkCount;
                        UndoItem item = dirFirstItem.logicalNext;
                        while (item != null) {
                            if ((item.mark.flags & 4) == 0) {
                                ++fbUndoMarkCount;
                            } else {
                                item.mark = null;
                            }
                            item = item.logicalNext;
                        }
                        break;
                    }
                    dirFirstItem.mark = null;
                    dirFirstItem = dirFirstItem.logicalNext;
                }
                if (dirFirstItem != null) {
                    int index;
                    MultiMark firstItemMark = dirFirstItem.mark;
                    for (index = offsetGapIndex; index < this.markArray.length && this.markArray[this.getRawIndex(index)] != firstItemMark; ++index) {
                    }
                    while (--index >= offsetGapIndex) {
                        this.markArray[this.getRawIndex((int)(index + fbUndoMarkCount))] = this.markArray[this.getRawIndex(index)];
                    }
                }
                dirFirstItem = undo.bbItem;
                int bbUndoMarkCount = 0;
                while (dirFirstItem != null) {
                    if ((dirFirstItem.mark.flags & 4) == 0) {
                        ++bbUndoMarkCount;
                        UndoItem item = dirFirstItem.logicalNext;
                        while (item != null) {
                            if ((item.mark.flags & 4) == 0) {
                                ++bbUndoMarkCount;
                            } else {
                                item.mark = null;
                            }
                            item = item.logicalNext;
                        }
                        break;
                    }
                    dirFirstItem.mark = null;
                    dirFirstItem = dirFirstItem.logicalNext;
                }
                if (dirFirstItem != null) {
                    int index;
                    MultiMark firstItemMark = dirFirstItem.mark;
                    for (index = offsetGapIndex - 1; index >= 0 && this.markArray[this.getRawIndex(index)] != firstItemMark; --index) {
                    }
                    ++index;
                    while (index < offsetGapIndex) {
                        this.markArray[this.getRawIndex((int)(index - bbUndoMarkCount))] = this.markArray[this.getRawIndex(index)];
                        ++index;
                    }
                }
                UndoItem origItem = undo.firstItem;
                offsetGapIndex -= bbUndoMarkCount;
                while (origItem != null) {
                    MultiMark mark = origItem.mark;
                    if (mark != null) {
                        mark.rawOffset = origItem.undoOffset;
                        this.markArray[this.getRawIndex((int)offsetGapIndex++)] = mark;
                    }
                    origItem = origItem.next;
                }
                if (offset == 0) {
                    ZeroUndoItem zeroItem = undo.zeroItem;
                    while (zeroItem != null) {
                        MultiMark mark = zeroItem.mark;
                        if ((mark.flags & 4) == 0) {
                            mark.flags &= -17;
                        }
                        zeroItem = zeroItem.next;
                    }
                }
            }
            undo = null;
        } else {
            UndoItem item = null;
            UndoItem fbItem = null;
            UndoItem bbItem = null;
            UndoItem upperBBItem = null;
            int upperBBMIndex = -1;
            int offsetAboveGap = (offset += length) + this.offsetGapLength;
            ZeroUndoItem zeroItem = null;
            if (offset == 0) {
                int offsetGapIndexCopy = offsetGapIndex;
                int markCount = this.getMarkCount();
                while (offsetGapIndexCopy < markCount) {
                    MultiMark mark = this.markArray[this.getRawIndex(offsetGapIndexCopy++)];
                    if (mark.rawOffset != offsetAboveGap) break;
                    if ((mark.flags & 24) != 8) continue;
                    mark.flags |= 16;
                    zeroItem = new ZeroUndoItem(mark, zeroItem);
                }
            }
            while (offsetGapIndex > 0) {
                boolean backwardBias;
                MultiMark mark = this.markArray[this.getRawIndex(--offsetGapIndex)];
                int markOffset = mark.rawOffset;
                boolean bl = backwardBias = (mark.flags & 1) != 0;
                if (markOffset < offset || mark.rawOffset == offset && backwardBias) break;
                item = new UndoItem(mark, markOffset, item);
                if (backwardBias) {
                    if (bbItem != null) {
                        bbItem.logicalNext = item;
                    } else {
                        upperBBItem = item;
                        upperBBMIndex = offsetGapIndex;
                    }
                    bbItem = item;
                    mark.rawOffset = offset;
                    continue;
                }
                item.logicalNext = fbItem;
                fbItem = item;
                mark.rawOffset = offsetAboveGap;
                if (upperBBMIndex < 0) continue;
                int upperBBMRawIndex = this.getRawIndex(upperBBMIndex--);
                this.markArray[this.getRawIndex((int)offsetGapIndex)] = this.markArray[upperBBMRawIndex];
                this.markArray[upperBBMRawIndex] = mark;
                UndoItem upperNext = upperBBItem.logicalNext;
                if (upperNext == null) continue;
                bbItem.logicalNext = upperBBItem;
                bbItem = upperBBItem;
                upperBBItem.logicalNext = null;
                upperBBItem = upperNext;
            }
            if (offset == 0 && item != null) {
                UndoItem i = item;
                while (i != null) {
                    MultiMark mark = i.mark;
                    if ((mark.flags & 24) == 8) {
                        mark.flags |= 16;
                        zeroItem = new ZeroUndoItem(mark, zeroItem);
                    }
                    i = i.next;
                }
            }
            undo = item != null || zeroItem != null ? new Undo(item, fbItem, upperBBItem, zeroItem) : null;
        }
        return undo;
    }

    private void removeDisposedMarks() {
        int rawIndex;
        MultiMark mark;
        int validInd = -1;
        int gapEnd = this.gapStart + this.gapLength;
        for (rawIndex = 0; rawIndex < this.gapStart; ++rawIndex) {
            mark = this.markArray[rawIndex];
            if ((mark.flags & 2) != 0) {
                if (rawIndex == ++validInd) continue;
                this.markArray[validInd] = mark;
                continue;
            }
            mark.flags |= 4;
        }
        this.gapStart = validInd + 1;
        validInd = rawIndex = this.markArray.length;
        while (--rawIndex >= gapEnd) {
            mark = this.markArray[rawIndex];
            if ((mark.flags & 2) != 0) {
                if (rawIndex == --validInd) continue;
                this.markArray[validInd] = mark;
                continue;
            }
            mark.flags |= 4;
        }
        this.gapLength = validInd - this.gapStart;
        this.disposedMarkCount = 0;
    }

    synchronized int getOffset(int rawOffset) {
        return rawOffset <= this.offsetGapStart ? rawOffset : rawOffset - this.offsetGapLength;
    }

    private int getRawIndex(int index) {
        return index < this.gapStart ? index : index + this.gapLength;
    }

    private int findInsertIndex(int offset) {
        int low = 0;
        int high = this.getMarkCount() - 1;
        while (low <= high) {
            int index = (low + high) / 2;
            MultiMark mark = this.markArray[this.getRawIndex(index)];
            int markOffset = this.getOffset(mark.rawOffset);
            if (markOffset < offset) {
                low = index + 1;
                continue;
            }
            if (markOffset > offset) {
                high = index - 1;
                continue;
            }
            if ((mark.flags & 1) != 0) {
                low = index + 1;
                continue;
            }
            high = index - 1;
        }
        return low;
    }

    private void moveGap(int index) {
        if (index <= this.gapStart) {
            int moveSize = this.gapStart - index;
            System.arraycopy(this.markArray, index, this.markArray, this.gapStart + this.gapLength - moveSize, moveSize);
            this.gapStart = index;
        } else {
            int moveSize = index - this.gapStart;
            System.arraycopy(this.markArray, this.gapStart + this.gapLength, this.markArray, this.gapStart, moveSize);
            this.gapStart += moveSize;
        }
    }

    private void moveOffsetGap(int index, int newOffsetGapStart) {
        int rawIndex = this.getRawIndex(index);
        int markArrayLength = this.markArray.length;
        int offset = this.offsetGapStart;
        this.offsetGapStart = newOffsetGapStart;
        int length = this.offsetGapLength;
        if (rawIndex == markArrayLength || this.markArray[rawIndex].rawOffset > offset) {
            int bound = rawIndex < this.gapStart ? 0 : this.gapStart + this.gapLength;
            boolean done = false;
            while (!done) {
                while (--rawIndex >= bound) {
                    MultiMark mark = this.markArray[rawIndex];
                    if (mark.rawOffset > offset) {
                        mark.rawOffset -= length;
                        continue;
                    }
                    done = true;
                    break;
                }
                if (bound > 0) {
                    bound = 0;
                    rawIndex = this.gapStart;
                    continue;
                }
                done = true;
            }
        } else {
            int bound = rawIndex < this.gapStart ? this.gapStart : markArrayLength;
            boolean done = false;
            while (!done) {
                while (rawIndex < bound) {
                    MultiMark mark = this.markArray[rawIndex];
                    if (mark.rawOffset <= offset) {
                        mark.rawOffset += length;
                    } else {
                        done = true;
                        break;
                    }
                    ++rawIndex;
                }
                if (bound < markArrayLength) {
                    bound = markArrayLength;
                    rawIndex += this.gapLength;
                    continue;
                }
                done = true;
            }
        }
    }

    private void enlargeGap(int extraLength) {
        int newLength = Math.max(8, this.markArray.length * 3 / 2 + extraLength);
        int gapEnd = this.gapStart + this.gapLength;
        int afterGapLength = this.markArray.length - gapEnd;
        int newGapEnd = newLength - afterGapLength;
        MultiMark[] newMarkArray = new MultiMark[newLength];
        System.arraycopy(this.markArray, 0, newMarkArray, 0, this.gapStart);
        System.arraycopy(this.markArray, gapEnd, newMarkArray, newGapEnd, afterGapLength);
        this.markArray = newMarkArray;
        this.gapLength = newGapEnd - this.gapStart;
    }

    public String toString() {
        return "markCount=" + this.getMarkCount() + ", gapStart=" + this.gapStart + ", gapLength=" + this.gapLength + ", offsetGapStart=" + this.offsetGapStart + ", offsetGapLength=" + this.offsetGapLength;
    }

    static final class ZeroUndoItem {
        final MultiMark mark;
        final ZeroUndoItem next;

        ZeroUndoItem(MultiMark mark, ZeroUndoItem next) {
            this.mark = mark;
            this.next = next;
        }
    }

    static final class UndoItem {
        MultiMark mark;
        int undoOffset;
        UndoItem next;
        UndoItem logicalNext;

        UndoItem(MultiMark mark, int undoOffset, UndoItem next) {
            this.mark = mark;
            this.undoOffset = undoOffset;
            this.next = next;
        }
    }

    public static final class Undo {
        UndoItem firstItem;
        UndoItem fbItem;
        UndoItem bbItem;
        ZeroUndoItem zeroItem;

        Undo(UndoItem firstItem, UndoItem fbItem, UndoItem bbItem, ZeroUndoItem zeroItem) {
            this.firstItem = firstItem;
            this.fbItem = fbItem;
            this.bbItem = bbItem;
            this.zeroItem = zeroItem;
        }
    }

}

