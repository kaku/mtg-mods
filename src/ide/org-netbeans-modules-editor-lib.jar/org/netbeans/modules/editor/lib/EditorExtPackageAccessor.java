/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib;

import org.netbeans.editor.EditorUI;
import org.netbeans.editor.ext.ToolTipSupport;

public abstract class EditorExtPackageAccessor {
    private static EditorExtPackageAccessor ACCESSOR = null;

    public static synchronized void register(EditorExtPackageAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized EditorExtPackageAccessor get() {
        try {
            Class clazz = Class.forName(ToolTipSupport.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected EditorExtPackageAccessor() {
    }

    public abstract ToolTipSupport createToolTipSupport(EditorUI var1);
}

