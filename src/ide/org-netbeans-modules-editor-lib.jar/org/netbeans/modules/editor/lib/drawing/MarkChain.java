/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import javax.swing.text.BadLocationException;
import javax.swing.text.Position;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.lib.EditorPackageAccessor;
import org.netbeans.modules.editor.lib.drawing.ChainDrawMark;

public final class MarkChain {
    protected ChainDrawMark chain;
    protected ChainDrawMark curMark;
    protected BaseDocument doc;
    protected String layerName;
    private ChainDrawMark recentlyAddedMark;

    public MarkChain(BaseDocument doc, String layerName) {
        this.doc = doc;
        this.layerName = layerName;
    }

    public final ChainDrawMark getChain() {
        return this.chain;
    }

    public final ChainDrawMark getCurMark() {
        return this.curMark;
    }

    public int compareMark(int pos) {
        try {
            int rel;
            if (this.curMark == null) {
                this.curMark = this.chain;
                if (this.curMark == null) {
                    return -1;
                }
            }
            boolean after = false;
            boolean before = false;
            while ((rel = this.curMark.compare(pos)) != 0) {
                if (rel > 0) {
                    if (before) {
                        return rel;
                    }
                    if (this.curMark.prev != null) {
                        after = true;
                        this.curMark = this.curMark.prev;
                        continue;
                    }
                    return rel;
                }
                if (after) {
                    return rel;
                }
                if (this.curMark.next != null) {
                    before = true;
                    this.curMark = this.curMark.next;
                    continue;
                }
                return rel;
            }
            return 0;
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
            return -1;
        }
    }

    protected ChainDrawMark createAndInsertNewMark(int pos) throws BadLocationException {
        ChainDrawMark mark = this.createMark();
        try {
            EditorPackageAccessor.get().Mark_insert(mark, this.doc, pos);
        }
        catch (InvalidMarkException e) {
            Utilities.annotateLoggable(e);
        }
        return mark;
    }

    protected ChainDrawMark createMark() {
        ChainDrawMark mark = new ChainDrawMark(this.layerName, null, Position.Bias.Backward);
        mark.activateLayer = true;
        return mark;
    }

    public boolean addMark(int pos) throws BadLocationException {
        return this.addMark(pos, false);
    }

    /*
     * Enabled aggressive block sorting
     */
    public boolean addMark(int pos, boolean forceAdd) throws BadLocationException {
        ChainDrawMark mark;
        int rel = this.compareMark(pos);
        if (rel == 0) {
            ChainDrawMark mark2;
            if (!forceAdd) {
                this.recentlyAddedMark = this.curMark;
                return false;
            }
            this.recentlyAddedMark = mark2 = this.createAndInsertNewMark(pos);
            if (this.curMark == this.chain) {
                this.chain = this.curMark.insertChain(mark2);
                return true;
            }
            this.curMark.insertChain(mark2);
            return true;
        }
        if (rel > 0) {
            ChainDrawMark mark3;
            this.recentlyAddedMark = mark3 = this.createAndInsertNewMark(pos);
            if (this.curMark == null) {
                this.chain = mark3;
                return true;
            }
            if (this.curMark == this.chain) {
                this.chain = this.curMark.insertChain(mark3);
                return true;
            }
            this.curMark.insertChain(mark3);
            return true;
        }
        this.recentlyAddedMark = mark = this.createAndInsertNewMark(pos);
        if (this.curMark == null) {
            this.chain = mark;
            return true;
        }
        if (this.curMark.next != null) {
            this.curMark.next.insertChain(mark);
            return true;
        }
        this.curMark.setNextChain(mark);
        return true;
    }

    public ChainDrawMark getAddedMark() {
        return this.recentlyAddedMark;
    }

    public boolean removeMark(int pos) {
        int rel = this.compareMark(pos);
        if (rel == 0) {
            boolean first = this.curMark == this.chain;
            this.curMark = this.curMark.removeChain();
            if (first) {
                this.chain = this.curMark;
            }
            return true;
        }
        return false;
    }

    public boolean removeMark(ChainDrawMark mark) {
        if (mark == null) {
            throw new NullPointerException();
        }
        if (this.curMark != mark) {
            this.curMark = this.chain;
            while (this.curMark != null && this.curMark != mark) {
                this.curMark = this.curMark.next;
            }
        }
        if (this.curMark != null) {
            boolean first = this.curMark == this.chain;
            this.curMark = this.curMark.removeChain();
            if (first) {
                this.chain = this.curMark;
            }
            return true;
        }
        return false;
    }

    public boolean isMark(int pos) {
        return this.compareMark(pos) == 0;
    }

    public boolean toggleMark(int pos) throws BadLocationException {
        int rel = this.compareMark(pos);
        if (rel == 0) {
            this.removeMark(pos);
            return false;
        }
        this.addMark(pos);
        return true;
    }

    public String toString() {
        return "MarkChain: curMark=" + this.curMark + ", mark chain: " + (this.chain != null ? new StringBuilder().append("\n").append(this.chain.toStringChain()).toString() : "Empty");
    }
}

