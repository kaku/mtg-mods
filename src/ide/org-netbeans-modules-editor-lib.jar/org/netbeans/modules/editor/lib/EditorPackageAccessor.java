/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.indent.api.Reformat
 */
package org.netbeans.modules.editor.lib;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.undo.UndoableEdit;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.modules.editor.indent.api.Reformat;
import org.netbeans.modules.editor.lib.impl.MarkVector;
import org.netbeans.modules.editor.lib.impl.MultiMark;

public abstract class EditorPackageAccessor {
    private static EditorPackageAccessor ACCESSOR = null;

    public static synchronized void register(EditorPackageAccessor accessor) {
        assert (ACCESSOR == null);
        ACCESSOR = accessor;
    }

    public static synchronized EditorPackageAccessor get() {
        try {
            Class clazz = Class.forName(BaseDocument.class.getName());
        }
        catch (ClassNotFoundException e) {
            // empty catch block
        }
        assert (ACCESSOR != null);
        return ACCESSOR;
    }

    protected EditorPackageAccessor() {
    }

    public abstract UndoableEdit BaseDocument_markAtomicEditsNonSignificant(BaseDocument var1);

    public abstract void BaseDocument_clearAtomicEdits(BaseDocument var1);

    public abstract MarkVector BaseDocument_getMarksStorage(BaseDocument var1);

    public abstract Mark BaseDocument_getMark(BaseDocument var1, MultiMark var2);

    public abstract void Mark_insert(Mark var1, BaseDocument var2, int var3) throws InvalidMarkException, BadLocationException;

    public abstract void ActionFactory_reformat(Reformat var1, Document var2, int var3, int var4, AtomicBoolean var5) throws BadLocationException;
}

