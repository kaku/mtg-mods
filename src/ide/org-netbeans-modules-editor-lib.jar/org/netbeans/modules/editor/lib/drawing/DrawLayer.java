/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import org.netbeans.modules.editor.lib.drawing.DrawContext;
import org.netbeans.modules.editor.lib.drawing.DrawMark;

interface DrawLayer {
    public static final String TEXT_FRAME_START_POSITION_COMPONENT_PROPERTY = "text-frame-start-position";
    public static final String TEXT_FRAME_END_POSITION_COMPONENT_PROPERTY = "text-frame-end-position";

    public String getName();

    public boolean extendsEOL();

    public boolean extendsEmptyLine();

    public int getNextActivityChangeOffset(DrawContext var1);

    public void init(DrawContext var1);

    public boolean isActive(DrawContext var1, DrawMark var2);

    public void updateContext(DrawContext var1);

    public int updateLineNumberContext(int var1, DrawContext var2);

    public static abstract class AbstractLayer
    implements DrawLayer {
        private String name;
        int nextActivityChangeOffset = Integer.MAX_VALUE;

        public AbstractLayer(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public boolean extendsEOL() {
            return false;
        }

        @Override
        public boolean extendsEmptyLine() {
            return false;
        }

        @Override
        public int getNextActivityChangeOffset(DrawContext ctx) {
            return this.nextActivityChangeOffset;
        }

        public void setNextActivityChangeOffset(int nextActivityChangeOffset) {
            this.nextActivityChangeOffset = nextActivityChangeOffset;
        }

        @Override
        public void init(DrawContext ctx) {
        }

        @Override
        public int updateLineNumberContext(int lineNumber, DrawContext ctx) {
            return lineNumber;
        }

        public String toString() {
            return "Layer " + this.getClass() + ", name='" + this.name;
        }
    }

}

