/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import javax.swing.text.Position;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Mark;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.modules.editor.lib.drawing.DrawMark;

class LayerChain
extends MarkBlockChain {
    private String layerName;

    public LayerChain(BaseDocument doc, String layerName) {
        super(doc);
        this.layerName = layerName;
    }

    public final String getLayerName() {
        return this.layerName;
    }

    @Override
    protected Mark createBlockStartMark() {
        DrawMark startMark = new DrawMark(this.layerName, null);
        startMark.activateLayer = true;
        return startMark;
    }

    @Override
    protected Mark createBlockEndMark() {
        DrawMark endMark = new DrawMark(this.layerName, null, Position.Bias.Backward);
        return endMark;
    }
}

