/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.impl;

import javax.swing.text.Position;
import org.netbeans.modules.editor.lib.impl.MultiMark;

public final class BasePosition
implements Position {
    private MultiMark mark;

    @Override
    public int getOffset() {
        return this.mark.getOffset();
    }

    void setMark(MultiMark mark) {
        this.mark = mark;
    }

    public String toString() {
        return super.toString() + " offset=" + this.getOffset();
    }
}

