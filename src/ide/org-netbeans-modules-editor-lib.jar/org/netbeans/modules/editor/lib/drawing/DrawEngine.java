/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Segment;
import javax.swing.text.View;
import org.netbeans.editor.Analyzer;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorDebug;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.FontMetricsCache;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.Mark;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.editor.lib.EditorPackageAccessor;
import org.netbeans.modules.editor.lib.drawing.DrawContext;
import org.netbeans.modules.editor.lib.drawing.DrawEngineLineView;
import org.netbeans.modules.editor.lib.drawing.DrawGraphics;
import org.netbeans.modules.editor.lib.drawing.DrawLayer;
import org.netbeans.modules.editor.lib.drawing.DrawLayerList;
import org.netbeans.modules.editor.lib.drawing.DrawMark;
import org.netbeans.modules.editor.lib.drawing.EditorUiAccessor;
import org.netbeans.modules.editor.lib.impl.MarkVector;
import org.netbeans.modules.editor.lib.impl.MultiMark;

public final class DrawEngine {
    private static final Logger LOG = Logger.getLogger(DrawEngine.class.getName());
    private static DrawEngine drawEngine;
    private static final char[] SPACE;

    private DrawEngine() {
    }

    public static synchronized DrawEngine getDrawEngine() {
        if (drawEngine == null) {
            drawEngine = new DrawEngine();
        }
        return drawEngine;
    }

    private void initLineNumbering(DrawInfo ctx, EditorUI eui) {
        EditorUiAccessor accessor = EditorUiAccessor.get();
        boolean bl = ctx.lineNumbering = accessor.isLineNumberVisible(eui) && ctx.drawGraphics.supportsLineNumbers();
        if (ctx.lineNumbering) {
            Color lnBackColor;
            Color lnForeColor;
            try {
                ctx.startLineNumber = Utilities.getLineOffset(ctx.doc, ctx.startOffset) + 1;
            }
            catch (BadLocationException e) {
                LOG.log(Level.WARNING, null, e);
            }
            ctx.lineNumberColoring = accessor.getColoring(eui, "line-number");
            ctx.lineNumberColoring = ctx.lineNumberColoring == null ? ctx.defaultColoring : ctx.lineNumberColoring.apply(ctx.defaultColoring);
            Font lnFont = ctx.lineNumberColoring.getFont();
            if (lnFont == null) {
                lnFont = ctx.defaultColoring.getFont();
            }
            if ((lnBackColor = ctx.lineNumberColoring.getBackColor()) == null) {
                lnBackColor = ctx.defaultColoring.getBackColor();
            }
            if ((lnForeColor = ctx.lineNumberColoring.getForeColor()) == null) {
                lnForeColor = ctx.defaultColoring.getForeColor();
            }
            ctx.lineNumberChars = new char[Math.max(accessor.getLineNumberMaxDigitCount(eui), 1)];
            ctx.lineNumberWidth = accessor.getLineNumberWidth(eui);
            ctx.lineNumberDigitWidth = accessor.getLineNumberDigitWidth(eui);
            ctx.lineNumberMargin = accessor.getLineNumberMargin(eui);
            if (ctx.graphics == null) {
                ctx.syncedLineNumbering = true;
            } else {
                try {
                    int endLineNumber = Utilities.getLineOffset(ctx.doc, ctx.endOffset) + 1;
                    ctx.lineStartOffsets = new int[endLineNumber - ctx.startLineNumber + 2];
                }
                catch (BadLocationException e) {
                    LOG.log(Level.WARNING, null, e);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initInfo(DrawInfo ctx, EditorUI eui) throws BadLocationException {
        MarkVector docMarksStorage;
        Map hints;
        if (ctx.startOffset < ctx.lineStartOffset) {
            throw new BadLocationException("Invalid startOffset: " + ctx.startOffset + " < line start offset = " + ctx.lineStartOffset, ctx.startOffset);
        }
        if (ctx.endOffset > ctx.lineEndOffset) {
            throw new BadLocationException("Invalid endOffset: " + ctx.endOffset + " > line end offset = " + ctx.lineEndOffset, ctx.endOffset);
        }
        EditorUiAccessor accessor = EditorUiAccessor.get();
        ctx.x = ctx.startX;
        ctx.y = ctx.startY;
        ctx.lineHeight = accessor.getLineHeight(eui);
        ctx.defaultColoring = accessor.getDefaultColoring(eui);
        ctx.tabSize = ctx.doc.getTabSize();
        ctx.defaultSpaceWidth = accessor.getDefaultSpaceWidth(eui);
        ctx.fragmentOffset = ctx.startOffset;
        ctx.graphics = ctx.drawGraphics.getGraphics();
        if (ctx.graphics != null && (hints = accessor.getRenderingHints(eui)) != null) {
            ((Graphics2D)ctx.graphics).addRenderingHints(hints);
        }
        ctx.textMargin = accessor.getTextMargin(eui);
        ctx.textLeftMarginWidth = accessor.getTextLeftMarginWidth(eui);
        ctx.textLimitLineVisible = accessor.getTextLimitLineVisible(eui);
        ctx.textLimitLineColor = accessor.getTextLimitLineColor(eui);
        ctx.textLimitWidth = accessor.getTextLimitWidth(eui);
        this.initLineNumbering(ctx, eui);
        ctx.foreColor = ctx.defaultColoring.getForeColor();
        ctx.backColor = ctx.defaultColoring.getBackColor();
        ctx.font = ctx.defaultColoring.getFont();
        ctx.bol = ctx.lineStartOffset == ctx.startOffset;
        ctx.drawGraphics.init(ctx);
        ctx.drawGraphics.setDefaultBackColor(ctx.defaultColoring.getBackColor());
        ctx.drawGraphics.setLineHeight(ctx.lineHeight);
        ctx.drawGraphics.setLineAscent(accessor.getLineAscent(eui));
        ctx.drawGraphics.setX(ctx.x);
        ctx.drawGraphics.setY(ctx.y);
        ctx.layers = accessor.getDrawLayerList(eui).currentLayers();
        int layersLength = ctx.layers.length;
        ctx.layerActives = new boolean[layersLength];
        ctx.layerActivityChangeOffsets = new int[layersLength];
        for (int i = 0; i < layersLength; ++i) {
            ctx.layers[i].init(ctx);
        }
        ctx.drawMarkList = new ArrayList<DrawMark>();
        MarkVector markVector = docMarksStorage = EditorPackageAccessor.get().BaseDocument_getMarksStorage(ctx.doc);
        synchronized (markVector) {
            int offset = ctx.startOffset;
            int low = 0;
            int markCount = docMarksStorage.getMarkCount();
            int high = markCount - 1;
            while (low <= high) {
                int mid = low + high >> 1;
                int cmp = docMarksStorage.getMarkOffsetInternal(mid) - offset;
                if (cmp < 0) {
                    low = mid + 1;
                    continue;
                }
                if (cmp > 0) {
                    high = mid - 1;
                    continue;
                }
                while (--mid >= 0 && docMarksStorage.getMarkOffsetInternal(mid) == offset) {
                }
                low = mid + 1;
                break;
            }
            offset = ctx.endOffset;
            while (low < markCount) {
                MultiMark m = docMarksStorage.getMark(low);
                if (m.isValid()) {
                    if (m.getOffset() > offset) break;
                    Mark mark = EditorPackageAccessor.get().BaseDocument_getMark(ctx.doc, m);
                    if (mark == null) {
                        throw new IllegalStateException("No mark for m=" + m);
                    }
                    if (mark instanceof DrawMark) {
                        ctx.drawMarkList.add((DrawMark)mark);
                    }
                }
                ++low;
            }
        }
        ctx.drawMarkUpdate = false;
        ctx.drawMarkOffset = Integer.MAX_VALUE;
        ctx.drawMarkIndex = 0;
        while (ctx.drawMarkIndex < ctx.drawMarkList.size()) {
            ctx.drawMark = ctx.drawMarkList.get(ctx.drawMarkIndex++);
            if (!ctx.drawMark.isValid()) continue;
            try {
                ctx.drawMarkOffset = ctx.drawMark.getOffset();
            }
            catch (InvalidMarkException ime) {
                LOG.log(Level.FINE, null, ime);
                continue;
            }
            if (ctx.drawMarkOffset >= ctx.updateOffset) break;
            ctx.updateOffset = ctx.drawMarkOffset;
            ctx.drawMarkUpdate = true;
            break;
        }
        ctx.text = new Segment();
        ctx.doc.getText(ctx.lineStartOffset, ctx.lineEndOffset - ctx.lineStartOffset, ctx.text);
        ctx.textArray = ctx.text.array;
        ctx.buffer = ctx.textArray;
        ctx.bufferStartOffset = ctx.lineStartOffset - ctx.text.offset;
        ctx.drawGraphics.setBuffer(ctx.textArray);
        if (ctx.drawGraphics instanceof DrawGraphics.GraphicsDG) {
            ((DrawGraphics.GraphicsDG)ctx.drawGraphics).setBufferStartOffset(ctx.bufferStartOffset);
        }
        ctx.continueDraw = true;
        ctx.visualColumn = ctx.bol ? 0 : Analyzer.getColumn(ctx.textArray, ctx.lineStartOffset - ctx.bufferStartOffset, ctx.startOffset - ctx.lineStartOffset, ctx.tabSize, 0);
    }

    private void handleBOL(DrawInfo ctx) {
        if (ctx.lineNumbering) {
            if (ctx.syncedLineNumbering) {
                int i;
                ctx.foreColor = ctx.lineNumberColoring.getForeColor();
                ctx.backColor = ctx.lineNumberColoring.getBackColor();
                ctx.font = ctx.lineNumberColoring.getFont();
                ctx.strikeThroughColor = null;
                ctx.underlineColor = null;
                ctx.waveUnderlineColor = null;
                ctx.topBorderLineColor = null;
                ctx.rightBorderLineColor = null;
                ctx.bottomBorderLineColor = null;
                ctx.leftBorderLineColor = null;
                int lineNumber = ctx.startLineNumber + ctx.lineIndex;
                int layersLength = ctx.layers.length;
                for (i = 0; i < layersLength; ++i) {
                    lineNumber = ctx.layers[i].updateLineNumberContext(lineNumber, ctx);
                }
                i = Math.max(ctx.lineNumberChars.length - 1, 0);
                do {
                    ctx.lineNumberChars[i--] = (char)(48 + lineNumber % 10);
                } while ((lineNumber /= 10) != 0 && i >= 0);
                while (i >= 0) {
                    ctx.lineNumberChars[i--] = 32;
                }
                int numX = ctx.x - ctx.lineNumberWidth;
                if (ctx.lineNumberMargin != null) {
                    numX += ctx.lineNumberMargin.left;
                }
                ctx.drawGraphics.setX(numX);
                ctx.drawGraphics.setBuffer(ctx.lineNumberChars);
                ctx.drawGraphics.setForeColor(ctx.foreColor);
                ctx.drawGraphics.setBackColor(ctx.backColor);
                ctx.drawGraphics.setStrikeThroughColor(ctx.strikeThroughColor);
                ctx.drawGraphics.setUnderlineColor(ctx.underlineColor);
                ctx.drawGraphics.setWaveUnderlineColor(ctx.waveUnderlineColor);
                ctx.drawGraphics.setTopBorderLineColor(ctx.topBorderLineColor);
                ctx.drawGraphics.setRightBorderLineColor(ctx.rightBorderLineColor);
                ctx.drawGraphics.setBottomBorderLineColor(ctx.bottomBorderLineColor);
                ctx.drawGraphics.setLeftBorderLineColor(ctx.leftBorderLineColor);
                ctx.drawGraphics.setFont(ctx.font);
                ctx.drawGraphics.drawChars(0, ctx.lineNumberChars.length, ctx.lineNumberWidth);
                if (ctx.drawGraphics.getGraphics() == null) {
                    ctx.drawGraphics.setBuffer(SPACE);
                    ctx.drawGraphics.drawChars(0, 1, ctx.lineNumberDigitWidth);
                }
                ctx.drawGraphics.setX(ctx.x);
                ctx.drawGraphics.setBuffer(ctx.textArray);
            } else {
                ctx.lineStartOffsets[ctx.lineIndex] = ctx.fragmentOffset;
            }
        }
        ++ctx.lineIndex;
    }

    private void handleEOL(DrawInfo ctx) {
        ctx.drawGraphics.setX(ctx.x);
        ctx.drawGraphics.setY(ctx.y);
        ctx.drawGraphics.eol();
        ctx.visualColumn = 0;
        ctx.x = ctx.startX;
        ctx.y += ctx.lineHeight;
    }

    private void updateOffsetReached(DrawInfo ctx) {
        if (ctx.drawMarkUpdate) {
            for (DrawLayer l : ctx.layers) {
                int naco;
                if (!l.getName().equals(ctx.drawMark.layerName) || !ctx.drawMark.isDocumentMark() && ctx.editorUI != ctx.drawMark.getEditorUI()) continue;
                ctx.layerActives[i] = l.isActive(ctx, ctx.drawMark);
                ctx.layerActivityChangeOffsets[i] = naco = l.getNextActivityChangeOffset(ctx);
                if (naco <= ctx.fragmentOffset || naco >= ctx.layerUpdateOffset) continue;
                ctx.layerUpdateOffset = naco;
            }
            while (ctx.drawMarkIndex < ctx.drawMarkList.size()) {
                ctx.drawMark = ctx.drawMarkList.get(ctx.drawMarkIndex++);
                if (!ctx.drawMark.isValid()) continue;
                try {
                    ctx.drawMarkOffset = ctx.drawMark.getOffset();
                    break;
                }
                catch (InvalidMarkException ime) {
                    LOG.log(Level.FINE, null, ime);
                    continue;
                }
            }
            if (ctx.drawMarkIndex >= ctx.drawMarkList.size()) {
                ctx.drawMark = null;
                ctx.drawMarkOffset = Integer.MAX_VALUE;
            }
        } else {
            ctx.layerUpdateOffset = Integer.MAX_VALUE;
            int layersLength = ctx.layers.length;
            for (int i = 0; i < layersLength; ++i) {
                int naco = ctx.layerActivityChangeOffsets[i];
                if (naco == ctx.fragmentOffset) {
                    DrawLayer l = ctx.layers[i];
                    ctx.layerActives[i] = l.isActive(ctx, null);
                    ctx.layerActivityChangeOffsets[i] = naco = l.getNextActivityChangeOffset(ctx);
                }
                if (naco <= ctx.fragmentOffset || naco >= ctx.layerUpdateOffset) continue;
                ctx.layerUpdateOffset = naco;
            }
        }
        if (ctx.drawMarkOffset < ctx.layerUpdateOffset) {
            ctx.drawMarkUpdate = true;
            ctx.updateOffset = ctx.drawMarkOffset;
        } else {
            ctx.drawMarkUpdate = false;
            ctx.updateOffset = ctx.layerUpdateOffset;
        }
    }

    private void computeFragmentLength(DrawInfo ctx) {
        ctx.fragmentStartIndex = ctx.fragmentOffset - ctx.bufferStartOffset;
        ctx.fragmentLength = Math.min(ctx.updateOffset - ctx.fragmentOffset, ctx.areaLength - ctx.drawnLength);
        int stopIndex = Analyzer.findFirstTabOrLF(ctx.textArray, ctx.fragmentStartIndex, ctx.fragmentLength);
        ctx.eol = ctx.fragmentOffset == ctx.docLen;
        ctx.tabsFragment = false;
        if (stopIndex >= 0) {
            if (stopIndex == ctx.fragmentStartIndex) {
                if (ctx.textArray[stopIndex] == '\t') {
                    ctx.tabsFragment = true;
                    int ntInd = Analyzer.findFirstNonTab(ctx.textArray, ctx.fragmentStartIndex, ctx.fragmentLength);
                    if (ntInd != -1) {
                        ctx.fragmentLength = ntInd - ctx.fragmentStartIndex;
                    }
                } else {
                    ctx.eol = true;
                    ctx.fragmentLength = 1;
                }
            } else {
                ctx.fragmentLength = stopIndex - ctx.fragmentStartIndex;
            }
        }
    }

    private void computeFragmentDisplayWidth(DrawInfo ctx) {
        if (!ctx.eol) {
            int layersLength = ctx.layers.length;
            for (int i = 0; i < layersLength; ++i) {
                if (!ctx.layerActives[i]) continue;
                ctx.layers[i].updateContext(ctx);
            }
        }
        FontMetricsCache.Info fmcInfo = FontMetricsCache.getInfo(ctx.font);
        ctx.spaceWidth = ctx.component != null ? fmcInfo.getSpaceWidth(ctx.component) : ctx.defaultSpaceWidth;
        ctx.fragmentCharCount = ctx.fragmentLength;
        if (ctx.tabsFragment) {
            ctx.fragmentCharCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, ctx.fragmentLength, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
            ctx.fragmentWidth = ctx.fragmentCharCount * ctx.spaceWidth;
        } else {
            ctx.fragmentWidth = ctx.eol ? ctx.spaceWidth : (ctx.fragmentLength > 0 ? (ctx.component != null ? FontMetricsCache.getFontMetrics(ctx.font, ctx.component).charsWidth(ctx.textArray, ctx.fragmentStartIndex, ctx.fragmentLength) : ctx.fragmentLength * ctx.spaceWidth) : 0);
        }
    }

    private void drawItNow(DrawInfo ctx) {
        if (ctx.eol) {
            int layersLength = ctx.layers.length;
            boolean emptyLine = false;
            int blankWidth = ctx.fragmentWidth;
            do {
                blankWidth = 0;
                if (ctx.bol) {
                    if (!emptyLine) {
                        for (int i = 0; i < layersLength; ++i) {
                            DrawLayer l;
                            if (!ctx.layerActives[i] || !(l = ctx.layers[i]).extendsEmptyLine()) continue;
                            emptyLine = true;
                            l.updateContext(ctx);
                        }
                        if (emptyLine) {
                            blankWidth = ctx.spaceWidth / 2;
                        }
                    } else {
                        emptyLine = false;
                    }
                }
                if (!emptyLine) {
                    boolean extendEOL = false;
                    for (int i = 0; i < layersLength; ++i) {
                        DrawLayer l;
                        if (!ctx.layerActives[i] || !(l = ctx.layers[i]).extendsEOL()) continue;
                        extendEOL = true;
                        l.updateContext(ctx);
                    }
                    if (extendEOL && ctx.component != null) {
                        ctx.drawGraphics.setStrikeThroughColor(null);
                        ctx.drawGraphics.setUnderlineColor(null);
                        ctx.drawGraphics.setWaveUnderlineColor(null);
                        ctx.drawGraphics.setTopBorderLineColor(null);
                        ctx.drawGraphics.setRightBorderLineColor(null);
                        ctx.drawGraphics.setBottomBorderLineColor(null);
                        ctx.drawGraphics.setLeftBorderLineColor(null);
                        blankWidth = ctx.component.getWidth();
                    }
                }
                if (blankWidth <= 0 || !(ctx.drawGraphics instanceof DrawGraphics.GraphicsDG)) continue;
                if (LOG.isLoggable(Level.FINE)) {
                    DrawGraphics.GraphicsDG dg = (DrawGraphics.GraphicsDG)ctx.drawGraphics;
                    String msg = "fillRect: bg = " + ctx.backColor + ", startOffset = " + ctx.startOffset + ", endOffset = " + ctx.endOffset + ", [" + dg.x + ", " + dg.y + ", " + blankWidth + ", " + dg.lineHeight + "]";
                    LOG.fine(msg);
                }
                ctx.drawGraphics.setBackColor(ctx.backColor);
                ctx.drawGraphics.fillRect(blankWidth);
                if (!emptyLine || ctx.x > ctx.textMargin.left) continue;
                ctx.x += blankWidth;
            } while (emptyLine);
        } else {
            ctx.drawGraphics.setBackColor(ctx.backColor);
            ctx.drawGraphics.setForeColor(ctx.foreColor);
            ctx.drawGraphics.setStrikeThroughColor(ctx.strikeThroughColor);
            ctx.drawGraphics.setUnderlineColor(ctx.underlineColor);
            ctx.drawGraphics.setWaveUnderlineColor(ctx.waveUnderlineColor);
            ctx.drawGraphics.setTopBorderLineColor(ctx.topBorderLineColor);
            ctx.drawGraphics.setRightBorderLineColor(ctx.rightBorderLineColor);
            ctx.drawGraphics.setBottomBorderLineColor(ctx.bottomBorderLineColor);
            ctx.drawGraphics.setLeftBorderLineColor(ctx.leftBorderLineColor);
            ctx.drawGraphics.setFont(ctx.font);
            if (ctx.tabsFragment) {
                ctx.drawGraphics.drawTabs(ctx.fragmentStartIndex, ctx.fragmentLength, ctx.fragmentCharCount, ctx.fragmentWidth);
            } else {
                ctx.drawGraphics.drawChars(ctx.fragmentStartIndex, ctx.fragmentLength, ctx.fragmentWidth);
            }
        }
    }

    private void checkTargetOffsetReached(DrawInfo ctx) {
        if (ctx.eol && (ctx.targetOffset == ctx.fragmentOffset || ctx.targetOffset == -1)) {
            char ch = '\n';
            ctx.continueDraw = ctx.drawGraphics.targetOffsetReached(ctx.fragmentOffset, ch, ctx.x, ctx.spaceWidth, ctx);
        } else if (ctx.targetOffset == -1 && ctx.fragmentLength > 0) {
            int high;
            int lastWidth;
            int lastWidthP1;
            FontMetrics fm = FontMetricsCache.getFontMetrics(ctx.font, ctx.component);
            int low = -1;
            int lastMid = high = ctx.fragmentLength - 1;
            if (ctx.tabsFragment) {
                int spaceCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, high, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
                lastWidth = spaceCount * ctx.spaceWidth;
            } else {
                lastWidth = fm.charsWidth(ctx.textArray, ctx.fragmentStartIndex, high);
            }
            if (ctx.tabsFragment) {
                int spaceCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, ctx.fragmentLength, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
                lastWidthP1 = spaceCount * ctx.spaceWidth;
            } else {
                lastWidthP1 = fm.charsWidth(ctx.textArray, ctx.fragmentStartIndex, ctx.fragmentLength);
            }
            ctx.continueDraw = ctx.drawGraphics.targetOffsetReached(ctx.fragmentOffset + high, ctx.textArray[ctx.fragmentStartIndex + high], ctx.x + lastWidth, lastWidthP1 - lastWidth, ctx);
            if (!ctx.continueDraw) {
                while (low <= high) {
                    int mid = (low + high) / 2;
                    int width = 0;
                    if (mid == lastMid + 1) {
                        width = lastWidthP1;
                    } else if (ctx.tabsFragment) {
                        int spaceCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, mid, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
                        width = spaceCount * ctx.spaceWidth;
                    } else {
                        width = fm.charsWidth(ctx.textArray, ctx.fragmentStartIndex, mid);
                    }
                    int widthP1 = 0;
                    if (mid == lastMid - 1) {
                        widthP1 = lastWidth;
                    } else if (ctx.tabsFragment) {
                        int spaceCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, mid + 1, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
                        widthP1 = spaceCount * ctx.spaceWidth;
                    } else {
                        widthP1 = fm.charsWidth(ctx.textArray, ctx.fragmentStartIndex, mid + 1);
                    }
                    lastWidth = width;
                    lastWidthP1 = widthP1;
                    lastMid = mid;
                    ctx.continueDraw = ctx.drawGraphics.targetOffsetReached(ctx.fragmentOffset + mid, ctx.textArray[ctx.fragmentStartIndex + mid], ctx.x + width, widthP1 - width, ctx);
                    if (ctx.continueDraw) {
                        low = mid + 1;
                        continue;
                    }
                    if (mid > low && mid != high) {
                        high = mid;
                        continue;
                    }
                    break;
                }
            }
        } else if (ctx.targetOffset < ctx.fragmentOffset + ctx.fragmentLength && ctx.fragmentOffset <= ctx.targetOffset) {
            int curWidth;
            int spaceCount;
            int prevWidth = 0;
            int i = ctx.targetOffset - ctx.fragmentOffset;
            if (i > 0) {
                if (ctx.tabsFragment) {
                    spaceCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, i, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
                    prevWidth = spaceCount * ctx.spaceWidth;
                } else {
                    prevWidth = FontMetricsCache.getFontMetrics(ctx.font, ctx.component).charsWidth(ctx.textArray, ctx.fragmentStartIndex, i);
                }
            }
            if (ctx.tabsFragment) {
                spaceCount = Analyzer.getColumn(ctx.textArray, ctx.fragmentStartIndex, i + 1, ctx.tabSize, ctx.visualColumn) - ctx.visualColumn;
                curWidth = spaceCount * ctx.spaceWidth;
            } else {
                curWidth = FontMetricsCache.getFontMetrics(ctx.font, ctx.component).charsWidth(ctx.textArray, ctx.fragmentStartIndex, i + 1);
            }
            ctx.continueDraw = ctx.drawGraphics.targetOffsetReached(ctx.fragmentOffset + i, ctx.textArray[ctx.fragmentStartIndex + i], ctx.x + prevWidth, curWidth - prevWidth, ctx);
        }
    }

    private void drawFragment(DrawInfo ctx) {
        ctx.foreColor = ctx.defaultColoring.getForeColor();
        ctx.backColor = ctx.defaultColoring.getBackColor();
        ctx.font = ctx.defaultColoring.getFont();
        ctx.strikeThroughColor = null;
        ctx.underlineColor = null;
        ctx.waveUnderlineColor = null;
        ctx.topBorderLineColor = null;
        ctx.rightBorderLineColor = null;
        ctx.bottomBorderLineColor = null;
        ctx.leftBorderLineColor = null;
        if (ctx.bol) {
            Color fg = ctx.foreColor;
            Color bg = ctx.backColor;
            this.handleBOL(ctx);
            ctx.foreColor = fg;
            ctx.backColor = bg;
        }
        while (ctx.fragmentOffset == ctx.updateOffset) {
            this.updateOffsetReached(ctx);
        }
        this.computeFragmentLength(ctx);
        this.computeFragmentDisplayWidth(ctx);
        this.drawItNow(ctx);
        if (LOG.isLoggable(Level.FINER)) {
            String msg = "   FRAGMENT='" + EditorDebug.debugChars(ctx.buffer, ctx.fragmentStartIndex, ctx.fragmentLength) + "', at offset=" + ctx.fragmentOffset + ", bol=" + ctx.bol + ", eol=" + ctx.eol;
            LOG.finer(msg);
        }
        if (ctx.component != null) {
            this.checkTargetOffsetReached(ctx);
        }
        ctx.fragmentOffset += ctx.fragmentLength;
        ctx.drawnLength += ctx.fragmentLength;
        ctx.visualColumn += ctx.fragmentCharCount;
        ctx.x += ctx.fragmentWidth;
        ctx.bol = false;
        if (ctx.eol) {
            this.handleEOL(ctx);
            ctx.bol = true;
            ctx.eol = false;
        }
        if (ctx.fragmentOffset >= ctx.endOffset && ctx.endOffset <= ctx.docLen) {
            ctx.continueDraw = false;
        }
    }

    private void drawArea(DrawInfo ctx) {
        ctx.areaOffset = ctx.startOffset;
        ctx.areaLength = ctx.endOffset - ctx.startOffset;
        ctx.layerUpdateOffset = Integer.MAX_VALUE;
        for (DrawLayer l : ctx.layers) {
            int naco;
            ctx.layerActives[i] = l.isActive(ctx, null);
            ctx.layerActivityChangeOffsets[i] = naco = l.getNextActivityChangeOffset(ctx);
            if (naco <= ctx.fragmentOffset || naco >= ctx.layerUpdateOffset) continue;
            ctx.layerUpdateOffset = naco;
        }
        ctx.updateOffset = Math.min(ctx.layerUpdateOffset, ctx.drawMarkOffset);
        ctx.drawnLength = 0;
        ctx.fragmentLength = 0;
        do {
            this.drawFragment(ctx);
        } while (ctx.continueDraw && ctx.drawnLength < ctx.areaLength);
        if (ctx.continueDraw) {
            ctx.areaOffset = ctx.fragmentOffset;
            ctx.areaLength = ctx.docLen - ctx.areaOffset;
            ctx.drawnLength = 0;
            this.drawFragment(ctx);
            ctx.continueDraw = false;
        }
    }

    private void drawTheRestOfTextLine(DrawInfo ctx) {
        this.handleEOL(ctx);
        if (ctx.textLimitLineVisible && ctx.textLimitWidth > 0) {
            int lineX = ctx.textMargin.left + ctx.textLimitWidth * ctx.defaultSpaceWidth;
            if (ctx.graphics != null) {
                ctx.graphics.setColor(ctx.textLimitLineColor);
                Rectangle clip = ctx.graphics.getClipBounds();
                if (clip.height > ctx.lineHeight) {
                    ctx.graphics.drawLine(lineX, ctx.y, lineX, ctx.y + clip.height);
                }
            }
        }
    }

    private void graphicsSpecificUpdates(DrawInfo ctx, EditorUI eui) {
        EditorUiAccessor accessor = EditorUiAccessor.get();
        Rectangle bounds = accessor.getExtentBounds(eui);
        Rectangle clip = ctx.graphics.getClipBounds();
        Insets textMargin = ctx.textMargin;
        int leftMarginWidth = textMargin.left - ctx.lineNumberWidth - ctx.textLeftMarginWidth;
        if (ctx.lineNumbering && !ctx.syncedLineNumbering) {
            Color lnBackColor = ctx.lineNumberColoring.getBackColor();
            int numY = ctx.startY;
            int lnBarX = bounds.x + leftMarginWidth;
            if (!lnBackColor.equals(ctx.defaultColoring.getBackColor()) || bounds.x > 0) {
                ctx.graphics.setColor(lnBackColor);
                ctx.graphics.fillRect(lnBarX, numY, ctx.lineNumberWidth, ctx.lineIndex * ctx.lineHeight);
            }
            ctx.drawGraphics.setDefaultBackColor(lnBackColor);
            int lastDigitInd = Math.max(ctx.lineNumberChars.length - 1, 0);
            int numX = lnBarX;
            if (ctx.lineNumberMargin != null) {
                numX += ctx.lineNumberMargin.left;
            }
            ctx.bol = true;
            for (int j = 0; j < ctx.lineIndex; ++j) {
                int i;
                ctx.fragmentOffset = ctx.lineStartOffsets[j];
                ctx.foreColor = ctx.lineNumberColoring.getForeColor();
                ctx.backColor = lnBackColor;
                ctx.font = ctx.lineNumberColoring.getFont();
                ctx.strikeThroughColor = null;
                ctx.underlineColor = null;
                ctx.waveUnderlineColor = null;
                ctx.topBorderLineColor = null;
                ctx.rightBorderLineColor = null;
                ctx.bottomBorderLineColor = null;
                ctx.leftBorderLineColor = null;
                int lineNumber = ctx.startLineNumber + j;
                int layersLength = ctx.layers.length;
                for (i = 0; i < layersLength; ++i) {
                    lineNumber = ctx.layers[i].updateLineNumberContext(lineNumber, ctx);
                }
                i = lastDigitInd;
                do {
                    ctx.lineNumberChars[i--] = (char)(48 + lineNumber % 10);
                } while ((lineNumber /= 10) != 0 && i >= 0);
                while (i >= 0) {
                    ctx.lineNumberChars[i--] = 32;
                }
                ctx.drawGraphics.setY(numY);
                ctx.drawGraphics.setBuffer(ctx.lineNumberChars);
                ctx.drawGraphics.setForeColor(ctx.foreColor);
                ctx.drawGraphics.setBackColor(ctx.backColor);
                ctx.drawGraphics.setStrikeThroughColor(ctx.strikeThroughColor);
                ctx.drawGraphics.setUnderlineColor(ctx.underlineColor);
                ctx.drawGraphics.setWaveUnderlineColor(ctx.waveUnderlineColor);
                ctx.drawGraphics.setTopBorderLineColor(ctx.topBorderLineColor);
                ctx.drawGraphics.setRightBorderLineColor(ctx.rightBorderLineColor);
                ctx.drawGraphics.setBottomBorderLineColor(ctx.bottomBorderLineColor);
                ctx.drawGraphics.setLeftBorderLineColor(ctx.leftBorderLineColor);
                ctx.drawGraphics.setFont(ctx.font);
                ctx.drawGraphics.setX(lnBarX);
                ctx.drawGraphics.fillRect(ctx.lineNumberWidth);
                ctx.drawGraphics.setX(numX);
                ctx.drawGraphics.drawChars(0, ctx.lineNumberChars.length, ctx.lineNumberChars.length * ctx.lineNumberDigitWidth);
                ctx.drawGraphics.setBuffer(null);
                numY += ctx.lineHeight;
            }
        }
        ctx.graphics.setColor(ctx.defaultColoring.getBackColor());
    }

    public final void draw(DrawGraphics drawGraphics, EditorUI editorUI, int startOffset, int endOffset, int startX, int startY, int targetOffset) throws BadLocationException {
        this.draw(null, drawGraphics, editorUI, startOffset, endOffset, startX, startY, targetOffset);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public final void draw(View view, DrawGraphics drawGraphics, EditorUI editorUI, int startOffset, int endOffset, int startX, int startY, int targetOffset) throws BadLocationException {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("draw-start: [" + startX + ", " + startY + "] --------------------");
        }
        try {
            this.drawInternal(view, drawGraphics, editorUI, startOffset, endOffset, startX, startY, targetOffset);
        }
        finally {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("draw-end: [" + startX + ", " + startY + "] --------------------");
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private final void drawInternal(View view, DrawGraphics drawGraphics, EditorUI editorUI, int startOffset, int endOffset, int startX, int startY, int targetOffset) throws BadLocationException {
        Document doc;
        Object lineRoot;
        assert (drawGraphics != null);
        assert (editorUI != null);
        Document document = doc = view != null ? view.getDocument() : editorUI.getDocument();
        assert (doc instanceof BaseDocument);
        if (startOffset < 0 || endOffset < 0 || startOffset > endOffset || startX < 0 || startY < 0 || endOffset > doc.getLength() + 1 || targetOffset != -1 && targetOffset != Integer.MAX_VALUE && (targetOffset < startOffset || targetOffset > endOffset)) {
            String msg = "Invalid parameters: startOffset = " + startOffset + ", endOffset = " + endOffset + ", targetOffset = " + targetOffset + ", startX = " + startX + ", startY = " + startY + ", docLen = " + doc.getLength();
            throw new BadLocationException(msg, Integer.MIN_VALUE);
        }
        if (LOG.isLoggable(Level.FINER)) {
            Graphics g;
            lineRoot = ((BaseDocument)doc).getParagraphElement(0).getParentElement();
            int startLine = lineRoot.getElementIndex(startOffset);
            int startLineOffset = lineRoot.getElement(startLine).getStartOffset();
            int endLine = lineRoot.getElementIndex(endOffset);
            int endLineOffset = lineRoot.getElement(endLine).getStartOffset();
            String msg = "---------- DRAWING startOffset=" + startOffset + ", startLine=" + startLine + "(o=" + startLineOffset + "), endOffset=" + endOffset + ", endLine=" + endLine + "(o=" + endLineOffset + "), targetOffset = " + targetOffset + ", clip=" + ((g = drawGraphics.getGraphics()) != null ? g.getClipBounds().toString() : "null") + "  ------------------";
            LOG.finer(msg);
        }
        lineRoot = editorUI;
        synchronized (lineRoot) {
            DrawInfo ctx = new DrawInfo();
            ctx.doc = (BaseDocument)doc;
            ctx.component = editorUI.getComponent();
            ctx.editorUI = editorUI;
            ctx.drawGraphics = drawGraphics;
            ctx.drawGraphics.setView(view);
            ctx.view = view;
            ctx.startX = startX;
            ctx.startY = startY;
            ctx.doc.readLock();
            try {
                ctx.docLen = ctx.doc.getLength();
                ctx.startOffset = startOffset;
                ctx.endOffset = endOffset;
                ctx.targetOffset = targetOffset;
                if (ctx.view != null) {
                    assert (ctx.view instanceof DrawEngineLineView);
                    Element line = ctx.doc.getParagraphElement(ctx.startOffset);
                    ctx.lineStartOffset = line.getStartOffset();
                    ctx.lineEndOffset = line.getEndOffset();
                } else {
                    ctx.lineStartOffset = ctx.startOffset;
                    ctx.lineEndOffset = ctx.endOffset;
                }
                if (ctx.endOffset < ctx.lineEndOffset) {
                    ++ctx.endOffset;
                } else if (ctx.endOffset == ctx.docLen) {
                    ++ctx.endOffset;
                    ++ctx.lineEndOffset;
                }
                this.initInfo(ctx, editorUI);
                this.drawArea(ctx);
                if (ctx.endOffset >= ctx.docLen) {
                    this.drawTheRestOfTextLine(ctx);
                }
                if (ctx.graphics != null) {
                    this.graphicsSpecificUpdates(ctx, editorUI);
                }
            }
            finally {
                ctx.doc.readUnlock();
                ctx.drawGraphics.setBuffer(null);
                ctx.drawGraphics.finish();
            }
        }
    }

    static {
        SPACE = new char[]{' '};
    }

    static class DrawInfo
    implements DrawContext {
        Color foreColor;
        Color backColor;
        Color underlineColor;
        Color waveUnderlineColor;
        Color strikeThroughColor;
        Color topBorderLineColor;
        Color rightBorderLineColor;
        Color bottomBorderLineColor;
        Color leftBorderLineColor;
        Font font;
        int startOffset;
        int endOffset;
        int lineStartOffset;
        int lineEndOffset;
        boolean bol;
        boolean eol;
        EditorUI editorUI;
        char[] buffer;
        int bufferStartOffset;
        int fragmentOffset;
        int fragmentLength;
        int areaOffset;
        int areaLength;
        DrawGraphics drawGraphics;
        int targetOffset;
        Segment text;
        char[] textArray;
        View view;
        JTextComponent component;
        BaseDocument doc;
        int docLen;
        int visualColumn;
        int x;
        int y;
        int startX;
        int startY;
        int lineHeight;
        Coloring defaultColoring;
        int tabSize;
        boolean continueDraw;
        int startLineNumber;
        int lineIndex;
        int[] lineStartOffsets;
        char[] lineNumberChars;
        Coloring lineNumberColoring;
        int lineNumberWidth;
        int lineNumberDigitWidth;
        Insets lineNumberMargin;
        Graphics graphics;
        boolean lineNumbering;
        boolean syncedLineNumbering;
        DrawLayer[] layers;
        boolean[] layerActives;
        int[] layerActivityChangeOffsets;
        int updateOffset;
        int layerUpdateOffset;
        boolean drawMarkUpdate;
        List<DrawMark> drawMarkList;
        int drawMarkIndex;
        DrawMark drawMark;
        int drawMarkOffset;
        int drawnLength;
        int fragmentStartIndex;
        boolean tabsFragment;
        int spaceWidth;
        int defaultSpaceWidth;
        int fragmentWidth;
        int fragmentCharCount;
        Insets textMargin;
        int textLeftMarginWidth;
        boolean textLimitLineVisible;
        Color textLimitLineColor;
        int textLimitWidth;

        DrawInfo() {
        }

        @Override
        public Color getForeColor() {
            return this.foreColor;
        }

        @Override
        public void setForeColor(Color foreColor) {
            this.foreColor = foreColor;
        }

        @Override
        public Color getBackColor() {
            return this.backColor;
        }

        @Override
        public void setBackColor(Color backColor) {
            this.backColor = backColor;
        }

        @Override
        public Color getUnderlineColor() {
            return this.underlineColor;
        }

        @Override
        public void setUnderlineColor(Color underlineColor) {
            this.underlineColor = underlineColor;
        }

        @Override
        public Color getWaveUnderlineColor() {
            return this.waveUnderlineColor;
        }

        @Override
        public void setWaveUnderlineColor(Color waveUnderlineColor) {
            this.waveUnderlineColor = waveUnderlineColor;
        }

        @Override
        public Color getStrikeThroughColor() {
            return this.strikeThroughColor;
        }

        @Override
        public void setStrikeThroughColor(Color strikeThroughColor) {
            this.strikeThroughColor = strikeThroughColor;
        }

        @Override
        public Color getTopBorderLineColor() {
            return this.topBorderLineColor;
        }

        @Override
        public void setTopBorderLineColor(Color topBorderLineColor) {
            this.topBorderLineColor = topBorderLineColor;
        }

        @Override
        public Color getRightBorderLineColor() {
            return this.rightBorderLineColor;
        }

        @Override
        public void setRightBorderLineColor(Color rightBorderLineColor) {
            this.rightBorderLineColor = rightBorderLineColor;
        }

        @Override
        public Color getBottomBorderLineColor() {
            return this.bottomBorderLineColor;
        }

        @Override
        public void setBottomBorderLineColor(Color bottomBorderLineColor) {
            this.bottomBorderLineColor = bottomBorderLineColor;
        }

        @Override
        public Color getLeftBorderLineColor() {
            return this.leftBorderLineColor;
        }

        @Override
        public void setLeftBorderLineColor(Color leftBorderLineColor) {
            this.leftBorderLineColor = leftBorderLineColor;
        }

        @Override
        public Font getFont() {
            return this.font;
        }

        @Override
        public void setFont(Font font) {
            this.font = font;
        }

        @Override
        public int getStartOffset() {
            return this.startOffset;
        }

        @Override
        public int getEndOffset() {
            return this.endOffset;
        }

        @Override
        public boolean isBOL() {
            return this.bol;
        }

        @Override
        public boolean isEOL() {
            return this.eol;
        }

        @Override
        public EditorUI getEditorUI() {
            return this.editorUI;
        }

        @Override
        public char[] getBuffer() {
            return this.buffer;
        }

        @Override
        public int getBufferStartOffset() {
            return this.bufferStartOffset;
        }

        @Override
        public TokenID getTokenID() {
            return null;
        }

        @Override
        public TokenContextPath getTokenContextPath() {
            return null;
        }

        @Override
        public int getTokenOffset() {
            return this.areaOffset;
        }

        @Override
        public int getTokenLength() {
            return this.areaLength;
        }

        @Override
        public int getFragmentOffset() {
            return this.fragmentOffset;
        }

        @Override
        public int getFragmentLength() {
            return this.fragmentLength;
        }
    }

}

