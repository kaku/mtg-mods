/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.lib.drawing;

import java.lang.ref.WeakReference;
import javax.swing.text.Position;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.InvalidMarkException;
import org.netbeans.editor.MarkFactory;

class DrawMark
extends MarkFactory.ContextMark {
    protected boolean activateLayer;
    String layerName;
    WeakReference editorUIRef;

    public DrawMark(String layerName, EditorUI editorUI) {
        this(layerName, editorUI, Position.Bias.Forward);
    }

    public DrawMark(String layerName, EditorUI editorUI, Position.Bias bias) {
        super(bias, false);
        this.layerName = layerName;
        this.setEditorUI(editorUI);
    }

    public boolean isDocumentMark() {
        return this.editorUIRef == null;
    }

    public EditorUI getEditorUI() {
        if (this.editorUIRef != null) {
            return (EditorUI)this.editorUIRef.get();
        }
        return null;
    }

    public void setEditorUI(EditorUI editorUI) {
        this.editorUIRef = editorUI != null ? new WeakReference<EditorUI>(editorUI) : null;
    }

    public boolean isValidUI() {
        return this.editorUIRef == null || this.editorUIRef.get() != null;
    }

    public void setActivateLayer(boolean activateLayer) {
        this.activateLayer = activateLayer;
    }

    public boolean getActivateLayer() {
        return this.activateLayer;
    }

    public boolean removeInvalid() {
        if (!this.isValidUI() && this.isValid()) {
            try {
                this.remove();
            }
            catch (InvalidMarkException e) {
                throw new IllegalStateException(e.toString());
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        try {
            return "pos=" + this.getOffset() + ", line=" + this.getLine();
        }
        catch (InvalidMarkException e) {
            return "mark not valid";
        }
    }
}

