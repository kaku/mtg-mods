/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.api.lexer.TokenId
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.WeakHashMap;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.text.AttributeSet;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.TokenCategory;
import org.netbeans.editor.TokenContext;
import org.netbeans.editor.TokenContextPath;
import org.netbeans.editor.TokenID;
import org.netbeans.modules.editor.lib.SettingsConversions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public final class ColoringMap {
    private static final Logger LOG = Logger.getLogger(ColoringMap.class.getName());
    public static final String PROP_COLORING_MAP = "ColoringMap.PROP_COLORING_MAP";
    private static final Map<MimePath, ColoringMap> CACHE = new WeakHashMap<MimePath, ColoringMap>();
    private static final ColoringMap EMPTY = new ColoringMap();
    private static final ThreadLocal<Boolean> IN_GET = new ThreadLocal<Boolean>(){

        @Override
        protected Boolean initialValue() {
            return Boolean.FALSE;
        }
    };
    private final List<String> legacyNonTokenColoringNames;
    private final Language<?> lexerLanguage;
    private final List<? extends TokenContext> syntaxLanguages;
    private final Lookup.Result<FontColorSettings> lookupResult;
    private final LookupListener lookupListener;
    private final PropertyChangeSupport PCS;
    private final String LOCK;
    private Map<String, Coloring> map;
    private static final List<String> FONT_COLOR_NAMES_COLORINGS = Arrays.asList("default", "line-number", "guarded", "code-folding", "code-folding-bar", "selection", "highlight-search", "inc-search", "block-search", "status-bar", "status-bar-bold", "highlight-caret-row", "text-limit-line-color", "caret-color-insert-mode", "caret-color-overwrite-mode", "documentation-popup-coloring");

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static ColoringMap get(String mimeType) {
        if (!IN_GET.get().booleanValue()) {
            IN_GET.set(true);
            try {
                ColoringMap coloringMap = ColoringMap.getInternal(mimeType);
                return coloringMap;
            }
            finally {
                IN_GET.set(false);
            }
        }
        return EMPTY;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Map<String, Coloring> getMap() {
        String string = this.LOCK;
        synchronized (string) {
            if (this.map == null) {
                this.map = ColoringMap.loadTheMap(this.legacyNonTokenColoringNames, this.lexerLanguage, this.syntaxLanguages, this.lookupResult.allInstances());
            }
            return this.map;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.PCS.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.PCS.removePropertyChangeListener(l);
    }

    private ColoringMap() {
        this.lookupListener = new LookupListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            public void resultChanged(LookupEvent ev) {
                String string = ColoringMap.this.LOCK;
                synchronized (string) {
                    ColoringMap.this.map = null;
                }
                ColoringMap.this.PCS.firePropertyChange("ColoringMap.PROP_COLORING_MAP", null, null);
            }
        };
        this.PCS = new PropertyChangeSupport(this);
        this.LOCK = new String("ColoringMap.LOCK");
        this.map = null;
        this.legacyNonTokenColoringNames = null;
        this.lexerLanguage = null;
        this.syntaxLanguages = null;
        this.lookupResult = null;
        this.map = Collections.emptyMap();
    }

    private ColoringMap(List<String> legacyNonTokenColoringNames, Language<?> lexerLanguage, List<? extends TokenContext> syntaxLanguages, Lookup.Result<FontColorSettings> lookupResult) {
        this.lookupListener = new ;
        this.PCS = new PropertyChangeSupport(this);
        this.LOCK = new String("ColoringMap.LOCK");
        this.map = null;
        this.legacyNonTokenColoringNames = legacyNonTokenColoringNames;
        this.lexerLanguage = lexerLanguage;
        this.syntaxLanguages = syntaxLanguages;
        this.lookupResult = lookupResult;
        this.map = ColoringMap.loadTheMap(legacyNonTokenColoringNames, lexerLanguage, syntaxLanguages, lookupResult.allInstances());
        this.lookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.lookupListener, this.lookupResult));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static ColoringMap getInternal(String mimeType) {
        MimePath mimePath = mimeType == null || mimeType.length() == 0 ? MimePath.EMPTY : MimePath.parse((String)mimeType);
        Map<MimePath, ColoringMap> map = CACHE;
        synchronized (map) {
            ColoringMap cm = CACHE.get((Object)mimePath);
            if (cm != null) {
                return cm;
            }
        }
        List<String> legacyNonTokenColoringNames = ColoringMap.findLegacyNonTokenColoringNames(mimePath);
        Lookup.Result lookupResult = MimeLookup.getLookup((MimePath)mimePath).lookupResult(FontColorSettings.class);
        Language lexerLanguage = null;
        List<? extends TokenContext> syntaxLanguage = null;
        if (mimePath.size() > 0) {
            lexerLanguage = Language.find((String)mimePath.getPath());
            syntaxLanguage = ColoringMap.findSyntaxLanguage(mimePath);
        }
        LOG.fine("Creating ColoringMap for '" + mimeType + "' ---------------------------");
        ColoringMap myCm = new ColoringMap(legacyNonTokenColoringNames, lexerLanguage, syntaxLanguage, lookupResult);
        LOG.fine("----------------------------------------------------------------------");
        Map<MimePath, ColoringMap> map2 = CACHE;
        synchronized (map2) {
            ColoringMap cm = CACHE.get((Object)mimePath);
            if (cm == null) {
                cm = myCm;
                CACHE.put(mimePath, cm);
            }
            return cm;
        }
    }

    private static Map<String, Coloring> loadTheMap(List<String> legacyNonTokenColoringNames, Language<?> lexerLanguage, List<? extends TokenContext> syntaxLanguages, Collection<? extends FontColorSettings> fontsColors) {
        HashMap<String, Coloring> coloringMap = new HashMap<String, Coloring>();
        if (!fontsColors.isEmpty()) {
            FontColorSettings fcs = fontsColors.iterator().next();
            if (legacyNonTokenColoringNames != null) {
                ColoringMap.collectLegacyNonTokenColorings(coloringMap, legacyNonTokenColoringNames, fcs);
            }
            ColoringMap.collectNonTokenColorings(coloringMap, fcs);
            if (syntaxLanguages != null) {
                ColoringMap.collectLegacyTokenColorings(coloringMap, syntaxLanguages, fcs);
            }
            if (lexerLanguage != null) {
                ColoringMap.collectTokenColorings(coloringMap, lexerLanguage, fcs);
            }
        }
        return Collections.unmodifiableMap(coloringMap);
    }

    private static void collectNonTokenColorings(HashMap<String, Coloring> coloringMap, FontColorSettings fcs) {
        for (String coloringName : FONT_COLOR_NAMES_COLORINGS) {
            AttributeSet attribs = fcs.getFontColors(coloringName);
            if (attribs == null) continue;
            LOG.fine("Loading coloring '" + coloringName + "'");
            coloringMap.put(coloringName, Coloring.fromAttributeSet(attribs));
        }
    }

    private static void collectLegacyNonTokenColorings(HashMap<String, Coloring> coloringMap, List<String> legacyNonTokenColoringNames, FontColorSettings fcs) {
        for (int i = legacyNonTokenColoringNames.size() - 1; i >= 0; --i) {
            String coloringName = legacyNonTokenColoringNames.get(i);
            AttributeSet attribs = fcs.getFontColors(coloringName);
            if (attribs == null) continue;
            LOG.fine("Loading legacy coloring '" + coloringName + "'");
            coloringMap.put(coloringName, Coloring.fromAttributeSet(attribs));
        }
    }

    private static void collectTokenColorings(HashMap<String, Coloring> coloringMap, Language<?> lexerLanguage, FontColorSettings fcs) {
        AttributeSet attribs;
        for (String category : lexerLanguage.tokenCategories()) {
            attribs = fcs.getTokenFontColors(category);
            if (attribs == null) continue;
            LOG.fine("Loading token coloring '" + category + "'");
            coloringMap.put(category, Coloring.fromAttributeSet(attribs));
        }
        for (TokenId tokenId : lexerLanguage.tokenIds()) {
            attribs = fcs.getTokenFontColors(tokenId.name());
            if (attribs == null) continue;
            LOG.fine("Loading token coloring '" + tokenId.name() + "'");
            coloringMap.put(tokenId.name(), Coloring.fromAttributeSet(attribs));
        }
    }

    private static void collectLegacyTokenColorings(HashMap<String, Coloring> coloringMap, List<? extends TokenContext> tokenContextList, FontColorSettings fcs) {
        for (int i = tokenContextList.size() - 1; i >= 0; --i) {
            TokenContext tc = tokenContextList.get(i);
            TokenContextPath[] allPaths = tc.getAllContextPaths();
            for (int j = 0; j < allPaths.length; ++j) {
                TokenContext firstContext = allPaths[j].getContexts()[0];
                TokenCategory[] tokenCategories = firstContext.getTokenCategories();
                for (int k = 0; k < tokenCategories.length; ++k) {
                    String fullName = allPaths[j].getFullTokenName(tokenCategories[k]);
                    AttributeSet attribs = fcs.getTokenFontColors(fullName);
                    if (attribs == null) continue;
                    LOG.fine("Loading legacy token coloring '" + fullName + "'");
                    coloringMap.put(fullName, Coloring.fromAttributeSet(attribs));
                }
                TokenID[] tokenIDs = firstContext.getTokenIDs();
                for (int k2 = 0; k2 < tokenIDs.length; ++k2) {
                    String fullName = allPaths[j].getFullTokenName(tokenIDs[k2]);
                    AttributeSet attribs = fcs.getTokenFontColors(fullName);
                    if (attribs == null) continue;
                    LOG.fine("Loading legacy token coloring '" + fullName + "'");
                    coloringMap.put(fullName, Coloring.fromAttributeSet(attribs));
                }
            }
        }
    }

    private static List<String> findLegacyNonTokenColoringNames(MimePath mimePath) {
        String namesList;
        ArrayList<String> legacyNonTokenColoringNames = new ArrayList<String>();
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        if (prefs != null && (namesList = prefs.get("coloring-name-list", null)) != null && namesList.length() > 0) {
            StringTokenizer t = new StringTokenizer(namesList, ",");
            while (t.hasMoreTokens()) {
                String coloringName = t.nextToken().trim();
                legacyNonTokenColoringNames.add(coloringName);
            }
        }
        return legacyNonTokenColoringNames;
    }

    private static List<? extends TokenContext> findSyntaxLanguage(MimePath mimePath) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        List languages = (List)SettingsConversions.callFactory(prefs, mimePath, "token-context-list", null);
        return languages;
    }

}

