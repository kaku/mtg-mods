/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.lib.drawing;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseKit;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.FoldingToolTip;
import org.netbeans.editor.FontMetricsCache;
import org.netbeans.editor.PopupManager;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.ext.ToolTipSupport;
import org.netbeans.editor.view.spi.LockView;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib.drawing.DrawEngineFakeDocView;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

@Deprecated
class CollapsedView
extends View {
    private static final int MARGIN_WIDTH = 4;
    private final Position startPos;
    private final Position endPos;
    private final String foldDescription;
    private volatile AttributeSet attribs;
    private Lookup.Result<? extends FontColorSettings> fcsLookupResult;
    private final LookupListener fcsTracker;

    public CollapsedView(Element elem, Position startPos, Position endPos, String foldDescription) {
        super(elem);
        this.fcsTracker = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                CollapsedView.this.attribs = null;
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        JTextComponent jtc = CollapsedView.this.getComponent();
                        if (jtc != null) {
                            CollapsedView.this.getBaseTextUI().damageRange(jtc, CollapsedView.this.getStartOffset(), CollapsedView.this.getEndOffset());
                        }
                    }
                });
            }

        };
        this.startPos = startPos;
        this.endPos = endPos;
        this.foldDescription = foldDescription;
    }

    private Coloring getColoring() {
        if (this.attribs == null) {
            FontColorSettings fcs;
            AttributeSet attr;
            if (this.fcsLookupResult == null) {
                this.fcsLookupResult = MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)this.getComponent())).lookupResult(FontColorSettings.class);
                this.fcsLookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.fcsTracker, this.fcsLookupResult));
            }
            attr = (attr = (fcs = (FontColorSettings)this.fcsLookupResult.allInstances().iterator().next()).getFontColors("code-folding")) == null ? fcs.getFontColors("default") : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{attr, fcs.getFontColors("default")});
            this.attribs = attr;
        }
        return Coloring.fromAttributeSet(this.attribs);
    }

    private JTextComponent getComponent() {
        return (JTextComponent)this.getContainer();
    }

    private BaseTextUI getBaseTextUI() {
        JTextComponent comp = this.getComponent();
        return comp != null ? (BaseTextUI)comp.getUI() : null;
    }

    private EditorUI getEditorUI() {
        BaseTextUI btui = this.getBaseTextUI();
        return btui != null ? btui.getEditorUI() : null;
    }

    @Override
    public Document getDocument() {
        View parent = this.getParent();
        return parent == null ? null : parent.getDocument();
    }

    @Override
    public int getStartOffset() {
        return this.startPos.getOffset();
    }

    @Override
    public int getEndOffset() {
        return this.endPos.getOffset();
    }

    @Override
    public float getAlignment(int axis) {
        return 0.0f;
    }

    @Override
    public float getPreferredSpan(int axis) {
        switch (axis) {
            case 1: {
                return this.getEditorUI().getLineHeight();
            }
            case 0: {
                return this.getCollapsedFoldStringWidth();
            }
        }
        return 1.0f;
    }

    private int getCollapsedFoldStringWidth() {
        JTextComponent comp = this.getComponent();
        if (comp == null) {
            return 0;
        }
        FontMetrics fm = FontMetricsCache.getFontMetrics(this.getColoring().getFont(), comp);
        if (fm == null) {
            return 0;
        }
        return fm.stringWidth(this.foldDescription) + 8;
    }

    @Override
    public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
        return new Rectangle(a.getBounds().x, a.getBounds().y, this.getCollapsedFoldStringWidth(), this.getEditorUI().getLineHeight());
    }

    @Override
    public int viewToModel(float x, float y, Shape a, Position.Bias[] biasReturn) {
        return this.getStartOffset();
    }

    @Override
    public void paint(Graphics g, Shape allocation) {
        Rectangle r = allocation.getBounds();
        Coloring coloring = this.getColoring();
        g.setColor(coloring.getBackColor());
        g.fillRect(r.x, r.y, r.width, r.height);
        g.setColor(coloring.getForeColor());
        g.drawRect(r.x, r.y, r.width - 1, r.height - 1);
        g.setFont(coloring.getFont());
        g.drawString(this.foldDescription, r.x + 4, r.y + this.getEditorUI().getLineAscent() - 1);
    }

    @Override
    public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
        biasRet[0] = Position.Bias.Forward;
        switch (direction) {
            case 1: 
            case 5: {
                Rectangle loc;
                JTextComponent target = (JTextComponent)this.getContainer();
                Caret c = target != null ? target.getCaret() : null;
                Point mcp = c != null ? c.getMagicCaretPosition() : null;
                int x = mcp == null ? ((loc = target.modelToView(pos)) == null ? 0 : loc.x) : mcp.x;
                if (direction == 1) {
                    pos = Utilities.getPositionAbove(target, pos, x);
                    break;
                }
                pos = Utilities.getPositionBelow(target, pos, x);
                break;
            }
            case 7: {
                if (pos == -1) {
                    pos = Math.max(0, this.getStartOffset());
                    break;
                }
                if (b == Position.Bias.Backward) {
                    pos = Math.max(0, this.getStartOffset());
                    break;
                }
                pos = Math.max(0, this.getStartOffset() - 1);
                break;
            }
            case 3: {
                if (pos == -1) {
                    pos = this.getStartOffset();
                    break;
                }
                pos = Math.min(this.getEndOffset(), this.getDocument().getLength());
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return pos;
    }

    private View getExpandedView() {
        Element parentElem = this.getElement().getParentElement();
        int sei = parentElem.getElementIndex(this.getStartOffset());
        int so = parentElem.getElement(sei).getStartOffset();
        int eei = parentElem.getElementIndex(this.getEndOffset());
        int eo = parentElem.getElement(eei).getEndOffset();
        LockView fakeView = new LockView(new DrawEngineFakeDocView(parentElem, so, eo, false, true));
        RootView rootView = new RootView();
        rootView.setView(fakeView);
        return fakeView;
    }

    @Override
    public String getToolTipText(float x, float y, Shape allocation) {
        ToolTipSupport tts = this.getEditorUI().getToolTipSupport();
        FoldingToolTip toolTip = new FoldingToolTip(this.getExpandedView(), this.getEditorUI());
        tts.setToolTip(toolTip, PopupManager.ScrollBarBounds, PopupManager.Largest, -2, 0);
        return "";
    }

    class RootView
    extends View {
        private View view;

        RootView() {
            super(null);
        }

        void setView(View v) {
            if (this.view != null) {
                this.view.setParent(null);
            }
            this.view = v;
            if (this.view != null) {
                this.view.setParent(this);
            }
        }

        @Override
        public AttributeSet getAttributes() {
            return null;
        }

        @Override
        public float getPreferredSpan(int axis) {
            if (this.view != null) {
                return this.view.getPreferredSpan(axis);
            }
            return 10.0f;
        }

        @Override
        public float getMinimumSpan(int axis) {
            if (this.view != null) {
                return this.view.getMinimumSpan(axis);
            }
            return 10.0f;
        }

        @Override
        public float getMaximumSpan(int axis) {
            return 2.14748365E9f;
        }

        @Override
        public float getAlignment(int axis) {
            if (this.view != null) {
                return this.view.getAlignment(axis);
            }
            return 0.0f;
        }

        @Override
        public void paint(Graphics g, Shape allocation) {
            if (this.view != null) {
                Rectangle alloc = allocation instanceof Rectangle ? (Rectangle)allocation : allocation.getBounds();
                this.setSize(alloc.width, alloc.height);
                this.view.paint(g, allocation);
            }
        }

        @Override
        public void setParent(View parent) {
            throw new Error("Can't set parent on root view");
        }

        @Override
        public int getViewCount() {
            return 1;
        }

        @Override
        public View getView(int n) {
            return this.view;
        }

        @Override
        public int getViewIndex(int pos, Position.Bias b) {
            return 0;
        }

        @Override
        public Shape getChildAllocation(int index, Shape a) {
            return a;
        }

        @Override
        public Shape modelToView(int pos, Shape a, Position.Bias b) throws BadLocationException {
            if (this.view != null) {
                return this.view.modelToView(pos, a, b);
            }
            return null;
        }

        @Override
        public Shape modelToView(int p0, Position.Bias b0, int p1, Position.Bias b1, Shape a) throws BadLocationException {
            if (this.view != null) {
                return this.view.modelToView(p0, b0, p1, b1, a);
            }
            return null;
        }

        @Override
        public int viewToModel(float x, float y, Shape a, Position.Bias[] bias) {
            if (this.view != null) {
                int retValue = this.view.viewToModel(x, y, a, bias);
                return retValue;
            }
            return -1;
        }

        @Override
        public int getNextVisualPositionFrom(int pos, Position.Bias b, Shape a, int direction, Position.Bias[] biasRet) throws BadLocationException {
            if (this.view != null) {
                int nextPos = this.view.getNextVisualPositionFrom(pos, b, a, direction, biasRet);
                if (nextPos != -1) {
                    pos = nextPos;
                } else {
                    biasRet[0] = b;
                }
            }
            return pos;
        }

        @Override
        public void insertUpdate(DocumentEvent e, Shape a, ViewFactory f) {
            if (this.view != null) {
                this.view.insertUpdate(e, a, f);
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e, Shape a, ViewFactory f) {
            if (this.view != null) {
                this.view.removeUpdate(e, a, f);
            }
        }

        @Override
        public void changedUpdate(DocumentEvent e, Shape a, ViewFactory f) {
            if (this.view != null) {
                this.view.changedUpdate(e, a, f);
            }
        }

        @Override
        public Document getDocument() {
            EditorUI editorUI = CollapsedView.this.getEditorUI();
            return editorUI == null ? null : editorUI.getDocument();
        }

        @Override
        public int getStartOffset() {
            if (this.view != null) {
                return this.view.getStartOffset();
            }
            return this.getElement().getStartOffset();
        }

        @Override
        public int getEndOffset() {
            if (this.view != null) {
                return this.view.getEndOffset();
            }
            return this.getElement().getEndOffset();
        }

        @Override
        public Element getElement() {
            if (this.view != null) {
                return this.view.getElement();
            }
            return this.view.getDocument().getDefaultRootElement();
        }

        public View breakView(int axis, float len, Shape a) {
            throw new Error("Can't break root view");
        }

        @Override
        public int getResizeWeight(int axis) {
            if (this.view != null) {
                return this.view.getResizeWeight(axis);
            }
            return 0;
        }

        @Override
        public void setSize(float width, float height) {
            if (this.view != null) {
                this.view.setSize(width, height);
            }
        }

        @Override
        public Container getContainer() {
            EditorUI editorUI = CollapsedView.this.getEditorUI();
            return editorUI == null ? null : editorUI.getComponent();
        }

        @Override
        public ViewFactory getViewFactory() {
            ViewFactory f;
            BaseKit kit;
            EditorUI editorUI = CollapsedView.this.getEditorUI();
            if (editorUI != null && (f = (kit = Utilities.getKit(editorUI.getComponent())).getViewFactory()) != null) {
                return f;
            }
            return CollapsedView.this.getBaseTextUI();
        }
    }

}

