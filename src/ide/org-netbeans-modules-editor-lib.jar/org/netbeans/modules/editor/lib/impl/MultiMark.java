/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.editor.lib.impl;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import javax.swing.text.Position;
import org.netbeans.modules.editor.lib.impl.BasePosition;
import org.netbeans.modules.editor.lib.impl.MarkVector;
import org.openide.util.Utilities;

public final class MultiMark
extends WeakReference<BasePosition>
implements Runnable {
    static final int BACKWARD_BIAS = 1;
    static final int VALID = 2;
    static final int REMOVED = 4;
    static final int COMPATIBLE = 8;
    static final int ZERO = 16;
    int rawOffset;
    int flags;
    private final MarkVector markVector;

    MultiMark(BasePosition pos, MarkVector markVector, int offset) {
        this(pos, markVector, offset, offset != 0 ? 8 : 25);
    }

    MultiMark(BasePosition pos, MarkVector markVector, int offset, Position.Bias bias) {
        this(pos, markVector, offset, bias == Position.Bias.Backward ? 1 : 0);
    }

    private MultiMark(BasePosition pos, MarkVector markVector, int offset, int flags) {
        super(pos, Utilities.activeReferenceQueue());
        if (pos != null) {
            pos.setMark(this);
        }
        this.markVector = markVector;
        this.rawOffset = offset;
        this.flags = flags;
    }

    public Position.Bias getBias() {
        return (this.flags & 1) != 0 ? Position.Bias.Backward : Position.Bias.Forward;
    }

    public int getOffset() {
        MarkVector markVector = this.markVector;
        synchronized (markVector) {
            if ((this.flags & 2) != 0) {
                return (this.flags & 16) == 0 ? this.markVector.getOffset(this.rawOffset) : 0;
            }
            throw new IllegalStateException();
        }
    }

    @Override
    public void run() {
        this.dispose();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void dispose() {
        MarkVector markVector = this.markVector;
        synchronized (markVector) {
            if ((this.flags & 2) != 0) {
                this.flags &= -3;
            } else {
                throw new IllegalStateException();
            }
            this.markVector.notifyMarkDisposed();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isValid() {
        MarkVector markVector = this.markVector;
        synchronized (markVector) {
            return (this.flags & 2) != 0;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        MarkVector markVector = this.markVector;
        synchronized (markVector) {
            if ((this.flags & 2) != 0) {
                sb.append("offset=" + this.getOffset());
            } else {
                sb.append("removed");
            }
            sb.append(", bias=");
            sb.append(this.getBias());
            return sb.toString();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toStringDetail() {
        StringBuffer sb = new StringBuffer();
        MarkVector markVector = this.markVector;
        synchronized (markVector) {
            sb.append(System.identityHashCode(this));
            sb.append(" (");
            sb.append(this.rawOffset);
            sb.append(" -> ");
            if ((this.flags & 2) != 0) {
                sb.append(this.getOffset());
            } else {
                sb.append('X');
                sb.append(this.markVector.getOffset(this.rawOffset));
                sb.append('X');
            }
            sb.append(", ");
            sb.append((this.flags & 1) != 0 ? 'B' : 'F');
            if ((this.flags & 2) != 0) {
                sb.append('V');
            }
            if ((this.flags & 4) != 0) {
                sb.append('R');
            }
            if ((this.flags & 8) != 0) {
                sb.append('C');
            }
            if ((this.flags & 16) != 0) {
                sb.append('Z');
            }
            sb.append(')');
            return sb.toString();
        }
    }
}

