/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.Field
 *  org.apache.lucene.document.Field$Index
 *  org.apache.lucene.document.Field$Store
 *  org.apache.lucene.document.Fieldable
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.search.Query
 *  org.apache.lucene.search.TermQuery
 */
package org.netbeans.modules.parsing.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;

public final class IndexDocumentImpl
implements IndexDocument {
    static final String FIELD_PRIMARY_KEY = "_sn";
    final Document doc;

    public IndexDocumentImpl(String primaryKey) {
        assert (primaryKey != null);
        this.doc = new Document();
        this.doc.add(IndexDocumentImpl.sourceNameField(primaryKey));
    }

    public IndexDocumentImpl(Document doc) {
        assert (doc != null);
        this.doc = doc;
    }

    @Override
    public void addPair(String key, String value, boolean searchable, boolean stored) {
        Field field = new Field(key, value, stored ? Field.Store.YES : Field.Store.NO, searchable ? Field.Index.NOT_ANALYZED_NO_NORMS : Field.Index.NO);
        this.doc.add((Fieldable)field);
    }

    @Override
    public String getValue(String key) {
        return this.doc.get(key);
    }

    @Override
    public String[] getValues(String key) {
        return this.doc.getValues(key);
    }

    @Override
    public String getPrimaryKey() {
        return this.doc.get("_sn");
    }

    public String toString() {
        return this.getClass().getSimpleName() + "@" + Integer.toHexString(System.identityHashCode(this)) + "; " + this.getPrimaryKey();
    }

    private static Fieldable sourceNameField(String relativePath) {
        return new Field("_sn", relativePath, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
    }

    static Query sourceNameQuery(String relativePath) {
        return new TermQuery(IndexDocumentImpl.sourceNameTerm(relativePath));
    }

    static Term sourceNameTerm(String relativePath) {
        assert (relativePath != null);
        return new Term("_sn", relativePath);
    }
}

