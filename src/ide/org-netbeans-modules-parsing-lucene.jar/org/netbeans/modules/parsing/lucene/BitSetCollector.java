/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.index.IndexReader
 *  org.apache.lucene.search.Collector
 *  org.apache.lucene.search.Scorer
 */
package org.netbeans.modules.parsing.lucene;

import java.util.BitSet;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;

class BitSetCollector
extends Collector {
    private int docBase;
    public final BitSet bits;

    BitSetCollector(BitSet bitSet) {
        assert (bitSet != null);
        this.bits = bitSet;
    }

    public void setScorer(Scorer scorer) {
    }

    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    public void collect(int doc) {
        this.bits.set(doc + this.docBase);
    }

    public void setNextReader(IndexReader reader, int docBase) {
        this.docBase = docBase;
    }
}

