/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.lucene;

import java.net.URI;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.lucene.Evictable;
import org.netbeans.modules.parsing.lucene.EvictionPolicy;
import org.netbeans.modules.parsing.lucene.LRUCache;
import org.openide.util.Parameters;
import org.openide.util.Utilities;

class IndexCacheFactory {
    private static final Logger LOG = Logger.getLogger(IndexCacheFactory.class.getName());
    private static final String PROP_CACHE_SIZE = "java.index.size";
    private static final IndexCacheFactory instance = new IndexCacheFactory();
    private final RAMContoller ramController = new RAMContoller();
    private final LRUCache<URI, Evictable> nioCache = new LRUCache(new NIOPolicy());
    private final LRUCache<URI, Evictable> ramCache = new LRUCache(new RAMPolicy(this.ramController));

    private IndexCacheFactory() {
    }

    @NonNull
    LRUCache<URI, Evictable> getNIOCache() {
        return this.nioCache;
    }

    @NonNull
    LRUCache<URI, Evictable> getRAMCache() {
        return this.ramCache;
    }

    @NonNull
    RAMContoller getRAMController() {
        return this.ramController;
    }

    public static IndexCacheFactory getDefault() {
        return instance;
    }

    private static final class RAMPolicy
    implements EvictionPolicy<URI, Evictable> {
        private final RAMContoller controller;

        RAMPolicy(@NonNull RAMContoller controller) {
            Parameters.notNull((CharSequence)"controller", (Object)controller);
            this.controller = controller;
        }

        @Override
        public boolean shouldEvict(int size, URI key, Evictable value) {
            return this.controller.isFull();
        }
    }

    private static final class NIOPolicy
    implements EvictionPolicy<URI, Evictable> {
        private static final int DEFAULT_SIZE = 400;
        private static final boolean NEEDS_REMOVE = Boolean.getBoolean("IndexCache.force") || Utilities.isUnix() && !Utilities.isMac();
        private static final int MAX_SIZE;

        private NIOPolicy() {
        }

        @Override
        public boolean shouldEvict(int size, URI key, Evictable value) {
            return NEEDS_REMOVE && size > MAX_SIZE;
        }

        static {
            int value = 400;
            String sizeStr = System.getProperty("IndexCache.size");
            if (sizeStr != null) {
                try {
                    value = Integer.parseInt(sizeStr);
                }
                catch (NumberFormatException nfe) {
                    LOG.warning("Wrong (non integer) cache size: " + sizeStr);
                }
            }
            MAX_SIZE = value;
            LOG.fine("NEEDS_REMOVE: " + NEEDS_REMOVE + " MAX_SIZE: " + MAX_SIZE);
        }
    }

    static final class RAMContoller {
        private static final float DEFAULT_CACHE_SIZE = 0.05f;
        private static final long maxCacheSize = RAMContoller.getCacheSize();
        private final AtomicLong currentCacheSize = new AtomicLong();

        RAMContoller() {
        }

        public long acquire(long bytes) {
            return this.currentCacheSize.addAndGet(bytes);
        }

        public long release(long bytes) {
            return this.currentCacheSize.addAndGet((bytes ^ -1) + 1);
        }

        public boolean isFull() {
            return this.currentCacheSize.get() > maxCacheSize;
        }

        public boolean shouldLoad(long bytes) {
            return bytes < maxCacheSize;
        }

        private static long getCacheSize() {
            float per = -1.0f;
            String propVal = System.getProperty("java.index.size");
            if (propVal != null) {
                try {
                    per = Float.parseFloat(propVal);
                }
                catch (NumberFormatException nfe) {
                    // empty catch block
                }
            }
            if (per < 0.0f) {
                per = 0.05f;
            }
            return (long)(per * (float)Runtime.getRuntime().maxMemory());
        }
    }

}

