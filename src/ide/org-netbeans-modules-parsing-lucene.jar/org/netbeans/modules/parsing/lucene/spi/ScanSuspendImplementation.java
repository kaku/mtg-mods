/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.lucene.spi;

public interface ScanSuspendImplementation {
    public void suspend();

    public void resume();
}

