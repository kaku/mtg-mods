/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.index.IndexReader
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.parsing.lucene.support;

import org.apache.lucene.index.IndexReader;
import org.netbeans.api.annotations.common.NullAllowed;

public interface IndexReaderInjection {
    public void setIndexReader(@NullAllowed IndexReader var1);
}

