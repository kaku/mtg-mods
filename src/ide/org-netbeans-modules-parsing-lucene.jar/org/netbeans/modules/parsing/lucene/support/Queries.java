/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.document.FieldSelectorResult
 *  org.apache.lucene.index.IndexReader
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.index.TermDocs
 *  org.apache.lucene.index.TermEnum
 *  org.apache.lucene.search.BooleanClause
 *  org.apache.lucene.search.BooleanClause$Occur
 *  org.apache.lucene.search.BooleanQuery
 *  org.apache.lucene.search.DocIdSet
 *  org.apache.lucene.search.Filter
 *  org.apache.lucene.search.FilteredQuery
 *  org.apache.lucene.search.FilteredTermEnum
 *  org.apache.lucene.search.MatchAllDocsQuery
 *  org.apache.lucene.search.MultiTermQuery
 *  org.apache.lucene.search.MultiTermQuery$RewriteMethod
 *  org.apache.lucene.search.PrefixQuery
 *  org.apache.lucene.search.PrefixTermEnum
 *  org.apache.lucene.search.Query
 *  org.apache.lucene.search.TermQuery
 *  org.apache.lucene.util.OpenBitSet
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.lucene.support;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.document.FieldSelectorResult;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.FilteredQuery;
import org.apache.lucene.search.FilteredTermEnum;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.MultiTermQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.PrefixTermEnum;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.util.OpenBitSet;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.lucene.TermCollector;
import org.openide.util.Parameters;

public final class Queries {
    public static Query createQuery(@NonNull String fieldName, @NonNull String caseInsensitiveFieldName, @NonNull String value, @NonNull QueryKind kind) {
        Parameters.notNull((CharSequence)"fieldName", (Object)fieldName);
        Parameters.notNull((CharSequence)"caseInsensitiveFieldName", (Object)caseInsensitiveFieldName);
        Parameters.notNull((CharSequence)"value", (Object)value);
        Parameters.notNull((CharSequence)"kind", (Object)((Object)kind));
        return Queries.createQueryImpl(fieldName, caseInsensitiveFieldName, value, kind, new StandardQueryFactory());
    }

    public static Query createTermCollectingQuery(@NonNull String fieldName, @NonNull String caseInsensitiveFieldName, @NonNull String value, @NonNull QueryKind kind) {
        Parameters.notNull((CharSequence)"fieldName", (Object)fieldName);
        Parameters.notNull((CharSequence)"caseInsensitiveFieldName", (Object)caseInsensitiveFieldName);
        Parameters.notNull((CharSequence)"value", (Object)value);
        Parameters.notNull((CharSequence)"kind", (Object)((Object)kind));
        return Queries.createQueryImpl(fieldName, caseInsensitiveFieldName, value, kind, new TCQueryFactory());
    }

    public static /* varargs */ FieldSelector createFieldSelector(@NonNull String ... fieldsToLoad) {
        return new FieldSelectorImpl(fieldsToLoad);
    }

    private static Query createQueryImpl(@NonNull String fieldName, @NonNull String caseInsensitiveFieldName, @NonNull String value, @NonNull QueryKind kind, @NonNull QueryFactory f) {
        switch (kind) {
            case EXACT: {
                return f.createTermQuery(fieldName, value);
            }
            case PREFIX: {
                if (value.length() == 0) {
                    return f.createAllDocsQuery(fieldName);
                }
                return f.createPrefixQuery(fieldName, value);
            }
            case CASE_INSENSITIVE_PREFIX: {
                if (value.length() == 0) {
                    return f.createAllDocsQuery(caseInsensitiveFieldName);
                }
                return f.createPrefixQuery(caseInsensitiveFieldName, value.toLowerCase());
            }
            case CAMEL_CASE: {
                if (value.length() == 0) {
                    throw new IllegalArgumentException();
                }
                return f.createRegExpQuery(fieldName, Queries.createCamelCaseRegExp(value, true), true);
            }
            case CASE_INSENSITIVE_REGEXP: {
                if (value.length() == 0) {
                    throw new IllegalArgumentException();
                }
                return f.createRegExpQuery(caseInsensitiveFieldName, value.toLowerCase(), false);
            }
            case REGEXP: {
                if (value.length() == 0) {
                    throw new IllegalArgumentException();
                }
                return f.createRegExpQuery(fieldName, value, true);
            }
            case CASE_INSENSITIVE_CAMEL_CASE: {
                if (value.length() == 0) {
                    return f.createAllDocsQuery(caseInsensitiveFieldName);
                }
                Query pq = f.createPrefixQuery(caseInsensitiveFieldName, value.toLowerCase());
                Query fq = f.createRegExpQuery(caseInsensitiveFieldName, Queries.createCamelCaseRegExp(value, false), false);
                BooleanQuery result = f.createBooleanQuery();
                result.add(pq, BooleanClause.Occur.SHOULD);
                result.add(fq, BooleanClause.Occur.SHOULD);
                return result;
            }
        }
        throw new UnsupportedOperationException(kind.toString());
    }

    private static String createCamelCaseRegExp(String camel, boolean caseSensitive) {
        int index;
        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        do {
            String token = camel.substring(lastIndex, (index = Queries.findNextUpper(camel, lastIndex + 1)) == -1 ? camel.length() : index);
            sb.append(Pattern.quote(caseSensitive ? token : token.toLowerCase()));
            sb.append(index != -1 ? "[\\p{javaLowerCase}\\p{Digit}_\\$]*" : ".*");
            lastIndex = index;
        } while (index != -1);
        return sb.toString();
    }

    private static int findNextUpper(String text, int offset) {
        for (int i = offset; i < text.length(); ++i) {
            if (!Character.isUpperCase(text.charAt(i))) continue;
            return i;
        }
        return -1;
    }

    private Queries() {
    }

    private static class FieldSelectorImpl
    implements FieldSelector {
        private final Term[] terms;

        /* varargs */ FieldSelectorImpl(String ... fieldNames) {
            this.terms = new Term[fieldNames.length];
            for (int i = 0; i < fieldNames.length; ++i) {
                this.terms[i] = new Term(fieldNames[i], "");
            }
        }

        public FieldSelectorResult accept(String fieldName) {
            for (Term t : this.terms) {
                if (fieldName != t.field()) continue;
                return FieldSelectorResult.LOAD;
            }
            return FieldSelectorResult.NO_LOAD;
        }
    }

    private static class TCQueryFactory
    implements QueryFactory {
        private TCQueryFactory() {
        }

        @Override
        public Query createTermQuery(@NonNull String name, @NonNull String value) {
            return new TCFilteredQuery((Query)new MatchAllDocsQuery(), new TermFilter(name, value));
        }

        @Override
        public Query createPrefixQuery(@NonNull String name, @NonNull String value) {
            return new TCFilteredQuery((Query)new MatchAllDocsQuery(), new PrefixFilter(name, value));
        }

        @Override
        public Query createRegExpQuery(@NonNull String name, @NonNull String value, boolean caseSensitive) {
            return new TCFilteredQuery((Query)new MatchAllDocsQuery(), new RegexpFilter(name, value, caseSensitive));
        }

        @Override
        public Query createAllDocsQuery(@NonNull String name) {
            throw new IllegalArgumentException();
        }

        @Override
        public BooleanQuery createBooleanQuery() {
            return new TCBooleanQuery();
        }
    }

    private static class StandardQueryFactory
    implements QueryFactory {
        private StandardQueryFactory() {
        }

        @Override
        public Query createTermQuery(@NonNull String name, @NonNull String value) {
            return new TermQuery(new Term(name, value));
        }

        @Override
        public Query createPrefixQuery(@NonNull String name, @NonNull String value) {
            PrefixQuery pq = new PrefixQuery(new Term(name, value));
            pq.setRewriteMethod(PrefixQuery.CONSTANT_SCORE_FILTER_REWRITE);
            return pq;
        }

        @Override
        public Query createRegExpQuery(@NonNull String name, @NonNull String value, boolean caseSensitive) {
            return new FilteredQuery((Query)new MatchAllDocsQuery(), (Filter)new RegexpFilter(name, value, caseSensitive));
        }

        @Override
        public Query createAllDocsQuery(@NonNull String name) {
            if (name.length() == 0) {
                return new MatchAllDocsQuery();
            }
            return new FilteredQuery((Query)new MatchAllDocsQuery(), (Filter)new HasFieldFilter(name));
        }

        @Override
        public BooleanQuery createBooleanQuery() {
            return new BooleanQuery();
        }
    }

    private static interface QueryFactory {
        public Query createTermQuery(@NonNull String var1, @NonNull String var2);

        public Query createPrefixQuery(@NonNull String var1, @NonNull String var2);

        public Query createRegExpQuery(@NonNull String var1, @NonNull String var2, boolean var3);

        public Query createAllDocsQuery(@NonNull String var1);

        public BooleanQuery createBooleanQuery();
    }

    private static class TCBooleanQuery
    extends BooleanQuery
    implements TermCollector.TermCollecting {
        private TermCollector collector;

        private TCBooleanQuery() {
        }

        @Override
        public void attach(TermCollector collector) {
            this.collector = collector;
            if (this.collector != null) {
                TCBooleanQuery.attach(this, this.collector);
            }
        }

        public Query rewrite(IndexReader reader) throws IOException {
            Query result = super.rewrite(reader);
            if (this.collector != null) {
                TCBooleanQuery.attach(this, this.collector);
            }
            return result;
        }

        private static void attach(BooleanQuery query, TermCollector collector) {
            for (BooleanClause clause : query.getClauses()) {
                Query q = clause.getQuery();
                if (!(q instanceof TermCollector.TermCollecting)) {
                    throw new IllegalArgumentException();
                }
                ((TermCollector.TermCollecting)q).attach(collector);
            }
        }
    }

    private static class TCFilteredQuery
    extends FilteredQuery
    implements TermCollector.TermCollecting {
        private TCFilteredQuery(Query query, TCFilter filter) {
            super(query, (Filter)filter);
        }

        @Override
        public void attach(TermCollector collector) {
            ((TCFilter)this.getFilter()).attach(collector);
        }
    }

    private static class HasFieldFilter
    extends PrefixFilter {
        public HasFieldFilter(String fieldName) {
            super(fieldName, "");
        }

        @Override
        protected FilteredTermEnum getTermEnum(IndexReader reader) throws IOException {
            return new PrefixTermEnum(reader, this.term){
                private boolean endEnum;

                protected boolean termCompare(Term term) {
                    if (HasFieldFilter.this.term.field() == term.field()) {
                        return true;
                    }
                    this.endEnum = true;
                    return false;
                }

                protected boolean endEnum() {
                    return this.endEnum;
                }
            };
        }

    }

    private static class TermFilter
    extends PrefixFilter {
        public TermFilter(String fieldName, String value) {
            super(fieldName, value);
        }

        @Override
        protected FilteredTermEnum getTermEnum(IndexReader reader) throws IOException {
            return new PrefixTermEnum(reader, this.term){
                private boolean endEnum;

                protected boolean termCompare(Term term) {
                    if (TermFilter.this.term.field() == term.field() && TermFilter.this.term.text().equals(term.text())) {
                        return true;
                    }
                    this.endEnum = true;
                    return false;
                }

                protected boolean endEnum() {
                    return this.endEnum;
                }
            };
        }

    }

    private static class PrefixFilter
    extends AbstractTCFilter {
        protected final Term term;

        public PrefixFilter(@NonNull String fieldName, @NonNull String prefix) {
            super();
            this.term = new Term(fieldName, prefix);
        }

        @Override
        protected FilteredTermEnum getTermEnum(@NonNull IndexReader reader) throws IOException {
            return new PrefixTermEnum(reader, this.term);
        }
    }

    private static class RegexpFilter
    extends AbstractTCFilter {
        private final String fieldName;
        private final String startPrefix;
        private final Pattern pattern;

        public RegexpFilter(String fieldName, String regexp, boolean caseSensitive) {
            super();
            this.fieldName = fieldName;
            this.pattern = caseSensitive ? Pattern.compile(regexp) : Pattern.compile(regexp, 2);
            this.startPrefix = RegexpFilter.getStartText(regexp);
        }

        @Override
        protected FilteredTermEnum getTermEnum(@NonNull IndexReader reader) throws IOException {
            return new RegexpTermEnum(reader, this.fieldName, this.pattern, this.startPrefix);
        }

        private static String getStartText(String regexp) {
            char c;
            if (!Character.isJavaIdentifierStart(regexp.charAt(0))) {
                return "";
            }
            StringBuilder startBuilder = new StringBuilder();
            startBuilder.append(regexp.charAt(0));
            for (int i = 1; i < regexp.length() && Character.isJavaIdentifierPart(c = regexp.charAt(i)); ++i) {
                startBuilder.append(c);
            }
            return startBuilder.toString();
        }
    }

    private static class RegexpTermEnum
    extends FilteredTermEnum {
        private final String fieldName;
        private final String startPrefix;
        private final Pattern pattern;
        private boolean endEnum;

        public RegexpTermEnum(IndexReader in, String fieldName, Pattern pattern, String startPrefix) throws IOException {
            Term term = new Term(fieldName, startPrefix);
            this.fieldName = term.field();
            this.pattern = pattern;
            this.startPrefix = startPrefix;
            this.setEnum(in.terms(term));
        }

        protected boolean termCompare(Term term) {
            String searchText;
            if (this.fieldName == term.field() && (searchText = term.text()).startsWith(this.startPrefix)) {
                return this.pattern.matcher(term.text()).matches();
            }
            this.endEnum = true;
            return false;
        }

        public float difference() {
            return 1.0f;
        }

        protected boolean endEnum() {
            return this.endEnum;
        }
    }

    private static abstract class AbstractTCFilter
    extends TCFilter {
        private TermCollector termCollector;

        private AbstractTCFilter() {
            super();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public final DocIdSet getDocIdSet(IndexReader reader) throws IOException {
            FilteredTermEnum enumerator = this.getTermEnum(reader);
            if (enumerator.term() == null) {
                return DocIdSet.EMPTY_DOCIDSET;
            }
            try {
                OpenBitSet bitSet;
                Term term;
                bitSet = new OpenBitSet((long)reader.maxDoc());
                int[] docs = new int[32];
                int[] freqs = new int[32];
                TermDocs termDocs = reader.termDocs();
                try {
                    while ((term = enumerator.term()) != null) {
                        int count;
                        termDocs.seek(term);
                        while ((count = termDocs.read(docs, freqs)) != 0) {
                            for (int i = 0; i < count; ++i) {
                                bitSet.set((long)docs[i]);
                                if (this.termCollector == null) continue;
                                this.termCollector.add(docs[i], term);
                            }
                        }
                        if (enumerator.next()) continue;
                        break;
                    }
                }
                finally {
                    termDocs.close();
                }
                term = bitSet;
                return term;
            }
            finally {
                enumerator.close();
            }
        }

        @Override
        public final void attach(TermCollector tc) {
            this.termCollector = tc;
        }

        protected abstract FilteredTermEnum getTermEnum(IndexReader var1) throws IOException;
    }

    private static abstract class TCFilter
    extends Filter {
        private TCFilter() {
        }

        public abstract void attach(TermCollector var1);
    }

    public static enum QueryKind {
        EXACT,
        PREFIX,
        CASE_INSENSITIVE_PREFIX,
        CAMEL_CASE,
        CASE_INSENSITIVE_CAMEL_CASE,
        REGEXP,
        CASE_INSENSITIVE_REGEXP;
        

        private QueryKind() {
        }
    }

}

