/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.lucene;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.netbeans.modules.parsing.lucene.Evictable;
import org.netbeans.modules.parsing.lucene.EvictionPolicy;

final class LRUCache<K, V extends Evictable> {
    private final LinkedHashMap<K, V> cache;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public LRUCache(final EvictionPolicy<? super K, ? super V> policy) {
        this.cache = new LinkedHashMap<K, V>(10, 0.75f, true){

            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                boolean evict = policy.shouldEvict(this.size(), eldest.getKey(), (Evictable)eldest.getValue());
                if (evict) {
                    ((Evictable)eldest.getValue()).evicted();
                }
                return evict;
            }
        };
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void put(K key, V evictable) {
        assert (key != null);
        assert (evictable != null);
        this.lock.writeLock().lock();
        try {
            this.cache.put(key, evictable);
        }
        finally {
            this.lock.writeLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public V get(K key) {
        assert (key != null);
        this.lock.readLock().lock();
        try {
            Evictable evictable = (Evictable)this.cache.get(key);
            return (V)evictable;
        }
        finally {
            this.lock.readLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public V remove(K key) {
        assert (key != null);
        this.lock.writeLock().lock();
        try {
            Evictable evictable = (Evictable)this.cache.remove(key);
            return (V)evictable;
        }
        finally {
            this.lock.writeLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString() {
        this.lock.readLock().lock();
        try {
            String string = this.cache.toString();
            return string;
        }
        finally {
            this.lock.readLock().unlock();
        }
    }

}

