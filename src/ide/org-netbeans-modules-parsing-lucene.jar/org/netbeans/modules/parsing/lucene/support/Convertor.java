/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.lucene.support;

public interface Convertor<P, R> {
    public R convert(P var1);
}

