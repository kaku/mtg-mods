/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.lucene.support;

import java.util.Collection;
import org.apache.lucene.document.Document;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;

public interface DocumentIndexCache {
    public boolean addDocument(@NonNull IndexDocument var1);

    public boolean removeDocument(@NonNull String var1);

    public void clear();

    @NonNull
    public Collection<? extends String> getRemovedKeys();

    @NonNull
    public Collection<? extends IndexDocument> getAddedDocuments();

    public static interface WithCustomIndexDocument
    extends DocumentIndexCache {
        @CheckForNull
        public Convertor<IndexDocument, Document> createAddConvertor();

        @CheckForNull
        public Convertor<Document, IndexDocument> createQueryConvertor();
    }

}

