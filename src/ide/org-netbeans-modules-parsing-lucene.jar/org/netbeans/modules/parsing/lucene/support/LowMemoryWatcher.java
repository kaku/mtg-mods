/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.parsing.lucene.support;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public final class LowMemoryWatcher {
    private static final Logger LOG = Logger.getLogger(LowMemoryWatcher.class.getName());
    private static final long LOGGER_RATE = Integer.getInteger(String.format("%s.logger_rate", LowMemoryWatcher.class.getName()), 1000).intValue();
    private static LowMemoryWatcher instance;
    private final Callable<Boolean> strategy = new DefaultStrategy();
    private final AtomicBoolean testEnforcesLowMemory = new AtomicBoolean();

    private LowMemoryWatcher() {
    }

    public boolean isLowMemory() {
        if (this.testEnforcesLowMemory.get()) {
            return true;
        }
        try {
            return this.strategy.call();
        }
        catch (Exception e) {
            Exceptions.printStackTrace((Throwable)e);
            return false;
        }
    }

    public void free() {
        Runtime rt = Runtime.getRuntime();
        rt.gc();
        rt.runFinalization();
        rt.gc();
        rt.gc();
    }

    void setLowMemory(boolean lowMemory) {
        this.testEnforcesLowMemory.set(lowMemory);
    }

    public static synchronized LowMemoryWatcher getInstance() {
        if (instance == null) {
            instance = new LowMemoryWatcher();
        }
        return instance;
    }

    private static class DefaultStrategy
    implements Callable<Boolean> {
        private static final float heapLimit = 0.8f;
        private final MemoryMXBean memBean = ManagementFactory.getMemoryMXBean();
        private volatile long lastTime;

        DefaultStrategy() {
            assert (this.memBean != null);
        }

        @Override
        public Boolean call() throws Exception {
            MemoryUsage usage;
            if (this.memBean != null && (usage = this.memBean.getHeapMemoryUsage()) != null) {
                long max;
                boolean res;
                long now;
                long used = usage.getUsed();
                boolean bl = res = (float)used > (float)(max = usage.getMax()) * 0.8f;
                if (LOG.isLoggable(Level.FINEST) && (now = System.currentTimeMillis()) - this.lastTime > LOGGER_RATE) {
                    LOG.log(Level.FINEST, "Max memory: {0}, Used memory: {1}, Low memory condition: {2}", new Object[]{max, used, res});
                    this.lastTime = now;
                }
                return res;
            }
            return false;
        }
    }

}

