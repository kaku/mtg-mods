/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.analysis.Analyzer
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.index.IndexReader
 *  org.apache.lucene.index.IndexWriter
 *  org.apache.lucene.index.IndexWriter$MaxFieldLength
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.index.TermEnum
 *  org.apache.lucene.search.Collector
 *  org.apache.lucene.search.IndexSearcher
 *  org.apache.lucene.search.Query
 *  org.apache.lucene.store.Directory
 *  org.apache.lucene.store.RAMDirectory
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.lucene;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.AllFieldsSelector;
import org.netbeans.modules.parsing.lucene.BitSetCollector;
import org.netbeans.modules.parsing.lucene.TermCollector;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.StoppableConvertor;
import org.openide.util.Parameters;

public class MemoryIndex
implements Index {
    private final Analyzer analyzer;
    private final ReentrantReadWriteLock lock;
    private RAMDirectory dir;
    private IndexReader cachedReader;

    private MemoryIndex(@NonNull Analyzer analyzer) {
        assert (analyzer != null);
        this.analyzer = analyzer;
        this.lock = new ReentrantReadWriteLock();
    }

    @NonNull
    static MemoryIndex create(@NonNull Analyzer analyzer) {
        return new MemoryIndex(analyzer);
    }

    @NonNull
    @Override
    public Index.Status getStatus(boolean tryOpen) throws IOException {
        return Index.Status.VALID;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public /* varargs */ <T> void query(@NonNull Collection<? super T> result, @NonNull Convertor<? super Document, T> convertor, @NullAllowed FieldSelector selector, @NullAllowed AtomicBoolean cancel, @NonNull Query ... queries) throws IOException, InterruptedException {
        Parameters.notNull((CharSequence)"queries", (Object)queries);
        Parameters.notNull((CharSequence)"convertor", convertor);
        Parameters.notNull((CharSequence)"result", result);
        if (selector == null) {
            selector = AllFieldsSelector.INSTANCE;
        }
        this.lock.readLock().lock();
        try {
            IndexReader in;
            BitSet bs;
            in = this.getReader();
            if (in == null) {
                return;
            }
            bs = new BitSet(in.maxDoc());
            BitSetCollector c = new BitSetCollector(bs);
            IndexSearcher searcher = new IndexSearcher(in);
            try {
                for (Query q : queries) {
                    if (cancel != null && cancel.get()) {
                        throw new InterruptedException();
                    }
                    searcher.search(q, (Collector)c);
                }
            }
            finally {
                searcher.close();
            }
            int docNum = bs.nextSetBit(0);
            while (docNum >= 0) {
                if (cancel != null && cancel.get()) {
                    throw new InterruptedException();
                }
                Document doc = in.document(docNum, selector);
                T value = convertor.convert(doc);
                if (value != null) {
                    result.add(value);
                }
                docNum = bs.nextSetBit(docNum + 1);
            }
        }
        finally {
            this.lock.readLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public /* varargs */ <S, T> void queryDocTerms(@NonNull Map<? super T, Set<S>> result, @NonNull Convertor<? super Document, T> convertor, @NonNull Convertor<? super Term, S> termConvertor, @NullAllowed FieldSelector selector, @NullAllowed AtomicBoolean cancel, @NonNull Query ... queries) throws IOException, InterruptedException {
        Parameters.notNull((CharSequence)"result", result);
        Parameters.notNull((CharSequence)"convertor", convertor);
        Parameters.notNull((CharSequence)"termConvertor", termConvertor);
        Parameters.notNull((CharSequence)"queries", (Object)queries);
        if (selector == null) {
            selector = AllFieldsSelector.INSTANCE;
        }
        this.lock.readLock().lock();
        try {
            IndexReader in;
            TermCollector termCollector;
            BitSet bs;
            in = this.getReader();
            if (in == null) {
                return;
            }
            bs = new BitSet(in.maxDoc());
            BitSetCollector c = new BitSetCollector(bs);
            IndexSearcher searcher = new IndexSearcher(in);
            termCollector = new TermCollector(c);
            try {
                for (Query q : queries) {
                    if (cancel != null && cancel.get()) {
                        throw new InterruptedException();
                    }
                    if (!(q instanceof TermCollector.TermCollecting)) {
                        throw new IllegalArgumentException(String.format("Query: %s does not implement TermCollecting", q.getClass().getName()));
                    }
                    ((TermCollector.TermCollecting)q).attach(termCollector);
                    searcher.search(q, (Collector)termCollector);
                }
            }
            finally {
                searcher.close();
            }
            int docNum = bs.nextSetBit(0);
            while (docNum >= 0) {
                Set<Term> terms;
                if (cancel != null && cancel.get()) {
                    throw new InterruptedException();
                }
                Document doc = in.document(docNum, selector);
                T value = convertor.convert(doc);
                if (value != null && (terms = termCollector.get(docNum)) != null) {
                    result.put(value, MemoryIndex.convertTerms(termConvertor, terms));
                }
                docNum = bs.nextSetBit(docNum + 1);
            }
        }
        finally {
            this.lock.readLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public <T> void queryTerms(@NonNull Collection<? super T> result, @NullAllowed Term start, @NonNull StoppableConvertor<Term, T> filter, @NullAllowed AtomicBoolean cancel) throws IOException, InterruptedException {
        Parameters.notNull((CharSequence)"result", result);
        Parameters.notNull((CharSequence)"filter", filter);
        this.lock.readLock().lock();
        try {
            IndexReader in = this.getReader();
            if (in == null) {
                return;
            }
            TermEnum terms = start == null ? in.terms() : in.terms(start);
            try {
                do {
                    T vote;
                    if (cancel != null && cancel.get()) {
                        throw new InterruptedException();
                    }
                    Term currentTerm = terms.term();
                    if (currentTerm == null || (vote = filter.convert(currentTerm)) == null) continue;
                    result.add(vote);
                } while (terms.next());
            }
            catch (StoppableConvertor.Stop stop) {}
            finally {
                terms.close();
            }
        }
        finally {
            this.lock.readLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public <S, T> void store(Collection<T> toAdd, Collection<S> toDelete, Convertor<? super T, ? extends Document> docConvertor, Convertor<? super S, ? extends Query> queryConvertor, boolean optimize) throws IOException {
        this.lock.writeLock().lock();
        try {
            IndexWriter out = this.getWriter();
            try {
                for (S td : toDelete) {
                    out.deleteDocuments(queryConvertor.convert(td));
                }
                if (toAdd.isEmpty()) {
                    return;
                }
                Iterator<T> it = toAdd.iterator();
                while (it.hasNext()) {
                    T entry = it.next();
                    it.remove();
                    Document doc = docConvertor.convert(entry);
                    out.addDocument(doc);
                }
            }
            finally {
                try {
                    out.close();
                }
                finally {
                    this.refreshReader();
                }
            }
        }
        finally {
            this.lock.writeLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void clear() throws IOException {
        this.lock.writeLock().lock();
        this.close();
        try {
            MemoryIndex memoryIndex = this;
            synchronized (memoryIndex) {
                if (this.dir != null) {
                    this.dir.close();
                    this.dir = null;
                }
            }
        }
        finally {
            this.lock.writeLock().unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void close() throws IOException {
        this.lock.writeLock().lock();
        try {
            MemoryIndex memoryIndex = this;
            synchronized (memoryIndex) {
                if (this.cachedReader != null) {
                    this.cachedReader.close();
                    this.cachedReader = null;
                }
            }
        }
        finally {
            this.lock.writeLock().unlock();
        }
    }

    @CheckForNull
    private synchronized IndexReader getReader() throws IOException {
        if (this.cachedReader == null) {
            try {
                this.cachedReader = IndexReader.open((Directory)this.getDirectory(), (boolean)true);
            }
            catch (FileNotFoundException fnf) {
                // empty catch block
            }
        }
        return this.cachedReader;
    }

    private synchronized void refreshReader() throws IOException {
        IndexReader newReader;
        assert (this.lock.isWriteLockedByCurrentThread());
        if (this.cachedReader != null && (newReader = this.cachedReader.reopen()) != this.cachedReader) {
            this.cachedReader.close();
            this.cachedReader = newReader;
        }
    }

    private synchronized IndexWriter getWriter() throws IOException {
        return new IndexWriter(this.getDirectory(), this.analyzer, IndexWriter.MaxFieldLength.LIMITED);
    }

    private synchronized Directory getDirectory() {
        if (this.dir == null) {
            this.dir = new RAMDirectory();
        }
        return this.dir;
    }

    private static <T> Set<T> convertTerms(Convertor<? super Term, T> convertor, Set<? extends Term> terms) {
        HashSet<T> result = new HashSet<T>(terms.size());
        for (Term term : terms) {
            result.add(convertor.convert(term));
        }
        return result;
    }
}

