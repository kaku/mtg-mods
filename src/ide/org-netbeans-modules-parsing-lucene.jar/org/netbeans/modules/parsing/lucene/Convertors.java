/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.index.IndexReader
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.index.TermEnum
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.lucene;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.search.Query;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.lucene.IndexDocumentImpl;
import org.netbeans.modules.parsing.lucene.SupportAccessor;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.IndexReaderInjection;
import org.netbeans.modules.parsing.lucene.support.StoppableConvertor;

class Convertors {
    private Convertors() {
        throw new IllegalStateException();
    }

    static Convertor<IndexDocument, Document> newIndexDocumentToDocumentConvertor() {
        return new AddConvertor();
    }

    static Convertor<Document, IndexDocumentImpl> newDocumentToIndexDocumentConvertor() {
        return new QueryConvertor();
    }

    static Convertor<String, Query> newSourceNameToQueryConvertor() {
        return new RemoveConvertor();
    }

    static <T> StoppableConvertor<TermEnum, T> newTermEnumToTermConvertor(@NonNull StoppableConvertor<Term, T> delegate) {
        return new TermEnumToTerm<T>(delegate);
    }

    static <T> StoppableConvertor<TermEnum, T> newTermEnumToFreqConvertor(@NonNull StoppableConvertor<Index.WithTermFrequencies.TermFreq, T> delegate) {
        return new TermEnumToFreq<T>(delegate);
    }

    private static class TermEnumToFreq<T>
    implements StoppableConvertor<TermEnum, T>,
    IndexReaderInjection {
        private final SupportAccessor accessor = SupportAccessor.getInstance();
        private final Index.WithTermFrequencies.TermFreq tf = this.accessor.newTermFreq();
        private final StoppableConvertor<Index.WithTermFrequencies.TermFreq, T> delegate;

        TermEnumToFreq(@NonNull StoppableConvertor<Index.WithTermFrequencies.TermFreq, T> convertor) {
            this.delegate = convertor;
        }

        @Override
        public T convert(TermEnum terms) throws StoppableConvertor.Stop {
            Term currentTerm = terms.term();
            if (currentTerm == null) {
                return null;
            }
            int freq = terms.docFreq();
            return this.delegate.convert(this.accessor.setTermFreq(this.tf, currentTerm, freq));
        }

        @Override
        public void setIndexReader(@NonNull IndexReader indexReader) {
            if (this.delegate instanceof IndexReaderInjection) {
                ((IndexReaderInjection)((Object)this.delegate)).setIndexReader(indexReader);
            }
        }
    }

    private static class TermEnumToTerm<T>
    implements StoppableConvertor<TermEnum, T>,
    IndexReaderInjection {
        private final StoppableConvertor<Term, T> delegate;

        TermEnumToTerm(@NonNull StoppableConvertor<Term, T> convertor) {
            this.delegate = convertor;
        }

        @Override
        public T convert(@NonNull TermEnum terms) throws StoppableConvertor.Stop {
            Term currentTerm = terms.term();
            if (currentTerm == null) {
                return null;
            }
            return this.delegate.convert(currentTerm);
        }

        @Override
        public void setIndexReader(@NonNull IndexReader indexReader) {
            if (this.delegate instanceof IndexReaderInjection) {
                ((IndexReaderInjection)((Object)this.delegate)).setIndexReader(indexReader);
            }
        }
    }

    private static final class QueryConvertor
    implements Convertor<Document, IndexDocumentImpl> {
        private QueryConvertor() {
        }

        @Override
        public IndexDocumentImpl convert(Document p) {
            return new IndexDocumentImpl(p);
        }
    }

    private static final class RemoveConvertor
    implements Convertor<String, Query> {
        private RemoveConvertor() {
        }

        @Override
        public Query convert(String p) {
            return IndexDocumentImpl.sourceNameQuery(p);
        }
    }

    private static final class AddConvertor
    implements Convertor<IndexDocument, Document> {
        private AddConvertor() {
        }

        @Override
        public Document convert(IndexDocument p) {
            return ((IndexDocumentImpl)p).doc;
        }
    }

}

