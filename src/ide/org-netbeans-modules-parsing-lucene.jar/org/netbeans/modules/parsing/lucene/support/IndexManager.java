/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.analysis.Analyzer
 *  org.apache.lucene.analysis.KeywordAnalyzer
 *  org.apache.lucene.index.Term
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.modules.masterfs.providers.ProvidedExtensions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Parameters
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.parsing.lucene.support;

import java.io.File;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.index.Term;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.masterfs.providers.ProvidedExtensions;
import org.netbeans.modules.parsing.lucene.DocumentIndexImpl;
import org.netbeans.modules.parsing.lucene.IndexDocumentImpl;
import org.netbeans.modules.parsing.lucene.IndexFactory;
import org.netbeans.modules.parsing.lucene.LuceneIndexFactory;
import org.netbeans.modules.parsing.lucene.SimpleDocumentIndexCache;
import org.netbeans.modules.parsing.lucene.SupportAccessor;
import org.netbeans.modules.parsing.lucene.spi.ScanSuspendImplementation;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Parameters;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public final class IndexManager {
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final Lookup.Result<? extends ScanSuspendImplementation> res = Lookup.getDefault().lookupResult(ScanSuspendImplementation.class);
    private static final LookupListener lookupListener = new LookupListener(){

        public void resultChanged(LookupEvent ev) {
            scanSuspendImpls = null;
        }
    };
    private static volatile Collection<? extends ScanSuspendImplementation> scanSuspendImpls;
    static IndexFactory factory;
    private static final Map<File, Reference<Index>> indexes;

    private IndexManager() {
    }

    @Deprecated
    public static <R> R writeAccess(final Action<R> action) throws IOException, InterruptedException {
        assert (action != null);
        lock.writeLock().lock();
        try {
            Object object = ProvidedExtensions.priorityIO((Callable)new Callable<R>(){

                @Override
                public R call() throws Exception {
                    return action.run();
                }
            });
            return (R)object;
        }
        catch (IOException ioe) {
            throw ioe;
        }
        catch (InterruptedException ie) {
            throw ie;
        }
        catch (RuntimeException re) {
            throw re;
        }
        catch (Exception e) {
            throw new IOException(e);
        }
        finally {
            lock.writeLock().unlock();
        }
    }

    /*
     * Exception decompiling
     */
    @Deprecated
    public static <R> R readAccess(Action<R> action) throws IOException, InterruptedException {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [0[TRYBLOCK]], but top level block is 4[CATCHBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:397)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:449)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:2877)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:825)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:217)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:162)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:95)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:355)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:768)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:700)
        // org.benf.cfr.reader.Main.doJar(Main.java:134)
        // org.benf.cfr.reader.Main.main(Main.java:189)
        throw new IllegalStateException("Decompilation failed");
    }

    public static <R> R priorityAccess(final Action<R> action) throws IOException, InterruptedException {
        assert (action != null);
        IndexManager.suspend();
        try {
            Object object = ProvidedExtensions.priorityIO((Callable)new Callable<R>(){

                @Override
                public R call() throws Exception {
                    return action.run();
                }
            });
            return (R)object;
        }
        catch (IOException ioe) {
            throw ioe;
        }
        catch (InterruptedException ie) {
            throw ie;
        }
        catch (RuntimeException re) {
            throw re;
        }
        catch (Exception e) {
            throw new IOException(e);
        }
        finally {
            IndexManager.resume();
        }
    }

    public static boolean holdsWriteLock() {
        return lock.isWriteLockedByCurrentThread();
    }

    @NonNull
    public static Index createIndex(@NonNull File cacheFolder, @NonNull Analyzer analyzer) throws IOException {
        return IndexManager.createTransactionalIndex(cacheFolder, analyzer);
    }

    @NonNull
    public static Index createIndex(@NonNull File cacheFolder, @NonNull Analyzer analyzer, boolean isWritable) throws IOException {
        return IndexManager.createTransactionalIndex(cacheFolder, analyzer, isWritable);
    }

    @NonNull
    public static Index.Transactional createTransactionalIndex(@NonNull File cacheFolder, @NonNull Analyzer analyzer, boolean isWritable) throws IOException {
        Parameters.notNull((CharSequence)"cacheFolder", (Object)cacheFolder);
        Parameters.notNull((CharSequence)"analyzer", (Object)analyzer);
        if (!cacheFolder.canRead()) {
            throw new IOException(String.format("Cannot read cache folder: %s.", cacheFolder.getAbsolutePath()));
        }
        if (isWritable && !cacheFolder.canWrite()) {
            throw new IOException(String.format("Cannot write to cache folder: %s.", cacheFolder.getAbsolutePath()));
        }
        Index.Transactional index = factory.createIndex(cacheFolder, analyzer);
        assert (index != null);
        indexes.put(cacheFolder, new Ref(cacheFolder, index));
        return index;
    }

    @NonNull
    public static Index.Transactional createTransactionalIndex(@NonNull File cacheFolder, @NonNull Analyzer analyzer) throws IOException {
        return IndexManager.createTransactionalIndex(cacheFolder, analyzer, true);
    }

    public static Index createMemoryIndex(@NonNull Analyzer analyzer) throws IOException {
        Parameters.notNull((CharSequence)"analyzer", (Object)analyzer);
        Index index = factory.createMemoryIndex(analyzer);
        assert (index != null);
        return index;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @NonNull
    public static Map<File, Index> getOpenIndexes() {
        HashMap<File, Index> result = new HashMap<File, Index>();
        Map<File, Reference<Index>> map = indexes;
        synchronized (map) {
            for (Map.Entry<File, Reference<Index>> e : indexes.entrySet()) {
                File folder = e.getKey();
                Index index = e.getValue().get();
                if (index == null) continue;
                result.put(folder, index);
            }
        }
        return Collections.unmodifiableMap(result);
    }

    public static DocumentIndex createDocumentIndex(@NonNull Index index) {
        Parameters.notNull((CharSequence)"index", (Object)index);
        return IndexManager.createDocumentIndex(index, (DocumentIndexCache)new SimpleDocumentIndexCache());
    }

    public static DocumentIndex createDocumentIndex(@NonNull Index index, @NonNull DocumentIndexCache cache) {
        Parameters.notNull((CharSequence)"index", (Object)index);
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        return DocumentIndexImpl.create(index, cache);
    }

    public static DocumentIndex createDocumentIndex(@NonNull File cacheFolder, boolean isWritable) throws IOException {
        Parameters.notNull((CharSequence)"cacheFolder", (Object)cacheFolder);
        return IndexManager.createDocumentIndex(IndexManager.createIndex(cacheFolder, (Analyzer)new KeywordAnalyzer(), isWritable));
    }

    public static DocumentIndex createDocumentIndex(@NonNull File cacheFolder) throws IOException {
        Parameters.notNull((CharSequence)"cacheFolder", (Object)cacheFolder);
        return IndexManager.createDocumentIndex(IndexManager.createIndex(cacheFolder, (Analyzer)new KeywordAnalyzer()));
    }

    public static DocumentIndex createDocumentIndex(@NonNull File cacheFolder, @NonNull DocumentIndexCache cache) throws IOException {
        Parameters.notNull((CharSequence)"cacheFolder", (Object)cacheFolder);
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        return IndexManager.createDocumentIndex(IndexManager.createIndex(cacheFolder, (Analyzer)new KeywordAnalyzer()), cache);
    }

    @NonNull
    public static DocumentIndex.Transactional createTransactionalDocumentIndex(@NonNull Index.Transactional index) {
        return IndexManager.createTransactionalDocumentIndex(index, (DocumentIndexCache)new SimpleDocumentIndexCache());
    }

    @NonNull
    public static DocumentIndex.Transactional createTransactionalDocumentIndex(@NonNull Index.Transactional index, @NonNull DocumentIndexCache cache) {
        Parameters.notNull((CharSequence)"index", (Object)index);
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        return DocumentIndexImpl.createTransactional(index, cache);
    }

    @NonNull
    public static DocumentIndex.Transactional createTransactionalDocumentIndex(@NonNull File cacheFolder) throws IOException {
        Parameters.notNull((CharSequence)"cacheFolder", (Object)cacheFolder);
        return IndexManager.createTransactionalDocumentIndex(cacheFolder, (DocumentIndexCache)new SimpleDocumentIndexCache());
    }

    @NonNull
    public static DocumentIndex.Transactional createTransactionalDocumentIndex(@NonNull File cacheFolder, @NonNull DocumentIndexCache cache) throws IOException {
        Parameters.notNull((CharSequence)"cacheFolder", (Object)cacheFolder);
        Parameters.notNull((CharSequence)"cache", (Object)cache);
        return IndexManager.createTransactionalDocumentIndex(IndexManager.createTransactionalIndex(cacheFolder, (Analyzer)new KeywordAnalyzer()), cache);
    }

    public static IndexDocument createDocument(@NonNull String primaryKey) {
        Parameters.notNull((CharSequence)"primaryKey", (Object)primaryKey);
        return new IndexDocumentImpl(primaryKey);
    }

    private static void suspend() {
        for (ScanSuspendImplementation impl : IndexManager.getScanSuspendImpls()) {
            impl.suspend();
        }
    }

    private static void resume() {
        for (ScanSuspendImplementation impl : IndexManager.getScanSuspendImpls()) {
            impl.resume();
        }
    }

    @NonNull
    private static Collection<? extends ScanSuspendImplementation> getScanSuspendImpls() {
        Collection<? extends ScanSuspendImplementation> result = scanSuspendImpls;
        if (result == null) {
            scanSuspendImpls = result = new ArrayList<ScanSuspendImplementation>(res.allInstances());
        }
        return result;
    }

    static {
        factory = new LuceneIndexFactory();
        SupportAccessor.setInstance(new SupportAccessorImpl());
        res.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)lookupListener, res));
        indexes = Collections.synchronizedMap(new HashMap());
    }

    private static class SupportAccessorImpl
    extends SupportAccessor {
        private SupportAccessorImpl() {
        }

        @NonNull
        @Override
        public Index.WithTermFrequencies.TermFreq newTermFreq() {
            return new Index.WithTermFrequencies.TermFreq();
        }

        @Override
        public Index.WithTermFrequencies.TermFreq setTermFreq(@NonNull Index.WithTermFrequencies.TermFreq into, @NonNull Term term, int freq) {
            into.setTerm(term);
            into.setFreq(freq);
            return into;
        }
    }

    private static class Ref
    extends WeakReference<Index>
    implements Runnable {
        private final File folder;

        Ref(@NonNull File folder, @NonNull Index index) {
            super(index, Utilities.activeReferenceQueue());
            this.folder = folder;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            Map map = indexes;
            synchronized (map) {
                if (indexes.get(this.folder) == this) {
                    indexes.remove(this.folder);
                }
            }
        }
    }

    public static interface Action<R> {
        public R run() throws IOException, InterruptedException;
    }

}

