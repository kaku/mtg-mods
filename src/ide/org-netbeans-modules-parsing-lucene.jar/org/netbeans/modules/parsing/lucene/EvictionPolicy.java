/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.lucene;

import org.netbeans.modules.parsing.lucene.Evictable;

public interface EvictionPolicy<K, V extends Evictable> {
    public boolean shouldEvict(int var1, K var2, V var3);
}

