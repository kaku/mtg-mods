/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.analysis.Analyzer
 */
package org.netbeans.modules.parsing.lucene;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;
import org.netbeans.modules.parsing.lucene.IndexFactory;
import org.netbeans.modules.parsing.lucene.LuceneIndex;
import org.netbeans.modules.parsing.lucene.MemoryIndex;
import org.netbeans.modules.parsing.lucene.support.Index;

public class LuceneIndexFactory
implements IndexFactory {
    @Override
    public Index.Transactional createIndex(File cacheFolder, Analyzer analyzer) throws IOException {
        return LuceneIndex.create(cacheFolder, analyzer);
    }

    @Override
    public Index createMemoryIndex(Analyzer analyzer) throws IOException {
        return MemoryIndex.create(analyzer);
    }
}

