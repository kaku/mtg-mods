/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.parsing.lucene.support;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.StoppableConvertor;

public interface Index {
    public Status getStatus(boolean var1) throws IOException;

    public /* varargs */ <T> void query(Collection<? super T> var1, @NonNull Convertor<? super Document, T> var2, @NullAllowed FieldSelector var3, @NullAllowed AtomicBoolean var4, @NonNull Query ... var5) throws IOException, InterruptedException;

    public /* varargs */ <S, T> void queryDocTerms(Map<? super T, Set<S>> var1, @NonNull Convertor<? super Document, T> var2, @NonNull Convertor<? super Term, S> var3, @NullAllowed FieldSelector var4, @NullAllowed AtomicBoolean var5, @NonNull Query ... var6) throws IOException, InterruptedException;

    public <T> void queryTerms(@NonNull Collection<? super T> var1, @NullAllowed Term var2, @NonNull StoppableConvertor<Term, T> var3, @NullAllowed AtomicBoolean var4) throws IOException, InterruptedException;

    public <S, T> void store(@NonNull Collection<T> var1, @NonNull Collection<S> var2, @NonNull Convertor<? super T, ? extends Document> var3, @NonNull Convertor<? super S, ? extends Query> var4, boolean var5) throws IOException;

    public void clear() throws IOException;

    public void close() throws IOException;

    public static interface WithTermFrequencies
    extends Index {
        public <T> void queryTermFrequencies(@NonNull Collection<? super T> var1, @NullAllowed Term var2, @NonNull StoppableConvertor<TermFreq, T> var3, @NullAllowed AtomicBoolean var4) throws IOException, InterruptedException;

        public static final class TermFreq {
            private int freq;
            private Term term;

            TermFreq() {
            }

            void setTerm(@NonNull Term term) {
                this.term = term;
            }

            void setFreq(int freq) {
                this.freq = freq;
            }

            @NonNull
            public Term getTerm() {
                return this.term;
            }

            public int getFreq() {
                return this.freq;
            }
        }

    }

    public static interface Transactional
    extends Index {
        public <S, T> void txStore(@NonNull Collection<T> var1, @NonNull Collection<S> var2, @NonNull Convertor<? super T, ? extends Document> var3, @NonNull Convertor<? super S, ? extends Query> var4) throws IOException;

        public void commit() throws IOException;

        public void rollback() throws IOException;
    }

    public static final class IndexClosedException
    extends IOException {
    }

    public static enum Status {
        EMPTY,
        INVALID,
        VALID,
        WRITING;
        

        private Status() {
        }
    }

}

