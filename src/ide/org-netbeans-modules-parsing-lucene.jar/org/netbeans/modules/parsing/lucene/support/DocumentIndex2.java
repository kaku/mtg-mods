/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.parsing.lucene.support;

import java.io.IOException;
import java.util.Collection;
import org.apache.lucene.search.Query;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;

public interface DocumentIndex2
extends DocumentIndex {
    @NonNull
    public /* varargs */ <T> Collection<? extends T> query(@NonNull Query var1, @NonNull Convertor<? super IndexDocument, ? extends T> var2, @NullAllowed String ... var3) throws IOException, InterruptedException;

    public static interface Transactional
    extends DocumentIndex2,
    DocumentIndex.Transactional {
    }

}

