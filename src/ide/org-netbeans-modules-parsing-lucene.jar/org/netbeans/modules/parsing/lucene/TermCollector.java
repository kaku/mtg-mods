/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.index.IndexReader
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.search.Collector
 *  org.apache.lucene.search.Scorer
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.lucene;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;
import org.netbeans.api.annotations.common.NonNull;

public final class TermCollector
extends Collector {
    private final Collector delegate;
    private final Map<Integer, Set<Term>> doc2Terms;
    private int indexOffset;

    TermCollector(Collector collector) {
        this.delegate = collector;
        this.doc2Terms = new HashMap<Integer, Set<Term>>();
    }

    public void add(int docId, @NonNull Term term) {
        int realId = docId + this.indexOffset;
        Set<Term> slot = this.doc2Terms.get(realId);
        if (slot == null) {
            slot = new HashSet<Term>();
            this.doc2Terms.put(realId, slot);
        }
        slot.add(term);
    }

    Set<Term> get(int docId) {
        return this.doc2Terms.get(docId);
    }

    Set<? extends Integer> docs() {
        return Collections.unmodifiableSet(this.doc2Terms.keySet());
    }

    public void setScorer(Scorer scorer) throws IOException {
        this.delegate.setScorer(scorer);
    }

    public void setNextReader(IndexReader reader, int i) throws IOException {
        this.delegate.setNextReader(reader, i);
        this.indexOffset = i;
    }

    public void collect(int i) throws IOException {
        this.delegate.collect(i);
    }

    public boolean acceptsDocsOutOfOrder() {
        return this.delegate.acceptsDocsOutOfOrder();
    }

    public static interface TermCollecting {
        public void attach(TermCollector var1);
    }

}

