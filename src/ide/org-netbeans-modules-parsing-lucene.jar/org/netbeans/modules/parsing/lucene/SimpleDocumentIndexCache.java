/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.lucene;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.netbeans.modules.parsing.lucene.DocumentIndexImpl;
import org.netbeans.modules.parsing.lucene.IndexDocumentImpl;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;

public class SimpleDocumentIndexCache
implements DocumentIndexCache {
    private boolean disableCache = Boolean.getBoolean("test." + DocumentIndexImpl.class.getName() + ".cacheDisable");
    private List<IndexDocument> toAdd;
    private List<String> toRemove;
    private Reference<List[]> dataRef;

    @Override
    public boolean addDocument(IndexDocument document) {
        if (!(document instanceof IndexDocumentImpl)) {
            throw new IllegalArgumentException(document.getClass().getName());
        }
        Reference<List[]> ref = this.getDataRef();
        assert (ref != null);
        boolean shouldFlush = this.disableCache || ref.get() == null;
        this.toAdd.add(document);
        this.toRemove.add(document.getPrimaryKey());
        return shouldFlush;
    }

    @Override
    public boolean removeDocument(String primaryKey) {
        Reference<List[]> ref = this.getDataRef();
        assert (ref != null);
        boolean shouldFlush = ref.get() == null;
        this.toRemove.add(primaryKey);
        return shouldFlush;
    }

    @Override
    public void clear() {
        this.toAdd = null;
        this.toRemove = null;
        this.dataRef = null;
    }

    @Override
    public Collection<? extends String> getRemovedKeys() {
        return this.toRemove != null ? this.toRemove : Collections.emptySet();
    }

    @Override
    public Collection<? extends IndexDocument> getAddedDocuments() {
        return this.toAdd != null ? this.toAdd : Collections.emptySet();
    }

    void testClearDataRef() {
        this.dataRef.clear();
    }

    private Reference<List[]> getDataRef() {
        if (this.toAdd == null || this.toRemove == null) {
            assert (this.toAdd == null && this.toRemove == null);
            assert (this.dataRef == null);
            this.toAdd = new ArrayList<IndexDocument>();
            this.toRemove = new ArrayList<String>();
            this.dataRef = new SoftReference<List[]>(new List[]{this.toAdd, this.toRemove});
        }
        return this.dataRef;
    }
}

