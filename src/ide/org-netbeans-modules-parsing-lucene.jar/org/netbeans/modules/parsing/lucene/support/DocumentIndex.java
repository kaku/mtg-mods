/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 */
package org.netbeans.modules.parsing.lucene.support;

import java.io.IOException;
import java.util.Collection;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.Queries;

public interface DocumentIndex {
    public void addDocument(@NonNull IndexDocument var1);

    public void removeDocument(@NonNull String var1);

    public Index.Status getStatus() throws IOException;

    public void close() throws IOException;

    public void store(boolean var1) throws IOException;

    @NonNull
    public /* varargs */ Collection<? extends IndexDocument> query(@NonNull String var1, @NonNull String var2, @NonNull Queries.QueryKind var3, @NullAllowed String ... var4) throws IOException, InterruptedException;

    @NonNull
    public /* varargs */ Collection<? extends IndexDocument> findByPrimaryKey(@NonNull String var1, @NonNull Queries.QueryKind var2, @NullAllowed String ... var3) throws IOException, InterruptedException;

    public void markKeyDirty(@NonNull String var1);

    public void removeDirtyKeys(@NonNull Collection<? extends String> var1);

    @NonNull
    public Collection<? extends String> getDirtyKeys();

    public static interface Transactional
    extends DocumentIndex {
        public void txStore() throws IOException;

        public void commit() throws IOException;

        public void rollback() throws IOException;

        public void clear() throws IOException;
    }

}

