/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.LucenePackage
 *  org.apache.lucene.analysis.Analyzer
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.index.CorruptIndexException
 *  org.apache.lucene.index.IndexReader
 *  org.apache.lucene.index.IndexWriter
 *  org.apache.lucene.index.IndexWriterConfig
 *  org.apache.lucene.index.MergePolicy
 *  org.apache.lucene.index.Term
 *  org.apache.lucene.index.TermEnum
 *  org.apache.lucene.index.TieredMergePolicy
 *  org.apache.lucene.search.Collector
 *  org.apache.lucene.search.IndexSearcher
 *  org.apache.lucene.search.Query
 *  org.apache.lucene.store.Directory
 *  org.apache.lucene.store.FSDirectory
 *  org.apache.lucene.store.Lock
 *  org.apache.lucene.store.LockFactory
 *  org.apache.lucene.store.LockObtainFailedException
 *  org.apache.lucene.store.MMapDirectory
 *  org.apache.lucene.store.NIOFSDirectory
 *  org.apache.lucene.store.RAMDirectory
 *  org.apache.lucene.store.SimpleFSDirectory
 *  org.apache.lucene.util.Version
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Exceptions
 *  org.openide.util.Pair
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.parsing.lucene;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.LucenePackage;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.MergePolicy;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.index.TieredMergePolicy;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.Lock;
import org.apache.lucene.store.LockFactory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.AllFieldsSelector;
import org.netbeans.modules.parsing.lucene.BitSetCollector;
import org.netbeans.modules.parsing.lucene.Convertors;
import org.netbeans.modules.parsing.lucene.Evictable;
import org.netbeans.modules.parsing.lucene.IndexCacheFactory;
import org.netbeans.modules.parsing.lucene.LRUCache;
import org.netbeans.modules.parsing.lucene.RecordOwnerLockFactory;
import org.netbeans.modules.parsing.lucene.TermCollector;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexReaderInjection;
import org.netbeans.modules.parsing.lucene.support.LowMemoryWatcher;
import org.netbeans.modules.parsing.lucene.support.StoppableConvertor;
import org.openide.util.Exceptions;
import org.openide.util.Pair;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class LuceneIndex
implements Index.Transactional,
Index.WithTermFrequencies,
Runnable {
    private static final String PROP_INDEX_POLICY = "java.index.useMemCache";
    private static final String PROP_DIR_TYPE = "java.index.dir";
    private static final String DIR_TYPE_MMAP = "mmap";
    private static final String DIR_TYPE_NIO = "nio";
    private static final String DIR_TYPE_IO = "io";
    private static final CachePolicy DEFAULT_CACHE_POLICY = CachePolicy.DYNAMIC;
    private static final CachePolicy cachePolicy = LuceneIndex.getCachePolicy();
    private static final Logger LOGGER = Logger.getLogger(LuceneIndex.class.getName());
    private static boolean disableLocks;
    private final DirCache dirCache;

    public static LuceneIndex create(File cacheRoot, Analyzer analyzer) throws IOException {
        return new LuceneIndex(cacheRoot, analyzer);
    }

    private LuceneIndex(File refCacheRoot, Analyzer analyzer) throws IOException {
        assert (refCacheRoot != null);
        assert (analyzer != null);
        this.dirCache = new DirCache(refCacheRoot, cachePolicy, analyzer);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public /* varargs */ <T> void query(@NonNull Collection<? super T> result, @NonNull Convertor<? super Document, T> convertor, @NullAllowed FieldSelector selector, @NullAllowed AtomicBoolean cancel, @NonNull Query ... queries) throws IOException, InterruptedException {
        Parameters.notNull((CharSequence)"queries", (Object)queries);
        Parameters.notNull((CharSequence)"convertor", convertor);
        Parameters.notNull((CharSequence)"result", result);
        if (selector == null) {
            selector = AllFieldsSelector.INSTANCE;
        }
        IndexReader in = null;
        try {
            BitSet bs;
            in = this.dirCache.acquireReader();
            if (in == null) {
                LOGGER.log(Level.FINE, "{0} is invalid!", this);
                return;
            }
            bs = new BitSet(in.maxDoc());
            BitSetCollector c = new BitSetCollector(bs);
            IndexSearcher searcher = new IndexSearcher(in);
            try {
                for (Query q : queries) {
                    if (cancel != null && cancel.get()) {
                        throw new InterruptedException();
                    }
                    searcher.search(q, (Collector)c);
                }
            }
            finally {
                searcher.close();
            }
            if (convertor instanceof IndexReaderInjection) {
                ((IndexReaderInjection)((Object)convertor)).setIndexReader(in);
            }
            try {
                int docNum = bs.nextSetBit(0);
                while (docNum >= 0) {
                    if (cancel != null && cancel.get()) {
                        throw new InterruptedException();
                    }
                    Document doc = in.document(docNum, selector);
                    T value = convertor.convert(doc);
                    if (value != null) {
                        result.add(value);
                    }
                    docNum = bs.nextSetBit(docNum + 1);
                }
            }
            finally {
                if (convertor instanceof IndexReaderInjection) {
                    ((IndexReaderInjection)((Object)convertor)).setIndexReader(null);
                }
            }
        }
        finally {
            this.dirCache.releaseReader(in);
        }
    }

    @Override
    public <T> void queryTerms(@NonNull Collection<? super T> result, @NullAllowed Term seekTo, @NonNull StoppableConvertor<Term, T> filter, @NullAllowed AtomicBoolean cancel) throws IOException, InterruptedException {
        this.queryTermsImpl(result, seekTo, Convertors.newTermEnumToTermConvertor(filter), cancel);
    }

    @Override
    public <T> void queryTermFrequencies(@NonNull Collection<? super T> result, @NullAllowed Term seekTo, @NonNull StoppableConvertor<Index.WithTermFrequencies.TermFreq, T> filter, @NullAllowed AtomicBoolean cancel) throws IOException, InterruptedException {
        this.queryTermsImpl(result, seekTo, Convertors.newTermEnumToFreqConvertor(filter), cancel);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private <T> void queryTermsImpl(@NonNull Collection<? super T> result, @NullAllowed Term seekTo, @NonNull StoppableConvertor<TermEnum, T> adapter, @NullAllowed AtomicBoolean cancel) throws IOException, InterruptedException {
        IndexReader in = null;
        try {
            in = this.dirCache.acquireReader();
            if (in == null) {
                return;
            }
            TermEnum terms = seekTo == null ? in.terms() : in.terms(seekTo);
            try {
                if (adapter instanceof IndexReaderInjection) {
                    ((IndexReaderInjection)((Object)adapter)).setIndexReader(in);
                }
                try {
                    do {
                        if (cancel != null && cancel.get()) {
                            throw new InterruptedException();
                        }
                        T vote = adapter.convert(terms);
                        if (vote == null) continue;
                        result.add(vote);
                    } while (terms.next());
                }
                catch (StoppableConvertor.Stop stop) {}
                finally {
                    if (adapter instanceof IndexReaderInjection) {
                        ((IndexReaderInjection)((Object)adapter)).setIndexReader(null);
                    }
                }
            }
            finally {
                terms.close();
            }
        }
        finally {
            this.dirCache.releaseReader(in);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public /* varargs */ <S, T> void queryDocTerms(@NonNull Map<? super T, Set<S>> result, @NonNull Convertor<? super Document, T> convertor, @NonNull Convertor<? super Term, S> termConvertor, @NullAllowed FieldSelector selector, @NullAllowed AtomicBoolean cancel, @NonNull Query ... queries) throws IOException, InterruptedException {
        Parameters.notNull((CharSequence)"queries", (Object)queries);
        Parameters.notNull((CharSequence)"slector", (Object)selector);
        Parameters.notNull((CharSequence)"convertor", convertor);
        Parameters.notNull((CharSequence)"termConvertor", termConvertor);
        Parameters.notNull((CharSequence)"result", result);
        if (selector == null) {
            selector = AllFieldsSelector.INSTANCE;
        }
        IndexReader in = null;
        try {
            TermCollector termCollector;
            BitSet bs;
            in = this.dirCache.acquireReader();
            if (in == null) {
                LOGGER.fine(String.format("LuceneIndex[%s] is invalid!\n", this.toString()));
                return;
            }
            bs = new BitSet(in.maxDoc());
            BitSetCollector c = new BitSetCollector(bs);
            IndexSearcher searcher = new IndexSearcher(in);
            termCollector = new TermCollector(c);
            try {
                for (Query q : queries) {
                    if (cancel != null && cancel.get()) {
                        throw new InterruptedException();
                    }
                    if (!(q instanceof TermCollector.TermCollecting)) {
                        throw new IllegalArgumentException(String.format("Query: %s does not implement TermCollecting", q.getClass().getName()));
                    }
                    ((TermCollector.TermCollecting)q).attach(termCollector);
                    searcher.search(q, (Collector)termCollector);
                }
            }
            finally {
                searcher.close();
            }
            boolean logged = false;
            if (convertor instanceof IndexReaderInjection) {
                ((IndexReaderInjection)((Object)convertor)).setIndexReader(in);
            }
            try {
                if (termConvertor instanceof IndexReaderInjection) {
                    ((IndexReaderInjection)((Object)termConvertor)).setIndexReader(in);
                }
                try {
                    int docNum = bs.nextSetBit(0);
                    while (docNum >= 0) {
                        if (cancel != null && cancel.get()) {
                            throw new InterruptedException();
                        }
                        Document doc = in.document(docNum, selector);
                        T value = convertor.convert(doc);
                        if (value != null) {
                            Set<Term> terms = termCollector.get(docNum);
                            if (terms != null) {
                                result.put(value, LuceneIndex.convertTerms(termConvertor, terms));
                            } else {
                                if (!logged) {
                                    LOGGER.log(Level.WARNING, "Index info [maxDoc: {0} numDoc: {1} docs: {2}]", new Object[]{in.maxDoc(), in.numDocs(), termCollector.docs()});
                                    logged = true;
                                }
                                LOGGER.log(Level.WARNING, "No terms found for doc: {0}", docNum);
                            }
                        }
                        docNum = bs.nextSetBit(docNum + 1);
                    }
                }
                finally {
                    if (termConvertor instanceof IndexReaderInjection) {
                        ((IndexReaderInjection)((Object)termConvertor)).setIndexReader(null);
                    }
                }
            }
            finally {
                if (convertor instanceof IndexReaderInjection) {
                    ((IndexReaderInjection)((Object)convertor)).setIndexReader(null);
                }
            }
        }
        finally {
            this.dirCache.releaseReader(in);
        }
    }

    private static <T> Set<T> convertTerms(Convertor<? super Term, T> convertor, Set<? extends Term> terms) {
        HashSet<T> result = new HashSet<T>(terms.size());
        for (Term term : terms) {
            result.add(convertor.convert(term));
        }
        return result;
    }

    @Override
    public void run() {
        this.dirCache.beginTx();
    }

    @Override
    public void commit() throws IOException {
        this.dirCache.closeTxWriter();
    }

    @Override
    public void rollback() throws IOException {
        this.dirCache.rollbackTxWriter();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public <S, T> void txStore(Collection<T> toAdd, Collection<S> toDelete, Convertor<? super T, ? extends Document> docConvertor, Convertor<? super S, ? extends Query> queryConvertor) throws IOException {
        IndexWriter wr = this.dirCache.acquireWriter();
        try {
            try {
                if (LOGGER.isLoggable(Level.FINE)) {
                    LOGGER.log(Level.FINE, "Storing in TX {0}: {1} added, {2} deleted", new Object[]{this, toAdd.size(), toDelete.size()});
                }
                this._doStore(toAdd, toDelete, docConvertor, queryConvertor, wr);
            }
            finally {
                boolean ok = false;
                try {
                    ((FlushIndexWriter)wr).callFlush(false, true);
                    ok = true;
                }
                finally {
                    if (!ok) {
                        this.dirCache.rollbackTxWriter();
                    }
                }
            }
        }
        finally {
            this.dirCache.releaseWriter(wr);
        }
    }

    private <S, T> void _doStore(@NonNull Collection<T> data, @NonNull Collection<S> toDelete, @NonNull Convertor<? super T, ? extends Document> docConvertor, @NonNull Convertor<? super S, ? extends Query> queryConvertor, @NonNull IndexWriter out) throws IOException {
        try {
            if (this.dirCache.exists()) {
                for (S td : toDelete) {
                    out.deleteDocuments(queryConvertor.convert(td));
                }
            }
            if (data.isEmpty()) {
                return;
            }
            LowMemoryWatcher lmListener = LowMemoryWatcher.getInstance();
            RAMDirectory memDir = null;
            IndexWriter activeOut = null;
            if (lmListener.isLowMemory()) {
                activeOut = out;
            } else {
                memDir = new RAMDirectory();
                activeOut = new IndexWriter((Directory)memDir, new IndexWriterConfig(Version.LUCENE_35, this.dirCache.getAnalyzer()));
            }
            Iterator<T> it = LuceneIndex.fastRemoveIterable(data).iterator();
            while (it.hasNext()) {
                T entry = it.next();
                it.remove();
                Document doc = docConvertor.convert(entry);
                activeOut.addDocument(doc);
                if (memDir == null || !lmListener.isLowMemory()) continue;
                activeOut.close();
                out.addIndexes(new Directory[]{memDir});
                memDir = new RAMDirectory();
                activeOut = new IndexWriter((Directory)memDir, new IndexWriterConfig(Version.LUCENE_35, this.dirCache.getAnalyzer()));
            }
            data.clear();
            if (memDir != null) {
                activeOut.close();
                out.addIndexes(new Directory[]{memDir});
                activeOut = null;
                memDir = null;
            }
        }
        catch (RuntimeException e) {
            throw (RuntimeException)Exceptions.attachMessage((Throwable)e, (String)("Lucene Index Folder: " + this.dirCache.folder.getAbsolutePath()));
        }
        catch (IOException e) {
            throw (IOException)Exceptions.attachMessage((Throwable)e, (String)("Lucene Index Folder: " + this.dirCache.folder.getAbsolutePath()));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public <S, T> void store(@NonNull Collection<T> data, @NonNull Collection<S> toDelete, @NonNull Convertor<? super T, ? extends Document> docConvertor, @NonNull Convertor<? super S, ? extends Query> queryConvertor, boolean optimize) throws IOException {
        IndexWriter wr = this.dirCache.acquireWriter();
        this.dirCache.storeCloseSynchronizer.enter();
        try {
            try {
                try {
                    this._doStore(data, toDelete, docConvertor, queryConvertor, wr);
                }
                finally {
                    LOGGER.log(Level.FINE, "Committing {0}", this);
                    this.dirCache.releaseWriter(wr);
                }
            }
            finally {
                this.dirCache.close(wr);
            }
        }
        finally {
            this.dirCache.storeCloseSynchronizer.exit();
        }
    }

    @Override
    public Index.Status getStatus(boolean force) throws IOException {
        return this.dirCache.getStatus(force);
    }

    @Override
    public void clear() throws IOException {
        this.dirCache.clear();
    }

    @Override
    public void close() throws IOException {
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "Closing index: {0} {1}", new Object[]{this.dirCache.toString(), Thread.currentThread().getStackTrace()});
        }
        this.dirCache.close(true);
    }

    public String toString() {
        return this.getClass().getSimpleName() + "[" + this.dirCache.toString() + "]";
    }

    private static CachePolicy getCachePolicy() {
        String value = System.getProperty("java.index.useMemCache");
        if (Boolean.TRUE.toString().equals(value) || CachePolicy.ALL.getSystemName().equals(value)) {
            return CachePolicy.ALL;
        }
        if (Boolean.FALSE.toString().equals(value) || CachePolicy.NONE.getSystemName().equals(value)) {
            return CachePolicy.NONE;
        }
        if (CachePolicy.DYNAMIC.getSystemName().equals(value)) {
            return CachePolicy.DYNAMIC;
        }
        return DEFAULT_CACHE_POLICY;
    }

    private static <T> Iterable<T> fastRemoveIterable(Collection<T> c) {
        return c instanceof ArrayList ? new Iterable<T>((Collection)c){
            final /* synthetic */ Collection val$c;

            @Override
            public Iterator<T> iterator() {
                return new Iterator<T>(){
                    private final ListIterator<T> delegate;

                    @Override
                    public boolean hasNext() {
                        return this.delegate.hasNext();
                    }

                    @Override
                    public T next() {
                        return this.delegate.next();
                    }

                    @Override
                    public void remove() {
                        this.delegate.set(null);
                    }
                };
            }

        } : c;
    }

    private static final class StoreCloseSynchronizer {
        private ThreadLocal<Boolean> isWriterThread;
        private int depth;

        StoreCloseSynchronizer() {
            this.isWriterThread = new ThreadLocal<Boolean>(){

                @Override
                protected Boolean initialValue() {
                    return Boolean.FALSE;
                }
            };
        }

        synchronized void enter() {
            ++this.depth;
            this.isWriterThread.set(Boolean.TRUE);
        }

        synchronized void exit() {
            assert (this.depth > 0);
            --this.depth;
            this.isWriterThread.remove();
            if (this.depth == 0) {
                this.notifyAll();
            }
        }

        synchronized Future<Void> getSync() {
            if (this.depth == 0 || this.isWriterThread.get() == Boolean.TRUE) {
                return null;
            }
            return new Future<Void>(){

                @Override
                public boolean cancel(boolean mayInterruptIfRunning) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public boolean isDone() {
                    StoreCloseSynchronizer storeCloseSynchronizer = StoreCloseSynchronizer.this;
                    synchronized (storeCloseSynchronizer) {
                        return StoreCloseSynchronizer.this.depth == 0;
                    }
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public Void get() throws InterruptedException, ExecutionException {
                    StoreCloseSynchronizer storeCloseSynchronizer = StoreCloseSynchronizer.this;
                    synchronized (storeCloseSynchronizer) {
                        while (StoreCloseSynchronizer.this.depth > 0) {
                            StoreCloseSynchronizer.this.wait();
                        }
                    }
                    return null;
                }

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                    if (unit != TimeUnit.MILLISECONDS) {
                        throw new UnsupportedOperationException();
                    }
                    StoreCloseSynchronizer storeCloseSynchronizer = StoreCloseSynchronizer.this;
                    synchronized (storeCloseSynchronizer) {
                        while (StoreCloseSynchronizer.this.depth > 0) {
                            StoreCloseSynchronizer.this.wait(timeout);
                        }
                    }
                    return null;
                }
            };
        }

    }

    private static class FlushIndexWriter
    extends IndexWriter {
        public FlushIndexWriter(@NonNull Directory d, @NonNull IndexWriterConfig conf) throws CorruptIndexException, LockObtainFailedException, IOException {
            super(d, conf);
        }

        void callFlush(boolean triggerMerges, boolean flushDeletes) throws IOException {
            super.flush(triggerMerges, true, flushDeletes);
        }
    }

    private static final class DirCache
    implements Evictable {
        private static final RequestProcessor RP = new RequestProcessor(LuceneIndex.class.getName(), 1);
        private final File folder;
        private final RecordOwnerLockFactory lockFactory;
        private final CachePolicy cachePolicy;
        private final Analyzer analyzer;
        private final StoreCloseSynchronizer storeCloseSynchronizer;
        private volatile FSDirectory fsDir;
        private RAMDirectory memDir;
        private CleanReference ref;
        private IndexReader reader;
        private volatile boolean closed;
        private volatile Throwable closeStackTrace;
        private volatile Index.Status validCache;
        private final OwnerReference owner = new OwnerReference();
        private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
        private ThreadLocal<IndexWriter> txWriter = new ThreadLocal();

        private DirCache(@NonNull File folder, @NonNull CachePolicy cachePolicy, @NonNull Analyzer analyzer) throws IOException {
            assert (folder != null);
            assert (cachePolicy != null);
            assert (analyzer != null);
            this.folder = folder;
            this.lockFactory = new RecordOwnerLockFactory();
            this.fsDir = DirCache.createFSDirectory(folder, this.lockFactory);
            this.cachePolicy = cachePolicy;
            this.analyzer = analyzer;
            this.storeCloseSynchronizer = new StoreCloseSynchronizer();
        }

        Analyzer getAnalyzer() {
            return this.analyzer;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void clear() throws IOException {
            do {
                Future<Void> sync;
                this.rwLock.writeLock().lock();
                try {
                    sync = this.storeCloseSynchronizer.getSync();
                    if (sync == null) {
                        this.doClear();
                    }
                }
                finally {
                    this.rwLock.writeLock().unlock();
                }
                try {
                    sync.get();
                    continue;
                }
                catch (InterruptedException ex) {
                    break;
                }
                catch (ExecutionException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                    continue;
                }
                break;
            } while (true);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private synchronized void doClear() throws IOException {
            this.checkPreconditions();
            this.doClose(false);
            try {
                File cacheDir;
                File[] children;
                this.lockFactory.forceClearLocks();
                String[] content = this.fsDir.listAll();
                boolean dirty = false;
                if (content != null) {
                    for (String file : content) {
                        try {
                            this.fsDir.deleteFile(file);
                            continue;
                        }
                        catch (IOException e) {
                            if (!this.fsDir.fileExists(file)) continue;
                            dirty = true;
                        }
                    }
                }
                if (dirty && (children = (cacheDir = this.fsDir.getDirectory()).listFiles()) != null) {
                    for (File child : children) {
                        if (child.delete()) continue;
                        throw this.annotateException(new IOException("Cannot delete: " + child.getAbsolutePath()));
                    }
                }
            }
            finally {
                this.fsDir.close();
                this.fsDir = DirCache.createFSDirectory(this.folder, this.lockFactory);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void close(IndexWriter writer) throws IOException {
            block16 : {
                if (writer == null) {
                    return;
                }
                boolean success = false;
                try {
                    writer.close();
                    success = true;
                    if (this.txWriter.get() != writer) break block16;
                    LOGGER.log(Level.FINE, "TX writer cleared for {0}", this);
                    this.txWriter.remove();
                    this.owner.release();
                }
                catch (Throwable var5_5) {
                    if (this.txWriter.get() == writer) {
                        LOGGER.log(Level.FINE, "TX writer cleared for {0}", this);
                        this.txWriter.remove();
                        this.owner.release();
                        try {
                            if (!success && IndexWriter.isLocked((Directory)this.fsDir)) {
                                IndexWriter.unlock((Directory)this.fsDir);
                            }
                        }
                        catch (IOException ioe) {
                            LOGGER.log(Level.WARNING, "Cannot unlock index {0} while recovering, {1}.", new Object[]{this.folder.getAbsolutePath(), ioe.getMessage()});
                        }
                        finally {
                            this.refreshReader();
                        }
                    }
                    throw var5_5;
                }
                try {
                    if (!success && IndexWriter.isLocked((Directory)this.fsDir)) {
                        IndexWriter.unlock((Directory)this.fsDir);
                    }
                }
                catch (IOException ioe) {
                    LOGGER.log(Level.WARNING, "Cannot unlock index {0} while recovering, {1}.", new Object[]{this.folder.getAbsolutePath(), ioe.getMessage()});
                }
                finally {
                    this.refreshReader();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void close(boolean closeFSDir) throws IOException {
            do {
                Future<Void> sync;
                this.rwLock.writeLock().lock();
                try {
                    sync = this.storeCloseSynchronizer.getSync();
                    if (sync == null) {
                        this.doClose(closeFSDir);
                    }
                }
                finally {
                    this.rwLock.writeLock().unlock();
                }
                try {
                    sync.get();
                    continue;
                }
                catch (InterruptedException ex) {
                    break;
                }
                catch (ExecutionException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                    continue;
                }
                break;
            } while (true);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        synchronized void doClose(boolean closeFSDir) throws IOException {
            try {
                this.rollbackTxWriter();
                if (this.reader != null) {
                    this.reader.close();
                    this.reader = null;
                }
            }
            finally {
                if (this.memDir != null) {
                    assert (this.cachePolicy.hasMemCache());
                    if (this.ref != null) {
                        this.ref.clear();
                    }
                    RAMDirectory tmpDir = this.memDir;
                    this.memDir = null;
                    tmpDir.close();
                }
                if (closeFSDir) {
                    this.closeStackTrace = new Throwable();
                    this.closed = true;
                    this.fsDir.close();
                }
            }
        }

        boolean exists() {
            try {
                return IndexReader.indexExists((Directory)this.fsDir);
            }
            catch (IOException e) {
                return false;
            }
            catch (RuntimeException e) {
                LOGGER.log(Level.INFO, "Broken index: " + this.folder.getAbsolutePath(), e);
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        Index.Status getStatus(boolean force) throws IOException {
            this.checkPreconditions();
            Index.Status valid = this.validCache;
            if (force || valid == null) {
                this.rwLock.writeLock().lock();
                try {
                    Index.Status res = Index.Status.INVALID;
                    if (this.lockFactory.hasLocks()) {
                        if (this.txWriter.get() != null) {
                            res = Index.Status.WRITING;
                        } else {
                            LOGGER.log(Level.WARNING, "Locked index folder: {0}", this.folder.getAbsolutePath());
                            if (force) {
                                this.clear();
                            }
                        }
                    } else if (!this.exists()) {
                        res = Index.Status.EMPTY;
                    } else if (force) {
                        try {
                            this.getReader();
                            res = Index.Status.VALID;
                        }
                        catch (IOException e) {
                            this.clear();
                        }
                        catch (RuntimeException e) {
                            this.clear();
                        }
                    } else {
                        res = Index.Status.VALID;
                    }
                    this.validCache = valid = res;
                }
                finally {
                    this.rwLock.writeLock().unlock();
                }
            }
            return valid;
        }

        boolean closeTxWriter() throws IOException {
            IndexWriter writer = this.txWriter.get();
            if (writer != null) {
                LOGGER.log(Level.FINE, "Committing {0}", this);
                this.close(writer);
                return true;
            }
            return false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        boolean rollbackTxWriter() throws IOException {
            IndexWriter writer = this.txWriter.get();
            if (writer != null) {
                try {
                    writer.rollback();
                    boolean bl = true;
                    return bl;
                }
                finally {
                    this.txWriter.remove();
                    this.owner.release();
                }
            }
            return false;
        }

        void beginTx() {
            this.owner.acquire();
        }

        IndexWriter acquireWriter() throws IOException {
            this.checkPreconditions();
            this.hit();
            boolean ok = false;
            this.rwLock.readLock().lock();
            IndexWriter writer = this.txWriter.get();
            try {
                boolean alwaysCFS;
                if (writer != null) {
                    this.owner.assertSingleThreadWriter();
                    ok = true;
                    IndexWriter indexWriter = writer;
                    return indexWriter;
                }
                IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_35, this.analyzer);
                boolean bl = alwaysCFS = Utilities.getOperatingSystem() == 16;
                if (alwaysCFS) {
                    TieredMergePolicy mergePolicy = new TieredMergePolicy();
                    mergePolicy.setNoCFSRatio(1.0);
                    iwc.setMergePolicy((MergePolicy)mergePolicy);
                }
                FlushIndexWriter iw = new FlushIndexWriter((Directory)this.fsDir, iwc);
                this.txWriter.set(iw);
                this.owner.modified();
                ok = true;
                FlushIndexWriter flushIndexWriter = iw;
                return flushIndexWriter;
            }
            finally {
                if (!ok) {
                    this.rwLock.readLock().unlock();
                }
            }
        }

        void releaseWriter(@NonNull IndexWriter w) {
            assert (this.txWriter.get() == w || this.txWriter.get() == null);
            this.rwLock.readLock().unlock();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        IndexReader acquireReader() throws IOException {
            this.rwLock.readLock().lock();
            IndexReader r = null;
            try {
                IndexReader indexReader = r = this.getReader();
                return indexReader;
            }
            finally {
                if (r == null) {
                    this.rwLock.readLock().unlock();
                }
            }
        }

        void releaseReader(IndexReader r) {
            if (r == null) {
                return;
            }
            assert (r == this.reader);
            this.rwLock.readLock().unlock();
        }

        private synchronized IndexReader getReader() throws IOException {
            this.checkPreconditions();
            this.hit();
            if (this.reader == null) {
                if (this.validCache != Index.Status.VALID && this.validCache != Index.Status.WRITING && this.validCache != null) {
                    return null;
                }
                try {
                    FSDirectory source;
                    if (this.cachePolicy.hasMemCache() && DirCache.fitsIntoMem((Directory)this.fsDir)) {
                        this.memDir = new RAMDirectory((Directory)this.fsDir);
                        if (this.cachePolicy == CachePolicy.DYNAMIC) {
                            this.ref = new CleanReference(new RAMDirectory[]{this.memDir});
                        }
                        source = this.memDir;
                    } else {
                        source = this.fsDir;
                    }
                    assert (source != null);
                    this.reader = IndexReader.open((Directory)source, (boolean)true);
                }
                catch (FileNotFoundException fnf) {
                }
                catch (IOException ioe) {
                    if (this.validCache == null) {
                        return null;
                    }
                    throw this.annotateException(ioe);
                }
            }
            this.hit();
            return this.reader;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void refreshReader() throws IOException {
            block11 : {
                try {
                    if (this.cachePolicy.hasMemCache()) {
                        this.close(false);
                        break block11;
                    }
                    this.rwLock.writeLock().lock();
                    try {
                        DirCache dirCache = this;
                        synchronized (dirCache) {
                            IndexReader newReader;
                            if (this.reader != null && (newReader = IndexReader.openIfChanged((IndexReader)this.reader)) != null) {
                                this.reader.close();
                                this.reader = newReader;
                            }
                        }
                    }
                    finally {
                        this.rwLock.writeLock().unlock();
                    }
                }
                finally {
                    this.validCache = Index.Status.VALID;
                }
            }
        }

        public String toString() {
            return this.folder.getAbsolutePath();
        }

        @Override
        public synchronized void evicted() {
            if (this.memDir != null) {
                if (this.ref != null) {
                    this.ref.clearHRef();
                }
            } else {
                RP.post(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            DirCache.this.close(false);
                            LOGGER.log(Level.FINE, "Evicted index: {0}", DirCache.this.folder.getAbsolutePath());
                        }
                        catch (IOException ex) {
                            Exceptions.printStackTrace((Throwable)ex);
                        }
                    }
                });
            }
        }

        private synchronized void hit() {
            if (this.reader != null) {
                URI uri = Utilities.toURI((File)this.folder);
                if (this.memDir != null) {
                    IndexCacheFactory.getDefault().getRAMCache().put(uri, this);
                    if (this.ref != null) {
                        this.ref.get();
                    }
                } else {
                    IndexCacheFactory.getDefault().getNIOCache().put(uri, this);
                }
            }
        }

        private void checkPreconditions() throws Index.IndexClosedException {
            if (this.closed) {
                throw (Index.IndexClosedException)new Index.IndexClosedException().initCause(this.closeStackTrace);
            }
        }

        private IOException annotateException(IOException ioe) {
            StringBuilder message = new StringBuilder();
            File[] children = this.folder.listFiles();
            if (children == null) {
                message.append("Non existing index folder");
            } else {
                message.append("Current Lucene version: ").append(LucenePackage.get().getSpecificationVersion()).append('(').append(LucenePackage.get().getImplementationVersion()).append(")\n");
                for (File c : children) {
                    message.append(c.getName()).append(" f: ").append(c.isFile()).append(" r: ").append(c.canRead()).append(" w: ").append(c.canWrite()).append("\n");
                }
                message.append("threads: ").append(this.stackTraces(Thread.getAllStackTraces())).append("\n");
                message.append("lockFactory: ").append((Object)this.lockFactory);
            }
            return (IOException)Exceptions.attachMessage((Throwable)ioe, (String)message.toString());
        }

        private static FSDirectory createFSDirectory(File indexFolder, LockFactory lockFactory) throws IOException {
            assert (indexFolder != null);
            assert (lockFactory != null);
            String dirType = System.getProperty("java.index.dir");
            MMapDirectory directory = "mmap".equals(dirType) ? new MMapDirectory(indexFolder, lockFactory) : ("nio".equals(dirType) ? new NIOFSDirectory(indexFolder, lockFactory) : ("io".equals(dirType) ? new SimpleFSDirectory(indexFolder, lockFactory) : FSDirectory.open((File)indexFolder, (LockFactory)lockFactory)));
            return directory;
        }

        private static boolean fitsIntoMem(@NonNull Directory dir) {
            try {
                long size = 0;
                for (String path : dir.listAll()) {
                    size += dir.fileLength(path);
                }
                return IndexCacheFactory.getDefault().getRAMController().shouldLoad(size);
            }
            catch (IOException ioe) {
                return false;
            }
        }

        private Map<String, String> stackTraces(Map<Thread, StackTraceElement[]> traces) {
            HashMap<String, String> result = new HashMap<String, String>();
            for (Map.Entry<Thread, StackTraceElement[]> entry : traces.entrySet()) {
                result.put(entry.getKey().toString() + "(" + entry.getKey().getId() + ")", Arrays.toString(entry.getValue()));
            }
            return result;
        }

        private final class CleanReference
        extends SoftReference<RAMDirectory[]>
        implements Runnable {
            private volatile Directory[] hardRef;
            private final AtomicLong size;

            private CleanReference(RAMDirectory[] dir) {
                boolean doHardRef;
                super(dir, Utilities.activeReferenceQueue());
                this.size = new AtomicLong();
                IndexCacheFactory.RAMContoller c = IndexCacheFactory.getDefault().getRAMController();
                boolean bl = doHardRef = !c.isFull();
                if (doHardRef) {
                    this.hardRef = dir;
                    long _size = dir[0].sizeInBytes();
                    this.size.set(_size);
                    c.acquire(_size);
                }
                LOGGER.log(Level.FINEST, "Caching index: {0} cache policy: {1}", new Object[]{DirCache.this.folder.getAbsolutePath(), DirCache.this.cachePolicy.getSystemName()});
            }

            @Override
            public void run() {
                try {
                    LOGGER.log(Level.FINEST, "Dropping cache index: {0} cache policy: {1}", new Object[]{DirCache.this.folder.getAbsolutePath(), DirCache.this.cachePolicy.getSystemName()});
                    DirCache.this.close(false);
                }
                catch (IOException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }

            @Override
            public void clear() {
                this.clearHRef();
                SoftReference.super.clear();
            }

            void clearHRef() {
                this.hardRef = null;
                IndexCacheFactory.getDefault().getRAMController().release(this.size.getAndSet(0));
            }
        }

        private static final class OwnerReference {
            private Pair<Thread, Pair<Long, Exception>> txThread;
            private boolean modified;

            private OwnerReference() {
            }

            synchronized void acquire() {
                this.assertNoModifiedWriter();
                this.txThread = Pair.of((Object)Thread.currentThread(), (Object)(OwnerReference.assertsEnabled() ? Pair.of((Object)System.currentTimeMillis(), (Object)new Exception("Owner stack")) : null));
                this.modified = false;
            }

            synchronized void release() {
                this.txThread = null;
                this.modified = false;
            }

            synchronized void modified() {
                this.modified = true;
            }

            synchronized void assertSingleThreadWriter() {
                if (this.txThread != null && this.txThread.first() != Thread.currentThread()) {
                    Throwable t = new Throwable(String.format("Other thread using opened writer, old owner Thread %s , new owner Thread %s.", this.txThread.first(), Thread.currentThread()));
                    LOGGER.log(Level.WARNING, "Multiple writers", t);
                }
            }

            private void assertNoModifiedWriter() {
                assert (Thread.holdsLock(this));
                if (OwnerReference.assertsEnabled() && this.txThread != null && this.modified) {
                    Throwable t = new Throwable(String.format("Using stale writer, possibly forgotten call to store, old owner Thread %s(%d) enter time: %d, new owner Thread %s(%d) enter time: %d.", this.txThread.first(), ((Thread)this.txThread.first()).getId(), ((Pair)this.txThread.second()).first(), Thread.currentThread(), Thread.currentThread().getId(), System.currentTimeMillis()), (Throwable)((Pair)this.txThread.second()).second());
                    LOGGER.log(Level.WARNING, "Using stale writer", t);
                }
            }

            private static boolean assertsEnabled() {
                boolean ae = false;
                if (!$assertionsDisabled) {
                    ae = true;
                    if (!true) {
                        throw new AssertionError();
                    }
                }
                return ae;
            }
        }

    }

    private static final class CachePolicy
    extends Enum<CachePolicy> {
        public static final /* enum */ CachePolicy NONE = new CachePolicy("none", false);
        public static final /* enum */ CachePolicy DYNAMIC = new CachePolicy("dynamic", true);
        public static final /* enum */ CachePolicy ALL = new CachePolicy("all", true);
        private final String sysName;
        private final boolean hasMemCache;
        private static final /* synthetic */ CachePolicy[] $VALUES;

        public static CachePolicy[] values() {
            return (CachePolicy[])$VALUES.clone();
        }

        public static CachePolicy valueOf(String name) {
            return Enum.valueOf(CachePolicy.class, name);
        }

        private CachePolicy(String sysName, boolean hasMemCache) {
            super(string, n);
            assert (sysName != null);
            this.sysName = sysName;
            this.hasMemCache = hasMemCache;
        }

        String getSystemName() {
            return this.sysName;
        }

        boolean hasMemCache() {
            return this.hasMemCache;
        }

        static {
            $VALUES = new CachePolicy[]{NONE, DYNAMIC, ALL};
        }
    }

}

