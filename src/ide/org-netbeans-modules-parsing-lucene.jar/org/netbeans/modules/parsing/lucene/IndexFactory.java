/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.analysis.Analyzer
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.lucene;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.lucene.support.Index;

public interface IndexFactory {
    public Index.Transactional createIndex(@NonNull File var1, @NonNull Analyzer var2) throws IOException;

    public Index createMemoryIndex(@NonNull Analyzer var1) throws IOException;
}

