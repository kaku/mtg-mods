/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.store.Lock
 *  org.apache.lucene.store.LockFactory
 */
package org.netbeans.modules.parsing.lucene;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.store.Lock;
import org.apache.lucene.store.LockFactory;

class RecordOwnerLockFactory
extends LockFactory {
    private final Map<String, RecordOwnerLock> locks = new HashMap<String, RecordOwnerLock>();

    RecordOwnerLockFactory() throws IOException {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Lock makeLock(String lockName) {
        Map<String, RecordOwnerLock> map = this.locks;
        synchronized (map) {
            RecordOwnerLock res = this.locks.get(lockName);
            if (res == null) {
                res = new RecordOwnerLock();
                this.locks.put(lockName, res);
            }
            return res;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clearLock(String lockName) throws IOException {
        Map<String, RecordOwnerLock> map = this.locks;
        synchronized (map) {
            RecordOwnerLock lock = this.locks.remove(lockName);
            if (lock != null) {
                lock.release();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    boolean hasLocks() {
        Map<String, RecordOwnerLock> map = this.locks;
        synchronized (map) {
            boolean res = false;
            for (RecordOwnerLock lock : this.locks.values()) {
                res |= lock.isLocked();
            }
            return res;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Collection<? extends Lock> forceClearLocks() {
        Map<String, RecordOwnerLock> map = this.locks;
        synchronized (map) {
            ArrayDeque<RecordOwnerLock> locked = new ArrayDeque<RecordOwnerLock>();
            Iterator<RecordOwnerLock> it = this.locks.values().iterator();
            while (it.hasNext()) {
                RecordOwnerLock lock = it.next();
                if (!lock.isLocked()) continue;
                it.remove();
                locked.offer(lock);
            }
            return locked;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append('[');
        Map<String, RecordOwnerLock> map = this.locks;
        synchronized (map) {
            boolean first = true;
            for (Map.Entry<String, RecordOwnerLock> e : this.locks.entrySet()) {
                if (!first) {
                    sb.append(',');
                } else {
                    first = false;
                }
                sb.append("name: ").append(e.getKey()).append("->").append((Object)e.getValue());
            }
        }
        sb.append(']');
        return sb.toString();
    }

    private final class RecordOwnerLock
    extends Lock {
        private Thread owner;
        private Exception caller;

        private RecordOwnerLock() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean obtain() {
            Map map = RecordOwnerLockFactory.this.locks;
            synchronized (map) {
                if (this.owner == null) {
                    this.owner = Thread.currentThread();
                    this.caller = new Exception();
                    return true;
                }
                return false;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void release() {
            Map map = RecordOwnerLockFactory.this.locks;
            synchronized (map) {
                this.owner = null;
                this.caller = null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public boolean isLocked() {
            Map map = RecordOwnerLockFactory.this.locks;
            synchronized (map) {
                return this.owner != null;
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public String toString() {
            Map map = RecordOwnerLockFactory.this.locks;
            synchronized (map) {
                Object[] arrobject = new Object[4];
                arrobject[0] = this.getClass().getSimpleName();
                arrobject[1] = this.owner;
                arrobject[2] = this.owner == null ? -1 : this.owner.getId();
                arrobject[3] = this.caller == null ? Collections.emptySet() : Arrays.asList(this.caller.getStackTrace());
                return String.format("%s[owned by: %s(%d), created from: %s]", arrobject);
            }
        }
    }

}

