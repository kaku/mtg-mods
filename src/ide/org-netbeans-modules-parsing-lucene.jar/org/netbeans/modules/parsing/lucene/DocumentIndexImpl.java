/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.Document
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.search.Query
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.lucene;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.search.Query;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.IndexDocumentImpl;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.netbeans.modules.parsing.lucene.support.Convertors;
import org.netbeans.modules.parsing.lucene.support.DocumentIndex2;
import org.netbeans.modules.parsing.lucene.support.DocumentIndexCache;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexDocument;
import org.netbeans.modules.parsing.lucene.support.Queries;
import org.openide.util.Parameters;

public class DocumentIndexImpl
implements DocumentIndex2,
Runnable {
    private static final Convertor<IndexDocument, Document> DEFAULT_ADD_CONVERTOR = org.netbeans.modules.parsing.lucene.Convertors.newIndexDocumentToDocumentConvertor();
    private static final Convertor<Document, IndexDocumentImpl> DEFAULT_QUERY_CONVERTOR = org.netbeans.modules.parsing.lucene.Convertors.newDocumentToIndexDocumentConvertor();
    private static final Convertor<String, Query> REMOVE_CONVERTOR = org.netbeans.modules.parsing.lucene.Convertors.newSourceNameToQueryConvertor();
    private static final Logger LOGGER = Logger.getLogger(DocumentIndexImpl.class.getName());
    private final Set<String> dirtyKeys = new HashSet<String>();
    private final DocumentIndexCache cache;
    private final Index luceneIndex;
    private final Convertor<? super IndexDocument, ? extends Document> addConvertor;
    private final Convertor<? super Document, ? extends IndexDocument> queryConvertor;
    final Index.Transactional txLuceneIndex;
    final AtomicBoolean requiresRollBack = new AtomicBoolean();

    private DocumentIndexImpl(@NonNull Index index, @NonNull DocumentIndexCache cache) {
        assert (index != null);
        assert (cache != null);
        this.luceneIndex = index;
        this.cache = cache;
        Convertor<IndexDocument, Document> _addConvertor = null;
        Convertor<Document, IndexDocument> _queryConvertor = null;
        if (cache instanceof DocumentIndexCache.WithCustomIndexDocument) {
            DocumentIndexCache.WithCustomIndexDocument cacheWithCustomDoc = (DocumentIndexCache.WithCustomIndexDocument)cache;
            _addConvertor = cacheWithCustomDoc.createAddConvertor();
            _queryConvertor = cacheWithCustomDoc.createQueryConvertor();
        }
        this.addConvertor = _addConvertor != null ? _addConvertor : DEFAULT_ADD_CONVERTOR;
        this.queryConvertor = _queryConvertor != null ? _queryConvertor : DEFAULT_QUERY_CONVERTOR;
        this.txLuceneIndex = index instanceof Index.Transactional ? (Index.Transactional)index : null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void addDocument(IndexDocument document) {
        boolean forceFlush;
        DocumentIndexImpl documentIndexImpl = this;
        synchronized (documentIndexImpl) {
            forceFlush = this.cache.addDocument(document);
        }
        if (forceFlush) {
            try {
                LOGGER.fine("Extra flush forced");
                this.store(false, true);
                System.gc();
            }
            catch (IOException ioe) {
                LOGGER.log(Level.WARNING, ioe.getMessage());
                this.requiresRollBack.set(true);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeDocument(String primaryKey) {
        boolean forceFlush;
        DocumentIndexImpl documentIndexImpl = this;
        synchronized (documentIndexImpl) {
            forceFlush = this.cache.removeDocument(primaryKey);
        }
        if (forceFlush) {
            try {
                LOGGER.fine("Extra flush forced");
                this.store(false, true);
            }
            catch (IOException ioe) {
                LOGGER.log(Level.WARNING, ioe.getMessage());
                this.requiresRollBack.set(true);
            }
        }
    }

    @Override
    public Index.Status getStatus() throws IOException {
        return this.luceneIndex.getStatus(true);
    }

    @Override
    public void close() throws IOException {
        this.luceneIndex.close();
    }

    @Override
    public void store(boolean optimize) throws IOException {
        this.checkRollBackNeeded();
        this.store(optimize, false);
    }

    @Override
    public void run() {
        if (this.luceneIndex instanceof Runnable) {
            ((Runnable)((Object)this.luceneIndex)).run();
        }
    }

    private void store(boolean optimize, boolean flushOnly) throws IOException {
        boolean change = this.storeImpl(optimize, flushOnly);
        if (!change && !flushOnly && this.txLuceneIndex != null) {
            this.commitImpl();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean storeImpl(boolean optimize, boolean flushOnly) throws IOException {
        Collection<? extends IndexDocument> _toAdd;
        Collection<? extends String> _toRemove;
        DocumentIndexImpl documentIndexImpl = this;
        synchronized (documentIndexImpl) {
            _toAdd = this.cache.getAddedDocuments();
            _toRemove = this.cache.getRemovedKeys();
            this.cache.clear();
            if (!this.dirtyKeys.isEmpty()) {
                for (IndexDocument ldoc : _toAdd) {
                    this.dirtyKeys.remove(ldoc.getPrimaryKey());
                }
                this.dirtyKeys.removeAll(_toRemove);
            }
        }
        if (!_toAdd.isEmpty() || !_toRemove.isEmpty()) {
            LOGGER.log(Level.FINE, "Flushing: {0}", this.luceneIndex.toString());
            if (flushOnly && this.txLuceneIndex != null) {
                this.txLuceneIndex.txStore(_toAdd, _toRemove, this.addConvertor, REMOVE_CONVERTOR);
            } else {
                this.luceneIndex.store(_toAdd, _toRemove, this.addConvertor, REMOVE_CONVERTOR, optimize);
            }
            return true;
        }
        return false;
    }

    private void commitImpl() throws IOException {
        this.checkRollBackNeeded();
        this.txLuceneIndex.commit();
    }

    private void checkRollBackNeeded() throws IOException {
        if (this.requiresRollBack.get()) {
            throw new IOException("Index requires rollback.");
        }
    }

    @Override
    public /* varargs */ Collection<? extends IndexDocument> query(String fieldName, String value, Queries.QueryKind kind, String ... fieldsToLoad) throws IOException, InterruptedException {
        assert (fieldName != null);
        assert (value != null);
        assert (kind != null);
        Query query = Queries.createQuery(fieldName, fieldName, value, kind);
        return this.query(query, Convertors.identity(), fieldsToLoad);
    }

    @Override
    public /* varargs */ Collection<? extends IndexDocument> findByPrimaryKey(String primaryKeyValue, Queries.QueryKind kind, String ... fieldsToLoad) throws IOException, InterruptedException {
        return this.query("_sn", primaryKeyValue, kind, fieldsToLoad);
    }

    @NonNull
    @Override
    public /* varargs */ <T> Collection<? extends T> query(@NonNull Query query, @NonNull Convertor<? super IndexDocument, ? extends T> convertor, @NullAllowed String ... fieldsToLoad) throws IOException, InterruptedException {
        Parameters.notNull((CharSequence)"query", (Object)query);
        Parameters.notNull((CharSequence)"convertor", convertor);
        ArrayDeque result = new ArrayDeque();
        FieldSelector selector = null;
        if (fieldsToLoad != null && fieldsToLoad.length > 0) {
            String[] fieldsWithSource = Arrays.copyOf(fieldsToLoad, fieldsToLoad.length + 1);
            fieldsWithSource[fieldsToLoad.length] = "_sn";
            selector = Queries.createFieldSelector(fieldsWithSource);
        }
        this.luceneIndex.query(result, Convertors.compose(this.queryConvertor, convertor), selector, null, query);
        return result;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void markKeyDirty(String primaryKey) {
        DocumentIndexImpl documentIndexImpl = this;
        synchronized (documentIndexImpl) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "{0}, adding dirty key: {1}", new Object[]{this, primaryKey});
            }
            this.dirtyKeys.add(primaryKey);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeDirtyKeys(Collection<? extends String> keysToRemove) {
        DocumentIndexImpl documentIndexImpl = this;
        synchronized (documentIndexImpl) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "{0}, Removing dirty keys: {1}", new Object[]{this, keysToRemove});
            }
            this.dirtyKeys.removeAll(keysToRemove);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Collection<? extends String> getDirtyKeys() {
        DocumentIndexImpl documentIndexImpl = this;
        synchronized (documentIndexImpl) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "{0}, dirty keys: {1}", new Object[]{this, this.dirtyKeys});
            }
            return new ArrayList<String>(this.dirtyKeys);
        }
    }

    public String toString() {
        return String.format("DocumentIndexImpl[%s]", this.luceneIndex.toString());
    }

    @NonNull
    public static DocumentIndex2 create(@NonNull Index index, @NonNull DocumentIndexCache cache) {
        return new DocumentIndexImpl(index, cache);
    }

    @NonNull
    public static DocumentIndex2.Transactional createTransactional(@NonNull Index.Transactional index, @NonNull DocumentIndexCache cache) {
        return new Transactional(index, cache);
    }

    private static final class Transactional
    extends DocumentIndexImpl
    implements DocumentIndex2.Transactional {
        private Transactional(@NonNull Index.Transactional index, @NonNull DocumentIndexCache cache) {
            super(index, cache);
        }

        @Override
        public void txStore() throws IOException {
            this.storeImpl(false, true);
        }

        @Override
        public void commit() throws IOException {
            this.commitImpl();
        }

        @Override
        public void rollback() throws IOException {
            this.requiresRollBack.set(false);
            this.txLuceneIndex.rollback();
        }

        @Override
        public void clear() throws IOException {
            this.requiresRollBack.set(false);
            this.txLuceneIndex.clear();
        }

        @Override
        public String toString() {
            return "DocumentIndex.Transactional [" + this.luceneIndex.toString() + "]";
        }
    }

}

