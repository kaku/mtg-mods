/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.index.Term
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.lucene;

import org.apache.lucene.index.Term;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.modules.parsing.lucene.support.Index;
import org.netbeans.modules.parsing.lucene.support.IndexManager;

public abstract class SupportAccessor {
    private static volatile SupportAccessor instance;

    public static synchronized SupportAccessor getInstance() {
        if (instance == null) {
            try {
                Class.forName(IndexManager.class.getName(), true, SupportAccessor.class.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                throw new IllegalStateException(ex);
            }
        }
        assert (instance != null);
        return instance;
    }

    public static void setInstance(@NonNull SupportAccessor _instance) {
        assert (_instance != null);
        instance = _instance;
    }

    public abstract Index.WithTermFrequencies.TermFreq newTermFreq();

    @NonNull
    public abstract Index.WithTermFrequencies.TermFreq setTermFreq(@NonNull Index.WithTermFrequencies.TermFreq var1, @NonNull Term var2, int var3);
}

