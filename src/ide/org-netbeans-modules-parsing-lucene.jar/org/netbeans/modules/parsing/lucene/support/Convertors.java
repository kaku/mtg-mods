/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.annotations.common.NullAllowed
 *  org.openide.util.Parameters
 */
package org.netbeans.modules.parsing.lucene.support;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.annotations.common.NullAllowed;
import org.netbeans.modules.parsing.lucene.support.Convertor;
import org.openide.util.Parameters;

public final class Convertors {
    private static final Convertor<?, ?> IDENTITY = new Convertor<Object, Object>(){

        @CheckForNull
        @Override
        public Object convert(@NullAllowed Object p) {
            return p;
        }
    };

    private Convertors() {
    }

    public static <T> Convertor<T, T> identity() {
        return IDENTITY;
    }

    public static <P, I, R> Convertor<P, R> compose(@NonNull Convertor<? super P, ? extends I> first, @NonNull Convertor<? super I, ? extends R> second) {
        Parameters.notNull((CharSequence)"first", first);
        Parameters.notNull((CharSequence)"second", second);
        return new CompositeConvertor<P, I, R>(first, second);
    }

    private static final class CompositeConvertor<P, I, R>
    implements Convertor<P, R> {
        private final Convertor<? super P, ? extends I> first;
        private final Convertor<? super I, ? extends R> second;

        CompositeConvertor(@NonNull Convertor<? super P, ? extends I> first, @NonNull Convertor<? super I, ? extends R> second) {
            this.first = first;
            this.second = second;
        }

        @CheckForNull
        @Override
        public R convert(@NullAllowed P p) {
            return this.second.convert(this.first.convert(p));
        }
    }

}

