/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.lucene.document.FieldSelector
 *  org.apache.lucene.document.FieldSelectorResult
 */
package org.netbeans.modules.parsing.lucene;

import org.apache.lucene.document.FieldSelector;
import org.apache.lucene.document.FieldSelectorResult;

final class AllFieldsSelector
implements FieldSelector {
    static final FieldSelector INSTANCE = new AllFieldsSelector();

    private AllFieldsSelector() {
    }

    public FieldSelectorResult accept(String fieldName) {
        return FieldSelectorResult.LOAD;
    }
}

