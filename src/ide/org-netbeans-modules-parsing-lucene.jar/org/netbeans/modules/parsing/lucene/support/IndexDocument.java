/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 */
package org.netbeans.modules.parsing.lucene.support;

import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;

public interface IndexDocument {
    @NonNull
    public String getPrimaryKey();

    public void addPair(@NonNull String var1, @NonNull String var2, boolean var3, boolean var4);

    @CheckForNull
    public String getValue(@NonNull String var1);

    @NonNull
    public String[] getValues(@NonNull String var1);
}

