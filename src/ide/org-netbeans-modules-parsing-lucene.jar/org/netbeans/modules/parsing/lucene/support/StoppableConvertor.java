/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.parsing.lucene.support;

public interface StoppableConvertor<P, R> {
    public R convert(P var1) throws Stop;

    public static final class Stop
    extends Exception {
    }

}

