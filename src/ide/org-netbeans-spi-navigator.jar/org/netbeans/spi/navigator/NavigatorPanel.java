/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.navigator;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.swing.JComponent;
import org.openide.util.Lookup;

public interface NavigatorPanel {
    public String getDisplayName();

    public String getDisplayHint();

    public JComponent getComponent();

    public void panelActivated(Lookup var1);

    public void panelDeactivated();

    public Lookup getLookup();

    @Target(value={ElementType.TYPE, ElementType.METHOD})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface Registrations {
        public Registration[] value();
    }

    @Target(value={ElementType.TYPE, ElementType.METHOD})
    @Retention(value=RetentionPolicy.SOURCE)
    public static @interface Registration {
        public String mimeType();

        public int position() default Integer.MAX_VALUE;

        public String displayName();
    }

}

