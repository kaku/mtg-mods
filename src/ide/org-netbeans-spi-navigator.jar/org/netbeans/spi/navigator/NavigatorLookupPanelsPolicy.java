/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.navigator;

public interface NavigatorLookupPanelsPolicy {
    public static final int LOOKUP_HINTS_ONLY = 1;

    public int getPanelsPolicy();
}

