/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 */
package org.netbeans.spi.navigator;

import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.awt.UndoRedo;

public interface NavigatorPanelWithUndo
extends NavigatorPanel {
    public UndoRedo getUndoRedo();
}

