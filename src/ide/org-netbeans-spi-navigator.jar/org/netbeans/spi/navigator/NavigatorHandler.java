/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package org.netbeans.spi.navigator;

import org.netbeans.modules.navigator.NavigatorController;
import org.netbeans.modules.navigator.NavigatorTC;
import org.netbeans.spi.navigator.NavigatorDisplayer;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.util.Lookup;

public final class NavigatorHandler {
    private static NavigatorController controller;

    private NavigatorHandler() {
    }

    public static void activatePanel(NavigatorPanel panel) {
        NavigatorHandler.getController().activatePanel(panel);
    }

    public static void activateNavigator() {
        NavigatorHandler.getController();
    }

    private static NavigatorController getController() {
        if (controller == null) {
            NavigatorDisplayer display = (NavigatorDisplayer)Lookup.getDefault().lookup(NavigatorDisplayer.class);
            controller = display != null ? new NavigatorController(display) : NavigatorTC.getInstance().getController();
        }
        return controller;
    }
}

