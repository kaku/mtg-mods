/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.navigator;

import javax.swing.JComponent;
import org.netbeans.spi.navigator.NavigatorPanel;

public interface NavigatorPanelWithToolbar
extends NavigatorPanel {
    public JComponent getToolbarComponent();
}

