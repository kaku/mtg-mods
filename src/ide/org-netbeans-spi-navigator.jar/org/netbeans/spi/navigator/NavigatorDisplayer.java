/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package org.netbeans.spi.navigator;

import java.beans.PropertyChangeListener;
import java.util.List;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.windows.TopComponent;

public interface NavigatorDisplayer {
    public static final String PROP_PANEL_SELECTION = "navigatorPanelSelection";

    public void setDisplayName(String var1);

    public void setPanels(List<? extends NavigatorPanel> var1, NavigatorPanel var2);

    public void setSelectedPanel(NavigatorPanel var1);

    public NavigatorPanel getSelectedPanel();

    public boolean allowAsyncUpdate();

    public TopComponent getTopComponent();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

