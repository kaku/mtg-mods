/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataShadow
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.TaskListener
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.navigator;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.FocusManager;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.netbeans.modules.navigator.LazyPanel;
import org.netbeans.modules.navigator.NavigatorTC;
import org.netbeans.modules.navigator.ProviderRegistry;
import org.netbeans.spi.navigator.NavigatorDisplayer;
import org.netbeans.spi.navigator.NavigatorLookupHint;
import org.netbeans.spi.navigator.NavigatorLookupPanelsPolicy;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataShadow;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.TaskListener;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class NavigatorController
implements LookupListener,
PropertyChangeListener,
NodeListener {
    static final int COALESCE_TIME = 100;
    private NavigatorDisplayer navigatorTC;
    private List<NavigatorPanel> currentPanels;
    private RequestProcessor.Task nodeSetterTask;
    private final Object NODE_SETTER_LOCK = new Object();
    private final Object CUR_NODES_LOCK = new Object();
    private static final Lookup.Template<Node> CUR_NODES = new Lookup.Template(Node.class);
    private static final Lookup.Template<NavigatorLookupHint> CUR_HINTS = new Lookup.Template(NavigatorLookupHint.class);
    private Lookup.Result<Node> curNodesRes;
    private Lookup.Result<NavigatorLookupHint> curHintsRes;
    private Collection<? extends Node> curNodes = Collections.emptyList();
    private final ClientsLookup clientsLookup;
    private final Lookup panelLookup;
    private final PanelLookupWithNodes panelLookupWithNodes;
    private Lookup.Result<Node> panelLookupNodesResult;
    private final LookupListener panelLookupListener;
    private Reference<TopComponent> lastActivatedRef;
    private List<NodeListener> weakNodesL = Collections.emptyList();
    private boolean inUpdate;
    private static final Logger LOG = Logger.getLogger(NavigatorController.class.getName());
    private boolean closed;
    private RequestProcessor requestProcessor = new RequestProcessor(NavigatorController.class);
    private boolean updateWhenActivated = false;
    private boolean tcShown;
    private boolean updateWhenNotShown = false;
    private boolean tcActivating = false;
    private boolean uiready;

    public NavigatorController(NavigatorDisplayer navigatorTC) {
        this.navigatorTC = navigatorTC;
        this.clientsLookup = new ClientsLookup();
        this.panelLookup = Lookups.proxy((Lookup.Provider)new PanelLookupWrapper());
        this.panelLookupWithNodes = new PanelLookupWithNodes();
        this.panelLookupListener = new PanelLookupListener();
        navigatorTC.addPropertyChangeListener(this);
        if (navigatorTC != navigatorTC.getTopComponent()) {
            navigatorTC.getTopComponent().addPropertyChangeListener((PropertyChangeListener)this);
        }
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
        this.installActions();
    }

    private void navigatorTCOpened() {
        if (this.panelLookupNodesResult != null) {
            return;
        }
        LOG.fine("Entering navigatorTCOpened");
        Lookup globalContext = Utilities.actionsGlobalContext();
        this.curNodesRes = globalContext.lookup(CUR_NODES);
        this.curNodesRes.addLookupListener((LookupListener)this);
        this.curHintsRes = globalContext.lookup(CUR_HINTS);
        this.curHintsRes.addLookupListener((LookupListener)this);
        this.panelLookupNodesResult = this.panelLookup.lookup(CUR_NODES);
        this.panelLookupNodesResult.addLookupListener(this.panelLookupListener);
        this.updateContext((NavigatorLookupPanelsPolicy)globalContext.lookup(NavigatorLookupPanelsPolicy.class), globalContext.lookupAll(NavigatorLookupHint.class));
        this.closed = false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void navigatorTCClosed() {
        if (this.panelLookupNodesResult == null || this.closed) {
            return;
        }
        LOG.fine("Entering navigatorTCClosed");
        this.curNodesRes.removeLookupListener((LookupListener)this);
        this.curHintsRes.removeLookupListener((LookupListener)this);
        this.panelLookupNodesResult.removeLookupListener(this.panelLookupListener);
        this.curNodesRes = null;
        this.curHintsRes = null;
        Object object = this.CUR_NODES_LOCK;
        synchronized (object) {
            this.curNodes = Collections.emptyList();
        }
        this.weakNodesL = Collections.emptyList();
        this.clientsLookup.lookup(Object.class);
        this.panelLookupWithNodes.setNodes(null);
        NavigatorPanel selPanel = this.navigatorTC.getSelectedPanel();
        if (selPanel != null) {
            selPanel.panelDeactivated();
        }
        this.lastActivatedRef = null;
        this.currentPanels = null;
        this.navigatorTC.setPanels(null, null);
        this.panelLookupNodesResult = null;
        LOG.fine("navigatorTCClosed: activated nodes: " + this.navigatorTC.getTopComponent().getActivatedNodes());
        if (this.navigatorTC.getTopComponent().getActivatedNodes() != null) {
            LOG.fine("navigatorTCClosed: clearing act nodes...");
            this.navigatorTC.getTopComponent().setActivatedNodes(new Node[0]);
        }
        this.closed = true;
    }

    public Lookup getPanelLookup() {
        return this.panelLookupWithNodes;
    }

    public void activatePanel(NavigatorPanel panel) {
        LOG.fine("activatePanel - entered, panel: " + panel);
        String iaeText = "Panel is not available for activation: ";
        if (this.currentPanels == null) {
            if (this.inUpdate) {
                LOG.fine("activatePanel - premature exit - currentPanels == null, inUpdate == true");
                this.cacheLastSelPanel(panel);
                return;
            }
            throw new IllegalArgumentException(iaeText + panel);
        }
        Object toActivate = null;
        boolean contains = false;
        for (NavigatorPanel navigatorPanel : this.currentPanels) {
            contains = navigatorPanel instanceof LazyPanel ? ((LazyPanel)navigatorPanel).panelMatch(panel) : navigatorPanel.equals(panel);
            if (!contains) continue;
            toActivate = navigatorPanel;
            break;
        }
        if (!contains) {
            if (this.inUpdate) {
                LOG.fine("activatePanel - premature exit - panel is not contained in currenPanels");
                this.cacheLastSelPanel(panel);
                return;
            }
            throw new IllegalArgumentException(iaeText + panel);
        }
        NavigatorPanel oldPanel = this.navigatorTC.getSelectedPanel();
        if (!toActivate.equals(oldPanel)) {
            if (oldPanel != null) {
                oldPanel.panelDeactivated();
            }
            toActivate.panelActivated((Lookup)this.clientsLookup);
            this.navigatorTC.setSelectedPanel((NavigatorPanel)toActivate);
            this.panelLookup.lookup(Object.class);
            LOG.fine("activatePanel - normal exit - caching panel: " + panel);
            this.cacheLastSelPanel((NavigatorPanel)toActivate);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resultChanged(LookupEvent ev) {
        if (!this.navigatorTC.getTopComponent().equals((Object)WindowManager.getDefault().getRegistry().getActivated()) || this.curNodes == null || this.curNodes.isEmpty()) {
            Lookup globalContext = Utilities.actionsGlobalContext();
            NavigatorLookupPanelsPolicy panelsPolicy = (NavigatorLookupPanelsPolicy)globalContext.lookup(NavigatorLookupPanelsPolicy.class);
            Collection lkpHints = globalContext.lookupAll(NavigatorLookupHint.class);
            ActNodeSetter nodeSetter = new ActNodeSetter(panelsPolicy, lkpHints);
            if (this.navigatorTC.allowAsyncUpdate()) {
                Object object = this.NODE_SETTER_LOCK;
                synchronized (object) {
                    if (this.nodeSetterTask != null) {
                        this.nodeSetterTask.cancel();
                    }
                    this.nodeSetterTask = RequestProcessor.getDefault().post((Runnable)nodeSetter, 100);
                    this.nodeSetterTask.addTaskListener((TaskListener)nodeSetter);
                }
            } else {
                nodeSetter.run();
            }
        }
    }

    private boolean shouldUpdate() {
        Node[] nodes = TopComponent.getRegistry().getCurrentNodes();
        return nodes != null && nodes.length > 0 || Utilities.actionsGlobalContext().lookup(NavigatorLookupHint.class) != null;
    }

    private void updateContext(NavigatorLookupPanelsPolicy panelsPolicy, Collection<? extends NavigatorLookupHint> lkpHints) {
        this.updateContext(false, panelsPolicy, lkpHints);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateContext(final boolean force, final NavigatorLookupPanelsPolicy panelsPolicy, final Collection<? extends NavigatorLookupHint> lkpHints) {
        block16 : {
            LOG.log(Level.FINE, "updateContext entered, force: {0}", force);
            if (!this.tcShown && !this.updateWhenNotShown) {
                LOG.log(Level.FINE, "Exit because TC is not showing - no need to refresh");
                this.updateWhenActivated = true;
                return;
            }
            if (this.inUpdate) {
                LOG.fine("Exit because inUpdate already, force: " + force);
                return;
            }
            boolean loadingProviders = false;
            this.inUpdate = true;
            this.navigatorTC.getTopComponent().makeBusy(true);
            try {
                if (this.curNodesRes == null) {
                    LOG.fine("Exit because curNodesRes is null, force: " + force);
                    return;
                }
                final Collection nodes = this.curNodesRes.allInstances();
                if (nodes.isEmpty() && !this.shouldUpdate() && !force) {
                    LOG.fine("Exit because act nodes empty, force: " + force);
                    return;
                }
                Object object = this.CUR_NODES_LOCK;
                synchronized (object) {
                    Iterator<NodeListener> curL = this.weakNodesL.iterator();
                    LOG.log(Level.FINE, "Removing {0} node listener(s)", this.curNodes.size());
                    Iterator<? extends Node> curNode = this.curNodes.iterator();
                    while (curNode.hasNext()) {
                        curNode.next().removeNodeListener(curL.next());
                    }
                    this.weakNodesL = new ArrayList<NodeListener>(nodes.size());
                    this.curNodes = nodes;
                    LOG.fine("new CurNodes size " + this.curNodes.size());
                    NodeListener weakNodeL = null;
                    for (Node curNode2 : this.curNodes) {
                        weakNodeL = (NodeListener)WeakListeners.create(NodeListener.class, (EventListener)this, (Object)curNode2);
                        this.weakNodesL.add(weakNodeL);
                        curNode2.addNodeListener(weakNodeL);
                    }
                }
                loadingProviders = true;
                if (this.navigatorTC.allowAsyncUpdate()) {
                    this.requestProcessor.post(new Runnable(){

                        @Override
                        public void run() {
                            final List providers = NavigatorController.this.obtainProviders(nodes, panelsPolicy, lkpHints);
                            final String mime = NavigatorController.this.findMimeForContext(lkpHints);
                            NavigatorController.this.runWhenUIReady(new Runnable(){

                                @Override
                                public void run() {
                                    NavigatorController.this.showProviders(providers, mime, force);
                                }
                            });
                        }

                    });
                    break block16;
                }
                this.showProviders(this.obtainProviders(nodes, panelsPolicy, lkpHints), this.findMimeForContext(lkpHints), force);
            }
            finally {
                if (!loadingProviders) {
                    this.inUpdate = false;
                    this.navigatorTC.getTopComponent().makeBusy(false);
                }
            }
        }
    }

    private void runWhenUIReady(final Runnable runnable) {
        if (this.uiready) {
            SwingUtilities.invokeLater(runnable);
        } else {
            WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

                @Override
                public void run() {
                    NavigatorController.this.uiready = true;
                    runnable.run();
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void showProviders(List<NavigatorPanel> providers, String mimeType, boolean force) {
        try {
            boolean areNewProviders;
            List<NavigatorPanel> oldProviders = this.currentPanels;
            boolean bl = areNewProviders = providers != null && !providers.isEmpty();
            if (oldProviders == null && providers == null) {
                LOG.fine("Exit because nav remain empty, force: " + force);
                return;
            }
            NavigatorPanel selPanel = this.navigatorTC.getSelectedPanel();
            if (oldProviders != null && oldProviders.contains(selPanel) && providers != null && providers.contains(selPanel)) {
                this.clientsLookup.lookup(Node.class);
                if (!oldProviders.equals(providers)) {
                    this.currentPanels = providers;
                    this.navigatorTC.setPanels(providers, selPanel);
                }
                this.updateActNodesAndTitle();
                LOG.fine("Exit because same provider and panel, notified. Force: " + force);
                return;
            }
            if (selPanel != null) {
                if (!areNewProviders && !force) {
                    LOG.fine("Exit because no new providers, force: " + force);
                    return;
                }
                selPanel.panelDeactivated();
            }
            this.clientsLookup.lookup(Node.class);
            NavigatorPanel newSel = null;
            if (areNewProviders) {
                newSel = this.getLastSelPanel(providers, mimeType);
                if (newSel == null) {
                    newSel = providers.get(0);
                }
                newSel.panelActivated((Lookup)this.clientsLookup);
            }
            this.currentPanels = providers;
            this.navigatorTC.setPanels(providers, newSel);
            this.panelLookup.lookup(Object.class);
            this.updateActNodesAndTitle();
            this.updateWhenActivated = false;
            LOG.fine("Normal exit, change to new provider, force: " + force);
        }
        finally {
            this.inUpdate = false;
            this.navigatorTC.getTopComponent().makeBusy(false);
            if (this.tcActivating && this.navigatorTC.allowAsyncUpdate()) {
                this.navigatorTC.getTopComponent().requestFocus();
                this.tcActivating = false;
            }
        }
    }

    boolean isInUpdate() {
        return this.inUpdate;
    }

    private void updateActNodesAndTitle() {
        LOG.fine("updateActNodesAndTitle called...");
        Node[] actNodes = this.obtainActivatedNodes();
        this.panelLookupWithNodes.setNodes(actNodes);
        this.updateTCTitle(actNodes);
    }

    private void updateTCTitle(Node[] nodes) {
        Node node;
        DataObject dObj;
        String newTitle = nodes != null && nodes.length > 0 ? ((dObj = this.obtainNodeDO(node = nodes[0])) != null && dObj.isValid() || this.updateWhenNotShown ? NbBundle.getMessage(NavigatorTC.class, (String)"FMT_Navigator", (Object)node.getDisplayName()) : NbBundle.getMessage(NavigatorTC.class, (String)"LBL_Navigator")) : NbBundle.getMessage(NavigatorTC.class, (String)"LBL_Navigator");
        this.navigatorTC.setDisplayName(newTitle);
    }

    List<NavigatorPanel> obtainProviders(Collection<? extends Node> nodes) {
        Lookup globalContext = Utilities.actionsGlobalContext();
        NavigatorLookupPanelsPolicy panelsPolicy = (NavigatorLookupPanelsPolicy)globalContext.lookup(NavigatorLookupPanelsPolicy.class);
        Collection lkpHints = globalContext.lookupAll(NavigatorLookupHint.class);
        return this.obtainProviders(nodes, panelsPolicy, lkpHints);
    }

    private List<NavigatorPanel> obtainProviders(Collection<? extends Node> nodes, NavigatorLookupPanelsPolicy panelsPolicy, Collection<? extends NavigatorLookupHint> lkpHints) {
        ArrayList<? extends NavigatorPanel> result = null;
        for (NavigatorLookupHint curHint : lkpHints) {
            Collection<? extends NavigatorPanel> providers = ProviderRegistry.getInstance().getProviders(curHint.getContentType());
            if (providers == null || providers.isEmpty()) continue;
            if (result == null) {
                result = new ArrayList<NavigatorPanel>(providers.size() * lkpHints.size());
            }
            for (NavigatorPanel np : providers) {
                if (result.contains(np)) continue;
                result.add(np);
            }
        }
        if (panelsPolicy != null && panelsPolicy.getPanelsPolicy() == 1) {
            return result;
        }
        ArrayList<? extends NavigatorPanel> fileResult = null;
        for (Node node : nodes) {
            DataObject dObj = this.obtainNodeDO(node);
            if (dObj == null || !dObj.isValid()) {
                fileResult = null;
                break;
            }
            FileObject fo = dObj.getPrimaryFile();
            if (fo.isVirtual()) {
                fileResult = null;
                break;
            }
            String contentType = fo.getMIMEType();
            Collection<? extends NavigatorPanel> providers = ProviderRegistry.getInstance().getProviders(contentType);
            if (providers == null || providers.isEmpty()) {
                fileResult = null;
                break;
            }
            LOG.fine("File mime type providers size: " + providers.size());
            if (fileResult == null) {
                fileResult = new ArrayList(providers.size());
                fileResult.addAll(providers);
                continue;
            }
            fileResult.retainAll(providers);
        }
        if (result != null) {
            if (fileResult != null) {
                for (NavigatorPanel np : fileResult) {
                    if (result.contains(np)) continue;
                    result.add(np);
                }
            }
        } else {
            result = fileResult;
        }
        return result;
    }

    private Node[] obtainActivatedNodes() {
        Lookup selLookup = this.getSelectedPanelLookup();
        if (selLookup == null) {
            return this.curNodes.toArray((T[])new Node[0]);
        }
        return selLookup.lookupAll(Node.class).toArray((T[])new Node[0]);
    }

    private DataObject obtainNodeDO(Node node) {
        DataObject dObj = (DataObject)node.getLookup().lookup(DataObject.class);
        while (dObj instanceof DataShadow) {
            dObj = ((DataShadow)dObj).getOriginal();
        }
        return dObj;
    }

    public void installActions() {
        KeyStroke returnKey = KeyStroke.getKeyStroke(27, 0, false);
        this.navigatorTC.getTopComponent().getInputMap(1).put(returnKey, "return");
        this.navigatorTC.getTopComponent().getActionMap().put("return", new ESCHandler());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("activated".equals(evt.getPropertyName())) {
            TopComponent tc = TopComponent.getRegistry().getActivated();
            if (tc != null && tc != this.navigatorTC) {
                this.lastActivatedRef = new WeakReference<TopComponent>(tc);
            }
        } else if ("tcOpened".equals(evt.getPropertyName())) {
            if (evt.getNewValue() == this.navigatorTC.getTopComponent()) {
                this.navigatorTCOpened();
            }
        } else if ("tcClosed".equals(evt.getPropertyName())) {
            if (evt.getNewValue() == this.navigatorTC.getTopComponent()) {
                this.navigatorTCClosed();
            } else if (this.panelLookupNodesResult != null) {
                LOG.fine("Component closed, invoking update through invokeLater...");
                TopComponent tc = TopComponent.getRegistry().getActivated();
                if (tc == this.navigatorTC.getTopComponent()) {
                    LOG.fine("navigator active, clearing its activated nodes");
                    this.navigatorTC.getTopComponent().setActivatedNodes(new Node[0]);
                }
                Lookup globalContext = Utilities.actionsGlobalContext();
                NavigatorLookupPanelsPolicy panelsPolicy = (NavigatorLookupPanelsPolicy)globalContext.lookup(NavigatorLookupPanelsPolicy.class);
                Collection lkpHints = globalContext.lookupAll(NavigatorLookupHint.class);
                EventQueue.invokeLater(this.getUpdateRunnable(false, panelsPolicy, lkpHints));
            }
        } else if ("navigatorPanelSelection".equals(evt.getPropertyName())) {
            this.activatePanel((NavigatorPanel)evt.getNewValue());
        } else if ("ancestor".equals(evt.getPropertyName()) && evt.getSource() == this.navigatorTC.getTopComponent()) {
            boolean shown = evt.getNewValue() != null;
            this.makeActive(shown);
        }
    }

    public void nodeDestroyed(NodeEvent ev) {
        LOG.fine("Node destroyed reaction...");
        if (this.navigatorTC.getTopComponent().equals((Object)WindowManager.getDefault().getRegistry().getActivated())) {
            LOG.fine("NavigatorTC active, skipping node destroyed reaction.");
            return;
        }
        LOG.fine("invokeLater on updateContext from node destroyed reaction...");
        Lookup globalContext = Utilities.actionsGlobalContext();
        NavigatorLookupPanelsPolicy panelsPolicy = (NavigatorLookupPanelsPolicy)globalContext.lookup(NavigatorLookupPanelsPolicy.class);
        Collection lkpHints = globalContext.lookupAll(NavigatorLookupHint.class);
        EventQueue.invokeLater(this.getUpdateRunnable(true, panelsPolicy, lkpHints));
    }

    public void childrenAdded(NodeMemberEvent ev) {
    }

    public void childrenRemoved(NodeMemberEvent ev) {
    }

    public void childrenReordered(NodeReorderEvent ev) {
    }

    public Runnable getUpdateRunnable(final boolean force, final NavigatorLookupPanelsPolicy panelsPolicy, final Collection<? extends NavigatorLookupHint> lkpHints) {
        return new Runnable(){

            @Override
            public void run() {
                NavigatorController.this.updateContext(force, panelsPolicy, lkpHints);
            }
        };
    }

    private void cacheLastSelPanel(final NavigatorPanel panel) {
        final Collection hints = this.curHintsRes != null ? this.curHintsRes.allInstances() : null;
        this.requestProcessor.post(new Runnable(){

            @Override
            public void run() {
                LOG.fine("cacheLastSelPanel - looking for mime");
                String mime = NavigatorController.this.findMimeForContext(hints);
                if (mime != null) {
                    String className = panel.getClass().getName();
                    NbPreferences.forModule(NavigatorController.class).put(mime, className);
                    LOG.fine("cacheLastSelPanel - cached " + className + "for mime " + mime);
                }
            }
        });
    }

    private NavigatorPanel getLastSelPanel(List<NavigatorPanel> panels, String mime) {
        if (mime == null) {
            return null;
        }
        String className = NbPreferences.forModule(NavigatorController.class).get(mime, null);
        if (className == null) {
            return null;
        }
        LOG.fine("getLastSelPanel - found cached " + className + "for mime " + mime);
        for (NavigatorPanel curPanel : panels) {
            if (!className.equals(curPanel.getClass().getName())) continue;
            LOG.fine("getLastSelPanel - returning cached " + className + "for mime " + mime);
            return curPanel;
        }
        return null;
    }

    private String findMimeForContext(Collection<? extends NavigatorLookupHint> lkpHints) {
        assert (!SwingUtilities.isEventDispatchThread() || !this.navigatorTC.allowAsyncUpdate());
        LOG.fine("findMimeForContext - looking for mime, lkpHints= " + lkpHints);
        if (lkpHints != null && !lkpHints.isEmpty()) {
            String mimeType = lkpHints.iterator().next().getContentType();
            LOG.fine("findMimeForContext - found mime for hints, mime: " + mimeType);
            return mimeType;
        }
        FileObject fob = this.getCurrentFileObject();
        LOG.fine("findMimeForContext - looking for mime, fob= " + (Object)fob);
        if (fob != null) {
            String mimeType = fob.getMIMEType();
            LOG.fine("findMimeForContext - found mime for FO, mime: " + mimeType);
            return mimeType;
        }
        LOG.fine("findMimeForContext - NO mime found");
        return null;
    }

    void makeActive(boolean tcShown) {
        boolean oldValue = this.tcShown;
        this.tcShown = tcShown;
        if (tcShown && tcShown != oldValue && this.updateWhenActivated) {
            this.updateWhenActivated = false;
            this.tcActivating = true;
            Lookup globalContext = Utilities.actionsGlobalContext();
            this.updateContext((NavigatorLookupPanelsPolicy)globalContext.lookup(NavigatorLookupPanelsPolicy.class), globalContext.lookupAll(NavigatorLookupHint.class));
        }
    }

    private FileObject getCurrentFileObject() {
        if (this.curNodesRes != null) {
            for (Node node : this.curNodesRes.allInstances()) {
                FileObject fo = (FileObject)node.getLookup().lookup(FileObject.class);
                if (fo == null) continue;
                return fo;
            }
        }
        return null;
    }

    private Lookup getSelectedPanelLookup() {
        Lookup panelLkp;
        NavigatorPanel selPanel = this.navigatorTC.getSelectedPanel();
        if (selPanel != null && (panelLkp = selPanel.getLookup()) != null) {
            return panelLkp;
        }
        return null;
    }

    void setUpdateWhenNotShown(boolean updateWhenNotShown) {
        this.updateWhenNotShown = updateWhenNotShown;
    }

    ClientsLookup getClientsLookup() {
        return this.clientsLookup;
    }

    class ClientsLookup
    extends ProxyLookup {
        ClientsLookup() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void beforeLookup(Lookup.Template<?> template) {
            Lookup[] curNodesLookups;
            super.beforeLookup(template);
            Object object = NavigatorController.this.CUR_NODES_LOCK;
            synchronized (object) {
                curNodesLookups = new Lookup[NavigatorController.this.curNodes.size()];
                int i = 0;
                Iterator it = NavigatorController.this.curNodes.iterator();
                while (it.hasNext()) {
                    curNodesLookups[i] = ((Node)it.next()).getLookup();
                    ++i;
                }
            }
            this.setLookups(curNodesLookups);
        }

        Lookup[] obtainLookups() {
            return this.getLookups();
        }
    }

    private class ActNodeSetter
    implements Runnable,
    TaskListener {
        private NavigatorLookupPanelsPolicy panelsPolicy;
        private Collection<? extends NavigatorLookupHint> lkpHints;

        public ActNodeSetter(NavigatorLookupPanelsPolicy panelsPolicy, Collection<? extends NavigatorLookupHint> lkpHints) {
            this.panelsPolicy = panelsPolicy;
            this.lkpHints = lkpHints;
        }

        @Override
        public void run() {
            if (RequestProcessor.getDefault().isRequestProcessorThread()) {
                LOG.fine("invokeLater on updateContext from ActNodeSetter");
                SwingUtilities.invokeLater(NavigatorController.this.getUpdateRunnable(false, this.panelsPolicy, this.lkpHints));
            } else {
                LOG.fine("Calling updateContext from ActNodeSetter");
                NavigatorController.this.updateContext(this.panelsPolicy, this.lkpHints);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void taskFinished(Task task) {
            Object object = NavigatorController.this.NODE_SETTER_LOCK;
            synchronized (object) {
                if (task == NavigatorController.this.nodeSetterTask) {
                    NavigatorController.this.nodeSetterTask = null;
                }
            }
        }
    }

    private class PanelLookupWithNodes
    extends ProxyLookup {
        PanelLookupWithNodes() {
            this.setLookups(new Lookup[]{NavigatorController.this.panelLookup});
        }

        void setNodes(Node[] nodes) {
            if (nodes != null && nodes.length > 0) {
                LinkedList<Lookup> l = new LinkedList<Lookup>();
                l.add(NavigatorController.this.panelLookup);
                for (Node n : nodes) {
                    if (NavigatorController.this.panelLookup.lookupResult(Object.class).allInstances().containsAll(n.getLookup().lookupResult(Object.class).allInstances())) continue;
                    l.add(n.getLookup());
                }
                Lookup[] lookups = l.toArray((T[])new Lookup[l.size()]);
                this.setLookups(lookups);
            } else {
                this.setLookups(new Lookup[]{NavigatorController.this.panelLookup});
            }
        }
    }

    private final class PanelLookupListener
    implements LookupListener,
    Runnable {
        private PanelLookupListener() {
        }

        public void resultChanged(LookupEvent ev) {
            if (SwingUtilities.isEventDispatchThread()) {
                this.run();
            } else {
                SwingUtilities.invokeLater(this);
            }
        }

        @Override
        public void run() {
            NavigatorController.this.updateActNodesAndTitle();
        }
    }

    private final class PanelLookupWrapper
    implements Lookup.Provider {
        private PanelLookupWrapper() {
        }

        public Lookup getLookup() {
            Lookup selLookup = NavigatorController.this.getSelectedPanelLookup();
            return selLookup != null ? selLookup : Lookup.EMPTY;
        }
    }

    private class ESCHandler
    extends AbstractAction {
        private ESCHandler() {
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            Component focusOwner = FocusManager.getCurrentManager().getFocusOwner();
            if (NavigatorController.this.lastActivatedRef == null || focusOwner == null || !SwingUtilities.isDescendingFrom(focusOwner, (Component)NavigatorController.this.navigatorTC.getTopComponent()) || focusOwner instanceof JComboBox) {
                return;
            }
            TopComponent prevFocusedTc = (TopComponent)NavigatorController.this.lastActivatedRef.get();
            if (prevFocusedTc != null) {
                prevFocusedTc.requestActive();
            }
        }
    }

}

