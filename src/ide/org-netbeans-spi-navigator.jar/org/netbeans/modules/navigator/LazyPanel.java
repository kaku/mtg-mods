/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.UndoRedo
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.navigator;

import java.util.Map;
import javax.swing.JComponent;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.netbeans.spi.navigator.NavigatorPanelWithToolbar;
import org.netbeans.spi.navigator.NavigatorPanelWithUndo;
import org.openide.awt.UndoRedo;
import org.openide.util.Lookup;

public class LazyPanel
implements NavigatorPanelWithUndo,
NavigatorPanelWithToolbar {
    private final Map<String, ?> attrs;
    private NavigatorPanel delegate;

    public static NavigatorPanel create(Map<String, ?> attrs) {
        return new LazyPanel(attrs);
    }

    private LazyPanel(Map<String, ?> attrs) {
        this.attrs = attrs;
    }

    private synchronized NavigatorPanel initialize() {
        if (this.delegate == null) {
            this.delegate = (NavigatorPanel)this.attrs.get("delegate");
        }
        return this.delegate;
    }

    @Override
    public String getDisplayName() {
        if (this.delegate != null) {
            return this.delegate.getDisplayName();
        }
        return (String)this.attrs.get("displayName");
    }

    @Override
    public String getDisplayHint() {
        if (this.delegate != null) {
            return this.delegate.getDisplayHint();
        }
        return (String)this.attrs.get("displayName");
    }

    @Override
    public JComponent getComponent() {
        return this.initialize().getComponent();
    }

    @Override
    public void panelActivated(Lookup context) {
        this.initialize().panelActivated(context);
    }

    @Override
    public void panelDeactivated() {
        this.initialize().panelDeactivated();
    }

    @Override
    public Lookup getLookup() {
        return this.initialize().getLookup();
    }

    @Override
    public UndoRedo getUndoRedo() {
        NavigatorPanel p = this.initialize();
        return p instanceof NavigatorPanelWithUndo ? ((NavigatorPanelWithUndo)p).getUndoRedo() : UndoRedo.NONE;
    }

    @Override
    public JComponent getToolbarComponent() {
        NavigatorPanel p = this.initialize();
        return p instanceof NavigatorPanelWithToolbar ? ((NavigatorPanelWithToolbar)p).getToolbarComponent() : null;
    }

    public boolean panelMatch(NavigatorPanel panel) {
        if (panel == null) {
            return false;
        }
        if (this.getClass().equals(panel.getClass())) {
            return super.equals(panel);
        }
        if (this.delegate != null) {
            return this.delegate.equals(panel);
        }
        if (panel.getDisplayName().equals(this.attrs.get("displayName"))) {
            return this.initialize().equals(panel);
        }
        return false;
    }
}

