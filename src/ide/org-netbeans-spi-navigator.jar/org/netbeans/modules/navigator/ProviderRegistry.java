/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.navigator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

class ProviderRegistry {
    static final String PANELS_FOLDER = "Navigator/Panels/";
    private static final Lookup.Template<NavigatorPanel> NAV_PANEL_TEMPLATE = new Lookup.Template(NavigatorPanel.class);
    private static ProviderRegistry instance;
    private Map<String, Collection<? extends NavigatorPanel>> contentTypes2Providers;

    private ProviderRegistry() {
    }

    public static ProviderRegistry getInstance() {
        if (instance == null) {
            instance = new ProviderRegistry();
        }
        return instance;
    }

    public Collection<? extends NavigatorPanel> getProviders(String contentType) {
        Collection<? extends NavigatorPanel> result;
        if (this.contentTypes2Providers == null) {
            this.contentTypes2Providers = new HashMap<String, Collection<? extends NavigatorPanel>>(15);
        }
        if ((result = this.contentTypes2Providers.get(contentType)) == null) {
            result = this.loadProviders(contentType);
            this.contentTypes2Providers.put(contentType, result);
        }
        return result;
    }

    private Collection<? extends NavigatorPanel> loadProviders(String contentType) {
        String path = "Navigator/Panels/" + contentType;
        Lookup.Result result = Lookups.forPath((String)path).lookup(NAV_PANEL_TEMPLATE);
        return result.allInstances();
    }
}

