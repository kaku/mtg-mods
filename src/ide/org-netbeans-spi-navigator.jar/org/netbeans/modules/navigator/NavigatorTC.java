/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.awt.UndoRedo
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package org.netbeans.modules.navigator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Logger;
import javax.accessibility.AccessibleContext;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.modules.navigator.NavigatorController;
import org.netbeans.spi.navigator.NavigatorDisplayer;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.netbeans.spi.navigator.NavigatorPanelWithToolbar;
import org.netbeans.spi.navigator.NavigatorPanelWithUndo;
import org.openide.ErrorManager;
import org.openide.awt.UndoRedo;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class NavigatorTC
extends TopComponent
implements NavigatorDisplayer {
    private static NavigatorTC instance;
    private NavigatorPanel selectedPanel;
    private List<? extends NavigatorPanel> panels;
    private NavigatorController controller;
    private final JLabel notAvailLbl = new JLabel(NbBundle.getMessage(NavigatorTC.class, (String)"MSG_NotAvailable"));
    private ActionListener panelSelectionListener;
    private JComponent toolbarComponent;
    private JPanel contentArea;
    private JPanel holderPanel;
    private JComboBox panelSelector;
    private JPanel pnlToolbar;

    private NavigatorTC() {
        this.initComponents();
        this.setName(NbBundle.getMessage(NavigatorTC.class, (String)"LBL_Navigator"));
        this.setIcon(ImageUtilities.loadImage((String)"org/netbeans/modules/navigator/resources/navigator.png"));
        this.setFocusable(true);
        this.putClientProperty((Object)"SlidingName", (Object)this.getName());
        this.getAccessibleContext().setAccessibleDescription(NbBundle.getMessage(NavigatorTC.class, (String)"ACC_DESC_NavigatorTC"));
        this.notAvailLbl.setHorizontalAlignment(0);
        this.notAvailLbl.setEnabled(false);
        Color usualWindowBkg = UIManager.getColor("Tree.background");
        this.notAvailLbl.setBackground(usualWindowBkg != null ? usualWindowBkg : Color.white);
        this.notAvailLbl.setOpaque(true);
        this.holderPanel.setOpaque(false);
        this.panelSelectionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int idx = NavigatorTC.this.panelSelector.getSelectedIndex();
                if (NavigatorTC.this.panels != null && idx >= 0 && idx < NavigatorTC.this.panels.size() && NavigatorTC.this.panels.get(idx) != NavigatorTC.this.selectedPanel) {
                    NavigatorTC.this.firePropertyChange("navigatorPanelSelection", NavigatorTC.this.selectedPanel, NavigatorTC.this.panels.get(idx));
                }
            }
        };
        this.panelSelector.addActionListener(this.panelSelectionListener);
        this.associateLookup((Lookup)new ProxyLookup(new Lookup[]{Lookups.singleton((Object)this.getActionMap()), this.getController().getPanelLookup()}));
        this.setToEmpty();
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            Color backColor = UIManager.getColor("NbExplorerView.background");
            this.setBackground(backColor);
            this.notAvailLbl.setBackground(backColor);
            this.setOpaque(true);
            this.holderPanel.setOpaque(true);
            this.holderPanel.setBackground(backColor);
            this.holderPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, UIManager.getColor("NbSplitPane.background")));
        }
    }

    public String getShortName() {
        return NbBundle.getMessage(NavigatorTC.class, (String)"LBL_Navigator");
    }

    public static final NavigatorTC getInstance() {
        NavigatorTC navTC = (NavigatorTC)WindowManager.getDefault().findTopComponent("navigatorTC");
        if (navTC == null) {
            navTC = NavigatorTC.privateGetInstance();
            Logger.getAnonymousLogger().warning("Could not locate the navigator component via its winsys id");
        }
        return navTC;
    }

    public static final NavigatorTC privateGetInstance() {
        if (instance == null) {
            instance = new NavigatorTC();
        }
        return instance;
    }

    @Override
    public boolean allowAsyncUpdate() {
        return true;
    }

    @Override
    public TopComponent getTopComponent() {
        return this;
    }

    @Override
    public void setSelectedPanel(NavigatorPanel panel) {
        int panelIdx = this.panels.indexOf(panel);
        assert (panelIdx != -1);
        if (panel.equals(this.selectedPanel)) {
            return;
        }
        this.selectedPanel = panel;
        assert (SwingUtilities.isEventDispatchThread());
        JComponent comp = this.selectedPanel.getComponent();
        if (comp == null) {
            NullPointerException npe = new NullPointerException("Method " + this.selectedPanel.getClass().getName() + ".getComponent() must not return null under any condition!");
            ErrorManager.getDefault().notify(4096, (Throwable)npe);
        } else {
            this.contentArea.removeAll();
            this.contentArea.add((Component)panel.getComponent(), "Center");
            this.pnlToolbar.removeAll();
            if (panel instanceof NavigatorPanelWithToolbar && ((NavigatorPanelWithToolbar)panel).getToolbarComponent() != null) {
                this.toolbarComponent = ((NavigatorPanelWithToolbar)panel).getToolbarComponent();
                GridBagConstraints gbc = new GridBagConstraints();
                gbc.anchor = 17;
                gbc.fill = 2;
                gbc.weightx = 1.0;
                this.pnlToolbar.add((Component)this.toolbarComponent, gbc);
                this.pnlToolbar.setVisible(true);
            } else {
                this.toolbarComponent = null;
                this.pnlToolbar.setVisible(false);
            }
            this.revalidate();
            this.repaint();
        }
        this.panelSelector.removeActionListener(this.panelSelectionListener);
        this.panelSelector.setSelectedIndex(panelIdx);
        this.panelSelector.addActionListener(this.panelSelectionListener);
    }

    @Override
    public NavigatorPanel getSelectedPanel() {
        return this.selectedPanel;
    }

    public List<? extends NavigatorPanel> getPanels() {
        return this.panels;
    }

    @Override
    public void setPanels(List<? extends NavigatorPanel> panels, NavigatorPanel select) {
        this.panels = panels;
        int panelsCount = panels == null ? -1 : panels.size();
        this.selectedPanel = null;
        this.toolbarComponent = null;
        this.panelSelector.removeActionListener(this.panelSelectionListener);
        this.contentArea.removeAll();
        this.panelSelector.removeAllItems();
        if (panelsCount <= 0) {
            this.setToEmpty();
        } else {
            this.holderPanel.setVisible(panelsCount != 1 || select instanceof NavigatorPanelWithToolbar && ((NavigatorPanelWithToolbar)select).getToolbarComponent() != null);
            boolean selectFound = false;
            for (NavigatorPanel curPanel : panels) {
                this.panelSelector.addItem(curPanel.getDisplayName());
                if (curPanel != select) continue;
                selectFound = true;
            }
            this.panelSelector.addActionListener(this.panelSelectionListener);
            if (selectFound) {
                this.setSelectedPanel(select);
            } else {
                this.setSelectedPanel(panels.get(0));
            }
            this.resetFromEmpty();
        }
    }

    JComboBox getPanelSelector() {
        return this.panelSelector;
    }

    public String preferredID() {
        return "navigatorTC";
    }

    public int getPersistenceType() {
        return 0;
    }

    public boolean requestFocusInWindow() {
        if (this.selectedPanel != null) {
            return this.selectedPanel.getComponent().requestFocusInWindow();
        }
        return super.requestFocusInWindow();
    }

    public void requestFocus() {
        if (this.selectedPanel != null) {
            this.selectedPanel.getComponent().requestFocus();
        } else {
            super.requestFocus();
        }
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("navigator.java");
    }

    public UndoRedo getUndoRedo() {
        if (this.selectedPanel == null || !(this.selectedPanel instanceof NavigatorPanelWithUndo)) {
            return UndoRedo.NONE;
        }
        return ((NavigatorPanelWithUndo)this.selectedPanel).getUndoRedo();
    }

    public NavigatorController getController() {
        if (this.controller == null) {
            this.controller = new NavigatorController(this);
        }
        return this.controller;
    }

    JComponent getToolbar() {
        return this.toolbarComponent;
    }

    private void setToEmpty() {
        if (this.notAvailLbl.isShowing()) {
            return;
        }
        this.remove((Component)this.holderPanel);
        this.holderPanel.setVisible(false);
        this.remove((Component)this.contentArea);
        this.add((Component)this.notAvailLbl, (Object)"Center");
        this.revalidate();
        this.repaint();
    }

    private void resetFromEmpty() {
        if (this.contentArea.isShowing()) {
            // empty if block
        }
        this.remove((Component)this.notAvailLbl);
        this.add((Component)this.holderPanel, (Object)"North");
        this.add((Component)this.contentArea, (Object)"Center");
        this.revalidate();
        this.repaint();
    }

    private void initComponents() {
        this.holderPanel = new JPanel();
        this.panelSelector = new JComboBox();
        this.pnlToolbar = new JPanel();
        this.contentArea = new JPanel();
        this.setLayout((LayoutManager)new BorderLayout());
        this.holderPanel.setLayout(new GridBagLayout());
        this.panelSelector.setMinimumSize(new Dimension(100, 20));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = -1;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        this.holderPanel.add((Component)this.panelSelector, gridBagConstraints);
        this.pnlToolbar.setOpaque(false);
        this.pnlToolbar.setLayout(new GridBagLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridwidth = -1;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 3, 0, 0);
        this.holderPanel.add((Component)this.pnlToolbar, gridBagConstraints);
        this.add((Component)this.holderPanel, (Object)"North");
        this.contentArea.setLayout(new BorderLayout());
        this.add((Component)this.contentArea, (Object)"Center");
    }

}

