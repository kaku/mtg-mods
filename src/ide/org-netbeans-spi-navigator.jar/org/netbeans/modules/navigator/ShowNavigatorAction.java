/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package org.netbeans.modules.navigator;

import org.netbeans.modules.navigator.NavigatorTC;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public class ShowNavigatorAction
extends CallableSystemAction {
    public void performAction() {
        NavigatorTC navTC = NavigatorTC.getInstance();
        navTC.open();
        navTC.requestActive();
    }

    public String getName() {
        return NbBundle.getMessage(ShowNavigatorAction.class, (String)"LBL_Action");
    }

    protected String iconResource() {
        return "org/netbeans/modules/navigator/resources/navigator.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public boolean asynchronous() {
        return false;
    }
}

