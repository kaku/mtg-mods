/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package org.netbeans.modules.navigator;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import org.netbeans.modules.navigator.LazyPanel;
import org.netbeans.spi.navigator.NavigatorPanel;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public class NavigatorPanelRegistrationProcessor
extends LayerGeneratingProcessor {
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(NavigatorPanel.Registration.class.getCanonicalName(), NavigatorPanel.Registrations.class.getCanonicalName()));
    }

    protected boolean handleProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) throws LayerGenerationException {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (Element e2 : roundEnv.getElementsAnnotatedWith(NavigatorPanel.Registration.class)) {
            NavigatorPanel.Registration r = e2.getAnnotation(NavigatorPanel.Registration.class);
            if (r == null) continue;
            this.register(e2, r);
        }
        for (Element e : roundEnv.getElementsAnnotatedWith(NavigatorPanel.Registrations.class)) {
            NavigatorPanel.Registrations rr = e.getAnnotation(NavigatorPanel.Registrations.class);
            if (rr == null) continue;
            for (NavigatorPanel.Registration r : rr.value()) {
                this.register(e, r);
            }
        }
        return true;
    }

    private void register(Element e, NavigatorPanel.Registration r) throws LayerGenerationException {
        String suffix = this.layer(new Element[]{e}).instanceFile("dummy", null, null, (Annotation)r, null).getPath().substring("dummy".length());
        this.layer(new Element[]{e}).file("Navigator/Panels/" + r.mimeType() + suffix).methodvalue("instanceCreate", LazyPanel.class.getName(), "create").instanceAttribute("delegate", NavigatorPanel.class, (Annotation)r, null).position(r.position()).bundlevalue("displayName", r.displayName()).write();
    }
}

