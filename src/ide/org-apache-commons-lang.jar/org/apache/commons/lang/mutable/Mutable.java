/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.lang.mutable;

public interface Mutable {
    public Object getValue();

    public void setValue(Object var1);
}

