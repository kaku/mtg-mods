/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.lang.text;

import java.text.Format;
import java.util.Locale;

public interface FormatFactory {
    public Format getFormat(String var1, String var2, Locale var3);
}

