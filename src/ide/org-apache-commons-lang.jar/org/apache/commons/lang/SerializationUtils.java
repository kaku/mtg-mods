/*
 * Decompiled with CFR 0_118.
 */
package org.apache.commons.lang;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import org.apache.commons.lang.SerializationException;

public class SerializationUtils {
    public static Object clone(Serializable object) {
        return SerializationUtils.deserialize(SerializationUtils.serialize(object));
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public static void serialize(Serializable obj, OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("The OutputStream must not be null");
        }
        out = null;
        try {
            try {
                out = new ObjectOutputStream(outputStream);
                out.writeObject(obj);
            }
            catch (IOException ex) {
                throw new SerializationException(ex);
            }
            var5_3 = null;
            if (out == null) return;
            out.close();
            return;
            catch (IOException ex) {
                return;
            }
        }
        catch (Throwable throwable) {
            var5_4 = null;
            ** try [egrp 2[TRYBLOCK] [3 : 56->67)] { 
lbl22: // 1 sources:
            if (out == null) throw throwable;
            out.close();
            throw throwable;
lbl25: // 1 sources:
            catch (IOException ex) {
                // empty catch block
            }
            throw throwable;
        }
    }

    public static byte[] serialize(Serializable obj) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        SerializationUtils.serialize(obj, baos);
        return baos.toByteArray();
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public static Object deserialize(InputStream inputStream) {
        if (inputStream == null) {
            throw new IllegalArgumentException("The InputStream must not be null");
        }
        in = null;
        try {
            in = new ObjectInputStream(inputStream);
            object = in.readObject();
            var4_5 = null;
            try {
                if (in == null) return object;
                in.close();
                return object;
            }
            catch (IOException ex) {
                // empty catch block
            }
            return object;
            catch (ClassNotFoundException ex) {
                throw new SerializationException(ex);
            }
            catch (IOException ex2) {
                throw new SerializationException(ex2);
            }
        }
        catch (Throwable throwable) {
            var4_6 = null;
            ** try [egrp 2[TRYBLOCK] [4 : 63->74)] { 
lbl25: // 1 sources:
            if (in == null) throw throwable;
            in.close();
            throw throwable;
lbl28: // 1 sources:
            catch (IOException ex) {
                // empty catch block
            }
            throw throwable;
        }
    }

    public static Object deserialize(byte[] objectData) {
        if (objectData == null) {
            throw new IllegalArgumentException("The byte[] must not be null");
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(objectData);
        return SerializationUtils.deserialize(bais);
    }
}

