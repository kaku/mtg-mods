/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.ExplorerManager
 */
package org.netbeans.modules.editor.breadcrumbs;

import javax.swing.text.Document;
import org.openide.explorer.ExplorerManager;

public class HolderImpl {
    private final ExplorerManager manager = new ExplorerManager();

    public static HolderImpl get(Document doc) {
        HolderImpl instance = (HolderImpl)doc.getProperty(HolderImpl.class);
        if (instance == null) {
            instance = new HolderImpl();
            doc.putProperty(HolderImpl.class, instance);
        }
        return instance;
    }

    public ExplorerManager getManager() {
        return this.manager;
    }
}

