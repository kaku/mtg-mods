/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.modules.parsing.api.Source
 *  org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.netbeans.modules.parsing.spi.SchedulerEvent
 *  org.netbeans.modules.parsing.spi.SourceModificationEvent
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.editor.breadcrumbs.support;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsController;
import org.netbeans.modules.parsing.api.Source;
import org.netbeans.modules.parsing.spi.CursorMovedSchedulerEvent;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileObject;

public final class BreadCrumbsScheduler
extends Scheduler {
    private JTextComponent currentEditor;
    private CaretListener caretListener;
    private Document currentDocument;

    public BreadCrumbsScheduler() {
        BreadcrumbsController.addBreadCrumbsEnabledListener(new AListener());
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)new EditorListener());
        this.setEditor(EditorRegistry.focusedComponent());
    }

    private void refresh() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (BreadCrumbsScheduler.this.currentEditor == null || BreadCrumbsScheduler.this.getSource() == null) {
                    return;
                }
                BreadCrumbsScheduler.this.schedule(BreadCrumbsScheduler.this.getSource(), (SchedulerEvent)new CursorMovedSchedulerEvent(this, BreadCrumbsScheduler.this.currentEditor.getCaret().getDot(), BreadCrumbsScheduler.this.currentEditor.getCaret().getMark()){});
            }

        });
    }

    protected void setEditor(JTextComponent editor) {
        if (this.currentEditor != null) {
            this.currentEditor.removeCaretListener(this.caretListener);
        }
        this.currentEditor = editor;
        if (editor != null) {
            if (this.caretListener == null) {
                this.caretListener = new ACaretListener();
            }
            editor.addCaretListener(this.caretListener);
            Document document = editor.getDocument();
            if (this.currentDocument == document) {
                return;
            }
            this.currentDocument = document;
            Source source = Source.create((Document)this.currentDocument);
            this.schedule(source, (SchedulerEvent)new CursorMovedSchedulerEvent((Object)this, editor.getCaret().getDot(), editor.getCaret().getMark()){});
        } else {
            this.currentDocument = null;
            this.schedule(null, null);
        }
    }

    public String toString() {
        return "BreadCrumbsScheduler";
    }

    protected SchedulerEvent createSchedulerEvent(SourceModificationEvent event) {
        JTextComponent ce = this.currentEditor;
        Caret caret = ce != null ? ce.getCaret() : null;
        Source s = this.getSource();
        if (event.getModifiedSource() == s && caret != null) {
            return new CursorMovedSchedulerEvent((Object)this, caret.getDot(), caret.getMark()){};
        }
        return null;
    }

    private class EditorListener
    implements PropertyChangeListener {
        private EditorListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName() == null || evt.getPropertyName().equals("focusedDocument") || evt.getPropertyName().equals("focusGained")) {
                FileObject fileObject;
                Document document;
                JTextComponent editor = EditorRegistry.focusedComponent();
                if (editor == BreadCrumbsScheduler.this.currentEditor) {
                    return;
                }
                BreadCrumbsScheduler.this.currentEditor = editor;
                if (BreadCrumbsScheduler.this.currentEditor != null && (fileObject = NbEditorUtilities.getFileObject((Document)(document = BreadCrumbsScheduler.this.currentEditor.getDocument()))) == null) {
                    return;
                }
                BreadCrumbsScheduler.this.setEditor(BreadCrumbsScheduler.this.currentEditor);
            } else if (evt.getPropertyName().equals("lastFocusedRemoved")) {
                BreadCrumbsScheduler.this.currentEditor = null;
                BreadCrumbsScheduler.this.setEditor(null);
            }
        }
    }

    private class ACaretListener
    implements CaretListener {
        private ACaretListener() {
        }

        @Override
        public void caretUpdate(CaretEvent e) {
            BreadCrumbsScheduler.this.schedule((SchedulerEvent)new CursorMovedSchedulerEvent(this, e.getDot(), e.getMark()){});
        }

    }

    private class AListener
    implements ChangeListener {
        private AListener() {
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            BreadCrumbsScheduler.this.refresh();
        }
    }

}

