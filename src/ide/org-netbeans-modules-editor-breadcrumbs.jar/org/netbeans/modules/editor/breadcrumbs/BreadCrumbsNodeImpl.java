/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.OpenAction
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 */
package org.netbeans.modules.editor.breadcrumbs;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.swing.Action;
import org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsElement;
import org.openide.actions.OpenAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

public class BreadCrumbsNodeImpl
extends AbstractNode {
    private final BreadcrumbsElement element;

    public BreadCrumbsNodeImpl(BreadcrumbsElement element) {
        super(Children.create((ChildFactory)new ChildrenFactoryImpl(element), (boolean)false), (Lookup)new ProxyLookup(new Lookup[]{Lookups.singleton((Object)element), element.getLookup()}));
        this.element = element;
    }

    public String getHtmlDisplayName() {
        return this.element.getHtmlDisplayName();
    }

    public Action getPreferredAction() {
        return OpenAction.get(OpenAction.class);
    }

    public boolean canCopy() {
        return false;
    }

    public boolean canCut() {
        return false;
    }

    public boolean canDestroy() {
        return false;
    }

    public boolean canRename() {
        return false;
    }

    public PasteType getDropType(Transferable t, int action, int index) {
        return null;
    }

    public Transferable drag() throws IOException {
        return null;
    }

    protected void createPasteTypes(Transferable t, List<PasteType> s) {
    }

    public Image getIcon(int type) {
        return this.element.getIcon(type);
    }

    public Image getOpenedIcon(int type) {
        return this.element.getOpenedIcon(type);
    }

    private static final class ChildrenFactoryImpl
    extends ChildFactory<BreadcrumbsElement> {
        private final BreadcrumbsElement element;

        public ChildrenFactoryImpl(BreadcrumbsElement element) {
            this.element = element;
        }

        protected boolean createKeys(List<BreadcrumbsElement> toPopulate) {
            toPopulate.addAll(this.element.getChildren());
            return true;
        }

        protected Node createNodeForKey(BreadcrumbsElement key) {
            return new BreadCrumbsNodeImpl(key);
        }
    }

}

