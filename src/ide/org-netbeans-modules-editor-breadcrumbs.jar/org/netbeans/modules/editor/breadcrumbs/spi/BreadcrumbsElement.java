/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.annotations.common.NonNull
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.breadcrumbs.spi;

import java.awt.Image;
import java.util.List;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.annotations.common.NonNull;
import org.openide.util.Lookup;

public interface BreadcrumbsElement {
    @NonNull
    public String getHtmlDisplayName();

    @NonNull
    public Image getIcon(int var1);

    @NonNull
    public Image getOpenedIcon(int var1);

    @NonNull
    public List<BreadcrumbsElement> getChildren();

    @NonNull
    public Lookup getLookup();

    @CheckForNull
    public BreadcrumbsElement getParent();
}

