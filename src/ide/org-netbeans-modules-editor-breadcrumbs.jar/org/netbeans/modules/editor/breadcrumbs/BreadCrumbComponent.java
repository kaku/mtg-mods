/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.actions.Openable
 *  org.netbeans.editor.JumpList
 *  org.openide.awt.HtmlRenderer
 *  org.openide.awt.HtmlRenderer$Renderer
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.ListView
 *  org.openide.explorer.view.NodeRenderer
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.breadcrumbs;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import org.netbeans.api.actions.Openable;
import org.netbeans.editor.JumpList;
import org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsController;
import org.openide.awt.HtmlRenderer;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.ListView;
import org.openide.explorer.view.NodeRenderer;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

public class BreadCrumbComponent<T extends JLabel>
extends JComponent
implements PropertyChangeListener {
    private final Image SEPARATOR = ImageUtilities.loadImage((String)"org/netbeans/modules/editor/breadcrumbs/resources/separator.png");
    private static final int USABLE_HEIGHT = 19;
    private static final int LEFT_SEPARATOR_INSET = 2;
    private static final int RIGHT_SEPARATOR_INSET = 10;
    private static final int ICON_TEXT_SEPARATOR = 5;
    private static final int START_INSET = 8;
    private static final int MAX_ROWS_IN_POP_UP = 20;
    public static final int COMPONENT_HEIGHT = 19;
    private ExplorerManager seenManager;
    private final T renderer = HtmlRenderer.createLabel();
    private Node[] nodes;
    private double[] sizes;
    private double height;

    public BreadCrumbComponent() {
        this.setPreferredSize(new Dimension(0, 19));
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent e) {
                BreadCrumbComponent.this.expand(e);
            }
        });
    }

    private ExplorerManager findManager() {
        ExplorerManager manager = ExplorerManager.find((Component)this);
        if (this.seenManager != manager) {
            if (this.seenManager != null) {
                this.seenManager.removePropertyChangeListener((PropertyChangeListener)this);
            }
            if (manager != null) {
                manager.addPropertyChangeListener((PropertyChangeListener)this);
            }
            this.seenManager = manager;
        }
        assert (manager != null);
        return manager;
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (this.nodes == null) {
            this.measurePrepaint();
        }
        assert (this.nodes != null);
        if ("Aqua".equals(UIManager.getLookAndFeel().getID())) {
            this.setBackground(UIManager.getColor("NbExplorerView.background"));
        }
        int height = this.getHeight();
        if (this.nodes.length == 0) {
            g.drawImage(this.SEPARATOR, 8, (height - this.SEPARATOR.getHeight(null)) / 2, null);
            return;
        }
        int x = 8;
        for (int i = 0; i < this.nodes.length; ++i) {
            this.configureForNode(this.nodes[i]);
            Dimension preferred = this.renderer.getPreferredSize();
            int labelY = (height - preferred.height) / 2;
            g.translate(x, labelY);
            this.renderer.setSize(preferred);
            this.renderer.paint(g);
            g.translate(- x, - labelY);
            x = (int)((double)x + this.sizes[i]);
            g.drawImage(this.SEPARATOR, x + 2, (height - this.SEPARATOR.getHeight(null)) / 2, null);
            x += 2 + this.SEPARATOR.getWidth(null) + 10;
        }
    }

    private void measurePrepaint() {
        List<Node> path = this.computeNodePath();
        int i = 0;
        this.nodes = path.toArray((T[])new Node[path.size()]);
        this.sizes = new double[path.size()];
        int xTotal = 0;
        this.height = 0.0;
        for (Node n : this.nodes) {
            this.configureForNode(n);
            Dimension preferedSize = this.renderer.getPreferredSize();
            this.sizes[i] = preferedSize.width;
            xTotal = (int)((double)xTotal + this.sizes[i]);
            this.height = Math.max(this.height, (double)preferedSize.height);
            ++i;
        }
        this.setPreferredSize(new Dimension(xTotal + (this.nodes.length - 1) * (2 + this.SEPARATOR.getWidth(null) + 10) + 8, 19));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                BreadCrumbComponent.this.measurePrepaint();
                BreadCrumbComponent.this.repaint();
            }
        });
    }

    private void expand(MouseEvent e) {
        int clickX = e.getPoint().x;
        int elemX = 8;
        for (int i = 0; i < this.sizes.length; ++i) {
            int startX = elemX;
            elemX = (int)((double)elemX + this.sizes[i]);
            if (clickX <= (elemX += 2)) {
                List<Node> path = this.computeNodePath();
                Node selected = path.get(i);
                if (e.getButton() == 1) {
                    this.open(selected);
                } else {
                    this.expand(startX, selected);
                }
                return;
            }
            startX = elemX;
            if (clickX <= (elemX += this.SEPARATOR.getWidth(null))) {
                List<Node> path = this.computeNodePath();
                this.expand(startX, path.get(i));
                return;
            }
            elemX += 10;
        }
    }

    private List<Node> computeNodePath() {
        ExplorerManager manager = this.findManager();
        ArrayList<Node> path = new ArrayList<Node>();
        Node stopAt = manager.getRootContext().getParentNode();
        for (Node sel = manager.getExploredContext(); sel != null && sel != stopAt; sel = sel.getParentNode()) {
            path.add(sel);
        }
        path.remove(path.size() - 1);
        Collections.reverse(path);
        return path;
    }

    private void open(Node node) {
        Openable openable = (Openable)node.getLookup().lookup(Openable.class);
        if (openable != null) {
            JumpList.checkAddEntry();
            openable.open();
        }
    }

    private void expand(int startX, final Node what) {
        class Expanded
        extends JPanel
        implements ExplorerManager.Provider {
            final /* synthetic */ ExplorerManager val$expandManager;

            public Expanded() {
                this.val$expandManager = layoutManager;
                super(layout);
            }

            public ExplorerManager getExplorerManager() {
                return this.val$expandManager;
            }
        }
        if (what.getChildren().getNodesCount() == 0) {
            return;
        }
        final ExplorerManager expandManager = new ExplorerManager();
        final Expanded expanded = new Expanded(this, (LayoutManager)new BorderLayout(), expandManager);
        expanded.setBorder(new LineBorder(Color.BLACK, 1));
        ListView listView = new ListView(){};
        listView.setPopupAllowed(false);
        expanded.add((Component)listView, "Center");
        expandManager.setRootContext(what);
        Point place = new Point(startX, 0);
        SwingUtilities.convertPointToScreen(place, this);
        expanded.validate();
        final Popup popup = PopupFactory.getSharedInstance().getPopup(this, expanded, place.x, place.y - expanded.getPreferredSize().height);
        final AWTEventListener multicastListener = new AWTEventListener(){

            @Override
            public void eventDispatched(AWTEvent event) {
                if (event instanceof MouseEvent && ((MouseEvent)event).getClickCount() > 0) {
                    Object source = event.getSource();
                    while (source instanceof Component) {
                        if (source == expanded) {
                            return;
                        }
                        source = ((Component)source).getParent();
                    }
                    popup.hide();
                    Toolkit.getDefaultToolkit().removeAWTEventListener(this);
                }
            }
        };
        Toolkit.getDefaultToolkit().addAWTEventListener(multicastListener, 16);
        expandManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Node[] selected;
                if ("selectedNodes".equals(evt.getPropertyName()) && (selected = expandManager.getSelectedNodes()).length == 1) {
                    BreadCrumbComponent.this.open(selected[0]);
                    popup.hide();
                    Toolkit.getDefaultToolkit().removeAWTEventListener(multicastListener);
                }
            }
        });
        popup.show();
    }

    private void configureForNode(Node node) {
        int width;
        ((HtmlRenderer.Renderer)this.renderer).reset();
        Image nodeIcon = node.getIcon(1);
        Icon icon = nodeIcon != null && nodeIcon != BreadcrumbsController.NO_ICON ? ImageUtilities.image2Icon((Image)nodeIcon) : null;
        int n = width = icon != null ? icon.getIconWidth() : 0;
        if (width > 0) {
            this.renderer.setIcon(icon);
            this.renderer.setIconTextGap(5);
        } else {
            this.renderer.setIcon(null);
            this.renderer.setIconTextGap(0);
        }
        String html = node.getHtmlDisplayName();
        if (html != null) {
            ((HtmlRenderer.Renderer)this.renderer).setHtml(true);
            this.renderer.setText(html);
        } else {
            ((HtmlRenderer.Renderer)this.renderer).setHtml(false);
            this.renderer.setText(node.getDisplayName());
        }
        this.renderer.setFont(this.getFont());
    }

}

