/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.SideBarFactory
 *  org.openide.awt.CloseButtonFactory
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.breadcrumbs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.modules.editor.breadcrumbs.BreadCrumbComponent;
import org.netbeans.modules.editor.breadcrumbs.HolderImpl;
import org.openide.awt.CloseButtonFactory;
import org.openide.explorer.ExplorerManager;
import org.openide.util.WeakListeners;

public class SideBarFactoryImpl
implements SideBarFactory {
    public static final String KEY_BREADCRUMBS = "enable.breadcrumbs";
    public static final boolean DEF_BREADCRUMBS = true;

    public JComponent createSideBar(JTextComponent target) {
        Document doc = target.getDocument();
        return new SideBar(doc);
    }

    private static final class SeparatorBorder
    implements Border {
        private static final int BORDER_WIDTH = 1;
        private final Insets INSETS = new Insets(1, 0, 0, 0);

        private SeparatorBorder() {
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Color originalColor = g.getColor();
            g.setColor(UIManager.getColor("controlShadow"));
            g.drawLine(0, 0, c.getWidth(), 0);
            g.setColor(originalColor);
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return this.INSETS;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }
    }

    private static final class SideBar
    extends JPanel
    implements ExplorerManager.Provider,
    PreferenceChangeListener {
        private final Document forDocument;
        private final Preferences prefs;
        private boolean enabled;

        public SideBar(Document forDocument) {
            super(new BorderLayout());
            this.forDocument = forDocument;
            this.add(new BreadCrumbComponent(), "Center");
            JButton closeButton = CloseButtonFactory.createBigCloseButton();
            this.add((Component)closeButton, "East");
            this.prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
            this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.prefs));
            closeButton.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    SideBar.this.prefs.putBoolean("enable.breadcrumbs", false);
                }
            });
            this.setBorder(new SeparatorBorder());
            this.preferenceChange(null);
        }

        public ExplorerManager getExplorerManager() {
            return HolderImpl.get(this.forDocument).getManager();
        }

        @Override
        public void preferenceChange(PreferenceChangeEvent evt) {
            if (evt == null || "enable.breadcrumbs".equals(evt.getKey())) {
                this.enabled = this.prefs.getBoolean("enable.breadcrumbs", true);
                this.updatePreferredSize();
            }
        }

        private void updatePreferredSize() {
            if (this.enabled) {
                this.setPreferredSize(new Dimension(Integer.MAX_VALUE, 20));
                this.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
            } else {
                this.setPreferredSize(new Dimension(0, 0));
                this.setMaximumSize(new Dimension(0, 0));
            }
            this.revalidate();
        }

    }

}

