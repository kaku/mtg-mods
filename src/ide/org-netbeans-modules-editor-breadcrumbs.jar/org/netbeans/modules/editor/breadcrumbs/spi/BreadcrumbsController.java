/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.NonNull
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.SideBarFactory
 *  org.netbeans.modules.parsing.spi.Scheduler
 *  org.openide.explorer.ExplorerManager
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Mutex
 *  org.openide.util.Mutex$Action
 *  org.openide.util.Parameters
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.breadcrumbs.spi;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import org.netbeans.api.annotations.common.NonNull;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.modules.editor.breadcrumbs.BreadCrumbsNodeImpl;
import org.netbeans.modules.editor.breadcrumbs.HolderImpl;
import org.netbeans.modules.editor.breadcrumbs.SideBarFactoryImpl;
import org.netbeans.modules.editor.breadcrumbs.spi.BreadcrumbsElement;
import org.netbeans.modules.editor.breadcrumbs.support.BreadCrumbsScheduler;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Mutex;
import org.openide.util.Parameters;
import org.openide.util.RequestProcessor;

public class BreadcrumbsController {
    private static final RequestProcessor WORKER = new RequestProcessor(BreadcrumbsController.class.getName(), 1, false, false);
    public static final Class<? extends Scheduler> BREADCRUMBS_SCHEDULER = BreadCrumbsScheduler.class;
    public static final Image NO_ICON = ImageUtilities.loadImage((String)"org/netbeans/modules/editor/breadcrumbs/resources/no-icon.png");

    private BreadcrumbsController() {
    }

    public static void setBreadcrumbs(final @NonNull Document doc, final @NonNull BreadcrumbsElement selected) {
        WORKER.post(new Runnable(){

            @Override
            public void run() {
                BreadCrumbsNodeImpl root;
                ArrayList<BreadcrumbsElement> path = new ArrayList<BreadcrumbsElement>();
                for (BreadcrumbsElement el = selected; el != null; el = el.getParent()) {
                    path.add(el);
                }
                BreadCrumbsNodeImpl last = root = new BreadCrumbsNodeImpl((BreadcrumbsElement)path.remove(path.size() - 1));
                Collections.reverse(path);
                block1 : for (BreadcrumbsElement current : path) {
                    for (Node n : last.getChildren().getNodes(true)) {
                        if (n.getLookup().lookup(BreadcrumbsElement.class) != current) continue;
                        last = n;
                        continue block1;
                    }
                }
                BreadcrumbsController.setBreadcrumbs(doc, (Node)root, (Node)last);
            }
        });
    }

    @Deprecated
    public static void setBreadcrumbs(@NonNull Document doc, final @NonNull Node root, final @NonNull Node selected) {
        Parameters.notNull((CharSequence)"doc", (Object)doc);
        Parameters.notNull((CharSequence)"root", (Object)root);
        Parameters.notNull((CharSequence)"selected", (Object)selected);
        final ExplorerManager manager = HolderImpl.get(doc).getManager();
        Children.MUTEX.writeAccess((Mutex.Action)new Mutex.Action<Void>(){

            public Void run() {
                manager.setRootContext(root);
                manager.setExploredContext(selected);
                return null;
            }
        });
    }

    public static boolean areBreadCrumsEnabled(@NonNull Document doc) {
        return ((Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class)).getBoolean("enable.breadcrumbs", true);
    }

    public static void addBreadCrumbsEnabledListener(final @NonNull ChangeListener l) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        prefs.addPreferenceChangeListener(new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                if (evt == null || "enable.breadcrumbs".equals(evt.getKey())) {
                    l.stateChanged(new ChangeEvent(evt));
                }
            }
        });
    }

    public static SideBarFactory createSideBarFactory() {
        return new SideBarFactoryImpl();
    }

}

