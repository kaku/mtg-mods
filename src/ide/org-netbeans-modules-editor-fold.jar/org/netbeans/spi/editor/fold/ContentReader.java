/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor.fold;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

public interface ContentReader {
    public CharSequence read(Document var1, Fold var2, FoldTemplate var3) throws BadLocationException;

    @MimeLocation(subfolderName="FoldManager")
    public static interface Factory {
        public ContentReader createReader(FoldType var1);
    }

}

