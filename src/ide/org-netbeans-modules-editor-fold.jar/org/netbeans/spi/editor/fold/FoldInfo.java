/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.editor.fold;

import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.openide.util.Parameters;

public final class FoldInfo {
    private int start;
    private int end;
    private FoldTemplate template;
    private FoldType type;
    private Boolean collapsed;
    private Object extraInfo;
    private String description;

    public static FoldInfo range(int start, int end, FoldType type) {
        return new FoldInfo(start, end, type);
    }

    private FoldInfo(int start, int end, FoldType ft) {
        Parameters.notNull((CharSequence)"ft", (Object)ft);
        if (start < 0) {
            throw new IllegalArgumentException("Invalid start offet: " + start);
        }
        if (end < start) {
            throw new IllegalArgumentException("Invalid end offset: " + end + ", start is: " + start);
        }
        this.type = ft;
        this.start = start;
        this.end = end;
        this.template = ft.getTemplate();
    }

    public FoldInfo withTemplate(FoldTemplate t) {
        Parameters.notNull((CharSequence)"t", (Object)t);
        this.template = t;
        return this;
    }

    public FoldInfo withDescription(String desc) {
        this.description = desc;
        return this;
    }

    public FoldInfo attach(Object extraInfo) {
        this.extraInfo = extraInfo;
        return this;
    }

    public String getDescriptionOverride() {
        return this.description;
    }

    public Object getExtraInfo() {
        return this.extraInfo;
    }

    public FoldInfo collapsed(boolean state) {
        this.collapsed = state;
        return this;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public FoldTemplate getTemplate() {
        return this.template;
    }

    public FoldType getType() {
        return this.type;
    }

    public Boolean getCollapsed() {
        return this.collapsed;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FoldInfo[").append(this.start).append(" - ").append(this.end).append(", ").append(this.type).append(", desc = ").append(this.description == null ? this.template.getDescription() : this.description).append(" collapsed = ").append(this.collapsed).append("]");
        return sb.toString();
    }
}

