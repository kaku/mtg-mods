/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.fold;

import org.netbeans.api.editor.fold.FoldHierarchy;

public interface FoldHierarchyMonitor {
    public void foldsAttached(FoldHierarchy var1);
}

