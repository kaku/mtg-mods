/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.fold;

import javax.swing.event.DocumentEvent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldOperation;

public interface FoldManager {
    public void init(FoldOperation var1);

    public void initFolds(FoldHierarchyTransaction var1);

    public void insertUpdate(DocumentEvent var1, FoldHierarchyTransaction var2);

    public void removeUpdate(DocumentEvent var1, FoldHierarchyTransaction var2);

    public void changedUpdate(DocumentEvent var1, FoldHierarchyTransaction var2);

    public void removeEmptyNotify(Fold var1);

    public void removeDamagedNotify(Fold var1);

    public void expandNotify(Fold var1);

    public void release();
}

