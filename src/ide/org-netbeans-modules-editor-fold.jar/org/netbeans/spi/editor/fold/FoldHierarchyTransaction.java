/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.spi.editor.fold;

import org.netbeans.modules.editor.fold.FoldHierarchyTransactionImpl;

public final class FoldHierarchyTransaction {
    private final FoldHierarchyTransactionImpl impl;

    FoldHierarchyTransaction(FoldHierarchyTransactionImpl impl) {
        this.impl = impl;
    }

    public void commit() {
        this.impl.commit();
    }

    FoldHierarchyTransactionImpl getImpl() {
        return this.impl;
    }
}

