/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor.fold;

import java.util.Collection;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="FoldManager")
public interface FoldTypeProvider {
    public Collection getValues(Class var1);

    public boolean inheritable();
}

