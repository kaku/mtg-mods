/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.spi.editor.fold;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import javax.swing.text.BadLocationException;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;
import org.netbeans.modules.editor.fold.FoldHierarchyTransactionImpl;
import org.netbeans.modules.editor.fold.FoldOperationImpl;
import org.netbeans.modules.editor.fold.SpiPackageAccessor;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldInfo;
import org.openide.util.Parameters;

public final class FoldOperation {
    private static boolean spiPackageAccessorRegistered;
    private final FoldOperationImpl impl;

    private static void ensureSpiAccessorRegistered() {
        if (!spiPackageAccessorRegistered) {
            spiPackageAccessorRegistered = true;
            SpiPackageAccessor.register(new SpiPackageAccessorImpl());
        }
    }

    private FoldOperation(FoldOperationImpl impl) {
        this.impl = impl;
    }

    public Fold addToHierarchy(FoldType type, String description, boolean collapsed, int startOffset, int endOffset, int startGuardedLength, int endGuardedLength, Object extraInfo, FoldHierarchyTransaction transaction) throws BadLocationException {
        Fold fold = this.impl.createFold(type, description, collapsed, startOffset, endOffset, startGuardedLength, endGuardedLength, extraInfo);
        this.impl.addToHierarchy(fold, transaction.getImpl());
        return fold;
    }

    public Fold addToHierarchy(FoldType type, int startOffset, int endOffset, Boolean collapsed, FoldTemplate template, String displayOverride, Object extraInfo, FoldHierarchyTransaction transaction) throws BadLocationException {
        Parameters.notNull((CharSequence)"type", (Object)type);
        Parameters.notNull((CharSequence)"transaction", (Object)transaction);
        boolean c = collapsed == null ? this.impl.getInitialState(type) : collapsed.booleanValue();
        if (template == null) {
            template = type.getTemplate();
        }
        if (displayOverride == null) {
            displayOverride = template.getDescription();
        }
        Fold fold = this.impl.createFold(type, displayOverride, c, startOffset, endOffset, template.getGuardedStart(), template.getGuardedEnd(), extraInfo);
        this.impl.addToHierarchy(fold, transaction.getImpl());
        return fold;
    }

    public static boolean isBoundsValid(int startOffset, int endOffset, int startGuardedLength, int endGuardedLength) {
        return startOffset < endOffset && startGuardedLength >= 0 && endGuardedLength >= 0 && startOffset + startGuardedLength <= endOffset - endGuardedLength;
    }

    public void removeFromHierarchy(Fold fold, FoldHierarchyTransaction transaction) {
        this.impl.removeFromHierarchy(fold, transaction.getImpl());
    }

    public boolean owns(Fold fold) {
        return ApiPackageAccessor.get().foldGetOperation(fold) == this.impl;
    }

    public Object getExtraInfo(Fold fold) {
        return this.impl.getExtraInfo(fold);
    }

    public boolean isStartDamaged(Fold fold) {
        return this.impl.isStartDamaged(fold);
    }

    public boolean isEndDamaged(Fold fold) {
        return this.impl.isEndDamaged(fold);
    }

    public FoldHierarchyTransaction openTransaction() {
        return this.impl.openTransaction().getTransaction();
    }

    public boolean isAddedOrBlocked(Fold fold) {
        return this.impl.isAddedOrBlocked(fold);
    }

    public boolean isBlocked(Fold fold) {
        return this.impl.isBlocked(fold);
    }

    public FoldHierarchy getHierarchy() {
        return this.impl.getHierarchy();
    }

    public boolean isReleased() {
        return this.impl.isReleased();
    }

    public Iterator<Fold> foldIterator() {
        return this.impl.foldIterator();
    }

    public Map<FoldInfo, Fold> update(Collection<FoldInfo> infos, Collection<Fold> removed, Collection<FoldInfo> created) throws BadLocationException {
        Parameters.notNull((CharSequence)"infos", infos);
        return this.impl.update(infos, removed, created);
    }

    static {
        FoldOperation.ensureSpiAccessorRegistered();
    }

    private static final class SpiPackageAccessorImpl
    extends SpiPackageAccessor {
        private SpiPackageAccessorImpl() {
        }

        @Override
        public FoldHierarchyTransaction createFoldHierarchyTransaction(FoldHierarchyTransactionImpl impl) {
            return new FoldHierarchyTransaction(impl);
        }

        @Override
        public FoldHierarchyTransactionImpl getImpl(FoldHierarchyTransaction transaction) {
            return transaction.getImpl();
        }

        @Override
        public FoldOperation createFoldOperation(FoldOperationImpl impl) {
            return new FoldOperation(impl);
        }
    }

}

