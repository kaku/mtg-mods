/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.spi.editor.fold;

import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="FoldManager")
public interface FoldManagerFactory {
    public FoldManager createFoldManager();
}

