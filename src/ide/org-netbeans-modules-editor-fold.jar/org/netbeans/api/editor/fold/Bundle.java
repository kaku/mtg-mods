/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.api.editor.fold;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String FT_DefaultBlockTemplate() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_DefaultBlockTemplate");
    }

    static String FT_DefaultTemplate() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_DefaultTemplate");
    }

    static String FT_Label_code_block() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_code-block");
    }

    static String FT_Label_comment() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_comment");
    }

    static String FT_Label_import() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_import");
    }

    static String FT_Label_initial_comment() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_initial-comment");
    }

    static String FT_Label_inner_class() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_inner-class");
    }

    static String FT_Label_javadoc() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_javadoc");
    }

    static String FT_Label_member() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_member");
    }

    static String FT_Label_tag() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_tag");
    }

    static String FT_Label_user_defined() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_Label_user-defined");
    }

    static String FT_display_code_block() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_display_code-block");
    }

    static String FT_display_default() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_display_default");
    }

    static String FT_display_tag() {
        return NbBundle.getMessage(Bundle.class, (String)"FT_display_tag");
    }

    private void Bundle() {
    }
}

