/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import org.netbeans.api.editor.fold.Bundle;

public final class FoldTemplate {
    public static final FoldTemplate DEFAULT = new FoldTemplate(0, 0, Bundle.FT_DefaultTemplate());
    public static final FoldTemplate DEFAULT_BLOCK = new FoldTemplate(0, 0, Bundle.FT_DefaultBlockTemplate());
    public static final String CONTENT_PLACEHOLDER = Bundle.FT_DefaultTemplate();
    private int guardedStart;
    private int guardedEnd;
    private String displayText;

    public FoldTemplate(int guardedStart, int guardedEnd, String displayText) {
        this.guardedStart = guardedStart;
        this.guardedEnd = guardedEnd;
        this.displayText = displayText;
    }

    public String getDescription() {
        return this.displayText;
    }

    public int getGuardedEnd() {
        return this.guardedEnd;
    }

    public int getGuardedStart() {
        return this.guardedStart;
    }
}

