/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import java.util.Map;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.modules.editor.fold.CustomFoldManager;
import org.netbeans.modules.editor.fold.JavadocReader;
import org.netbeans.spi.editor.fold.ContentReader;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;

public final class FoldingSupport {
    private FoldingSupport() {
    }

    public static FoldManagerFactory userFoldManagerFactory(Map params) {
        final String s = (String)params.get("tokenId");
        return new FoldManagerFactory(){

            @Override
            public FoldManager createFoldManager() {
                return FoldingSupport.userFoldManager(s);
            }
        };
    }

    public static FoldManager userFoldManager(String tokenId) {
        if (tokenId != null) {
            return new CustomFoldManager(tokenId);
        }
        return new CustomFoldManager();
    }

    public static ContentReader contentReader(String start, String terminator, String stop, String prefix) {
        return new JavadocReader(start, terminator, stop, prefix);
    }

    public static ContentReader.Factory contentReaderFactory(Map m) {
        final String start = (String)m.get("start");
        final String terminator = (String)m.get("terminator");
        final String stop = (String)m.get("stop");
        final String foldType = (String)m.get("type");
        final String prefix = (String)m.get("prefix");
        return new ContentReader.Factory(){

            @Override
            public ContentReader createReader(FoldType ft) {
                if (foldType != null && foldType.equals(ft.code()) || foldType == null && ft.isKindOf(FoldType.DOCUMENTATION)) {
                    return FoldingSupport.contentReader(start, terminator, stop, prefix);
                }
                return null;
            }
        };
    }

}

