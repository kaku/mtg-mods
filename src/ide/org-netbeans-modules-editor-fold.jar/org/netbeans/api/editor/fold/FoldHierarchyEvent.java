/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import java.util.EventObject;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.modules.editor.fold.FoldUtilitiesImpl;

public final class FoldHierarchyEvent
extends EventObject {
    private Fold[] removedFolds;
    private Fold[] addedFolds;
    private FoldStateChange[] foldStateChanges;
    private int affectedStartOffset;
    private int affectedEndOffset;

    FoldHierarchyEvent(FoldHierarchy source, Fold[] removedFolds, Fold[] addedFolds, FoldStateChange[] foldStateChanges, int affectedStartOffset, int affectedEndOffset) {
        super(source);
        this.removedFolds = removedFolds;
        this.addedFolds = addedFolds;
        this.foldStateChanges = foldStateChanges;
        this.affectedStartOffset = affectedStartOffset;
        this.affectedEndOffset = affectedEndOffset;
    }

    public int getRemovedFoldCount() {
        return this.removedFolds.length;
    }

    public Fold getRemovedFold(int removedFoldIndex) {
        return this.removedFolds[removedFoldIndex];
    }

    public int getAddedFoldCount() {
        return this.addedFolds.length;
    }

    public Fold getAddedFold(int addedFoldIndex) {
        return this.addedFolds[addedFoldIndex];
    }

    public int getFoldStateChangeCount() {
        return this.foldStateChanges.length;
    }

    public FoldStateChange getFoldStateChange(int index) {
        return this.foldStateChanges[index];
    }

    public int getAffectedStartOffset() {
        return this.affectedStartOffset;
    }

    public int getAffectedEndOffset() {
        return this.affectedEndOffset;
    }

    @Override
    public String toString() {
        return FoldUtilitiesImpl.foldHierarchyEventToString(this);
    }
}

