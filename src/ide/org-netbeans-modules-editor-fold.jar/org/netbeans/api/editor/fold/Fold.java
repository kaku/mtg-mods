/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.modules.editor.fold.FoldChildren;
import org.netbeans.modules.editor.fold.FoldOperationImpl;
import org.netbeans.modules.editor.fold.FoldUtilitiesImpl;
import org.netbeans.spi.editor.fold.FoldManager;

public final class Fold {
    private static final Fold[] EMPTY_FOLD_ARRAY = new Fold[0];
    private static final String DEFAULT_DESCRIPTION = "...";
    private final FoldOperationImpl operation;
    private final FoldType type;
    private static final byte FLAG_COLLAPSED = 1;
    private static final byte FLAG_START_DAMAGED = 2;
    private static final byte FLAG_END_DAMAGED = 4;
    private volatile byte flags;
    private String description;
    private Fold parent;
    private FoldChildren children;
    private int rawIndex;
    private int startGuardedLength;
    private int endGuardedLength;
    private Position startPos;
    private Position endPos;
    private Object extraInfo;
    private static final Logger LOG = Logger.getLogger(Fold.class.getName());

    Fold(FoldOperationImpl operation, FoldType type, String description, boolean collapsed, Document doc, int startOffset, int endOffset, int startGuardedLength, int endGuardedLength, Object extraInfo) throws BadLocationException {
        if (startGuardedLength < 0) {
            throw new IllegalArgumentException("startGuardedLength=" + startGuardedLength + " < 0");
        }
        if (endGuardedLength < 0) {
            throw new IllegalArgumentException("endGuardedLength=" + endGuardedLength + " < 0");
        }
        if (startOffset >= endOffset) {
            throw new IllegalArgumentException("startOffset=" + startOffset + " >= endOffset=" + endOffset);
        }
        if (endOffset - startOffset < startGuardedLength + endGuardedLength) {
            throw new IllegalArgumentException("(endOffset=" + endOffset + " - startOffset=" + startOffset + ") < " + "(startGuardedLength=" + startGuardedLength + " + endGuardedLength=" + endGuardedLength + ")");
        }
        this.operation = operation;
        this.type = type;
        this.flags = collapsed ? 1 : 0;
        this.description = description;
        this.startPos = doc.createPosition(startOffset);
        this.endPos = doc.createPosition(endOffset);
        this.startGuardedLength = startGuardedLength;
        this.endGuardedLength = endGuardedLength;
        this.extraInfo = extraInfo;
    }

    public FoldType getType() {
        return this.type;
    }

    public Fold getParent() {
        return this.parent;
    }

    void setParent(Fold parent) {
        if (this.isRootFold()) {
            throw new IllegalArgumentException("Cannot set parent on root");
        }
        this.parent = parent;
    }

    public FoldHierarchy getHierarchy() {
        return this.operation.getHierarchy();
    }

    FoldOperationImpl getOperation() {
        return this.operation;
    }

    boolean isRootFold() {
        return this.operation.getManager() == null;
    }

    public int getStartOffset() {
        return this.isRootFold() ? 0 : this.startPos.getOffset();
    }

    void setStartOffset(Document doc, int startOffset) throws BadLocationException {
        if (this.isRootFold()) {
            throw new IllegalStateException("Cannot set endOffset of root fold");
        }
        this.startPos = doc.createPosition(startOffset);
    }

    public int getEndOffset() {
        return this.isRootFold() ? this.operation.getHierarchy().getComponent().getDocument().getLength() + 1 : this.endPos.getOffset();
    }

    void setEndOffset(Document doc, int endOffset) throws BadLocationException {
        if (this.isRootFold()) {
            throw new IllegalStateException("Cannot set endOffset of root fold");
        }
        this.endPos = doc.createPosition(endOffset);
    }

    public boolean isCollapsed() {
        return (this.flags & 1) > 0;
    }

    void setCollapsed(boolean collapsed) {
        if (this.isRootFold()) {
            throw new IllegalStateException("Cannot set collapsed flag on root fold.");
        }
        this.flags = (byte)(this.flags & -2 | (collapsed ? 1 : 0));
    }

    public String getDescription() {
        return this.description != null ? this.description : "...";
    }

    void setDescription(String description) {
        this.description = description;
    }

    public int getFoldCount() {
        return this.children != null ? this.children.getFoldCount() : 0;
    }

    public Fold getFold(int index) {
        if (this.children != null) {
            return this.children.getFold(index);
        }
        throw new IndexOutOfBoundsException("index=" + index + " but no children exist.");
    }

    Fold[] foldsToArray(int index, int count) {
        if (this.children != null) {
            return this.children.foldsToArray(index, count);
        }
        if (count == 0) {
            return EMPTY_FOLD_ARRAY;
        }
        throw new IndexOutOfBoundsException("No children but count=" + count);
    }

    void extractToChildren(int index, int length, Fold fold) {
        if (fold.getFoldCount() != 0 || fold.getParent() != null) {
            throw new IllegalStateException();
        }
        if (length != 0) {
            fold.setChildren(this.children.extractToChildren(index, length, fold));
        } else {
            if (this.children == null) {
                this.children = new FoldChildren(this);
            }
            this.children.insert(index, fold);
        }
    }

    Fold replaceByChildren(int index) {
        Fold fold = this.getFold(index);
        FoldChildren foldChildren = fold.getChildren();
        boolean check = false;
        if (!$assertionsDisabled) {
            check = true;
            if (!true) {
                throw new AssertionError();
            }
        }
        if (check && foldChildren != null) {
            Fold[] folds = foldChildren.foldsToArray(0, foldChildren.getFoldCount());
            Fold toRemove = fold;
            if (folds.length > 0) {
                int ps = this.getStartOffset();
                int pe = this.getEndOffset();
                for (Fold f : folds) {
                    int fs = f.getStartOffset();
                    int fe = f.getEndOffset();
                    if (fs >= ps && fe <= pe) continue;
                    LOG.log(Level.WARNING, "Illegal attempt to replace-by-children fold. Parent fold: {0}, fold to be replaced: {1}, at index {2}", new Object[]{this, toRemove, index});
                    LOG.log(Level.WARNING, "Dumping hierarchy: " + this.getHierarchy(), new Throwable());
                    break;
                }
            }
        }
        fold.setChildren(null);
        this.children.replaceByChildren(index, foldChildren);
        return fold;
    }

    private FoldChildren getChildren() {
        return this.children;
    }

    void setChildren(FoldChildren children) {
        this.children = children;
    }

    Object getExtraInfo() {
        return this.extraInfo;
    }

    void setExtraInfo(Object info) {
        this.extraInfo = info;
    }

    public int getFoldIndex(Fold child) {
        return this.children != null ? this.children.getFoldIndex(child) : -1;
    }

    public int getGuardedStart() {
        if (this.isRootFold()) {
            return 0;
        }
        if (this.isZeroStartGuardedLength()) {
            return this.getStartOffset();
        }
        return this.getStartOffset() + this.startGuardedLength;
    }

    public int getGuardedEnd() {
        if (this.isRootFold()) {
            return this.getEndOffset();
        }
        if (this.isZeroEndGuardedLength()) {
            return this.getEndOffset();
        }
        return this.getEndOffset() - this.endGuardedLength;
    }

    int getStartGuardedLength() {
        return this.startGuardedLength;
    }

    int getEndGuardedLength() {
        return this.endGuardedLength;
    }

    private boolean isZeroStartGuardedLength() {
        return this.startGuardedLength == 0;
    }

    private boolean isZeroEndGuardedLength() {
        return this.endGuardedLength == 0;
    }

    boolean isStartDamaged() {
        return !this.isZeroStartGuardedLength() && (this.flags & 2) > 0;
    }

    boolean isEndDamaged() {
        return !this.isZeroEndGuardedLength() && (this.flags & 4) > 0;
    }

    int getRawIndex() {
        return this.rawIndex;
    }

    void setRawIndex(int rawIndex) {
        this.rawIndex = rawIndex;
    }

    void updateRawIndex(int rawIndexDelta) {
        this.rawIndex += rawIndexDelta;
    }

    public String toString() {
        return FoldUtilitiesImpl.foldToString(this) + ", [" + this.getStartOffset() + ", " + this.getEndOffset() + "] {" + this.getGuardedStart() + ", " + this.getGuardedEnd() + '}';
    }

    void setDamaged(byte f) {
        this.flags = (byte)(this.flags & -7 | f);
    }
}

