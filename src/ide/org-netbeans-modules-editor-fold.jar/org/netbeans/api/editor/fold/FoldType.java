/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Parameters
 */
package org.netbeans.api.editor.fold;

import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.netbeans.api.editor.fold.Bundle;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.openide.util.Parameters;

public final class FoldType {
    public static final FoldType CODE_BLOCK = FoldType.create("code-block", Bundle.FT_Label_code_block(), new FoldTemplate(1, 1, Bundle.FT_display_code_block()));
    public static final FoldType DOCUMENTATION = FoldType.create("documentation", Bundle.FT_Label_javadoc(), FoldTemplate.DEFAULT);
    public static final FoldType COMMENT = FoldType.create("comment", Bundle.FT_Label_comment(), FoldTemplate.DEFAULT);
    public static final FoldType INITIAL_COMMENT = FoldType.create("initial-comment", Bundle.FT_Label_initial_comment(), FoldTemplate.DEFAULT);
    public static final FoldType TAG = FoldType.create("tags", Bundle.FT_Label_tag(), new FoldTemplate(1, 1, Bundle.FT_display_tag()));
    public static final FoldType NESTED = FoldType.create("nested", Bundle.FT_Label_inner_class(), FoldTemplate.DEFAULT);
    public static final FoldType MEMBER = FoldType.create("member", Bundle.FT_Label_member(), FoldTemplate.DEFAULT);
    public static final FoldType IMPORT = FoldType.create("import", Bundle.FT_Label_import(), FoldTemplate.DEFAULT);
    public static final FoldType USER = FoldType.create("user", Bundle.FT_Label_user_defined(), null);
    private final String code;
    private final FoldType parent;
    private final String label;
    private final FoldTemplate template;

    @Deprecated
    public FoldType(String description) {
        this(description, null, null, null);
    }

    private FoldType(String code, String label, FoldTemplate template, FoldType parent) {
        this.code = code;
        this.parent = parent;
        this.label = label;
        this.template = template != null ? template : FoldTemplate.DEFAULT;
    }

    public static FoldType create(String code, String label, FoldTemplate template) {
        Parameters.notWhitespace((CharSequence)"code", (CharSequence)code);
        Parameters.notWhitespace((CharSequence)"label", (CharSequence)label);
        return new FoldType(code, label, template, null);
    }

    public FoldType override(String label, FoldTemplate template) {
        return this.derive(this.code(), label, template);
    }

    public FoldType derive(String code, String label, FoldTemplate template) {
        Parameters.notWhitespace((CharSequence)"code", (CharSequence)code);
        if (template == null) {
            template = this.template;
        }
        return new FoldType(code, label, template, this);
    }

    @Deprecated
    public boolean accepts(FoldType type) {
        return this.isKindOf(type);
    }

    public String toString() {
        return this.code();
    }

    public String getLabel() {
        return this.label;
    }

    public FoldTemplate getTemplate() {
        return this.template;
    }

    public boolean isKindOf(FoldType other) {
        return other == this || this.parent != null && this.parent.isKindOf(other);
    }

    public FoldType parent() {
        return this.parent;
    }

    public String code() {
        return this.code;
    }

    public static interface Domain {
        public Collection<FoldType> values();

        public FoldType valueOf(String var1);

        public void addChangeListener(ChangeListener var1);

        public void removeChangeListener(ChangeListener var1);
    }

}

