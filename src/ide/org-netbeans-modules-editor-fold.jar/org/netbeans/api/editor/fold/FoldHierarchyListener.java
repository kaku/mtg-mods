/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import java.util.EventListener;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;

public interface FoldHierarchyListener
extends EventListener {
    public void foldHierarchyChanged(FoldHierarchyEvent var1);
}

