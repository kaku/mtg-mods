/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import java.util.Collection;
import java.util.Collections;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;
import org.netbeans.modules.editor.fold.FoldChildren;
import org.netbeans.modules.editor.fold.FoldHierarchyExecution;
import org.netbeans.modules.editor.fold.FoldOperationImpl;

public final class FoldHierarchy {
    public static final FoldType ROOT_FOLD_TYPE = new FoldType("root-fold");
    private static boolean apiPackageAccessorRegistered;
    private FoldHierarchyExecution execution;

    private static void ensureApiAccessorRegistered() {
        if (!apiPackageAccessorRegistered) {
            apiPackageAccessorRegistered = true;
            ApiPackageAccessor.register(new ApiPackageAccessorImpl());
        }
    }

    public static synchronized FoldHierarchy get(JTextComponent component) {
        return FoldHierarchyExecution.getOrCreateFoldHierarchy(component);
    }

    private FoldHierarchy(FoldHierarchyExecution execution) {
        this.execution = execution;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void render(Runnable r) {
        this.lock();
        try {
            r.run();
        }
        finally {
            this.unlock();
        }
    }

    public void lock() {
        this.execution.lock();
    }

    public void unlock() {
        this.execution.unlock();
    }

    public void collapse(Fold f) {
        this.collapse(Collections.singletonList(f));
    }

    public void collapse(Collection c) {
        this.execution.collapse(c);
    }

    public void expand(Fold f) {
        this.expand(Collections.singletonList(f));
    }

    public void expand(Collection c) {
        this.execution.expand(c);
    }

    public void toggle(Fold f) {
        if (f.isCollapsed()) {
            this.expand(f);
        } else {
            this.collapse(f);
        }
    }

    public JTextComponent getComponent() {
        return this.execution.getComponent();
    }

    public Fold getRootFold() {
        return this.execution.getRootFold();
    }

    public void addFoldHierarchyListener(FoldHierarchyListener l) {
        this.execution.addFoldHierarchyListener(l);
    }

    public void removeFoldHierarchyListener(FoldHierarchyListener l) {
        this.execution.removeFoldHierarchyListener(l);
    }

    public String toString() {
        return this.execution.toString();
    }

    public boolean isActive() {
        return this.execution.hasProviders();
    }

    static {
        FoldHierarchy.ensureApiAccessorRegistered();
    }

    private static final class ApiPackageAccessorImpl
    extends ApiPackageAccessor {
        private ApiPackageAccessorImpl() {
        }

        @Override
        public FoldHierarchy createFoldHierarchy(FoldHierarchyExecution execution) {
            return new FoldHierarchy(execution);
        }

        @Override
        public Fold createFold(FoldOperationImpl operation, FoldType type, String description, boolean collapsed, Document doc, int startOffset, int endOffset, int startGuardedLength, int endGuardedLength, Object extraInfo) throws BadLocationException {
            return new Fold(operation, type, description, collapsed, doc, startOffset, endOffset, startGuardedLength, endGuardedLength, extraInfo);
        }

        @Override
        public FoldHierarchyEvent createFoldHierarchyEvent(FoldHierarchy source, Fold[] removedFolds, Fold[] addedFolds, FoldStateChange[] foldStateChanges, int affectedStartOffset, int affectedEndOffset) {
            return new FoldHierarchyEvent(source, removedFolds, addedFolds, foldStateChanges, affectedStartOffset, affectedEndOffset);
        }

        @Override
        public FoldStateChange createFoldStateChange(Fold fold) {
            return new FoldStateChange(fold);
        }

        @Override
        public void foldSetParent(Fold fold, Fold parent) {
            fold.setParent(parent);
        }

        @Override
        public void foldExtractToChildren(Fold fold, int index, int length, Fold targetFold) {
            fold.extractToChildren(index, length, targetFold);
        }

        @Override
        public Fold foldReplaceByChildren(Fold fold, int index) {
            return fold.replaceByChildren(index);
        }

        @Override
        public void foldSetCollapsed(Fold fold, boolean collapsed) {
            fold.setCollapsed(collapsed);
        }

        @Override
        public void foldSetDescription(Fold fold, String description) {
            fold.setDescription(description);
        }

        @Override
        public void foldSetStartOffset(Fold fold, Document doc, int startOffset) throws BadLocationException {
            fold.setStartOffset(doc, startOffset);
        }

        @Override
        public void foldSetEndOffset(Fold fold, Document doc, int endOffset) throws BadLocationException {
            fold.setEndOffset(doc, endOffset);
        }

        @Override
        public boolean foldIsStartDamaged(Fold fold) {
            return fold.isStartDamaged();
        }

        @Override
        public boolean foldIsEndDamaged(Fold fold) {
            return fold.isEndDamaged();
        }

        @Override
        public void foldInsertUpdate(Fold fold, DocumentEvent evt) {
        }

        @Override
        public void foldRemoveUpdate(Fold fold, DocumentEvent evt) {
        }

        @Override
        public FoldOperationImpl foldGetOperation(Fold fold) {
            return fold.getOperation();
        }

        @Override
        public int foldGetRawIndex(Fold fold) {
            return fold.getRawIndex();
        }

        @Override
        public void foldSetRawIndex(Fold fold, int rawIndex) {
            fold.setRawIndex(rawIndex);
        }

        @Override
        public void foldUpdateRawIndex(Fold fold, int rawIndexDelta) {
            fold.updateRawIndex(rawIndexDelta);
        }

        @Override
        public Object foldGetExtraInfo(Fold fold) {
            return fold.getExtraInfo();
        }

        @Override
        public void foldSetExtraInfo(Fold fold, Object info) {
            fold.setExtraInfo(info);
        }

        @Override
        public void foldStateChangeCollapsedChanged(FoldStateChange fsc) {
            fsc.collapsedChanged();
        }

        @Override
        public void foldStateChangeDescriptionChanged(FoldStateChange fsc) {
            fsc.descriptionChanged();
        }

        @Override
        public void foldStateChangeStartOffsetChanged(FoldStateChange fsc, int originalStartOffset) {
            fsc.startOffsetChanged(originalStartOffset);
        }

        @Override
        public void foldStateChangeEndOffsetChanged(FoldStateChange fsc, int originalEndOffset) {
            fsc.endOffsetChanged(originalEndOffset);
        }

        @Override
        public FoldHierarchyExecution foldGetExecution(FoldHierarchy h) {
            return h.execution;
        }

        @Override
        public void foldMarkDamaged(Fold f, int flags) {
            f.setDamaged((byte)flags);
        }

        @Override
        public int foldStartGuardedLength(Fold f) {
            return f.getStartGuardedLength();
        }

        @Override
        public int foldEndGuardedLength(Fold f) {
            return f.getEndGuardedLength();
        }

        @Override
        public void foldTearOut(Fold f, Collection c) {
            int index;
            Fold parent = f.getParent();
            this.foldTearOut2(f, c);
            if (parent != null && (index = parent.getFoldIndex(f)) >= 0) {
                parent.replaceByChildren(index);
            }
        }

        private void foldTearOut2(Fold f, Collection c) {
            int cnt = f.getFoldCount();
            if (cnt > 0) {
                Fold[] children;
                for (Fold x : children = f.foldsToArray(0, cnt)) {
                    this.foldTearOut2(x, c);
                }
            }
            c.add(f);
            f.setChildren(null);
            f.setParent(null);
        }
    }

}

