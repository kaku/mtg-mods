/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.api.editor.fold;

import org.netbeans.api.editor.fold.Fold;
import org.netbeans.modules.editor.fold.FoldUtilitiesImpl;

public final class FoldStateChange {
    private static final int COLLAPSED_CHANGED_BIT = 1;
    private static final int START_OFFSET_CHANGED_BIT = 2;
    private static final int END_OFFSET_CHANGED_BIT = 4;
    private static final int DESCRIPTION_CHANGED_BIT = 8;
    private Fold fold;
    private int stateChangeBits;
    private int originalStartOffset = -1;
    private int originalEndOffset = -1;

    FoldStateChange(Fold fold) {
        this.fold = fold;
    }

    public Fold getFold() {
        return this.fold;
    }

    public boolean isCollapsedChanged() {
        return (this.stateChangeBits & 1) != 0;
    }

    public boolean isStartOffsetChanged() {
        return (this.stateChangeBits & 2) != 0;
    }

    public int getOriginalStartOffset() {
        return this.originalStartOffset;
    }

    public boolean isEndOffsetChanged() {
        return (this.stateChangeBits & 4) != 0;
    }

    public int getOriginalEndOffset() {
        return this.originalEndOffset;
    }

    public boolean isDescriptionChanged() {
        return (this.stateChangeBits & 8) != 0;
    }

    void collapsedChanged() {
        this.stateChangeBits |= 1;
    }

    void startOffsetChanged(int originalStartOffset) {
        this.stateChangeBits |= 2;
        this.originalStartOffset = originalStartOffset;
    }

    void endOffsetChanged(int originalEndOffset) {
        this.stateChangeBits |= 4;
        this.originalEndOffset = originalEndOffset;
    }

    void descriptionChanged() {
        this.stateChangeBits |= 8;
    }

    public String toString() {
        return FoldUtilitiesImpl.foldStateChangeToString(this);
    }
}

