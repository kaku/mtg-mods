/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.api.editor.fold;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.fold.FoldRegistry;
import org.netbeans.modules.editor.fold.FoldUtilitiesImpl;

public final class FoldUtilities {
    private FoldUtilities() {
    }

    public static boolean isRootFold(Fold fold) {
        return fold.isRootFold();
    }

    public static int findFoldStartIndex(Fold fold, int offset) {
        return FoldUtilitiesImpl.findFoldStartIndex(fold, offset, true);
    }

    public static int findFoldEndIndex(Fold fold, int offset) {
        return FoldUtilitiesImpl.findFoldEndIndex(fold, offset);
    }

    public static boolean isEmpty(Fold fold) {
        return fold.getStartOffset() == fold.getEndOffset();
    }

    public static void collapseAll(FoldHierarchy hierarchy) {
        FoldUtilities.collapse(hierarchy, (Collection)null);
    }

    public static void collapse(FoldHierarchy hierarchy, FoldType type) {
        FoldUtilities.collapse(hierarchy, Collections.singleton(type));
    }

    public static void collapse(FoldHierarchy hierarchy, Collection foldTypes) {
        FoldUtilitiesImpl.collapseOrExpand(hierarchy, foldTypes, true);
    }

    public static void expandAll(FoldHierarchy hierarchy) {
        FoldUtilities.expand(hierarchy, (Collection)null);
    }

    public static void expand(FoldHierarchy hierarchy, FoldType type) {
        FoldUtilities.expand(hierarchy, Collections.singleton(type));
    }

    public static void expand(FoldHierarchy hierarchy, Collection foldTypes) {
        FoldUtilitiesImpl.collapseOrExpand(hierarchy, foldTypes, false);
    }

    public static boolean containsOffset(Fold fold, int offset) {
        return offset < fold.getEndOffset() && offset >= fold.getStartOffset();
    }

    public static Fold[] childrenToArray(Fold fold) {
        return FoldUtilities.childrenToArray(fold, 0, fold.getFoldCount());
    }

    public static Fold[] childrenToArray(Fold fold, int index, int count) {
        return fold.foldsToArray(index, count);
    }

    public static List childrenAsList(Fold fold) {
        return FoldUtilities.childrenAsList(fold, 0, fold.getFoldCount());
    }

    public static List childrenAsList(Fold fold, int index, int count) {
        return FoldUtilitiesImpl.childrenAsList(fold, index, count);
    }

    public static List find(Fold fold, FoldType foldType) {
        return FoldUtilities.find(fold, Collections.singletonList(foldType));
    }

    public static List find(Fold fold, Collection foldTypes) {
        return FoldUtilitiesImpl.find(fold, foldTypes);
    }

    public static List findRecursive(Fold fold) {
        return FoldUtilities.findRecursive(fold, (Collection)null);
    }

    public static List findRecursive(Fold fold, FoldType foldType) {
        return FoldUtilities.findRecursive(fold, Collections.singletonList(foldType));
    }

    public static List findRecursive(Fold fold, Collection foldTypes) {
        return FoldUtilitiesImpl.findRecursive(null, fold, foldTypes);
    }

    public static Fold findNearestFold(FoldHierarchy hierarchy, int offset) {
        return FoldUtilitiesImpl.findNearestFold(hierarchy, offset, Integer.MAX_VALUE);
    }

    public static Fold findOffsetFold(FoldHierarchy hierarchy, int offset) {
        return FoldUtilitiesImpl.findOffsetFold(hierarchy, offset);
    }

    public static Fold findCollapsedFold(FoldHierarchy hierarchy, int startOffset, int endOffset) {
        return FoldUtilitiesImpl.findFirstCollapsedFold(hierarchy, startOffset, endOffset);
    }

    public static Iterator collapsedFoldIterator(FoldHierarchy hierarchy, int startOffset, int endOffset) {
        return FoldUtilitiesImpl.collapsedFoldIterator(hierarchy, startOffset, endOffset);
    }

    public static FoldType.Domain getFoldTypes(String mimeType) {
        return FoldRegistry.get().getDomain(MimePath.parse((String)mimeType));
    }

    public static boolean isAutoCollapsed(FoldType ft, FoldHierarchy hierarchy) {
        return FoldUtilitiesImpl.isAutoCollapsed(ft, hierarchy);
    }

    public static boolean isFoldingEnabled(String mimeType) {
        return FoldUtilitiesImpl.isFoldingEnabled(mimeType);
    }
}

