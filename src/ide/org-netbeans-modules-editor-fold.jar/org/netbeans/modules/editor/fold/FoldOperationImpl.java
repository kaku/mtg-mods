/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.fold;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;
import org.netbeans.modules.editor.fold.FoldHierarchyExecution;
import org.netbeans.modules.editor.fold.FoldHierarchyTransactionImpl;
import org.netbeans.modules.editor.fold.SpiPackageAccessor;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldInfo;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.util.Exceptions;

public final class FoldOperationImpl {
    private static final Logger LOG = Logger.getLogger(FoldHierarchy.class.getName());
    private FoldOperation operation;
    private FoldHierarchyExecution execution;
    private FoldManager manager;
    private int priority;
    private boolean released;
    private static final Comparator<Fold> FOLD_COMPARATOR = new Comparator<Fold>(){

        @Override
        public int compare(Fold a, Fold b) {
            int diff = a.getStartOffset() - b.getStartOffset();
            if (diff != 0) {
                return diff;
            }
            int diff2 = b.getEndOffset() - a.getEndOffset();
            if (diff2 != 0) {
                return diff2;
            }
            ApiPackageAccessor accessor = FoldOperationImpl.getAccessor();
            return accessor.foldGetOperation(a).getPriority() - accessor.foldGetOperation(b).getPriority();
        }
    };

    public FoldOperationImpl(FoldHierarchyExecution execution, FoldManager manager, int priority) {
        this.execution = execution;
        this.manager = manager;
        this.priority = priority;
        this.operation = SpiPackageAccessor.get().createFoldOperation(this);
        if (manager != null) {
            manager.init(this.getOperation());
        }
    }

    public FoldOperation getOperation() {
        return this.operation;
    }

    public void initFolds(FoldHierarchyTransactionImpl transaction) {
        this.manager.initFolds(transaction.getTransaction());
        if (LOG.isLoggable(Level.FINER)) {
            LOG.finer("Fold Hierarchy after initFolds():\n" + this.execution + '\n');
            this.execution.checkConsistency();
        }
    }

    public FoldHierarchy getHierarchy() {
        return this.execution.getHierarchy();
    }

    public FoldManager getManager() {
        return this.manager;
    }

    public int getPriority() {
        return this.priority;
    }

    public Document getDocument() {
        return this.execution.getComponent().getDocument();
    }

    public Fold createFold(FoldType type, String description, boolean collapsed, int startOffset, int endOffset, int startGuardedLength, int endGuardedLength, Object extraInfo) throws BadLocationException {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Creating fold: type=" + type + ", description='" + description + "', collapsed=" + collapsed + ", startOffset=" + startOffset + ", endOffset=" + endOffset + ", startGuardedLength=" + startGuardedLength + ", endGuardedLength=" + endGuardedLength + ", extraInfo=" + extraInfo + '\n');
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.INFO, "Fold creation stack", new Exception());
            }
        }
        if (type == null) {
            LOG.warning("Null fold type supplier for fold start=" + startOffset + ", end=" + endOffset + "by manager " + this.manager);
            type = FoldType.CODE_BLOCK;
        }
        return FoldOperationImpl.getAccessor().createFold(this, type, description, collapsed, this.getDocument(), startOffset, endOffset, startGuardedLength, endGuardedLength, extraInfo);
    }

    public Object getExtraInfo(Fold fold) {
        this.checkFoldOperation(fold);
        return FoldOperationImpl.getAccessor().foldGetExtraInfo(fold);
    }

    public boolean isStartDamaged(Fold fold) {
        this.checkFoldOperation(fold);
        return FoldOperationImpl.getAccessor().foldIsStartDamaged(fold);
    }

    public boolean isEndDamaged(Fold fold) {
        this.checkFoldOperation(fold);
        return FoldOperationImpl.getAccessor().foldIsEndDamaged(fold);
    }

    public FoldHierarchyTransactionImpl openTransaction() {
        return this.execution.openTransaction();
    }

    public boolean addToHierarchy(Fold fold, FoldHierarchyTransactionImpl transaction) {
        this.checkFoldOperation(fold);
        return this.execution.add(fold, transaction);
    }

    public void removeFromHierarchy(Fold fold, FoldHierarchyTransactionImpl transaction) {
        this.checkFoldOperation(fold);
        this.execution.remove(fold, transaction);
    }

    public boolean isAddedOrBlocked(Fold fold) {
        this.checkFoldOperation(fold);
        return this.execution.isAddedOrBlocked(fold);
    }

    public boolean isBlocked(Fold fold) {
        this.checkFoldOperation(fold);
        return this.execution.isBlocked(fold);
    }

    public void setEndOffset(Fold fold, int endOffset, FoldHierarchyTransactionImpl transaction) throws BadLocationException {
        this.checkFoldOperation(fold);
        int origEndOffset = fold.getEndOffset();
        if (origEndOffset == endOffset) {
            return;
        }
        ApiPackageAccessor api = FoldOperationImpl.getAccessor();
        FoldStateChange state = transaction.getFoldStateChange(fold);
        if (state.getOriginalStartOffset() >= 0 && state.getOriginalStartOffset() > endOffset) {
            LOG.warning("Original start offset > end offset, dumping fold hierarchy: " + this.execution);
        }
        api.foldSetEndOffset(fold, this.getDocument(), endOffset);
        api.foldStateChangeEndOffsetChanged(transaction.getFoldStateChange(fold), origEndOffset);
    }

    public void insertUpdate(DocumentEvent evt, FoldHierarchyTransactionImpl transaction) {
        if (!this.isReleased()) {
            this.manager.insertUpdate(evt, transaction.getTransaction());
        }
    }

    public void removeUpdate(DocumentEvent evt, FoldHierarchyTransactionImpl transaction) {
        if (!this.isReleased()) {
            this.manager.removeUpdate(evt, transaction.getTransaction());
        }
    }

    public void changedUpdate(DocumentEvent evt, FoldHierarchyTransactionImpl transaction) {
        if (!this.isReleased()) {
            this.manager.changedUpdate(evt, transaction.getTransaction());
        }
    }

    public void release() {
        this.released = true;
        this.manager.release();
    }

    public boolean isReleased() {
        return this.released;
    }

    public Iterator<Fold> foldIterator() {
        return new BI(new DFSI(this.execution.getRootFold()));
    }

    private void checkFoldOperation(Fold fold) {
        FoldOperationImpl foldOperation = FoldOperationImpl.getAccessor().foldGetOperation(fold);
        if (foldOperation != this) {
            throw new IllegalStateException("Attempt to use the fold " + fold + " with invalid fold operation " + foldOperation + " instead of " + this);
        }
    }

    private static ApiPackageAccessor getAccessor() {
        return ApiPackageAccessor.get();
    }

    public Map<FoldInfo, Fold> update(Collection<FoldInfo> fi, Collection<Fold> removed, Collection<FoldInfo> created) throws BadLocationException {
        Refresher r = new Refresher(fi);
        if (!this.isReleased()) {
            if (!this.execution.isLockedByCaller()) {
                throw new IllegalStateException("Update must run under FoldHierarchy lock");
            }
        } else {
            return null;
        }
        r.run();
        if (removed != null) {
            removed.addAll(r.toRemove);
        }
        if (created != null) {
            created.addAll(r.toAdd);
        }
        return r.currentFolds;
    }

    public boolean getInitialState(FoldType ft) {
        return this.execution.getInitialFoldState(ft);
    }

    public String toString() {
        return "FoldOp[mgr = " + this.manager + ", rel = " + this.released + "]";
    }

    private class Refresher
    implements Comparator<FoldInfo> {
        private Collection<FoldInfo> foldInfos;
        private List<Fold> toRemove;
        private Set<Fold> removedFolds;
        private Collection<FoldInfo> toAdd;
        private Map<FoldInfo, Fold> currentFolds;
        private Map<Fold, FoldInfo> foldsToUpdate;
        private FoldHierarchyTransactionImpl tran;
        private Iterator<Fold> foldIt;
        private Iterator<FoldInfo> infoIt;
        private FoldInfo nextInfo;
        private FoldStateChange fsch;

        public Refresher(Collection<FoldInfo> foldInfos) {
            this.toRemove = new ArrayList<Fold>();
            this.removedFolds = new HashSet<Fold>();
            this.toAdd = new ArrayList<FoldInfo>();
            this.currentFolds = new LinkedHashMap<FoldInfo, Fold>();
            this.foldsToUpdate = new IdentityHashMap<Fold, FoldInfo>();
            this.foldInfos = foldInfos;
        }

        @Override
        public int compare(FoldInfo a, FoldInfo b) {
            int diff = a.getStart() - b.getStart();
            if (diff != 0) {
                return diff;
            }
            int diff2 = b.getEnd() - a.getEnd();
            return diff2;
        }

        @Override
        private int compare(FoldInfo info, Fold f) {
            if (info == null) {
                if (f == null) {
                    return 0;
                }
                return 1;
            }
            if (f == null) {
                return -1;
            }
            int diff = info.getStart() - f.getStartOffset();
            if (diff != 0) {
                return diff;
            }
            diff = f.getEndOffset() - info.getEnd();
            if (diff != 0) {
                return diff;
            }
            if (info.getType() == f.getType()) {
                return 0;
            }
            return info.getType().code().compareToIgnoreCase(f.getType().code());
        }

        private FoldInfo ni() {
            FoldInfo f = null;
            do {
                if (this.nextInfo != null) {
                    f = this.nextInfo;
                    this.nextInfo = null;
                    if (this.isValidFold(f)) {
                        return f;
                    }
                }
                if (!this.infoIt.hasNext()) {
                    return null;
                }
                f = this.infoIt.next();
            } while (!this.isValidFold(f));
            return f;
        }

        private FoldInfo peek() {
            FoldInfo f;
            this.nextInfo = f = this.ni();
            return f;
        }

        private boolean containsOneAnother(FoldInfo i, Fold f) {
            int s1 = i.getStart();
            int s2 = f.getStartOffset();
            int e1 = i.getEnd();
            int e2 = f.getEndOffset();
            return s1 >= s2 && e2 >= e1 || s2 >= s1 && e1 >= e2;
        }

        private boolean nextSameRange(FoldInfo i, Fold f) {
            if (i == null || f == null) {
                return false;
            }
            if (i.getType() != f.getType() || !this.containsOneAnother(i, f)) {
                return false;
            }
            FoldInfo next = this.peek();
            if (next == null) {
                return true;
            }
            return next.getStart() > i.getEnd();
        }

        private Fold markRemoveFold(Fold f) {
            this.toRemove.add(f);
            this.removedFolds.add(f);
            Fold fold = f = this.foldIt.hasNext() ? this.foldIt.next() : null;
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.finest("Advanced fold, next = " + f);
            }
            return f;
        }

        private boolean isValidFold(FoldInfo fi) {
            if (fi.getStart() >= fi.getEnd()) {
                return false;
            }
            int glen = fi.getTemplate().getGuardedEnd() + fi.getTemplate().getGuardedStart();
            return fi.getStart() + glen <= fi.getEnd();
        }

        private boolean isValidFold(Fold f) {
            if (f.getParent() != null) {
                return true;
            }
            return FoldOperationImpl.this.execution.isBlocked(f);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void run() throws BadLocationException {
            ArrayList<FoldInfo> ll = new ArrayList<FoldInfo>(this.foldInfos);
            Collections.sort(ll, this);
            this.foldIt = FoldOperationImpl.this.foldIterator();
            this.infoIt = ll.iterator();
            this.tran = FoldOperationImpl.this.openTransaction();
            Document d = FoldOperationImpl.this.getDocument();
            int len = d.getLength();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Updating fold hierarchy, doclen = {1}, foldInfos = " + ll, len);
                ArrayList<Fold> c = new ArrayList<Fold>();
                while (this.foldIt.hasNext()) {
                    c.add(this.foldIt.next());
                }
                LOG.log(Level.FINE, "Current ordered folds: " + c);
                LOG.log(Level.FINE, "Current hierarchy: " + FoldOperationImpl.this.getOperation().getHierarchy());
                this.foldIt = FoldOperationImpl.this.foldIterator();
            }
            Fold f = this.foldIt.hasNext() ? this.foldIt.next() : null;
            FoldInfo i = this.ni();
            try {
                while (f != null || i != null) {
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("Fold = " + f + ", FoldInfo = " + i);
                    }
                    int action = this.compare(i, f);
                    boolean nextSameRange = this.nextSameRange(i, f);
                    if (f == null || action < 0 && !nextSameRange) {
                        this.toAdd.add(i);
                        i = this.ni();
                        if (!LOG.isLoggable(Level.FINEST)) continue;
                        LOG.finest("Advanced info, next = " + i);
                        continue;
                    }
                    if (i == null || action > 0 && !nextSameRange) {
                        f = this.markRemoveFold(f);
                        continue;
                    }
                    if (this.isChanged(f, i)) {
                        this.foldsToUpdate.put(f, i);
                    }
                    this.currentFolds.put(i, f);
                    i = this.ni();
                    Fold fold = f = this.foldIt.hasNext() ? this.foldIt.next() : null;
                    if (!LOG.isLoggable(Level.FINEST)) continue;
                    LOG.finest("Advanced both info & fold");
                }
                for (int ri = this.toRemove.size() - 1; ri >= 0; --ri) {
                    Fold fold = this.toRemove.get(ri);
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("Removing: " + f);
                    }
                    if (fold.getParent() == null) continue;
                    FoldOperationImpl.this.removeFromHierarchy(fold, this.tran);
                }
                for (Map.Entry<Fold, FoldInfo> updateEntry : this.foldsToUpdate.entrySet()) {
                    FoldInfo fi = updateEntry.getValue();
                    Fold ff = updateEntry.getKey();
                    if (!this.checkFoldInPlace(ff, fi)) {
                        LOG.finest("Updated fold does not fit in hierarchy, scheduling reinsertion: " + ff + ", info: " + fi);
                        this.tran.reinsertFoldTree(ff);
                    }
                    this.update(ff, fi);
                }
                for (FoldInfo info : this.toAdd) {
                    try {
                        int s = info.getStart();
                        int e = info.getEnd();
                        if (s > len || e > len + 1 || s >= e) continue;
                        this.currentFolds.put(info, FoldOperationImpl.this.getOperation().addToHierarchy(info.getType(), info.getStart(), info.getEnd(), info.getCollapsed(), info.getTemplate(), info.getDescriptionOverride(), info.getExtraInfo(), this.tran.getTransaction()));
                        if (!LOG.isLoggable(Level.FINEST)) continue;
                        LOG.finest("Adding: " + i);
                    }
                    catch (BadLocationException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                }
                if (LOG.isLoggable(Level.FINE)) {
                    FoldOperationImpl.this.execution.checkConsistency();
                }
            }
            finally {
                this.tran.commit();
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Updated fold hierarchy: " + FoldOperationImpl.this.getOperation().getHierarchy());
                }
            }
        }

        private boolean isChanged(Fold f, FoldInfo info) {
            int fs = f.getStartOffset();
            int fe = f.getEndOffset();
            boolean c = f.isCollapsed();
            String fd = f.getDescription();
            if (fs != info.getStart() || fe != info.getEnd()) {
                return true;
            }
            if (info.getCollapsed() != null && c != info.getCollapsed()) {
                return true;
            }
            return !fd.equals(this.getInfoDescription(info));
        }

        private String getInfoDescription(FoldInfo info) {
            String desc = info.getDescriptionOverride();
            if (desc == null) {
                desc = info.getTemplate().getDescription();
            }
            return desc;
        }

        private int getUpdatedFoldStart(Fold f) {
            FoldInfo update = this.foldsToUpdate.get(f);
            if (update == null) {
                return f.getStartOffset();
            }
            return update.getStart();
        }

        private int getUpdatedFoldEnd(Fold f) {
            FoldInfo update = this.foldsToUpdate.get(f);
            if (update == null) {
                return f.getEndOffset();
            }
            return update.getEnd();
        }

        private boolean checkFoldInPlace(Fold f, FoldInfo info) {
            Fold prev;
            Fold next;
            if (FoldOperationImpl.this.getHierarchy().getRootFold() == f) {
                return true;
            }
            Fold parent = f.getParent();
            if (parent == null) {
                return !FoldOperationImpl.this.execution.isBlocked(f);
            }
            int s = this.getUpdatedFoldStart(parent);
            int e = this.getUpdatedFoldEnd(parent);
            int is = info.getStart();
            int ie = info.getEnd();
            if (is < s || ie > e) {
                return false;
            }
            int index = parent.getFoldIndex(f);
            if (index > 0 && is < (e = this.getUpdatedFoldEnd(prev = parent.getFold(index - 1)))) {
                return false;
            }
            if (index < parent.getFoldCount() - 1 && ie > (s = this.getUpdatedFoldStart(next = parent.getFold(index + 1)))) {
                return false;
            }
            int cc = f.getFoldCount();
            if (cc > 0) {
                Fold c1 = f.getFold(0);
                s = this.getUpdatedFoldStart(c1);
                if (is > s) {
                    return false;
                }
                Fold c2 = cc > 1 ? f.getFold(cc - 1) : c1;
                e = this.getUpdatedFoldEnd(c2);
                if (ie < e) {
                    return false;
                }
            }
            return true;
        }

        public Fold update(Fold f, FoldInfo info) throws BadLocationException {
            int soffs;
            int index;
            int eoffs;
            this.fsch = null;
            int origStart = soffs = f.getStartOffset();
            ApiPackageAccessor acc = FoldOperationImpl.getAccessor();
            int len = FoldOperationImpl.this.getDocument().getLength();
            if (info.getStart() > len || info.getEnd() > len + 1) {
                return f;
            }
            if (info.getStart() >= info.getEnd()) {
                LOG.warning("FoldInfo: " + info + ", invalid start and end offsets");
                return f;
            }
            if (info.getStart() != soffs) {
                acc.foldSetStartOffset(f, FoldOperationImpl.this.getDocument(), info.getStart());
                FoldStateChange state = this.getFSCH(f);
                if (state.getOriginalEndOffset() >= 0 && state.getOriginalEndOffset() < soffs) {
                    FoldOperationImpl.this.execution.markDamaged();
                    LOG.warning("Original start offset > end offset, dumping fold hierarchy: " + FoldOperationImpl.this.execution);
                    LOG.warning("FoldInfo: " + info + ", fold: " + f);
                }
                acc.foldStateChangeStartOffsetChanged(state, soffs);
                soffs = info.getStart();
            }
            int origEnd = eoffs = f.getEndOffset();
            if (info.getEnd() != eoffs) {
                FoldStateChange state = this.getFSCH(f);
                if (state.getOriginalStartOffset() >= 0 && state.getOriginalStartOffset() > eoffs) {
                    FoldOperationImpl.this.execution.markDamaged();
                    LOG.warning("Original end offset < start offset, dumping fold hierarchy: " + FoldOperationImpl.this.execution);
                    LOG.warning("FoldInfo: " + info + ", fold: " + f);
                }
                acc.foldSetEndOffset(f, FoldOperationImpl.this.getDocument(), info.getEnd());
                acc.foldStateChangeEndOffsetChanged(state, eoffs);
                eoffs = info.getEnd();
            }
            if (soffs > eoffs) {
                FoldOperationImpl.this.execution.markDamaged();
                LOG.warning("Updated end offset < start offset, dumping fold hierarchy: " + FoldOperationImpl.this.execution);
                LOG.warning("FoldInfo: " + info + ", fold: " + f);
            }
            String desc = this.getInfoDescription(info);
            Fold p = f.getParent();
            if (p != null && (index = p.getFoldIndex(f)) != -1) {
                Fold prev;
                Fold next;
                if (index > 0 && (prev = p.getFold(index - 1)).getEndOffset() > f.getStartOffset()) {
                    FoldOperationImpl.this.execution.markDamaged();
                    LOG.warning("Wrong fold nesting after update, hierarchy: " + FoldOperationImpl.this.execution);
                    LOG.warning("FoldInfo: " + info + ", fold: " + f + " origStart-End" + origStart + "-" + origEnd);
                }
                if (index < p.getFoldCount() - 1 && (next = p.getFold(index + 1)).getStartOffset() < f.getEndOffset()) {
                    FoldOperationImpl.this.execution.markDamaged();
                    LOG.warning("Wrong fold nesting after update, hierarchy: " + FoldOperationImpl.this.execution);
                    LOG.warning("FoldInfo: " + info + ", fold: " + f + " origStart-End" + origStart + "-" + origEnd);
                }
            }
            if (!f.getDescription().equals(desc)) {
                acc.foldSetDescription(f, desc);
                acc.foldStateChangeDescriptionChanged(this.getFSCH(f));
            }
            if (info.getCollapsed() != null && f.isCollapsed() != info.getCollapsed().booleanValue()) {
                acc.foldSetCollapsed(f, info.getCollapsed());
                acc.foldStateChangeCollapsedChanged(this.getFSCH(f));
            }
            return f;
        }

        private FoldStateChange getFSCH(Fold f) {
            if (this.fsch != null) {
                return this.fsch;
            }
            this.fsch = this.tran.getFoldStateChange(f);
            return this.fsch;
        }
    }

    private class BI
    implements Iterator<Fold> {
        private Iterator<Fold> dfsi;
        private Iterator<Fold> blockedFolds;
        private Fold ret;
        private Stack<Object[]> blockStack;
        private Fold blocker;

        public BI(Iterator<Fold> dfsi) {
            this.blockStack = new Stack();
            this.dfsi = dfsi;
        }

        private boolean processBlocked(Fold f) {
            if (f == this.blocker) {
                return false;
            }
            Set<Fold> blocked = FoldOperationImpl.this.execution.getBlockedFolds(f);
            if (blocked != null && !blocked.isEmpty()) {
                ArrayList<Fold> blockedSorted = new ArrayList<Fold>(blocked.size() + 1);
                blockedSorted.addAll(blocked);
                blockedSorted.add(f);
                Collections.sort(blockedSorted, FOLD_COMPARATOR);
                this.blockStack.push(new Object[]{this.blockedFolds, this.blocker});
                this.blockedFolds = blockedSorted.iterator();
                this.blocker = f;
                return true;
            }
            return false;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Fold next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            Fold f = this.ret;
            this.ret = null;
            return f;
        }

        @Override
        public boolean hasNext() {
            Fold f;
            if (this.ret != null) {
                return true;
            }
            if (this.blockedFolds != null) {
                while (this.blockedFolds.hasNext()) {
                    f = this.blockedFolds.next();
                    if (this.processBlocked(f) || !FoldOperationImpl.this.operation.owns(f)) continue;
                    this.ret = f;
                    return true;
                }
                this.blockedFolds = null;
            }
            if (!this.blockStack.isEmpty()) {
                Object[] o = this.blockStack.pop();
                this.blocker = (Fold)o[1];
                this.blockedFolds = (Iterator)o[0];
                return this.hasNext();
            }
            while (this.dfsi.hasNext()) {
                f = this.dfsi.next();
                if (this.processBlocked(f)) {
                    return this.hasNext();
                }
                if (!FoldOperationImpl.this.operation.owns(f)) continue;
                this.ret = f;
                return true;
            }
            return false;
        }
    }

    private class DFSI
    implements Iterator<Fold> {
        PS level;

        private DFSI(Fold root) {
            this.level = new PS(root, null);
        }

        @Override
        public Fold next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            if (this.level.childIndex == -1) {
                this.level.childIndex++;
                return this.level.parent;
            }
            Fold f = this.level.parent.getFold(this.level.childIndex++);
            if (f.getFoldCount() > 0) {
                this.level = new PS(f, this.level);
                this.level.childIndex++;
                return this.level.parent;
            }
            return f;
        }

        @Override
        public boolean hasNext() {
            while (this.level != null) {
                if (this.level.childIndex == -1) {
                    return true;
                }
                if (this.level.childIndex >= this.level.parent.getFoldCount()) {
                    this.level = this.level.next;
                    continue;
                }
                return true;
            }
            return false;
        }

        @Override
        public void remove() {
            throw new IllegalArgumentException();
        }
    }

    static class PS {
        private Fold parent;
        private int childIndex = -1;
        private PS next;

        PS(Fold parent, PS next) {
            this.parent = parent;
            this.next = next;
        }
    }

}

