/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.lib.editor.util.PriorityMutex
 *  org.netbeans.lib.editor.util.swing.DocumentListenerPriority
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.ErrorManager
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold;

import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.lib.editor.util.PriorityMutex;
import org.netbeans.lib.editor.util.swing.DocumentListenerPriority;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;
import org.netbeans.modules.editor.fold.FoldHierarchyTransactionImpl;
import org.netbeans.modules.editor.fold.FoldManagerFactoryProvider;
import org.netbeans.modules.editor.fold.FoldOperationImpl;
import org.netbeans.modules.editor.fold.FoldUtilitiesImpl;
import org.netbeans.modules.editor.fold.LegacySettingsSync;
import org.netbeans.spi.editor.fold.FoldHierarchyMonitor;
import org.netbeans.spi.editor.fold.FoldManager;
import org.netbeans.spi.editor.fold.FoldManagerFactory;
import org.netbeans.spi.editor.fold.FoldOperation;
import org.openide.ErrorManager;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.util.Task;
import org.openide.util.WeakListeners;

public final class FoldHierarchyExecution
implements DocumentListener,
Runnable {
    private static final Logger LOG = Logger.getLogger(FoldHierarchyExecution.class.getName());
    private static final Logger PREF_LOG = Logger.getLogger(FoldHierarchy.class.getName() + ".enabled");
    private static final RequestProcessor RP = new RequestProcessor("Folding initializer");
    private static final String PROPERTY_FOLD_HIERARCHY_MUTEX = "foldHierarchyMutex";
    private static final String PROPERTY_FOLDING_ENABLED = "code-folding-enable";
    private static final boolean DEFAULT_CODE_FOLDING_ENABLED = true;
    private static final boolean debug = Boolean.getBoolean("netbeans.debug.editor.fold");
    private static final boolean debugFire = Boolean.getBoolean("netbeans.debug.editor.fold.fire");
    private static final FoldOperationImpl[] EMPTY_FOLD_OPERTAION_IMPL_ARRAY = new FoldOperationImpl[0];
    private final JTextComponent component;
    private FoldHierarchy hierarchy;
    private Fold rootFold;
    private FoldOperationImpl[] operations = EMPTY_FOLD_OPERTAION_IMPL_ARRAY;
    private Map blocked2block = new HashMap(4);
    private Map block2blockedSet = new HashMap(4);
    private int damaged;
    private String committedContent;
    private AbstractDocument lastDocument;
    private Reference<Document> lastRootDocument;
    private PriorityMutex mutex;
    private final EventListenerList listenerList;
    private boolean foldingEnabled;
    private FoldHierarchyTransactionImpl activeTransaction;
    private PropertyChangeListener componentChangesListener;
    private RequestProcessor.Task initTask;
    private volatile boolean active;
    private volatile Preferences foldPreferences;
    private PreferenceChangeListener prefL;
    private DocumentListener updateListener;
    private static final AtomicInteger TASK_WATCH;
    private volatile boolean suspended;
    private volatile Map<FoldType, Boolean> initialFoldState;
    private ThreadLocal<Save> removePostProcess;
    static final int OPERATION_EMPTY = 0;
    static final int OPERATION_DAMAGE = 8;
    static final int OPERATION_COLLAPSE = 16;
    static final int OPERATION_UPDATE = 24;
    static final int OPERATION_MASK = 24;
    private static volatile Method addUpdateListener;
    private static volatile Method removeUpdateListener;
    private static volatile Method eventInUndo;
    private static volatile Method eventInRedo;

    public static synchronized FoldHierarchy getOrCreateFoldHierarchy(JTextComponent component) {
        return FoldHierarchyExecution.getOrCreateFoldExecution(component).getHierarchy();
    }

    private static synchronized FoldHierarchyExecution getOrCreateFoldExecution(JTextComponent component) {
        if (component == null) {
            throw new NullPointerException("component cannot be null");
        }
        FoldHierarchyExecution execution = (FoldHierarchyExecution)component.getClientProperty(FoldHierarchyExecution.class);
        if (execution == null) {
            execution = new FoldHierarchyExecution(component);
            execution.init();
            component.putClientProperty(FoldHierarchyExecution.class, execution);
            String mime = DocumentUtilities.getMimeType((JTextComponent)component);
            Collection monitors = MimeLookup.getLookup((String)mime).lookupAll(FoldHierarchyMonitor.class);
            for (FoldHierarchyMonitor m : monitors) {
                try {
                    m.foldsAttached(execution.getHierarchy());
                }
                catch (Exception ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
        }
        return execution;
    }

    private FoldHierarchyExecution(JTextComponent component) {
        this.updateListener = new DL();
        this.suspended = false;
        this.initialFoldState = new HashMap<FoldType, Boolean>();
        this.removePostProcess = new ThreadLocal();
        this.component = component;
        this.listenerList = new EventListenerList();
    }

    public boolean hasProviders() {
        return this.active;
    }

    private void init() {
        this.mutex = (PriorityMutex)this.component.getClientProperty("foldHierarchyMutex");
        if (this.mutex == null) {
            this.mutex = new PriorityMutex();
            this.component.putClientProperty("foldHierarchyMutex", (Object)this.mutex);
        }
        this.hierarchy = ApiPackageAccessor.get().createFoldHierarchy(this);
        this.updateRootFold(this.component.getDocument());
        this.foldingEnabled = this.getFoldingEnabledSetting();
        this.startComponentChangesListening();
        this.active = !FoldManagerFactoryProvider.getDefault().getFactoryList(this.hierarchy).isEmpty();
        this.initTask = RP.create((Runnable)this);
        this.scheduleInit(500);
    }

    private void updateRootFold(Document doc) {
        Document d;
        if (this.lastRootDocument != null && (d = this.lastRootDocument.get()) == doc) {
            return;
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Updating root fold. lastDocument = {0}, newDocument = {1}", new Object[]{this.lastRootDocument, doc});
        }
        try {
            this.rootFold = ApiPackageAccessor.get().createFold(new FoldOperationImpl(this, null, Integer.MAX_VALUE), FoldHierarchy.ROOT_FOLD_TYPE, "root", false, doc, 0, doc.getEndPosition().getOffset(), 0, 0, null);
            this.lastRootDocument = new WeakReference<Document>(doc);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    public static void waitHierarchyInitialized(JTextComponent panel) {
        FoldHierarchyExecution.getOrCreateFoldExecution(panel).getInitTask().waitFinished();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    static boolean waitAllTasks() throws InterruptedException {
        AtomicInteger atomicInteger = TASK_WATCH;
        synchronized (atomicInteger) {
            while (TASK_WATCH.get() > 0) {
                TASK_WATCH.wait(30000);
            }
        }
        return true;
    }

    private Task getInitTask() {
        return this.initTask;
    }

    @Override
    public void run() {
        this.rebuild(false);
        FoldHierarchyExecution.notifyTaskFinished();
    }

    public final FoldHierarchy getHierarchy() {
        return this.hierarchy;
    }

    public final void lock() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Locked FoldHierarchy for " + System.identityHashCode(this.getComponent()));
        }
        this.mutex.lock();
    }

    public final boolean isLockedByCaller() {
        return this.mutex.getLockThread() == Thread.currentThread();
    }

    public void unlock() {
        if (this.activeTransaction != null) {
            this.activeTransaction.cancelled();
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Unlocked FoldHierarchy for " + System.identityHashCode(this.getComponent()));
        }
        this.mutex.unlock();
    }

    public JTextComponent getComponent() {
        return this.component;
    }

    public Fold getRootFold() {
        return this.rootFold;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addFoldHierarchyListener(FoldHierarchyListener l) {
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            this.listenerList.add(FoldHierarchyListener.class, l);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeFoldHierarchyListener(FoldHierarchyListener l) {
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            this.listenerList.remove(FoldHierarchyListener.class, l);
        }
    }

    void fireFoldHierarchyListener(FoldHierarchyEvent evt) {
        if (debugFire) {
            System.err.println("Firing FoldHierarchyEvent:\n" + evt);
        }
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = 0; i < listeners.length; i += 2) {
            if (listeners[i] != FoldHierarchyListener.class) continue;
            ((FoldHierarchyListener)listeners[i + 1]).foldHierarchyChanged(evt);
        }
    }

    public boolean add(Fold fold, FoldHierarchyTransactionImpl transaction) {
        if (fold.getParent() != null || this.isBlocked(fold)) {
            throw new IllegalStateException("Fold already added: " + fold);
        }
        boolean added = transaction.addFold(fold);
        return added;
    }

    public void remove(Fold fold, FoldHierarchyTransactionImpl transaction) {
        transaction.removeFold(fold);
    }

    public boolean isAddedOrBlocked(Fold fold) {
        return fold.getParent() != null || this.isBlocked(fold);
    }

    public boolean isBlocked(Fold fold) {
        return this.getBlock(fold) != null;
    }

    Fold getBlock(Fold fold) {
        return this.blocked2block.size() > 0 ? (Fold)this.blocked2block.get(fold) : null;
    }

    Set<Fold> getBlockedFolds(Fold f) {
        return (Set)this.block2blockedSet.get(f);
    }

    void markBlocked(Fold blocked, Fold block) {
        this.blocked2block.put(blocked, block);
        HashSet<Fold> blockedSet = (HashSet<Fold>)this.block2blockedSet.get(block);
        if (blockedSet == null) {
            blockedSet = new HashSet<Fold>();
            this.block2blockedSet.put(block, blockedSet);
        }
        if (!blockedSet.add(blocked)) {
            throw new IllegalStateException("fold " + blocked + " already blocked");
        }
    }

    Fold unmarkBlocked(Fold blocked) {
        Fold block = (Fold)this.blocked2block.remove(blocked);
        if (block == null) {
            throw new IllegalArgumentException("Not blocked: " + blocked);
        }
        Set blockedSet = (Set)this.block2blockedSet.get(block);
        if (!blockedSet.remove(blocked)) {
            throw new IllegalStateException("Not blocker for " + blocked);
        }
        if (blockedSet.isEmpty()) {
            this.block2blockedSet.remove(block);
        }
        return block;
    }

    Set unmarkBlock(Fold block) {
        Set blockedSet = (Set)this.block2blockedSet.remove(block);
        if (blockedSet != null) {
            int size = this.blocked2block.size();
            this.blocked2block.keySet().removeAll(blockedSet);
            if (size - this.blocked2block.size() != blockedSet.size()) {
                throw new IllegalStateException("Not all removed: " + blockedSet);
            }
        }
        return blockedSet;
    }

    public void collapse(Collection c) {
        this.setCollapsed(c, true);
    }

    public void expand(Collection c) {
        this.setCollapsed(c, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void setCollapsed(Collection c, boolean collapsed) {
        FoldHierarchyTransactionImpl transaction = this.openTransaction();
        try {
            for (Fold fold : c) {
                transaction.setCollapsed(fold, collapsed);
            }
        }
        finally {
            transaction.commit();
        }
        this.active = this.operations.length > 0;
    }

    public FoldHierarchyTransactionImpl openTransaction() {
        if (this.activeTransaction != null) {
            throw new IllegalStateException("Active transaction already exists.");
        }
        this.activeTransaction = new FoldHierarchyTransactionImpl(this);
        return this.activeTransaction;
    }

    void clearActiveTransaction() {
        if (this.activeTransaction == null) {
            throw new IllegalStateException("No transaction in progress");
        }
        this.activeTransaction = null;
    }

    void createAndFireFoldHierarchyEvent(Fold[] removedFolds, Fold[] addedFolds, FoldStateChange[] foldStateChanges, int affectedStartOffset, int affectedEndOffset) {
        if (affectedStartOffset < 0) {
            throw new IllegalArgumentException("affectedStartOffset=" + affectedStartOffset + " < 0");
        }
        if (affectedEndOffset < affectedStartOffset) {
            throw new IllegalArgumentException("affectedEndOffset=" + affectedEndOffset + " < affectedStartOffset=" + affectedStartOffset);
        }
        FoldHierarchyEvent evt = ApiPackageAccessor.get().createFoldHierarchyEvent(this.hierarchy, removedFolds, addedFolds, foldStateChanges, affectedStartOffset, affectedEndOffset);
        this.fireFoldHierarchyListener(evt);
    }

    private void postWatchDocumentChanges(final boolean stop) {
        if (this.suspended == stop) {
            return;
        }
        TASK_WATCH.incrementAndGet();
        RP.post(new Runnable(){

            @Override
            public void run() {
                FoldHierarchyExecution.this.rebuild(stop);
                FoldHierarchyExecution.this.suspended = stop;
                FoldHierarchyExecution.notifyTaskFinished();
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void notifyTaskFinished() {
        AtomicInteger atomicInteger = TASK_WATCH;
        synchronized (atomicInteger) {
            TASK_WATCH.getAndDecrement();
            TASK_WATCH.notifyAll();
        }
    }

    public void rebuild() {
        this.rebuild(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void rebuild(boolean doRelease) {
        boolean releaseOnly;
        AbstractDocument adoc;
        Document doc = this.getComponent().getDocument();
        if (this.lastDocument != null) {
            FoldHierarchyExecution.invokeUpdateListener(doc, this.updateListener, false);
            DocumentUtilities.removeDocumentListener((Document)this.lastDocument, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.FOLD_UPDATE);
            this.lastDocument = null;
        }
        if (doc instanceof AbstractDocument) {
            adoc = (AbstractDocument)doc;
            releaseOnly = false;
        } else {
            adoc = null;
            releaseOnly = true;
        }
        if (!this.foldingEnabled) {
            releaseOnly = true;
        }
        releaseOnly |= doRelease;
        if (adoc != null) {
            adoc.readLock();
            this.updateRootFold(doc);
            if (!releaseOnly) {
                this.lastDocument = adoc;
                DocumentUtilities.addDocumentListener((Document)this.lastDocument, (DocumentListener)this, (DocumentListenerPriority)DocumentListenerPriority.FOLD_UPDATE);
                FoldHierarchyExecution.invokeUpdateListener(doc, this.updateListener, true);
                this.suspended = false;
            }
        }
        try {
            this.lock();
            try {
                this.rebuildManagers(releaseOnly);
            }
            finally {
                this.unlock();
            }
        }
        finally {
            if (adoc != null) {
                adoc.readUnlock();
            }
        }
    }

    private String getMimeType() {
        EditorKit ek = this.component.getUI().getEditorKit(this.component);
        String mimeType = ek != null ? ek.getContentType() : (this.component.getDocument() != null ? DocumentUtilities.getMimeType((Document)this.component.getDocument()) : "");
        return mimeType;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    private void rebuildManagers(boolean releaseOnly) {
        block15 : {
            if (FoldHierarchyExecution.LOG.isLoggable(Level.FINER)) {
                FoldHierarchyExecution.LOG.log(Level.FINER, "rebuilding fold managers, release = {0}, document = {1}, component = {2}", new Object[]{releaseOnly, this.lastDocument, Integer.toHexString(System.identityHashCode(this.component))});
            }
            for (i = 0; i < this.operations.length; ++i) {
                this.operations[i].release();
            }
            this.operations = FoldHierarchyExecution.EMPTY_FOLD_OPERTAION_IMPL_ARRAY;
            factoryList = releaseOnly != false ? Collections.emptyList() : FoldManagerFactoryProvider.getDefault().getFactoryList(this.getHierarchy());
            factoryListLength = factoryList.size();
            if (FoldHierarchyExecution.debug) {
                System.err.println("FoldHierarchy rebuild(): FoldManager factory count=" + factoryListLength);
            }
            priority = factoryListLength - 1;
            ok = false;
            try {
                this.operations = new FoldOperationImpl[factoryListLength];
                for (i = 0; i < factoryListLength; ++i) {
                    factory = (FoldManagerFactory)factoryList.get(i);
                    manager = factory.createFoldManager();
                    if (manager == null) continue;
                    this.operations[i] = new FoldOperationImpl(this, manager, priority);
                    --priority;
                }
                if (i < factoryListLength) {
                    ops = new FoldOperationImpl[i];
                    System.arraycopy(this.operations, 0, ops, 0, i);
                    this.operations = ops;
                }
                ok = true;
            }
            finally {
                this.damaged = 0;
                if (!ok) {
                    this.operations = FoldHierarchyExecution.EMPTY_FOLD_OPERTAION_IMPL_ARRAY;
                }
            }
            transaction = this.openTransaction();
            ok = false;
            try {
                allBlocked = new Fold[this.blocked2block.size()];
                this.blocked2block.keySet().toArray(allBlocked);
                transaction.removeAllFolds(allBlocked);
                for (i = 0; i < factoryListLength; ++i) {
                    this.operations[i].initFolds(transaction);
                }
                if (!FoldHierarchyExecution.LOG.isLoggable(Level.FINER)) break block15;
            }
            catch (Throwable var10_13) {
                if (!ok) {
                    this.operations = FoldHierarchyExecution.EMPTY_FOLD_OPERTAION_IMPL_ARRAY;
                }
                if (FoldHierarchyExecution.LOG.isLoggable(Level.FINER)) {
                    FoldHierarchyExecution.LOG.log(Level.FINER, "Fold managers initialized. New managers = {0}, status = {1}", new Object[]{Arrays.asList(this.operations), ok});
                }
                transaction.commit();
                throw var10_13;
            }
            FoldHierarchyExecution.LOG.log(Level.FINER, "Fold managers initialized. New managers = {0}, status = {1}", new Object[]{Arrays.asList(this.operations), ok});
        }
        transaction.commit();
        if (this.operations.length <= 0) ** GOTO lbl58
        v0 = true;
        ** GOTO lbl59
lbl58: // 1 sources:
        v0 = false;
lbl59: // 2 sources:
        this.active = v0;
    }

    private void scheduleInit(int delay) {
        if (!this.initTask.cancel()) {
            TASK_WATCH.incrementAndGet();
        }
        this.initTask.schedule(delay);
    }

    private void startComponentChangesListening() {
        if (this.componentChangesListener == null) {
            final ComponentL l = new ComponentL();
            this.componentChangesListener = l;
            this.getComponent().addPropertyChangeListener(this.componentChangesListener);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    FoldHierarchyExecution.this.getComponent().addHierarchyListener(l);
                }
            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void insertUpdate(DocumentEvent evt) {
        this.lock();
        try {
            FoldHierarchyTransactionImpl transaction = this.openTransaction();
            try {
                transaction.insertUpdate(evt);
                int operationsLength = this.operations.length;
                for (int i = 0; i < operationsLength; ++i) {
                    this.operations[i].insertUpdate(evt, transaction);
                }
            }
            finally {
                transaction.commit();
            }
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeUpdate(DocumentEvent evt) {
        this.lock();
        try {
            FoldHierarchyTransactionImpl transaction = this.openTransaction();
            try {
                transaction.removeUpdate(evt);
                int operationsLength = this.operations.length;
                for (int i = 0; i < operationsLength; ++i) {
                    this.operations[i].removeUpdate(evt, transaction);
                }
            }
            finally {
                transaction.commit();
            }
        }
        finally {
            this.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void changedUpdate(DocumentEvent evt) {
        this.lock();
        try {
            FoldHierarchyTransactionImpl transaction = this.openTransaction();
            try {
                transaction.changedUpdate(evt);
                int operationsLength = this.operations.length;
                for (int i = 0; i < operationsLength; ++i) {
                    this.operations[i].changedUpdate(evt, transaction);
                }
            }
            finally {
                transaction.commit();
            }
        }
        finally {
            this.unlock();
        }
    }

    private boolean getFoldingEnabledSetting() {
        return this.getFoldingEnabledSetting(true);
    }

    private boolean getFoldingEnabledSetting(boolean useProperty) {
        String mime;
        Boolean b;
        Boolean bl = b = useProperty ? (Boolean)this.component.getClientProperty("code-folding-enable") : null;
        if (b == null && this.component.getDocument() != null && (mime = DocumentUtilities.getMimeType((Document)this.component.getDocument())) != null) {
            Preferences prefs = this.getFoldPreferences();
            b = prefs.getBoolean("code-folding-enable", true);
        }
        boolean ret = b != null ? b : true;
        PREF_LOG.log(Level.FINE, "Execution read enable: " + ret);
        this.component.putClientProperty("code-folding-enable", ret);
        return ret;
    }

    public void foldingEnabledSettingChange() {
        boolean origFoldingEnabled = this.foldingEnabled;
        this.foldingEnabled = this.getFoldingEnabledSetting(false);
        if (origFoldingEnabled != this.foldingEnabled) {
            PREF_LOG.log(Level.FINE, "Execution scheduled fold update: " + this.foldingEnabled);
            this.scheduleInit(100);
        }
    }

    public void checkConsistency() {
        try {
            FoldHierarchyExecution.checkFoldConsistency(this.getRootFold());
        }
        catch (RuntimeException e) {
            System.err.println("FOLD HIERARCHY INCONSISTENCY FOUND\n" + this);
            throw e;
        }
    }

    private static void checkFoldConsistency(Fold fold) {
        int startOffset = fold.getStartOffset();
        int endOffset = fold.getEndOffset();
        int lastEndOffset = startOffset;
        for (int i = 0; i < fold.getFoldCount(); ++i) {
            Fold child = fold.getFold(i);
            if (child.getParent() != fold) {
                throw new IllegalStateException("Wrong parent of child=" + child + ": " + child.getParent() + " != " + fold);
            }
            int foldIndex = fold.getFoldIndex(child);
            if (foldIndex != i) {
                throw new IllegalStateException("Fold index " + foldIndex + " instead of " + i);
            }
            int childStartOffset = child.getStartOffset();
            int childEndOffset = child.getEndOffset();
            if (childStartOffset < lastEndOffset) {
                throw new IllegalStateException("childStartOffset=" + childStartOffset + " < lastEndOffset=" + lastEndOffset);
            }
            if (childStartOffset >= childEndOffset) {
                throw new IllegalStateException("childStartOffset=" + childStartOffset + " > childEndOffset=" + childEndOffset);
            }
            if (childStartOffset < startOffset || childEndOffset > endOffset) {
                throw new IllegalStateException("Invalid child offsets. Child = " + child + ", parent =" + fold);
            }
            lastEndOffset = childEndOffset;
            FoldHierarchyExecution.checkFoldConsistency(child);
        }
    }

    public String toString() {
        int operationsLength;
        StringBuffer sb = new StringBuffer();
        sb.append("component=");
        sb.append(System.identityHashCode(this.getComponent()));
        sb.append('\n');
        sb.append(FoldUtilitiesImpl.foldToStringChildren(this.hierarchy.getRootFold(), 0));
        sb.append('\n');
        if (this.blocked2block != null) {
            sb.append("BLOCKED\n");
            for (Map.Entry entry : this.blocked2block.entrySet()) {
                sb.append("    ");
                sb.append(entry.getKey());
                sb.append(" blocked-by ");
                sb.append(entry.getValue());
                sb.append('\n');
            }
        }
        if (this.block2blockedSet != null) {
            sb.append("BLOCKERS\n");
            for (Map.Entry entry : this.block2blockedSet.entrySet()) {
                sb.append("    ");
                sb.append(entry.getKey());
                sb.append('\n');
                Set blockedSet = (Set)entry.getValue();
                Iterator it2 = blockedSet.iterator();
                while (it2.hasNext()) {
                    sb.append("        blocks ");
                    sb.append(it2.next());
                    sb.append('\n');
                }
            }
        }
        if ((operationsLength = this.operations.length) > 0) {
            sb.append("Fold Managers\n");
            for (int i = 0; i < operationsLength; ++i) {
                sb.append("FOLD MANAGER [");
                sb.append(i);
                sb.append("]:\n");
                sb.append(this.operations[i].getManager());
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public boolean getInitialFoldState(FoldType ft) {
        if (!this.isLockedByCaller()) {
            throw new IllegalStateException("Must be called under FH lock");
        }
        Boolean b = this.initialFoldState.get(ft);
        if (b != null) {
            return b;
        }
        b = FoldUtilities.isAutoCollapsed(ft, this.hierarchy);
        this.initialFoldState.put(ft, b);
        return b;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Preferences getFoldPreferences() {
        if (this.foldPreferences == null) {
            FoldHierarchyExecution foldHierarchyExecution = this;
            synchronized (foldHierarchyExecution) {
                if (this.foldPreferences != null) {
                    return this.foldPreferences;
                }
                String mimeType = this.getMimeType();
                Preferences prefs = LegacySettingsSync.get().processMime(mimeType);
                if ("".equals(mimeType)) {
                    return prefs;
                }
                this.foldPreferences = prefs;
                this.prefL = new PreferenceChangeListener(){

                    @Override
                    public void preferenceChange(PreferenceChangeEvent evt) {
                        if ((evt.getKey() == null || evt.getKey().startsWith("code-folding-collapse-")) && !FoldHierarchyExecution.this.initialFoldState.isEmpty()) {
                            FoldHierarchyExecution.this.initialFoldState = new HashMap();
                        }
                        if (evt.getKey() != null && "code-folding-enable".equals(evt.getKey())) {
                            FoldHierarchyExecution.this.foldingEnabledSettingChange();
                        }
                    }
                };
                PreferenceChangeListener weakPrefL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefL, (Object)this.foldPreferences);
                this.foldPreferences.addPreferenceChangeListener(weakPrefL);
            }
        }
        return this.foldPreferences;
    }

    public Collection getRemovePostProcess(DocumentEvent evt) {
        Save p = this.removePostProcess.get();
        this.removePostProcess.remove();
        if (p == null || p.evt != evt) {
            return Collections.EMPTY_LIST;
        }
        return p.postProcess;
    }

    void preRemoveCheckDamaged(Fold fold, DocumentEvent evt, Collection damaged) {
        boolean removed;
        Fold prevChildFold;
        int removeOffset = evt.getOffset();
        int endRemove = removeOffset + evt.getLength();
        int childIndex = FoldUtilitiesImpl.findFoldStartIndex(fold, removeOffset, true);
        if (childIndex == -1) {
            if (fold.getFoldCount() == 0) {
                return;
            }
            Fold first = fold.getFold(0);
            if (first.getStartOffset() <= endRemove) {
                childIndex = 0;
            } else {
                return;
            }
        }
        if (childIndex >= 1 && (prevChildFold = fold.getFold(childIndex - 1)).getEndOffset() == removeOffset) {
            this.preRemoveCheckDamaged(prevChildFold, evt, damaged);
        }
        boolean startsWithin = false;
        do {
            Fold childFold;
            int flag;
            startsWithin = (childFold = fold.getFold(childIndex)).getStartOffset() < removeOffset && childFold.getEndOffset() <= endRemove;
            removed = false;
            if (FoldUtilitiesImpl.becomesEmptyAfterRemove(childFold, evt)) {
                damaged.add(6);
                damaged.add(childFold);
                this.preRemoveCheckDamaged(childFold, evt, damaged);
                removed = true;
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("preRemoveCheck: removed empty " + childFold + '\n');
                }
            } else {
                flag = FoldUtilitiesImpl.becomesDamagedByRemove(childFold, evt, false);
                if (flag != 0) {
                    damaged.add(8 | flag);
                    damaged.add(childFold);
                    this.preRemoveCheckDamaged(childFold, evt, damaged);
                    removed = true;
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.finer("preRemoveCheck: removed damaged " + childFold + '\n');
                    }
                } else if (childFold.getFoldCount() > 0) {
                    this.preRemoveCheckDamaged(childFold, evt, damaged);
                }
            }
            if (removed) continue;
            if (childFold.isCollapsed() && (flag = FoldUtilitiesImpl.becomesDamagedByRemove(childFold, evt, true)) > 0) {
                damaged.add(16 | flag);
                damaged.add(childFold);
                if (!LOG.isLoggable(Level.FINER)) continue;
                LOG.finer("preRemoveCheck: expansion needed " + childFold + '\n');
                continue;
            }
            damaged.add(24);
            damaged.add(childFold);
            if (!LOG.isLoggable(Level.FINER)) continue;
            LOG.finer("preRemoveCheck: removeUpdate call " + childFold + '\n');
        } while ((startsWithin || removed) && ++childIndex < fold.getFoldCount());
    }

    static boolean isEventInUndoRedoHack(DocumentEvent evt) {
        if (eventInRedo == null) {
            if (!evt.getClass().getName().endsWith("BaseDocumentEvent")) {
                return false;
            }
            try {
                eventInUndo = evt.getClass().getMethod("isInUndo", new Class[0]);
                eventInRedo = evt.getClass().getMethod("isInRedo", new Class[0]);
            }
            catch (NoSuchMethodException | SecurityException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return false;
            }
        }
        if (eventInRedo.getDeclaringClass() != evt.getClass()) {
            return false;
        }
        try {
            return (Boolean)eventInUndo.invoke(evt, new Object[0]) != false || (Boolean)eventInRedo.invoke(evt, new Object[0]) != false;
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
            return false;
        }
    }

    private static void invokeUpdateListener(Document doc, DocumentListener l, boolean add) {
        Method m;
        Class clazz;
        Method method = m = add ? addUpdateListener : removeUpdateListener;
        if (m == null) {
            try {
                m = doc.getClass().getMethod(add ? "addUpdateDocumentListener" : "removeUpdateDocumentListener", DocumentListener.class);
                if (!"org.netbeans.editor.BaseDocument".equals(m.getDeclaringClass().getName())) {
                    return;
                }
                if (add) {
                    addUpdateListener = m;
                } else {
                    removeUpdateListener = m;
                }
            }
            catch (NoSuchMethodException ex) {
                return;
            }
            catch (SecurityException ex) {
                Exceptions.printStackTrace((Throwable)ex);
                return;
            }
        }
        if (!(clazz = m.getDeclaringClass()).isInstance(doc)) {
            return;
        }
        try {
            m.invoke(doc, l);
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    int getDamagedCount() {
        return this.damaged;
    }

    void markDamaged() {
        ++this.damaged;
    }

    String getCommittedContent() {
        return this.committedContent;
    }

    void transactionCommitted() {
        if (this.damaged > 0) {
            Document d = this.component.getDocument();
            try {
                this.committedContent = d.getText(0, d.getLength());
            }
            catch (BadLocationException ex) {
                // empty catch block
            }
        }
    }

    static {
        FoldOperation.isBoundsValid(0, 0, 0, 0);
        TASK_WATCH = new AtomicInteger(0);
    }

    private class DL
    implements DocumentListener {
        private DL() {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }

        @Override
        public void removeUpdate(DocumentEvent evt) {
            ArrayList pp = new ArrayList(16);
            FoldHierarchyExecution.this.preRemoveCheckDamaged(FoldHierarchyExecution.this.rootFold, evt, pp);
            FoldHierarchyExecution.this.removePostProcess.set(new Save(evt, pp));
        }
    }

    private static class Save {
        private DocumentEvent evt;
        private Collection postProcess;

        public Save(DocumentEvent evt, Collection postProcess) {
            this.evt = evt;
            this.postProcess = postProcess;
        }
    }

    private class ComponentL
    implements PropertyChangeListener,
    HierarchyListener,
    Runnable {
        private boolean editorKitLive;
        private boolean updating;

        private ComponentL() {
            this.editorKitLive = true;
        }

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & 1) == 0) {
                return;
            }
            if (this.updating) {
                return;
            }
            this.updating = true;
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            this.updating = false;
            JTextComponent c = FoldHierarchyExecution.this.getComponent();
            boolean disableFolding = !c.isDisplayable() && !this.editorKitLive;
            FoldHierarchyExecution.this.postWatchDocumentChanges(disableFolding);
            this.editorKitLive = true;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propName = evt.getPropertyName();
            if ("document".equals(propName)) {
                FoldHierarchyExecution.this.foldingEnabled = FoldHierarchyExecution.this.getFoldingEnabledSetting();
                FoldHierarchyExecution.this.scheduleInit(0);
            } else if ("code-folding-enable".equals(propName)) {
                FoldHierarchyExecution.this.foldingEnabledSettingChange();
            } else if ("editorKit".equals(propName)) {
                this.editorKitLive = evt.getNewValue() != null;
            }
        }
    }

}

