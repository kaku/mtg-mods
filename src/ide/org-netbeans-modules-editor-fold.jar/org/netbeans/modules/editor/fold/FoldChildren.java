/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.GapList
 */
package org.netbeans.modules.editor.fold;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;

public final class FoldChildren
extends GapList {
    private static final Logger LOG = Logger.getLogger(FoldChildren.class.getName());
    private static final int INITIAL_INDEX_GAP_LENGTH = 1073741823;
    Fold parent;
    private int indexGapIndex;
    private int indexGapLength;
    private static int invalidIndexHierarchySnapshot;

    public FoldChildren(Fold parent) {
        this.parent = parent;
        this.indexGapLength = 1073741823;
    }

    public int getFoldCount() {
        return this.size();
    }

    public Fold getFold(int index) {
        if (index >= this.getFoldCount() && invalidIndexHierarchySnapshot == 0) {
            StringBuilder sb = new StringBuilder(200);
            sb.append("Invalid index=").append(index).append("; foldCount=").append(this.getFoldCount()).append('\n');
            if (this.parent != null) {
                ++invalidIndexHierarchySnapshot;
                sb.append(this.parent.getHierarchy().toString());
                --invalidIndexHierarchySnapshot;
            }
            throw new IndexOutOfBoundsException(sb.toString());
        }
        return (Fold)this.get(index);
    }

    public int getFoldIndex(Fold child) {
        int index = this.getTranslatedFoldIndex(ApiPackageAccessor.get().foldGetRawIndex(child));
        if (index < 0 || index >= this.getFoldCount() || this.getFold(index) != child) {
            index = -1;
        }
        return index;
    }

    public Fold[] foldsToArray(int index, int length) {
        Object[] folds = new Fold[length];
        this.copyElements(index, index + length, folds, 0);
        return folds;
    }

    public void insert(int index, Fold fold) {
        this.moveIndexGap(index);
        this.insertImpl(index, fold);
    }

    public void insert(int index, Fold[] folds) {
        this.moveIndexGap(index);
        this.insertImpl(index, folds);
    }

    public void remove(int index, int length) {
        this.moveIndexGap(index + length);
        for (int i = index + length - 1; i >= index; --i) {
            ApiPackageAccessor.get().foldSetParent(this.getFold(i), null);
        }
        super.remove(index, length);
        this.indexGapLength += length;
        this.indexGapIndex -= length;
    }

    public FoldChildren extractToChildren(int index, int length, Fold fold) {
        int ps = this.parent.getStartOffset();
        int pe = this.parent.getEndOffset();
        int fs = fold.getStartOffset();
        int fe = fold.getEndOffset();
        if (fs < ps || fe > pe) {
            LOG.log(Level.WARNING, "Illegal attempt to change hierarchy. Parent fold: {0}, fold to be inserted: {1}, extract from {2}, len {3}", new Object[]{this.parent, fold, index, length});
            LOG.log(Level.WARNING, "Dumping hierarchy: " + this.parent.getHierarchy(), new Throwable());
        }
        FoldChildren foldChildren = new FoldChildren(fold);
        if (length == 1) {
            Fold insertFold = this.getFold(index);
            this.remove(index, length);
            foldChildren.insert(0, insertFold);
        } else {
            Fold[] insertFolds = this.foldsToArray(index, length);
            this.remove(index, length);
            foldChildren.insert(0, insertFolds);
        }
        this.insertImpl(index, fold);
        return foldChildren;
    }

    public void replaceByChildren(int index, FoldChildren children) {
        this.remove(index, 1);
        if (children != null) {
            int childCount = children.getFoldCount();
            this.insertImpl(index, children, 0, childCount);
        }
    }

    private void insertImpl(int index, FoldChildren children, int childIndex, int childCount) {
        switch (childCount) {
            case 0: {
                break;
            }
            case 1: {
                this.insertImpl(index, children.getFold(childIndex));
                break;
            }
            default: {
                Fold[] folds = children.foldsToArray(childIndex, childCount);
                this.insertImpl(index, folds);
            }
        }
    }

    private void insertImpl(int index, Fold fold) {
        int ps = this.parent.getStartOffset();
        int pe = this.parent.getEndOffset();
        int fs = fold.getStartOffset();
        int fe = fold.getEndOffset();
        if (fs < ps || fe > pe) {
            LOG.log(Level.WARNING, "Illegal attempt to insert fold. Parent fold: {0}, fold to be inserted: {1}, at index {2}", new Object[]{this.parent, fold, index});
            LOG.log(Level.WARNING, "Dumping hierarchy: " + this.parent.getHierarchy(), new Throwable());
        }
        --this.indexGapLength;
        ++this.indexGapIndex;
        ApiPackageAccessor api = ApiPackageAccessor.get();
        api.foldSetRawIndex(fold, index);
        api.foldSetParent(fold, this.parent);
        this.add(index, (Object)fold);
    }

    private void insertImpl(int index, Fold[] folds) {
        if (folds.length > 0) {
            int ps = this.parent.getStartOffset();
            int pe = this.parent.getEndOffset();
            for (Fold f : folds) {
                int fs = f.getStartOffset();
                int fe = f.getEndOffset();
                if (fs >= ps && fe <= pe) continue;
                LOG.log(Level.WARNING, "Illegal attempt to insert fold. Parent fold: {0}, folds to be inserted: {1}, at index {2}", new Object[]{this.parent, Arrays.asList(folds), index});
                LOG.log(Level.WARNING, "Dumping hierarchy: " + this.parent.getHierarchy(), new Throwable());
                break;
            }
        }
        ApiPackageAccessor api = ApiPackageAccessor.get();
        int foldsLength = folds.length;
        this.indexGapLength -= foldsLength;
        this.indexGapIndex += foldsLength;
        for (int i = foldsLength - 1; i >= 0; --i) {
            Fold fold = folds[i];
            api.foldSetRawIndex(fold, index + i);
            api.foldSetParent(fold, this.parent);
        }
        this.addArray(index, (Object[])folds);
    }

    private int getTranslatedFoldIndex(int rawIndex) {
        if (rawIndex >= this.indexGapLength) {
            rawIndex -= this.indexGapLength;
        }
        return rawIndex;
    }

    private void moveIndexGap(int index) {
        if (index != this.indexGapIndex) {
            ApiPackageAccessor api = ApiPackageAccessor.get();
            int gapLen = this.indexGapLength;
            if (index < this.indexGapIndex) {
                for (int i = this.indexGapIndex - 1; i >= index; --i) {
                    api.foldUpdateRawIndex(this.getFold(i), gapLen);
                }
            } else {
                for (int i = this.indexGapIndex; i < index; ++i) {
                    api.foldUpdateRawIndex(this.getFold(i), - gapLen);
                }
            }
            this.indexGapIndex = index;
        }
    }
}

