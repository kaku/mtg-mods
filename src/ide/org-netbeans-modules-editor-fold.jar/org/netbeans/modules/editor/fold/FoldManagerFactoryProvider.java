/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold;

import java.util.Collections;
import java.util.List;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.modules.editor.fold.CustomProvider;
import org.netbeans.modules.editor.fold.LayerProvider;

public abstract class FoldManagerFactoryProvider {
    private static FoldManagerFactoryProvider defaultProvider;
    private static FoldManagerFactoryProvider emptyProvider;
    private static boolean forceCustom;

    public static synchronized FoldManagerFactoryProvider getDefault() {
        if (defaultProvider == null) {
            defaultProvider = FoldManagerFactoryProvider.findDefault();
        }
        return defaultProvider;
    }

    public static FoldManagerFactoryProvider getEmpty() {
        if (emptyProvider == null) {
            emptyProvider = new EmptyProvider();
        }
        return emptyProvider;
    }

    public static synchronized void setForceCustomProvider(boolean forceCustomProvider) {
        if (!forceCustom) {
            defaultProvider = null;
        }
        forceCustom = forceCustomProvider;
    }

    private static FoldManagerFactoryProvider findDefault() {
        FoldManagerFactoryProvider provider = null;
        if (!forceCustom) {
            try {
                provider = new LayerProvider();
            }
            catch (Throwable t) {
                // empty catch block
            }
        }
        if (provider == null) {
            provider = new CustomProvider();
        }
        return provider;
    }

    public abstract List getFactoryList(FoldHierarchy var1);

    private static final class EmptyProvider
    extends FoldManagerFactoryProvider {
        private EmptyProvider() {
        }

        @Override
        public List getFactoryList(FoldHierarchy hierarchy) {
            return Collections.EMPTY_LIST;
        }
    }

}

