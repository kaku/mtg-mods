/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.OverridePreferences
 */
package org.netbeans.modules.editor.fold;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.settings.storage.api.OverridePreferences;

class LegacySettingsSync
implements PreferenceChangeListener {
    private static final Logger PREF_LOG = Logger.getLogger(FoldHierarchy.class.getName() + ".enabled");
    private static LegacySettingsSync INSTANCE;
    private Reference<Preferences> defaultMimePrefs;

    LegacySettingsSync() {
    }

    static synchronized LegacySettingsSync get() {
        if (INSTANCE == null) {
            INSTANCE = new LegacySettingsSync();
        }
        return INSTANCE;
    }

    synchronized Preferences processMime(String mime) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)mime).lookup(Preferences.class);
        if ("".equals(mime)) {
            Preferences p;
            Preferences preferences = p = this.defaultMimePrefs == null ? null : this.defaultMimePrefs.get();
            if (p == prefs) {
                return prefs;
            }
            if (p != null) {
                p.removePreferenceChangeListener(this);
            }
            this.syncKey(FoldType.MEMBER.code(), prefs);
            this.syncKey(FoldType.NESTED.code(), prefs);
            this.syncKey(FoldType.DOCUMENTATION.code(), prefs);
            this.defaultMimePrefs = new WeakReference<Preferences>(prefs);
            prefs.addPreferenceChangeListener(this);
        } else {
            if (!(prefs instanceof OverridePreferences)) {
                return prefs;
            }
            if (((OverridePreferences)prefs).isOverriden("code-folding-use-defaults")) {
                return prefs;
            }
            this.processMime("");
            boolean state = prefs.getBoolean("code-folding-use-defaults", false);
            if (!state) {
                this.cleanupPreferences(mime, prefs);
            } else {
                this.clonePreferences(mime, prefs);
            }
        }
        return prefs;
    }

    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        if (evt.getKey() == null || !evt.getKey().startsWith("code-folding-collapse-")) {
            return;
        }
        String k = evt.getKey().substring("code-folding-collapse-".length());
        this.syncKey(k, evt.getNode());
    }

    private void syncKey(String k, Preferences pref) {
        String l;
        if (FoldType.MEMBER.code().equals(k)) {
            l = "method";
        } else if (FoldType.NESTED.code().equals(k)) {
            l = "innerclass";
        } else if (FoldType.DOCUMENTATION.code().equals(k)) {
            l = "javadoc";
        } else {
            return;
        }
        String syncKey = "code-folding-collapse-" + l;
        pref.putBoolean(syncKey, pref.getBoolean("code-folding-collapse-" + k, false));
    }

    private void clonePreferences(String mime, Preferences pref) {
        Collection<FoldType> types = FoldUtilities.getFoldTypes(mime).values();
        for (FoldType ft : types) {
            String key = "code-folding-collapse-" + ft.code();
            if (this.isDefinedLocally(pref, key)) continue;
            boolean val = pref.getBoolean(key, ft.parent() == null ? false : pref.getBoolean("code-folding-collapse-" + ft.parent().code(), false));
            pref.putBoolean(key, val);
        }
    }

    private boolean isDefinedLocally(Preferences pref, String key) {
        return pref instanceof OverridePreferences && ((OverridePreferences)pref).isOverriden(key);
    }

    private void cleanupPreferences(String mime, Preferences pref) {
        Collection<FoldType> types = FoldUtilities.getFoldTypes(mime).values();
        String parent = MimePath.parse((String)mime).getInheritedType();
        if (parent == null) {
            return;
        }
        if (types.isEmpty()) {
            return;
        }
        Preferences pprefs = (Preferences)MimeLookup.getLookup((String)parent).lookup(Preferences.class);
        for (FoldType ft : types) {
            String key = "code-folding-collapse-" + ft.code();
            if (!this.isDefinedLocally(pref, key) || pprefs.get(key, null) == null) continue;
            pref.remove(key);
        }
    }
}

