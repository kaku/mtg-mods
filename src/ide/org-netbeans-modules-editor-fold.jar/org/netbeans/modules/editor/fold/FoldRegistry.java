/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.spi.editor.fold.FoldTypeProvider;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public final class FoldRegistry {
    private static final Logger LOG = Logger.getLogger(FoldRegistry.class.getName());
    private Class enumType;
    private final Map<MimePath, R> enums = new HashMap<MimePath, R>();
    private static volatile Reference<FoldRegistry> INSTANCE = new WeakReference<Object>(null);

    private FoldRegistry(Class enumType) {
        this.enumType = enumType;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public static FoldRegistry get() {
        FoldRegistry fr = INSTANCE.get();
        if (fr != null) return fr;
        Class<FoldRegistry> class_ = FoldRegistry.class;
        synchronized (FoldRegistry.class) {
            fr = INSTANCE.get();
            if (fr != null) return fr;
            {
                fr = new FoldRegistry(FoldType.class);
                INSTANCE = new SoftReference<FoldRegistry>(fr);
            }
            // ** MonitorExit[var1_1] (shouldn't be in output)
            return fr;
        }
    }

    public Collection<FoldType> values(MimePath mime) {
        return this.get(mime).enums;
    }

    public FoldType valueOf(MimePath mime, String val) {
        return this.get(mime).valueOf(val);
    }

    public FoldType.Domain getDomain(MimePath mime) {
        return this.get(mime);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private R get(MimePath mime) {
        Map<MimePath, R> map = this.enums;
        synchronized (map) {
            R r = this.enums.get((Object)mime);
            if (r != null) {
                return r;
            }
        }
        return this.refreshMime(mime, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private R refreshMime(MimePath mime, R holder) {
        Lookup.Result r;
        boolean register;
        Lookup.Result pr = null;
        if (holder == null) {
            r = MimeLookup.getLookup((MimePath)mime).lookup(new Lookup.Template(FoldTypeProvider.class));
            String parentMime = mime.getInheritedType();
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("Get providers for " + (Object)mime + ", parent " + parentMime);
            }
            if (parentMime != null) {
                pr = MimeLookup.getLookup((String)parentMime).lookup(new Lookup.Template(FoldTypeProvider.class));
            }
        } else {
            r = holder.result;
            pr = holder.result2;
        }
        Collection providers = r.allInstances();
        Collection parentProvs = pr == null ? Collections.emptySet() : pr.allInstances();
        Iterator it = parentProvs.iterator();
        while (it.hasNext()) {
            FoldTypeProvider p = (FoldTypeProvider)it.next();
            if (!p.inheritable()) continue;
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.FINER, "Inheritable: " + p);
            }
            it.remove();
        }
        LinkedHashSet<FoldType> allValues = new LinkedHashSet<FoldType>();
        for (Object p2 : providers) {
            if (parentProvs.contains(p2)) {
                if (!LOG.isLoggable(Level.FINER)) continue;
                LOG.log(Level.FINER, "Removing not inheritable: " + p2);
                continue;
            }
            Collection vals = p2.getValues(this.enumType);
            if (vals == null) continue;
            allValues.addAll(vals);
        }
        boolean bl = register = holder == null;
        if (holder == null) {
            holder = new R(this, mime, r, pr);
        }
        holder.reset(allValues);
        if (register) {
            Object p2;
            p2 = this.enums;
            synchronized (p2) {
                R oldHolder = this.enums.put(mime, holder);
                if (oldHolder != null) {
                    this.enums.put(mime, oldHolder);
                    holder = oldHolder;
                }
            }
        }
        return holder;
    }

    private static class R
    implements LookupListener,
    FoldType.Domain {
        final FoldRegistry dom;
        final MimePath mime;
        final Lookup.Result result;
        final Lookup.Result result2;
        Collection<ChangeListener> listeners;
        private Set<FoldType> enums;
        volatile Map<String, FoldType> valueMap;

        public R(FoldRegistry dom, MimePath mime, Lookup.Result result, Lookup.Result result2) {
            this.dom = dom;
            this.mime = mime;
            this.result = result;
            this.result2 = result2;
            result.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), (Object)result));
            if (result2 != null) {
                result2.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), (Object)result2));
            }
        }

        public void resultChanged(LookupEvent ev) {
            this.dom.refreshMime(this.mime, this);
        }

        @Override
        public Collection<FoldType> values() {
            return Collections.unmodifiableCollection(this.map().values());
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Map<String, FoldType> map() {
            Map<String, FoldType> m = this.valueMap;
            if (m == null) {
                R r = this;
                synchronized (r) {
                    if (this.valueMap != null) {
                        return this.valueMap;
                    }
                    Set<FoldType> vals = this.enums;
                    m = new LinkedHashMap<String, FoldType>();
                    for (FoldType e : vals) {
                        FoldType old = m.put(e.code(), e);
                        if (old == null) continue;
                        throw new IllegalArgumentException("Two fold types share the same code: " + old + " and " + e);
                    }
                    this.valueMap = m;
                }
            }
            return m;
        }

        @Override
        public FoldType valueOf(String val) {
            return this.map().get(val);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void addChangeListener(ChangeListener l) {
            R r = this;
            synchronized (r) {
                if (this.listeners == null) {
                    this.listeners = new ArrayList<ChangeListener>();
                }
                this.listeners.add(l);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void removeChangeListener(ChangeListener l) {
            R r = this;
            synchronized (r) {
                if (this.listeners != null) {
                    this.listeners.remove(l);
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        void reset(Set<FoldType> allValues) {
            ChangeListener[] ll;
            R r = this;
            synchronized (r) {
                this.enums = allValues;
                this.valueMap = null;
                if (this.listeners == null) {
                    return;
                }
                ll = this.listeners.toArray(new ChangeListener[this.listeners.size()]);
            }
            ChangeEvent e = new ChangeEvent(this);
            for (int i = 0; i < ll.length; ++i) {
                ll[i].stateChanged(e);
            }
        }
    }

}

