/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold;

import org.netbeans.modules.editor.fold.FoldHierarchyTransactionImpl;
import org.netbeans.modules.editor.fold.FoldOperationImpl;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldOperation;

public abstract class SpiPackageAccessor {
    private static SpiPackageAccessor INSTANCE;

    public static SpiPackageAccessor get() {
        return INSTANCE;
    }

    public static void register(SpiPackageAccessor accessor) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already registered");
        }
        INSTANCE = accessor;
    }

    public abstract FoldHierarchyTransaction createFoldHierarchyTransaction(FoldHierarchyTransactionImpl var1);

    public abstract FoldHierarchyTransactionImpl getImpl(FoldHierarchyTransaction var1);

    public abstract FoldOperation createFoldOperation(FoldOperationImpl var1);
}

