/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.plaf.TextUI;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.modules.editor.fold.FoldManagerFactoryProvider;
import org.netbeans.spi.editor.fold.FoldManagerFactory;

public class CustomProvider
extends FoldManagerFactoryProvider {
    private static final String FOLDER_NAME = "FoldManager";
    private final Map mime2factoryList = new HashMap();

    CustomProvider() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void registerFactory(String mimeType, FoldManagerFactory factory) {
        assert (mimeType != null && factory != null);
        Map map = this.mime2factoryList;
        synchronized (map) {
            ArrayList<FoldManagerFactory> factoryList = (ArrayList<FoldManagerFactory>)this.mime2factoryList.get(mimeType);
            if (factoryList == null) {
                factoryList = new ArrayList<FoldManagerFactory>();
                this.mime2factoryList.put(mimeType, factoryList);
            }
            factoryList.add(factory);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void registerFactories(String mimeType, FoldManagerFactory[] factories) {
        Map map = this.mime2factoryList;
        synchronized (map) {
            for (int i = 0; i < factories.length; ++i) {
                this.registerFactory(mimeType, factories[i]);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeAllFactories(String mimeType) {
        Map map = this.mime2factoryList;
        synchronized (map) {
            this.mime2factoryList.put(mimeType, null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeAllFactories() {
        Map map = this.mime2factoryList;
        synchronized (map) {
            this.mime2factoryList.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List getFactoryList(FoldHierarchy hierarchy) {
        String mimeType;
        List factoryList = null;
        JTextComponent editorComponent = hierarchy.getComponent();
        EditorKit kit = editorComponent.getUI().getEditorKit(editorComponent);
        if (kit != null && (mimeType = kit.getContentType()) != null) {
            Map map = this.mime2factoryList;
            synchronized (map) {
                factoryList = (List)this.mime2factoryList.get(mimeType);
            }
        }
        if (factoryList == null) {
            return Collections.EMPTY_LIST;
        }
        return factoryList;
    }
}

