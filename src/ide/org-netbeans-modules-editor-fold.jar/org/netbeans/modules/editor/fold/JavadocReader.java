/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 */
package org.netbeans.modules.editor.fold;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.fold.ContentReader;

public final class JavadocReader
implements ContentReader {
    private final String lineStartMarker;
    private final Pattern stopPattern;
    private final Pattern termPattern;
    private final String prefix;

    public JavadocReader(String lineStartMarker, String terminator, String stopPattern, String prefix) {
        this.lineStartMarker = lineStartMarker;
        this.termPattern = terminator != null ? Pattern.compile(terminator) : null;
        this.stopPattern = stopPattern != null ? Pattern.compile(stopPattern, 2) : null;
        this.prefix = prefix == null ? " " : prefix;
    }

    private int nonWhiteBwd(CharSequence seq, int pos, int limit) {
        while (pos > limit) {
            char c = seq.charAt(pos);
            if (!Character.isWhitespace(c)) {
                return pos;
            }
            --pos;
        }
        return limit;
    }

    private int nonWhiteFwdOnRow(CharSequence seq, int pos) {
        int l = seq.length();
        while (pos < l) {
            char c = seq.charAt(pos);
            if (!Character.isWhitespace(c)) {
                return pos;
            }
            if (c == '\n') {
                return -1;
            }
            ++pos;
        }
        return -1;
    }

    @Override
    public CharSequence read(Document d, Fold f, FoldTemplate ft) throws BadLocationException {
        int contentStart = f.getGuardedStart();
        int contentEnd = f.getGuardedEnd();
        CharSequence seq = DocumentUtilities.getText((Document)d);
        int rowStart = contentStart;
        while (rowStart < contentEnd) {
            Element e = DocumentUtilities.getParagraphElement((Document)d, (int)rowStart);
            int nextRow = Math.min(contentEnd, e.getEndOffset());
            int nonWhite = this.nonWhiteFwdOnRow(seq, rowStart);
            if (nonWhite != -1 && this.lineStartMarker != null && CharSequenceUtilities.textEquals((CharSequence)DocumentUtilities.getText((Document)d, (int)nonWhite, (int)this.lineStartMarker.length()), (CharSequence)this.lineStartMarker)) {
                nonWhite = this.nonWhiteFwdOnRow(seq, nonWhite + this.lineStartMarker.length());
            }
            if (nonWhite != -1) {
                Matcher m;
                int endIndex = this.nonWhiteBwd(seq, nextRow, nonWhite);
                if (endIndex < nextRow) {
                    ++endIndex;
                }
                CharSequence ret = DocumentUtilities.getText((Document)d, (int)nonWhite, (int)(endIndex - nonWhite));
                if (this.stopPattern != null && this.stopPattern.matcher(ret).lookingAt()) {
                    return null;
                }
                int idx = -1;
                if (this.termPattern != null && (m = this.termPattern.matcher(ret)).find()) {
                    idx = m.start();
                }
                if (idx > -1) {
                    return this.prefix + DocumentUtilities.getText((Document)d, (int)nonWhite, (int)idx);
                }
                return this.prefix + ret;
            }
            rowStart = nextRow;
        }
        return null;
    }
}

