/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.editor.fold;

import java.util.Collection;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.fold.FoldHierarchyExecution;
import org.netbeans.modules.editor.fold.FoldOperationImpl;
import org.netbeans.spi.editor.fold.FoldHierarchyMonitor;

public abstract class ApiPackageAccessor {
    private static ApiPackageAccessor INSTANCE;

    public static ApiPackageAccessor get() {
        return INSTANCE;
    }

    public static void register(ApiPackageAccessor accessor) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already registered");
        }
        INSTANCE = accessor;
        MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FoldHierarchyMonitor.class);
    }

    public abstract FoldHierarchy createFoldHierarchy(FoldHierarchyExecution var1);

    public abstract Fold createFold(FoldOperationImpl var1, FoldType var2, String var3, boolean var4, Document var5, int var6, int var7, int var8, int var9, Object var10) throws BadLocationException;

    public abstract FoldHierarchyEvent createFoldHierarchyEvent(FoldHierarchy var1, Fold[] var2, Fold[] var3, FoldStateChange[] var4, int var5, int var6);

    public abstract FoldStateChange createFoldStateChange(Fold var1);

    public abstract void foldSetCollapsed(Fold var1, boolean var2);

    public abstract void foldSetParent(Fold var1, Fold var2);

    public abstract void foldTearOut(Fold var1, Collection var2);

    public abstract void foldExtractToChildren(Fold var1, int var2, int var3, Fold var4);

    public abstract Fold foldReplaceByChildren(Fold var1, int var2);

    public abstract void foldSetDescription(Fold var1, String var2);

    public abstract void foldSetStartOffset(Fold var1, Document var2, int var3) throws BadLocationException;

    public abstract void foldSetEndOffset(Fold var1, Document var2, int var3) throws BadLocationException;

    public abstract boolean foldIsStartDamaged(Fold var1);

    public abstract boolean foldIsEndDamaged(Fold var1);

    public abstract void foldInsertUpdate(Fold var1, DocumentEvent var2);

    public abstract void foldRemoveUpdate(Fold var1, DocumentEvent var2);

    public abstract FoldOperationImpl foldGetOperation(Fold var1);

    public abstract int foldGetRawIndex(Fold var1);

    public abstract void foldSetRawIndex(Fold var1, int var2);

    public abstract void foldUpdateRawIndex(Fold var1, int var2);

    public abstract Object foldGetExtraInfo(Fold var1);

    public abstract void foldSetExtraInfo(Fold var1, Object var2);

    public abstract void foldStateChangeCollapsedChanged(FoldStateChange var1);

    public abstract void foldStateChangeDescriptionChanged(FoldStateChange var1);

    public abstract void foldStateChangeStartOffsetChanged(FoldStateChange var1, int var2);

    public abstract void foldStateChangeEndOffsetChanged(FoldStateChange var1, int var2);

    public abstract FoldHierarchyExecution foldGetExecution(FoldHierarchy var1);

    public abstract void foldMarkDamaged(Fold var1, int var2);

    public abstract int foldStartGuardedLength(Fold var1);

    public abstract int foldEndGuardedLength(Fold var1);
}

