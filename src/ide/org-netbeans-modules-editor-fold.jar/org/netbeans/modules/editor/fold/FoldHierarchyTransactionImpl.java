/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.ErrorManager
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.fold;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;
import org.netbeans.modules.editor.fold.FoldHierarchyExecution;
import org.netbeans.modules.editor.fold.FoldOperationImpl;
import org.netbeans.modules.editor.fold.FoldUtilitiesImpl;
import org.netbeans.modules.editor.fold.SpiPackageAccessor;
import org.netbeans.spi.editor.fold.FoldHierarchyTransaction;
import org.netbeans.spi.editor.fold.FoldManager;
import org.openide.ErrorManager;
import org.openide.util.Exceptions;

public final class FoldHierarchyTransactionImpl {
    private static final Logger LOG = Logger.getLogger(FoldHierarchy.class.getName());
    private static final Fold[] EMPTY_FOLDS = new Fold[0];
    private static final FoldStateChange[] EMPTY_FOLD_STATE_CHANGES = new FoldStateChange[0];
    private static final int[] EMPTY_INT_ARRAY = new int[0];
    private static final int MAX_NESTING_LEVEL = 1000;
    private FoldHierarchyTransaction transaction;
    private boolean committed;
    private FoldHierarchyExecution execution;
    private Fold lastOperationFold;
    private int lastOperationIndex;
    private Fold addFoldBlock;
    private List unblockedFoldLists = new ArrayList(4);
    private int unblockedFoldMaxPriority = -1;
    private Set addedToHierarchySet;
    private Set removedFromHierarchySet;
    private Map fold2StateChange;
    private int affectedStartOffset;
    private int affectedEndOffset;
    private Set<Fold> reinsertSet;
    private String initialSnapshot;
    private final int dmgCounter;
    private boolean removeAll;

    public FoldHierarchyTransactionImpl(FoldHierarchyExecution execution) {
        this.execution = execution;
        this.affectedStartOffset = -1;
        this.affectedEndOffset = -1;
        this.transaction = SpiPackageAccessor.get().createFoldHierarchyTransaction(this);
        this.dmgCounter = execution.getDamagedCount();
        if (this.dmgCounter > 0) {
            String t = null;
            JTextComponent comp = execution.getComponent();
            if (comp != null) {
                Document doc = comp.getDocument();
                try {
                    t = doc.getText(0, doc.getLength());
                }
                catch (BadLocationException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
            }
            this.initialSnapshot = execution.toString() + "\nContent at previous commit:\n====\n" + execution.getCommittedContent() + "\n====\n" + "\nText content:\n====\n" + t + "\n====\n";
        }
    }

    public FoldHierarchyTransaction getTransaction() {
        return this.transaction;
    }

    void cancelled() {
        this.checkNotCommitted();
        LOG.log(Level.WARNING, "Fold transaction not committed at unlock");
        this.committed = true;
        this.execution.clearActiveTransaction();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void commit() {
        block19 : {
            this.checkNotCommitted();
            try {
                if (!this.isEmpty()) {
                    int size;
                    FoldStateChange[] stateChanges;
                    Fold[] addedFolds;
                    Fold[] removedFolds;
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("FoldHierarchy BEFORE transaction commit:\n" + this.execution);
                        this.execution.checkConsistency();
                    }
                    if (this.removedFromHierarchySet != null && (size = this.removedFromHierarchySet.size()) != 0) {
                        removedFolds = new Fold[size];
                        this.removedFromHierarchySet.toArray(removedFolds);
                    } else {
                        removedFolds = EMPTY_FOLDS;
                    }
                    if (this.addedToHierarchySet != null && (size = this.addedToHierarchySet.size()) != 0) {
                        addedFolds = new Fold[size];
                        this.addedToHierarchySet.toArray(addedFolds);
                    } else {
                        addedFolds = EMPTY_FOLDS;
                    }
                    if (this.reinsertSet != null) {
                        ApiPackageAccessor acc = ApiPackageAccessor.get();
                        for (Fold f : this.reinsertSet) {
                            acc.foldSetParent(f, null);
                            if (f.getFoldCount() <= 0) {
                                this.addFold(f);
                                continue;
                            }
                            LOG.warning("Unexpected children for fold: " + f + ", dumping hierarchy: " + this.execution);
                        }
                    }
                    if (this.fold2StateChange != null) {
                        stateChanges = new FoldStateChange[this.fold2StateChange.size()];
                        this.fold2StateChange.values().toArray(stateChanges);
                    } else {
                        stateChanges = EMPTY_FOLD_STATE_CHANGES;
                    }
                    for (int i = stateChanges.length - 1; i >= 0; --i) {
                        FoldStateChange change = stateChanges[i];
                        Fold fold = change.getFold();
                        this.updateAffectedOffsets(fold);
                        int startOffset = change.getOriginalStartOffset();
                        int endOffset = change.getOriginalEndOffset();
                        assert (endOffset < 0 || startOffset <= endOffset);
                        if (startOffset != -1) {
                            this.updateAffectedStartOffset(startOffset);
                        }
                        if (endOffset == -1) continue;
                        this.updateAffectedEndOffset(endOffset);
                    }
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("FoldHierarchy AFTER transaction commit:\n" + this.execution);
                        this.execution.checkConsistency();
                    }
                    this.committed = true;
                    this.execution.clearActiveTransaction();
                    int so = Math.max(0, this.affectedStartOffset);
                    this.execution.createAndFireFoldHierarchyEvent(removedFolds, addedFolds, stateChanges, so, Math.max(this.affectedEndOffset, so));
                    break block19;
                }
                this.committed = true;
                this.execution.clearActiveTransaction();
            }
            finally {
                this.execution.transactionCommitted();
                if (this.execution.getDamagedCount() > this.dmgCounter && this.dmgCounter > 0) {
                    LOG.warning("Fold Hierarchy damaged. Dumping initial state:\n----------------------");
                    LOG.warning(this.initialSnapshot);
                    LOG.warning("\n----------------------\nCurrent state:");
                    LOG.warning(this.execution.toString());
                }
            }
        }
    }

    void validateAffectedFolds(Fold fold, DocumentEvent evt) {
        ArrayList pp = new ArrayList();
        this.validateAffectedFolds(fold, evt, pp);
        if (!pp.isEmpty()) {
            ApiPackageAccessor api = ApiPackageAccessor.get();
            for (Fold childFold : pp) {
                this.removeFold(childFold);
                this.removeEmptyNotify(childFold);
            }
        }
    }

    void validateAffectedFolds(Fold fold, DocumentEvent evt, Collection damaged) {
        boolean removed;
        Fold prevChildFold;
        int startOffset = evt.getOffset();
        int endOffset = startOffset + evt.getLength();
        int childIndex = FoldUtilitiesImpl.findFoldStartIndex(fold, startOffset, true);
        if (childIndex == -1) {
            if (fold.getFoldCount() == 0) {
                return;
            }
            Fold first = fold.getFold(0);
            if (first.getStartOffset() <= endOffset) {
                childIndex = 0;
            } else {
                return;
            }
        }
        if (childIndex >= 1 && (prevChildFold = fold.getFold(childIndex - 1)).getEndOffset() == startOffset) {
            this.validateAffectedFolds(prevChildFold, evt, damaged);
        }
        int pStart = fold.getStartOffset();
        int pEnd = fold.getEndOffset();
        boolean startsWithin = false;
        do {
            Fold childFold = fold.getFold(childIndex);
            int cStart = childFold.getStartOffset();
            int cEnd = childFold.getEndOffset();
            startsWithin = cStart < startOffset && cEnd <= endOffset;
            removed = false;
            if (cStart < pStart || cEnd > pEnd || cStart == cEnd) {
                damaged.add(childFold);
            }
            if (childFold.getFoldCount() <= 0) continue;
            this.validateAffectedFolds(childFold, evt, damaged);
        } while ((startsWithin || removed) && ++childIndex < fold.getFoldCount());
    }

    public void insertUpdate(DocumentEvent evt) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("insertUpdate: offset=" + evt.getOffset() + ", length=" + evt.getLength() + '\n');
        }
        try {
            if (FoldHierarchyExecution.isEventInUndoRedoHack(evt)) {
                this.validateAffectedFolds(this.execution.getRootFold(), evt);
            }
            this.insertCheckEndOffset(this.execution.getRootFold(), evt);
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
        }
    }

    private void insertCheckEndOffset(Fold fold, DocumentEvent evt) throws BadLocationException {
        int insertOffset = evt.getOffset();
        int insertEndOffset = insertOffset + evt.getLength();
        int childIndex = FoldUtilitiesImpl.findFoldStartIndex(fold, insertEndOffset, false);
        if (childIndex >= 0) {
            int childFoldStartOffset;
            Fold childFold = fold.getFold(childIndex);
            if (childIndex > 0 && childFold.getStartOffset() == insertEndOffset) {
                childFold = fold.getFold(--childIndex);
            }
            if ((childFoldStartOffset = childFold.getStartOffset()) >= insertOffset) {
                return;
            }
            int childFoldEndOffset = childFold.getEndOffset();
            if (childFoldEndOffset >= insertEndOffset) {
                this.insertCheckEndOffset(childFold, evt);
                ApiPackageAccessor api = ApiPackageAccessor.get();
                api.foldInsertUpdate(childFold, evt);
                if (childFoldEndOffset < childFoldStartOffset) {
                    LOG.warning("Child start offset > end offset, dumping fold hierarchy: " + this.execution);
                    LOG.warning("Document event was: " + evt + " offset: " + insertOffset + ", len: " + evt.getLength());
                    this.execution.markDamaged();
                    this.execution.remove(childFold, this);
                    api.foldMarkDamaged(childFold, 6);
                    this.removeDamagedNotify(childFold);
                    return;
                }
                if (childFoldEndOffset == insertEndOffset) {
                    FoldStateChange state;
                    Document doc = evt.getDocument();
                    if (childFoldStartOffset == childFoldEndOffset) {
                        api.foldSetStartOffset(childFold, doc, insertOffset);
                        state = this.getFoldStateChange(childFold);
                        if (state.getOriginalEndOffset() >= 0 && state.getOriginalEndOffset() < childFoldStartOffset) {
                            this.execution.markDamaged();
                            LOG.warning("Original start offset > end offset, dumping fold hierarchy: " + this.execution);
                            LOG.warning("Document event was: " + evt + " offset: " + insertOffset + ", len: " + evt.getLength());
                        }
                        api.foldStateChangeStartOffsetChanged(state, childFoldStartOffset);
                    }
                    api.foldSetEndOffset(childFold, doc, insertOffset);
                    state = this.getFoldStateChange(childFold);
                    if (state.getOriginalEndOffset() >= 0 && state.getOriginalStartOffset() > childFoldEndOffset) {
                        this.execution.markDamaged();
                        LOG.warning("Original start offset > end offset, dumping fold hierarchy: " + this.execution);
                        LOG.warning("Document event was: " + evt + " offset: " + insertOffset + ", len: " + evt.getLength());
                    }
                    api.foldStateChangeEndOffsetChanged(state, childFoldEndOffset);
                    if (childFold.getStartOffset() > childFold.getEndOffset()) {
                        this.execution.markDamaged();
                        LOG.warning("Updated fold " + childFold + " is inconsistent, dumping fold hierarchy: " + this.execution);
                        LOG.warning("The original offsets were: " + childFoldStartOffset + "-" + childFoldEndOffset);
                        LOG.warning("Document event was: " + evt + " offset: " + insertOffset + ", len: " + evt.getLength());
                    }
                } else {
                    int dmg = FoldUtilitiesImpl.isFoldDamagedByInsert(childFold, evt);
                    if (dmg > 0) {
                        this.execution.remove(childFold, this);
                        api.foldMarkDamaged(childFold, dmg);
                        this.removeDamagedNotify(childFold);
                        if (LOG.isLoggable(Level.FINE)) {
                            LOG.fine("insertUpdate: removed damaged " + childFold + '\n');
                        }
                    }
                }
            }
        }
    }

    private FoldOperationImpl getOperation(Fold fold) {
        return ApiPackageAccessor.get().foldGetOperation(fold);
    }

    private FoldManager getManager(Fold fold) {
        return this.getOperation(fold).getManager();
    }

    public void setCollapsed(Fold fold, boolean collapsed) {
        boolean oldCollapsed = fold.isCollapsed();
        if (oldCollapsed != collapsed) {
            ApiPackageAccessor api = ApiPackageAccessor.get();
            api.foldSetCollapsed(fold, collapsed);
            api.foldStateChangeCollapsedChanged(this.getFoldStateChange(fold));
        }
    }

    private void removeDamagedNotify(Fold fold) {
        this.getManager(fold).removeDamagedNotify(fold);
    }

    private void removeEmptyNotify(Fold fold) {
        this.getManager(fold).removeEmptyNotify(fold);
    }

    public void removeUpdate(DocumentEvent evt) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("removeUpdate: offset=" + evt.getOffset() + ", len=" + evt.getLength() + '\n');
        }
        if (FoldHierarchyExecution.isEventInUndoRedoHack(evt)) {
            this.validateAffectedFolds(this.execution.getRootFold(), evt);
        }
        this.removeCheckDamaged2(evt);
    }

    private void removeCheckDamaged2(DocumentEvent evt) {
        Collection pp = this.execution.getRemovePostProcess(evt);
        if (pp.isEmpty()) {
            return;
        }
        ApiPackageAccessor api = ApiPackageAccessor.get();
        Iterator it = pp.iterator();
        block6 : while (it.hasNext()) {
            int cmd = (Integer)it.next();
            int damagedFlags = cmd & 6;
            Fold childFold = (Fold)it.next();
            switch (cmd &= 24) {
                case 0: {
                    this.removeFold(childFold);
                    this.getManager(childFold).removeEmptyNotify(childFold);
                    api.foldMarkDamaged(childFold, damagedFlags);
                    continue block6;
                }
                case 8: {
                    api.foldMarkDamaged(childFold, damagedFlags);
                    this.removeFold(childFold);
                    this.getManager(childFold).removeDamagedNotify(childFold);
                    continue block6;
                }
                case 16: {
                    this.setCollapsed(childFold, false);
                }
                case 24: {
                    api.foldRemoveUpdate(childFold, evt);
                    continue block6;
                }
            }
            throw new IllegalStateException();
        }
    }

    private boolean isEmpty() {
        return !(this.fold2StateChange != null && !this.fold2StateChange.isEmpty() || this.addedToHierarchySet != null && !this.addedToHierarchySet.isEmpty() || this.removedFromHierarchySet != null && !this.removedFromHierarchySet.isEmpty() || this.reinsertSet != null && !this.reinsertSet.isEmpty());
    }

    public FoldStateChange getFoldStateChange(Fold fold) {
        FoldStateChange change;
        if (this.fold2StateChange == null) {
            this.fold2StateChange = new HashMap();
        }
        if ((change = (FoldStateChange)this.fold2StateChange.get(fold)) == null) {
            change = ApiPackageAccessor.get().createFoldStateChange(fold);
            this.fold2StateChange.put(fold, change);
        }
        return change;
    }

    void removeFold(Fold fold) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("removeFold: " + fold + '\n');
        }
        this.checkNotCommitted();
        Fold parent = fold.getParent();
        if (parent != null) {
            int index = parent.getFoldIndex(fold);
            this.removeFoldFromHierarchy(parent, index, null);
            this.lastOperationFold = parent;
            this.lastOperationIndex = index;
        } else {
            if (!this.execution.isBlocked(fold)) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("Fold already removed: " + fold + '\n');
                }
                return;
            }
            this.execution.unmarkBlocked(fold);
            this.unblockBlocked(fold);
        }
        this.processUnblocked();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeAllFolds(Fold[] allBlocked) {
        this.removeAll = true;
        try {
            for (int i = allBlocked.length - 1; i >= 0; --i) {
                this.removeFold(allBlocked[i]);
            }
            this.removeAllChildrenAndSelf(this.execution.getRootFold());
        }
        finally {
            this.removeAll = false;
        }
    }

    private void removeAllChildrenAndSelf(Fold fold) {
        int foldCount = fold.getFoldCount();
        if (foldCount > 0) {
            for (int i = foldCount - 1; i >= 0; --i) {
                this.removeAllChildrenAndSelf(fold.getFold(i));
            }
        }
        if (!FoldUtilities.isRootFold(fold)) {
            this.removeFold(fold);
        }
    }

    public void changedUpdate(DocumentEvent evt) {
    }

    boolean addFold(Fold fold) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("addFold: " + fold + '\n');
        }
        if (this.getOperation(fold).isReleased()) {
            throw new IllegalStateException("The manager has been already released");
        }
        this.checkNotCommitted();
        return this.addFold(fold, null, 0);
    }

    private int findFoldInsertIndex(Fold parentFold, int startOffset, int endOffset) {
        int index = FoldUtilitiesImpl.findFoldInsertIndex(parentFold, startOffset);
        if (index < 1) {
            return index;
        }
        Fold pF = parentFold.getFold(index - 1);
        if (pF.getStartOffset() == startOffset && pF.getEndOffset() < endOffset) {
            return index - 1;
        }
        return index;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private boolean addFold(Fold fold, Fold parentFold, int level) {
        foldStartOffset = fold.getStartOffset();
        foldEndOffset = fold.getEndOffset();
        foldPriority = this.getOperation(fold).getPriority();
        sbDebug = new StringBuilder();
        ea = false;
        if (!FoldHierarchyTransactionImpl.$assertionsDisabled) {
            ea = true;
            if (!true) {
                throw new AssertionError();
            }
            if (ea) {
                sbDebug.append("\n addFold1 ENTER");
            }
        }
        if (parentFold == null) {
            parentFold = this.lastOperationFold;
            if (parentFold == null || foldStartOffset < parentFold.getStartOffset() || foldEndOffset > parentFold.getEndOffset()) {
                parentFold = this.execution.getRootFold();
                index = this.findFoldInsertIndex(parentFold, foldStartOffset, foldEndOffset);
                useLast = false;
            } else {
                index = this.lastOperationIndex;
                useLast = true;
            }
        } else {
            index = this.findFoldInsertIndex(parentFold, foldStartOffset, foldEndOffset);
            useLast = false;
        }
        if (ea) {
            sbDebug.append(", rootFold = " + this.execution.getRootFold()).append(", useLast = " + useLast + ", parentFold = " + parentFold + ", index = " + index);
        }
        foldCount = parentFold.getFoldCount();
        if (useLast && index > foldCount) {
            index = this.findFoldInsertIndex(parentFold, foldStartOffset, foldEndOffset);
            useLast = false;
        }
        if (index > 0) {
            prevFold = parentFold.getFold(index - 1);
            if (useLast && foldStartOffset < prevFold.getEndOffset()) {
                index = this.findFoldInsertIndex(parentFold, foldStartOffset, foldEndOffset);
                useLast = false;
                v0 = prevFold = index > 0 ? parentFold.getFold(index - 1) : null;
                if (ea) {
                    sbDebug.append("\n reset prev fold, new index = " + index);
                }
            }
        } else {
            prevFold = null;
        }
        if (index < foldCount) {
            nextFold = parentFold.getFold(index);
            if (useLast && foldStartOffset >= nextFold.getStartOffset()) {
                index = this.findFoldInsertIndex(parentFold, foldStartOffset, foldEndOffset);
                useLast = false;
                prevFold = index > 0 ? parentFold.getFold(index - 1) : null;
                v1 = nextFold = index < foldCount ? parentFold.getFold(index) : null;
                if (ea) {
                    sbDebug.append("\n reset next fold, new index = " + index);
                }
            }
        } else {
            nextFold = null;
        }
        if (ea) {
            sbDebug.append("\nprevFold = " + prevFold + ", nextFold = " + nextFold);
        }
        if (prevFold == null || foldStartOffset >= prevFold.getEndOffset()) ** GOTO lbl112
        if (foldEndOffset > prevFold.getEndOffset()) ** GOTO lbl100
        if (level >= 1000) ** GOTO lbl72
        if (this.getManager(fold) != this.getManager(prevFold)) return this.addFold(fold, prevFold, level + 1);
        if (fold.getStartOffset() != prevFold.getStartOffset()) return this.addFold(fold, prevFold, level + 1);
        if (fold.getEndOffset() != prevFold.getEndOffset()) return this.addFold(fold, prevFold, level + 1);
        if (FoldHierarchyTransactionImpl.LOG.isLoggable(Level.WARNING)) {
            sb = new StringBuilder();
            sb.append("Adding a fold that is identical with another previously added fold from the same FoldManager is not allowed.\n");
            sb.append("Existing fold: ");
            sb.append(prevFold.toString());
            sb.append("; FoldManager: ").append(this.getManager(prevFold));
            sb.append("\n");
            sb.append("     New fold: ");
            sb.append(fold.toString());
            sb.append("; FoldManager: ").append(this.getManager(fold));
            sb.append("\n");
            FoldHierarchyTransactionImpl.LOG.warning(sb.toString());
        }
        ** GOTO lbl96
lbl72: // 1 sources:
        if (!FoldHierarchyTransactionImpl.LOG.isLoggable(Level.WARNING)) ** GOTO lbl96
        sb = new StringBuilder();
        doc = this.getOperation(prevFold).getDocument();
        sb.append("Too many nested folds in ");
        sb.append(doc.getClass().getName());
        sb.append("@");
        sb.append(Integer.toHexString(System.identityHashCode(doc)));
        sb.append("['").append((String)doc.getProperty("mimeType")).append("', ");
        sb.append(this.findFilePath(doc)).append("]\n");
        sb.append("Dumping the nesting folds:\n");
        for (f = prevFold; f != null; f = f.getParent()) {
            sb.append(f.toString());
            sb.append("; FoldManager: ").append(this.getManager(f));
            sb.append("\n");
        }
        assertionsOn = false;
        if (FoldHierarchyTransactionImpl.$assertionsDisabled) ** GOTO lbl-1000
        assertionsOn = true;
        if (!true) {
            throw new AssertionError();
        }
        if (assertionsOn) {
            FoldHierarchyTransactionImpl.LOG.log(Level.WARNING, null, new Throwable(sb.toString()));
        } else lbl-1000: // 2 sources:
        {
            FoldHierarchyTransactionImpl.LOG.warning(sb.toString());
        }
lbl96: // 4 sources:
        blocked = true;
        this.addFoldBlock = prevFold;
        prevOverlapIndexes = null;
        ** GOTO lbl114
lbl100: // 1 sources:
        if (foldPriority > this.getOperation(prevFold).getPriority()) {
            if (prevFold.getFoldCount() > 0) {
                prevOverlapIndexes = this.inspectOverlap(prevFold, foldStartOffset, foldPriority, 1);
                blocked = prevOverlapIndexes == null;
            } else {
                blocked = false;
                prevOverlapIndexes = FoldHierarchyTransactionImpl.EMPTY_INT_ARRAY;
            }
        } else {
            blocked = true;
            this.addFoldBlock = prevFold;
            prevOverlapIndexes = null;
        }
        ** GOTO lbl114
lbl112: // 1 sources:
        blocked = false;
        prevOverlapIndexes = null;
lbl114: // 5 sources:
        if (!blocked) {
            nextIndex = index;
            if (ea) {
                sbDebug.append("\n addFold2 nextIndex:" + nextIndex + " index:" + index);
            }
            nextOverlapIndexes = null;
            if (nextFold != null && foldEndOffset > nextFold.getStartOffset()) {
                if (foldEndOffset >= nextFold.getEndOffset()) {
                    nextIndex = FoldUtilitiesImpl.findFoldStartIndex(parentFold, foldEndOffset, false);
                    nextFold = parentFold.getFold(nextIndex);
                    if (ea) {
                        sbDebug.append("\n addFold3 nextIndex = FoldUtilitiesImpl.findFoldStartIndex(parentFold, foldEndOffset, false) nextIndex:" + nextIndex + " index:" + index + ", new nextFold = " + nextFold);
                    }
                }
                if (foldEndOffset < nextFold.getEndOffset()) {
                    if (foldPriority > this.getOperation(nextFold).getPriority()) {
                        if (nextFold.getFoldCount() > 0) {
                            nextOverlapIndexes = this.inspectOverlap(nextFold, foldEndOffset, foldPriority, 1);
                            if (nextOverlapIndexes == null) {
                                blocked = true;
                            }
                        } else {
                            nextOverlapIndexes = FoldHierarchyTransactionImpl.EMPTY_INT_ARRAY;
                        }
                    } else {
                        blocked = true;
                        this.addFoldBlock = nextFold;
                    }
                } else {
                    ++nextIndex;
                    if (ea) {
                        sbDebug.append("\n addFold4 nextIndex++ nextIndex:" + nextIndex + " index:" + index);
                    }
                }
            }
            if (!blocked) {
                if (prevOverlapIndexes != null) {
                    if (prevOverlapIndexes.length == 0) {
                        replaceIndexShift = 0;
                    } else {
                        replaceIndexShift = this.removeOverlap(prevFold, prevOverlapIndexes, fold);
                        nextIndex += prevFold.getFoldCount();
                        if (ea) {
                            sbDebug.append("\n addFold5 nextIndex += prevFold.getFoldCount() nextIndex:" + nextIndex + " index:" + index);
                        }
                    }
                    this.removeFoldFromHierarchy(parentFold, index - 1, fold);
                    index += replaceIndexShift - 1;
                    --nextIndex;
                    if (ea) {
                        sbDebug.append("\n addFold6 nextIndex-- nextIndex:" + nextIndex + " index:" + index);
                    }
                }
                if (nextOverlapIndexes != null) {
                    replaceIndexShift = nextOverlapIndexes.length == 0 ? 0 : this.removeOverlap(nextFold, nextOverlapIndexes, fold);
                    this.removeFoldFromHierarchy(parentFold, nextIndex, fold);
                    nextIndex += replaceIndexShift;
                    if (ea) {
                        sbDebug.append("\n addFold7 nextIndex += replaceIndexShift nextIndex:" + nextIndex + " replaceIndexShift:" + replaceIndexShift + " index:" + index);
                    }
                }
                if (ea) {
                    sbDebug.append("\n addFold8 INVOKE ApiPackageAccessor.get().foldExtractToChildren index:" + index + " nextIndex:" + nextIndex + " diff:" + (nextIndex - index) + " parentFold.getFoldCount():" + parentFold.getFoldCount());
                }
                if (!FoldHierarchyTransactionImpl.$assertionsDisabled && nextIndex - index < 0) {
                    throw new AssertionError((Object)("Negative length." + sbDebug.toString()));
                }
                if (!FoldHierarchyTransactionImpl.$assertionsDisabled && nextIndex > parentFold.getFoldCount()) {
                    throw new AssertionError((Object)("End index exceeds children list size." + sbDebug.toString()));
                }
                ApiPackageAccessor.get().foldExtractToChildren(parentFold, index, nextIndex - index, fold);
                realPF = fold.getParent();
                if (realPF == null || realPF != parentFold || realPF.getStartOffset() > fold.getStartOffset() || realPF.getEndOffset() < fold.getEndOffset()) {
                    FoldHierarchyTransactionImpl.LOG.warning("Invalid parent fold after insertion. Fold = " + fold + ", parent = " + realPF);
                    FoldHierarchyTransactionImpl.LOG.warning("debug info: " + sbDebug.toString());
                } else {
                    ix = realPF.getFoldIndex(fold);
                    realPrev = ix > 0 ? realPF.getFold(ix - 1) : null;
                    v2 = realNext = ix < realPF.getFoldCount() - 1 ? realPF.getFold(ix + 1) : null;
                    if (realPrev != null && (realPrev.getEndOffset() > fold.getStartOffset() || realNext != null && realNext.getStartOffset() < fold.getEndOffset())) {
                        FoldHierarchyTransactionImpl.LOG.warning("Invalid next/prev offsets: fold = " + fold + ", prev = " + realPrev + ", next = " + realNext);
                        FoldHierarchyTransactionImpl.LOG.warning("debug info: " + sbDebug.toString());
                    }
                }
                this.updateAffectedOffsets(fold);
                this.markFoldAddedToHierarchy(fold);
                this.processUnblocked();
            }
        }
        if (blocked) {
            this.execution.markBlocked(fold, this.addFoldBlock);
            this.addFoldBlock = null;
        }
        this.lastOperationFold = parentFold;
        this.lastOperationIndex = index + 1;
        if (blocked != false) return false;
        return true;
    }

    private String findFilePath(Document doc) {
        Object o = doc.getProperty("stream");
        if (o != null) {
            return o.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(o)) + ":" + o.toString();
        }
        return "null";
    }

    private int[] inspectOverlap(Fold fold, int offset, int priority, int level) {
        int[] result;
        Fold indexFold;
        int index = FoldUtilitiesImpl.findFoldStartIndex(fold, offset, false);
        if (index >= 0 && FoldUtilities.containsOffset(indexFold = fold.getFold(index), offset)) {
            if (priority > this.getOperation(indexFold).getPriority()) {
                if (indexFold.getFoldCount() > 0) {
                    result = this.inspectOverlap(indexFold, offset, priority, level + 1);
                    if (result != null) {
                        result[level] = index;
                    }
                } else {
                    result = new int[level + 1];
                    result[0] = 1;
                    result[level] = index;
                }
            } else {
                this.addFoldBlock = indexFold;
                result = null;
            }
        } else {
            result = new int[level + 1];
            result[0] = 0;
            result[level] = index;
        }
        return result;
    }

    private int removeOverlap(Fold fold, int[] indexes, Fold block) {
        int indexShift = 0;
        int indexesLengthM1 = indexes.length - 1;
        for (int i = 1; i < indexesLengthM1; ++i) {
            int index = indexes[i] + indexShift;
            this.removeFoldFromHierarchy(fold, index, block);
            indexShift += index;
        }
        int index = indexes[indexesLengthM1] + indexShift;
        if (indexes[0] == 0) {
            ++index;
        } else {
            this.removeFoldFromHierarchy(fold, index, block);
        }
        return index;
    }

    private void removeFoldFromHierarchy(Fold parentFold, int index, Fold block) {
        Fold removedFold = parentFold.getFold(index);
        this.updateAffectedOffsets(removedFold);
        ApiPackageAccessor.get().foldReplaceByChildren(parentFold, index);
        this.markFoldRemovedFromHierarchy(removedFold);
        this.unblockBlocked(removedFold);
        if (block != null) {
            int eo;
            int so = removedFold.getStartOffset();
            if (so < (eo = removedFold.getEndOffset())) {
                this.execution.markBlocked(removedFold, block);
            } else {
                this.execution.markDamaged();
            }
        }
    }

    private void unblockBlocked(Fold block) {
        Set blockedSet = this.execution.unmarkBlock(block);
        if (blockedSet != null) {
            for (Fold blocked : blockedSet) {
                int priority = this.getOperation(blocked).getPriority();
                while (this.unblockedFoldLists.size() <= priority) {
                    this.unblockedFoldLists.add(new ArrayList(4));
                }
                ((List)this.unblockedFoldLists.get(priority)).add(blocked);
                if (priority <= this.unblockedFoldMaxPriority) continue;
                this.unblockedFoldMaxPriority = priority;
            }
        }
    }

    private void processUnblocked() {
        ApiPackageAccessor api = ApiPackageAccessor.get();
        if (this.unblockedFoldMaxPriority >= 0) {
            for (int priority = this.unblockedFoldMaxPriority; priority >= 0; --priority) {
                List foldList = (List)this.unblockedFoldLists.get(priority);
                Fold rootFold = this.execution.getRootFold();
                for (int i = foldList.size() - 1; i >= 0; --i) {
                    int end;
                    Fold unblocked = (Fold)foldList.remove(i);
                    if (this.execution.isAddedOrBlocked(unblocked)) continue;
                    this.unblockedFoldMaxPriority = -1;
                    int start = unblocked.getStartOffset();
                    if (start >= (end = unblocked.getEndOffset())) {
                        api.foldMarkDamaged(unblocked, 6);
                        this.getManager(unblocked).removeEmptyNotify(unblocked);
                        continue;
                    }
                    this.addFold(unblocked, rootFold, 0);
                    if (this.unblockedFoldMaxPriority >= priority) {
                        throw new IllegalStateException("Folds removed with priority=" + this.unblockedFoldMaxPriority);
                    }
                    if (foldList.size() == i) continue;
                    throw new IllegalStateException("Same priority folds removed");
                }
            }
        }
        this.unblockedFoldMaxPriority = -1;
    }

    void reinsertFoldTree(Fold f) {
        if (this.reinsertSet != null && this.reinsertSet.contains(f)) {
            return;
        }
        boolean bl = false;
        if (f.getParent() == null) {
            if (this.execution.isBlocked(f)) {
                this.execution.unmarkBlocked(f);
                bl = true;
            } else {
                return;
            }
        }
        if (this.reinsertSet == null) {
            this.reinsertSet = new LinkedHashSet<Fold>();
        }
        if (bl) {
            if (f.getFoldCount() > 0 || f.getParent() != null) {
                LOG.warning("Blocked fold should have no parent and no children: " + f + ", dumping hierarchy: " + this.execution);
            }
            if (this.lastOperationFold == f) {
                this.lastOperationFold = null;
                this.lastOperationIndex = -1;
            }
            this.reinsertSet.add(f);
        } else {
            ArrayList c = new ArrayList();
            ApiPackageAccessor.get().foldTearOut(f, c);
            if (f.getFoldCount() > 0 || f.getParent() != null) {
                LOG.warning("Paret fold should have no parent and no children: " + f + ", dumping hierarchy: " + this.execution);
            }
            for (Fold x : c) {
                if (this.execution.isBlocked(x)) {
                    this.execution.unmarkBlocked(x);
                }
                this.unblockBlocked(x);
                if (this.lastOperationFold == x) {
                    this.lastOperationFold = null;
                    this.lastOperationIndex = -1;
                }
                if (x.getFoldCount() <= 0 && x.getParent() == null) continue;
                LOG.warning("Teared-out fold should have no parent and no children: " + f + ", dumping hierarchy: " + this.execution);
            }
            this.reinsertSet.addAll(c);
        }
    }

    boolean isReinserting(Fold f) {
        return this.reinsertSet != null && this.reinsertSet.contains(f);
    }

    private void markFoldAddedToHierarchy(Fold fold) {
        if (this.removedFromHierarchySet == null || !this.removedFromHierarchySet.remove(fold)) {
            if (this.addedToHierarchySet == null) {
                this.addedToHierarchySet = new HashSet();
            }
            this.addedToHierarchySet.add(fold);
        }
    }

    private void markFoldRemovedFromHierarchy(Fold fold) {
        if (this.addedToHierarchySet == null || !this.addedToHierarchySet.remove(fold)) {
            if (this.removedFromHierarchySet == null) {
                this.removedFromHierarchySet = new HashSet();
            }
            this.removedFromHierarchySet.add(fold);
        }
    }

    private void updateAffectedOffsets(Fold fold) {
        int endOffset;
        int startOffset = fold.getStartOffset();
        if (startOffset > (endOffset = fold.getEndOffset())) {
            this.execution.markDamaged();
            LOG.warning("Invalid fold range: " + fold + ". Dumping hierarchy");
            LOG.warning(this.execution.toString());
            Fold f = fold.getParent();
            startOffset = f.getStartOffset();
            endOffset = f.getEndOffset();
        }
        this.updateAffectedStartOffset(startOffset);
        this.updateAffectedEndOffset(endOffset);
    }

    private void updateAffectedStartOffset(int offset) {
        if (!(offset < 0 || this.affectedStartOffset != -1 && offset >= this.affectedStartOffset || this.affectedEndOffset >= 0 && offset > this.affectedEndOffset)) {
            this.affectedStartOffset = offset;
        }
    }

    private void updateAffectedEndOffset(int offset) {
        if (offset >= 0 && offset > this.affectedEndOffset && offset >= this.affectedStartOffset) {
            this.affectedEndOffset = offset;
        }
    }

    private void checkNotCommitted() {
        if (this.committed) {
            throw new IllegalStateException("FoldHierarchyChange already committed.");
        }
    }
}

