/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold;

import java.util.ArrayList;
import java.util.Collection;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.spi.editor.fold.FoldTypeProvider;

public class DefaultFoldProvider
implements FoldTypeProvider {
    private final Collection<FoldType> defaultTypes = new ArrayList<FoldType>(7);

    public DefaultFoldProvider() {
        this.defaultTypes.add(FoldType.CODE_BLOCK);
        this.defaultTypes.add(FoldType.COMMENT);
        this.defaultTypes.add(FoldType.DOCUMENTATION);
        this.defaultTypes.add(FoldType.INITIAL_COMMENT);
        this.defaultTypes.add(FoldType.MEMBER);
        this.defaultTypes.add(FoldType.NESTED);
        this.defaultTypes.add(FoldType.TAG);
        this.defaultTypes.add(FoldType.USER);
    }

    @Override
    public Collection getValues(Class type) {
        return type == FoldType.class ? this.defaultTypes : null;
    }

    @Override
    public boolean inheritable() {
        return false;
    }
}

