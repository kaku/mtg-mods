/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 */
package org.netbeans.modules.editor.fold;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.plaf.TextUI;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.fold.FoldManagerFactoryProvider;
import org.netbeans.spi.editor.fold.FoldManagerFactory;

class LayerProvider
extends FoldManagerFactoryProvider {
    private static final String FOLDER_NAME = "FoldManager";
    private Map kit2factoryList = new WeakHashMap();

    LayerProvider() {
    }

    @Override
    public List getFactoryList(FoldHierarchy hierarchy) {
        String mimeType;
        ArrayList factoryList = null;
        JTextComponent editorComponent = hierarchy.getComponent();
        EditorKit kit = editorComponent.getUI().getEditorKit(editorComponent);
        if (kit != null && (factoryList = (ArrayList)this.kit2factoryList.get(kit)) == null && (mimeType = kit.getContentType()) != null) {
            MimePath mimePath = MimePath.parse((String)mimeType);
            factoryList = new ArrayList(MimeLookup.getLookup((MimePath)mimePath).lookupAll(FoldManagerFactory.class));
        }
        if (factoryList == null) {
            return Collections.EMPTY_LIST;
        }
        return factoryList;
    }
}

