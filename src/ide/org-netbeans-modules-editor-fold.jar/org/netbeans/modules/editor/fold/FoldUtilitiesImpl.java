/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 */
package org.netbeans.modules.editor.fold;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.event.DocumentEvent;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.modules.editor.fold.ApiPackageAccessor;
import org.netbeans.modules.editor.fold.FoldHierarchyExecution;
import org.netbeans.modules.editor.fold.FoldOperationImpl;

public final class FoldUtilitiesImpl {
    public static final String PREF_COLLAPSE_PREFIX = "code-folding-collapse-";
    public static final String PREF_OVERRIDE_DEFAULTS = "code-folding-use-defaults";
    public static final String PREF_CODE_FOLDING_ENABLED = "code-folding-enable";
    public static final String PREF_CONTENT_PREVIEW = "code-folding-content.preview";
    public static final String PREF_CONTENT_SUMMARY = "code-folding-content.summary";
    public static final byte FLAG_NOTHING_DAMAGED = 0;
    public static final byte FLAG_COLLAPSED = 1;
    public static final byte FLAG_START_DAMAGED = 2;
    public static final byte FLAG_END_DAMAGED = 4;
    public static final byte FLAGS_DAMAGED = 6;

    private FoldUtilitiesImpl() {
    }

    public static boolean isFoldingEnabled(String mime) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)mime).lookup(Preferences.class);
        return prefs == null ? false : prefs.getBoolean("code-folding-enable", false);
    }

    public static boolean isFoldingEnabled(FoldHierarchy h) {
        Preferences p = ApiPackageAccessor.get().foldGetExecution(h).getFoldPreferences();
        return p.getBoolean("code-folding-enable", false);
    }

    public static boolean isAutoCollapsed(FoldType ft, FoldHierarchy h) {
        FoldType parent;
        Preferences p = ApiPackageAccessor.get().foldGetExecution(h).getFoldPreferences();
        return p.getBoolean("code-folding-collapse-" + ft.code(), (parent = ft.parent()) == null ? false : p.getBoolean("code-folding-collapse-" + parent.code(), false));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void collapseOrExpand(FoldHierarchy hierarchy, Collection foldTypes, boolean collapse) {
        block8 : {
            Document d = hierarchy.getComponent().getDocument();
            if (!(d instanceof AbstractDocument)) {
                return;
            }
            AbstractDocument adoc = (AbstractDocument)d;
            adoc.readLock();
            try {
                hierarchy.lock();
                try {
                    List foldList = FoldUtilitiesImpl.findRecursive(null, hierarchy.getRootFold(), foldTypes);
                    if (collapse) {
                        hierarchy.collapse(foldList);
                        break block8;
                    }
                    hierarchy.expand(foldList);
                }
                finally {
                    hierarchy.unlock();
                }
            }
            finally {
                adoc.readUnlock();
            }
        }
    }

    public static int findFoldStartIndex(Fold fold, int offset, boolean first) {
        int foldCount = fold.getFoldCount();
        int low = 0;
        int high = foldCount - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            Fold midFold = fold.getFold(mid);
            int midFoldStartOffset = midFold.getStartOffset();
            if (midFoldStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midFoldStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            if (first) {
                --mid;
                while (mid >= 0 && fold.getFold(mid).getStartOffset() == offset) {
                    --mid;
                }
                ++mid;
            } else {
                ++mid;
                while (mid < foldCount && fold.getFold(mid).getStartOffset() == offset) {
                    ++mid;
                }
                --mid;
            }
            return mid;
        }
        return high;
    }

    public static int findFoldInsertIndex(Fold fold, int childStartOffset) {
        return FoldUtilitiesImpl.findFoldStartIndex(fold, childStartOffset, false) + 1;
    }

    public static int findFoldEndIndex(Fold fold, int offset) {
        int foldCount = fold.getFoldCount();
        int low = 0;
        int high = foldCount - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            Fold midFold = fold.getFold(mid);
            int midFoldEndOffset = midFold.getEndOffset();
            if (midFoldEndOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midFoldEndOffset > offset) {
                high = mid - 1;
                continue;
            }
            ++mid;
            while (mid < foldCount && fold.getFold(mid).getEndOffset() <= offset) {
                ++mid;
            }
            return mid;
        }
        return low;
    }

    public static List childrenAsList(Fold fold, int index, int count) {
        ArrayList<Fold> l = new ArrayList<Fold>(count);
        while (--count >= 0) {
            l.add(fold.getFold(index));
            ++index;
        }
        return l;
    }

    public static List find(Fold fold, Collection foldTypes) {
        ArrayList<Fold> l = new ArrayList<Fold>();
        int foldCount = fold.getFoldCount();
        for (int i = 0; i < foldCount; ++i) {
            Fold child = fold.getFold(i);
            if (foldTypes != null && !foldTypes.contains(child.getType())) continue;
            l.add(child);
        }
        return l;
    }

    public static List findRecursive(List l, Fold fold, Collection foldTypes) {
        if (l == null) {
            l = new ArrayList<Fold>();
        }
        int foldCount = fold.getFoldCount();
        for (int i = 0; i < foldCount; ++i) {
            Fold child = fold.getFold(i);
            if (foldTypes == null || foldTypes.contains(child.getType())) {
                l.add(child);
            }
            FoldUtilitiesImpl.findRecursive(l, child, foldTypes);
        }
        return l;
    }

    public static Fold findOffsetFold(FoldHierarchy hierarchy, int offset) {
        Fold rootFold;
        int distance = Integer.MAX_VALUE;
        Fold fold = rootFold = hierarchy.getRootFold();
        boolean inspectNested = true;
        while (inspectNested) {
            int childIndex = FoldUtilitiesImpl.findFoldStartIndex(fold, offset, false);
            if (childIndex >= 0) {
                Fold wrapFold = fold.getFold(childIndex);
                int startOffset = wrapFold.getStartOffset();
                int endOffset = wrapFold.getEndOffset();
                if (startOffset <= offset && offset <= endOffset) {
                    fold = wrapFold;
                    continue;
                }
                inspectNested = false;
                continue;
            }
            inspectNested = false;
        }
        return fold != rootFold ? fold : null;
    }

    public static Fold findNearestFold(FoldHierarchy hierarchy, int offset, int endOffset) {
        Fold nearestFold = null;
        int distance = Integer.MAX_VALUE;
        Fold fold = hierarchy.getRootFold();
        boolean inspectNested = true;
        while (inspectNested) {
            int childCount = fold.getFoldCount();
            int childIndex = FoldUtilitiesImpl.findFoldEndIndex(fold, offset);
            if (childIndex < childCount) {
                Fold afterFold;
                int afterFoldDistance;
                Fold wrapOrAfterFold = fold.getFold(childIndex);
                int startOffset = wrapOrAfterFold.getStartOffset();
                if (startOffset >= endOffset) break;
                if (startOffset < offset) {
                    afterFold = ++childIndex < childCount ? fold.getFold(childIndex) : null;
                    fold = wrapOrAfterFold;
                } else {
                    afterFold = wrapOrAfterFold;
                    inspectNested = false;
                }
                if (afterFold == null || (afterFoldDistance = afterFold.getStartOffset() - offset) >= distance) continue;
                distance = afterFoldDistance;
                nearestFold = afterFold;
                continue;
            }
            inspectNested = false;
        }
        return nearestFold;
    }

    public static Fold findFirstCollapsedFold(FoldHierarchy hierarchy, int startOffset, int endOffset) {
        Fold fold = hierarchy.getRootFold();
        Fold lastFold = null;
        int lastIndex = 0;
        do {
            int index;
            if ((index = FoldUtilitiesImpl.findFoldEndIndex(fold, startOffset)) >= fold.getFoldCount()) {
                if (lastFold != null) {
                    return FoldUtilitiesImpl.findCollapsedRec(lastFold, lastIndex + 1, endOffset);
                }
                return null;
            }
            Fold childFold = fold.getFold(index);
            if (childFold.isCollapsed()) {
                return childFold;
            }
            if (childFold.getStartOffset() >= startOffset) {
                return FoldUtilitiesImpl.findCollapsedRec(fold, index, endOffset);
            }
            lastFold = fold;
            lastIndex = index;
            fold = childFold;
        } while (true);
    }

    public static Iterator collapsedFoldIterator(FoldHierarchy hierarchy, int startOffset, int endOffset) {
        return new CollapsedFoldIterator(FoldUtilitiesImpl.findFirstCollapsedFold(hierarchy, startOffset, endOffset), endOffset);
    }

    public static Fold findNextCollapsedFold(Fold fold, int endOffset) {
        if (FoldUtilities.isRootFold(fold)) {
            return FoldUtilitiesImpl.findCollapsedRec(fold, 0, endOffset);
        }
        Fold parent = fold.getParent();
        return FoldUtilitiesImpl.findCollapsedRec(parent, parent.getFoldIndex(fold) + 1, endOffset);
    }

    private static Fold findCollapsedRec(Fold fold, int startIndex, int endOffset) {
        return FoldUtilitiesImpl.findCollapsedRec(fold, startIndex, endOffset, true);
    }

    private static Fold findCollapsedRec(Fold fold, int startIndex, int endOffset, boolean findInUpperLevel) {
        if (fold.getStartOffset() > endOffset) {
            return null;
        }
        int foldCount = fold.getFoldCount();
        while (startIndex < foldCount) {
            Fold child = fold.getFold(startIndex);
            if (child.isCollapsed()) {
                return child;
            }
            Fold maybeCollapsed = FoldUtilitiesImpl.findCollapsedRec(child, 0, endOffset, false);
            if (maybeCollapsed != null) {
                return maybeCollapsed;
            }
            ++startIndex;
        }
        if (FoldUtilities.isRootFold(fold) || !findInUpperLevel) {
            return null;
        }
        Fold parent = fold.getParent();
        return FoldUtilitiesImpl.findCollapsedRec(parent, parent.getFoldIndex(fold) + 1, endOffset, true);
    }

    public static String foldToString(Fold fold) {
        return "[" + fold.getType() + "] " + (fold.isCollapsed() ? "C" : "E") + (FoldUtilities.isRootFold(fold) ? "" : Integer.toString(ApiPackageAccessor.get().foldGetOperation(fold).getPriority())) + " <" + fold.getStartOffset() + "," + fold.getEndOffset() + ">" + (FoldUtilities.isRootFold(fold) ? "" : new StringBuilder().append(", desc='").append(fold.getDescription()).append("'").toString()) + ", hash=0x" + Integer.toHexString(System.identityHashCode(fold));
    }

    public static void appendSpaces(StringBuffer sb, int spaces) {
        while (--spaces >= 0) {
            sb.append(' ');
        }
    }

    public static String foldToStringChildren(Fold fold, int indent) {
        indent += 4;
        StringBuffer sb = new StringBuffer();
        sb.append(fold);
        sb.append('\n');
        int foldCount = fold.getFoldCount();
        for (int i = 0; i < foldCount; ++i) {
            FoldUtilitiesImpl.appendSpaces(sb, indent);
            sb.append('[');
            sb.append(i);
            sb.append("]: ");
            sb.append(FoldUtilitiesImpl.foldToStringChildren(fold.getFold(i), indent));
        }
        return sb.toString();
    }

    public static String foldHierarchyEventToString(FoldHierarchyEvent evt) {
        StringBuffer sb = new StringBuffer();
        int removedFoldCount = evt.getRemovedFoldCount();
        for (int i = 0; i < removedFoldCount; ++i) {
            sb.append("R[");
            sb.append(i);
            sb.append("]: ");
            sb.append(evt.getRemovedFold(i));
            sb.append('\n');
        }
        int addedFoldCount = evt.getAddedFoldCount();
        for (int i2 = 0; i2 < addedFoldCount; ++i2) {
            sb.append("A[");
            sb.append(i2);
            sb.append("]: ");
            sb.append(evt.getAddedFold(i2));
            sb.append('\n');
        }
        int foldStateChangeCount = evt.getFoldStateChangeCount();
        for (int i3 = 0; i3 < foldStateChangeCount; ++i3) {
            FoldStateChange change = evt.getFoldStateChange(i3);
            sb.append("SC[");
            sb.append(i3);
            sb.append("]: ");
            sb.append(change);
            sb.append('\n');
        }
        if (foldStateChangeCount == 0) {
            sb.append("No FoldStateChange\n");
        }
        sb.append("affected: <");
        sb.append(evt.getAffectedStartOffset());
        sb.append(",");
        sb.append(evt.getAffectedEndOffset());
        sb.append(">\n");
        return sb.toString();
    }

    public static String foldStateChangeToString(FoldStateChange change) {
        StringBuffer sb = new StringBuffer();
        if (change.isCollapsedChanged()) {
            sb.append("C");
        }
        if (change.isDescriptionChanged()) {
            sb.append("D");
        }
        if (change.isEndOffsetChanged()) {
            sb.append("E");
        }
        sb.append(" fold=");
        sb.append(change.getFold());
        return sb.toString();
    }

    public static int isFoldDamagedByInsert(Fold f, DocumentEvent evt) {
        int s;
        int gel;
        int o = evt.getOffset();
        if (o < (s = f.getStartOffset())) {
            return 0;
        }
        int gs = f.getGuardedStart();
        if (gs == s) {
            ++gs;
        }
        if (o >= s && o < gs) {
            return 2;
        }
        int e = f.getEndOffset();
        if (o >= e) {
            return 0;
        }
        int l = evt.getLength();
        int ge = (e -= l) - (gel = ApiPackageAccessor.get().foldEndGuardedLength(f));
        if (o <= ge) {
            return 0;
        }
        if (e == ge) {
            --ge;
        }
        if (o >= ge && o < e) {
            return 4;
        }
        return 0;
    }

    public static int becomesDamagedByRemove(Fold f, DocumentEvent evt, boolean zero) {
        assert (evt.getType() == DocumentEvent.EventType.REMOVE);
        ApiPackageAccessor api = ApiPackageAccessor.get();
        int fs = f.getStartOffset();
        int fe = f.getEndOffset();
        int gs = fs + api.foldStartGuardedLength(f);
        int ge = fe - api.foldEndGuardedLength(f);
        int removeStart = evt.getOffset();
        int removeEnd = removeStart + evt.getLength();
        if (zero) {
            gs = gs == fs ? ++gs : -1;
            ge = ge == fe ? --ge : removeEnd + 1;
        } else {
            if (gs == fs) {
                gs = -1;
            }
            if (ge == fe) {
                ge = removeEnd + 1;
            }
        }
        int ret = 0;
        if (removeStart < gs && removeEnd >= fs) {
            ret |= 2;
        }
        if (removeStart < fe && removeEnd > ge) {
            ret |= 4;
        }
        return ret;
    }

    public static boolean becomesEmptyAfterRemove(Fold f, DocumentEvent evt) {
        assert (evt.getType() == DocumentEvent.EventType.REMOVE);
        int s = evt.getOffset();
        int e = evt.getLength() + s;
        return s <= f.getStartOffset() && e >= f.getEndOffset();
    }

    private static final class CollapsedFoldIterator
    implements Iterator {
        private Fold nextFold;
        private int endOffset;

        public CollapsedFoldIterator(Fold nextFold, int endOffset) {
            this.nextFold = nextFold;
            this.endOffset = endOffset;
        }

        @Override
        public boolean hasNext() {
            return this.nextFold != null;
        }

        public Object next() {
            Fold result = this.nextFold;
            this.nextFold = FoldUtilitiesImpl.findNextCollapsedFold(this.nextFold, this.endOffset);
            return result;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}

