/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationType$Severity
 *  org.netbeans.editor.Annotations
 *  org.netbeans.editor.Annotations$AnnotationsListener
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.errorstripe.apimodule.SPIAccessor
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatus
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatusProviderFactory
 *  org.netbeans.spi.editor.mimelookup.InstanceProvider
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 *  org.openide.ErrorManager
 *  org.openide.cookies.InstanceCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.NbCollections
 */
package org.netbeans.modules.editor.errorstripe;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.plaf.TextUI;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.AnnotationType;
import org.netbeans.editor.Annotations;
import org.netbeans.editor.BaseDocument;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.errorstripe.AnnotationMark;
import org.netbeans.modules.editor.errorstripe.AnnotationView;
import org.netbeans.modules.editor.errorstripe.AnnotationViewData;
import org.netbeans.modules.editor.errorstripe.apimodule.SPIAccessor;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;
import org.netbeans.spi.editor.errorstripe.UpToDateStatus;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProvider;
import org.netbeans.spi.editor.errorstripe.UpToDateStatusProviderFactory;
import org.netbeans.spi.editor.mimelookup.InstanceProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;
import org.openide.ErrorManager;
import org.openide.cookies.InstanceCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.NbCollections;

final class AnnotationViewDataImpl
implements PropertyChangeListener,
AnnotationViewData,
Annotations.AnnotationsListener {
    private static final Logger LOG = Logger.getLogger(AnnotationViewDataImpl.class.getName());
    private static final String UP_TO_DATE_STATUS_PROVIDER_FOLDER_NAME = "UpToDateStatusProvider";
    private static final String TEXT_BASE_PATH = "Editors/text/base/";
    private AnnotationView view;
    private JTextComponent pane;
    private BaseDocument document;
    private List<MarkProvider> markProviders = new ArrayList<MarkProvider>();
    private List<UpToDateStatusProvider> statusProviders = new ArrayList<UpToDateStatusProvider>();
    private Collection<Mark> currentMarks = null;
    private SortedMap<Integer, List<Mark>> marksMap = null;
    private static WeakHashMap<String, Collection<? extends MarkProviderCreator>> mime2Creators = new WeakHashMap();
    private static WeakHashMap<String, Collection<? extends UpToDateStatusProviderFactory>> mime2StatusProviders = new WeakHashMap();
    private static LegacyCrapProvider legacyCrap;

    public AnnotationViewDataImpl(AnnotationView view, JTextComponent pane) {
        this.view = view;
        this.pane = pane;
        this.document = null;
    }

    @Override
    public void register(BaseDocument document) {
        this.document = document;
        this.gatherProviders(this.pane);
        this.addListenersToProviders();
        if (document != null) {
            document.getAnnotations().addAnnotationsListener((Annotations.AnnotationsListener)this);
        }
        this.clear();
    }

    @Override
    public void unregister() {
        if (this.document != null) {
            this.document.getAnnotations().removeAnnotationsListener((Annotations.AnnotationsListener)this);
        }
        this.removeListenersFromProviders();
        this.document = null;
    }

    public static void initProviders(String mimeType) {
        MimePath legacyMimePath = MimePath.parse((String)"text/base");
        legacyCrap = (LegacyCrapProvider)MimeLookup.getLookup((MimePath)legacyMimePath).lookup(LegacyCrapProvider.class);
        AnnotationViewDataImpl.lookupProviders(mimeType);
    }

    private static void lookupProviders(String mimeType) {
        MimePath mimePath = MimePath.parse((String)mimeType);
        mime2Creators.put(mimeType, MimeLookup.getLookup((MimePath)mimePath).lookupAll(MarkProviderCreator.class));
        mime2StatusProviders.put(mimeType, MimeLookup.getLookup((MimePath)mimePath).lookupAll(UpToDateStatusProviderFactory.class));
    }

    private void gatherProviders(JTextComponent pane) {
        String mimeType;
        Collection<? extends MarkProviderCreator> creators;
        Collection<? extends UpToDateStatusProviderFactory> factories;
        long start = System.currentTimeMillis();
        ArrayList<MarkProvider> newMarkProviders = new ArrayList<MarkProvider>();
        if (legacyCrap != null) {
            AnnotationViewDataImpl.createMarkProviders(legacyCrap.getMarkProviderCreators(), newMarkProviders, pane);
        }
        if ((mimeType = DocumentUtilities.getMimeType((JTextComponent)pane)) == null) {
            mimeType = pane.getUI().getEditorKit(pane).getContentType();
        }
        if ((creators = mime2Creators.get(mimeType)) == null) {
            AnnotationViewDataImpl.lookupProviders(mimeType);
            creators = mime2Creators.get(mimeType);
        }
        AnnotationViewDataImpl.createMarkProviders(creators, newMarkProviders, pane);
        this.markProviders = newMarkProviders;
        ArrayList<UpToDateStatusProvider> newStatusProviders = new ArrayList<UpToDateStatusProvider>();
        if (legacyCrap != null) {
            AnnotationViewDataImpl.createStatusProviders(legacyCrap.getUpToDateStatusProviderFactories(), newStatusProviders, pane);
        }
        if ((factories = mime2StatusProviders.get(mimeType)) != null) {
            AnnotationViewDataImpl.createStatusProviders(factories, newStatusProviders, pane);
        } else if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Factories set to null in mimeType " + mimeType);
        }
        this.statusProviders = newStatusProviders;
        long end = System.currentTimeMillis();
        if (AnnotationView.TIMING_ERR.isLoggable(1)) {
            AnnotationView.TIMING_ERR.log(1, "gather providers took: " + (end - start));
        }
    }

    private static void createMarkProviders(Collection<? extends MarkProviderCreator> creators, List<MarkProvider> providers, JTextComponent pane) {
        for (MarkProviderCreator creator : creators) {
            MarkProvider provider;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("creator = " + creator);
            }
            if ((provider = creator.createMarkProvider(pane)) == null) continue;
            providers.add(provider);
        }
    }

    private static void createStatusProviders(Collection<? extends UpToDateStatusProviderFactory> factories, List<UpToDateStatusProvider> providers, JTextComponent pane) {
        for (UpToDateStatusProviderFactory factory : factories) {
            UpToDateStatusProvider provider;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("factory = " + (Object)factory);
            }
            if ((provider = factory.createUpToDateStatusProvider(pane.getDocument())) == null) continue;
            providers.add(provider);
        }
    }

    private void addListenersToProviders() {
        for (UpToDateStatusProvider provider2 : this.statusProviders) {
            SPIAccessor.getDefault().addPropertyChangeListener(provider2, (PropertyChangeListener)this);
        }
        for (MarkProvider provider : this.markProviders) {
            provider.addPropertyChangeListener(this);
        }
    }

    private void removeListenersFromProviders() {
        for (UpToDateStatusProvider provider2 : this.statusProviders) {
            SPIAccessor.getDefault().removePropertyChangeListener(provider2, (PropertyChangeListener)this);
        }
        for (MarkProvider provider : this.markProviders) {
            provider.removePropertyChangeListener(this);
        }
    }

    static Collection<Mark> createMergedMarks(List<MarkProvider> providers) {
        LinkedHashSet<Mark> result = new LinkedHashSet<Mark>();
        for (MarkProvider provider : providers) {
            result.addAll(provider.getMarks());
        }
        return result;
    }

    synchronized Collection<Mark> getMergedMarks() {
        if (this.currentMarks == null) {
            this.currentMarks = AnnotationViewDataImpl.createMergedMarks(this.markProviders);
        }
        return new ArrayList<Mark>(this.currentMarks);
    }

    static List<Mark> getStatusesForLineImpl(int line, SortedMap<Integer, List<Mark>> marks) {
        List inside = marks.get(line);
        return inside == null ? Collections.emptyList() : inside;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Mark getMainMarkForBlock(int startLine, int endLine) {
        Mark m1;
        AnnotationViewDataImpl annotationViewDataImpl = this;
        synchronized (annotationViewDataImpl) {
            m1 = AnnotationViewDataImpl.getMainMarkForBlockImpl(startLine, endLine, this.getMarkMap());
        }
        Mark m2 = this.getMainMarkForBlockAnnotations(startLine, endLine);
        if (m1 == null) {
            return m2;
        }
        if (m2 == null) {
            return m1;
        }
        if (AnnotationViewDataImpl.isMoreImportant(m1, m2)) {
            return m1;
        }
        return m2;
    }

    static Mark getMainMarkForBlockImpl(int startLine, int endLine, SortedMap<Integer, List<Mark>> marks) {
        int current = startLine - 1;
        Mark found = null;
        while ((current = AnnotationViewDataImpl.findNextUsedLine(current, marks)) != Integer.MAX_VALUE && current <= endLine) {
            for (Mark newMark : AnnotationViewDataImpl.getStatusesForLineImpl(current, marks)) {
                if (found != null && !AnnotationViewDataImpl.isMoreImportant(newMark, found)) continue;
                found = newMark;
            }
        }
        return found;
    }

    private static boolean isMoreImportant(Mark m1, Mark m2) {
        int compared = m1.getStatus().compareTo(m2.getStatus());
        if (compared == 0) {
            return m1.getPriority() < m2.getPriority();
        }
        return compared > 0;
    }

    private boolean isMoreImportant(AnnotationDesc a1, AnnotationDesc a2) {
        AnnotationType t1 = a1.getAnnotationTypeInstance();
        AnnotationType t2 = a2.getAnnotationTypeInstance();
        int compared = t1.getSeverity().compareTo((Object)t2.getSeverity());
        if (compared == 0) {
            return t1.getPriority() < t2.getPriority();
        }
        return compared > 0;
    }

    private boolean isValidForErrorStripe(AnnotationDesc a) {
        return a.getAnnotationTypeInstance().getSeverity() != AnnotationType.Severity.STATUS_NONE;
    }

    private Mark getMainMarkForBlockAnnotations(int startLine, int endLine) {
        AnnotationDesc foundDesc = null;
        for (AnnotationDesc desc : NbCollections.iterable(this.listAnnotations(startLine, endLine))) {
            if (foundDesc != null && !this.isMoreImportant(desc, foundDesc) || !this.isValidForErrorStripe(desc)) continue;
            foundDesc = desc;
        }
        if (foundDesc != null) {
            return new AnnotationMark(foundDesc);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int findNextUsedLine(int from) {
        int line1;
        AnnotationViewDataImpl annotationViewDataImpl = this;
        synchronized (annotationViewDataImpl) {
            line1 = AnnotationViewDataImpl.findNextUsedLine(from, this.getMarkMap());
        }
        int line2 = this.document.getAnnotations().getNextLineWithAnnotation(from + 1);
        if (line2 == -1) {
            line2 = Integer.MAX_VALUE;
        }
        return line1 < line2 ? line1 : line2;
    }

    static int findNextUsedLine(int from, SortedMap<Integer, List<Mark>> marks) {
        SortedMap<Integer, List<Mark>> next = marks.tailMap(from + 1);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("AnnotationView.findNextUsedLine from: " + from + "; marks: " + marks + "; next: " + next);
        }
        if (next.isEmpty()) {
            return Integer.MAX_VALUE;
        }
        return next.firstKey();
    }

    private void registerMark(Mark mark) {
        int[] span = mark.getAssignedLines();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("AnnotationView.registerMark mark: " + mark + "; from-to: " + span[0] + "-" + span[1]);
        }
        for (int line = span[0]; line <= span[1]; ++line) {
            List<Mark> inside = this.marksMap.get(line);
            if (inside == null) {
                inside = new ArrayList<Mark>();
                this.marksMap.put(line, inside);
            }
            inside.add(mark);
        }
    }

    private void unregisterMark(Mark mark) {
        int[] span = mark.getAssignedLines();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("AnnotationView.unregisterMark mark: " + mark + "; from-to: " + span[0] + "-" + span[1]);
        }
        for (int line = span[0]; line <= span[1]; ++line) {
            List<Mark> inside = this.marksMap.get(line);
            if (inside == null) continue;
            inside.remove(mark);
            if (inside.size() != 0) continue;
            this.marksMap.remove(line);
        }
    }

    synchronized SortedMap<Integer, List<Mark>> getMarkMap() {
        if (this.marksMap == null) {
            Collection<Mark> marks = this.getMergedMarks();
            this.marksMap = new TreeMap<Integer, List<Mark>>();
            for (Mark mark : marks) {
                this.registerMark(mark);
            }
        }
        return this.marksMap;
    }

    @Override
    public Status computeTotalStatus() {
        Status s;
        Status targetStatus = Status.STATUS_OK;
        Collection<Mark> marks = this.getMergedMarks();
        for (Mark mark : marks) {
            s = mark.getStatus();
            targetStatus = Status.getCompoundStatus(s, targetStatus);
        }
        for (AnnotationDesc desc : NbCollections.iterable(this.listAnnotations(-1, Integer.MAX_VALUE))) {
            s = AnnotationViewDataImpl.get(desc.getAnnotationTypeInstance());
            if (s == null) continue;
            targetStatus = Status.getCompoundStatus(s, targetStatus);
        }
        return targetStatus;
    }

    @Override
    public UpToDateStatus computeTotalStatusType() {
        if (this.statusProviders.isEmpty()) {
            return UpToDateStatus.UP_TO_DATE_DIRTY;
        }
        UpToDateStatus statusType = UpToDateStatus.UP_TO_DATE_OK;
        for (UpToDateStatusProvider provider : this.statusProviders) {
            UpToDateStatus newType = provider.getUpToDate();
            if (newType.compareTo((Object)statusType) <= 0) continue;
            statusType = newType;
        }
        return statusType;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("marks".equals(evt.getPropertyName())) {
            AnnotationViewDataImpl annotationViewDataImpl = this;
            synchronized (annotationViewDataImpl) {
                List<Mark> nue = (List<Mark>)evt.getNewValue();
                Collection old = (Collection)evt.getOldValue();
                if (nue == null && evt.getSource() instanceof MarkProvider) {
                    nue = ((MarkProvider)evt.getSource()).getMarks();
                }
                if (old != null && nue != null) {
                    LinkedHashSet added = new LinkedHashSet(nue);
                    LinkedHashSet removed = new LinkedHashSet(old);
                    Iterator old_it = old.iterator();
                    while (old_it.hasNext()) {
                        added.remove(old_it.next());
                    }
                    Iterator<Mark> nue_it = nue.iterator();
                    while (nue_it.hasNext()) {
                        removed.remove(nue_it.next());
                    }
                    if (this.marksMap != null) {
                        for (Mark mark2 : removed) {
                            this.unregisterMark(mark2);
                        }
                        for (Mark mark2 : added) {
                            this.registerMark(mark2);
                        }
                    }
                    if (this.currentMarks != null) {
                        LinkedHashSet<Mark> copy = new LinkedHashSet<Mark>(this.currentMarks);
                        copy.removeAll(removed);
                        copy.addAll(added);
                        this.currentMarks = copy;
                    }
                    this.view.fullRepaint();
                } else {
                    LOG.warning("For performance reasons, the providers should fill both old and new value in property changes. Problematic event: " + evt);
                    this.clear();
                    this.view.fullRepaint();
                }
                return;
            }
        }
        if ("upToDate".equals(evt.getPropertyName())) {
            this.view.fullRepaint(false);
            return;
        }
    }

    @Override
    public synchronized void clear() {
        this.currentMarks = null;
        this.marksMap = null;
    }

    @Override
    public int[] computeErrorsAndWarnings() {
        Status s;
        int errors = 0;
        int warnings = 0;
        Collection<Mark> marks = this.getMergedMarks();
        for (Mark mark : marks) {
            s = mark.getStatus();
            errors += s == Status.STATUS_ERROR ? 1 : 0;
            warnings += s == Status.STATUS_WARNING ? 1 : 0;
        }
        for (AnnotationDesc desc : NbCollections.iterable(this.listAnnotations(-1, Integer.MAX_VALUE))) {
            s = AnnotationViewDataImpl.get(desc.getAnnotationTypeInstance());
            if (s == null) continue;
            errors += s == Status.STATUS_ERROR ? 1 : 0;
            warnings += s == Status.STATUS_WARNING ? 1 : 0;
        }
        return new int[]{errors, warnings};
    }

    public void changedLine(int Line) {
        this.changedAll();
    }

    public void changedAll() {
        this.view.fullRepaint(false);
    }

    static Status get(AnnotationType.Severity severity) {
        if (severity == AnnotationType.Severity.STATUS_ERROR) {
            return Status.STATUS_ERROR;
        }
        if (severity == AnnotationType.Severity.STATUS_WARNING) {
            return Status.STATUS_WARNING;
        }
        if (severity == AnnotationType.Severity.STATUS_OK) {
            return Status.STATUS_OK;
        }
        return null;
    }

    static Status get(AnnotationType ann) {
        return AnnotationViewDataImpl.get(ann.getSeverity());
    }

    private Iterator<? extends AnnotationDesc> listAnnotations(final int startLine, final int endLine) {
        final Annotations annotations = this.document.getAnnotations();
        return new Iterator<AnnotationDesc>(){
            private final List<AnnotationDesc> remaining;
            private int line;
            private int last;
            private int unchagedLoops;
            private boolean stop;

            @Override
            public boolean hasNext() {
                if (this.stop) {
                    return false;
                }
                if (this.remaining.isEmpty() && (this.line = annotations.getNextLineWithAnnotation(this.line)) <= endLine && this.line != -1) {
                    AnnotationDesc desc;
                    AnnotationDesc[] descriptions;
                    if (this.last == this.line) {
                        ++this.unchagedLoops;
                        if (this.unchagedLoops >= 100) {
                            LOG.log(Level.WARNING, "Please add the following info to https://netbeans.org/bugzilla/show_bug.cgi?id=188843 : Possible infinite loop in getMainMarkForBlockAnnotations, debug data: {0}, unchaged loops: {1}", new Object[]{annotations.toString(), this.unchagedLoops});
                            this.stop = true;
                            return false;
                        }
                    } else {
                        if (this.line < this.last) {
                            LOG.log(Level.WARNING, "Please add the following info to https://netbeans.org/bugzilla/show_bug.cgi?id=188843 : line < last: {0} < {1}", new Object[]{this.line, this.last});
                            this.stop = true;
                            return false;
                        }
                        this.last = this.line;
                        this.unchagedLoops = 0;
                    }
                    if ((desc = annotations.getActiveAnnotation(this.line)) != null) {
                        this.remaining.add(desc);
                    }
                    if (annotations.getNumberOfAnnotations(this.line) > 1 && (descriptions = annotations.getPassiveAnnotationsForLine(this.line)) != null) {
                        this.remaining.addAll(Arrays.asList(descriptions));
                    }
                    ++this.line;
                }
                return !(this.stop = this.remaining.isEmpty());
            }

            @Override
            public AnnotationDesc next() {
                if (this.hasNext()) {
                    return this.remaining.remove(0);
                }
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }

    @MimeLocation(subfolderName="UpToDateStatusProvider", instanceProviderClass=LegacyCrapProvider.class)
    public static final class LegacyCrapProvider
    implements InstanceProvider {
        private final List<FileObject> instanceFiles;
        private List<MarkProviderCreator> creators;
        private List<UpToDateStatusProviderFactory> factories;

        public LegacyCrapProvider() {
            this(null);
        }

        public LegacyCrapProvider(List<FileObject> files) {
            this.instanceFiles = files;
        }

        public Collection<? extends MarkProviderCreator> getMarkProviderCreators() {
            if (this.creators == null) {
                this.computeInstances();
            }
            return this.creators;
        }

        public Collection<? extends UpToDateStatusProviderFactory> getUpToDateStatusProviderFactories() {
            if (this.factories == null) {
                this.computeInstances();
            }
            return this.factories;
        }

        public Object createInstance(List fileObjectList) {
            ArrayList<FileObject> textBaseFilesList = new ArrayList<FileObject>();
            for (Object o : fileObjectList) {
                FileObject fileObject = null;
                if (!(o instanceof FileObject)) continue;
                fileObject = (FileObject)o;
                String fullPath = fileObject.getPath();
                int idx = fullPath.lastIndexOf("UpToDateStatusProvider");
                assert (idx != -1);
                String path = fullPath.substring(0, idx);
                if (!"Editors/text/base/".equals(path)) continue;
                textBaseFilesList.add(fileObject);
                if (!LOG.isLoggable(Level.WARNING)) continue;
                LOG.warning("The 'text/base' mime type is deprecated, please move your file to the root. Offending file: " + fullPath);
            }
            return new LegacyCrapProvider(textBaseFilesList);
        }

        private void computeInstances() {
            ArrayList<MarkProviderCreator> newCreators = new ArrayList<MarkProviderCreator>();
            ArrayList<UpToDateStatusProviderFactory> newFactories = new ArrayList<UpToDateStatusProviderFactory>();
            for (FileObject f : this.instanceFiles) {
                if (!f.isValid() || !f.isData()) continue;
                try {
                    DataObject d = DataObject.find((FileObject)f);
                    InstanceCookie ic = (InstanceCookie)d.getLookup().lookup(InstanceCookie.class);
                    if (ic == null) continue;
                    if (MarkProviderCreator.class.isAssignableFrom(ic.instanceClass())) {
                        MarkProviderCreator creator = (MarkProviderCreator)ic.instanceCreate();
                        newCreators.add(creator);
                        continue;
                    }
                    if (!UpToDateStatusProviderFactory.class.isAssignableFrom(ic.instanceClass())) continue;
                    UpToDateStatusProviderFactory factory = (UpToDateStatusProviderFactory)ic.instanceCreate();
                    newFactories.add(factory);
                }
                catch (Exception e) {
                    LOG.log(Level.WARNING, null, e);
                }
            }
            this.creators = newCreators;
            this.factories = newFactories;
        }
    }

}

