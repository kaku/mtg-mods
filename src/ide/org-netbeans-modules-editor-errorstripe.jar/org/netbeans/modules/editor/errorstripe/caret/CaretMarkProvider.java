/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.text.NbDocument
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.errorstripe.caret;

import java.util.Collections;
import java.util.List;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.modules.editor.errorstripe.caret.CaretMark;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.openide.text.NbDocument;
import org.openide.util.RequestProcessor;

public class CaretMarkProvider
extends MarkProvider
implements CaretListener {
    private static final RequestProcessor RP = new RequestProcessor("CaretMarkProvider");
    private Mark mark;
    private JTextComponent component;

    public CaretMarkProvider(JTextComponent component) {
        this.component = component;
        component.addCaretListener(this);
        this.mark = this.createMark();
    }

    private Mark createMark() {
        int offset = this.component.getCaretPosition();
        Document doc = this.component.getDocument();
        int line = 0;
        if (doc instanceof StyledDocument) {
            line = NbDocument.findLineNumber((StyledDocument)((StyledDocument)doc), (int)offset);
        }
        return new CaretMark(line);
    }

    @Override
    public synchronized List<Mark> getMarks() {
        return Collections.singletonList(this.mark);
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        final List<Mark> old = this.getMarks();
        this.mark = this.createMark();
        final List<Mark> nue = this.getMarks();
        RP.post(new Runnable(){

            @Override
            public void run() {
                CaretMarkProvider.this.firePropertyChange("marks", old, nue);
            }
        });
    }

}

