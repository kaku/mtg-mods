/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.errorstripe.privatespi;

import java.awt.Color;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;

public interface Mark {
    public static final int TYPE_ERROR_LIKE = 1;
    public static final int TYPE_CARET = 2;
    public static final int PRIORITY_DEFAULT = 1000;

    public int getType();

    public Status getStatus();

    public int getPriority();

    public Color getEnhancedColor();

    public int[] getAssignedLines();

    public String getShortDescription();
}

