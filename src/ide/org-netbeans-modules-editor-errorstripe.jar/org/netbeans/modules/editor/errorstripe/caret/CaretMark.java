/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.errorstripe.caret;

import java.awt.Color;
import javax.swing.UIManager;
import org.netbeans.modules.editor.errorstripe.AnnotationView;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;
import org.openide.util.NbBundle;

public class CaretMark
implements Mark {
    private static final Color COLOR;
    private int line;

    public CaretMark(int line) {
        this.line = line;
    }

    @Override
    public String getShortDescription() {
        return NbBundle.getMessage(AnnotationView.class, (String)"TP_Current_Line");
    }

    @Override
    public int[] getAssignedLines() {
        return new int[]{this.line, this.line};
    }

    @Override
    public Color getEnhancedColor() {
        return COLOR;
    }

    @Override
    public int getPriority() {
        return 1000;
    }

    @Override
    public Status getStatus() {
        return Status.STATUS_OK;
    }

    @Override
    public int getType() {
        return 2;
    }

    public static Color getCaretMarkColor() {
        return COLOR;
    }

    static {
        Color c = UIManager.getColor("nb.editor.errorstripe.caret.color");
        if (null == c) {
            c = new Color(110, 110, 110);
        }
        COLOR = c;
    }
}

