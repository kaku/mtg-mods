/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldHierarchyEvent
 *  org.netbeans.api.editor.fold.FoldHierarchyListener
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.StringEscapeUtils
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatus
 *  org.openide.ErrorManager
 *  org.openide.text.NbDocument
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.editor.errorstripe;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import java.net.URL;
import java.text.MessageFormat;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import javax.swing.text.View;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.StringEscapeUtils;
import org.netbeans.modules.editor.errorstripe.AnnotationViewData;
import org.netbeans.modules.editor.errorstripe.AnnotationViewDataImpl;
import org.netbeans.modules.editor.errorstripe.caret.CaretMark;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;
import org.netbeans.spi.editor.errorstripe.UpToDateStatus;
import org.openide.ErrorManager;
import org.openide.text.NbDocument;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;

public class AnnotationView
extends JComponent
implements FoldHierarchyListener,
MouseListener,
MouseMotionListener,
DocumentListener,
PropertyChangeListener,
Accessible {
    static final ErrorManager ERR = ErrorManager.getDefault().getInstance("org.netbeans.modules.editor.errorstripe.AnnotationView");
    static final ErrorManager TIMING_ERR = ErrorManager.getDefault().getInstance("org.netbeans.modules.editor.errorstripe.AnnotationView.timing");
    private static final int STATUS_BOX_SIZE = 7;
    private static final int THICKNESS = 13;
    static final int PIXELS_FOR_LINE = 3;
    static final int LINE_SEPARATOR_SIZE = 1;
    static final int HEIGHT_OFFSET = 20;
    static final int UPPER_HANDLE = 4;
    static final int LOWER_HANDLE = 4;
    private BaseDocument doc;
    private final JTextComponent pane;
    private static final Color STATUS_UP_PART_COLOR = Color.WHITE;
    private static final Color STATUS_DOWN_PART_COLOR = new Color(13486779);
    private static final int QUIET_TIME = 100;
    private static final RequestProcessor WORKER = new RequestProcessor(AnnotationView.class.getName(), 1, false, false);
    private final RequestProcessor.Task repaintTask;
    private final RepaintTask repaintTaskRunnable;
    private final Insets scrollBar;
    private final AnnotationViewData data;
    private static final Icon busyIcon = new ImageIcon(AnnotationView.class.getResource("resources/hodiny.gif"));
    private static final Color GLOBAL_RED = new Color(16722460);
    private static final Color GLOBAL_YELLOW = new Color(14789120);
    private static final Color GLOBAL_GREEN = new Color(6665579);
    private static Field currWriterField;
    private int[] modelToViewCache = null;
    private int lines = -1;
    private int height = -1;
    private static final int VIEW_TO_MODEL_IMPORTANCE = 1;
    private static final String HTML_PREFIX_LOWERCASE = "<html";
    private static final String HTML_PREFIX_UPPERCASE = "<HTML";

    public AnnotationView(JTextComponent pane) {
        this.pane = pane;
        this.setName("errorStripe");
        this.repaintTaskRunnable = new RepaintTask();
        this.repaintTask = WORKER.create((Runnable)this.repaintTaskRunnable);
        this.data = new AnnotationViewDataImpl(this, pane);
        this.scrollBar = UIManager.getInsets("Nb.Editor.ErrorStripe.ScrollBar.Insets");
        FoldHierarchy.get((JTextComponent)pane).addFoldHierarchyListener((FoldHierarchyListener)this);
        pane.addPropertyChangeListener(this);
        this.updateForNewDocument();
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.setOpaque(true);
        this.setToolTipText(NbBundle.getMessage(AnnotationView.class, (String)"TP_ErrorStripe"));
    }

    AnnotationViewData getData() {
        return this.data;
    }

    private synchronized void updateForNewDocument() {
        this.data.unregister();
        Document newDocument = this.pane.getDocument();
        if (this.doc != null) {
            this.doc.removeDocumentListener((DocumentListener)this);
            this.doc = null;
        }
        if (newDocument instanceof BaseDocument) {
            this.doc = (BaseDocument)this.pane.getDocument();
            this.doc.addDocumentListener((DocumentListener)this);
        }
        this.data.register(this.doc);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    int[] getLinesSpan(int currentLine) {
        BaseDocument adoc = this.doc;
        if (adoc != null) {
            adoc.readLock();
        }
        try {
            int startLine;
            double componentHeight = this.getComponentHeight();
            double usableHeight = this.getUsableHeight();
            double position = this._modelToView(currentLine, componentHeight, usableHeight);
            if (position == -1.0) {
                int[] arrn = new int[]{currentLine, currentLine};
                return arrn;
            }
            int endLine = currentLine;
            for (startLine = currentLine; position == this._modelToView(startLine - 1, componentHeight, usableHeight) && startLine > 0; --startLine) {
            }
            while (endLine + 1 < Utilities.getRowCount((BaseDocument)this.doc) && position == this._modelToView(endLine + 1, componentHeight, usableHeight)) {
                ++endLine;
            }
            int[] arrn = new int[]{startLine, endLine};
            return arrn;
        }
        finally {
            if (adoc != null) {
                adoc.readUnlock();
            }
        }
    }

    private void drawOneColorGlobalStatus(Graphics g, Color color) {
        g.setColor(color);
        int x = 3;
        int y = (this.topOffset() - 7) / 2;
        g.fillRect(x, y, 7, 7);
        g.setColor(STATUS_DOWN_PART_COLOR);
        g.drawLine(x - 1, y - 1, x + 7, y - 1);
        g.drawLine(x - 1, y - 1, x - 1, y + 7);
        g.setColor(STATUS_UP_PART_COLOR);
        g.drawLine(x - 1, y + 7, x + 7, y + 7);
        g.drawLine(x + 7, y - 1, x + 7, y + 7);
    }

    private void drawInProgressGlobalStatus(Graphics g, Color color) {
        int x = 3;
        int y = (this.topOffset() - 7) / 2;
        busyIcon.paintIcon(this, g, x, y);
        g.setColor(STATUS_DOWN_PART_COLOR);
        g.drawLine(x - 1, y - 1, x + 7, y - 1);
        g.drawLine(x - 1, y - 1, x - 1, y + 7);
        g.setColor(STATUS_UP_PART_COLOR);
        g.drawLine(x - 1, y + 7, x + 7, y + 7);
        g.drawLine(x + 7, y - 1, x + 7, y + 7);
    }

    private Color getColorForGlobalStatus(Status status) {
        if (Status.STATUS_ERROR == status) {
            return GLOBAL_RED;
        }
        if (Status.STATUS_WARNING == status) {
            return GLOBAL_YELLOW;
        }
        return GLOBAL_GREEN;
    }

    private void drawGlobalStatus(Graphics g) {
        UpToDateStatus type = this.data.computeTotalStatusType();
        if (type == UpToDateStatus.UP_TO_DATE_DIRTY) {
            this.drawOneColorGlobalStatus(g, UIManager.getColor("Panel.background"));
        } else if (type == UpToDateStatus.UP_TO_DATE_PROCESSING) {
            this.drawInProgressGlobalStatus(g, null);
        } else if (type == UpToDateStatus.UP_TO_DATE_OK) {
            Status totalStatus = this.data.computeTotalStatus();
            this.drawOneColorGlobalStatus(g, this.getColorForGlobalStatus(totalStatus));
        } else {
            throw new IllegalStateException("Unknown up-to-date type: " + (Object)type);
        }
    }

    private int getCurrentLine() {
        int offset = this.pane.getCaretPosition();
        Document doc = this.pane.getDocument();
        int line = -1;
        if (doc instanceof StyledDocument) {
            line = NbDocument.findLineNumber((StyledDocument)((StyledDocument)doc), (int)offset);
        }
        return line;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean isWriteLocked(AbstractDocument doc) {
        Object f;
        if (currWriterField == null) {
            f = null;
            try {
                f = AbstractDocument.class.getDeclaredField("currWriter");
            }
            catch (NoSuchFieldException ex2) {
                throw new IllegalStateException(ex2);
            }
            f.setAccessible(true);
            AbstractDocument ex2 = doc;
            synchronized (ex2) {
                currWriterField = f;
            }
        }
        try {
            f = doc;
            synchronized (f) {
                return currWriterField.get(doc) != null;
            }
        }
        catch (IllegalAccessException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void paintComponent(Graphics g) {
        if (AnnotationView.isWriteLocked((AbstractDocument)this.doc)) {
            this.repaint(100);
            return;
        }
        long startTime = System.currentTimeMillis();
        super.paintComponent(g);
        Color oldColor = g.getColor();
        Color backColor = UIManager.getColor("NbEditorGlyphGutter.background");
        if (null == backColor) {
            backColor = UIManager.getColor("Panel.background");
        }
        g.setColor(backColor);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        int currentline = this.getCurrentLine();
        int annotatedLine = this.data.findNextUsedLine(-1);
        BaseDocument adoc = this.doc;
        if (AnnotationView.isWriteLocked((AbstractDocument)this.doc)) {
            this.repaint(100);
            return;
        }
        if (adoc != null) {
            adoc.readLock();
        }
        try {
            while (annotatedLine != Integer.MAX_VALUE) {
                int endLine;
                int[] lineSpan = this.getLinesSpan(annotatedLine);
                int startLine = lineSpan[0];
                Mark m = this.data.getMainMarkForBlock(startLine, endLine = lineSpan[1]);
                if (m != null) {
                    Status s = m.getStatus();
                    double start = this.modelToView(annotatedLine);
                    if (s != null) {
                        Color color = m.getEnhancedColor();
                        if (color == null) {
                            color = Status.getDefaultColor(s);
                        }
                        assert (color != null);
                        g.setColor(color);
                        if (m.getType() != 2) {
                            g.fillRect(1, (int)start, 11, 3);
                        }
                        if (startLine <= currentline && currentline <= endLine || m.getType() == 2) {
                            this.drawCurrentLineMark(g, (int)start);
                        }
                    }
                }
                annotatedLine = this.data.findNextUsedLine(endLine);
            }
            this.drawGlobalStatus(g);
        }
        finally {
            if (adoc != null) {
                adoc.readUnlock();
            }
        }
        g.setColor(oldColor);
        long end = System.currentTimeMillis();
        if (TIMING_ERR.isLoggable(1)) {
            TIMING_ERR.log("AnnotationView.paintComponent consumed: " + (end - startTime));
        }
    }

    private void drawCurrentLineMark(Graphics g, int start) {
        g.setColor(CaretMark.getCaretMarkColor());
        g.drawLine(2, start + 1, 10, start + 1);
        g.fillRect(5, start, 3, 3);
        g.draw3DRect(5, start, 2, 2, true);
    }

    void fullRepaint() {
        this.fullRepaint(false);
    }

    void fullRepaint(boolean clearMarksCache) {
        this.fullRepaint(clearMarksCache, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void fullRepaint(boolean clearMarksCache, boolean clearModelToViewCache) {
        RepaintTask repaintTask = this.repaintTaskRunnable;
        synchronized (repaintTask) {
            this.repaintTaskRunnable.setClearMarksCache(clearMarksCache);
            this.repaintTaskRunnable.setClearModelToViewCache(clearModelToViewCache);
            this.repaintTask.schedule(100);
        }
    }

    private void documentChange() {
        this.fullRepaint(this.lines != Utilities.getRowCount((BaseDocument)this.doc));
    }

    private double getComponentHeight() {
        final double[] ret = new double[1];
        this.pane.getDocument().render(new Runnable(){

            @Override
            public void run() {
                ret[0] = AnnotationView.this.pane.getUI().getRootView(AnnotationView.this.pane).getPreferredSpan(1);
            }
        });
        return ret[0];
    }

    double getUsableHeight() {
        Container scrollPaneCandidade = this.pane.getParent();
        if (scrollPaneCandidade != null && !(scrollPaneCandidade instanceof JScrollPane)) {
            scrollPaneCandidade = scrollPaneCandidade.getParent();
        }
        if (scrollPaneCandidade == null || !(scrollPaneCandidade instanceof JScrollPane) || this.scrollBar == null) {
            return this.getHeight() - 20;
        }
        JScrollPane scrollPane = (JScrollPane)scrollPaneCandidade;
        int visibleHeight = scrollPane.getViewport().getExtentSize().height;
        int topButton = this.topOffset();
        int bottomButton = this.scrollBar.bottom;
        return visibleHeight - topButton - bottomButton;
    }

    int topOffset() {
        if (this.scrollBar == null) {
            return 20;
        }
        return (20 > this.scrollBar.top ? 20 : this.scrollBar.top) + 3;
    }

    private int getYFromPos(int offset) throws BadLocationException {
        int result;
        TextUI ui = this.pane.getUI();
        offset = Math.max(offset, 0);
        if (ui instanceof BaseTextUI) {
            result = ((BaseTextUI)ui).getYFromPos(offset);
        } else {
            Rectangle r = this.pane.modelToView(offset);
            int n = result = r != null ? r.y : 0;
        }
        if (result == 0) {
            return -1;
        }
        return result;
    }

    private synchronized int getModelToViewImpl(int line) throws BadLocationException {
        int docLines = Utilities.getRowCount((BaseDocument)this.doc);
        if (this.modelToViewCache == null || this.height != this.pane.getHeight() || this.lines != docLines) {
            this.modelToViewCache = new int[Utilities.getRowCount((BaseDocument)this.doc) + 2];
            this.lines = Utilities.getRowCount((BaseDocument)this.doc);
            this.height = this.pane.getHeight();
        }
        if (line >= docLines) {
            return -1;
        }
        int result = this.modelToViewCache[line + 1];
        if (result == 0) {
            int lineOffset = Utilities.getRowStartFromLineOffset((BaseDocument)((BaseDocument)this.pane.getDocument()), (int)line);
            this.modelToViewCache[line + 1] = result = this.getYFromPos(lineOffset);
        }
        if (result == -1) {
            result = 0;
        }
        return result;
    }

    double modelToView(int line) {
        return this._modelToView(line, this.getComponentHeight(), this.getUsableHeight());
    }

    private double _modelToView(int line, double componentHeight, double usableHeight) {
        try {
            int r = this.getModelToViewImpl(line);
            if (r == -1) {
                return -1.0;
            }
            if (ERR.isLoggable(1)) {
                ERR.log(1, "AnnotationView.modelToView: line=" + line);
                ERR.log(1, "AnnotationView.modelToView: r=" + r);
                ERR.log(1, "AnnotationView.modelToView: getComponentHeight()=" + this.getComponentHeight());
                ERR.log(1, "AnnotationView.modelToView: getUsableHeight()=" + this.getUsableHeight());
            }
            if (componentHeight <= usableHeight) {
                return r + this.topOffset();
            }
            double position = (double)r / componentHeight;
            int blocksCount = (int)(usableHeight / 4.0);
            int block = (int)(position * (double)blocksCount);
            return block * 4 + this.topOffset();
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
            return -1.0;
        }
    }

    int[] viewToModel(double offset) {
        try {
            int positionOffset;
            if (this.getComponentHeight() <= this.getUsableHeight()) {
                double position;
                int positionOffset2 = this.pane.viewToModel(new Point(1, (int)(offset - (double)this.topOffset())));
                if (positionOffset2 == -1) {
                    return null;
                }
                int line = Utilities.getLineOffset((BaseDocument)this.doc, (int)positionOffset2);
                if (ERR.isLoggable(1)) {
                    ERR.log(1, "AnnotationView.viewToModel: line=" + line);
                }
                if (offset < (position = this.modelToView(line)) || offset >= position + 3.0) {
                    return null;
                }
                return this.getLinesSpan(line);
            }
            int blocksCount = (int)(this.getUsableHeight() / 4.0);
            int block = (int)((offset - (double)this.topOffset()) / 4.0);
            double yPos = this.getComponentHeight() * (double)block / (double)blocksCount;
            if (yPos == (double)((int)yPos)) {
                yPos -= 1.0;
            }
            if ((positionOffset = this.pane.viewToModel(new Point(0, (int)yPos))) == -1) {
                return null;
            }
            int line = Utilities.getLineOffset((BaseDocument)this.doc, (int)positionOffset) + 1;
            int[] span = this.getLinesSpan(line);
            double normalizedOffset = this.modelToView(span[0]);
            if (ERR.isLoggable(1)) {
                ERR.log(1, "AnnotationView.viewToModel: offset=" + offset);
                ERR.log(1, "AnnotationView.viewToModel: block=" + block);
                ERR.log(1, "AnnotationView.viewToModel: blocksCount=" + blocksCount);
                ERR.log(1, "AnnotationView.viewToModel: pane.getHeight()=" + this.pane.getHeight());
                ERR.log(1, "AnnotationView.viewToModel: yPos=" + yPos);
                ERR.log(1, "AnnotationView.viewToModel: positionOffset=" + positionOffset);
                ERR.log(1, "AnnotationView.viewToModel: line=" + line);
                ERR.log(1, "AnnotationView.viewToModel: normalizedOffset=" + normalizedOffset);
            }
            if (offset < normalizedOffset || offset >= normalizedOffset + 3.0) {
                return null;
            }
            if (block < 0) {
                return null;
            }
            return span;
        }
        catch (BadLocationException e) {
            ErrorManager.getDefault().notify((Throwable)e);
            return null;
        }
    }

    private Mark getMarkForPointImpl(double point) {
        int[] lineSpan = this.viewToModel(point);
        if (lineSpan == null) {
            return null;
        }
        int startLine = lineSpan[0];
        int endLine = lineSpan[1];
        if (startLine != -1) {
            return this.data.getMainMarkForBlock(startLine, endLine);
        }
        return null;
    }

    Mark getMarkForPoint(double point) {
        point = (int)(point / 4.0) * 4;
        Mark a = this.getMarkForPointImpl(point);
        if (ERR.isLoggable(1)) {
            ERR.log(1, "AnnotationView.getAnnotationForPoint: point=" + point);
            ERR.log(1, "AnnotationView.getAnnotationForPoint: a=" + a);
        }
        int relativeMax = Math.max(5, 5);
        for (int relative = 1; relative < relativeMax && a == null; relative = (int)((short)(relative + 1))) {
            if (relative <= 4) {
                a = this.getMarkForPointImpl(point + (double)relative);
                if (ERR.isLoggable(1)) {
                    ERR.log(1, "AnnotationView.getAnnotationForPoint: a=" + a);
                    ERR.log(1, "AnnotationView.getAnnotationForPoint: relative=" + relative);
                }
            }
            if (relative > 4 || a != null) continue;
            a = this.getMarkForPointImpl(point - (double)relative);
            if (!ERR.isLoggable(1)) continue;
            ERR.log(1, "AnnotationView.getAnnotationForPoint: a=" + a);
            ERR.log(1, "AnnotationView.getAnnotationForPoint: relative=-" + relative);
        }
        return a;
    }

    @Override
    public Dimension getMaximumSize() {
        return new Dimension(13, Integer.MAX_VALUE);
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(13, Integer.MIN_VALUE);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(13, Integer.MIN_VALUE);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.resetCursor();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.resetCursor();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.checkCursor(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.resetCursor();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.checkCursor(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.resetCursor();
        Mark mark = this.getMarkForPoint(e.getPoint().getY());
        if (mark != null) {
            this.pane.setCaretPosition(Utilities.getRowStartFromLineOffset((BaseDocument)this.doc, (int)mark.getAssignedLines()[0]));
        }
    }

    private void resetCursor() {
        this.setCursor(Cursor.getPredefinedCursor(0));
    }

    private void checkCursor(MouseEvent e) {
        Mark mark = this.getMarkForPoint(e.getPoint().getY());
        if (mark == null) {
            this.resetCursor();
            return;
        }
        this.setCursor(Cursor.getPredefinedCursor(12));
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        String description;
        int y;
        if (ERR.isLoggable(1)) {
            ERR.log(1, "getToolTipText: event=" + event);
        }
        if ((y = event.getY()) <= this.topOffset()) {
            int[] errWar = this.data.computeErrorsAndWarnings();
            int errors = errWar[0];
            int warnings = errWar[1];
            if (errors == 0 && warnings == 0) {
                return NbBundle.getBundle(AnnotationView.class).getString("TP_NoErrors");
            }
            if (errors == 0 && warnings != 0) {
                return MessageFormat.format(NbBundle.getBundle(AnnotationView.class).getString("TP_X_warning(s)"), new Integer(warnings));
            }
            if (errors != 0 && warnings == 0) {
                return MessageFormat.format(NbBundle.getBundle(AnnotationView.class).getString("TP_X_error(s)"), new Integer(errors));
            }
            return MessageFormat.format(NbBundle.getBundle(AnnotationView.class).getString("TP_X_error(s)_Y_warning(s)"), new Integer(errors), new Integer(warnings));
        }
        Mark mark = this.getMarkForPoint(y);
        if (mark != null && (description = mark.getShortDescription()) != null && description != null) {
            if (description.startsWith("<html") || description.startsWith("<HTML")) {
                return description;
            }
            return "<html><body>" + StringEscapeUtils.escapeHtml((String)description);
        }
        return null;
    }

    public void foldHierarchyChanged(FoldHierarchyEvent evt) {
        this.fullRepaint(false, true);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.documentChange();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.documentChange();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() == this.pane && "document".equals(evt.getPropertyName())) {
            this.updateForNewDocument();
            return;
        }
        this.fullRepaint();
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PANEL;
                }
            };
            this.accessibleContext.setAccessibleName(NbBundle.getMessage(AnnotationView.class, (String)"ACSN_AnnotationView"));
            this.accessibleContext.setAccessibleDescription(NbBundle.getMessage(AnnotationView.class, (String)"ACSD_AnnotationView"));
        }
        return this.accessibleContext;
    }

    private class RepaintTask
    implements Runnable {
        private boolean clearMarksCache;
        private boolean clearModelToViewCache;

        private RepaintTask() {
        }

        public void setClearMarksCache(boolean clearMarksCache) {
            this.clearMarksCache |= clearMarksCache;
        }

        public void setClearModelToViewCache(boolean clearModelToViewCache) {
            this.clearModelToViewCache |= clearModelToViewCache;
        }

        private synchronized boolean readAndDestroyClearMarksCache() {
            boolean result = this.clearMarksCache;
            this.clearMarksCache = false;
            return result;
        }

        private synchronized boolean readAndDestroyClearModelToViewCache() {
            boolean result = this.clearModelToViewCache;
            this.clearModelToViewCache = false;
            return result;
        }

        @Override
        public void run() {
            final boolean clearMarksCache = this.readAndDestroyClearMarksCache();
            final boolean clearModelToViewCache = this.readAndDestroyClearModelToViewCache();
            SwingUtilities.invokeLater(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    AnnotationView annotationView = AnnotationView.this;
                    synchronized (annotationView) {
                        if (clearMarksCache) {
                            AnnotationView.this.data.clear();
                        }
                        if (clearModelToViewCache) {
                            AnnotationView.this.modelToViewCache = null;
                        }
                    }
                    AnnotationView.this.invalidate();
                    AnnotationView.this.repaint();
                }
            });
        }

    }

}

