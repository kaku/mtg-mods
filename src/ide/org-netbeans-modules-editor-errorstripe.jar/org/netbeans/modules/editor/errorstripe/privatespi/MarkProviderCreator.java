/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.modules.editor.errorstripe.privatespi;

import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="UpToDateStatusProvider")
public interface MarkProviderCreator {
    public MarkProvider createMarkProvider(JTextComponent var1);
}

