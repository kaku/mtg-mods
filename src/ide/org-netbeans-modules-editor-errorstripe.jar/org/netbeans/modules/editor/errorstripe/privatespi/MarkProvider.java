/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.errorstripe.privatespi;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;

public abstract class MarkProvider {
    public static final String PROP_MARKS = "marks";
    private PropertyChangeSupport pcs;

    public MarkProvider() {
        this.pcs = new PropertyChangeSupport(this);
    }

    public abstract List<Mark> getMarks();

    public final void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public final void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    protected final void firePropertyChange(String name, Object old, Object nue) {
        this.pcs.firePropertyChange(name, old, nue);
    }
}

