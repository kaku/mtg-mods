/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.errorstripe.privatespi;

import java.awt.Color;

public final class Status
implements Comparable {
    private static final int STATUS_OK_NUMBER = 0;
    private static final int STATUS_WARNING_NUMBER = 1;
    private static final int STATUS_ERROR_NUMBER = 2;
    public static final Status STATUS_OK = new Status(0);
    public static final Status STATUS_WARNING = new Status(1);
    public static final Status STATUS_ERROR = new Status(2);
    private static final Status[] VALUES = new Status[]{STATUS_OK, STATUS_WARNING, STATUS_ERROR};
    private static final Color[] DEFAULT_STATUS_COLORS = new Color[]{Color.GREEN, new Color(14789120), new Color(16722460)};
    private int status;
    private static String[] STATUS_NAMES = new String[]{"OK", "WARNING", "ERROR"};

    private Status(int status) throws IllegalArgumentException {
        if (status != 2 && status != 1 && status != 0) {
            throw new IllegalArgumentException("Invalid status provided: " + status);
        }
        this.status = status;
    }

    private int getStatus() {
        return this.status;
    }

    public int compareTo(Object o) {
        Status remote = (Status)o;
        if (this.status > remote.status) {
            return 1;
        }
        if (this.status < remote.status) {
            return -1;
        }
        return 0;
    }

    public boolean equals(Object o) {
        if (!(o instanceof Status)) {
            return false;
        }
        Status remote = (Status)o;
        return this.status == remote.status;
    }

    public int hashCode() {
        return 43 ^ this.status;
    }

    public String toString() {
        return "[Status: " + STATUS_NAMES[this.getStatus()] + "]";
    }

    public static Status getCompoundStatus(Status first, Status second) throws IllegalArgumentException {
        if (first != STATUS_ERROR && first != STATUS_WARNING && first != STATUS_OK) {
            throw new IllegalArgumentException("Invalid status provided: " + first);
        }
        if (second != STATUS_ERROR && second != STATUS_WARNING && second != STATUS_OK) {
            throw new IllegalArgumentException("Invalid status provided: " + second);
        }
        return VALUES[Math.max(first.getStatus(), second.getStatus())];
    }

    public static Color getDefaultColor(Status s) {
        return DEFAULT_STATUS_COLORS[s.getStatus()];
    }
}

