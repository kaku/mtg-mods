/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.spi.editor.errorstripe.UpToDateStatus
 */
package org.netbeans.modules.editor.errorstripe;

import org.netbeans.editor.BaseDocument;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;
import org.netbeans.spi.editor.errorstripe.UpToDateStatus;

interface AnnotationViewData {
    public void register(BaseDocument var1);

    public void unregister();

    public Mark getMainMarkForBlock(int var1, int var2);

    public int findNextUsedLine(int var1);

    public Status computeTotalStatus();

    public UpToDateStatus computeTotalStatusType();

    public int[] computeErrorsAndWarnings();

    public void clear();
}

