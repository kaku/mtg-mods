/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.errorstripe.caret;

import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.errorstripe.caret.CaretMarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProvider;
import org.netbeans.modules.editor.errorstripe.privatespi.MarkProviderCreator;

public class CaretMarkProviderCreator
implements MarkProviderCreator {
    public static boolean switchOff = false;
    private static final boolean ENABLE = true;

    @Override
    public MarkProvider createMarkProvider(JTextComponent document) {
        if (!switchOff) {
            return new CaretMarkProvider(document);
        }
        return null;
    }
}

