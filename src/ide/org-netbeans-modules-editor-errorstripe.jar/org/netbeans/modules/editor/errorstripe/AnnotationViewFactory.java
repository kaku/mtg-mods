/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.MimeTypeInitializer
 *  org.netbeans.editor.SideBarFactory
 *  org.openide.ErrorManager
 */
package org.netbeans.modules.editor.errorstripe;

import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.MimeTypeInitializer;
import org.netbeans.editor.SideBarFactory;
import org.netbeans.modules.editor.errorstripe.AnnotationView;
import org.netbeans.modules.editor.errorstripe.AnnotationViewDataImpl;
import org.openide.ErrorManager;

public class AnnotationViewFactory
implements SideBarFactory,
MimeTypeInitializer {
    public JComponent createSideBar(JTextComponent target) {
        long start = System.currentTimeMillis();
        AnnotationView view = new AnnotationView(target);
        long end = System.currentTimeMillis();
        if (AnnotationView.TIMING_ERR.isLoggable(1)) {
            AnnotationView.TIMING_ERR.log(1, "creating AnnotationView component took: " + (end - start));
        }
        return view;
    }

    public void init(String mimeType) {
        AnnotationViewDataImpl.initProviders(mimeType);
    }
}

