/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.AnnotationDesc
 *  org.netbeans.editor.AnnotationType
 *  org.netbeans.editor.AnnotationType$Severity
 */
package org.netbeans.modules.editor.errorstripe;

import java.awt.Color;
import org.netbeans.editor.AnnotationDesc;
import org.netbeans.editor.AnnotationType;
import org.netbeans.modules.editor.errorstripe.AnnotationViewDataImpl;
import org.netbeans.modules.editor.errorstripe.privatespi.Mark;
import org.netbeans.modules.editor.errorstripe.privatespi.Status;

public class AnnotationMark
implements Mark {
    private AnnotationDesc annotation;

    public AnnotationMark(AnnotationDesc annotation) {
        this.annotation = annotation;
    }

    @Override
    public Status getStatus() {
        AnnotationType type = this.annotation.getAnnotationTypeInstance();
        AnnotationType.Severity severity = type.getSeverity();
        return AnnotationViewDataImpl.get(severity);
    }

    @Override
    public Color getEnhancedColor() {
        AnnotationType type = this.annotation.getAnnotationTypeInstance();
        return type.getCustomSidebarColor();
    }

    @Override
    public int[] getAssignedLines() {
        return new int[]{this.annotation.getLine(), this.annotation.getLine()};
    }

    @Override
    public String getShortDescription() {
        return this.annotation.getShortDescription();
    }

    @Override
    public int getType() {
        return 1;
    }

    @Override
    public int getPriority() {
        return this.annotation.getAnnotationTypeInstance().getPriority();
    }

    public AnnotationDesc getAnnotationDesc() {
        return this.annotation;
    }
}

