/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.lookup.Lookups
 */
package org.netbeans.modules.tasklist.trampoline;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.netbeans.modules.tasklist.trampoline.TaskGroup;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.lookup.Lookups;

public final class TaskGroupFactory {
    static final String ATTR_GROUP_NAME = "groupName";
    static final String ATTR_BUNDLE_NAME = "localizingBundle";
    static final String ATTR_DISPLAY_NAME_KEY = "diplayNameKey";
    static final String ATTR_DESCRIPTION_KEY = "descriptionKey";
    static final String ATTR_ICON_KEY = "iconKey";
    private static final String GROUP_LIST_PATH = "TaskList/Groups";
    private static TaskGroupFactory theInstance;
    private Lookup.Result<TaskGroup> lookupRes;
    private Map<String, TaskGroup> name2group;
    private List<TaskGroup> groups;
    private static TaskGroup defaultGroup;

    private TaskGroupFactory() {
    }

    public static TaskGroup create(Map<String, String> attrs) {
        String groupName = attrs.get("groupName");
        String bundleName = attrs.get("localizingBundle");
        String displayNameKey = attrs.get("diplayNameKey");
        String descriptionKey = attrs.get("descriptionKey");
        String iconKey = attrs.get("iconKey");
        return TaskGroupFactory.create(groupName, bundleName, displayNameKey, descriptionKey, iconKey);
    }

    public static TaskGroup create(String groupName, String bundleName, String displayNameKey, String descriptionKey, String iconKey) {
        ResourceBundle bundle = NbBundle.getBundle((String)bundleName);
        String displayName = bundle.getString(displayNameKey);
        String description = bundle.getString(descriptionKey);
        String iconPath = bundle.getString(iconKey);
        Image icon = ImageUtilities.loadImage((String)iconPath);
        return new TaskGroup(groupName, displayName, description, icon);
    }

    public static TaskGroupFactory getDefault() {
        if (null == theInstance) {
            theInstance = new TaskGroupFactory();
        }
        return theInstance;
    }

    public TaskGroup getDefaultGroup() {
        if (null == defaultGroup) {
            ResourceBundle bundle = NbBundle.getBundle(TaskGroupFactory.class);
            defaultGroup = new TaskGroup("nb-unknown-group", bundle.getString("LBL_UnknownGroup"), bundle.getString("HINT_UnknownGroup"), ImageUtilities.loadImage((String)"org/netbeans/modules/tasklist/trampoline/unknown.gif"));
        }
        return defaultGroup;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void initGroups() {
        TaskGroupFactory taskGroupFactory = this;
        synchronized (taskGroupFactory) {
            if (null == this.name2group) {
                if (null == this.lookupRes) {
                    this.lookupRes = this.initLookup();
                    this.lookupRes.addLookupListener(new LookupListener(){

                        /*
                         * WARNING - Removed try catching itself - possible behaviour change.
                         */
                        public void resultChanged(LookupEvent ev) {
                            TaskGroupFactory taskGroupFactory = TaskGroupFactory.this;
                            synchronized (taskGroupFactory) {
                                TaskGroupFactory.this.name2group = null;
                                TaskGroupFactory.this.groups = null;
                            }
                        }
                    });
                }
                int index = 0;
                this.groups = new ArrayList<TaskGroup>(this.lookupRes.allInstances());
                this.name2group = new HashMap<String, TaskGroup>(this.groups.size());
                for (TaskGroup tg : this.groups) {
                    this.name2group.put(tg.getName(), tg);
                    tg.setIndex(index++);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public TaskGroup getGroup(String groupName) {
        assert (null != groupName);
        TaskGroupFactory taskGroupFactory = this;
        synchronized (taskGroupFactory) {
            this.initGroups();
            return this.name2group.get(groupName);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<? extends TaskGroup> getGroups() {
        TaskGroupFactory taskGroupFactory = this;
        synchronized (taskGroupFactory) {
            this.initGroups();
            return this.groups;
        }
    }

    private Lookup.Result<TaskGroup> initLookup() {
        Lookup lkp = Lookups.forPath((String)"TaskList/Groups");
        Lookup.Template template = new Lookup.Template(TaskGroup.class);
        Lookup.Result res = lkp.lookup(template);
        return res;
    }

}

