/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.tasklist.trampoline;

import java.awt.Image;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.Action;
import org.netbeans.modules.tasklist.trampoline.EmptyScanningScope;
import org.netbeans.modules.tasklist.trampoline.TaskGroup;
import org.netbeans.modules.tasklist.trampoline.TaskManager;
import org.netbeans.spi.tasklist.FileTaskScanner;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;

public abstract class Accessor {
    public static Accessor DEFAULT;
    private static TaskScanningScope EMPTY_SCOPE;

    public abstract String getDescription(Task var1);

    public abstract FileObject getFile(Task var1);

    public abstract URL getURL(Task var1);

    public abstract TaskGroup getGroup(Task var1);

    public abstract int getLine(Task var1);

    public abstract ActionListener getDefaultAction(Task var1);

    public abstract Action[] getActions(Task var1);

    public abstract String getDisplayName(TaskScanningScope var1);

    public abstract String getDescription(TaskScanningScope var1);

    public abstract Image getIcon(TaskScanningScope var1);

    public abstract boolean isDefault(TaskScanningScope var1);

    public abstract TaskScanningScope.Callback createCallback(TaskManager var1, TaskScanningScope var2);

    public abstract String getDisplayName(FileTaskScanner var1);

    public abstract String getDescription(FileTaskScanner var1);

    public abstract String getOptionsPath(FileTaskScanner var1);

    public abstract FileTaskScanner.Callback createCallback(TaskManager var1, FileTaskScanner var2);

    public abstract String getDisplayName(PushTaskScanner var1);

    public abstract String getDescription(PushTaskScanner var1);

    public abstract String getOptionsPath(PushTaskScanner var1);

    public abstract PushTaskScanner.Callback createCallback(TaskManager var1, PushTaskScanner var2);

    public static TaskScanningScope getEmptyScope() {
        if (null == EMPTY_SCOPE) {
            EMPTY_SCOPE = new EmptyScanningScope();
        }
        return EMPTY_SCOPE;
    }

    static {
        block2 : {
            Class<Task> c = Task.class;
            try {
                Class.forName(c.getName(), true, c.getClassLoader());
            }
            catch (ClassNotFoundException ex) {
                if ($assertionsDisabled) break block2;
                throw new AssertionError(ex);
            }
        }
        EMPTY_SCOPE = null;
    }
}

