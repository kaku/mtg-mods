/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.tasklist.trampoline;

import java.awt.Image;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

class EmptyScanningScope
extends TaskScanningScope {
    private final Iterator<FileObject> emptyIterator;

    public EmptyScanningScope() {
        super(null, null, null);
        this.emptyIterator = new Iterator<FileObject>(){

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public FileObject next() {
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public boolean isInScope(FileObject resource) {
        return false;
    }

    @Override
    public void attach(TaskScanningScope.Callback callback) {
    }

    @Override
    public Lookup getLookup() {
        return Lookup.EMPTY;
    }

    @Override
    public Iterator<FileObject> iterator() {
        return this.emptyIterator;
    }

}

