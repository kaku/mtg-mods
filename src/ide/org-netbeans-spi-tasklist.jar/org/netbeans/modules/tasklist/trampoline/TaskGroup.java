/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.tasklist.trampoline;

import java.awt.Image;
import java.util.List;
import org.netbeans.modules.tasklist.trampoline.TaskGroupFactory;

public final class TaskGroup
implements Comparable<TaskGroup> {
    private String name;
    private String displayName;
    private String description;
    private Image icon;
    private int index;

    public TaskGroup(String name, String displayName, String description, Image icon) {
        assert (null != name);
        assert (null != displayName);
        assert (null != icon);
        this.name = name;
        this.displayName = displayName;
        this.description = description;
        this.icon = icon;
    }

    public static List<? extends TaskGroup> getGroups() {
        return TaskGroupFactory.getDefault().getGroups();
    }

    public String getName() {
        return this.name;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getDescription() {
        return this.description;
    }

    public Image getIcon() {
        return this.icon;
    }

    @Override
    public int compareTo(TaskGroup otherGroup) {
        return this.index - otherGroup.index;
    }

    void setIndex(int index) {
        this.index = index;
    }

    public String toString() {
        return this.getDisplayName();
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        TaskGroup test = (TaskGroup)o;
        if (this.name != test.name && this.name != null && !this.name.equals(test.name)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
}

