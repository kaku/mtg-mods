/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.modules.tasklist.trampoline;

import java.util.List;
import org.netbeans.spi.tasklist.FileTaskScanner;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;

public abstract class TaskManager {
    public /* varargs */ abstract void refresh(FileTaskScanner var1, FileObject ... var2);

    public abstract void refresh(FileTaskScanner var1);

    public abstract void refresh(TaskScanningScope var1);

    public abstract void started(PushTaskScanner var1);

    public abstract void finished(PushTaskScanner var1);

    public abstract void setTasks(PushTaskScanner var1, FileObject var2, List<? extends Task> var3);

    public void setTasks(PushTaskScanner scanner, List<? extends Task> tasks) {
        this.setTasks(scanner, null, tasks);
    }

    public abstract void clearAllTasks(PushTaskScanner var1);

    public abstract boolean isObserved();

    public abstract boolean isCurrentEditorScope();
}

