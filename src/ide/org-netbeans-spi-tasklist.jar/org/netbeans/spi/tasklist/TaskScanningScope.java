/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 */
package org.netbeans.spi.tasklist;

import java.awt.Image;
import java.util.Map;
import org.netbeans.modules.tasklist.trampoline.TaskManager;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class TaskScanningScope
implements Iterable<FileObject>,
Lookup.Provider {
    private String displayName;
    private String description;
    private Image icon;
    private boolean isDefault;

    public TaskScanningScope(String displayName, String description, Image icon) {
        this(displayName, description, icon, false);
    }

    public TaskScanningScope(String displayName, String description, Image icon, boolean isDefault) {
        this.displayName = displayName;
        this.description = description;
        this.icon = icon;
        this.isDefault = isDefault;
    }

    final String getDisplayName() {
        String res = null;
        Map labels = (Map)this.getLookup().lookup(Map.class);
        if (null != labels) {
            res = (String)labels.get("Name");
        }
        if (null == res) {
            res = this.displayName;
        }
        return res;
    }

    final String getDescription() {
        String res = null;
        Map labels = (Map)this.getLookup().lookup(Map.class);
        if (null != labels) {
            res = (String)labels.get("ShortDescription");
        }
        if (null == res) {
            res = this.description;
        }
        return res;
    }

    final Image getIcon() {
        return this.icon;
    }

    final boolean isDefault() {
        return this.isDefault;
    }

    public abstract boolean isInScope(FileObject var1);

    public abstract void attach(Callback var1);

    public abstract Lookup getLookup();

    public static final class Callback {
        private TaskScanningScope scope;
        private TaskManager tm;

        Callback(TaskManager tm, TaskScanningScope scope) {
            this.tm = tm;
            this.scope = scope;
        }

        public void refresh() {
            this.tm.refresh(this.scope);
        }
    }

}

