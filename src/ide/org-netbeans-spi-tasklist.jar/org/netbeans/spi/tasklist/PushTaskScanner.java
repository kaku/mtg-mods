/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.tasklist;

import java.util.List;
import org.netbeans.modules.tasklist.trampoline.TaskManager;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;

public abstract class PushTaskScanner {
    private String displayName;
    private String description;
    private String optionsPath;

    public PushTaskScanner(String displayName, String description, String optionsPath) {
        assert (null != displayName);
        this.displayName = displayName;
        this.description = description;
        this.optionsPath = optionsPath;
    }

    final String getDisplayName() {
        return this.displayName;
    }

    final String getDescription() {
        return this.description;
    }

    final String getOptionsPath() {
        return this.optionsPath;
    }

    public abstract void setScope(TaskScanningScope var1, Callback var2);

    public static final class Callback {
        private PushTaskScanner scanner;
        private TaskManager tm;

        Callback(TaskManager tm, PushTaskScanner scanner) {
            this.tm = tm;
            this.scanner = scanner;
        }

        public void started() {
            this.tm.started(this.scanner);
        }

        public void setTasks(FileObject file, List<? extends Task> tasks) {
            this.tm.setTasks(this.scanner, file, tasks);
        }

        public void setTasks(List<? extends Task> tasks) {
            this.tm.setTasks(this.scanner, tasks);
        }

        public void clearAllTasks() {
            this.tm.clearAllTasks(this.scanner);
        }

        public void finished() {
            this.tm.finished(this.scanner);
        }

        public boolean isObserved() {
            return this.tm.isObserved();
        }

        public boolean isCurrentEditorScope() {
            return this.tm.isCurrentEditorScope();
        }
    }

}

