/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.tasklist;

import java.util.List;
import org.netbeans.modules.tasklist.trampoline.TaskManager;
import org.netbeans.spi.tasklist.Task;
import org.openide.filesystems.FileObject;

public abstract class FileTaskScanner {
    private String displayName;
    private String description;
    private String optionsPath;

    public FileTaskScanner(String displayName, String description, String optionsPath) {
        assert (null != displayName);
        this.displayName = displayName;
        this.description = description;
        this.optionsPath = optionsPath;
    }

    final String getDisplayName() {
        return this.displayName;
    }

    final String getDescription() {
        return this.description;
    }

    final String getOptionsPath() {
        return this.optionsPath;
    }

    public void notifyPrepare() {
    }

    public void notifyFinish() {
    }

    public abstract List<? extends Task> scan(FileObject var1);

    public abstract void attach(Callback var1);

    public static final class Callback {
        private FileTaskScanner scanner;
        private TaskManager tm;

        Callback(TaskManager tm, FileTaskScanner scanner) {
            this.tm = tm;
            this.scanner = scanner;
        }

        public /* varargs */ void refresh(FileObject ... resources) {
            this.tm.refresh(this.scanner, resources);
        }

        public void refreshAll() {
            this.tm.refresh(this.scanner);
        }
    }

}

