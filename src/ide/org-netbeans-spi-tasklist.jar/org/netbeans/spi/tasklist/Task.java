/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.NbBundle
 */
package org.netbeans.spi.tasklist;

import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.modules.tasklist.trampoline.Accessor;
import org.netbeans.modules.tasklist.trampoline.TaskGroup;
import org.netbeans.modules.tasklist.trampoline.TaskGroupFactory;
import org.netbeans.spi.tasklist.AccessorImpl;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

public final class Task {
    private final URL url;
    private final FileObject file;
    private final TaskGroup group;
    private final String description;
    private final int line;
    private final ActionListener defaultAction;
    private final Action[] actions;
    private static Set<String> unknownTaskGroups;

    public static Task create(URL resource, String groupName, String description) {
        return new Task(null, resource, Task.getTaskGroup(groupName), description, -1, null, null);
    }

    public static Task create(URL resource, String groupName, String description, ActionListener defaultAction, Action[] popupActions) {
        return new Task(null, resource, Task.getTaskGroup(groupName), description, -1, defaultAction, popupActions);
    }

    public static Task create(FileObject resource, String groupName, String description, int line) {
        assert (null != resource);
        return new Task(resource, null, Task.getTaskGroup(groupName), description, line, null, null);
    }

    public static Task create(FileObject resource, String groupName, String description, ActionListener al) {
        assert (null != resource);
        return new Task(resource, null, Task.getTaskGroup(groupName), description, -1, al, null);
    }

    private Task(FileObject file, URL url, TaskGroup group, String description, int line, ActionListener defaultAction, Action[] actions) {
        assert (null != group);
        assert (null != description);
        assert (null == file || null == url);
        this.file = file;
        this.url = url;
        this.group = group;
        this.description = description;
        this.line = line;
        this.defaultAction = defaultAction;
        this.actions = actions;
    }

    URL getURL() {
        return this.url;
    }

    FileObject getFile() {
        return this.file;
    }

    TaskGroup getGroup() {
        return this.group;
    }

    String getDescription() {
        return this.description;
    }

    int getLine() {
        return this.line;
    }

    ActionListener getDefaultAction() {
        return this.defaultAction;
    }

    Action[] getActions() {
        return this.actions;
    }

    static TaskGroup createGroup(Map<String, String> attrs) {
        return TaskGroupFactory.create(attrs);
    }

    private static TaskGroup getTaskGroup(String groupName) {
        TaskGroup group = TaskGroupFactory.getDefault().getGroup(groupName);
        if (null == group) {
            if (null == unknownTaskGroups || !unknownTaskGroups.contains(groupName)) {
                Logger.getLogger(Task.class.getName()).log(Level.INFO, NbBundle.getMessage(Task.class, (String)"Err_UnknownGroupName"), groupName);
                if (null == unknownTaskGroups) {
                    unknownTaskGroups = new HashSet<String>(10);
                }
                unknownTaskGroups.add(groupName);
            }
            group = TaskGroupFactory.getDefault().getDefaultGroup();
        }
        return group;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        Task test = (Task)o;
        if (this.line != test.line) {
            return false;
        }
        if (this.description != test.description && this.description != null && !this.description.equals(test.description)) {
            return false;
        }
        if (this.group != test.group && this.group != null && !this.group.equals(test.group)) {
            return false;
        }
        if (this.url != test.url && this.url != null && !this.url.equals(test.url)) {
            return false;
        }
        if (this.file != test.file && this.file != null && !this.file.equals((Object)test.file)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.line;
        hash = 17 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 17 * hash + (this.group != null ? this.group.hashCode() : 0);
        hash = 17 * hash + (this.file != null ? this.file.hashCode() : 0);
        hash = 17 * hash + (this.url != null ? this.url.hashCode() : 0);
        return hash;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[");
        buffer.append((Object)(null == this.url ? this.getFile() : this.getURL()));
        buffer.append(", ");
        buffer.append(this.getLine());
        buffer.append(", ");
        buffer.append(this.getDescription());
        buffer.append(", ");
        buffer.append(this.getGroup());
        buffer.append("]");
        return buffer.toString();
    }

    static {
        Accessor.DEFAULT = new AccessorImpl();
    }
}

