/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package org.netbeans.spi.tasklist;

import java.awt.Image;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.Action;
import org.netbeans.modules.tasklist.trampoline.Accessor;
import org.netbeans.modules.tasklist.trampoline.TaskGroup;
import org.netbeans.modules.tasklist.trampoline.TaskManager;
import org.netbeans.spi.tasklist.FileTaskScanner;
import org.netbeans.spi.tasklist.PushTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.netbeans.spi.tasklist.TaskScanningScope;
import org.openide.filesystems.FileObject;

class AccessorImpl
extends Accessor {
    AccessorImpl() {
    }

    @Override
    public String getDescription(Task t) {
        return t.getDescription();
    }

    @Override
    public FileObject getFile(Task t) {
        return t.getFile();
    }

    @Override
    public URL getURL(Task t) {
        return t.getURL();
    }

    @Override
    public TaskGroup getGroup(Task t) {
        return t.getGroup();
    }

    @Override
    public int getLine(Task t) {
        return t.getLine();
    }

    @Override
    public ActionListener getDefaultAction(Task t) {
        return t.getDefaultAction();
    }

    @Override
    public Action[] getActions(Task t) {
        return t.getActions();
    }

    @Override
    public String getDisplayName(TaskScanningScope scope) {
        return scope.getDisplayName();
    }

    @Override
    public String getDescription(TaskScanningScope scope) {
        return scope.getDescription();
    }

    @Override
    public Image getIcon(TaskScanningScope scope) {
        return scope.getIcon();
    }

    @Override
    public boolean isDefault(TaskScanningScope scope) {
        return scope.isDefault();
    }

    @Override
    public String getDisplayName(FileTaskScanner scanner) {
        return scanner.getDisplayName();
    }

    @Override
    public String getDescription(FileTaskScanner scanner) {
        return scanner.getDescription();
    }

    @Override
    public String getOptionsPath(FileTaskScanner scanner) {
        return scanner.getOptionsPath();
    }

    @Override
    public String getDisplayName(PushTaskScanner scanner) {
        return scanner.getDisplayName();
    }

    @Override
    public String getDescription(PushTaskScanner scanner) {
        return scanner.getDescription();
    }

    @Override
    public String getOptionsPath(PushTaskScanner scanner) {
        return scanner.getOptionsPath();
    }

    @Override
    public TaskScanningScope.Callback createCallback(TaskManager tm, TaskScanningScope scope) {
        return new TaskScanningScope.Callback(tm, scope);
    }

    @Override
    public FileTaskScanner.Callback createCallback(TaskManager tm, FileTaskScanner scanner) {
        return new FileTaskScanner.Callback(tm, scanner);
    }

    @Override
    public PushTaskScanner.Callback createCallback(TaskManager tm, PushTaskScanner scanner) {
        return new PushTaskScanner.Callback(tm, scanner);
    }
}

