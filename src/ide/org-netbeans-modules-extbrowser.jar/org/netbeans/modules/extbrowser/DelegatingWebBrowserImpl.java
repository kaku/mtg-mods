/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.awt.EventQueue;
import java.net.URL;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.SimpleExtBrowserImpl;
import org.netbeans.modules.extbrowser.UnixBrowserImpl;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.Utilities;

public class DelegatingWebBrowserImpl
extends ExtBrowserImpl {
    private NbDdeBrowserImpl ddeImpl;
    private UnixBrowserImpl unixImpl;
    private SimpleExtBrowserImpl simpleImpl;

    public DelegatingWebBrowserImpl() {
    }

    public DelegatingWebBrowserImpl(ExtWebBrowser extBrowserFactory) {
        this.extBrowserFactory = extBrowserFactory;
    }

    public ExtBrowserImpl getImplementation() {
        String pName = this.extBrowserFactory.getBrowserExecutable().getProcessName().toUpperCase();
        if (pName != null) {
            if (Utilities.isWindows()) {
                if (pName.indexOf("IEXPLORE.EXE") > -1 || pName.indexOf("NETSCP.EXE") > -1 || pName.indexOf("MOZILLA.EXE") > -1 || pName.indexOf("FIREFOX.EXE") > -1 || pName.indexOf("NETSCAPE.EXE") > -1) {
                    if (this.ddeImpl == null) {
                        this.ddeImpl = new NbDdeBrowserImpl(this.extBrowserFactory);
                    }
                    return this.ddeImpl;
                }
            } else if (Utilities.isUnix() && !Utilities.isMac() && (pName.indexOf("MOZILLA") > -1 || pName.indexOf("NETSCAPE") > -1 || pName.indexOf("FIREFOX") > -1)) {
                if (this.unixImpl == null) {
                    this.unixImpl = new UnixBrowserImpl(this.extBrowserFactory);
                }
                return this.unixImpl;
            }
        }
        if (this.simpleImpl == null) {
            this.simpleImpl = new SimpleExtBrowserImpl(this.extBrowserFactory);
        }
        return this.simpleImpl;
    }

    @Override
    protected void loadURLInBrowserInternal(URL url) {
        assert (!EventQueue.isDispatchThread());
        this.getImplementation().loadURLInBrowserInternal(url);
    }
}

