/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.io.File;
import java.util.logging.Level;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.MacBrowserImpl;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.UnixBrowserImpl;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class FirefoxBrowser
extends ExtWebBrowser {
    private static final long serialVersionUID = -3982770681461437966L;

    public FirefoxBrowser() {
        super(PrivateBrowserFamilyId.FIREFOX);
        this.ddeServer = "FIREFOX";
    }

    public static Boolean isHidden() {
        String detectedPath = null;
        if (Utilities.isWindows()) {
            try {
                detectedPath = NbDdeBrowserImpl.getBrowserPath("FIREFOX");
            }
            catch (NbBrowserException e) {
                ExtWebBrowser.getEM().log(Level.FINEST, "Cannot detect Firefox : " + e);
            }
            if (detectedPath != null && detectedPath.trim().length() > 0) {
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }
        return Utilities.isUnix() ? Boolean.FALSE : Boolean.TRUE;
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(FirefoxBrowser.class, (String)"CTL_FirefoxBrowserName");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        ExtBrowserImpl impl = null;
        if (Utilities.isWindows()) {
            impl = new NbDdeBrowserImpl(this);
        } else if (Utilities.isMac()) {
            impl = new MacBrowserImpl(this);
        } else if (Utilities.isUnix() && !Utilities.isMac()) {
            impl = new UnixBrowserImpl(this);
        } else {
            throw new UnsupportedOperationException(NbBundle.getMessage(FirefoxBrowser.class, (String)"MSG_CannotUseBrowser"));
        }
        return impl;
    }

    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        String params = "";
        if (Utilities.isWindows()) {
            String prg2;
            params = params + "{URL}";
            try {
                String prg2 = NbDdeBrowserImpl.getBrowserPath(this.getDDEServer());
                return new NbProcessDescriptor(prg2, params);
            }
            catch (NbBrowserException e) {
                prg2 = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";
            }
            catch (UnsatisfiedLinkError e) {
                prg2 = "firefox.exe";
            }
            NbProcessDescriptor retValue = new NbProcessDescriptor(prg2, params);
            return retValue;
        }
        if (Utilities.isMac()) {
            params = params + "-a firefox {URL}";
            NbProcessDescriptor retValue = new NbProcessDescriptor("/usr/bin/open", params, ExtWebBrowser.UnixBrowserFormat.getHint());
            return retValue;
        }
        String prg = "firefox";
        if (Utilities.getOperatingSystem() == 16) {
            File f = new File("/usr/bin/firefox");
            if (f.exists()) {
                prg = f.getAbsolutePath();
            }
            if ((f = new File("/usr/bin/mozilla-firefox")).exists()) {
                prg = f.getAbsolutePath();
            }
            if ((f = new File("/usr/local/firefox/firefox")).exists()) {
                prg = f.getAbsolutePath();
            }
        } else if (Utilities.getOperatingSystem() == 8) {
            File f = new File("/usr/sfw/lib/firefox/firefox");
            if (f.exists()) {
                prg = f.getAbsolutePath();
            } else {
                f = new File("/opt/csw/bin/firefox");
                if (f.exists()) {
                    prg = f.getAbsolutePath();
                }
            }
        }
        NbProcessDescriptor retValue = new NbProcessDescriptor(prg, "-remote \"openURL({URL})\"", ExtWebBrowser.UnixBrowserFormat.getHint());
        return retValue;
    }
}

