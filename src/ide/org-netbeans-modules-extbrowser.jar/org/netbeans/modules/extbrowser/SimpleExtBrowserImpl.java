/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.extbrowser;

import java.awt.EventQueue;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.Format;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.extbrowser.BrowserUtils;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.SimpleExtBrowser;
import org.netbeans.modules.extbrowser.URLUtil;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.Exceptions;

public class SimpleExtBrowserImpl
extends ExtBrowserImpl {
    public SimpleExtBrowserImpl(ExtWebBrowser extBrowserFactory) {
        this.extBrowserFactory = extBrowserFactory;
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "SimpleExtBrowserImpl created from factory: " + extBrowserFactory);
        }
    }

    @Override
    protected void loadURLInBrowserInternal(URL url) {
        assert (!EventQueue.isDispatchThread());
        if (url == null) {
            return;
        }
        NbProcessDescriptor np = this.extBrowserFactory.getBrowserExecutable();
        try {
            url = URLUtil.createExternalURL(url, false);
            URI uri = url.toURI();
            if (np != null) {
                np.exec((Format)((Object)new SimpleExtBrowser.BrowserFormat(uri == null ? "" : uri.toASCIIString())));
            }
        }
        catch (URISyntaxException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IOException ex) {
            SimpleExtBrowserImpl.logInfo(ex);
            BrowserUtils.notifyMissingBrowser(np.getProcessName());
        }
    }

    private static void logInfo(Exception ex) {
        Logger logger = Logger.getLogger(SimpleExtBrowserImpl.class.getName());
        logger.log(Level.INFO, null, ex);
    }
}

