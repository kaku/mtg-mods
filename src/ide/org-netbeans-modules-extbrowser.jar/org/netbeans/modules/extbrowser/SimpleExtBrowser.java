/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.MapFormat
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.SimpleExtBrowserImpl;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.MapFormat;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class SimpleExtBrowser
extends ExtWebBrowser {
    private static final long serialVersionUID = -8494345762328555637L;

    public static Boolean isHidden() {
        return Utilities.isWindows() || Utilities.isUnix();
    }

    public SimpleExtBrowser() {
        super(PrivateBrowserFamilyId.UNKNOWN);
        this.browserExecutable = Utilities.getOperatingSystem() == 2048 ? new NbProcessDescriptor("Netscape.exe", " {URL}", NbBundle.getBundle(SimpleExtBrowser.class).getString("MSG_BrowserExecutorHint")) : (Utilities.isMac() ? new NbProcessDescriptor("/usr/bin/open", " {URL}", NbBundle.getBundle(SimpleExtBrowser.class).getString("MSG_BrowserExecutorHint")) : new NbProcessDescriptor("", " {URL}", NbBundle.getBundle(SimpleExtBrowser.class).getString("MSG_BrowserExecutorHint")));
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(SimpleExtBrowser.class, (String)"CTL_SimpleExtBrowser");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        return new SimpleExtBrowserImpl(this);
    }

    public static class BrowserFormat
    extends MapFormat {
        private static final long serialVersionUID = 5990981835151848381L;
        public static final String TAG_URL = "URL";

        public BrowserFormat(String url) {
            super(new HashMap());
            Map map = this.getMap();
            map.put("URL", url);
        }
    }

}

