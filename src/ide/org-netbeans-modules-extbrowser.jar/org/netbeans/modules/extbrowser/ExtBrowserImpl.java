/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package org.netbeans.modules.extbrowser;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;

public abstract class ExtBrowserImpl
extends HtmlBrowser.Impl {
    private static final RequestProcessor RP = new RequestProcessor(ExtBrowserImpl.class);
    private Lookup lookup;
    protected PropertyChangeSupport pcs;
    private URL url;
    protected String title = "";
    protected ExtWebBrowser extBrowserFactory;

    public ExtBrowserImpl() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public PrivateBrowserFamilyId getPrivateBrowserFamilyId() {
        return this.extBrowserFactory.getPrivateBrowserFamilyId();
    }

    protected PrivateBrowserFamilyId detectPrivateBrowserFamilyId() {
        return PrivateBrowserFamilyId.UNKNOWN;
    }

    public boolean isBackward() {
        return false;
    }

    public boolean isForward() {
        return false;
    }

    public void backward() {
    }

    public void forward() {
    }

    public boolean isHistory() {
        return false;
    }

    public void showHistory() {
    }

    public void stopLoading() {
    }

    protected void setTitle(String title) {
    }

    public String getTitle() {
        return "";
    }

    public String getStatusMessage() {
        return "";
    }

    public void reloadDocument() {
        if (this.url == null) {
            return;
        }
        this.setURL(this.url);
    }

    public URL getURL() {
        return this.url;
    }

    public void setURL(URL url) {
        this.loadURLInBrowser(url);
        this.url = url;
    }

    protected final void loadURLInBrowser(final URL url) {
        RP.post(new Runnable(){

            @Override
            public void run() {
                ExtBrowserImpl.this.loadURLInBrowserInternal(url);
            }
        });
    }

    protected abstract void loadURLInBrowserInternal(URL var1);

    public final Component getComponent() {
        return null;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    public final Lookup getLookup() {
        return Lookup.EMPTY;
    }

}

