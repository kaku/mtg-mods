/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.extbrowser;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.extbrowser.SystemDefaultBrowser;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class SystemDefaultBrowserBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        BeanDescriptor descr = new BeanDescriptor(SystemDefaultBrowser.class);
        descr.setDisplayName(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"CTL_SystemDefaultBrowserName"));
        descr.setShortDescription(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"HINT_SystemDefaultBrowserName"));
        descr.setValue("helpID", "org.netbeans.modules.extbrowser.ExtWebBrowser");
        return descr;
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] properties;
        try {
            properties = new PropertyDescriptor[]{new PropertyDescriptor("browserExecutable", SystemDefaultBrowser.class, "getBrowserExecutable", null), new PropertyDescriptor("activateTimeout", SystemDefaultBrowser.class), new PropertyDescriptor("openurlTimeout", SystemDefaultBrowser.class)};
            properties[0].setDisplayName(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"PROP_browserExecutable"));
            properties[0].setShortDescription(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"HINT_browserExecutable"));
            properties[0].setPreferred(true);
            properties[1].setDisplayName(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"PROP_DDE_ACTIVATE_TIMEOUT"));
            properties[1].setShortDescription(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"HINT_DDE_ACTIVATE_TIMEOUT"));
            properties[1].setExpert(Boolean.TRUE);
            properties[1].setHidden(true);
            properties[2].setDisplayName(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"PROP_DDE_OPENURL_TIMEOUT"));
            properties[2].setShortDescription(NbBundle.getMessage(SystemDefaultBrowserBeanInfo.class, (String)"HINT_DDE_OPENURL_TIMEOUT"));
            properties[2].setExpert(Boolean.TRUE);
            properties[2].setHidden(true);
        }
        catch (IntrospectionException ie) {
            Exceptions.printStackTrace((Throwable)ie);
            return null;
        }
        return properties;
    }

    @Override
    public Image getIcon(int type) {
        return this.loadImage("/org/netbeans/modules/extbrowser/resources/extbrowser.png");
    }
}

