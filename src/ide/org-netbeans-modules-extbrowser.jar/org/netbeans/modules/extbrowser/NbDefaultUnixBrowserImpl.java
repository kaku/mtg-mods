/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.annotations.common.CheckForNull
 *  org.netbeans.api.extexecution.ExecutionDescriptor
 *  org.netbeans.api.extexecution.ExecutionDescriptor$InputProcessorFactory
 *  org.netbeans.api.extexecution.ExecutionService
 *  org.netbeans.api.extexecution.ExternalProcessBuilder
 *  org.netbeans.api.extexecution.input.InputProcessor
 *  org.netbeans.api.extexecution.input.InputProcessors
 *  org.netbeans.api.extexecution.input.LineProcessor
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.windows.InputOutput
 */
package org.netbeans.modules.extbrowser;

import java.awt.EventQueue;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.annotations.common.CheckForNull;
import org.netbeans.api.extexecution.ExecutionDescriptor;
import org.netbeans.api.extexecution.ExecutionService;
import org.netbeans.api.extexecution.ExternalProcessBuilder;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.InputProcessors;
import org.netbeans.api.extexecution.input.LineProcessor;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.URLUtil;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;
import org.openide.windows.InputOutput;

class NbDefaultUnixBrowserImpl
extends ExtBrowserImpl {
    private static final Logger LOGGER = Logger.getLogger(NbDefaultUnixBrowserImpl.class.getName());
    private static final String XDG_OPEN_COMMAND = "xdg-open";
    private static final String XDG_SETTINGS_COMMAND = "xdg-settings";
    private static final String XBROWSER_COMMAND = "x-www-browser";
    private static final RequestProcessor REQUEST_PROCESSOR = new RequestProcessor(NbDefaultUnixBrowserImpl.class);
    private static final boolean XDG_OPEN_AVAILABLE = new File("/usr/bin/xdg-open").exists();
    private static final boolean XDG_SETTINGS_AVAILABLE = new File("/usr/bin/xdg-settings").exists();
    private static final boolean XBROWSER_AVAILABLE = new File("/usr/bin/x-www-browser").exists();

    static boolean isAvailable() {
        return XDG_OPEN_AVAILABLE || XBROWSER_AVAILABLE;
    }

    NbDefaultUnixBrowserImpl(ExtWebBrowser extBrowser) {
        this.extBrowserFactory = extBrowser;
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "NbDefaultUnixBrowserImpl created with factory: " + this.extBrowserFactory);
        }
    }

    @Override
    protected PrivateBrowserFamilyId detectPrivateBrowserFamilyId() {
        PrivateBrowserFamilyId browserFamilyId = this.detectBrowserFamily();
        if (browserFamilyId != null) {
            return browserFamilyId;
        }
        return super.detectPrivateBrowserFamilyId();
    }

    @CheckForNull
    private PrivateBrowserFamilyId detectBrowserFamily() {
        String browserIdent = NbDefaultUnixBrowserImpl.detectDefaultWebBrowser();
        if (browserIdent == null && XBROWSER_AVAILABLE) {
            try {
                browserIdent = Paths.get("/usr/bin/x-www-browser", new String[0]).toRealPath(new LinkOption[0]).getFileName().toString().toLowerCase(Locale.US);
            }
            catch (IOException ex) {
                LOGGER.log(Level.INFO, "Could not detect browser", ex);
            }
        }
        if (browserIdent == null) {
            return null;
        }
        if (browserIdent.indexOf("chrome") != -1) {
            return PrivateBrowserFamilyId.CHROME;
        }
        if (browserIdent.indexOf("chromium") != -1) {
            return PrivateBrowserFamilyId.CHROMIUM;
        }
        if (browserIdent.indexOf("firefox") != -1) {
            return PrivateBrowserFamilyId.FIREFOX;
        }
        if (browserIdent.indexOf("opera") != -1) {
            return PrivateBrowserFamilyId.OPERA;
        }
        if (browserIdent.indexOf("mozilla") != -1) {
            return PrivateBrowserFamilyId.MOZILLA;
        }
        return null;
    }

    @Override
    protected void loadURLInBrowserInternal(URL url) {
        assert (!EventQueue.isDispatchThread());
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "NbDeaultUnixBrowserImpl.setUrl: " + url);
        }
        url = URLUtil.createExternalURL(url, false);
        String urlArg = url.toExternalForm();
        String command = XDG_OPEN_AVAILABLE ? "xdg-open" : "x-www-browser";
        ProcessBuilder pb = new ProcessBuilder(command, urlArg);
        try {
            Process p = pb.start();
            REQUEST_PROCESSOR.post((Runnable)new ProcessWatcher(p));
        }
        catch (IOException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private static String detectDefaultWebBrowser() {
        if (!XDG_SETTINGS_AVAILABLE) {
            return null;
        }
        OutputProcessorFactory outputProcessorFactory = new OutputProcessorFactory();
        ExternalProcessBuilder processBuilder = new ExternalProcessBuilder("xdg-settings").addArgument("get").addArgument("default-web-browser");
        ExecutionDescriptor silentDescriptor = new ExecutionDescriptor().inputOutput(InputOutput.NULL).inputVisible(false).frontWindow(false).showProgress(false).outProcessorFactory((ExecutionDescriptor.InputProcessorFactory)outputProcessorFactory);
        Future result = ExecutionService.newService((Callable)processBuilder, (ExecutionDescriptor)silentDescriptor, (String)"Detecting default web browser").run();
        try {
            result.get(10, TimeUnit.SECONDS);
        }
        catch (InterruptedException | ExecutionException | TimeoutException ex) {
            LOGGER.log(Level.INFO, null, ex);
        }
        String output = outputProcessorFactory.getOutput();
        if (output == null) {
            return null;
        }
        return output.toLowerCase(Locale.US);
    }

    private static void cleanupProcess(Process p) {
        NbDefaultUnixBrowserImpl.closeStream(p.getOutputStream());
        NbDefaultUnixBrowserImpl.closeStream(p.getInputStream());
        NbDefaultUnixBrowserImpl.closeStream(p.getErrorStream());
        p.destroy();
    }

    private static void closeStream(Closeable stream) {
        try {
            stream.close();
        }
        catch (IOException ioe) {
            NbDefaultUnixBrowserImpl.log(ioe);
        }
    }

    private static void log(Exception e) {
        Logger.getLogger(NbDefaultUnixBrowserImpl.class.getName()).log(Level.INFO, null, e);
    }

    static final class OutputProcessorFactory
    implements ExecutionDescriptor.InputProcessorFactory {
        private volatile String output;

        OutputProcessorFactory() {
        }

        public InputProcessor newInputProcessor(InputProcessor defaultProcessor) {
            return InputProcessors.bridge((LineProcessor)new LineProcessor(){

                public void processLine(String line) {
                    assert (OutputProcessorFactory.this.output == null);
                    OutputProcessorFactory.this.output = line;
                }

                public void reset() {
                }

                public void close() {
                }
            });
        }

        public String getOutput() {
            return this.output;
        }

    }

    private static final class ProcessWatcher
    implements Runnable {
        private final Process p;

        ProcessWatcher(Process p) {
            this.p = p;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            block8 : {
                try {
                    int exitValue = this.p.waitFor();
                    if (exitValue == 0) break block8;
                    StringBuilder sb = new StringBuilder();
                    InputStream is = this.p.getErrorStream();
                    try {
                        int curByte = 0;
                        while ((curByte = is.read()) != -1) {
                            sb.append((char)curByte);
                        }
                    }
                    catch (IOException ex) {
                        Exceptions.printStackTrace((Throwable)ex);
                    }
                    ExtWebBrowser.getEM().log(Level.WARNING, sb.toString());
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                finally {
                    NbDefaultUnixBrowserImpl.cleanupProcess(this.p);
                }
            }
        }
    }

}

