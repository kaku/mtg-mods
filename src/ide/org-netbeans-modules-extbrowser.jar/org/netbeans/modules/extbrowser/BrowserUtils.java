/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.util.ImageUtilities
 */
package org.netbeans.modules.extbrowser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import org.netbeans.api.options.OptionsDisplayer;
import org.netbeans.modules.extbrowser.Bundle;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;

public final class BrowserUtils {
    private static final String BROWSER_ICON = "org/netbeans/modules/extbrowser/resources/browser_generic_16x.png";

    private BrowserUtils() {
    }

    public static void notifyMissingBrowser(String browserName) {
        NotificationDisplayer.getDefault().notify(Bundle.BrowserUtils_cannot_run_title(browserName), (Icon)ImageUtilities.loadImageIcon((String)"org/netbeans/modules/extbrowser/resources/browser_generic_16x.png", (boolean)false), Bundle.BrowserUtils_cannot_run_detail(), new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                OptionsDisplayer.getDefault().open("General");
            }
        });
    }

}

