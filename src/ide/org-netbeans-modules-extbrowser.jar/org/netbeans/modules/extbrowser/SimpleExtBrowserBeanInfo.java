/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.extbrowser;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.extbrowser.SimpleExtBrowser;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class SimpleExtBrowserBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        BeanDescriptor descr = new BeanDescriptor(SimpleExtBrowser.class);
        descr.setDisplayName(NbBundle.getMessage(SimpleExtBrowserBeanInfo.class, (String)"CTL_SimpleExtBrowser"));
        descr.setShortDescription(NbBundle.getMessage(SimpleExtBrowserBeanInfo.class, (String)"HINT_SimpleExtBrowser"));
        descr.setValue("helpID", "org.netbeans.modules.extbrowser.SimpleExtBrowser");
        return descr;
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] properties;
        try {
            properties = new PropertyDescriptor[]{new PropertyDescriptor("browserExecutable", SimpleExtBrowser.class)};
            properties[0].setDisplayName(NbBundle.getMessage(SimpleExtBrowserBeanInfo.class, (String)"PROP_browserExecutable"));
            properties[0].setShortDescription(NbBundle.getMessage(SimpleExtBrowserBeanInfo.class, (String)"HINT_browserExecutable"));
        }
        catch (IntrospectionException ie) {
            Exceptions.printStackTrace((Throwable)ie);
            return null;
        }
        return properties;
    }

    @Override
    public Image getIcon(int type) {
        return this.loadImage("/org/netbeans/modules/extbrowser/resources/extbrowser.gif");
    }
}

