/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.beans.PropertyChangeListener;
import java.io.File;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.FirefoxBrowser;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.UnixBrowserImpl;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class ChromiumBrowser
extends ExtWebBrowser
implements PropertyChangeListener {
    private static final long serialVersionUID = -2700536003661091286L;
    private static final String CHROMIUM_PATH = "/usr/bin/chromium-browser";
    private static final String CHROMIUM_PATH2 = "/usr/bin/chromium";

    public ChromiumBrowser() {
        super(PrivateBrowserFamilyId.CHROMIUM);
        this.ddeServer = "CHROMIUM";
    }

    public static Boolean isHidden() {
        File file = null;
        if (Utilities.isUnix() && !Utilities.isMac()) {
            file = new File("/usr/bin/chromium-browser");
            if (!file.exists()) {
                file = new File("/usr/bin/chromium");
            }
        } else if (Utilities.isWindows()) {
            file = ChromiumBrowser.getLocalAppPath();
        }
        return file == null || !file.exists() || !file.canExecute();
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(ChromiumBrowser.class, (String)"CTL_ChromiumBrowserName");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        ExtBrowserImpl impl = null;
        if (Utilities.isUnix() && !Utilities.isMac()) {
            impl = new UnixBrowserImpl(this);
        } else if (Utilities.isWindows()) {
            impl = new NbDdeBrowserImpl(this);
        } else {
            throw new UnsupportedOperationException(NbBundle.getMessage(FirefoxBrowser.class, (String)"MSG_CannotUseBrowser"));
        }
        return impl;
    }

    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        File file = null;
        if (Utilities.isUnix() && !Utilities.isMac()) {
            file = new File("/usr/bin/chromium-browser");
            if (!file.exists()) {
                file = new File("/usr/bin/chromium");
            }
        } else if (Utilities.isWindows()) {
            file = ChromiumBrowser.getLocalAppPath();
        }
        if (file != null && file.exists()) {
            return new NbProcessDescriptor(file.getAbsolutePath(), "{URL}", NbBundle.getMessage(ChromiumBrowser.class, (String)"MSG_BrowserExecutorHint"));
        }
        return null;
    }

    private static File getLocalAppPath() {
        String localFiles = System.getenv("LOCALAPPDATA");
        String chrome = localFiles + "\\Chromium\\Application\\chrome.exe";
        return new File(chrome);
    }
}

