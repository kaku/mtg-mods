/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.MacBrowserImpl;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.NbDefaultUnixBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.URLUtil;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class SystemDefaultBrowser
extends ExtWebBrowser {
    private static final long serialVersionUID = -7317179197254112564L;
    private static final Logger logger = Logger.getLogger(SystemDefaultBrowser.class.getName());
    private static RequestProcessor RP = new RequestProcessor(SystemDefaultBrowser.class.getName(), 3);
    private transient AtomicBoolean detected = new AtomicBoolean(false);
    private static final boolean ACTIVE;

    public static Boolean isHidden() {
        return !Utilities.isWindows() && !SystemDefaultBrowser.defaultBrowserUnixReady() && !Utilities.isMac() && !ACTIVE;
    }

    private static boolean defaultBrowserUnixReady() {
        return Utilities.isUnix() && NbDefaultUnixBrowserImpl.isAvailable();
    }

    public SystemDefaultBrowser() {
        super(PrivateBrowserFamilyId.UNKNOWN);
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        if (ACTIVE) {
            return new Jdk6BrowserImpl();
        }
        if (Utilities.isWindows()) {
            return new NbDdeBrowserImpl(this);
        }
        if (Utilities.isMac()) {
            return new MacBrowserImpl(this);
        }
        if (Utilities.isUnix() && !Utilities.isMac()) {
            return new NbDefaultUnixBrowserImpl(this);
        }
        throw new UnsupportedOperationException(NbBundle.getMessage(SystemDefaultBrowser.class, (String)"MSG_CannotUseBrowser"));
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(SystemDefaultBrowser.class, (String)"CTL_SystemDefaultBrowserName");
        }
        return this.name;
    }

    @Override
    public void setName(String name) {
    }

    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        String b;
        if (Utilities.isMac()) {
            return new NbProcessDescriptor("/usr/bin/open", "{URL}", ExtWebBrowser.UnixBrowserFormat.getHint());
        }
        if (!Utilities.isWindows() || ACTIVE) {
            return new NbProcessDescriptor("", "");
        }
        String params = "";
        try {
            b = NbDdeBrowserImpl.getDefaultOpenCommand();
            String[] args = Utilities.parseParameters((String)b);
            if (args == null || args.length == 0) {
                throw new NbBrowserException();
            }
            b = args[0];
            params = params + " {URL}";
        }
        catch (NbBrowserException e) {
            b = "";
        }
        catch (UnsatisfiedLinkError e) {
            b = "iexplore";
        }
        NbProcessDescriptor p = new NbProcessDescriptor(b, params, ExtWebBrowser.UnixBrowserFormat.getHint());
        return p;
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        this.detected = new AtomicBoolean(false);
    }

    @Override
    public PrivateBrowserFamilyId getPrivateBrowserFamilyId() {
        this.detectSystemDefaultBrowser();
        return super.getPrivateBrowserFamilyId();
    }

    private synchronized void detectSystemDefaultBrowser() {
        ExtBrowserImpl extImpl;
        if (this.detected.getAndSet(true)) {
            return;
        }
        HtmlBrowser.Impl impl = this.createHtmlBrowserImpl();
        ExtBrowserImpl extBrowserImpl = extImpl = impl != null && impl instanceof ExtBrowserImpl ? (ExtBrowserImpl)impl : null;
        if (extImpl != null) {
            RP.post(new Runnable(){

                @Override
                public void run() {
                    SystemDefaultBrowser.this.setPrivateBrowserFamilyId(extImpl.detectPrivateBrowserFamilyId());
                }
            });
        }
    }

    static {
        if (Boolean.getBoolean("org.netbeans.modules.extbrowser.UseDesktopBrowse")) {
            if (Boolean.getBoolean("java.net.useSystemProxies") && Utilities.isUnix()) {
                logger.log(Level.FINE, "Ignoring java.awt.Desktop.browse support to avoid hang from #89540");
                ACTIVE = false;
            } else {
                ACTIVE = Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE);
            }
        } else {
            ACTIVE = false;
        }
    }

    private static final class Jdk6BrowserImpl
    extends ExtBrowserImpl {
        public Jdk6BrowserImpl() {
            assert (ACTIVE);
        }

        @Override
        protected void loadURLInBrowserInternal(URL url) {
            assert (!EventQueue.isDispatchThread());
            URL extURL = URLUtil.createExternalURL(url, false);
            try {
                URI uri = extURL.toURI();
                logger.log(Level.FINE, "Calling java.awt.Desktop.browse({0})", uri);
                Desktop.getDesktop().browse(uri);
            }
            catch (URISyntaxException e) {
                logger.log(Level.SEVERE, "The URL:\n{0}\nis not fully RFC 2396 compliant and cannot be used with Desktop.browse().", extURL);
            }
            catch (IOException e) {
                logger.log(Level.WARNING, null, e);
            }
        }
    }

}

