/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.logging.Level;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.FirefoxBrowser;
import org.netbeans.modules.extbrowser.MacBrowserImpl;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.UnixBrowserImpl;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class ChromeBrowser
extends ExtWebBrowser
implements PropertyChangeListener {
    private static final long serialVersionUID = -4553174676787993831L;

    public ChromeBrowser() {
        super(PrivateBrowserFamilyId.CHROME);
        this.ddeServer = "CHROME";
    }

    public static Boolean isHidden() {
        if (Utilities.isWindows()) {
            String detectedPath = ChromeBrowser.getLocalAppPath().getPath();
            try {
                if (detectedPath == null) {
                    detectedPath = NbDdeBrowserImpl.getBrowserPath("chrome");
                }
            }
            catch (NbBrowserException e) {
                ExtWebBrowser.getEM().log(Level.FINEST, "Cannot detect chrome : " + e);
            }
            if (detectedPath != null && detectedPath.trim().length() > 0) {
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(ChromeBrowser.class, (String)"CTL_ChromeBrowserName");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        ExtBrowserImpl impl = null;
        if (Utilities.isWindows()) {
            impl = new NbDdeBrowserImpl(this);
        } else if (Utilities.isMac()) {
            impl = new MacBrowserImpl(this);
        } else if (Utilities.isUnix() && !Utilities.isMac()) {
            impl = new UnixBrowserImpl(this);
        } else {
            throw new UnsupportedOperationException(NbBundle.getMessage(FirefoxBrowser.class, (String)"MSG_CannotUseBrowser"));
        }
        return impl;
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        String params = "";
        NbProcessDescriptor retValue = null;
        if (Utilities.isWindows()) {
            params = params + "{URL}";
            File file = ChromeBrowser.getLocalAppPath();
            if (file.exists() && file.canExecute()) {
                this.setDDEServer("CHROME");
                return new NbProcessDescriptor(file.getPath(), params);
            }
            try {
                try {
                    String b = NbDdeBrowserImpl.getBrowserPath("chrome");
                    if (b == null) return retValue;
                    if (b.trim().length() <= 0) return retValue;
                    this.setDDEServer("CHROME");
                    return new NbProcessDescriptor(b, params);
                }
                catch (NbBrowserException e) {
                    File chrome;
                    if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                        ExtWebBrowser.getEM().log(Level.FINE, "Cannot get Path for Chrome: " + e);
                    }
                    if (!(chrome = new File("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe")).isFile()) {
                        chrome = new File("C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe");
                    }
                    if (!chrome.isFile()) return retValue;
                    if (!chrome.canExecute()) return retValue;
                    this.setDDEServer("CHROME");
                    return new NbProcessDescriptor(chrome.getPath(), params);
                }
            }
            catch (UnsatisfiedLinkError e) {
                if (!ExtWebBrowser.getEM().isLoggable(Level.FINE)) return retValue;
                ExtWebBrowser.getEM().log(Level.FINE, "Some problem here:" + e);
                return retValue;
            }
        } else {
            if (Utilities.isMac()) {
                params = params + "-b com.google.chrome {URL}";
                return new NbProcessDescriptor("/usr/bin/open", params, ExtWebBrowser.UnixBrowserFormat.getHint());
            }
            boolean found = false;
            String b = "chrome";
            File f = new File("/usr/local/bin/google-chrome");
            if (f.exists()) {
                found = true;
                b = f.getAbsolutePath();
            }
            f = new File("/usr/bin/google-chrome");
            if (!found && f.exists()) {
                found = true;
                b = f.getAbsolutePath();
            }
            f = new File("/opt/google/chrome/google-chrome");
            if (!found && f.exists()) {
                found = true;
                b = f.getAbsolutePath();
            }
            f = new File("/opt/google/chrome/chrome");
            if (found) return new NbProcessDescriptor(b, "{URL}", NbBundle.getMessage(ChromeBrowser.class, (String)"MSG_BrowserExecutorHint"));
            if (!f.exists()) return new NbProcessDescriptor(b, "{URL}", NbBundle.getMessage(ChromeBrowser.class, (String)"MSG_BrowserExecutorHint"));
            found = true;
            b = f.getAbsolutePath();
            return new NbProcessDescriptor(b, "{URL}", NbBundle.getMessage(ChromeBrowser.class, (String)"MSG_BrowserExecutorHint"));
        }
    }

    private static File getLocalAppPath() {
        String localFiles = System.getenv("LOCALAPPDATA");
        String chrome = localFiles + "\\Google\\Chrome\\Application\\chrome.exe";
        return new File(chrome);
    }
}

