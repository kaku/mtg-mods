/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.awt.EventQueue;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.Format;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.extbrowser.BrowserUtils;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.URLUtil;
import org.openide.awt.StatusDisplayer;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;

public class UnixBrowserImpl
extends ExtBrowserImpl {
    protected static final int CMD_TIMEOUT = 6;
    private static RequestProcessor RP = new RequestProcessor();

    protected static NbProcessDescriptor createPatchedExecutable(NbProcessDescriptor p) {
        NbProcessDescriptor newP = null;
        String[] args = Utilities.parseParameters((String)p.getArguments());
        if (args.length > 1) {
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "Old arguments: " + p.getArguments());
            }
            StringBuffer newArgs = new StringBuffer();
            boolean found = false;
            for (int i = 0; i < args.length - 1; ++i) {
                if (newArgs.length() > 0) {
                    newArgs.append(" ");
                }
                if (args[i].indexOf("-remote") >= 0 && args[i + 1].indexOf("openURL(") >= 0) {
                    found = true;
                    newArgs.append("\"{URL}\"");
                    continue;
                }
                newArgs.append("\"" + args[i] + "\"");
            }
            if (found) {
                newP = new NbProcessDescriptor(p.getProcessName(), newArgs.toString(), p.getInfo());
            }
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "ProcessName: " + p.getProcessName());
                ExtWebBrowser.getEM().log(Level.FINE, "New arguments: " + newArgs.toString());
            }
        }
        return newP;
    }

    public UnixBrowserImpl() {
        this(null);
    }

    public UnixBrowserImpl(ExtWebBrowser extBrowserFactory) {
        this.extBrowserFactory = extBrowserFactory;
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "UnixBrowserImpl created from factory: " + extBrowserFactory);
        }
    }

    @Override
    protected void loadURLInBrowserInternal(URL url) {
        assert (!EventQueue.isDispatchThread());
        NbProcessDescriptor cmd = this.extBrowserFactory.getBrowserExecutable();
        StatusDisplayer sd = StatusDisplayer.getDefault();
        try {
            url = URLUtil.createExternalURL(url, false);
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "External url: " + url);
            }
            cmd = this.extBrowserFactory.getBrowserExecutable();
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "Executable: " + (Object)cmd);
            }
            sd.setStatusText(NbBundle.getMessage(UnixBrowserImpl.class, (String)"MSG_Running_command", (Object)cmd.getProcessName()));
            Process p = cmd.exec((Format)((Object)new ExtWebBrowser.UnixBrowserFormat(url.toString())));
            RP.post((Runnable)new Status(cmd, p, url), 1000);
            this.pcs.firePropertyChange("url", this.getURL(), url);
        }
        catch (IOException ex) {
            ExtWebBrowser.getEM().log(Level.INFO, null, ex);
            BrowserUtils.notifyMissingBrowser(cmd.getProcessName());
        }
        catch (NumberFormatException ex) {
            Logger.getLogger("global").log(Level.INFO, null, ex);
        }
        catch (Exception ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
    }

    private class Status
    implements Runnable {
        private static final String FAILURE_MSG_BADWINDOW = "BadWindow";
        private static final String FAILURE_MSG = "No running window found.";
        private NbProcessDescriptor cmd;
        private Process p;
        private URL url;
        private int retries;

        public Status(NbProcessDescriptor cmd, Process p, URL url) {
            this.retries = 6;
            this.cmd = cmd;
            this.p = p;
            this.url = url;
        }

        @Override
        public void run() {
            boolean retried;
            char[] buff;
            InputStreamReader r;
            int l;
            int exitStatus;
            StringBuffer sb;
            block25 : {
                try {
                    this.p.waitFor();
                }
                catch (InterruptedException ex) {
                    Exceptions.printStackTrace((Throwable)ex);
                }
                retried = false;
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "Retried: " + retried);
                }
                exitStatus = 1;
                r = new InputStreamReader(this.p.getErrorStream());
                try {
                    exitStatus = this.p.exitValue();
                    if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                        ExtWebBrowser.getEM().log(Level.FINE, "Command executed. exitValue = " + exitStatus);
                    }
                }
                catch (IllegalThreadStateException ex) {
                    --this.retries;
                    if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                        ExtWebBrowser.getEM().log(Level.FINE, "Retries: " + this.retries);
                        ExtWebBrowser.getEM().log(Level.FINE, "Time: " + System.currentTimeMillis());
                    }
                    if (this.retries > 0) {
                        RP.post((Runnable)this, 1000);
                        return;
                    }
                    if (!ExtWebBrowser.getEM().isLoggable(Level.FINE)) break block25;
                    ExtWebBrowser.getEM().log(Level.FINE, "Command not finished yet");
                }
            }
            if (exitStatus == 0 && Utilities.getOperatingSystem() == 16) {
                int LEN = 2048;
                buff = new char[2048];
                sb = new StringBuffer();
                try {
                    while ((l = r.read(buff, 0, 2048)) != -1) {
                        sb.append(buff, 0, l);
                    }
                    if (sb.toString().indexOf("No running window found.") >= 0) {
                        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                            ExtWebBrowser.getEM().log(Level.FINE, "Browser output: \"No running window found.\"");
                        }
                        exitStatus = 2;
                    }
                }
                catch (IOException ioe) {
                    ExtWebBrowser.getEM().log(Level.WARNING, null, ioe);
                }
            }
            if (exitStatus == 1 && Utilities.getOperatingSystem() == 16) {
                int LEN = 2048;
                buff = new char[2048];
                sb = new StringBuffer();
                try {
                    while ((l = r.read(buff, 0, 2048)) != -1) {
                        sb.append(buff, 0, l);
                    }
                    if (sb.toString().indexOf("BadWindow") >= 0) {
                        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                            ExtWebBrowser.getEM().log(Level.FINE, "Browser output: \"BadWindow\"");
                        }
                        exitStatus = 0;
                    }
                }
                catch (IOException ioe) {
                    ExtWebBrowser.getEM().log(Level.WARNING, null, ioe);
                }
            }
            if (exitStatus == 2) {
                try {
                    NbProcessDescriptor startCmd = UnixBrowserImpl.createPatchedExecutable(this.cmd);
                    if (startCmd != null) {
                        retried = true;
                        StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(UnixBrowserImpl.class, (String)"MSG_Running_command", (Object)startCmd.getProcessName()));
                        Process pr = startCmd.exec((Format)((Object)new ExtWebBrowser.UnixBrowserFormat(this.url.toString())));
                    }
                }
                catch (IOException ioe) {
                    ExtWebBrowser.getEM().log(Level.WARNING, null, ioe);
                }
            }
            if (exitStatus != 0 && !retried && exitStatus != 23) {
                BrowserUtils.notifyMissingBrowser(this.cmd.getProcessName());
                return;
            }
        }
    }

}

