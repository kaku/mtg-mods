/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.MacBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class SafariBrowser
extends ExtWebBrowser {
    private static final long serialVersionUID = -1;

    public SafariBrowser() {
        super(PrivateBrowserFamilyId.SAFARI);
    }

    public static Boolean isHidden() {
        return Utilities.isMac() ? Boolean.FALSE : Boolean.TRUE;
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(SafariBrowser.class, (String)"CTL_SafariBrowserName");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        MacBrowserImpl impl = null;
        if (!Utilities.isMac()) {
            throw new UnsupportedOperationException(NbBundle.getMessage(SafariBrowser.class, (String)"MSG_CannotUseBrowser"));
        }
        impl = new MacBrowserImpl(this);
        return impl;
    }

    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        return new NbProcessDescriptor("/usr/bin/open", "-a safari {URL}", ExtWebBrowser.UnixBrowserFormat.getHint());
    }
}

