/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.extbrowser;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.Format;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.extbrowser.BrowserUtils;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.SimpleExtBrowser;
import org.netbeans.modules.extbrowser.URLUtil;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.Exceptions;

public class MacBrowserImpl
extends ExtBrowserImpl {
    public MacBrowserImpl(ExtWebBrowser extBrowserFactory) {
        this.extBrowserFactory = extBrowserFactory;
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "MacBrowserImpl created from factory: " + extBrowserFactory);
        }
    }

    @Override
    protected void loadURLInBrowserInternal(URL url) {
        assert (!EventQueue.isDispatchThread());
        if (url == null) {
            return;
        }
        NbProcessDescriptor np = this.extBrowserFactory.getBrowserExecutable();
        try {
            url = URLUtil.createExternalURL(url, false);
            URI uri = url.toURI();
            if (np != null) {
                np.exec((Format)((Object)new SimpleExtBrowser.BrowserFormat(uri == null ? "" : uri.toASCIIString())));
            }
        }
        catch (URISyntaxException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IOException ex) {
            MacBrowserImpl.logInfo(ex);
            BrowserUtils.notifyMissingBrowser(np.getProcessName());
        }
    }

    private static void logInfo(Exception ex) {
        Logger logger = Logger.getLogger(MacBrowserImpl.class.getName());
        logger.log(Level.INFO, null, ex);
    }

    @Override
    protected PrivateBrowserFamilyId detectPrivateBrowserFamilyId() {
        PrivateBrowserFamilyId pluginId = super.detectPrivateBrowserFamilyId();
        if (pluginId != PrivateBrowserFamilyId.UNKNOWN) {
            return pluginId;
        }
        String defaultApps = this.getDefaultApps();
        if (pluginId == null || pluginId == PrivateBrowserFamilyId.UNKNOWN) {
            pluginId = MacBrowserImpl.getPrivateBrowserFamilyIdFromDefaultApps(defaultApps);
            if (pluginId == null) {
                pluginId = PrivateBrowserFamilyId.UNKNOWN;
            }
            return pluginId;
        }
        return pluginId;
    }

    static PrivateBrowserFamilyId getPrivateBrowserFamilyIdFromDefaultApps(String defaultApps) {
        return MacBrowserImpl.parseDefaultApps(defaultApps, "LSHandlerContentType", "public.html");
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    private static PrivateBrowserFamilyId parseDefaultApps(String defaultApps, String key, String value) {
        if (defaultApps == null) {
            return null;
        }
        index = 0;
        do lbl-1000: // 3 sources:
        {
            if ((index = defaultApps.indexOf(value, index + 1)) == -1) {
                return null;
            }
            lBrace = defaultApps.substring(0, index).lastIndexOf(123);
            rBrace = defaultApps.indexOf(125, index);
            if (lBrace == -1) return null;
            if (rBrace == -1) {
                return null;
            }
            valueIndex = defaultApps.indexOf(key, lBrace);
            if (valueIndex == -1 || valueIndex >= index) ** GOTO lbl-1000
            chromeIndex = defaultApps.indexOf("chrome", lBrace);
            if (chromeIndex < rBrace) {
                return PrivateBrowserFamilyId.CHROME;
            }
            firefoxIndex = defaultApps.indexOf("firefox", lBrace);
            if (firefoxIndex < rBrace) {
                return PrivateBrowserFamilyId.FIREFOX;
            }
            safariIndex = defaultApps.indexOf("safari", lBrace);
            if (safariIndex >= rBrace) continue;
            return PrivateBrowserFamilyId.SAFARI;
        } while ((operaIndex = defaultApps.indexOf("opera", lBrace)) >= rBrace);
        return PrivateBrowserFamilyId.OPERA;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String getDefaultApps() {
        BufferedReader reader = null;
        try {
            String line;
            Process process = Runtime.getRuntime().exec("defaults read com.apple.LaunchServices");
            process.waitFor();
            InputStream inputStream = process.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                if (line.trim().length() == 0) continue;
                builder.append(line);
            }
            String string = builder.toString();
            return string;
        }
        catch (Exception ex) {
            Logger.getLogger(MacBrowserImpl.class.getCanonicalName()).log(Level.INFO, "Unable to run process: 'defaults read com.apple.LaunchServices'", ex);
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                }
                catch (IOException ex) {
                    Logger.getLogger(MacBrowserImpl.class.getCanonicalName()).log(Level.INFO, "Unable close process input stream reader ", ex);
                }
            }
        }
        return null;
    }
}

