/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extbrowser;

public class NbBrowserException
extends Exception {
    private static final long serialVersionUID = 8883382856731051881L;

    public NbBrowserException() {
    }

    public NbBrowserException(String msg) {
        super(msg);
    }
}

