/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.URLMapper
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 */
package org.netbeans.modules.extbrowser;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Lookup;

public class URLUtil {
    private static Lookup.Result result = Lookup.getDefault().lookup(new Lookup.Template(URLMapper.class));

    public static URL createExternalURL(URL url, boolean allowJar) {
        if (url == null) {
            return null;
        }
        URL compliantURL = URLUtil.getFullyRFC2396CompliantURL(url);
        if (URLUtil.isAcceptableProtocol(compliantURL, allowJar)) {
            return compliantURL;
        }
        String anchor = compliantURL.getRef();
        String urlString = compliantURL.toString();
        int ind = urlString.indexOf(35);
        if (ind >= 0) {
            urlString = urlString.substring(0, ind);
        }
        try {
            URL newUrl;
            FileObject fo = URLMapper.findFileObject((URL)new URL(urlString));
            if (fo != null && (newUrl = URLUtil.getURLOfAppropriateType(fo, allowJar)) != null) {
                urlString = newUrl.toString();
                if (ind >= 0) {
                    urlString = urlString + "#" + anchor;
                }
                return new URL(urlString);
            }
        }
        catch (MalformedURLException e) {
            Logger.getLogger("global").log(Level.INFO, null, e);
        }
        return compliantURL;
    }

    private static URL getFullyRFC2396CompliantURL(URL url) {
        String urlStr = url.toString();
        int ind = urlStr.indexOf(35);
        if (ind > -1) {
            String urlWithoutRef = urlStr.substring(0, ind);
            String anchorOrg = url.getRef();
            try {
                String anchorEscaped = URLEncoder.encode(URLDecoder.decode(anchorOrg, "UTF8"), "UTF8");
                anchorEscaped = anchorEscaped.replaceAll("\\+", "%20");
                if (!anchorOrg.equals(anchorEscaped)) {
                    URL escapedURL = new URL(urlWithoutRef + '#' + anchorEscaped);
                    Logger.getLogger("global").warning("The URL:\n" + urlStr + "\nis not fully RFC 2396 compliant and cannot " + "be used with Desktop.browse(). Instead using URL:" + escapedURL);
                    return escapedURL;
                }
            }
            catch (IOException e) {
                Logger.getLogger("global").log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return url;
    }

    private static URL getURLOfAppropriateType(FileObject fo, boolean allowJar) {
        URL suitable = null;
        for (URLMapper mapper : result.allInstances()) {
            URL retVal = mapper.getURL(fo, 1);
            if (retVal == null || !URLUtil.isAcceptableProtocol(retVal, allowJar)) continue;
            String p = retVal.getProtocol().toLowerCase();
            if ("file".equals(p) || "jar".equals(p)) {
                return retVal;
            }
            suitable = retVal;
        }
        if (suitable != null) {
            return suitable;
        }
        URL url = URLMapper.findURL((FileObject)fo, (int)2);
        if (url == null) {
            Logger.getLogger("global").log(Level.SEVERE, "URLMapper.findURL() failed for " + (Object)fo);
            return null;
        }
        return URLUtil.makeURLLocal(url);
    }

    private static URL makeURLLocal(URL input) {
        String host = input.getHost();
        try {
            if (host.equals(InetAddress.getLocalHost().getHostName())) {
                host = "127.0.0.1";
                return new URL(input.getProtocol(), host, input.getPort(), input.getFile());
            }
            return input;
        }
        catch (UnknownHostException e) {
            return input;
        }
        catch (MalformedURLException e) {
            return input;
        }
    }

    private static boolean isAcceptableProtocol(URL url, boolean allowJar) {
        String urlString;
        String protocol = url.getProtocol().toLowerCase();
        if ("http".equals(protocol) || "https".equals(protocol) || "ftp".equals(protocol) || "file".equals(protocol)) {
            return true;
        }
        if (allowJar && "jar".equals(protocol) && !(urlString = url.toString()).toLowerCase().startsWith("jar:nbinst:")) {
            return true;
        }
        return false;
    }

    public static boolean browserHandlesJarURLs(String browser) {
        return "MOZILLA".equals(browser) || "FIREFOX".equals(browser);
    }
}

