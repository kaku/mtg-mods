/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Factory
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.MapFormat
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.modules.extbrowser.ChromeBrowser;
import org.netbeans.modules.extbrowser.ChromiumBrowser;
import org.netbeans.modules.extbrowser.DelegatingWebBrowserImpl;
import org.netbeans.modules.extbrowser.FirefoxBrowser;
import org.netbeans.modules.extbrowser.IExplorerBrowser;
import org.netbeans.modules.extbrowser.MozillaBrowser;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.SafariBrowser;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.MapFormat;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class ExtWebBrowser
implements HtmlBrowser.Factory,
Serializable,
PropertyChangeListener {
    private static final long serialVersionUID = -3021027901671504127L;
    public static final String PROP_NAME = "name";
    public static final String PROP_BROWSER_EXECUTABLE = "browserExecutable";
    public static final String PROP_DDESERVER = "dDEServer";
    public static final String PROP_DDE_ACTIVATE_TIMEOUT = "activateTimeout";
    public static final String PROP_DDE_OPENURL_TIMEOUT = "openurlTimeout";
    public static final String PROP_PRIVATE_BROWSER_FAMILY = "privateBrowserFamilyId";
    public static final String CHROME = "CHROME";
    public static final String CHROMIUM = "CHROMIUM";
    public static final String IEXPLORE = "IEXPLORE";
    public static final String MOZILLA = "MOZILLA";
    public static final String FIREFOX = "FIREFOX";
    protected static final int DEFAULT_ACTIVATE_TIMEOUT = 2000;
    protected static final int DEFAULT_OPENURL_TIMEOUT = 3000;
    protected String ddeServer;
    protected int activateTimeout = 2000;
    protected int openurlTimeout = 3000;
    private static final Logger err = Logger.getLogger("org.netbeans.modules.extbrowser");
    protected String name;
    private PrivateBrowserFamilyId family;
    protected NbProcessDescriptor browserExecutable;
    protected transient PropertyChangeSupport pcs;
    private ExtWebBrowser browserExecutableDelegate = null;

    public static Logger getEM() {
        return err;
    }

    public ExtWebBrowser() {
        this(PrivateBrowserFamilyId.UNKNOWN);
    }

    public ExtWebBrowser(PrivateBrowserFamilyId family) {
        this.family = family;
        this.init();
    }

    private void init() {
        if (err.isLoggable(Level.FINE)) {
            err.log(Level.FINE, this.getClass().getName() + " " + System.currentTimeMillis() + "> init");
        }
        this.pcs = new PropertyChangeSupport(this);
        if (Utilities.isWindows()) {
            this.pcs.addPropertyChangeListener(this);
        }
    }

    public void useBrowserExecutableDelegate(ExtWebBrowser otherBrowser) {
        this.browserExecutableDelegate = otherBrowser;
    }

    public String getDDEServer() {
        return this.ddeServer;
    }

    public void setDDEServer(String ddeServer) {
        if (ddeServer != null && !ddeServer.equals(this.ddeServer)) {
            String old = this.ddeServer;
            this.ddeServer = ddeServer;
            this.pcs.firePropertyChange("dDEServer", old, ddeServer);
            err.log(Level.INFO, "DDEServer changed to: " + ddeServer);
        }
    }

    public int getOpenurlTimeout() {
        return this.openurlTimeout;
    }

    public void setOpenurlTimeout(int openurlTimeout) {
        if (openurlTimeout != this.openurlTimeout) {
            int oldVal = this.openurlTimeout;
            this.openurlTimeout = openurlTimeout;
            this.pcs.firePropertyChange("openurlTimeout", oldVal, openurlTimeout);
        }
    }

    public int getActivateTimeout() {
        return this.activateTimeout;
    }

    public void setActivateTimeout(int activateTimeout) {
        if (activateTimeout != this.activateTimeout) {
            int oldVal = this.activateTimeout;
            this.activateTimeout = activateTimeout;
            this.pcs.firePropertyChange("activateTimeout", oldVal, activateTimeout);
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (name != null && !name.equals(this.name)) {
            String oldVal = this.name;
            this.name = name;
            this.pcs.firePropertyChange("name", oldVal, name);
        }
    }

    public NbProcessDescriptor getBrowserExecutable() {
        if (this.browserExecutableDelegate != null) {
            return this.browserExecutableDelegate.getBrowserExecutable();
        }
        NbProcessDescriptor result = this.browserExecutable;
        if (this.browserExecutable == null || "".equals(this.browserExecutable.getProcessName())) {
            result = this.defaultBrowserExecutable();
        }
        if (err.isLoggable(Level.FINE)) {
            err.log(Level.FINE, "" + System.currentTimeMillis() + " getBrowserExecutable: " + (result == null ? "null" : new StringBuilder().append(result.getProcessName()).append(" ").append(result.getArguments()).toString()));
        }
        return result;
    }

    public void setBrowserExecutable(NbProcessDescriptor browserExecutable) {
        NbProcessDescriptor oldVal;
        if (browserExecutable != null && !browserExecutable.equals((Object)this.browserExecutable)) {
            oldVal = this.browserExecutable;
            this.browserExecutable = browserExecutable;
            this.pcs.firePropertyChange("browserExecutable", (Object)oldVal, (Object)browserExecutable);
        }
        if (browserExecutable == null) {
            oldVal = this.browserExecutable;
            this.browserExecutable = this.defaultBrowserExecutable();
            this.pcs.firePropertyChange("browserExecutable", (Object)oldVal, (Object)browserExecutable);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object np;
        if (evt.getPropertyName().equals("browserExecutable") && (np = evt.getNewValue()) != null && np instanceof NbProcessDescriptor) {
            String processName = ((NbProcessDescriptor)np).getProcessName();
            if (err.isLoggable(Level.FINE)) {
                err.log(Level.FINE, "" + System.currentTimeMillis() + "> propertychange: " + processName);
            }
            if (processName != null && processName.trim().length() > 0) {
                if (processName.toUpperCase().indexOf("IEXPLORE.EXE") > -1) {
                    this.setDDEServer("IEXPLORE");
                } else if (processName.toUpperCase().indexOf("MOZILLA.EXE") > -1) {
                    this.setDDEServer("MOZILLA");
                } else if (processName.toUpperCase().indexOf("FIREFOX.EXE") > -1) {
                    this.setDDEServer("FIREFOX");
                } else if (processName.toUpperCase().indexOf("CHROME.EXE") > -1) {
                    this.setDDEServer("CHROME");
                } else {
                    this.setDDEServer(null);
                }
            }
        }
    }

    protected NbProcessDescriptor defaultBrowserExecutable() {
        String b = "mozilla";
        if (err.isLoggable(Level.FINE)) {
            err.log(Level.FINE, "" + System.currentTimeMillis() + "> ExtBrowser: defaultBrowserExecutable: ");
        }
        if (Utilities.isWindows()) {
            String params;
            params = "";
            try {
                b = NbDdeBrowserImpl.getDefaultOpenCommand();
                String[] args = Utilities.parseParameters((String)b);
                if (args == null || args.length == 0) {
                    throw new NbBrowserException();
                }
                b = args[0];
                if (args[0].toUpperCase().indexOf("IEXPLORE.EXE") > -1) {
                    this.setDDEServer("IEXPLORE");
                    params = "-nohome ";
                } else if (args[0].toUpperCase().indexOf("MOZILLA.EXE") > -1) {
                    this.setDDEServer("MOZILLA");
                } else if (args[0].toUpperCase().indexOf("FIREFOX.EXE") > -1) {
                    this.setDDEServer("FIREFOX");
                } else if (args[0].toUpperCase().indexOf("CHROME.EXE") > -1) {
                    this.setDDEServer("CHROME");
                }
                params = params + "{URL}";
                return new NbProcessDescriptor(b, params);
            }
            catch (NbBrowserException e) {
                try {
                    b = NbDdeBrowserImpl.getBrowserPath("IEXPLORE");
                    if (b != null && b.trim().length() > 0) {
                        this.setDDEServer("IEXPLORE");
                        params = params + "{URL}";
                        return new NbProcessDescriptor(b, params);
                    }
                    b = NbDdeBrowserImpl.getBrowserPath("MOZILLA");
                    if (b != null && b.trim().length() > 0) {
                        this.setDDEServer("MOZILLA");
                        params = params + "{URL}";
                        return new NbProcessDescriptor(b, params);
                    }
                    b = NbDdeBrowserImpl.getBrowserPath("FIREFOX");
                    if (b != null && b.trim().length() > 0) {
                        this.setDDEServer("FIREFOX");
                        params = params + "{URL}";
                        return new NbProcessDescriptor(b, params);
                    }
                    b = NbDdeBrowserImpl.getBrowserPath("chrome");
                    if (b != null && b.trim().length() > 0) {
                        this.setDDEServer("CHROME");
                        params = params + "{URL}";
                        return new NbProcessDescriptor(b, params);
                    }
                }
                catch (NbBrowserException e2) {
                    this.setDDEServer("IEXPLORE");
                    b = "C:\\Program Files\\Internet Explorer\\iexplore.exe";
                }
            }
            catch (UnsatisfiedLinkError e) {
                b = "iexplore";
            }
            params = params + "{URL}";
            return new NbProcessDescriptor(b, params);
        }
        if (Utilities.isUnix() && !Utilities.isMac()) {
            if (Utilities.getOperatingSystem() == 16) {
                b = "mozilla";
                File f = new File("/usr/local/mozilla/mozilla");
                if (f.exists()) {
                    b = f.getAbsolutePath();
                } else {
                    f = new File("/usr/bin/firefox");
                    if (f.exists()) {
                        b = f.getAbsolutePath();
                    }
                }
            } else if (Utilities.getOperatingSystem() == 8) {
                b = "mozilla";
                File f = new File("/usr/sfw/lib/mozilla");
                if (f.exists()) {
                    b = f.getAbsolutePath();
                }
            }
            return new NbProcessDescriptor(b, "-remote \"openURL({URL})\"", UnixBrowserFormat.getHint());
        }
        if (Utilities.getOperatingSystem() == 2048) {
            return new NbProcessDescriptor("Netscape.exe", " {URL}", UnixBrowserFormat.getHint());
        }
        if (Utilities.isMac()) {
            return new NbProcessDescriptor("/usr/bin/open", " {URL}", UnixBrowserFormat.getHint());
        }
        return new NbProcessDescriptor("", " {URL}", UnixBrowserFormat.getHint());
    }

    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        return new DelegatingWebBrowserImpl(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        if (this.pcs == null) {
            this.pcs = new PropertyChangeSupport(this);
        }
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        if (this.pcs != null) {
            this.pcs.removePropertyChangeListener(l);
        }
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        int idx;
        String args;
        ois.defaultReadObject();
        if (this.browserExecutable != null && this.browserExecutable.getArguments() != null && (idx = (args = this.browserExecutable.getArguments()).indexOf("{params}")) >= 0) {
            this.browserExecutable = new NbProcessDescriptor(this.browserExecutable.getProcessName(), args.substring(0, idx) + "-remote \"openURL({URL})" + args.substring(idx + 8), UnixBrowserFormat.getHint());
        }
        if (this.family == null) {
            this.family = this instanceof ChromeBrowser ? PrivateBrowserFamilyId.CHROME : (this instanceof ChromiumBrowser ? PrivateBrowserFamilyId.CHROMIUM : (this instanceof FirefoxBrowser ? PrivateBrowserFamilyId.FIREFOX : (this instanceof IExplorerBrowser ? PrivateBrowserFamilyId.IE : (this instanceof MozillaBrowser ? PrivateBrowserFamilyId.MOZILLA : (this instanceof SafariBrowser ? PrivateBrowserFamilyId.SAFARI : PrivateBrowserFamilyId.UNKNOWN)))));
        }
        this.init();
    }

    public PrivateBrowserFamilyId getPrivateBrowserFamilyId() {
        return this.family;
    }

    void setPrivateBrowserFamilyId(PrivateBrowserFamilyId family) {
        if (!this.family.equals((Object)family)) {
            PrivateBrowserFamilyId oldVal = this.family;
            this.family = family;
            this.pcs.firePropertyChange("privateBrowserFamilyId", (Object)oldVal, (Object)family);
        }
    }

    public static class UnixBrowserFormat
    extends MapFormat {
        private static final long serialVersionUID = -699340388834127437L;
        public static final String TAG_URL = "URL";

        public UnixBrowserFormat(String url) {
            super(new HashMap());
            Map map = this.getMap();
            map.put("URL", url);
        }

        public static String getHint() {
            return NbBundle.getMessage(ExtWebBrowser.class, (String)"MSG_BrowserExecutorHint");
        }
    }

}

