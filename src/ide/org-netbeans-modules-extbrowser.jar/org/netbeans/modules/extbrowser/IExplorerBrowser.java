/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class IExplorerBrowser
extends ExtWebBrowser {
    private static final long serialVersionUID = 6433332055280422486L;

    public static Boolean isHidden() {
        return Utilities.isWindows() ? Boolean.FALSE : Boolean.TRUE;
    }

    public IExplorerBrowser() {
        super(PrivateBrowserFamilyId.IE);
        this.ddeServer = "IEXPLORE";
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(IExplorerBrowser.class, (String)"CTL_IExplorerBrowserName");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        NbDdeBrowserImpl impl = null;
        if (!Utilities.isWindows()) {
            throw new UnsupportedOperationException(NbBundle.getMessage(IExplorerBrowser.class, (String)"MSG_CannotUseBrowser"));
        }
        impl = new NbDdeBrowserImpl(this);
        return impl;
    }

    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        String b;
        String params = "-nohome ";
        params = params + "{URL}";
        try {
            b = NbDdeBrowserImpl.getBrowserPath(this.getDDEServer());
        }
        catch (NbBrowserException e) {
            b = "C:\\Program Files\\Internet Explorer\\iexplore.exe";
        }
        catch (UnsatisfiedLinkError e) {
            b = "iexplore";
        }
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " IE: defaultBrowserExecutable: " + params + ", " + b);
        }
        return new NbProcessDescriptor(b, params);
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
    }
}

