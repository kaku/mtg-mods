/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.extbrowser;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.extbrowser.ChromiumBrowser;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ChromiumBrowserBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        BeanDescriptor descr = new BeanDescriptor(ChromiumBrowser.class);
        descr.setDisplayName(NbBundle.getMessage(ChromiumBrowserBeanInfo.class, (String)"CTL_ChromiumBrowserName"));
        descr.setShortDescription(NbBundle.getMessage(ChromiumBrowserBeanInfo.class, (String)"HINT_ChromiumBrowserName"));
        descr.setValue("helpID", "org.netbeans.modules.extbrowser.ExtWebBrowser");
        return descr;
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        return new PropertyDescriptor[0];
    }

    @Override
    public Image getIcon(int type) {
        return this.loadImage("/org/netbeans/modules/extbrowser/resources/extbrowser.gif");
    }

    @Override
    public BeanInfo[] getAdditionalBeanInfo() {
        try {
            return new BeanInfo[]{Introspector.getBeanInfo(ExtWebBrowser.class)};
        }
        catch (IntrospectionException ie) {
            Exceptions.printStackTrace((Throwable)ie);
            return null;
        }
    }
}

