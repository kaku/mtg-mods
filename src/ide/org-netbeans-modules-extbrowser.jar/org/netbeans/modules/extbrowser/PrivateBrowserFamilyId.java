/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.extbrowser;

public enum PrivateBrowserFamilyId {
    FIREFOX,
    MOZILLA,
    CHROME,
    CHROMIUM,
    SAFARI,
    IE,
    OPERA,
    UNKNOWN;
    

    private PrivateBrowserFamilyId() {
    }
}

