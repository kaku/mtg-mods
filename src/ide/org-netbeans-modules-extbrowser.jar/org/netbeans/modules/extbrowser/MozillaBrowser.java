/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$Impl
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.io.File;
import java.util.logging.Level;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.NbDdeBrowserImpl;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.UnixBrowserImpl;
import org.openide.awt.HtmlBrowser;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class MozillaBrowser
extends ExtWebBrowser {
    private static final long serialVersionUID = -3982770681461437966L;

    public MozillaBrowser() {
        super(PrivateBrowserFamilyId.MOZILLA);
        this.ddeServer = "MOZILLA";
    }

    public static Boolean isHidden() {
        String detectedPath = null;
        if (Utilities.isWindows()) {
            try {
                detectedPath = NbDdeBrowserImpl.getBrowserPath("MOZILLA");
            }
            catch (NbBrowserException e) {
                ExtWebBrowser.getEM().log(Level.FINEST, "Cannot detect Mozilla : " + e);
            }
            if (detectedPath != null && detectedPath.trim().length() > 0) {
                return Boolean.FALSE;
            }
            return Boolean.TRUE;
        }
        return Utilities.isUnix() && !Utilities.isMac() ? Boolean.FALSE : Boolean.TRUE;
    }

    @Override
    public String getName() {
        if (this.name == null) {
            this.name = NbBundle.getMessage(MozillaBrowser.class, (String)"CTL_MozillaBrowserName");
        }
        return this.name;
    }

    @Override
    public HtmlBrowser.Impl createHtmlBrowserImpl() {
        ExtBrowserImpl impl = null;
        if (Utilities.isWindows()) {
            impl = new NbDdeBrowserImpl(this);
        } else if (Utilities.isUnix() && !Utilities.isMac()) {
            impl = new UnixBrowserImpl(this);
        } else {
            throw new UnsupportedOperationException(NbBundle.getMessage(MozillaBrowser.class, (String)"MSG_CannotUseBrowser"));
        }
        return impl;
    }

    @Override
    protected NbProcessDescriptor defaultBrowserExecutable() {
        String params = "";
        if (Utilities.isWindows()) {
            String prg2;
            params = params + "{URL}";
            try {
                String prg2 = NbDdeBrowserImpl.getBrowserPath(this.getDDEServer());
                return new NbProcessDescriptor(prg2, params);
            }
            catch (NbBrowserException e) {
                prg2 = "C:\\Program Files\\Mozilla.org\\Mozilla\\mozilla.exe";
            }
            catch (UnsatisfiedLinkError e) {
                prg2 = "iexplore";
            }
            NbProcessDescriptor retValue = new NbProcessDescriptor(prg2, params);
            return retValue;
        }
        String prg = "mozilla";
        if (Utilities.getOperatingSystem() == 16) {
            File f = new File("/usr/bin/mozilla");
            if (f.exists()) {
                prg = f.getAbsolutePath();
            }
            if ((f = new File("/usr/local/mozilla/mozilla")).exists()) {
                prg = f.getAbsolutePath();
            }
        } else if (Utilities.getOperatingSystem() == 8) {
            File f = new File("/usr/sfw/lib/mozilla/mozilla");
            if (f.exists()) {
                prg = f.getAbsolutePath();
            } else {
                f = new File("/opt/csw/bin/mozilla");
                if (f.exists()) {
                    prg = f.getAbsolutePath();
                }
            }
        }
        NbProcessDescriptor retValue = new NbProcessDescriptor(prg, "-remote \"openURL({URL})\"", NbBundle.getMessage(MozillaBrowser.class, (String)"MSG_BrowserExecutorHint"));
        return retValue;
    }
}

