/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.extbrowser;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String BrowserUtils_cannot_run_detail() {
        return NbBundle.getMessage(Bundle.class, (String)"BrowserUtils.cannot.run.detail");
    }

    static String BrowserUtils_cannot_run_title(Object browser_name) {
        return NbBundle.getMessage(Bundle.class, (String)"BrowserUtils.cannot.run.title", (Object)browser_name);
    }

    static String NbDdeBrowserImpl_browser_external() {
        return NbBundle.getMessage(Bundle.class, (String)"NbDdeBrowserImpl.browser.external");
    }

    private void Bundle() {
    }
}

