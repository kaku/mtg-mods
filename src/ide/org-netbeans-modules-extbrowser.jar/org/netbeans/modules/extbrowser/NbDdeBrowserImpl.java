/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.StatusDisplayer
 *  org.openide.execution.NbProcessDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.awt.EventQueue;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.Format;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.modules.extbrowser.BrowserUtils;
import org.netbeans.modules.extbrowser.Bundle;
import org.netbeans.modules.extbrowser.ExtBrowserImpl;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.netbeans.modules.extbrowser.NbBrowserException;
import org.netbeans.modules.extbrowser.PrivateBrowserFamilyId;
import org.netbeans.modules.extbrowser.SimpleExtBrowser;
import org.netbeans.modules.extbrowser.URLUtil;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.execution.NbProcessDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class NbDdeBrowserImpl
extends ExtBrowserImpl {
    private static final String WWW_ACTIVATE = "WWW_Activate";
    private static final String WWW_OPEN_URL = "WWW_OpenURL";
    private static final String EXTBROWSER_DLL = "extbrowser";
    private static final String EXTBROWSER_DLL_64BIT = "extbrowser64";
    private static Thread nativeThread;
    private static URLDisplayer nativeRunnable;
    static final /* synthetic */ boolean $assertionsDisabled;

    public NbDdeBrowserImpl(ExtWebBrowser extBrowserFactory) {
        this.extBrowserFactory = extBrowserFactory;
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "NbDdeBrowserImpl created with factory: " + extBrowserFactory);
        }
    }

    private native byte[] reqDdeMessage(String var1, String var2, String var3, int var4) throws NbBrowserException;

    public static native String getBrowserPath(String var0) throws NbBrowserException;

    public static native String getDefaultOpenCommand() throws NbBrowserException;

    @Override
    protected void loadURLInBrowserInternal(URL url) {
        if (!$assertionsDisabled && EventQueue.isDispatchThread()) {
            throw new AssertionError();
        }
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "NbDdeBrowserImpl.setUrl: " + url);
        }
        if (url == null) {
            return;
        }
        if (this.isInternetExplorer()) {
            if (nativeThread == null) {
                nativeRunnable = new URLDisplayer();
                nativeThread = new Thread((Runnable)nativeRunnable, "URLdisplayer");
                nativeThread.start();
            }
            nativeRunnable.postTask(new DisplayTask(url, this));
        } else {
            NbProcessDescriptor np = this.extBrowserFactory.getBrowserExecutable();
            try {
                url = URLUtil.createExternalURL(url, false);
                URI uri = url.toURI();
                if (np != null) {
                    np.exec((Format)((Object)new SimpleExtBrowser.BrowserFormat(uri == null ? "" : uri.toASCIIString())));
                }
            }
            catch (URISyntaxException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
            catch (IOException ex) {
                NbDdeBrowserImpl.logInfo(ex);
                BrowserUtils.notifyMissingBrowser(np.getProcessName());
            }
        }
    }

    private static void logInfo(Exception ex) {
        Logger logger = Logger.getLogger(NbDdeBrowserImpl.class.getName());
        logger.log(Level.INFO, null, ex);
    }

    @Override
    protected PrivateBrowserFamilyId detectPrivateBrowserFamilyId() {
        PrivateBrowserFamilyId id = super.detectPrivateBrowserFamilyId();
        if (id != PrivateBrowserFamilyId.UNKNOWN) {
            return id;
        }
        String ddeServer = this.realDDEServer();
        if ("FIREFOX".equals(ddeServer)) {
            return PrivateBrowserFamilyId.FIREFOX;
        }
        if ("CHROME".equals(ddeServer)) {
            return PrivateBrowserFamilyId.CHROME;
        }
        return PrivateBrowserFamilyId.UNKNOWN;
    }

    private String realDDEServer() {
        String srv;
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "NbDdeBrowserImpl.realDDEServer");
        }
        if ((srv = this.extBrowserFactory.getDDEServer()) != null) {
            return srv;
        }
        try {
            String cmd = NbDdeBrowserImpl.getDefaultOpenCommand();
            if (cmd != null) {
                if (cmd.toUpperCase().indexOf("IEXPLORE") >= 0) {
                    return "IEXPLORE";
                }
                if (cmd.toUpperCase().indexOf("CHROME") >= 0) {
                    return "CHROME";
                }
                if (cmd.toUpperCase().indexOf("FIREFOX") >= 0) {
                    return "FIREFOX";
                }
                if (cmd.toUpperCase().indexOf("MOZILLA") >= 0) {
                    return "MOZILLA";
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger("global").log(Level.INFO, null, ex);
        }
        return "IEXPLORE";
    }

    public int getActivateTimeout() {
        return this.extBrowserFactory.getActivateTimeout();
    }

    public int getOpenUrlTimeout() {
        return this.extBrowserFactory.getOpenurlTimeout();
    }

    private boolean isInternetExplorer() {
        return this.realDDEServer().equals("IEXPLORE");
    }

    static {
        boolean bl = NbDdeBrowserImpl.$assertionsDisabled = !NbDdeBrowserImpl.class.desiredAssertionStatus();
        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "> NbDdeBrowser: static initializer: ");
        }
        try {
            if (Utilities.isWindows()) {
                String sunDataModel = System.getProperty("sun.arch.data.model");
                if (sunDataModel != null) {
                    if ("64".equals(sunDataModel)) {
                        System.loadLibrary("extbrowser64");
                    } else {
                        System.loadLibrary("extbrowser");
                    }
                } else {
                    String javaVMName = System.getProperty("java.vm.name");
                    if (javaVMName != null && javaVMName.indexOf("64") > -1) {
                        System.loadLibrary("extbrowser64");
                    } else {
                        System.loadLibrary("extbrowser");
                    }
                }
            }
        }
        catch (Exception e) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"ERR_cant_locate_dll"), 1));
        }
        nativeThread = null;
        nativeRunnable = null;
    }

    private static class DisplayTask {
        URL url;
        NbDdeBrowserImpl browser;

        DisplayTask(URL url, NbDdeBrowserImpl browser) {
            this.url = url;
            this.browser = browser;
        }
    }

    static class URLDisplayer
    implements Runnable {
        private static final int ADDITIONAL_WAIT_TIMEOUT = 6000;
        Vector tasks = new Vector();
        boolean doProcessing = true;
        boolean isDisplaying = false;

        private URLDisplayer() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void postTask(DisplayTask task) {
            URLDisplayer uRLDisplayer = this;
            synchronized (uRLDisplayer) {
                boolean shouldNotify = this.tasks.isEmpty();
                this.tasks.add(task);
                if (shouldNotify) {
                    this.notifyAll();
                }
            }
        }

        private synchronized DisplayTask getNextTask() throws InterruptedException {
            while (this.tasks.isEmpty()) {
                this.wait();
            }
            return (DisplayTask)this.tasks.remove(0);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "NbDdeBrowserImpl.run");
            }
            while (this.doProcessing) {
                try {
                    DisplayTask task = this.getNextTask();
                    this.isDisplaying = true;
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask(){

                        @Override
                        public void run() {
                            if (URLDisplayer.this.isDisplaying) {
                                nativeThread.interrupt();
                                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                                    ExtWebBrowser.getEM().log(Level.FINE, "interrupted in URLDisplayer.run.TimerTask.run()");
                                }
                                BrowserUtils.notifyMissingBrowser(Bundle.NbDdeBrowserImpl_browser_external());
                            }
                        }
                    }, 6000);
                    this.dispatchURL(task);
                    timer.cancel();
                    continue;
                }
                catch (InterruptedException ex) {
                    ExtWebBrowser.getEM().log(Level.INFO, "interrupted in run(): " + ex);
                    continue;
                }
                finally {
                    this.isDisplaying = false;
                    continue;
                }
            }
        }

        public void dispatchURL(DisplayTask task) {
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " NbDdeBrowserImpl.dispatchURL: " + task);
            }
            try {
                URL url = task.url;
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " URLDispatcher.url: " + url);
                }
                url = URLUtil.createExternalURL(url, URLUtil.browserHandlesJarURLs(task.browser.realDDEServer()));
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " url: " + url);
                }
                String urlStr = url.toString();
                boolean triedStart = false;
                int MAX_URL_LENGTH = 199;
                if (urlStr != null && urlStr.length() > 199) {
                    urlStr = this.getFileUrl(urlStr);
                }
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " urlstr: " + urlStr);
                }
                if (!this.win9xHack(task.browser.realDDEServer())) {
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"MSG_activatingBrowser"));
                    try {
                        task.browser.reqDdeMessage(task.browser.realDDEServer(), "WWW_Activate", "-1,0x0", task.browser.getActivateTimeout());
                    }
                    catch (NbBrowserException ex) {
                        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                            ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "Exception, gonna start browser: " + ex);
                        }
                        triedStart = true;
                        this.startBrowser(task.browser.extBrowserFactory.getBrowserExecutable(), urlStr);
                    }
                }
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " firstpart");
                }
                if (!triedStart) {
                    StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"MSG_openingURLInBrowser", (Object)urlStr));
                    String args1 = "\"" + urlStr + "\",,-1,0x1,,,";
                    try {
                        Thread.sleep(500);
                        task.browser.reqDdeMessage(task.browser.realDDEServer(), "WWW_OpenURL", args1, task.browser.getOpenUrlTimeout());
                    }
                    catch (NbBrowserException ex) {
                        if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                            ExtWebBrowser.getEM().log(Level.FINE, "Restarting browser.");
                        }
                        this.startBrowser(task.browser.extBrowserFactory.getBrowserExecutable(), urlStr);
                    }
                    catch (InterruptedException ex) {
                        // empty catch block
                    }
                }
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " secondpart");
                }
                task.browser.pcs.firePropertyChange("url", task.browser.getURL(), url);
            }
            catch (Exception ex) {
                final Exception ex1 = ex;
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + " Interrupted in URLDisplayer.dispatchURL.end");
                }
                Exceptions.attachLocalizedMessage((Throwable)ex1, (String)NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"MSG_win_browser_invocation_failed"));
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Exceptions.printStackTrace((Throwable)ex1);
                    }
                });
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private String getFileUrl(String url) {
            if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "Gonna get redirect file for long url: " + url);
            }
            String newurl = null;
            OutputStreamWriter fw = null;
            File f = null;
            int retries = 10;
            while (f == null && retries > 0) {
                --retries;
                try {
                    f = File.createTempFile("extbrowser", ".html");
                    if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                        ExtWebBrowser.getEM().log(Level.FINE, "file: " + f);
                    }
                    if (f == null) continue;
                    fw = new FileWriter(f);
                    if (f.canWrite()) {
                        String s1 = NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"TXT_RedirectURL1");
                        String s2 = NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"TXT_RedirectURL2");
                        String s = s1.concat(url).concat(s2);
                        fw.write(s);
                        fw.flush();
                    }
                    newurl = "file:/" + f.getAbsolutePath();
                    continue;
                }
                catch (IOException ioe) {
                    Logger.getLogger("global").log(Level.INFO, "" + System.currentTimeMillis() + ioe.toString());
                    continue;
                }
                finally {
                    if (fw == null) continue;
                    try {
                        fw.close();
                        continue;
                    }
                    catch (IOException ioe) {
                        Logger.getLogger("global").log(Level.INFO, "" + System.currentTimeMillis() + ioe.toString());
                    }
                    continue;
                }
            }
            if (newurl != null) {
                if (ExtWebBrowser.getEM().isLoggable(Level.FINE)) {
                    ExtWebBrowser.getEM().log(Level.FINE, "" + System.currentTimeMillis() + "New URL: " + newurl);
                }
                return newurl;
            }
            return url;
        }

        private boolean win9xHack(String browser) {
            return browser.equals("IEXPLORE") && (Utilities.getOperatingSystem() == 4 || Utilities.getOperatingSystem() == 2);
        }

        private void startBrowser(NbProcessDescriptor cmd, String url) throws IOException {
            StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(NbDdeBrowserImpl.class, (String)"MSG_startingBrowser", (Object)url));
            cmd.exec((Format)((Object)new ExtWebBrowser.UnixBrowserFormat(url)));
        }

    }

}

