/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package org.netbeans.modules.extbrowser;

import java.awt.Image;
import java.beans.BeanDescriptor;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;
import org.netbeans.modules.extbrowser.ExtWebBrowser;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

public class ExtWebBrowserBeanInfo
extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(ExtWebBrowser.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] properties;
        if (Utilities.isWindows()) {
            try {
                properties = new PropertyDescriptor[]{new PropertyDescriptor("browserExecutable", ExtWebBrowser.class), new PropertyDescriptor("activateTimeout", ExtWebBrowser.class), new PropertyDescriptor("openurlTimeout", ExtWebBrowser.class)};
                properties[0].setDisplayName(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"PROP_browserExecutable"));
                properties[0].setShortDescription(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"HINT_browserExecutable"));
                properties[0].setPreferred(true);
                properties[1].setDisplayName(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"PROP_DDE_ACTIVATE_TIMEOUT"));
                properties[1].setShortDescription(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"HINT_DDE_ACTIVATE_TIMEOUT"));
                properties[1].setExpert(true);
                properties[1].setHidden(true);
                properties[2].setDisplayName(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"PROP_DDE_OPENURL_TIMEOUT"));
                properties[2].setShortDescription(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"HINT_DDE_OPENURL_TIMEOUT"));
                properties[2].setExpert(true);
                properties[2].setHidden(true);
            }
            catch (IntrospectionException ie) {
                Exceptions.printStackTrace((Throwable)ie);
                return null;
            }
        }
        try {
            properties = new PropertyDescriptor[]{new PropertyDescriptor("browserExecutable", ExtWebBrowser.class)};
            properties[0].setDisplayName(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"PROP_browserExecutable"));
            properties[0].setShortDescription(NbBundle.getMessage(ExtWebBrowserBeanInfo.class, (String)"HINT_browserExecutable"));
        }
        catch (IntrospectionException ie) {
            Exceptions.printStackTrace((Throwable)ie);
            return null;
        }
        return properties;
    }

    @Override
    public Image getIcon(int type) {
        return this.loadImage("/org/netbeans/modules/extbrowser/resources/extbrowser.gif");
    }
}

