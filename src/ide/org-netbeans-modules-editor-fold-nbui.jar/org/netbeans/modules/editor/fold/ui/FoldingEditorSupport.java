/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldHierarchyEvent
 *  org.netbeans.api.editor.fold.FoldHierarchyListener
 *  org.netbeans.api.editor.fold.FoldStateChange
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.editor.BaseCaret
 *  org.netbeans.spi.editor.fold.FoldHierarchyMonitor
 */
package org.netbeans.modules.editor.fold.ui;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.fold.FoldStateChange;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.editor.BaseCaret;
import org.netbeans.modules.editor.fold.ui.FoldViewFactory;
import org.netbeans.spi.editor.fold.FoldHierarchyMonitor;

public class FoldingEditorSupport
implements FoldHierarchyListener {
    private static final Logger LOG = Logger.getLogger(FoldingEditorSupport.class.getName());
    private final JTextComponent component;
    private final FoldHierarchy foldHierarchy;

    FoldingEditorSupport(FoldHierarchy h, JTextComponent component) {
        this.component = component;
        this.foldHierarchy = h;
        component.putClientProperty("org.netbeans.api.fold.expander", new C());
        this.foldHierarchy.addFoldHierarchyListener((FoldHierarchyListener)this);
    }

    public void foldHierarchyChanged(final FoldHierarchyEvent evt) {
        Caret c = this.component.getCaret();
        if (!(c instanceof BaseCaret)) {
            return;
        }
        final BaseCaret bc = (BaseCaret)c;
        int caretOffset = c.getDot();
        final int addedFoldCnt = evt.getAddedFoldCount();
        boolean scrollToView = false;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Received fold hierarchy change {1}, added folds: {0}", new Object[]{addedFoldCnt, evt.hashCode()});
        }
        boolean expand = false;
        boolean includeEnd = false;
        int newPosition = -1;
        FoldHierarchy hierarchy = (FoldHierarchy)evt.getSource();
        if (addedFoldCnt > 0) {
            expand = true;
        } else {
            int startOffset = Integer.MAX_VALUE;
            if (evt.getAffectedStartOffset() <= caretOffset && evt.getAffectedEndOffset() >= caretOffset) {
                for (int i = 0; i < evt.getFoldStateChangeCount(); ++i) {
                    boolean e;
                    int from;
                    int to;
                    Fold fold;
                    FoldStateChange change = evt.getFoldStateChange(i);
                    if (change.isCollapsedChanged()) {
                        fold = change.getFold();
                        if (!fold.isCollapsed() || fold.getStartOffset() > caretOffset || fold.getEndOffset() < caretOffset || fold.getStartOffset() >= startOffset) continue;
                        startOffset = fold.getStartOffset();
                        LOG.log(Level.FINER, "Moving caret from just collapsed fold {0} to offset {1}; evt=" + evt.hashCode(), new Object[]{fold, startOffset});
                        continue;
                    }
                    if (change.isStartOffsetChanged()) {
                        fold = change.getFold();
                        int ostart = change.getOriginalStartOffset();
                        int nstart = fold.getStartOffset();
                        int nend = fold.getEndOffset();
                        to = Math.max(ostart, nstart);
                        from = Math.min(ostart, nstart);
                        boolean bl = e = caretOffset >= from && caretOffset <= to && caretOffset >= nstart && caretOffset < nend;
                        if (e && LOG.isLoggable(Level.FINE)) {
                            LOG.log(Level.FINER, "Fold start extended over caret: {0}; evt= " + evt.hashCode(), (Object)fold);
                        }
                        expand |= e;
                        continue;
                    }
                    if (!change.isEndOffsetChanged()) continue;
                    fold = change.getFold();
                    int oend = change.getOriginalEndOffset();
                    int nend = fold.getEndOffset();
                    int nstart = fold.getStartOffset();
                    to = Math.max(oend, nend);
                    from = Math.min(oend, nend);
                    e = caretOffset >= from && caretOffset <= to && caretOffset >= nstart && caretOffset <= nend;
                    expand |= e;
                    boolean bl = includeEnd = caretOffset == nend && nend - nstart > 1;
                    if (!e || !LOG.isLoggable(Level.FINE)) continue;
                    LOG.log(Level.FINER, "Fold end extended over caret: {0}, includeEnd = {1}; evt= " + evt.hashCode(), new Object[]{fold, includeEnd});
                }
                if (startOffset != Integer.MAX_VALUE) {
                    newPosition = startOffset;
                    bc.setDot(startOffset, false);
                    expand = false;
                }
            }
            scrollToView = false;
        }
        if (expand) {
            Fold collapsed = null;
            boolean wasExpanded = false;
            if (includeEnd) {
                --caretOffset;
            }
            while ((collapsed = FoldUtilities.findCollapsedFold((FoldHierarchy)hierarchy, (int)caretOffset, (int)caretOffset)) != null && collapsed.getStartOffset() < caretOffset && collapsed.getEndOffset() > caretOffset) {
                LOG.log(Level.FINER, "Expanding fold {0}; evt= " + evt.hashCode(), (Object)collapsed);
                hierarchy.expand(collapsed);
                wasExpanded = true;
            }
            scrollToView = wasExpanded;
        }
        final int newPositionF = newPosition;
        if (LOG.isLoggable(Level.FINER)) {
            LOG.log(Level.FINER, "Added folds: {0}, should scroll: {1}, new pos: {2}; evt= " + evt.hashCode(), new Object[]{addedFoldCnt, scrollToView, newPosition});
        }
        if (addedFoldCnt > 1 || scrollToView || newPosition >= 0) {
            final boolean scroll = scrollToView;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    LOG.fine("Updating after fold hierarchy change; evt= " + evt.hashCode());
                    if (FoldingEditorSupport.this.component == null || FoldingEditorSupport.this.component.getCaret() != bc) {
                        return;
                    }
                    if (newPositionF >= 0) {
                        bc.setDot(newPositionF);
                    } else {
                        bc.refresh(addedFoldCnt > 1 && !scroll);
                    }
                }
            });
        }
    }

    public static class F
    implements FoldHierarchyMonitor {
        public void foldsAttached(FoldHierarchy h) {
            FoldingEditorSupport supp = new FoldingEditorSupport(h, h.getComponent());
            h.getComponent().putClientProperty(F.class, supp);
        }

        static {
            FoldViewFactory.register();
        }
    }

    private class C
    implements Runnable,
    Callable<Boolean> {
        private boolean res;
        private boolean sharp;

        private C() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            FoldingEditorSupport.this.foldHierarchy.lock();
            try {
                int offset = FoldingEditorSupport.this.component.getCaret().getDot();
                this.res = false;
                Fold f = FoldUtilities.findCollapsedFold((FoldHierarchy)FoldingEditorSupport.this.foldHierarchy, (int)offset, (int)offset);
                if (f != null) {
                    if (this.sharp) {
                        this.res = f.getStartOffset() < offset && f.getEndOffset() > offset;
                    } else {
                        boolean bl = this.res = f.getStartOffset() <= offset && f.getEndOffset() >= offset;
                    }
                    if (this.res) {
                        FoldingEditorSupport.this.foldHierarchy.expand(f);
                    }
                }
            }
            finally {
                FoldingEditorSupport.this.foldHierarchy.unlock();
            }
        }

        public boolean equals(Object whatever) {
            if (!(whatever instanceof Caret)) {
                return super.equals(whatever);
            }
            this.sharp = false;
            Document doc = FoldingEditorSupport.this.component.getDocument();
            doc.render(this);
            return this.res;
        }

        @Override
        public Boolean call() {
            this.sharp = true;
            Document doc = FoldingEditorSupport.this.component.getDocument();
            doc.render(this);
            return this.res;
        }
    }

}

