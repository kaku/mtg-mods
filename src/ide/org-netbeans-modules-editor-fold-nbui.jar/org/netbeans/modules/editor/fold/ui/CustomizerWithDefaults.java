/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold.ui;

import java.util.prefs.Preferences;

public interface CustomizerWithDefaults {
    public void setDefaultPreferences(Preferences var1);
}

