/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.view.EditorView
 *  org.netbeans.modules.editor.lib2.view.EditorView$Parent
 *  org.netbeans.modules.editor.lib2.view.ViewRenderContext
 *  org.netbeans.modules.editor.lib2.view.ViewUtils
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.TextHitInfo;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.fold.ui.Bundle;
import org.netbeans.modules.editor.fold.ui.FoldContentReaders;
import org.netbeans.modules.editor.fold.ui.FoldToolTip;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.ViewRenderContext;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.openide.util.NbBundle;

final class FoldView
extends EditorView {
    private static final Logger LOG = Logger.getLogger(FoldView.class.getName());
    private static final float EXTRA_MARGIN_WIDTH = 3.0f;
    private int rawEndOffset;
    private int length;
    private final JTextComponent textComponent;
    private final Fold fold;
    private TextLayout collapsedTextLayout;
    private AttributeSet foldingColors;
    private AttributeSet selectedColors;
    private int options;

    public FoldView(JTextComponent textComponent, Fold fold, FontColorSettings colorSettings, int options) {
        super(null);
        int offset = fold.getStartOffset();
        int len = fold.getEndOffset() - offset;
        assert (len > 0);
        this.length = len;
        this.textComponent = textComponent;
        this.fold = fold;
        this.foldingColors = colorSettings.getFontColors("code-folding");
        this.selectedColors = colorSettings.getFontColors("selection");
        this.options = options;
    }

    public float getPreferredSpan(int axis) {
        if (axis == 0) {
            String desc = this.fold.getDescription();
            float advance = 0.0f;
            if (desc.length() > 0) {
                TextLayout textLayout = this.getTextLayout();
                if (textLayout == null) {
                    return 0.0f;
                }
                advance = textLayout.getAdvance();
            }
            return advance + 6.0f;
        }
        EditorView.Parent parent = (EditorView.Parent)this.getParent();
        return parent != null ? parent.getViewRenderContext().getDefaultRowHeight() : 0.0f;
    }

    public int getRawEndOffset() {
        return this.rawEndOffset;
    }

    public void setRawEndOffset(int rawOffset) {
        this.rawEndOffset = rawOffset;
    }

    public int getLength() {
        return this.length;
    }

    public int getStartOffset() {
        return this.getEndOffset() - this.getLength();
    }

    public int getEndOffset() {
        EditorView.Parent parent = (EditorView.Parent)this.getParent();
        return parent != null ? parent.getViewEndOffset(this.rawEndOffset) : this.rawEndOffset;
    }

    public Document getDocument() {
        View parent = this.getParent();
        return parent != null ? parent.getDocument() : null;
    }

    public AttributeSet getAttributes() {
        return null;
    }

    private String resolvePlaceholder(String text, int at) {
        if ((this.options & 3) == 0) {
            return text;
        }
        Document d = this.getDocument();
        if (!(d instanceof BaseDocument)) {
            return null;
        }
        BaseDocument bd = (BaseDocument)d;
        CharSequence contentSeq = "";
        String summary = "";
        int mask = this.options;
        try {
            if ((this.options & 1) > 0 && (contentSeq = FoldContentReaders.get().readContent(DocumentUtilities.getMimeType((JTextComponent)this.textComponent), d, this.fold, this.fold.getType().getTemplate())) == null) {
                mask &= -2;
            }
            if ((this.options & 2) > 0) {
                int start = this.fold.getStartOffset();
                int end = this.fold.getEndOffset();
                int startLine = Utilities.getLineOffset((BaseDocument)bd, (int)start);
                int endLine = Utilities.getLineOffset((BaseDocument)bd, (int)end) + 1;
                if (endLine <= startLine + 1) {
                    mask &= -3;
                } else {
                    summary = Bundle.FMT_contentSummary(endLine - startLine);
                }
            }
        }
        catch (BadLocationException ex) {
            // empty catch block
        }
        if (mask == 0) {
            return text;
        }
        String replacement = NbBundle.getMessage(FoldView.class, (String)("FMT_ContentPlaceholder_" + (mask & 3)), (Object)contentSeq, (Object)summary);
        StringBuilder sb = new StringBuilder(text.length() + replacement.length());
        sb.append(text.subSequence(0, at));
        sb.append(replacement);
        sb.append(text.subSequence(at + FoldTemplate.CONTENT_PLACEHOLDER.length(), text.length()));
        return sb.toString();
    }

    private TextLayout getTextLayout() {
        if (this.collapsedTextLayout == null) {
            int placeIndex;
            EditorView.Parent parent = (EditorView.Parent)this.getParent();
            ViewRenderContext context = parent.getViewRenderContext();
            FontRenderContext frc = context.getFontRenderContext();
            assert (frc != null);
            Font font = context.getRenderFont(this.textComponent.getFont());
            String text = this.fold.getDescription();
            if (text.length() == 0) {
                text = " ";
            }
            if ((placeIndex = text.indexOf(FoldTemplate.CONTENT_PLACEHOLDER)) > -1) {
                text = this.resolvePlaceholder(text, placeIndex);
            }
            this.collapsedTextLayout = new TextLayout(text, font, frc);
        }
        return this.collapsedTextLayout;
    }

    public Shape modelToViewChecked(int offset, Shape alloc, Position.Bias bias) {
        return alloc;
    }

    public int viewToModelChecked(double x, double y, Shape alloc, Position.Bias[] biasReturn) {
        int startOffset = this.getStartOffset();
        return startOffset;
    }

    static TextHitInfo x2RelOffset(TextLayout textLayout, float x) {
        TextHitInfo hit = (x -= 3.0f) >= textLayout.getAdvance() ? TextHitInfo.trailing(textLayout.getCharacterCount()) : textLayout.hitTestChar(x, 0.0f);
        return hit;
    }

    public int getNextVisualPositionFromChecked(int offset, Position.Bias bias, Shape alloc, int direction, Position.Bias[] biasRet) {
        int startOffset = this.getStartOffset();
        int retOffset = -1;
        switch (direction) {
            case 7: {
                if (offset == -1) {
                    retOffset = startOffset;
                    break;
                }
                retOffset = -1;
                break;
            }
            case 3: {
                if (offset == -1) {
                    retOffset = startOffset;
                    break;
                }
                retOffset = -1;
                break;
            }
            case 1: 
            case 5: {
                break;
            }
            default: {
                throw new IllegalArgumentException("Bad direction: " + direction);
            }
        }
        return retOffset;
    }

    public JComponent getToolTip(double x, double y, Shape allocation) {
        Container container = this.getContainer();
        if (container instanceof JEditorPane) {
            JEditorPane editorPane = (JEditorPane)this.getContainer();
            JEditorPane tooltipPane = new JEditorPane();
            EditorKit kit = editorPane.getEditorKit();
            Document doc = this.getDocument();
            if (kit != null && doc != null) {
                Element lineRootElement = doc.getDefaultRootElement();
                tooltipPane.putClientProperty("view-folds-expanded", true);
                try {
                    int lineIndex = lineRootElement.getElementIndex(this.fold.getStartOffset());
                    Position pos = doc.createPosition(lineRootElement.getElement(lineIndex).getStartOffset());
                    tooltipPane.putClientProperty("document-view-start-position", pos);
                    lineIndex = lineRootElement.getElementIndex(this.fold.getEndOffset());
                    pos = doc.createPosition(lineRootElement.getElement(lineIndex).getEndOffset());
                    tooltipPane.putClientProperty("document-view-end-position", pos);
                    tooltipPane.putClientProperty("document-view-accurate-span", true);
                    tooltipPane.setEditorKit(kit);
                    tooltipPane.setDocument(doc);
                    tooltipPane.setEditable(false);
                    return new FoldToolTip(editorPane, tooltipPane, this.getForegroundColor());
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
        }
        return null;
    }

    private Color getForegroundColor() {
        if (this.foldingColors == null) {
            return this.textComponent.getForeground();
        }
        Object bgColorObj = this.foldingColors.getAttribute(StyleConstants.Foreground);
        if (bgColorObj instanceof Color) {
            return (Color)bgColorObj;
        }
        return this.textComponent.getForeground();
    }

    private Color getBackgroundColor() {
        Object bgColorObj;
        if (this.foldingColors == null) {
            return this.textComponent.getBackground();
        }
        int start = this.textComponent.getSelectionStart();
        int end = this.textComponent.getSelectionEnd();
        boolean partSelected = false;
        if (start != end) {
            int foldStart = this.fold.getStartOffset();
            int foldEnd = this.fold.getEndOffset();
            partSelected = start < foldEnd && end > foldStart;
        }
        Object object = bgColorObj = partSelected ? this.selectedColors.getAttribute(StyleConstants.Background) : this.foldingColors.getAttribute(StyleConstants.Background);
        if (bgColorObj instanceof Color) {
            return (Color)bgColorObj;
        }
        return this.textComponent.getBackground();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void paint(Graphics2D g, Shape alloc, Rectangle clipBounds) {
        Rectangle2D.Double allocBounds = ViewUtils.shape2Bounds((Shape)alloc);
        if (allocBounds.intersects(clipBounds)) {
            Font origFont = g.getFont();
            Color origColor = g.getColor();
            Color origBkColor = g.getBackground();
            Shape origClip = g.getClip();
            try {
                g.setColor(this.getForegroundColor());
                g.setBackground(this.getBackgroundColor());
                int xInt = (int)allocBounds.getX();
                int yInt = (int)allocBounds.getY();
                int endXInt = (int)(allocBounds.getX() + allocBounds.getWidth() - 1.0);
                int endYInt = (int)(allocBounds.getY() + allocBounds.getHeight() - 1.0);
                g.drawRect(xInt, yInt, endXInt - xInt, endYInt - yInt);
                g.clearRect(xInt + 1, yInt + 1, endXInt - xInt - 1, endYInt - yInt - 1);
                g.clip(alloc);
                TextLayout textLayout = this.getTextLayout();
                if (textLayout != null) {
                    EditorView.Parent parent = (EditorView.Parent)this.getParent();
                    float ascent = parent.getViewRenderContext().getDefaultAscent();
                    String desc = this.fold.getDescription();
                    float x = (float)(allocBounds.getX() + 3.0);
                    float y = (float)allocBounds.getY();
                    if (desc.length() > 0) {
                        textLayout.draw(g, x, y + ascent);
                    }
                }
            }
            finally {
                g.setClip(origClip);
                g.setBackground(origBkColor);
                g.setColor(origColor);
                g.setFont(origFont);
            }
        }
    }

    protected String getDumpName() {
        return "FV";
    }

    public String toString() {
        return this.appendViewInfo(new StringBuilder(200), 0, "", -1).toString();
    }
}

