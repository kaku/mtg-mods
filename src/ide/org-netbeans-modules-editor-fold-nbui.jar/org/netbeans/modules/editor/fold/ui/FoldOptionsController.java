/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.MemoryPreferences
 *  org.netbeans.modules.editor.settings.storage.api.OverridePreferences
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold.ui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.fold.ui.FoldOptionsPanel;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.MemoryPreferences;
import org.netbeans.modules.editor.settings.storage.api.OverridePreferences;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public class FoldOptionsController
extends OptionsPanelController
implements PreferenceChangeListener {
    private static final Logger PREF_LOG = Logger.getLogger(FoldHierarchy.class.getName() + ".enabled");
    static final Map<String, String> LEGACY_SETTINGS_MAP = new HashMap<String, String>();
    private FoldOptionsPanel panel;
    private boolean changed;
    private PropertyChangeSupport propSupport;
    private Map<String, MemoryPreferences> preferences;
    private Collection<String> updatedLangs;
    private Collection<String> legacyLangs;
    private static String[] LEGACY_SETTINGS;
    private boolean suppressPrefChanges;
    private PreferenceChangeListener weakChangeL;

    public FoldOptionsController() {
        this.propSupport = new PropertyChangeSupport(this);
        this.preferences = new HashMap<String, MemoryPreferences>();
        this.updatedLangs = Collections.EMPTY_SET;
        this.legacyLangs = Collections.EMPTY_SET;
        this.weakChangeL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)null);
    }

    public void update() {
        this.clearContents();
        this.initLanguages();
        if (this.panel != null) {
            this.panel.update();
        }
        this.changed = false;
    }

    private void initLanguages() {
        Set mimeTypes = EditorSettings.getDefault().getAllMimeTypes();
        HashSet<String> legacy = new HashSet<String>();
        HashSet<String> updated = new HashSet<String>();
        for (String s : mimeTypes) {
            if (FoldUtilities.getFoldTypes((String)s).values().isEmpty()) {
                legacy.add(s);
                continue;
            }
            updated.add(s);
        }
        this.updatedLangs = updated;
        this.legacyLangs = legacy;
    }

    public Collection<String> getUpdatedLanguages() {
        return this.updatedLangs;
    }

    public void applyChanges() {
        for (String s : this.preferences.keySet()) {
            MemoryPreferences p = this.preferences.get(s);
            try {
                if (PREF_LOG.isLoggable(Level.FINE) && p.getPreferences() instanceof OverridePreferences && ((OverridePreferences)p.getPreferences()).isOverriden("code-folding-enable")) {
                    PREF_LOG.log(Level.FINE, "Setting fold enable: {0} = {1}", new Object[]{s, p.getPreferences().get("code-folding-enable", null)});
                }
                if ("".equals(s)) {
                    for (String k : LEGACY_SETTINGS_MAP.values()) {
                        p.getPreferences().remove("code-folding-collapse-" + k);
                    }
                }
                p.getPreferences().flush();
            }
            catch (BackingStoreException ex) {
                Exceptions.printStackTrace((Throwable)ex);
            }
        }
        Preferences basePrefs = (Preferences)MimeLookup.getLookup((String)"").lookup(Preferences.class);
        for (String mime : this.legacyLangs) {
            Preferences prefs = (Preferences)MimeLookup.getLookup((String)mime).lookup(Preferences.class);
            prefs.putBoolean("code-folding-enable", basePrefs.getBoolean("code-folding-enable", true));
            for (String s2 : LEGACY_SETTINGS) {
                String k = "code-folding-collapse-" + s2;
                prefs.putBoolean(k, basePrefs.getBoolean(k, false));
            }
        }
        this.changed = false;
        this.propSupport.firePropertyChange("changed", true, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        boolean ch;
        if (this.suppressPrefChanges) {
            return;
        }
        ch = this.detectIsChanged();
        MemoryPreferences defMime = this.preferences.get("");
        if (defMime != null && defMime.getPreferences() == evt.getNode() && "code-folding-enable".equals(evt.getKey())) {
            this.suppressPrefChanges = true;
            try {
                for (MemoryPreferences p : this.preferences.values()) {
                    if (p == defMime || !((OverridePreferences)p.getPreferences()).isOverriden("code-folding-enable")) continue;
                    p.getPreferences().remove("code-folding-enable");
                }
            }
            finally {
                this.suppressPrefChanges = false;
            }
        }
        if (ch != this.changed) {
            this.propSupport.firePropertyChange("changed", !ch, ch);
        }
    }

    void globalEnableFolding(boolean enable) {
        PREF_LOG.log(Level.FINE, "Globally set folding-enable: " + enable);
        this.prefs("").putBoolean("code-folding-enable", enable);
        for (String mime : EditorSettings.getDefault().getAllMimeTypes()) {
            this.prefs(mime).remove("code-folding-enable");
        }
    }

    Preferences prefs(String mime) {
        MemoryPreferences cached = this.preferences.get(mime);
        if (cached != null) {
            return cached.getPreferences();
        }
        MimePath path = MimePath.parse((String)mime);
        Preferences result = (Preferences)MimeLookup.getLookup((String)mime).lookup(Preferences.class);
        if (!mime.equals("")) {
            String parentMime = path.getInheritedType();
            cached = MemoryPreferences.getWithInherited((Object)this, (Preferences)this.prefs(parentMime), (Preferences)result);
        } else {
            cached = MemoryPreferences.get((Object)this, (Preferences)result);
        }
        cached.getPreferences().addPreferenceChangeListener(this.weakChangeL);
        this.preferences.put(mime, cached);
        return cached.getPreferences();
    }

    private void clearContents() {
        for (MemoryPreferences m : this.preferences.values()) {
            m.getPreferences().removePreferenceChangeListener(this.weakChangeL);
            m.destroy();
        }
        this.preferences.clear();
        if (this.panel != null) {
            this.panel.clear();
        }
        this.changed = false;
    }

    public void cancel() {
        this.clearContents();
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.getPanel().isChanged();
    }

    private boolean detectIsChanged() {
        for (MemoryPreferences cached : this.preferences.values()) {
            if (!cached.isDirty(cached.getPreferences())) continue;
            return true;
        }
        return false;
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getPanel();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.editor.folding");
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.propSupport.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.propSupport.removePropertyChangeListener(l);
    }

    private FoldOptionsPanel getPanel() {
        if (this.panel == null) {
            this.panel = new FoldOptionsPanel(this);
        }
        return this.panel;
    }

    static {
        LEGACY_SETTINGS_MAP.put(FoldType.MEMBER.code(), "method");
        LEGACY_SETTINGS_MAP.put(FoldType.NESTED.code(), "innerclass");
        LEGACY_SETTINGS_MAP.put(FoldType.DOCUMENTATION.code(), "javadoc");
        LEGACY_SETTINGS = new String[]{"import", "initial-comment", "innerclass", "javadoc", "method", "tags"};
    }
}

