/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.netbeans.modules.editor.settings.storage.api.OverridePreferences
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.modules.editor.fold.ui.CustomizerWithDefaults;
import org.netbeans.modules.editor.fold.ui.FoldOptionsController;
import org.netbeans.modules.editor.fold.ui.VerticalFlowLayout;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.netbeans.modules.editor.settings.storage.api.OverridePreferences;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public final class DefaultFoldingOptions
extends JPanel
implements PreferenceChangeListener,
ChangeListener,
CustomizerWithDefaults,
ItemListener {
    private static final Logger LOG = Logger.getLogger(DefaultFoldingOptions.class.getName());
    public static final String COLLAPSE_PREFIX = "code-folding-collapse-";
    public static final String PREF_OVERRIDE_DEFAULTS = "code-folding-use-defaults";
    private static Set<FoldType> LEGACY_FOLD_TYPES = new HashSet<FoldType>();
    private String mimeType;
    private Preferences preferences;
    private Preferences defaultPrefs;
    private Collection<? extends FoldType> types;
    private Collection<JCheckBox> controls = new ArrayList<JCheckBox>();
    private PreferenceChangeListener weakL;
    private Collection<String> parentFoldTypes;
    private boolean isChanged = false;
    private boolean loaded;
    private boolean ignoreStateChange;
    private JCheckBox lastChangedCB;
    private JPanel collapseContainer;
    private JPanel localSwitchboard;

    public DefaultFoldingOptions(String mime, Preferences preferences) {
        this.initComponents();
        VerticalFlowLayout vfl = new VerticalFlowLayout();
        this.localSwitchboard.setLayout(vfl);
        vfl = new VerticalFlowLayout();
        this.localSwitchboard.setLayout(vfl);
        this.mimeType = mime;
        this.preferences = preferences;
        String parentMime = MimePath.parse((String)mime).getInheritedType();
        if (parentMime != null) {
            this.parentFoldTypes = new HashSet<String>(13);
            for (FoldType ft : FoldUtilities.getFoldTypes((String)parentMime).values()) {
                this.parentFoldTypes.add(ft.code());
            }
        } else {
            this.parentFoldTypes = Collections.emptyList();
        }
    }

    @Override
    public void setDefaultPreferences(Preferences pref) {
        if (this.defaultPrefs != null) {
            this.defaultPrefs.removePreferenceChangeListener(this.weakL);
        }
        this.defaultPrefs = pref;
        if (pref != null) {
            this.weakL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)pref);
            pref.addPreferenceChangeListener(this.weakL);
        }
    }

    private static String k(FoldType ft) {
        return "code-folding-collapse-" + ft.code();
    }

    private JCheckBox createCheckBox(FoldType ft) {
        return new JCheckBox();
    }

    private void filterUsedMimeTypes() {
        Set mimeTypes = EditorSettings.getDefault().getAllMimeTypes();
        HashSet<String> codes = new HashSet<String>();
        for (String mt : mimeTypes) {
            Collection fts = FoldUtilities.getFoldTypes((String)mt).values();
            for (FoldType ft : fts) {
                codes.add(ft.code());
                if (ft.parent() == null) continue;
                codes.add(ft.parent().code());
            }
        }
        Iterator<? extends FoldType> it = this.types.iterator();
        while (it.hasNext()) {
            FoldType ft = it.next();
            if (LEGACY_FOLD_TYPES.contains((Object)ft) || codes.contains(ft.code())) continue;
            it.remove();
        }
    }

    private void load() {
        boolean currentOverride;
        this.types = new ArrayList<FoldType>(FoldUtilities.getFoldTypes((String)this.mimeType).values());
        if ("".equals(this.mimeType)) {
            this.filterUsedMimeTypes();
        }
        boolean override = this.isCollapseRedefined();
        boolean bl = this.isDefinedLocally("code-folding-use-defaults") ? !this.preferences.getBoolean("code-folding-use-defaults", true) : (currentOverride = false);
        if (override != currentOverride) {
            this.updateOverrideChanged();
        }
        for (FoldType ft : this.types) {
            String name = ft.getLabel();
            JCheckBox cb = this.createCheckBox(ft);
            cb.setText(name);
            cb.putClientProperty("id", ft.code());
            cb.putClientProperty("type", (Object)ft);
            this.localSwitchboard.add(cb);
            this.controls.add(cb);
            cb.addItemListener(this);
        }
        this.preferences.addPreferenceChangeListener(this);
        this.updateEnabledState();
    }

    private boolean isCollapseRedefined() {
        for (FoldType ft : this.types) {
            String pref = DefaultFoldingOptions.k(ft);
            if (!((OverridePreferences)this.preferences).isOverriden(pref) || this.defaultPrefs != null && !this.parentFoldTypes.contains(ft.code()) && (ft.parent() == null || !this.parentFoldTypes.contains(ft.parent().code()))) continue;
            return true;
        }
        return false;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (!this.loaded) {
            this.load();
            this.updateEnabledState();
            this.updateValueState();
            this.loaded = true;
        }
    }

    @Override
    public void preferenceChange(final PreferenceChangeEvent evt) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DefaultFoldingOptions.this.fireChanged(DefaultFoldingOptions.this.updateCheckers(evt));
            }
        });
    }

    private void updateValueState() {
        this.ignoreStateChange = true;
        for (JCheckBox cb : this.controls) {
            FoldType ft = (FoldType)cb.getClientProperty("type");
            String k = "code-folding-collapse-" + ft.code();
            boolean val = this.isCollapseEnabled(ft);
            cb.setSelected(val);
        }
        this.ignoreStateChange = false;
    }

    private void updateEnabledState() {
        boolean foldEnable = this.preferences.getBoolean("code-folding-enable", true);
        boolean useDefaults = this.preferences.getBoolean("code-folding-use-defaults", true);
        for (JComponent c : this.controls) {
            FoldType ft = (FoldType)c.getClientProperty("type");
            boolean enable = true;
            if (this.defaultPrefs != null && useDefaults) {
                enable = !this.isDefinedDefault(ft);
            }
            c.setEnabled(enable &= foldEnable);
        }
    }

    boolean isChanged() {
        return this.isChanged;
    }

    private void fireChanged(String pk) {
        if (pk == null) {
            this.isChanged = false;
            return;
        }
        Preferences prefs = (Preferences)MimeLookup.getLookup((String)this.mimeType).lookup(Preferences.class);
        boolean changed = this.preferences.getBoolean("code-folding-enable", true) != prefs.getBoolean("code-folding-enable", true) || this.preferences.getBoolean("code-folding-use-defaults", true) != prefs.getBoolean("code-folding-use-defaults", true) || this.preferences.getBoolean("code-folding-content.preview", true) != prefs.getBoolean("code-folding-content.preview", true) || this.preferences.getBoolean("code-folding-content.summary", true) != prefs.getBoolean("code-folding-content.summary", true);
        for (JCheckBox cb : this.controls) {
            changed |= this.isFoldTypeChanged((FoldType)cb.getClientProperty("type"), prefs);
        }
        this.isChanged = changed;
    }

    private boolean isFoldTypeChanged(FoldType ft, Preferences prefs) {
        if (this.defaultPrefs == null) {
            return this.preferences.getBoolean(DefaultFoldingOptions.k(ft), ft.parent() == null ? false : this.preferences.getBoolean(DefaultFoldingOptions.k(ft.parent()), false)) != prefs.getBoolean(DefaultFoldingOptions.k(ft), ft.parent() == null ? false : prefs.getBoolean(DefaultFoldingOptions.k(ft.parent()), false));
        }
        String k = DefaultFoldingOptions.k(ft);
        return this.preferences.getBoolean(k, this.defaultPrefs.getBoolean(k, ft.parent() == null ? false : this.preferences.getBoolean(DefaultFoldingOptions.k(ft.parent()), false))) != prefs.getBoolean(k, this.defaultPrefs.getBoolean(k, ft.parent() == null ? false : prefs.getBoolean(DefaultFoldingOptions.k(ft.parent()), false)));
    }

    private String updateCheckers(PreferenceChangeEvent evt) {
        String pk = evt.getKey();
        if (pk != null) {
            if (pk.equals("code-folding-enable")) {
                this.updateEnabledState();
                return pk;
            }
            if (pk.equals("code-folding-use-defaults")) {
                this.updateOverrideChanged();
            } else if (!pk.startsWith("code-folding-collapse-")) {
                return pk;
            }
        } else {
            this.updateEnabledState();
        }
        String c = pk == null ? null : pk.substring("code-folding-collapse-".length());
        for (JCheckBox cb : this.controls) {
            FoldType ft = (FoldType)cb.getClientProperty("type");
            FoldType ftp = ft.parent();
            if (c != null && !ft.code().equals(c) && (ftp == null || !ftp.code().equals(c))) continue;
            this.updateChecker(pk, cb, ft);
            return pk;
        }
        return pk;
    }

    private boolean isCollapseEnabled(FoldType ft) {
        if (this.defaultPrefs == null) {
            return this.preferences.getBoolean(DefaultFoldingOptions.k(ft), ft.parent() == null ? false : this.preferences.getBoolean(DefaultFoldingOptions.k(ft.parent()), false));
        }
        String k = DefaultFoldingOptions.k(ft);
        return this.preferences.getBoolean(k, this.defaultPrefs.getBoolean(k, ft.parent() == null ? false : this.preferences.getBoolean(DefaultFoldingOptions.k(ft.parent()), false)));
    }

    private void updateOverrideChanged() {
        boolean en;
        boolean bl = en = !this.preferences.getBoolean("code-folding-use-defaults", true);
        if (this.defaultPrefs == null) {
            return;
        }
        if (en) {
            for (FoldType ft : this.types) {
                if (!this.isDefinedDefault(ft) && this.isDefinedLocally(DefaultFoldingOptions.k(ft))) continue;
                this.preferences.putBoolean(DefaultFoldingOptions.k(ft), this.defaultPrefs.getBoolean(DefaultFoldingOptions.k(ft), ft.parent() == null ? false : this.defaultPrefs.getBoolean(DefaultFoldingOptions.k(ft.parent()), false)));
            }
        } else {
            for (FoldType ft : this.types) {
                if (!this.isDefinedDefault(ft) || !this.isDefinedLocally(DefaultFoldingOptions.k(ft))) continue;
                this.preferences.remove(DefaultFoldingOptions.k(ft));
            }
        }
        this.updateEnabledState();
        this.updateValueState();
    }

    private boolean isDefinedDefault(FoldType ft) {
        return this.parentFoldTypes.contains(ft.code()) || ft.parent() != null && this.parentFoldTypes.contains(ft.parent().code());
    }

    private boolean isDefinedLocally(String prefKey) {
        return !(this.preferences instanceof OverridePreferences) || ((OverridePreferences)this.preferences).isOverriden(prefKey);
    }

    private void updateChecker(String prefKey, JCheckBox cb, FoldType ft) {
        if (this.lastChangedCB == cb) {
            this.lastChangedCB = null;
            return;
        }
        boolean val = this.isCollapseEnabled(ft);
        this.ignoreStateChange = true;
        LOG.log(Level.FINE, "Updating checker: " + prefKey + ", setSelected " + val);
        cb.setSelected(val);
        this.ignoreStateChange = false;
    }

    @Override
    public void itemStateChanged(final ItemEvent e) {
        if (this.ignoreStateChange) {
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                DefaultFoldingOptions.this.updatePref(e);
            }
        });
    }

    private void updatePref(ItemEvent e) {
        JCheckBox cb = (JCheckBox)e.getSource();
        FoldType ft = (FoldType)cb.getClientProperty("type");
        String prefKey = "code-folding-collapse-" + ft.code();
        this.lastChangedCB = cb;
        LOG.log(Level.FINE, "Updating preference: " + prefKey + ", value = " + cb.isSelected());
        this.preferences.putBoolean(prefKey, cb.isSelected());
        if (!"".equals(this.mimeType)) {
            return;
        }
        String propagate = FoldOptionsController.LEGACY_SETTINGS_MAP.get(ft.code());
        if (propagate != null) {
            prefKey = "code-folding-collapse-" + propagate;
            LOG.log(Level.FINE, "Updating LEGACY preference: " + prefKey + ", value = " + cb.isSelected());
            this.preferences.putBoolean(prefKey, cb.isSelected());
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (this.ignoreStateChange) {
            return;
        }
    }

    private void initComponents() {
        this.collapseContainer = new JPanel();
        this.localSwitchboard = new JPanel();
        this.collapseContainer.setBorder(BorderFactory.createTitledBorder(NbBundle.getMessage(DefaultFoldingOptions.class, (String)"DefaultFoldingOptions.collapseContainer.border.title")));
        this.collapseContainer.setLayout(new BorderLayout());
        this.localSwitchboard.setLayout(null);
        this.collapseContainer.add((Component)this.localSwitchboard, "Center");
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.collapseContainer, -1, 180, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.collapseContainer, -1, 151, 32767));
    }

    static {
        LEGACY_FOLD_TYPES.add(FoldType.CODE_BLOCK);
        LEGACY_FOLD_TYPES.add(FoldType.INITIAL_COMMENT);
        LEGACY_FOLD_TYPES.add(FoldType.DOCUMENTATION);
        LEGACY_FOLD_TYPES.add(FoldType.TAG);
        LEGACY_FOLD_TYPES.add(FoldType.MEMBER);
        LEGACY_FOLD_TYPES.add(FoldType.NESTED);
        LEGACY_FOLD_TYPES.add(FoldType.IMPORT);
    }

}

