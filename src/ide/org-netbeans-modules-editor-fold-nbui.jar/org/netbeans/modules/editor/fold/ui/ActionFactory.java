/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.editor.BaseAction
 *  org.openide.util.Exceptions
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Utilities;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.editor.BaseAction;
import org.openide.util.Exceptions;

final class ActionFactory {
    ActionFactory() {
    }

    private static Fold getLineFold(FoldHierarchy hierarchy, int dot, int lineStart, int lineEnd) {
        Fold nextFold;
        Fold caretOffsetFold = FoldUtilities.findOffsetFold((FoldHierarchy)hierarchy, (int)dot);
        Fold fold = FoldUtilities.findNearestFold((FoldHierarchy)hierarchy, (int)lineStart);
        while (fold != null && (fold.getEndOffset() <= dot || !fold.isCollapsed() && fold.getFoldCount() > 0 && fold.getStartOffset() + 1 < dot) && (nextFold = FoldUtilities.findNearestFold((FoldHierarchy)hierarchy, (int)(fold.getFoldCount() > 0 ? fold.getStartOffset() + 1 : fold.getEndOffset()))) != null && nextFold.getStartOffset() < lineEnd) {
            if (nextFold == fold) {
                return fold;
            }
            fold = nextFold;
        }
        if (fold == null || fold.getStartOffset() > lineEnd) {
            if (caretOffsetFold == null) {
                caretOffsetFold = FoldUtilities.findOffsetFold((FoldHierarchy)hierarchy, (int)lineStart);
            }
            return caretOffsetFold;
        }
        if (caretOffsetFold == null) {
            return fold;
        }
        if (caretOffsetFold.isCollapsed()) {
            return caretOffsetFold;
        }
        if (caretOffsetFold.getEndOffset() > fold.getEndOffset() && fold.getEndOffset() > dot) {
            return fold;
        }
        if (fold.getStartOffset() > caretOffsetFold.getEndOffset()) {
            return caretOffsetFold;
        }
        if (fold.getEndOffset() < dot) {
            return caretOffsetFold;
        }
        return fold;
    }

    static abstract class LocalBaseAction
    extends BaseAction {
        protected Class getShortDescriptionBundleClass() {
            return ActionFactory.class;
        }
    }

    public static class ExpandAllFolds
    extends LocalBaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            FoldUtilities.expandAll((FoldHierarchy)hierarchy);
        }
    }

    public static class CollapseAllFolds
    extends LocalBaseAction {
        public void actionPerformed(ActionEvent evt, JTextComponent target) {
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
            FoldUtilities.collapseAll((FoldHierarchy)hierarchy);
        }
    }

    public static class CollapseFoldsTree
    extends LocalBaseAction {
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            Document doc = target.getDocument();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Fold fold;
                    FoldHierarchy hierarchy;
                    block6 : {
                        hierarchy = FoldHierarchy.get((JTextComponent)target);
                        int dot = target.getCaret().getDot();
                        hierarchy.lock();
                        int rowStart = Utilities.getRowStart(target, dot);
                        int rowEnd = Utilities.getRowEnd(target, dot);
                        fold = ActionFactory.getLineFold(hierarchy, dot, rowStart, rowEnd);
                        if (fold != null) break block6;
                        hierarchy.unlock();
                        return;
                    }
                    try {
                        try {
                            ArrayList<Fold> allFolds = new ArrayList<Fold>(FoldUtilities.findRecursive((Fold)fold));
                            Collections.reverse(allFolds);
                            allFolds.add(0, fold);
                            hierarchy.collapse(allFolds);
                        }
                        catch (BadLocationException ble) {
                            Exceptions.printStackTrace((Throwable)ble);
                        }
                    }
                    catch (Throwable var7_8) {
                        throw var7_8;
                    }
                    finally {
                        hierarchy.unlock();
                    }
                }
            });
        }

    }

    public static class ExpandFoldsTree
    extends LocalBaseAction {
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            Document doc = target.getDocument();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Fold fold;
                    FoldHierarchy hierarchy;
                    block6 : {
                        hierarchy = FoldHierarchy.get((JTextComponent)target);
                        int dot = target.getCaret().getDot();
                        hierarchy.lock();
                        int rowStart = Utilities.getRowStart(target, dot);
                        int rowEnd = Utilities.getRowEnd(target, dot);
                        fold = ActionFactory.getLineFold(hierarchy, dot, rowStart, rowEnd);
                        if (fold != null) break block6;
                        hierarchy.unlock();
                        return;
                    }
                    try {
                        try {
                            ArrayList<Fold> allFolds = new ArrayList<Fold>(FoldUtilities.findRecursive((Fold)fold));
                            Collections.reverse(allFolds);
                            allFolds.add(0, fold);
                            hierarchy.expand(allFolds);
                        }
                        catch (BadLocationException ble) {
                            Exceptions.printStackTrace((Throwable)ble);
                        }
                    }
                    catch (Throwable var7_8) {
                        throw var7_8;
                    }
                    finally {
                        hierarchy.unlock();
                    }
                }
            });
        }

    }

    public static class ExpandFold
    extends LocalBaseAction {
        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            Document doc = target.getDocument();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)target);
                    int dot = target.getCaret().getDot();
                    hierarchy.lock();
                    try {
                        try {
                            int rowStart = Utilities.getRowStart(target, dot);
                            int rowEnd = Utilities.getRowEnd(target, dot);
                            Fold fold = ActionFactory.getLineFold(hierarchy, dot, rowStart, rowEnd);
                            if (fold != null) {
                                hierarchy.expand(fold);
                            }
                        }
                        catch (BadLocationException ble) {
                            Exceptions.printStackTrace((Throwable)ble);
                        }
                    }
                    finally {
                        hierarchy.unlock();
                    }
                }
            });
        }

    }

    public static class CollapseFold
    extends LocalBaseAction {
        private boolean dotInFoldArea(JTextComponent target, Fold fold, int dot) throws BadLocationException {
            int foldStart = fold.getStartOffset();
            int foldEnd = fold.getEndOffset();
            int foldRowStart = Utilities.getRowStart(target, foldStart);
            int foldRowEnd = Utilities.getRowEnd(target, foldEnd);
            if (foldRowStart > dot || foldRowEnd < dot) {
                return false;
            }
            return true;
        }

        public void actionPerformed(ActionEvent evt, final JTextComponent target) {
            Document doc = target.getDocument();
            doc.render(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    int dot;
                    Fold fold;
                    FoldHierarchy hierarchy;
                    block7 : {
                        hierarchy = FoldHierarchy.get((JTextComponent)target);
                        dot = target.getCaret().getDot();
                        hierarchy.lock();
                        int rowStart = Utilities.getRowStart(target, dot);
                        int rowEnd = Utilities.getRowEnd(target, dot);
                        fold = FoldUtilities.findNearestFold((FoldHierarchy)hierarchy, (int)rowStart);
                        fold = ActionFactory.getLineFold(hierarchy, dot, rowStart, rowEnd);
                        if (fold != null) break block7;
                        hierarchy.unlock();
                        return;
                    }
                    try {
                        try {
                            if (CollapseFold.this.dotInFoldArea(target, fold, dot)) {
                                hierarchy.collapse(fold);
                            }
                        }
                        catch (BadLocationException ble) {
                            Exceptions.printStackTrace((Throwable)ble);
                        }
                    }
                    catch (Throwable var6_7) {
                        throw var6_7;
                    }
                    finally {
                        hierarchy.unlock();
                    }
                }
            });
        }

    }

}

