/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.modules.editor.lib2.view.DocumentView
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.lib2.view.DocumentView;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

final class FoldToolTip
extends JPanel {
    private int editorPaneWidth;

    public FoldToolTip(JEditorPane editorPane, final JEditorPane foldPreviewPane, Color borderColor) {
        this.setLayout(new BorderLayout());
        this.add((Component)foldPreviewPane, "Center");
        this.putClientProperty("tooltip-type", "fold-preview");
        this.addGlyphGutter(foldPreviewPane);
        this.addAncestorListener(new AncestorListener(){

            @Override
            public void ancestorAdded(AncestorEvent event) {
            }

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                final DocumentView docView = DocumentView.get((JTextComponent)foldPreviewPane);
                if (docView != null) {
                    docView.runTransaction(new Runnable(){

                        @Override
                        public void run() {
                            docView.updateLengthyAtomicEdit(100);
                        }
                    });
                }
                FoldToolTip.this.removeAncestorListener(this);
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {
            }

        });
        this.editorPaneWidth = editorPane.getSize().width;
        this.setBorder(new LineBorder(borderColor));
        this.setOpaque(true);
    }

    private void addGlyphGutter(JTextComponent jtx) {
        ClassLoader cls = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        JComponent gutter = null;
        try {
            Class clazz = Class.forName("org.netbeans.editor.GlyphGutter", true, cls);
            Class editorUiClass = Class.forName("org.netbeans.editor.EditorUI", true, cls);
            Object o = clazz.newInstance();
            Method m = clazz.getDeclaredMethod("createSideBar", JTextComponent.class);
            gutter = (JComponent)m.invoke(o, jtx);
        }
        catch (IllegalArgumentException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InvocationTargetException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (SecurityException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (InstantiationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (IllegalAccessException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        catch (ClassNotFoundException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        if (gutter != null) {
            this.add((Component)gutter, "West");
        }
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension prefSize = super.getPreferredSize();
        prefSize.width = Math.min(prefSize.width, this.editorPaneWidth);
        return prefSize;
    }

}

