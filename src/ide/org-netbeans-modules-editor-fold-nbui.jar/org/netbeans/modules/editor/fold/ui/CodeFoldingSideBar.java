/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldHierarchyEvent
 *  org.netbeans.api.editor.fold.FoldHierarchyListener
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseDocumentEvent
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.Coloring
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.view.LockedViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ParagraphViewDescriptor
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchy
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent
 *  org.netbeans.modules.editor.lib2.view.ViewHierarchyListener
 *  org.netbeans.spi.editor.SideBarFactory
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.accessibility.AccessibleRole;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.TextUI;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseDocumentEvent;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Coloring;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.lib2.view.LockedViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ParagraphViewDescriptor;
import org.netbeans.modules.editor.lib2.view.ViewHierarchy;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyEvent;
import org.netbeans.modules.editor.lib2.view.ViewHierarchyListener;
import org.netbeans.spi.editor.SideBarFactory;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public final class CodeFoldingSideBar
extends JComponent
implements Accessible {
    public static final String PROP_SIDEBAR_MARK = "org.netbeans.editor.CodeFoldingSidebar";
    private static final Logger PREF_LOG = Logger.getLogger(FoldHierarchy.class.getName() + ".enabled");
    private static final Logger LOG = Logger.getLogger(CodeFoldingSideBar.class.getName());
    protected Color backColor;
    protected Color foreColor;
    protected Font font;
    protected JTextComponent component;
    private volatile AttributeSet attribs;
    private Lookup.Result<? extends FontColorSettings> fcsLookupResult;
    private final LookupListener fcsTracker;
    private final Listener listener;
    private boolean enabled;
    protected List<Mark> visibleMarks;
    private int mousePoint;
    private boolean mousePointConsumed;
    private Rectangle mouseBoundary;
    private int lowestAboveMouse;
    private int topmostBelowMouse;
    private boolean alreadyPresent;
    public static final int PAINT_NOOP = 0;
    public static final int PAINT_MARK = 1;
    public static final int PAINT_LINE = 2;
    public static final int PAINT_END_MARK = 3;
    public static final int SINGLE_PAINT_MARK = 4;
    private static final int NO_MOUSE_POINT = -1;
    private static Stroke LINE_DASHED = new BasicStroke(1.0f, 0, 0, 1.0f, new float[]{1.0f, 1.0f}, 0.0f);
    private static final Stroke LINE_BOLD = new BasicStroke(2.0f, 1, 0);
    private final Preferences prefs;
    private final PreferenceChangeListener prefsListener;

    private void checkRepaint(ViewHierarchyEvent vhe) {
        if (!vhe.isChangeY()) {
            return;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                CodeFoldingSideBar.this.updatePreferredSize();
                CodeFoldingSideBar.this.repaint();
            }
        });
    }

    private static boolean canDisplay(JTextComponent component) {
        Object o = component.getClientProperty("org.netbeans.editor.CodeFoldingSidebar");
        return o == null || o instanceof JComponent && !((JComponent)o).isVisible();
    }

    @Override
    public void removeNotify() {
        Object o = this.component.getClientProperty("org.netbeans.editor.CodeFoldingSidebar");
        if (o == this) {
            this.component.putClientProperty("org.netbeans.editor.CodeFoldingSidebar", null);
        }
        super.removeNotify();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        Object o = this.component.getClientProperty("org.netbeans.editor.CodeFoldingSidebar");
        if (o == null) {
            this.component.putClientProperty("org.netbeans.editor.CodeFoldingSidebar", this);
        }
    }

    public CodeFoldingSideBar(JTextComponent component) {
        this.fcsTracker = new LookupListener(){

            public void resultChanged(LookupEvent ev) {
                CodeFoldingSideBar.this.attribs = null;
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        CodeFoldingSideBar.this.updatePreferredSize();
                        CodeFoldingSideBar.this.repaint();
                    }
                });
            }

        };
        this.listener = new Listener();
        this.enabled = false;
        this.visibleMarks = new ArrayList<Mark>();
        this.mousePoint = -1;
        this.lowestAboveMouse = -1;
        this.topmostBelowMouse = Integer.MAX_VALUE;
        this.prefsListener = new PreferenceChangeListener(){

            @Override
            public void preferenceChange(PreferenceChangeEvent evt) {
                String key;
                String string = key = evt == null ? null : evt.getKey();
                if (key == null || "code-folding-enable".equals(key)) {
                    CodeFoldingSideBar.this.updateColors();
                    boolean newEnabled = CodeFoldingSideBar.this.prefs.getBoolean("code-folding-enable", true);
                    PREF_LOG.log(Level.FINE, "Sidebar folding-enable pref change: " + newEnabled);
                    if (CodeFoldingSideBar.this.enabled != newEnabled) {
                        CodeFoldingSideBar.this.enabled = newEnabled;
                        CodeFoldingSideBar.this.updatePreferredSize();
                    }
                }
            }
        };
        this.component = component;
        if (!CodeFoldingSideBar.canDisplay(component)) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Folding sidebar already present at {0}", component);
            }
            this.alreadyPresent = true;
            this.prefs = null;
            return;
        }
        component.putClientProperty("org.netbeans.editor.CodeFoldingSidebar", this);
        this.addMouseListener(this.listener);
        this.addMouseMotionListener(this.listener);
        FoldHierarchy foldHierarchy = FoldHierarchy.get((JTextComponent)component);
        foldHierarchy.addFoldHierarchyListener((FoldHierarchyListener)WeakListeners.create(FoldHierarchyListener.class, (EventListener)this.listener, (Object)foldHierarchy));
        Document doc = this.getDocument();
        doc.addDocumentListener(WeakListeners.document((DocumentListener)this.listener, (Object)doc));
        this.setOpaque(true);
        this.prefs = (Preferences)MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)component)).lookup(Preferences.class);
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this.prefsListener, (Object)this.prefs));
        this.prefsListener.preferenceChange(null);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Code folding sidebar initialized for: {0}", doc);
        }
        ViewHierarchy.get((JTextComponent)component).addViewHierarchyListener(new ViewHierarchyListener(){

            public void viewHierarchyChanged(ViewHierarchyEvent evt) {
                CodeFoldingSideBar.this.checkRepaint(evt);
            }
        });
    }

    private void updatePreferredSize() {
        if (this.enabled && !this.alreadyPresent) {
            this.setPreferredSize(new Dimension(this.getColoring().getFont().getSize(), this.component.getHeight()));
            this.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        } else {
            this.setPreferredSize(new Dimension(0, 0));
            this.setMaximumSize(new Dimension(0, 0));
        }
        this.revalidate();
    }

    private void updateColors() {
        Coloring c = this.getColoring();
        this.backColor = c.getBackColor();
        this.foreColor = c.getForeColor();
        this.font = c.getFont();
    }

    protected Color getBackColor() {
        if (this.backColor == null) {
            this.updateColors();
        }
        return this.backColor;
    }

    protected Color getForeColor() {
        if (this.foreColor == null) {
            this.updateColors();
        }
        return this.foreColor;
    }

    protected Font getColoringFont() {
        if (this.font == null) {
            this.updateColors();
        }
        return this.font;
    }

    @Override
    public void update(Graphics g) {
    }

    protected void collectPaintInfos(View rootView, Fold fold, Map<Integer, PaintInfo> map, int level, int startIndex, int endIndex) throws BadLocationException {
    }

    private void setMouseBoundaries(int y1, int y2, int level) {
        if (!this.hasMousePoint() || this.mousePointConsumed) {
            return;
        }
        int y = this.mousePoint;
        if (y2 < y && this.lowestAboveMouse < y2) {
            LOG.log(Level.FINEST, "lowestAbove at {1}: {0}", new Object[]{y2, level});
            this.lowestAboveMouse = y2;
        }
        if (y1 > y && this.topmostBelowMouse > y1) {
            LOG.log(Level.FINEST, "topmostBelow at {1}: {0}", new Object[]{y1, level});
            this.topmostBelowMouse = y1;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected List<? extends PaintInfo> getPaintInfo(Rectangle clip) throws BadLocationException {
        TextUI textUI = this.component.getUI();
        if (!(textUI instanceof BaseTextUI)) {
            return Collections.emptyList();
        }
        BaseTextUI baseTextUI = (BaseTextUI)textUI;
        BaseDocument bdoc = Utilities.getDocument((JTextComponent)this.component);
        if (bdoc == null) {
            return Collections.emptyList();
        }
        this.mousePointConsumed = false;
        this.mouseBoundary = null;
        this.topmostBelowMouse = Integer.MAX_VALUE;
        this.lowestAboveMouse = -1;
        bdoc.readLock();
        try {
            FoldHierarchy hierarchy;
            block22 : {
                TreeMap<Integer, PaintInfo> map;
                block23 : {
                    Object fold;
                    int startPos = baseTextUI.getPosFromY(clip.y);
                    int endPos = baseTextUI.viewToModel(this.component, 16383, clip.y + clip.height);
                    if (startPos < 0 || endPos < 0) {
                        List list = Collections.emptyList();
                        return list;
                    }
                    int docLen = bdoc.getLength();
                    if (startPos >= docLen || endPos > docLen) {
                        List list = Collections.emptyList();
                        return list;
                    }
                    startPos = Utilities.getRowStart((BaseDocument)bdoc, (int)startPos);
                    endPos = Utilities.getRowEnd((BaseDocument)bdoc, (int)endPos);
                    hierarchy = FoldHierarchy.get((JTextComponent)this.component);
                    hierarchy.lock();
                    try {
                        int i;
                        View rootView = Utilities.getDocumentView((JTextComponent)this.component);
                        if (rootView == null) break block22;
                        Object[] arr = CodeFoldingSideBar.getFoldList(hierarchy.getRootFold(), startPos, endPos);
                        List foldList = (List)arr[0];
                        int idxOfFirstFoldStartingInsideClip = (Integer)arr[1];
                        map = new TreeMap<Integer, PaintInfo>();
                        for (i = idxOfFirstFoldStartingInsideClip - 1; i >= 0 && this.traverseBackwards((Fold)(fold = (Fold)foldList.get(i)), bdoc, baseTextUI, startPos, endPos, 0, map); --i) {
                        }
                        for (i = idxOfFirstFoldStartingInsideClip; i < foldList.size() && this.traverseForward((Fold)(fold = (Fold)foldList.get(i)), bdoc, baseTextUI, startPos, endPos, 0, map); ++i) {
                        }
                        if (!map.isEmpty() || foldList.size() <= 0) break block23;
                        if (foldList.size() != 1) {
                            LOG.log(Level.WARNING, "More folds found on screen, but no fold in paint map. foldList: {0},foldHierarchy: {1}", new Object[]{foldList, hierarchy.toString()});
                        }
                        assert (foldList.size() == 1);
                        PaintInfo pi = new PaintInfo(2, 0, clip.y, clip.height, -1, -1);
                        this.mouseBoundary = new Rectangle(0, 0, 0, clip.height);
                        LOG.log(Level.FINEST, "Mouse boundary for full side line set to: {0}", this.mouseBoundary);
                        if (this.hasMousePoint()) {
                            pi.markActive(true, true, true);
                        }
                        fold = Collections.singletonList(pi);
                    }
                    catch (Throwable var16_21) {
                        hierarchy.unlock();
                        throw var16_21;
                    }
                    hierarchy.unlock();
                    return fold;
                }
                if (this.mouseBoundary == null) {
                    this.mouseBoundary = this.makeMouseBoundary(clip.y, clip.y + clip.height);
                    LOG.log(Level.FINEST, "Mouse boundary not set, defaulting to: {0}", this.mouseBoundary);
                }
                ArrayList<PaintInfo> pi = new ArrayList<PaintInfo>(map.values());
                hierarchy.unlock();
                return pi;
            }
            List arr = Collections.emptyList();
            hierarchy.unlock();
            return arr;
        }
        finally {
            bdoc.readUnlock();
        }
    }

    private void addPaintInfo(Map<Integer, PaintInfo> infos, int yOffset, PaintInfo nextInfo) {
        PaintInfo prevInfo = infos.get(yOffset);
        nextInfo.mergeWith(prevInfo);
        infos.put(yOffset, nextInfo);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean traverseForward(Fold f, BaseDocument doc, BaseTextUI btui, int lowerBoundary, int upperBoundary, int level, NavigableMap<Integer, PaintInfo> infos) throws BadLocationException {
        boolean activated;
        PaintInfo spi;
        if (f.getStartOffset() > upperBoundary) {
            return false;
        }
        int lineStartOffset1 = Utilities.getRowStart((BaseDocument)doc, (int)f.getStartOffset());
        int lineStartOffset2 = Utilities.getRowStart((BaseDocument)doc, (int)f.getEndOffset());
        int y1 = btui.getYFromPos(lineStartOffset1);
        int h = btui.getEditorUI().getLineHeight();
        int y2 = btui.getYFromPos(lineStartOffset2);
        boolean activeMark = false;
        boolean activeIn = false;
        boolean activeOut = false;
        if (y1 == y2) {
            spi = new PaintInfo(4, level, y1, h, f.isCollapsed(), lineStartOffset1, lineStartOffset2);
            activated = this.isActivated(y1, y1 + h);
            if (activated) {
                activeMark = true;
            }
            this.addPaintInfo(infos, y1, spi);
        } else {
            spi = new PaintInfo(1, level, y1, h, f.isCollapsed(), lineStartOffset1, lineStartOffset2);
            activated = this.isActivated(y1, y2 + h / 2);
            if (activated) {
                activeMark = true;
                activeOut = true;
            }
            this.addPaintInfo(infos, y1, spi);
        }
        this.setMouseBoundaries(y1, y2 + h / 2, level);
        PaintInfo epi = null;
        if (y1 != y2 && !f.isCollapsed() && f.getEndOffset() <= upperBoundary) {
            epi = new PaintInfo(3, level, y2, h, lineStartOffset1, lineStartOffset2);
            this.addPaintInfo(infos, y2, epi);
        }
        int topmost = this.topmostBelowMouse;
        int lowest = this.lowestAboveMouse;
        this.topmostBelowMouse = y2 + h / 2;
        this.lowestAboveMouse = y1;
        try {
            if (!f.isCollapsed()) {
                int i;
                Fold fold;
                Object[] arr = CodeFoldingSideBar.getFoldList(f, lowerBoundary, upperBoundary);
                List foldList = (List)arr[0];
                int idxOfFirstFoldStartingInsideClip = (Integer)arr[1];
                for (i = idxOfFirstFoldStartingInsideClip - 1; i >= 0 && this.traverseBackwards(fold = (Fold)foldList.get(i), doc, btui, lowerBoundary, upperBoundary, level + 1, infos); --i) {
                }
                for (i = idxOfFirstFoldStartingInsideClip; i < foldList.size(); ++i) {
                    fold = (Fold)foldList.get(i);
                    if (this.traverseForward(fold, doc, btui, lowerBoundary, upperBoundary, level + 1, infos)) continue;
                    boolean bl = false;
                    return bl;
                }
            }
            if (!this.mousePointConsumed && activated) {
                this.mousePointConsumed = true;
                this.mouseBoundary = this.makeMouseBoundary(y1, y2 + h);
                LOG.log(Level.FINEST, "Mouse boundary set to: {0}", this.mouseBoundary);
                spi.markActive(activeMark, activeIn, activeOut);
                if (epi != null) {
                    epi.markActive(true, true, false);
                }
                this.markDeepChildrenActive(infos, y1, y2, level);
            }
        }
        finally {
            this.topmostBelowMouse = topmost;
            this.lowestAboveMouse = lowest;
        }
        return true;
    }

    private int markDeepChildrenActive(NavigableMap<Integer, PaintInfo> infos, int yFrom, int yTo, int level) {
        int result = Integer.MAX_VALUE;
        SortedMap<Integer, PaintInfo> m = infos.subMap(yFrom, yTo);
        for (Map.Entry<Integer, PaintInfo> me : m.entrySet()) {
            PaintInfo pi = me.getValue();
            int y = pi.getPaintY();
            if (y <= yFrom || y >= yTo) continue;
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.log(Level.FINEST, "Marking chind as active: {0}", pi);
            }
            pi.markActive(false, true, true);
            if (y >= result) continue;
            y = result;
        }
        return result;
    }

    private static Stroke getStroke(Stroke s, boolean active) {
        if (active) {
            return LINE_BOLD;
        }
        return s;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean traverseBackwards(Fold f, BaseDocument doc, BaseTextUI btui, int lowerBoundary, int upperBoundary, int level, NavigableMap<Integer, PaintInfo> infos) throws BadLocationException {
        if (f.getEndOffset() < lowerBoundary) {
            return false;
        }
        int lineStartOffset1 = Utilities.getRowStart((BaseDocument)doc, (int)f.getStartOffset());
        int lineStartOffset2 = Utilities.getRowStart((BaseDocument)doc, (int)f.getEndOffset());
        int h = btui.getEditorUI().getLineHeight();
        boolean activeMark = false;
        boolean activeIn = false;
        boolean activeOut = false;
        PaintInfo spi = null;
        PaintInfo epi = null;
        boolean activated = false;
        int y1 = 0;
        int y2 = 0;
        if (lineStartOffset1 == lineStartOffset2) {
            y2 = y1 = btui.getYFromPos(lineStartOffset1);
            spi = new PaintInfo(4, level, y1, h, f.isCollapsed(), lineStartOffset1, lineStartOffset1);
            activated = this.isActivated(y1, y1 + h);
            if (activated) {
                activeMark = true;
            }
            this.addPaintInfo(infos, y1, spi);
        } else {
            y2 = btui.getYFromPos(lineStartOffset2);
            y1 = btui.getYFromPos(lineStartOffset1);
            activated = this.isActivated(y1, y2 + h / 2);
            if (f.getStartOffset() >= upperBoundary) {
                spi = new PaintInfo(1, level, y1, h, f.isCollapsed(), lineStartOffset1, lineStartOffset2);
                if (activated) {
                    activeMark = true;
                    activeOut = true;
                }
                this.addPaintInfo(infos, y1, spi);
            }
            if (!f.isCollapsed() && f.getEndOffset() <= upperBoundary) {
                activated |= this.isActivated(y1, y2 + h / 2);
                epi = new PaintInfo(3, level, y2, h, lineStartOffset1, lineStartOffset2);
                this.addPaintInfo(infos, y2, epi);
            }
        }
        this.setMouseBoundaries(y1, y2 + h / 2, level);
        int topmost = this.topmostBelowMouse;
        int lowest = this.lowestAboveMouse;
        this.topmostBelowMouse = y2 + h / 2;
        this.lowestAboveMouse = y1;
        try {
            if (!f.isCollapsed()) {
                int i;
                Fold fold;
                Object[] arr = CodeFoldingSideBar.getFoldList(f, lowerBoundary, upperBoundary);
                List foldList = (List)arr[0];
                int idxOfFirstFoldStartingInsideClip = (Integer)arr[1];
                for (i = idxOfFirstFoldStartingInsideClip - 1; i >= 0; --i) {
                    fold = (Fold)foldList.get(i);
                    if (this.traverseBackwards(fold, doc, btui, lowerBoundary, upperBoundary, level + 1, infos)) continue;
                    boolean bl = false;
                    return bl;
                }
                for (i = idxOfFirstFoldStartingInsideClip; i < foldList.size() && this.traverseForward(fold = (Fold)foldList.get(i), doc, btui, lowerBoundary, upperBoundary, level + 1, infos); ++i) {
                }
            }
            if (!this.mousePointConsumed && activated) {
                int lowestChild;
                this.mousePointConsumed = true;
                this.mouseBoundary = this.makeMouseBoundary(y1, y2 + h);
                LOG.log(Level.FINEST, "Mouse boundary set to: {0}", this.mouseBoundary);
                if (spi != null) {
                    spi.markActive(activeMark, activeIn, activeOut);
                }
                if (epi != null) {
                    epi.markActive(true, true, false);
                }
                if ((lowestChild = this.markDeepChildrenActive(infos, y1, y2, level)) < Integer.MAX_VALUE && lineStartOffset1 < upperBoundary) {
                    epi = new PaintInfo(2, level, y1, y2 - y1, false, lineStartOffset1, lineStartOffset2);
                    epi.markActive(true, true, false);
                    this.addPaintInfo(infos, y1, epi);
                }
            }
        }
        finally {
            this.topmostBelowMouse = topmost;
            this.lowestAboveMouse = lowest;
        }
        return true;
    }

    private Rectangle makeMouseBoundary(int y1, int y2) {
        if (!this.hasMousePoint()) {
            return null;
        }
        if (this.topmostBelowMouse < Integer.MAX_VALUE) {
            y2 = this.topmostBelowMouse;
        }
        if (this.lowestAboveMouse > -1) {
            y1 = this.lowestAboveMouse;
        }
        return new Rectangle(0, y1, 0, y2 - y1);
    }

    protected EditorUI getEditorUI() {
        return Utilities.getEditorUI((JTextComponent)this.component);
    }

    protected Document getDocument() {
        return this.component.getDocument();
    }

    private Fold getLastLineFold(FoldHierarchy hierarchy, int rowStart, int rowEnd, boolean shift) {
        Fold fold;
        Fold prevFold = fold = FoldUtilities.findNearestFold((FoldHierarchy)hierarchy, (int)rowStart);
        while (fold != null && fold.getStartOffset() < rowEnd) {
            Fold nextFold = FoldUtilities.findNearestFold((FoldHierarchy)hierarchy, (int)(fold.isCollapsed() ? fold.getEndOffset() : fold.getStartOffset() + 1));
            if (nextFold == fold) {
                return fold;
            }
            if (nextFold != null && nextFold.getStartOffset() < rowEnd) {
                prevFold = shift ? fold : nextFold;
                fold = nextFold;
                continue;
            }
            return prevFold;
        }
        return prevFold;
    }

    protected void performAction(Mark mark) {
        this.performAction(mark, false, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void performActionAt(Mark mark, int mouseY) throws BadLocationException {
        if (mark != null) {
            return;
        }
        BaseDocument bdoc = Utilities.getDocument((JTextComponent)this.component);
        BaseTextUI textUI = (BaseTextUI)this.component.getUI();
        View rootView = Utilities.getDocumentView((JTextComponent)this.component);
        if (rootView == null) {
            return;
        }
        bdoc.readLock();
        try {
            int yOffset = textUI.getPosFromY(mouseY);
            FoldHierarchy hierarchy = FoldHierarchy.get((JTextComponent)this.component);
            hierarchy.lock();
            try {
                int h2;
                Fold f = FoldUtilities.findOffsetFold((FoldHierarchy)hierarchy, (int)yOffset);
                if (f == null) {
                    return;
                }
                if (f.isCollapsed()) {
                    LOG.log(Level.WARNING, "Clicked on a collapsed fold {0} at {1}", new Object[]{f, mouseY});
                    return;
                }
                int startOffset = f.getStartOffset();
                int endOffset = f.getEndOffset();
                int startY = textUI.getYFromPos(startOffset);
                int nextLineOffset = Utilities.getRowStart((BaseDocument)bdoc, (int)startOffset, (int)1);
                int nextY = textUI.getYFromPos(nextLineOffset);
                if (mouseY >= startY && mouseY <= nextY) {
                    LOG.log(Level.FINEST, "Starting line clicked, ignoring. MouseY={0}, startY={1}, nextY={2}", new Object[]{mouseY, startY, nextY});
                    return;
                }
                startY = textUI.getYFromPos(endOffset);
                nextLineOffset = Utilities.getRowStart((BaseDocument)bdoc, (int)endOffset, (int)1);
                nextY = textUI.getYFromPos(nextLineOffset);
                if (mouseY >= startY && mouseY <= nextY && mouseY >= (h2 = (startY + nextY) / 2)) {
                    Fold f2 = f;
                    f = FoldUtilities.findOffsetFold((FoldHierarchy)hierarchy, (int)nextLineOffset);
                    if (f == null) {
                        return;
                    }
                }
                LOG.log(Level.FINEST, "Collapsing fold: {0}", (Object)f);
                hierarchy.collapse(f);
            }
            finally {
                hierarchy.unlock();
            }
        }
        finally {
            bdoc.readUnlock();
        }
    }

    private void performAction(final Mark mark, final boolean shiftFold, final boolean recursive) {
        Document doc = this.component.getDocument();
        doc.render(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                block11 : {
                    ViewHierarchy vh = ViewHierarchy.get((JTextComponent)CodeFoldingSideBar.this.component);
                    LockedViewHierarchy lockedVH = vh.lock();
                    try {
                        int pViewIndex = lockedVH.yToParagraphViewIndex((double)(mark.y + mark.size / 2));
                        if (pViewIndex < 0) break block11;
                        ParagraphViewDescriptor pViewDesc = lockedVH.getParagraphViewDescriptor(pViewIndex);
                        int pViewStartOffset = pViewDesc.getStartOffset();
                        int pViewEndOffset = pViewStartOffset + pViewDesc.getLength();
                        FoldHierarchy foldHierarchy = FoldHierarchy.get((JTextComponent)CodeFoldingSideBar.this.component);
                        foldHierarchy.lock();
                        try {
                            int rowStart = javax.swing.text.Utilities.getRowStart(CodeFoldingSideBar.this.component, pViewStartOffset);
                            int rowEnd = javax.swing.text.Utilities.getRowEnd(CodeFoldingSideBar.this.component, pViewStartOffset);
                            Fold clickedFold = CodeFoldingSideBar.this.getLastLineFold(foldHierarchy, rowStart, rowEnd, shiftFold);
                            if (clickedFold == null || clickedFold.getStartOffset() >= pViewEndOffset) break block11;
                            if (recursive) {
                                ArrayList<Fold> folds = new ArrayList<Fold>(FoldUtilities.findRecursive((Fold)clickedFold));
                                Collections.reverse(folds);
                                folds.add(clickedFold);
                                if (clickedFold.isCollapsed()) {
                                    foldHierarchy.expand(folds);
                                } else {
                                    foldHierarchy.collapse(folds);
                                }
                                break block11;
                            }
                            foldHierarchy.toggle(clickedFold);
                        }
                        catch (BadLocationException ble) {
                            LOG.log(Level.WARNING, null, ble);
                        }
                        finally {
                            foldHierarchy.unlock();
                        }
                    }
                    finally {
                        lockedVH.unlock();
                    }
                }
            }
        });
    }

    protected int getMarkSize(Graphics g) {
        FontMetrics fm;
        if (g != null && (fm = g.getFontMetrics(this.getColoring().getFont())) != null) {
            int ret = fm.getAscent() - fm.getDescent();
            return ret - ret % 2;
        }
        return -1;
    }

    private boolean hasMousePoint() {
        return this.mousePoint >= 0;
    }

    private boolean isActivated(int y1, int y2) {
        return this.hasMousePoint() && this.mousePoint >= y1 && this.mousePoint < y2;
    }

    private void drawFoldLine(Graphics2D g2d, boolean active, int x1, int y1, int x2, int y2) {
        Stroke origStroke = g2d.getStroke();
        g2d.setStroke(CodeFoldingSideBar.getStroke(origStroke, active));
        g2d.drawLine(x1, y1, x2, y2);
        g2d.setStroke(origStroke);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void paintComponent(Graphics g) {
        if (!this.enabled) {
            return;
        }
        Rectangle clip = this.getVisibleRect();
        this.visibleMarks.clear();
        Coloring coloring = this.getColoring();
        g.setColor(coloring.getBackColor());
        g.fillRect(clip.x, clip.y, clip.width, clip.height);
        g.setColor(coloring.getForeColor());
        AbstractDocument adoc = (AbstractDocument)this.component.getDocument();
        adoc.readLock();
        try {
            List<? extends PaintInfo> ps = this.getPaintInfo(clip);
            Font defFont = coloring.getFont();
            int markSize = this.getMarkSize(g);
            int halfMarkSize = markSize / 2;
            int markX = (defFont.getSize() - markSize) / 2;
            int plusGap = (int)Math.round((double)markSize / 3.8);
            int lineX = markX + halfMarkSize;
            LOG.finer("CFSBar: PAINT START ------\n");
            int descent = g.getFontMetrics(defFont).getDescent();
            PaintInfo previousInfo = null;
            Graphics2D g2d = (Graphics2D)g;
            LOG.log(Level.FINEST, "MousePoint: {0}", this.mousePoint);
            for (PaintInfo paintInfo : ps) {
                boolean isFolded = paintInfo.isCollapsed();
                int y = paintInfo.getPaintY();
                int height = paintInfo.getPaintHeight();
                int markY = y + descent;
                int paintOperation = paintInfo.getPaintOperation();
                if (previousInfo == null) {
                    if (paintInfo.hasLineIn()) {
                        if (LOG.isLoggable(Level.FINER)) {
                            LOG.finer("prevInfo=NULL; y=" + y + ", PI:" + paintInfo + "\n");
                        }
                        this.drawFoldLine(g2d, paintInfo.lineInActive, lineX, clip.y, lineX, y);
                    }
                } else if (previousInfo.hasLineOut() || paintInfo.hasLineIn()) {
                    int prevY = previousInfo.getPaintY();
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.log(Level.FINER, "prevInfo={0}; y=" + y + ", PI:" + paintInfo + "\n", previousInfo);
                    }
                    this.drawFoldLine(g2d, previousInfo.lineOutActive || paintInfo.lineInActive, lineX, prevY + previousInfo.getPaintHeight(), lineX, y);
                }
                if (paintInfo.hasSign()) {
                    String opStr;
                    g.drawRect(markX, markY, markSize, markSize);
                    g.drawLine(plusGap + markX, markY + halfMarkSize, markSize + markX - plusGap, markY + halfMarkSize);
                    String string = opStr = paintOperation == 1 ? "PAINT_MARK" : "SINGLE_PAINT_MARK";
                    if (isFolded) {
                        if (LOG.isLoggable(Level.FINER)) {
                            LOG.finer(opStr + ": folded; y=" + y + ", PI:" + paintInfo + "\n");
                        }
                        g.drawLine(lineX, markY + plusGap, lineX, markY + markSize - plusGap);
                    }
                    if (paintOperation != 4 && LOG.isLoggable(Level.FINER)) {
                        LOG.finer(opStr + ": non-single; y=" + y + ", PI:" + paintInfo + "\n");
                    }
                    if (paintInfo.hasLineIn()) {
                        this.drawFoldLine(g2d, paintInfo.lineInActive, lineX, y, lineX, markY);
                    }
                    if (paintInfo.hasLineOut()) {
                        this.drawFoldLine(g2d, paintInfo.lineOutActive, lineX, markY + markSize, lineX, y + height);
                    }
                    this.visibleMarks.add(new Mark(markX, markY, markSize, isFolded));
                } else if (paintOperation == 2) {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("PAINT_LINE: y=" + y + ", PI:" + paintInfo + "\n");
                    }
                    this.drawFoldLine(g2d, paintInfo.signActive, lineX, y, lineX, y + height);
                } else if (paintOperation == 3) {
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("PAINT_END_MARK: y=" + y + ", PI:" + paintInfo + "\n");
                    }
                    if (previousInfo == null || y != previousInfo.getPaintY()) {
                        this.drawFoldLine(g2d, paintInfo.lineInActive, lineX, y, lineX, y + height / 2);
                        this.drawFoldLine(g2d, paintInfo.signActive, lineX, y + height / 2, lineX + halfMarkSize, y + height / 2);
                        if (paintInfo.getInnerLevel() > 0) {
                            if (LOG.isLoggable(Level.FINER)) {
                                LOG.finer("  PAINT middle-line\n");
                            }
                            this.drawFoldLine(g2d, paintInfo.lineOutActive, lineX, y + height / 2, lineX, y + height);
                        }
                    }
                }
                previousInfo = paintInfo;
            }
            if (previousInfo != null && (previousInfo.getInnerLevel() > 0 || previousInfo.getPaintOperation() == 1 && !previousInfo.isCollapsed())) {
                this.drawFoldLine(g2d, previousInfo.lineOutActive, lineX, previousInfo.getPaintY() + previousInfo.getPaintHeight(), lineX, clip.y + clip.height);
            }
        }
        catch (BadLocationException ble) {
            LOG.log(Level.WARNING, null, ble);
        }
        finally {
            LOG.finer("CFSBar: PAINT END ------\n\n");
            adoc.readUnlock();
        }
    }

    private static Object[] getFoldList(Fold parentFold, int start, int end) {
        Fold f;
        ArrayList<Fold> ret = new ArrayList<Fold>();
        int foldCount = parentFold.getFoldCount();
        int idxOfFirstFoldStartingInside = -1;
        for (int index = FoldUtilities.findFoldEndIndex((Fold)parentFold, (int)start); index < foldCount && (f = parentFold.getFold(index)).getStartOffset() <= end; ++index) {
            ret.add(f);
            if (idxOfFirstFoldStartingInside != -1 || f.getStartOffset() < start) continue;
            idxOfFirstFoldStartingInside = ret.size() - 1;
        }
        Object[] arrobject = new Object[2];
        arrobject[0] = ret;
        arrobject[1] = idxOfFirstFoldStartingInside != -1 ? idxOfFirstFoldStartingInside : ret.size();
        return arrobject;
    }

    @Override
    public AccessibleContext getAccessibleContext() {
        if (this.accessibleContext == null) {
            this.accessibleContext = new JComponent.AccessibleJComponent(){

                @Override
                public AccessibleRole getAccessibleRole() {
                    return AccessibleRole.PANEL;
                }
            };
            this.accessibleContext.setAccessibleName(NbBundle.getMessage(CodeFoldingSideBar.class, (String)"ACSN_CodeFoldingSideBar"));
            this.accessibleContext.setAccessibleDescription(NbBundle.getMessage(CodeFoldingSideBar.class, (String)"ACSD_CodeFoldingSideBar"));
        }
        return this.accessibleContext;
    }

    private Coloring getColoring() {
        if (this.attribs == null) {
            FontColorSettings fcs;
            AttributeSet attr;
            if (this.fcsLookupResult == null) {
                this.fcsLookupResult = MimeLookup.getLookup((String)DocumentUtilities.getMimeType((JTextComponent)this.component)).lookupResult(FontColorSettings.class);
                this.fcsLookupResult.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this.fcsTracker, this.fcsLookupResult));
            }
            attr = (attr = (fcs = (FontColorSettings)this.fcsLookupResult.allInstances().iterator().next()).getFontColors("code-folding-bar")) == null ? fcs.getFontColors("default") : AttributesUtilities.createComposite((AttributeSet[])new AttributeSet[]{attr, fcs.getFontColors("default")});
            this.attribs = attr;
        }
        return Coloring.fromAttributeSet((AttributeSet)this.attribs);
    }

    public static class Factory
    implements SideBarFactory {
        public JComponent createSideBar(JTextComponent target) {
            if (!CodeFoldingSideBar.canDisplay(target)) {
                return null;
            }
            FoldHierarchy fh = FoldHierarchy.get((JTextComponent)target);
            if (fh == null || !fh.isActive()) {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "Fold hierarchy not active for: {0}", target);
                }
                return null;
            }
            return new CodeFoldingSideBar(target);
        }
    }

    private final class Listener
    extends MouseAdapter
    implements FoldHierarchyListener,
    DocumentListener,
    Runnable {
        public void foldHierarchyChanged(FoldHierarchyEvent evt) {
            this.refresh();
        }

        @Override
        public void insertUpdate(DocumentEvent evt) {
            if (!(evt instanceof BaseDocumentEvent)) {
                return;
            }
            BaseDocumentEvent bevt = (BaseDocumentEvent)evt;
            if (bevt.getLFCount() > 0) {
                this.refresh();
            }
        }

        @Override
        public void removeUpdate(DocumentEvent evt) {
            if (!(evt instanceof BaseDocumentEvent)) {
                return;
            }
            BaseDocumentEvent bevt = (BaseDocumentEvent)evt;
            if (bevt.getLFCount() > 0) {
                this.refresh();
            }
        }

        @Override
        public void changedUpdate(DocumentEvent evt) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            Mark mark = this.getClickedMark(e);
            if (mark != null) {
                e.consume();
                CodeFoldingSideBar.this.performAction(mark, (e.getModifiersEx() & 128) > 0, (e.getModifiersEx() & 64) > 0);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() > 1) {
                LOG.log(Level.FINEST, "Mouse {0}click at {1}", new Object[]{e.getClickCount(), e.getY()});
                Mark mark = this.getClickedMark(e);
                try {
                    CodeFoldingSideBar.this.performActionAt(mark, e.getY());
                }
                catch (BadLocationException ex) {
                    LOG.log(Level.WARNING, "Error during fold expansion using sideline", ex);
                }
            } else {
                e.consume();
            }
        }

        private void refreshIfMouseOutside(Point pt) {
            CodeFoldingSideBar.this.mousePoint = (int)pt.getY();
            if (LOG.isLoggable(Level.FINEST)) {
                if (CodeFoldingSideBar.this.mouseBoundary == null) {
                    LOG.log(Level.FINEST, "Mouse boundary not set, refreshing: {0}", CodeFoldingSideBar.this.mousePoint);
                } else {
                    LOG.log(Level.FINEST, "Mouse {0} inside known mouse boundary: {1}-{2}", new Object[]{CodeFoldingSideBar.this.mousePoint, CodeFoldingSideBar.access$1200((CodeFoldingSideBar)CodeFoldingSideBar.this).y, CodeFoldingSideBar.this.mouseBoundary.getMaxY()});
                }
            }
            if (CodeFoldingSideBar.this.mouseBoundary == null || CodeFoldingSideBar.this.mousePoint < CodeFoldingSideBar.access$1200((CodeFoldingSideBar)CodeFoldingSideBar.this).y || (double)CodeFoldingSideBar.this.mousePoint > CodeFoldingSideBar.this.mouseBoundary.getMaxY()) {
                this.refresh();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            this.refreshIfMouseOutside(e.getPoint());
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            this.refreshIfMouseOutside(e.getPoint());
        }

        @Override
        public void mouseExited(MouseEvent e) {
            CodeFoldingSideBar.this.mousePoint = -1;
            this.refresh();
        }

        private Mark getClickedMark(MouseEvent e) {
            if (e == null || !SwingUtilities.isLeftMouseButton(e)) {
                return null;
            }
            int x = e.getX();
            int y = e.getY();
            for (Mark mark : CodeFoldingSideBar.this.visibleMarks) {
                if (x < mark.x || x > mark.x + mark.size || y < mark.y || y > mark.y + mark.size) continue;
                return mark;
            }
            return null;
        }

        private void refresh() {
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            if (CodeFoldingSideBar.this.getPreferredSize().width == 0 && CodeFoldingSideBar.this.enabled) {
                CodeFoldingSideBar.this.updatePreferredSize();
            }
            CodeFoldingSideBar.this.repaint();
        }
    }

    public class Mark {
        public int x;
        public int y;
        public int size;
        public boolean isFolded;

        public Mark(int x, int y, int size, boolean isFolded) {
            this.x = x;
            this.y = y;
            this.size = size;
            this.isFolded = isFolded;
        }
    }

    public class PaintInfo {
        int paintOperation;
        int innerLevel;
        int paintY;
        int paintHeight;
        boolean isCollapsed;
        boolean allCollapsed;
        int startOffset;
        int endOffset;
        int outgoingLevel;
        boolean lineIn;
        boolean lineOut;
        boolean lineInActive;
        boolean lineOutActive;
        boolean signActive;

        public PaintInfo(int paintOperation, int innerLevel, int paintY, int paintHeight, boolean isCollapsed, int startOffset, int endOffset) {
            this.paintOperation = paintOperation;
            this.outgoingLevel = innerLevel;
            this.innerLevel = this.outgoingLevel++;
            this.paintY = paintY;
            this.paintHeight = paintHeight;
            this.isCollapsed = this.allCollapsed = isCollapsed;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            switch (paintOperation) {
                case 1: {
                    this.lineIn = false;
                    this.lineOut = true;
                    break;
                }
                case 4: {
                    this.lineIn = false;
                    this.lineOut = false;
                    break;
                }
                case 3: {
                    this.lineIn = true;
                    this.lineOut = false;
                    isCollapsed = true;
                    this.allCollapsed = true;
                    break;
                }
                case 2: {
                    this.lineOut = true;
                    this.lineIn = true;
                }
            }
        }

        void markActive(boolean mark, boolean lineIn, boolean lineOut) {
            this.signActive |= mark;
            this.lineInActive |= lineIn;
            this.lineOutActive |= lineOut;
        }

        boolean hasLineIn() {
            return this.lineIn || this.innerLevel > 0;
        }

        boolean hasLineOut() {
            return this.lineOut || this.outgoingLevel > 0 || this.paintOperation != 4 && !this.isAllCollapsed();
        }

        public PaintInfo(int paintOperation, int innerLevel, int paintY, int paintHeight, int startOffset, int endOffset) {
            this(paintOperation, innerLevel, paintY, paintHeight, false, startOffset, endOffset);
        }

        public int getPaintOperation() {
            return this.paintOperation;
        }

        public int getInnerLevel() {
            return this.innerLevel;
        }

        public int getPaintY() {
            return this.paintY;
        }

        public int getPaintHeight() {
            return this.paintHeight;
        }

        public boolean isCollapsed() {
            return this.isCollapsed;
        }

        boolean isAllCollapsed() {
            return this.allCollapsed;
        }

        public void setPaintOperation(int paintOperation) {
            this.paintOperation = paintOperation;
        }

        public void setInnerLevel(int innerLevel) {
            this.innerLevel = innerLevel;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer("");
            if (this.paintOperation == 1) {
                sb.append("PAINT_MARK");
            } else if (this.paintOperation == 2) {
                sb.append("PAINT_LINE");
            } else if (this.paintOperation == 3) {
                sb.append("PAINT_END_MARK");
            } else if (this.paintOperation == 4) {
                sb.append("SINGLE_PAINT_MARK");
            }
            sb.append(",L:").append(this.innerLevel).append("/").append(this.outgoingLevel);
            sb.append(',').append(this.isCollapsed ? "C" : "E");
            sb.append(", start=").append(this.startOffset).append(", end=").append(this.endOffset);
            sb.append(", lineIn=").append(this.lineIn).append(", lineOut=").append(this.lineOut);
            return sb.toString();
        }

        boolean hasSign() {
            return this.paintOperation == 1 || this.paintOperation == 4;
        }

        void mergeWith(PaintInfo prevInfo) {
            if (prevInfo == null) {
                return;
            }
            int operation = this.paintOperation;
            boolean lineIn = prevInfo.lineIn;
            boolean lineOut = prevInfo.lineOut;
            LOG.log(Level.FINER, "Merging {0} with {1}: ", new Object[]{this, prevInfo});
            if (prevInfo.getPaintOperation() == 3) {
                lineIn = true;
            } else if (prevInfo.getPaintOperation() != 4) {
                operation = 1;
            }
            int level1 = Math.min(prevInfo.innerLevel, this.innerLevel);
            int level2 = prevInfo.outgoingLevel;
            if (this.getPaintOperation() == 3 && this.innerLevel == prevInfo.outgoingLevel) {
                level2 = this.outgoingLevel;
            } else if (!this.isCollapsed) {
                level2 = Math.max(prevInfo.outgoingLevel, this.outgoingLevel);
            }
            if (prevInfo.getInnerLevel() < this.getInnerLevel()) {
                int paintFrom = Math.min(prevInfo.paintY, this.paintY);
                int paintTo = Math.max(prevInfo.paintY + prevInfo.paintHeight, this.paintY + this.paintHeight);
                boolean collapsed = prevInfo.isCollapsed() || this.isCollapsed();
                int offsetFrom = Math.min(prevInfo.startOffset, this.startOffset);
                int offsetTo = Math.max(prevInfo.endOffset, this.endOffset);
                this.paintY = paintFrom;
                this.paintHeight = paintTo - paintFrom;
                this.isCollapsed = collapsed;
                this.startOffset = offsetFrom;
                this.endOffset = offsetTo;
            }
            this.paintOperation = operation;
            this.allCollapsed = prevInfo.allCollapsed && this.allCollapsed;
            this.innerLevel = level1;
            this.outgoingLevel = level2;
            this.lineIn |= lineIn;
            this.lineOut |= lineOut;
            this.signActive |= prevInfo.signActive;
            this.lineInActive |= prevInfo.lineInActive;
            this.lineOutActive |= prevInfo.lineOutActive;
            LOG.log(Level.FINER, "Merged result: {0}", this);
        }
    }

}

