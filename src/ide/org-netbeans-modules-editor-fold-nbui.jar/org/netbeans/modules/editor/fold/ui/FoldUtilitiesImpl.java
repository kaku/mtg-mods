/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold.ui;

public class FoldUtilitiesImpl {
    public static final String PREF_COLLAPSE_PREFIX = "code-folding-collapse-";
    public static final String PREF_OVERRIDE_DEFAULTS = "code-folding-use-defaults";
    public static final String PREF_CODE_FOLDING_ENABLED = "code-folding-enable";
    public static final String PREF_CONTENT_PREVIEW = "code-folding-content.preview";
    public static final String PREF_CONTENT_SUMMARY = "code-folding-content.summary";
}

