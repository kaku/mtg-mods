/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.LinkedHashSet;
import java.util.Set;

final class VerticalFlowLayout
implements LayoutManager2 {
    private final Set<Component> components = new LinkedHashSet<Component>();
    private int hgap = 0;
    private int vgap = 0;

    VerticalFlowLayout() {
    }

    public void setHGap(int hgap) {
        this.hgap = hgap;
    }

    public void setVGap(int vgap) {
        this.vgap = vgap;
    }

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {
        this.components.add(comp);
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0.0f;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0.0f;
    }

    @Override
    public void invalidateLayout(Container target) {
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
        this.components.add(comp);
    }

    private Dimension computeDimension(Container parent, int type) {
        Insets insets = parent.getInsets();
        int x = insets.left;
        int y = insets.top;
        int columnWidth = 0;
        int maxY = 0;
        for (Component c : this.components) {
            if (!c.isVisible()) continue;
            switch (type) {
                Dimension d;
                case 0: {
                    d = c.getPreferredSize();
                    break;
                }
                case 1: {
                    d = c.getMinimumSize();
                    break;
                }
                default: {
                    d = c.getMaximumSize();
                }
            }
            columnWidth = Math.max(columnWidth, d.width);
            maxY = Math.max(y += d.height, maxY);
            y += this.vgap;
        }
        return new Dimension(x += columnWidth, maxY);
    }

    @Override
    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();
        int x = insets.left;
        int y = insets.top;
        int columnWidth = 0;
        int limitHeight = parent.getHeight() - insets.bottom;
        for (Component c : this.components) {
            if (!c.isVisible()) continue;
            Dimension d = c.getPreferredSize();
            columnWidth = Math.max(columnWidth, d.width);
            if (y + d.height >= limitHeight) {
                x += columnWidth + this.hgap;
                y = insets.top;
            }
            c.setBounds(x, y, d.width, d.height);
            y += d.height + this.vgap;
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return this.computeDimension(parent, 1);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return this.computeDimension(parent, 1);
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        return this.computeDimension(target, 2);
    }

    @Override
    public void removeLayoutComponent(Component comp) {
        this.components.remove(comp);
    }
}

