/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package org.netbeans.modules.editor.fold.ui;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String FMT_contentSummary(Object number_of_lines_of_folded_code) {
        return NbBundle.getMessage(Bundle.class, (String)"FMT_contentSummary", (Object)number_of_lines_of_folded_code);
    }

    static String ITEM_AllLanguages() {
        return NbBundle.getMessage(Bundle.class, (String)"ITEM_AllLanguages");
    }

    private void Bundle() {
    }
}

