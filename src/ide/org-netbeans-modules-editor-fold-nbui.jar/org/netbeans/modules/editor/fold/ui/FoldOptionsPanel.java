/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.Language
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold.ui;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.Language;
import org.netbeans.modules.editor.fold.ui.Bundle;
import org.netbeans.modules.editor.fold.ui.CustomizerWithDefaults;
import org.netbeans.modules.editor.fold.ui.DefaultFoldingOptions;
import org.netbeans.modules.editor.fold.ui.FoldOptionsController;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

final class FoldOptionsPanel
extends JPanel
implements ActionListener,
PreferenceChangeListener {
    private List<String[]> languageMimeTypes;
    private FoldOptionsController ctrl;
    private Preferences parentPrefs;
    private Preferences currentPreferences;
    private Map<String, JComponent> panels = new HashMap<String, JComponent>();
    private PreferenceChangeListener wPrefL;
    private static final Comparator<String[]> LANG_COMPARATOR = new Comparator<String[]>(){

        @Override
        public int compare(String[] o1, String[] o2) {
            if (o1 == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            if (o1[0].equals(o2[0])) {
                return 0;
            }
            if (o1[0].length() == 0) {
                return -1;
            }
            if (o2[0].length() == 0) {
                return 1;
            }
            return o1[1].compareToIgnoreCase(o2[1]);
        }
    };
    private boolean ignoreEnableTrigger;
    private JPanel content;
    private JCheckBox contentPreview;
    private JCheckBox enableFolds;
    private JCheckBox foldedSummary;
    private JPanel jPanel1;
    private JLabel langLabel;
    private JComboBox langSelect;
    private JCheckBox useDefaults;

    public FoldOptionsPanel(FoldOptionsController ctrl) {
        this.wPrefL = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)null);
        this.ctrl = ctrl;
        this.initComponents();
        this.langSelect.setRenderer(new DefaultListCellRenderer(){

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                if (value instanceof String[]) {
                    value = ((String[])value)[1];
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
        this.langSelect.addActionListener(this);
        this.contentPreview.addActionListener(this);
        this.foldedSummary.addActionListener(this);
    }

    private void languageSelected() {
        String[] sel = (String[])this.langSelect.getSelectedItem();
        if (sel == null) {
            return;
        }
        String mime = sel[0];
        if (this.currentPreferences != null) {
            this.currentPreferences.removePreferenceChangeListener(this.wPrefL);
        }
        this.currentPreferences = this.ctrl.prefs(mime);
        JComponent panel = this.panels.get(mime);
        String parentMime = MimePath.parse((String)mime).getInheritedType();
        this.parentPrefs = parentMime != null ? this.ctrl.prefs(parentMime) : null;
        if (panel == null) {
            panel = new DefaultFoldingOptions(mime, this.currentPreferences);
            if (panel instanceof CustomizerWithDefaults) {
                ((CustomizerWithDefaults)((Object)panel)).setDefaultPreferences(this.parentPrefs);
            }
            this.panels.put(mime, panel);
            this.content.add((Component)panel, mime);
        }
        ((CardLayout)this.content.getLayout()).show(this.content, mime);
        this.currentPreferences.addPreferenceChangeListener(this.wPrefL);
        this.useDefaults.setVisible(!"".equals(mime));
        this.preferenceChange(null);
    }

    private void previewChanged() {
        this.currentPreferences.putBoolean("code-folding-content.preview", this.contentPreview.isSelected());
    }

    private void summaryChanged() {
        this.currentPreferences.putBoolean("code-folding-content.summary", this.foldedSummary.isSelected());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (this.ignoreEnableTrigger) {
            return;
        }
        if (o == this.langSelect) {
            this.languageSelected();
        } else if (o == this.contentPreview) {
            this.previewChanged();
        } else if (o == this.foldedSummary) {
            this.summaryChanged();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        String k = evt == null ? null : evt.getKey();
        this.ignoreEnableTrigger = true;
        try {
            if (k == null || k.equals("code-folding-use-defaults")) {
                this.useDefaults.setSelected(this.currentPreferences.getBoolean("code-folding-use-defaults", true));
            }
            if (k == null || k.equals("code-folding-enable")) {
                boolean enabled = this.currentPreferences.getBoolean("code-folding-enable", true);
                this.enableFolds.setSelected(enabled);
                this.contentPreview.setEnabled(enabled);
                this.foldedSummary.setEnabled(enabled);
                this.useDefaults.setEnabled(enabled);
            }
            if (k == null || "code-folding-content.preview".equals("code-folding-content.preview")) {
                this.contentPreview.setSelected(this.currentPreferences.getBoolean("code-folding-content.preview", true));
            }
            if (k == null || "code-folding-content.summary".equals("code-folding-content.summary")) {
                this.foldedSummary.setSelected(this.currentPreferences.getBoolean("code-folding-content.summary", true));
            }
            if (k != null && "code-folding-use-defaults".equals(k)) {
                boolean b;
                boolean bl = b = this.parentPrefs == null || !this.currentPreferences.getBoolean("code-folding-use-defaults", true);
                if (this.parentPrefs != null) {
                    if (b) {
                        this.currentPreferences.putBoolean("code-folding-content.preview", this.parentPrefs.getBoolean("code-folding-content.preview", true));
                        this.currentPreferences.putBoolean("code-folding-content.summary", this.parentPrefs.getBoolean("code-folding-content.summary", true));
                    } else {
                        this.currentPreferences.remove("code-folding-content.preview");
                        this.currentPreferences.remove("code-folding-content.summary");
                    }
                }
                this.contentPreview.setEnabled(b);
                this.foldedSummary.setEnabled(b);
                this.contentPreview.setSelected(this.currentPreferences.getBoolean("code-folding-content.preview", true));
                this.foldedSummary.setSelected(this.currentPreferences.getBoolean("code-folding-content.summary", true));
            }
        }
        finally {
            this.ignoreEnableTrigger = false;
        }
    }

    boolean isChanged() {
        boolean isChanged = false;
        for (String mime : this.panels.keySet()) {
            JComponent panel = this.panels.get(mime);
            if (!(panel instanceof DefaultFoldingOptions)) continue;
            isChanged |= ((DefaultFoldingOptions)panel).isChanged();
        }
        return isChanged;
    }

    void update() {
        this.initialize();
    }

    private void initialize() {
        Collection<String> mimeTypes = this.ctrl.getUpdatedLanguages();
        ArrayList<String[]> langMimes = new ArrayList<String[]>(mimeTypes.size());
        langMimes.add(new String[]{"", Bundle.ITEM_AllLanguages()});
        for (String s : mimeTypes) {
            String name;
            Language l = Language.find((String)s);
            if (l == null || (name = EditorSettings.getDefault().getLanguageName(s)).equals(s) || FoldUtilities.getFoldTypes((String)s).values().isEmpty()) continue;
            langMimes.add(new String[]{s, EditorSettings.getDefault().getLanguageName(s)});
        }
        Collections.sort(langMimes, LANG_COMPARATOR);
        this.languageMimeTypes = langMimes;
        int idx = this.langSelect.getSelectedIndex();
        this.langSelect.setModel(new DefaultComboBoxModel<Object>(this.languageMimeTypes.toArray(new Object[this.languageMimeTypes.size()])));
        this.langSelect.setSelectedIndex(idx >= 0 && idx < this.langSelect.getItemCount() ? idx : 0);
    }

    void clear() {
        this.panels.clear();
    }

    private void initComponents() {
        this.langSelect = new JComboBox();
        this.langLabel = new JLabel();
        this.content = new JPanel();
        this.enableFolds = new JCheckBox();
        this.jPanel1 = new JPanel();
        this.contentPreview = new JCheckBox();
        this.foldedSummary = new JCheckBox();
        this.useDefaults = new JCheckBox();
        Mnemonics.setLocalizedText((JLabel)this.langLabel, (String)NbBundle.getMessage(FoldOptionsPanel.class, (String)"FoldOptionsPanel.langLabel.text"));
        this.content.setLayout(new CardLayout());
        Mnemonics.setLocalizedText((AbstractButton)this.enableFolds, (String)NbBundle.getMessage(FoldOptionsPanel.class, (String)"FoldOptionsPanel.enableFolds.text"));
        this.enableFolds.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FoldOptionsPanel.this.enableFoldsActionPerformed(evt);
            }
        });
        this.jPanel1.setBorder(BorderFactory.createTitledBorder(NbBundle.getMessage(FoldOptionsPanel.class, (String)"Title_FoldDisplayOptions")));
        Mnemonics.setLocalizedText((AbstractButton)this.contentPreview, (String)NbBundle.getMessage(FoldOptionsPanel.class, (String)"FoldOptionsPanel.contentPreview.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.foldedSummary, (String)NbBundle.getMessage(FoldOptionsPanel.class, (String)"FoldOptionsPanel.foldedSummary.text"));
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.contentPreview).addComponent(this.foldedSummary)).addContainerGap(-1, 32767)));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(this.contentPreview).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.foldedSummary).addContainerGap(-1, 32767)));
        Mnemonics.setLocalizedText((AbstractButton)this.useDefaults, (String)NbBundle.getMessage(FoldOptionsPanel.class, (String)"FoldOptionsPanel.useDefaults.text"));
        this.useDefaults.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent evt) {
                FoldOptionsPanel.this.useDefaultsActionPerformed(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.content, -1, -1, 32767).addComponent(this.jPanel1, -1, -1, 32767).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.useDefaults).addGroup(layout.createSequentialGroup().addComponent(this.langLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.langSelect, -2, 186, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.enableFolds))).addGap(0, 0, 32767))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.langSelect, -2, -1, -2).addComponent(this.langLabel).addComponent(this.enableFolds)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.useDefaults).addGap(12, 12, 12).addComponent(this.content, -1, 25, 32767).addGap(18, 18, 18).addComponent(this.jPanel1, -2, -1, -2).addContainerGap()));
    }

    private void enableFoldsActionPerformed(ActionEvent evt) {
        if (this.ignoreEnableTrigger) {
            return;
        }
        boolean enable = this.enableFolds.isSelected();
        this.currentPreferences.putBoolean("code-folding-enable", enable);
    }

    private void useDefaultsActionPerformed(ActionEvent evt) {
        if (this.ignoreEnableTrigger) {
            return;
        }
        this.currentPreferences.putBoolean("code-folding-use-defaults", this.useDefaults.isSelected());
    }

}

