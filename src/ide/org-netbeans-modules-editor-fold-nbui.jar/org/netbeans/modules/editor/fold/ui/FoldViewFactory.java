/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldHierarchy
 *  org.netbeans.api.editor.fold.FoldHierarchyEvent
 *  org.netbeans.api.editor.fold.FoldHierarchyListener
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.lib2.view.EditorView
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactory
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactory$Factory
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange
 *  org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange$Type
 *  org.netbeans.modules.editor.lib2.view.ViewUtils
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.WeakListeners
 */
package org.netbeans.modules.editor.fold.ui;

import java.util.Collection;
import java.util.EventListener;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.View;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldHierarchy;
import org.netbeans.api.editor.fold.FoldHierarchyEvent;
import org.netbeans.api.editor.fold.FoldHierarchyListener;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.fold.ui.FoldView;
import org.netbeans.modules.editor.lib2.view.EditorView;
import org.netbeans.modules.editor.lib2.view.EditorViewFactory;
import org.netbeans.modules.editor.lib2.view.EditorViewFactoryChange;
import org.netbeans.modules.editor.lib2.view.ViewUtils;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.WeakListeners;

public final class FoldViewFactory
extends EditorViewFactory
implements FoldHierarchyListener,
LookupListener,
PreferenceChangeListener {
    static final String VIEW_FOLDS_EXPANDED_PROPERTY = "view-folds-expanded";
    static final Logger CHANGE_LOG = Logger.getLogger("org.netbeans.editor.view.change");
    private static final Logger LOG = Logger.getLogger(FoldViewFactory.class.getName());
    private FoldHierarchy foldHierarchy;
    private boolean foldHierarchyLocked;
    private Fold fold;
    private int foldStartOffset;
    private Iterator<Fold> collapsedFoldIterator;
    private boolean viewFoldsExpanded;
    private FontColorSettings colorSettings;
    private Lookup.Result colorSource;
    private Preferences prefs;
    private int viewFlags = 0;

    public static void register() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Fold view factory registered");
        }
        EditorViewFactory.registerFactory((EditorViewFactory.Factory)new FoldFactory());
    }

    public FoldViewFactory(View documentView) {
        super(documentView);
        this.foldHierarchy = FoldHierarchy.get((JTextComponent)this.textComponent());
        this.foldHierarchy.addFoldHierarchyListener((FoldHierarchyListener)this);
        this.viewFoldsExpanded = Boolean.TRUE.equals(this.textComponent().getClientProperty("view-folds-expanded"));
        String mime = DocumentUtilities.getMimeType((Document)this.document());
        Lookup lkp = MimeLookup.getLookup((String)mime);
        this.colorSource = lkp.lookupResult(FontColorSettings.class);
        this.colorSource.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, (Object)this.colorSource));
        this.colorSettings = (FontColorSettings)this.colorSource.allInstances().iterator().next();
        this.prefs = (Preferences)lkp.lookup(Preferences.class);
        this.prefs.addPreferenceChangeListener((PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.prefs));
        this.initViewFlags();
    }

    private void initViewFlags() {
        this.viewFlags = (this.prefs.getBoolean("code-folding-content.preview", true) ? 1 : 0) | (this.prefs.getBoolean("code-folding-content.summary", true) ? 2 : 0);
    }

    public void resultChanged(LookupEvent ev) {
        this.refreshColors();
    }

    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        String k = evt.getKey();
        if ("code-folding-content.preview".equals(k) || "code-folding-content.summary".equals(k)) {
            this.initViewFlags();
            this.document().render(new Runnable(){

                @Override
                public void run() {
                    int end = FoldViewFactory.this.document().getLength();
                    FoldViewFactory.this.fireEvent(EditorViewFactoryChange.createList((int)0, (int)end, (EditorViewFactoryChange.Type)EditorViewFactoryChange.Type.CHARACTER_CHANGE));
                }
            });
        }
    }

    private void refreshColors() {
        this.colorSettings = (FontColorSettings)this.colorSource.allInstances().iterator().next();
        this.document().render(new Runnable(){

            @Override
            public void run() {
                int end = FoldViewFactory.this.document().getLength();
                FoldViewFactory.this.fireEvent(EditorViewFactoryChange.createList((int)0, (int)end, (EditorViewFactoryChange.Type)EditorViewFactoryChange.Type.CHARACTER_CHANGE));
            }
        });
    }

    public void restart(int startOffset, int endOffset, boolean createViews) {
        Iterator it;
        this.foldHierarchy.lock();
        this.foldHierarchyLocked = true;
        this.collapsedFoldIterator = it = FoldUtilities.collapsedFoldIterator((FoldHierarchy)this.foldHierarchy, (int)startOffset, (int)Integer.MAX_VALUE);
        this.foldStartOffset = -1;
    }

    private void updateFold(int offset) {
        if (this.foldStartOffset < offset) {
            while (this.collapsedFoldIterator.hasNext()) {
                this.fold = this.collapsedFoldIterator.next();
                this.foldStartOffset = this.fold.getStartOffset();
                int l = this.fold.getEndOffset() - this.foldStartOffset;
                if (this.foldStartOffset < offset || l <= 0) continue;
                return;
            }
            this.fold = null;
            this.foldStartOffset = Integer.MAX_VALUE;
        }
    }

    public int nextViewStartOffset(int offset) {
        if (!this.viewFoldsExpanded) {
            this.updateFold(offset);
            return this.foldStartOffset;
        }
        return Integer.MAX_VALUE;
    }

    public EditorView createView(int startOffset, int limitOffset, boolean forcedLimit, EditorView origView, int nextOrigViewOffset) {
        assert (startOffset == this.foldStartOffset);
        if (this.fold.getEndOffset() <= limitOffset || !forcedLimit) {
            return new FoldView(this.textComponent(), this.fold, this.colorSettings, this.viewFlags);
        }
        return null;
    }

    public int viewEndOffset(int startOffset, int limitOffset, boolean forcedLimit) {
        int foldEndOffset = this.fold.getEndOffset();
        if (foldEndOffset <= limitOffset) {
            return foldEndOffset;
        }
        return -1;
    }

    public void continueCreation(int startOffset, int endOffset) {
    }

    public void finishCreation() {
        this.fold = null;
        this.collapsedFoldIterator = null;
        if (this.foldHierarchyLocked) {
            this.foldHierarchy.unlock();
        }
    }

    public void foldHierarchyChanged(FoldHierarchyEvent evt) {
        int startOffset = evt.getAffectedStartOffset();
        int endOffset = evt.getAffectedEndOffset();
        if (CHANGE_LOG.isLoggable(Level.FINE)) {
            ViewUtils.log((Logger)CHANGE_LOG, (String)("CHANGE in FoldViewFactory: <" + startOffset + "," + endOffset + ">\n"));
        }
        this.fireEvent(EditorViewFactoryChange.createList((int)startOffset, (int)endOffset, (EditorViewFactoryChange.Type)EditorViewFactoryChange.Type.PARAGRAPH_CHANGE));
    }

    public static final class FoldFactory
    implements EditorViewFactory.Factory {
        public EditorViewFactory createEditorViewFactory(View documentView) {
            return new FoldViewFactory(documentView);
        }

        public int weight() {
            return 100;
        }
    }

}

