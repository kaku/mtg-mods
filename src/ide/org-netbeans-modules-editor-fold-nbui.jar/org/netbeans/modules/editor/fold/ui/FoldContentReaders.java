/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.fold.Fold
 *  org.netbeans.api.editor.fold.FoldTemplate
 *  org.netbeans.api.editor.fold.FoldType
 *  org.netbeans.api.editor.fold.FoldUtilities
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.spi.editor.fold.ContentReader
 *  org.netbeans.spi.editor.fold.ContentReader$Factory
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.netbeans.modules.editor.fold.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.editor.fold.Fold;
import org.netbeans.api.editor.fold.FoldTemplate;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.spi.editor.fold.ContentReader;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public final class FoldContentReaders {
    private static final FoldContentReaders INSTANCE = new FoldContentReaders();
    private final Map<String, N> mimeNodes = new HashMap<String, N>();

    public static FoldContentReaders get() {
        return INSTANCE;
    }

    public CharSequence readContent(String mime, Document d, Fold f, FoldTemplate ft) throws BadLocationException {
        List<ContentReader> readers = this.getReaders(mime, f.getType());
        for (ContentReader r : readers) {
            CharSequence chs = r.read(d, f, ft);
            if (chs == null) continue;
            return chs;
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<ContentReader> getReaders(String mime, FoldType ft) {
        N node;
        Map<String, N> map = this.mimeNodes;
        synchronized (map) {
            node = this.mimeNodes.get(mime);
        }
        if (node == null && (node = this.mimeNodes.get(mime)) == null) {
            node = new N(mime, MimeLookup.getLookup((String)mime));
            map = this.mimeNodes;
            synchronized (map) {
                N n2 = this.mimeNodes.get(mime);
                if (n2 == null) {
                    this.mimeNodes.put(mime, node);
                } else {
                    node = n2;
                }
            }
        }
        return node.readers(ft);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void flush() {
        Map<String, N> map = this.mimeNodes;
        synchronized (map) {
            for (N n : this.mimeNodes.values()) {
                n.clear();
            }
            this.mimeNodes.clear();
        }
    }

    private class N
    implements LookupListener {
        String mime;
        Lookup.Result result;
        Map<FoldType, List<ContentReader>> readers;

        public N(String mime, Lookup mimeLookup) {
            this.readers = new HashMap<FoldType, List<ContentReader>>();
            this.mime = mime;
            this.init(mimeLookup);
        }

        void clear() {
            this.result.removeLookupListener((LookupListener)this);
        }

        public void resultChanged(LookupEvent ev) {
            FoldContentReaders.this.flush();
        }

        private void init(Lookup mimeLookup) {
            ArrayList types = new ArrayList(FoldUtilities.getFoldTypes((String)this.mime).values());
            this.result = mimeLookup.lookupResult(ContentReader.Factory.class);
            Collection factories = this.result.allInstances();
            for (FoldType ft : types) {
                ArrayList<ContentReader> l = null;
                for (ContentReader.Factory f : factories) {
                    ContentReader cr = f.createReader(ft);
                    if (cr == null) continue;
                    if (l == null) {
                        l = new ArrayList<ContentReader>(3);
                    }
                    l.add(cr);
                }
                if (l == null) continue;
                this.readers.put(ft, l);
            }
            this.result.addLookupListener((LookupListener)this);
        }

        List<ContentReader> readers(FoldType ft) {
            List r = this.readers.get((Object)ft);
            return r == null ? Collections.emptyList() : r;
        }
    }

}

