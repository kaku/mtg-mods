/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.SideBarFactory
 */
package org.netbeans.api.editor.fold.ui;

import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import org.netbeans.modules.editor.fold.ui.CodeFoldingSideBar;
import org.netbeans.spi.editor.SideBarFactory;

public class FoldingUISupport {
    private static SideBarFactory FACTORY = null;

    private FoldingUISupport() {
    }

    public static JComponent sidebarComponent(JTextComponent textComponent) {
        return new CodeFoldingSideBar(textComponent);
    }

    public static SideBarFactory foldingSidebarFactory() {
        if (FACTORY != null) {
            return FACTORY;
        }
        FACTORY = new CodeFoldingSideBar.Factory();
        return FACTORY;
    }

    public static void disableCodeFoldingSidebar(JTextComponent text) {
        text.putClientProperty("org.netbeans.editor.CodeFoldingSidebar", true);
    }
}

