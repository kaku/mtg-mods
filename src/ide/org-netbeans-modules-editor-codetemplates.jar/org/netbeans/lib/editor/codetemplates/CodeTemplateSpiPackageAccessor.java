/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates;

import org.netbeans.lib.editor.codetemplates.CodeTemplateInsertHandler;
import org.netbeans.lib.editor.codetemplates.CodeTemplateParameterImpl;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;

public abstract class CodeTemplateSpiPackageAccessor {
    private static CodeTemplateSpiPackageAccessor INSTANCE;

    public static CodeTemplateSpiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class clazz = Class.forName(CodeTemplateInsertRequest.class.getName());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        assert (INSTANCE != null);
        return INSTANCE;
    }

    public static void register(CodeTemplateSpiPackageAccessor accessor) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already registered");
        }
        INSTANCE = accessor;
    }

    public abstract CodeTemplateInsertRequest createInsertRequest(CodeTemplateInsertHandler var1);

    public abstract CodeTemplateParameter createParameter(CodeTemplateParameterImpl var1);

    public abstract CodeTemplateParameterImpl getImpl(CodeTemplateParameter var1);
}

