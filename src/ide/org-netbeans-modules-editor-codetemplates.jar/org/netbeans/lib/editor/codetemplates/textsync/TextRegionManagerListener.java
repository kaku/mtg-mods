/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.textsync;

import java.util.EventListener;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManagerEvent;

public interface TextRegionManagerListener
extends EventListener {
    public void stateChanged(TextRegionManagerEvent var1);
}

