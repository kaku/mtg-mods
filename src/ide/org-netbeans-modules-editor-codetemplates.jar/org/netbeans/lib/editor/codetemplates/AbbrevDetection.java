/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.Abbrev
 *  org.netbeans.editor.Acceptor
 *  org.netbeans.editor.AcceptorFactory
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.hints.ErrorDescription
 *  org.netbeans.spi.editor.hints.ErrorDescriptionFactory
 *  org.netbeans.spi.editor.hints.Fix
 *  org.netbeans.spi.editor.hints.HintsController
 *  org.netbeans.spi.editor.hints.Severity
 *  org.openide.ErrorManager
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.lib.editor.codetemplates;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.Abbrev;
import org.netbeans.editor.Acceptor;
import org.netbeans.editor.AcceptorFactory;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.SurroundWithFix;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.Fix;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.ErrorManager;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class AbbrevDetection
implements DocumentListener,
PropertyChangeListener,
KeyListener,
CaretListener,
PreferenceChangeListener {
    private static final Logger LOG = Logger.getLogger(AbbrevDetection.class.getName());
    private static final RequestProcessor RP = new RequestProcessor(AbbrevDetection.class.getName());
    private static final String ABBREV_IGNORE_MODIFICATION_DOC_PROPERTY = "abbrev-ignore-modification";
    private static final String EDITING_TEMPLATE_DOC_PROPERTY = "code-template-insert-handler";
    private static final String SURROUND_WITH = NbBundle.getMessage(SurroundWithFix.class, (String)"TXT_SurroundWithHint_Label");
    private static final int SURROUND_WITH_DELAY = 250;
    private JTextComponent component;
    private Document doc;
    private Position abbrevEndPosition;
    private final StringBuffer abbrevChars = new StringBuffer();
    private Acceptor resetAcceptor;
    private MimePath mimePath = null;
    private Preferences prefs = null;
    private PreferenceChangeListener weakPrefsListener = null;
    private ErrorDescription errorDescription = null;
    private List<Fix> surrounsWithFixes = null;
    private Timer surroundsWithTimer;

    public static AbbrevDetection get(JTextComponent component) {
        AbbrevDetection ad = (AbbrevDetection)component.getClientProperty(AbbrevDetection.class);
        if (ad == null) {
            ad = new AbbrevDetection(component);
            component.putClientProperty(AbbrevDetection.class, ad);
        }
        return ad;
    }

    public static synchronized void remove(JTextComponent component) {
        AbbrevDetection ad = (AbbrevDetection)component.getClientProperty(AbbrevDetection.class);
        if (ad != null) {
            assert (ad.component == component);
            ad.uninstall();
            component.putClientProperty(AbbrevDetection.class, null);
        }
    }

    private AbbrevDetection(JTextComponent component) {
        String mimeType;
        this.component = component;
        component.addCaretListener(this);
        this.doc = component.getDocument();
        if (this.doc != null) {
            this.doc.addDocumentListener(this);
        }
        if ((mimeType = DocumentUtilities.getMimeType((JTextComponent)component)) != null) {
            this.mimePath = MimePath.parse((String)mimeType);
            this.prefs = (Preferences)MimeLookup.getLookup((MimePath)this.mimePath).lookup(Preferences.class);
            this.weakPrefsListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.prefs);
            this.prefs.addPreferenceChangeListener(this.weakPrefsListener);
        }
        this.preferenceChange(null);
        component.addKeyListener(this);
        component.addPropertyChangeListener(this);
        this.surroundsWithTimer = new Timer(0, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if (!DocumentUtilities.isReadLocked((Document)AbbrevDetection.this.doc)) {
                    AbbrevDetection.this.showSurroundWithHint();
                }
            }
        });
        this.surroundsWithTimer.setRepeats(false);
    }

    private void uninstall() {
        assert (this.component != null);
        this.component.removeCaretListener(this);
        if (this.doc != null) {
            this.doc.addDocumentListener(this);
        }
        this.component.removeKeyListener(this);
        this.component.removePropertyChangeListener(this);
        this.surroundsWithTimer.stop();
    }

    @Override
    public void preferenceChange(PreferenceChangeEvent evt) {
        String settingName;
        String string = settingName = evt == null ? null : evt.getKey();
        if (settingName == null || "abbrev-reset-acceptor".equals(settingName)) {
            this.resetAcceptor = AbbrevDetection.getResetAcceptor(this.prefs, this.mimePath);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent evt) {
        if (!this.isIgnoreModification()) {
            if (DocumentUtilities.isTypingModification((Document)evt.getDocument()) && !this.isAbbrevDisabled()) {
                int offset = evt.getOffset();
                int length = evt.getLength();
                this.appendTypedText(offset, length);
            } else {
                this.resetAbbrevChars();
            }
        }
    }

    @Override
    public void removeUpdate(DocumentEvent evt) {
        if (!this.isIgnoreModification()) {
            if (DocumentUtilities.isTypingModification((Document)evt.getDocument()) && !this.isAbbrevDisabled()) {
                int offset = evt.getOffset();
                int length = evt.getLength();
                this.removeAbbrevText(offset, length);
            } else {
                this.resetAbbrevChars();
            }
        }
    }

    @Override
    public void changedUpdate(DocumentEvent evt) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("document".equals(evt.getPropertyName())) {
            String mimeType;
            if (this.doc != null) {
                this.doc.removeDocumentListener(this);
            }
            this.doc = this.component.getDocument();
            if (this.doc != null) {
                this.doc.addDocumentListener(this);
            }
            if (this.prefs != null) {
                this.prefs.removePreferenceChangeListener(this.weakPrefsListener);
                this.prefs = null;
                this.weakPrefsListener = null;
                this.mimePath = null;
            }
            if ((mimeType = DocumentUtilities.getMimeType((JTextComponent)this.component)) != null) {
                this.mimePath = MimePath.parse((String)mimeType);
                this.prefs = (Preferences)MimeLookup.getLookup((MimePath)this.mimePath).lookup(Preferences.class);
                this.weakPrefsListener = (PreferenceChangeListener)WeakListeners.create(PreferenceChangeListener.class, (EventListener)this, (Object)this.prefs);
                this.prefs.addPreferenceChangeListener(this.weakPrefsListener);
            }
            this.preferenceChange(null);
        }
    }

    @Override
    public void keyPressed(KeyEvent evt) {
        this.checkExpansionKeystroke(evt);
    }

    @Override
    public void keyReleased(KeyEvent evt) {
        this.checkExpansionKeystroke(evt);
    }

    @Override
    public void keyTyped(KeyEvent evt) {
        this.checkExpansionKeystroke(evt);
    }

    @Override
    public void caretUpdate(CaretEvent evt) {
        if (evt.getDot() != evt.getMark()) {
            this.surroundsWithTimer.setInitialDelay(250);
            this.surroundsWithTimer.restart();
        } else {
            this.surroundsWithTimer.stop();
            this.hideSurroundWithHint();
        }
    }

    private boolean isIgnoreModification() {
        return Boolean.TRUE.equals(this.doc.getProperty("abbrev-ignore-modification")) || !DocumentUtilities.isTypingModification((Document)this.doc);
    }

    private boolean isAbbrevDisabled() {
        return Abbrev.isAbbrevDisabled((JTextComponent)this.component);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkExpansionKeystroke(KeyEvent evt) {
        KeyStroke expandKeyStroke;
        CodeTemplateManagerOperation operation;
        Position pos = null;
        Document d = null;
        StringBuffer stringBuffer = this.abbrevChars;
        synchronized (stringBuffer) {
            if (this.abbrevEndPosition != null && this.component != null && this.doc != null && this.component.getCaretPosition() == this.abbrevEndPosition.getOffset() && !this.isAbbrevDisabled() && this.doc.getProperty("code-template-insert-handler") == null) {
                pos = this.abbrevEndPosition;
                d = this.component.getDocument();
            }
        }
        if (pos != null && d != null && (operation = CodeTemplateManagerOperation.get(d, pos.getOffset())) != null && (expandKeyStroke = operation.getExpansionKey()).equals(KeyStroke.getKeyStrokeForEvent(evt)) && this.expand(operation)) {
            evt.consume();
        }
    }

    private CharSequence getAbbrevText() {
        return this.abbrevChars;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void resetAbbrevChars() {
        StringBuffer stringBuffer = this.abbrevChars;
        synchronized (stringBuffer) {
            this.abbrevChars.setLength(0);
            this.abbrevEndPosition = null;
        }
    }

    private void appendTypedText(int offset, int insertLength) {
        if (this.abbrevEndPosition == null || offset + insertLength != this.abbrevEndPosition.getOffset()) {
            this.resetAbbrevChars();
        }
        if (this.abbrevEndPosition == null) {
            try {
                if (offset == 0 || this.resetAcceptor.accept(DocumentUtilities.getText((Document)this.doc, (int)(offset - 1), (int)1).charAt(0))) {
                    this.abbrevEndPosition = this.doc.createPosition(offset + insertLength);
                }
            }
            catch (BadLocationException e) {
                ErrorManager.getDefault().notify((Throwable)e);
            }
        }
        if (this.abbrevEndPosition != null) {
            try {
                String typedText = this.doc.getText(offset, insertLength);
                boolean textAccepted = true;
                for (int i = typedText.length() - 1; i >= 0; --i) {
                    if (!this.resetAcceptor.accept(typedText.charAt(i))) continue;
                    textAccepted = false;
                    break;
                }
                if (textAccepted) {
                    this.abbrevChars.append(typedText);
                } else {
                    this.resetAbbrevChars();
                }
            }
            catch (BadLocationException e) {
                ErrorManager.getDefault().notify((Throwable)e);
                this.resetAbbrevChars();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void removeAbbrevText(int offset, int removeLength) {
        StringBuffer stringBuffer = this.abbrevChars;
        synchronized (stringBuffer) {
            if (this.abbrevEndPosition != null) {
                if (offset == this.abbrevEndPosition.getOffset() && this.abbrevChars.length() >= removeLength) {
                    this.abbrevChars.setLength(this.abbrevChars.length() - removeLength);
                } else {
                    this.resetAbbrevChars();
                }
            }
        }
    }

    public boolean expand(CodeTemplateManagerOperation op) {
        CharSequence abbrevText = this.getAbbrevText();
        int abbrevEndOffset = this.abbrevEndPosition.getOffset();
        if (AbbrevDetection.expand(op, this.component, abbrevEndOffset - abbrevText.length(), abbrevText)) {
            this.resetAbbrevChars();
            return true;
        }
        return false;
    }

    private void showSurroundWithHint() {
        try {
            final Position pos = this.doc.createPosition(this.component.getCaretPosition());
            RP.post(new Runnable(){

                @Override
                public void run() {
                    final List<Fix> fixes = SurroundWithFix.getFixes(AbbrevDetection.this.component);
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            if (!fixes.isEmpty()) {
                                AbbrevDetection.this.errorDescription = ErrorDescriptionFactory.createErrorDescription((Severity)Severity.HINT, (String)SURROUND_WITH, (List)(AbbrevDetection.this.surrounsWithFixes = fixes), (Document)AbbrevDetection.this.doc, (Position)pos, (Position)pos);
                                HintsController.setErrors((Document)AbbrevDetection.this.doc, (String)SURROUND_WITH, Collections.singleton(AbbrevDetection.this.errorDescription));
                            } else {
                                AbbrevDetection.this.hideSurroundWithHint();
                            }
                        }
                    });
                }

            });
        }
        catch (BadLocationException ble) {
            Logger.getLogger("global").log(Level.WARNING, ble.getMessage(), ble);
        }
    }

    private void hideSurroundWithHint() {
        if (this.surrounsWithFixes != null) {
            this.surrounsWithFixes = null;
        }
        if (this.errorDescription != null) {
            this.errorDescription = null;
            HintsController.setErrors((Document)this.doc, (String)SURROUND_WITH, Collections.emptySet());
        }
    }

    public static Acceptor getResetAcceptor(Preferences prefs, MimePath mimePath) {
        return prefs != null ? (Acceptor)AbbrevDetection.callFactory(prefs, mimePath, "abbrev-reset-acceptor", (Object)AcceptorFactory.WHITESPACE) : AcceptorFactory.WHITESPACE;
    }

    private static Object callFactory(Preferences prefs, MimePath mimePath, String settingName, Object defaultValue) {
        String factoryRef = prefs.get(settingName, null);
        if (factoryRef != null) {
            int lastDot = factoryRef.lastIndexOf(46);
            assert (lastDot != -1);
            String classFqn = factoryRef.substring(0, lastDot);
            String methodName = factoryRef.substring(lastDot + 1);
            ClassLoader loader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            try {
                Method factoryMethod;
                Class factoryClass = loader.loadClass(classFqn);
                try {
                    factoryMethod = factoryClass.getDeclaredMethod(methodName, MimePath.class, String.class);
                }
                catch (NoSuchMethodException nsme) {
                    try {
                        factoryMethod = factoryClass.getDeclaredMethod(methodName, new Class[0]);
                    }
                    catch (NoSuchMethodException nsme2) {
                        throw nsme;
                    }
                }
                Object value = factoryMethod.getParameterTypes().length == 2 ? factoryMethod.invoke(null, new Object[]{mimePath, settingName}) : factoryMethod.invoke(null, new Object[0]);
                if (value != null) {
                    return value;
                }
            }
            catch (Exception e) {
                LOG.log(Level.WARNING, null, e);
            }
        }
        return defaultValue;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static boolean expand(CodeTemplateManagerOperation op, JTextComponent component, int abbrevStartOffset, CharSequence abbrev) {
        op.waitLoaded();
        CodeTemplate ct = op.findByAbbreviation(abbrev.toString());
        if (ct != null && AbbrevDetection.accept(ct, CodeTemplateManagerOperation.getTemplateFilters(component, abbrevStartOffset))) {
            Document doc = component.getDocument();
            AbbrevDetection.sendUndoableEdit(doc, CloneableEditorSupport.BEGIN_COMMIT_GROUP);
            try {
                doc.remove(abbrevStartOffset, abbrev.length());
                ct.insert(component);
            }
            catch (BadLocationException ble) {}
            finally {
                AbbrevDetection.sendUndoableEdit(doc, CloneableEditorSupport.END_COMMIT_GROUP);
            }
            return true;
        }
        return false;
    }

    private static boolean accept(CodeTemplate template, Collection<? extends CodeTemplateFilter> filters) {
        for (CodeTemplateFilter filter : filters) {
            if (filter.accept(template)) continue;
            return false;
        }
        return true;
    }

    private static void sendUndoableEdit(Document d, UndoableEdit ue) {
        if (d instanceof AbstractDocument) {
            UndoableEditListener[] uels = ((AbstractDocument)d).getUndoableEditListeners();
            UndoableEditEvent ev = new UndoableEditEvent(d, ue);
            for (UndoableEditListener uel : uels) {
                uel.undoableEditHappened(ev);
            }
        }
    }

}

