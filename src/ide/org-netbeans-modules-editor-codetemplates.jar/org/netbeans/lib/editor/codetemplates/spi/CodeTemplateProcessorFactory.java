/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.lib.editor.codetemplates.spi;

import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessor;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

@MimeLocation(subfolderName="CodeTemplateProcessorFactories")
public interface CodeTemplateProcessorFactory {
    public CodeTemplateProcessor createProcessor(CodeTemplateInsertRequest var1);
}

