/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.api;

import java.util.List;
import javax.swing.text.JTextComponent;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.ParametrizedTextParser;

public final class CodeTemplate {
    private final CodeTemplateManagerOperation managerOperation;
    private final String abbreviation;
    private final String description;
    private final String parametrizedText;
    private final List<String> contexts;
    private final String mimePath;
    private String singleLineText = null;

    CodeTemplate(CodeTemplateManagerOperation managerOperation, String abbreviation, String description, String parametrizedText, List<String> contexts, String mimePath) {
        assert (managerOperation != null);
        if (abbreviation == null) {
            throw new NullPointerException("abbreviation cannot be null");
        }
        if (parametrizedText == null) {
            throw new NullPointerException("parametrizedText cannot be null");
        }
        this.managerOperation = managerOperation;
        this.abbreviation = abbreviation;
        this.description = description;
        this.parametrizedText = parametrizedText;
        this.contexts = contexts;
        this.mimePath = mimePath;
    }

    public void insert(JTextComponent component) {
        CodeTemplateManagerOperation.insert(this, component);
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public String getDescription() {
        return this.description;
    }

    public String getParametrizedText() {
        return this.parametrizedText;
    }

    public List<String> getContexts() {
        return this.contexts;
    }

    CodeTemplateManagerOperation getOperation() {
        return this.managerOperation;
    }

    String getSingleLineText() {
        if (this.singleLineText == null) {
            int nlInd = this.parametrizedText.indexOf(10);
            String singleLine = nlInd != -1 ? this.parametrizedText.substring(0, nlInd) + "..." : this.parametrizedText;
            this.singleLineText = ParametrizedTextParser.parseToHtml(new StringBuffer(), singleLine).toString();
        }
        return this.singleLineText;
    }

    String getMimePath() {
        return this.mimePath;
    }

    public String toString() {
        return "abbrev='" + this.getAbbreviation() + "', parametrizedText='" + this.getParametrizedText() + "'";
    }
}

