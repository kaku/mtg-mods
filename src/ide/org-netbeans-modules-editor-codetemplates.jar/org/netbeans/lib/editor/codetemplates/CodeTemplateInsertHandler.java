/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.GuardedException
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.CharacterConversions
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.indent.api.Indent
 *  org.netbeans.modules.editor.indent.api.Reformat
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.GuardedException;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.codetemplates.CodeTemplateParameterImpl;
import org.netbeans.lib.editor.codetemplates.CodeTemplateSpiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.ParametrizedTextParser;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateInsertRequest;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessor;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessorFactory;
import org.netbeans.lib.editor.codetemplates.storage.CodeTemplateSettingsImpl;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegion;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManager;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManagerEvent;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManagerListener;
import org.netbeans.lib.editor.codetemplates.textsync.TextSync;
import org.netbeans.lib.editor.codetemplates.textsync.TextSyncGroup;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.CharacterConversions;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.indent.api.Indent;
import org.netbeans.modules.editor.indent.api.Reformat;

public final class CodeTemplateInsertHandler
implements TextRegionManagerListener,
Runnable {
    private static final Logger LOG = Logger.getLogger(CodeTemplateInsertHandler.class.getName());
    private static final Logger TIMERS = Logger.getLogger("TIMER");
    private static final Object CT_HANDLER_DOC_PROPERTY = "code-template-insert-handler";
    private final CodeTemplate codeTemplate;
    private final JTextComponent component;
    private final List<CodeTemplateProcessor> processors;
    private String parametrizedText;
    private ParametrizedTextParser parametrizedTextParser;
    private String insertText;
    private List<CodeTemplateParameter> allParameters;
    private List<CodeTemplateParameter> masterParameters;
    private CodeTemplateInsertRequest request;
    private boolean inserted;
    private boolean released;
    private boolean completionInvoke;
    private TextRegion completeTextRegion;
    private String completeInsertString;
    private Reformat formatter;
    private Indent indenter;
    private TextSyncGroup textSyncGroup;

    public CodeTemplateInsertHandler(CodeTemplate codeTemplate, JTextComponent component, Collection<? extends CodeTemplateProcessorFactory> processorFactories, CodeTemplateSettingsImpl.OnExpandAction onExpandAction) {
        this.codeTemplate = codeTemplate;
        this.component = component;
        this.completeTextRegion = new TextRegion();
        TextSync completeTextSync = new TextSync(this.completeTextRegion);
        this.textSyncGroup = new TextSyncGroup(completeTextSync);
        this.request = CodeTemplateSpiPackageAccessor.get().createInsertRequest(this);
        this.setParametrizedText(codeTemplate.getParametrizedText());
        this.processors = new ArrayList<CodeTemplateProcessor>();
        for (CodeTemplateProcessorFactory factory : processorFactories) {
            this.processors.add(factory.createProcessor(this.request));
        }
        for (CodeTemplateParameter parameter : this.masterParameters) {
            if ("no-format".equals(parameter.getName()) && onExpandAction != CodeTemplateSettingsImpl.OnExpandAction.NOOP) {
                onExpandAction = CodeTemplateSettingsImpl.OnExpandAction.INDENT;
                break;
            }
            if (!"no-indent".equals(parameter.getName())) continue;
            onExpandAction = CodeTemplateSettingsImpl.OnExpandAction.NOOP;
            break;
        }
        switch (onExpandAction) {
            case FORMAT: {
                this.formatter = Reformat.get((Document)component.getDocument());
                break;
            }
            case INDENT: {
                this.indenter = Indent.get((Document)component.getDocument());
            }
        }
        if (TIMERS.isLoggable(Level.FINE)) {
            LogRecord rec = new LogRecord(Level.FINE, "CodeTemplateInsertHandler");
            rec.setParameters(new Object[]{this});
            TIMERS.log(rec);
        }
    }

    public CodeTemplate getCodeTemplate() {
        return this.codeTemplate;
    }

    public JTextComponent getComponent() {
        return this.component;
    }

    public CodeTemplateInsertRequest getRequest() {
        return this.request;
    }

    public synchronized boolean isInserted() {
        return this.inserted;
    }

    public synchronized boolean isReleased() {
        return this.released;
    }

    public String getParametrizedText() {
        return this.parametrizedText;
    }

    public void setParametrizedText(String parametrizedText) {
        int idx;
        for (idx = 0; idx < parametrizedText.length() && Character.isWhitespace(parametrizedText.charAt(idx)); ++idx) {
        }
        this.parametrizedText = CharacterConversions.lineSeparatorToLineFeed((CharSequence)(idx > 0 ? parametrizedText.substring(idx) : parametrizedText));
        this.parseParametrizedText();
    }

    public int getInsertOffset() {
        return this.completeTextRegion.startOffset();
    }

    public String getInsertText() {
        if (this.inserted) {
            try {
                int startOffset = this.getInsertOffset();
                Document doc = this.component.getDocument();
                return doc.getText(startOffset, this.completeTextRegion.endOffset() - startOffset);
            }
            catch (BadLocationException e) {
                LOG.log(Level.WARNING, "Invalid offset", e);
                return "";
            }
        }
        this.checkInsertTextBuilt();
        return this.insertText;
    }

    public List<? extends CodeTemplateParameter> getAllParameters() {
        return Collections.unmodifiableList(this.allParameters);
    }

    public List<? extends CodeTemplateParameter> getMasterParameters() {
        return Collections.unmodifiableList(this.masterParameters);
    }

    public void processTemplate() {
        for (CodeTemplateProcessor processor : this.processors) {
            processor.updateDefaultValues();
        }
        this.insertTemplate();
        if (!this.isEditable()) {
            this.checkInvokeCompletion();
        }
    }

    void checkInsertTextBuilt() {
        if (this.insertText == null) {
            this.insertText = this.buildInsertText();
        }
    }

    void resetCachedInsertText() {
        this.insertText = null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void insertTemplate() {
        block9 : {
            TextRegionManager trm = TextRegionManager.reserve(this.component);
            if (trm == null) {
                return;
            }
            Document doc = this.component.getDocument();
            doc.putProperty(CT_HANDLER_DOC_PROPERTY, this);
            this.completeInsertString = this.getInsertText();
            if (this.formatter != null) {
                this.formatter.lock();
            }
            if (this.indenter != null) {
                this.indenter.lock();
            }
            try {
                if (doc instanceof BaseDocument) {
                    ((BaseDocument)doc).runAtomicAsUser((Runnable)this);
                    break block9;
                }
                this.run();
            }
            finally {
                if (this.formatter != null) {
                    this.formatter.unlock();
                    this.formatter = null;
                }
                if (this.indenter != null) {
                    this.indenter.unlock();
                    this.indenter = null;
                }
                this.completeInsertString = null;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        boolean success = false;
        try {
            Position pos;
            Document doc = this.component.getDocument();
            BaseDocument bdoc = doc instanceof BaseDocument ? (BaseDocument)doc : null;
            Caret caret = this.component.getCaret();
            if (Utilities.isSelectionShowing((Caret)caret)) {
                int removeOffset = this.component.getSelectionStart();
                int removeLength = this.component.getSelectionEnd() - removeOffset;
                pos = doc.createPosition(removeOffset);
                doc.remove(removeOffset, removeLength);
            } else {
                pos = doc.createPosition(caret.getDot());
            }
            this.completeTextRegion.updateBounds(null, TextRegion.createFixedPosition(this.completeInsertString.length()));
            doc.insertString(pos.getOffset(), this.completeInsertString, null);
            pos = doc.createPosition(Math.max(pos.getOffset() - this.completeInsertString.length(), 0));
            if (bdoc != null) {
                bdoc.addUndoableEdit((UndoableEdit)new TemplateInsertUndoEdit(doc));
            }
            TextRegion caretTextRegion = null;
            for (CodeTemplateParameter master : this.masterParameters) {
                CodeTemplateParameterImpl masterImpl = CodeTemplateParameterImpl.get(master);
                if ("cursor".equals(master.getName())) {
                    caretTextRegion = masterImpl.textRegion();
                    this.completionInvoke = master.getHints().get("completionInvoke") != null;
                    continue;
                }
                this.textSyncGroup.addTextSync(masterImpl.textRegion().textSync());
            }
            if (caretTextRegion == null) {
                Position caretFixedPos = TextRegion.createFixedPosition(this.completeInsertString.length());
                caretTextRegion = new TextRegion(caretFixedPos, caretFixedPos);
                TextSync caretTextSync = new TextSync(caretTextRegion);
                caretTextSync.setCaretMarker(true);
            }
            this.textSyncGroup.addTextSync(caretTextRegion.textSync());
            this.textSyncGroup.setClientInfo(this);
            TextRegionManager trm = this.textRegionManager();
            trm.addGroup(this.textSyncGroup, pos.getOffset());
            trm.addTextRegionManagerListener(this);
            this.inserted = true;
            if (bdoc != null) {
                this.component.setCaretPosition(caretTextRegion.startOffset());
                if (this.formatter != null) {
                    this.formatter.reformat(pos.getOffset(), pos.getOffset() + this.completeInsertString.length());
                }
                if (this.indenter != null) {
                    this.indenter.reindent(pos.getOffset(), pos.getOffset() + this.completeInsertString.length());
                }
            }
            if (!this.released) {
                trm.activateGroup(this.textSyncGroup);
            }
            success = true;
        }
        catch (GuardedException ge) {
            LOG.log(Level.FINE, null, (Throwable)ge);
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, "Invalid offset", e);
        }
        finally {
            this.resetCachedInsertText();
            if (!success) {
                this.inserted = false;
                this.release();
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("CodeTemplateInsertHandler.insertTemplate()\n");
            LOG.fine(this.toStringDetail());
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer(this.textRegionManager().toString() + "\n");
            }
        }
    }

    private TextRegionManager textRegionManager() {
        return TextRegionManager.get(this.component.getDocument(), true);
    }

    public String getDocParameterValue(CodeTemplateParameterImpl paramImpl) {
        String parameterText;
        TextRegion textRegion = paramImpl.textRegion();
        int offset = textRegion.startOffset();
        int len = textRegion.endOffset() - offset;
        try {
            parameterText = this.component.getDocument().getText(offset, len);
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, "Invalid offset", e);
            parameterText = "";
        }
        return parameterText;
    }

    public void setDocMasterParameterValue(CodeTemplateParameterImpl paramImpl, String newValue) {
        assert (!paramImpl.isSlave());
        TextRegion textRegion = paramImpl.textRegion();
        int offset = textRegion.startOffset();
        int length = textRegion.endOffset() - offset;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("CodeTemplateInsertHandler.setMasterParameterValue(): parameter-name=" + paramImpl.getName() + ", offset=" + offset + ", length=" + length + ", newValue=\"" + newValue + "\"\n");
        }
        try {
            Document doc = this.component.getDocument();
            CharSequence parameterText = DocumentUtilities.getText((Document)doc, (int)offset, (int)length);
            if (!CharSequenceUtilities.textEquals((CharSequence)parameterText, (CharSequence)newValue)) {
                textRegion.textSync().setText(newValue);
                this.notifyParameterUpdate(paramImpl.getParameter(), false);
            }
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, "Invalid offset", e);
        }
    }

    private void notifyParameterUpdate(CodeTemplateParameter parameter, boolean typingChange) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("CodeTemplateInsertHandler.notifyParameterUpdate() CALLED for " + parameter.getName() + "\n");
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer(this.textRegionManager().toString() + "\n");
            }
        }
        for (CodeTemplateProcessor processor : this.processors) {
            processor.parameterValueChanged(parameter, typingChange);
        }
    }

    private void parseParametrizedText() {
        this.allParameters = new ArrayList<CodeTemplateParameter>(2);
        this.masterParameters = new ArrayList<CodeTemplateParameter>(2);
        this.parametrizedTextParser = new ParametrizedTextParser(this, this.parametrizedText);
        this.parametrizedTextParser.parse();
    }

    void notifyParameterParsed(CodeTemplateParameterImpl paramImpl) {
        this.allParameters.add(paramImpl.getParameter());
        for (CodeTemplateParameter master : this.masterParameters) {
            if (!master.getName().equals(paramImpl.getName())) continue;
            paramImpl.markSlave(master);
            CodeTemplateParameterImpl masterImpl = CodeTemplateParameterImpl.get(master);
            TextSync textSync = masterImpl.textRegion().textSync();
            textSync.addRegion(paramImpl.textRegion());
            return;
        }
        this.masterParameters.add(paramImpl.getParameter());
        TextSync textSync = new TextSync(paramImpl.textRegion());
        if (paramImpl.isEditable()) {
            textSync.setEditable(true);
        }
        if ("cursor".equals(paramImpl.getName())) {
            textSync.setCaretMarker(true);
        }
    }

    @Override
    public void stateChanged(TextRegionManagerEvent evt) {
        TextRegionManager trm = evt.textRegionManager();
        if (evt.isFocusChange()) {
            List removedGroups = evt.removedGroups();
            for (int i = removedGroups.size() - 1; i >= 0; --i) {
                CodeTemplateInsertHandler handler = (CodeTemplateInsertHandler)removedGroups.get(i).clientInfo();
                if (handler != this) continue;
                this.release();
                if (!this.isEditable()) break;
                this.checkInvokeCompletion();
                break;
            }
            if (removedGroups.size() > 0) {
                TextSync textSync = trm.activeTextSync();
                if (textSync != null) {
                    TextSyncGroup activeGroup = textSync.group();
                    CodeTemplateInsertHandler activeHandler = (CodeTemplateInsertHandler)activeGroup.clientInfo();
                    if (activeHandler == this) {
                        textSync.syncByMaster();
                        CodeTemplateParameterImpl activeMasterImpl = (CodeTemplateParameterImpl)textSync.masterRegion().clientInfo();
                        activeMasterImpl.markUserModified();
                        this.component.getDocument().putProperty(CT_HANDLER_DOC_PROPERTY, this);
                    }
                } else {
                    this.component.getDocument().putProperty(CT_HANDLER_DOC_PROPERTY, null);
                }
            }
        } else {
            TextSync activeTextSync = trm.activeTextSync();
            CodeTemplateParameterImpl activeMasterImpl = (CodeTemplateParameterImpl)activeTextSync.masterRegion().clientInfo();
            if (activeMasterImpl != null) {
                activeMasterImpl.markUserModified();
                this.notifyParameterUpdate(activeMasterImpl.getParameter(), true);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void release() {
        CodeTemplateInsertHandler codeTemplateInsertHandler = this;
        synchronized (codeTemplateInsertHandler) {
            if (this.released) {
                return;
            }
            this.released = true;
        }
        TextRegionManager trm = this.textRegionManager();
        if (this.textSyncGroup.textRegionManager() == trm) {
            trm.stopGroupEditing(this.textSyncGroup);
        }
        trm.removeTextRegionManagerListener(this);
        if (LOG.isLoggable(Level.FINE)) {
            if (LOG.isLoggable(Level.FINER)) {
                LOG.log(Level.INFO, "", new Exception());
            }
            LOG.fine("CodeTemplateInsertHandler.release() CALLED\n");
            LOG.fine(this.toStringDetail());
        }
        for (CodeTemplateProcessor processor : this.processors) {
            processor.release();
        }
    }

    private String buildInsertText() {
        return this.parametrizedTextParser.buildInsertText(this.allParameters);
    }

    private void checkInvokeCompletion() {
        if (this.completionInvoke) {
            this.completionInvoke = false;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Completion.get().showCompletion();
                }
            });
        }
    }

    private boolean isEditable() {
        for (CodeTemplateParameter param : this.masterParameters) {
            if (!param.isEditable()) continue;
            return true;
        }
        return false;
    }

    public String toString() {
        return "Abbrev: \"" + this.codeTemplate.getAbbreviation() + "\"";
    }

    String toStringDetail() {
        StringBuilder sb = new StringBuilder();
        for (CodeTemplateParameter param : this.allParameters) {
            CodeTemplateParameterImpl paramImpl = CodeTemplateParameterImpl.get(param);
            sb.append("  ").append(paramImpl.getName()).append(":");
            sb.append(paramImpl.textRegion());
            if (!paramImpl.isSlave()) {
                sb.append(" Master");
            }
            sb.append('\n');
        }
        return sb.toString();
    }

    private static final class TemplateInsertUndoEdit
    extends AbstractUndoableEdit {
        private Document doc;
        private boolean inactive;

        TemplateInsertUndoEdit(Document doc) {
            assert (doc != null);
            this.doc = doc;
        }

        @Override
        public void undo() throws CannotUndoException {
            super.undo();
            if (!this.inactive) {
                this.inactive = true;
                CodeTemplateInsertHandler handler = (CodeTemplateInsertHandler)this.doc.getProperty(CT_HANDLER_DOC_PROPERTY);
                if (handler != null) {
                    handler.release();
                }
            }
        }
    }

}

