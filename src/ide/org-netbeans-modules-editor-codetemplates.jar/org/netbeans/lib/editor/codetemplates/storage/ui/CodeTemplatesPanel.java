/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.editor.Acceptor
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$InputLine
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package org.netbeans.lib.editor.codetemplates.storage.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.editor.Acceptor;
import org.netbeans.lib.editor.codetemplates.AbbrevDetection;
import org.netbeans.lib.editor.codetemplates.storage.CodeTemplateSettingsImpl;
import org.netbeans.lib.editor.codetemplates.storage.ui.CodeTemplatesModel;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class CodeTemplatesPanel
extends JPanel
implements ActionListener,
ListSelectionListener,
KeyListener,
DocumentListener {
    private static final Logger LOG = Logger.getLogger(CodeTemplatesPanel.class.getName());
    private CodeTemplatesModel model;
    private String selectedLanguage;
    private String panelLanguage;
    private int unsavedTemplateIndex = -1;
    private int forceRowIndex = -1;
    private JButton bNew;
    private JButton bRemove;
    private JComboBox cbExpandTemplateOn;
    private JComboBox cbLanguage;
    private JComboBox cbOnExpandAction;
    private JEditorPane epDescription;
    private JEditorPane epExpandedText;
    private JSplitPane jSplitPane1;
    private JLabel lExplandTemplateOn;
    private JLabel lLanguage;
    private JLabel lOnExpandAction;
    private JLabel lTemplates;
    private JScrollPane spDescription;
    private JScrollPane spExpandedText;
    private JScrollPane spTemplates;
    private JTable tTemplates;
    private JTabbedPane tabPane;
    private JScrollPane spContexts;
    private JList<String> lContexts;
    private Set<String> selectedContexts;

    public CodeTemplatesPanel() {
        this.initComponents();
        CodeTemplatesPanel.loc(this.lLanguage, "Language");
        CodeTemplatesPanel.loc(this.lTemplates, "Templates");
        CodeTemplatesPanel.loc(this.bNew, "New");
        CodeTemplatesPanel.loc(this.bRemove, "Remove");
        CodeTemplatesPanel.loc(this.lExplandTemplateOn, "ExpandTemplateOn");
        CodeTemplatesPanel.loc(this.lOnExpandAction, "OnExpandAction");
        CodeTemplatesPanel.loc(this.tabPane, 0, "Expanded_Text", this.epExpandedText);
        CodeTemplatesPanel.loc(this.tabPane, 1, "Description", this.epDescription);
        this.tabPane.getAccessibleContext().setAccessibleName(CodeTemplatesPanel.loc("AN_tabPane"));
        this.tabPane.getAccessibleContext().setAccessibleDescription(CodeTemplatesPanel.loc("AD_tabPane"));
        this.cbExpandTemplateOn.addItem(CodeTemplatesPanel.loc("SPACE"));
        this.cbExpandTemplateOn.addItem(CodeTemplatesPanel.loc("S-SPACE"));
        this.cbExpandTemplateOn.addItem(CodeTemplatesPanel.loc("TAB"));
        this.cbExpandTemplateOn.addItem(CodeTemplatesPanel.loc("ENTER"));
        this.cbOnExpandAction.addItem(CodeTemplatesPanel.loc("FORMAT"));
        this.cbOnExpandAction.addItem(CodeTemplatesPanel.loc("INDENT"));
        this.cbOnExpandAction.addItem(CodeTemplatesPanel.loc("NOOP"));
        this.bRemove.setEnabled(false);
        this.tTemplates.getTableHeader().setReorderingAllowed(false);
        this.tTemplates.getSelectionModel().setSelectionMode(0);
        this.epExpandedText.addKeyListener(this);
        this.epDescription.addKeyListener(this);
        this.lContexts = new JList();
        this.lContexts.setCellRenderer(new ListRenderer());
        CheckListener checkListener = new CheckListener();
        this.lContexts.addMouseListener((MouseListener)checkListener);
        this.lContexts.addKeyListener((KeyListener)checkListener);
        this.spContexts = new JScrollPane(this.lContexts);
    }

    private static String loc(String key) {
        return NbBundle.getMessage(CodeTemplatesPanel.class, (String)key);
    }

    private static void loc(Component c, String key) {
        if (!(c instanceof JLabel)) {
            c.getAccessibleContext().setAccessibleName(CodeTemplatesPanel.loc("AN_" + key));
            c.getAccessibleContext().setAccessibleDescription(CodeTemplatesPanel.loc("AD_" + key));
        }
        if (c instanceof AbstractButton) {
            Mnemonics.setLocalizedText((AbstractButton)((AbstractButton)c), (String)CodeTemplatesPanel.loc("CTL_" + key));
        } else {
            Mnemonics.setLocalizedText((JLabel)((JLabel)c), (String)CodeTemplatesPanel.loc("CTL_" + key));
        }
    }

    private static void loc(JTabbedPane p, int tabIdx, String key, JEditorPane ep) {
        JLabel label = new JLabel();
        String tabName = CodeTemplatesPanel.loc("CTL_" + key);
        Mnemonics.setLocalizedText((JLabel)label, (String)tabName);
        p.setTitleAt(tabIdx, label.getText());
        int idx = Mnemonics.findMnemonicAmpersand((String)tabName);
        if (idx != -1 && idx + 1 < tabName.length()) {
            char ch = Character.toUpperCase(tabName.charAt(idx + 1));
            p.setMnemonicAt(tabIdx, ch);
            if (ep != null) {
                ep.setFocusAccelerator(ch);
            }
        }
    }

    void update() {
        String mimeType;
        this.model = new CodeTemplatesModel();
        String lastPanelLanguage = this.panelLanguage;
        int lastSelectedRowIndex = this.tTemplates.getSelectedRow();
        this.selectedLanguage = null;
        this.panelLanguage = null;
        this.cbLanguage.removeActionListener(this);
        this.bNew.removeActionListener(this);
        this.bRemove.removeActionListener(this);
        this.cbExpandTemplateOn.removeActionListener(this);
        this.cbOnExpandAction.removeActionListener(this);
        this.tTemplates.getSelectionModel().removeListSelectionListener(this);
        String defaultSelectedLang = null;
        Object selectedItem = this.cbLanguage.getSelectedItem();
        if (selectedItem instanceof String) {
            defaultSelectedLang = (String)selectedItem;
        }
        this.cbLanguage.removeAllItems();
        ArrayList<String> languages = new ArrayList<String>(this.model.getLanguages());
        Collections.sort(languages);
        for (String l : languages) {
            this.cbLanguage.addItem(l);
        }
        if (languages.isEmpty()) {
            this.cbLanguage.setEnabled(false);
            this.bNew.setEnabled(false);
            this.bRemove.setEnabled(false);
            this.tTemplates.setEnabled(false);
            this.tabPane.setEnabled(false);
            this.cbExpandTemplateOn.setEnabled(false);
        }
        KeyStroke expander = this.model.getExpander();
        if (KeyStroke.getKeyStroke(32, 1).equals(expander)) {
            this.cbExpandTemplateOn.setSelectedIndex(1);
        } else if (KeyStroke.getKeyStroke(9, 0).equals(expander)) {
            this.cbExpandTemplateOn.setSelectedIndex(2);
        } else if (KeyStroke.getKeyStroke(10, 0).equals(expander)) {
            this.cbExpandTemplateOn.setSelectedIndex(3);
        } else {
            this.cbExpandTemplateOn.setSelectedIndex(0);
        }
        CodeTemplateSettingsImpl.OnExpandAction onExpandAction = this.model.getOnExpandAction();
        switch (onExpandAction) {
            case FORMAT: {
                this.cbOnExpandAction.setSelectedIndex(0);
                break;
            }
            case INDENT: {
                this.cbOnExpandAction.setSelectedIndex(1);
                break;
            }
            default: {
                this.cbOnExpandAction.setSelectedIndex(2);
            }
        }
        this.cbLanguage.addActionListener(this);
        this.bNew.addActionListener(this);
        this.bRemove.addActionListener(this);
        this.cbExpandTemplateOn.addActionListener(this);
        this.cbOnExpandAction.addActionListener(this);
        this.tTemplates.getSelectionModel().addListSelectionListener(this);
        JTextComponent pane = EditorRegistry.lastFocusedComponent();
        if (defaultSelectedLang == null && pane != null && (mimeType = (String)pane.getDocument().getProperty("mimeType")) != null) {
            defaultSelectedLang = this.model.findLanguage(mimeType);
        }
        if (defaultSelectedLang == null) {
            defaultSelectedLang = this.model.findLanguage("text/x-java");
        }
        if (defaultSelectedLang == null) {
            defaultSelectedLang = this.model.findLanguage("text/x-ruby");
        }
        if (defaultSelectedLang == null) {
            defaultSelectedLang = this.model.findLanguage("text/x-c++");
        }
        if (defaultSelectedLang == null && this.model.getLanguages().size() > 0) {
            defaultSelectedLang = this.model.getLanguages().get(0);
        }
        this.forceRowIndex = -1;
        if (defaultSelectedLang != null) {
            this.cbLanguage.setSelectedItem(defaultSelectedLang);
            if (defaultSelectedLang.equals(lastPanelLanguage)) {
                this.forceRowIndex = lastSelectedRowIndex;
            }
        }
    }

    void applyChanges() {
        this.saveCurrentTemplate();
        if (this.model != null) {
            this.model.saveChanges();
        }
    }

    void cancel() {
    }

    boolean dataValid() {
        return true;
    }

    boolean isChanged() {
        this.saveCurrentTemplate();
        if (this.model == null) {
            return false;
        }
        return this.model.isChanged();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.cbLanguage) {
            this.saveCurrentTemplate();
            this.selectedLanguage = (String)this.cbLanguage.getSelectedItem();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (CodeTemplatesPanel.this.selectedLanguage.equals(CodeTemplatesPanel.this.panelLanguage)) {
                        return;
                    }
                    CodeTemplatesPanel.this.saveCurrentTemplate();
                    CodeTemplatesPanel.this.panelLanguage = CodeTemplatesPanel.this.selectedLanguage;
                    CodeTemplatesModel.TM tableModel = CodeTemplatesPanel.this.model.getTableModel(CodeTemplatesPanel.this.panelLanguage);
                    CodeTemplatesPanel.this.tTemplates.setModel(tableModel);
                    TableColumn c1 = CodeTemplatesPanel.this.tTemplates.getTableHeader().getColumnModel().getColumn(0);
                    c1.setMinWidth(80);
                    c1.setPreferredWidth(100);
                    c1.setResizable(true);
                    TableColumn c2 = CodeTemplatesPanel.this.tTemplates.getTableHeader().getColumnModel().getColumn(1);
                    c2.setMinWidth(180);
                    c2.setPreferredWidth(250);
                    c2.setResizable(true);
                    TableColumn c3 = CodeTemplatesPanel.this.tTemplates.getTableHeader().getColumnModel().getColumn(2);
                    c3.setMinWidth(180);
                    c3.setPreferredWidth(250);
                    c3.setResizable(true);
                    CodeTemplatesPanel.this.epExpandedText.getDocument().removeDocumentListener(CodeTemplatesPanel.this);
                    CodeTemplatesPanel.this.epDescription.getDocument().removeDocumentListener(CodeTemplatesPanel.this);
                    CodeTemplatesPanel.this.epDescription.setEditorKit(CloneableEditorSupport.getEditorKit((String)"text/html"));
                    CodeTemplatesPanel.this.epExpandedText.setEditorKit(CloneableEditorSupport.getEditorKit((String)CodeTemplatesPanel.this.model.getMimeType(CodeTemplatesPanel.this.panelLanguage)));
                    int rowCount = tableModel.getRowCount();
                    int selectRowIndex = CodeTemplatesPanel.this.forceRowIndex != -1 && CodeTemplatesPanel.this.forceRowIndex < rowCount ? CodeTemplatesPanel.this.forceRowIndex : 0;
                    CodeTemplatesPanel.this.forceRowIndex = -1;
                    if (selectRowIndex < rowCount) {
                        CodeTemplatesPanel.this.tTemplates.getSelectionModel().setSelectionInterval(selectRowIndex, selectRowIndex);
                    }
                    CodeTemplatesPanel.this.epExpandedText.getDocument().addDocumentListener(CodeTemplatesPanel.this);
                    CodeTemplatesPanel.this.epDescription.getDocument().addDocumentListener(CodeTemplatesPanel.this);
                    ListModel<String> supportedContexts = tableModel.getSupportedContexts();
                    if (supportedContexts.getSize() > 0) {
                        CodeTemplatesPanel.this.lContexts.setModel(supportedContexts);
                        if (CodeTemplatesPanel.this.tabPane.getTabCount() < 3) {
                            CodeTemplatesPanel.this.tabPane.addTab(null, CodeTemplatesPanel.this.spContexts);
                            CodeTemplatesPanel.loc(CodeTemplatesPanel.this.tabPane, 2, "Contexts", null);
                        }
                    } else if (CodeTemplatesPanel.this.tabPane.getTabCount() > 2) {
                        CodeTemplatesPanel.this.tabPane.remove(2);
                    }
                }
            });
        } else if (e.getSource() == this.bNew) {
            this.saveCurrentTemplate();
            NotifyDescriptor.InputLine descriptor = new NotifyDescriptor.InputLine(CodeTemplatesPanel.loc("CTL_Enter_template_name"), CodeTemplatesPanel.loc("CTL_New_template_dialog_title"));
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)descriptor) == NotifyDescriptor.InputLine.OK_OPTION) {
                String newAbbrev = descriptor.getInputText().trim();
                if (newAbbrev.length() == 0) {
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)CodeTemplatesPanel.loc("CTL_Empty_template_name"), 0));
                } else if (!this.checkAbbrev(newAbbrev, this.model.getMimeType(this.panelLanguage))) {
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)CodeTemplatesPanel.loc("CTL_Rejected_template_name"), 0));
                } else {
                    int i;
                    CodeTemplatesModel.TM tableModel = (CodeTemplatesModel.TM)this.tTemplates.getModel();
                    int rows = tableModel.getRowCount();
                    for (i = 0; i < rows; ++i) {
                        String abbrev = tableModel.getAbbreviation(i);
                        if (!newAbbrev.equals(abbrev)) continue;
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)CodeTemplatesPanel.loc("CTL_Duplicate_template_name"), 0));
                        break;
                    }
                    if (i == rows) {
                        int rowIdx = this.tTemplates.convertRowIndexToView(tableModel.addCodeTemplate(newAbbrev));
                        this.tTemplates.getSelectionModel().setSelectionInterval(rowIdx, rowIdx);
                    }
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        CodeTemplatesPanel.this.spTemplates.getVerticalScrollBar().setValue(CodeTemplatesPanel.this.spTemplates.getVerticalScrollBar().getMaximum());
                        CodeTemplatesPanel.this.tabPane.setSelectedIndex(0);
                        CodeTemplatesPanel.this.epExpandedText.requestFocus();
                    }
                });
            }
        } else if (e.getSource() == this.bRemove) {
            CodeTemplatesModel.TM tableModel = (CodeTemplatesModel.TM)this.tTemplates.getModel();
            int index = this.tTemplates.convertRowIndexToModel(this.tTemplates.getSelectedRow());
            this.unsavedTemplateIndex = -1;
            tableModel.removeCodeTemplate(index);
            int rowCount = tableModel.getRowCount();
            if (index < rowCount) {
                this.tTemplates.getSelectionModel().setSelectionInterval(index, index);
            } else if (rowCount > 0) {
                this.tTemplates.getSelectionModel().setSelectionInterval(rowCount - 1, rowCount - 1);
            } else {
                this.bRemove.setEnabled(false);
            }
        } else if (e.getSource() == this.cbExpandTemplateOn) {
            switch (this.cbExpandTemplateOn.getSelectedIndex()) {
                case 0: {
                    this.model.setExpander(KeyStroke.getKeyStroke(32, 0));
                    break;
                }
                case 1: {
                    this.model.setExpander(KeyStroke.getKeyStroke(32, 1));
                    break;
                }
                case 2: {
                    this.model.setExpander(KeyStroke.getKeyStroke(9, 0));
                    break;
                }
                case 3: {
                    this.model.setExpander(KeyStroke.getKeyStroke(10, 0));
                }
            }
        } else if (e.getSource() == this.cbOnExpandAction) {
            switch (this.cbOnExpandAction.getSelectedIndex()) {
                case 0: {
                    this.model.setOnExpandAction(CodeTemplateSettingsImpl.OnExpandAction.FORMAT);
                    break;
                }
                case 1: {
                    this.model.setOnExpandAction(CodeTemplateSettingsImpl.OnExpandAction.INDENT);
                    break;
                }
                default: {
                    this.model.setOnExpandAction(CodeTemplateSettingsImpl.OnExpandAction.NOOP);
                }
            }
        }
    }

    private boolean checkAbbrev(String abbrev, String mimeType) {
        MimePath mimePath = MimePath.get((String)mimeType);
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
        Acceptor acceptor = AbbrevDetection.getResetAcceptor(prefs, mimePath);
        for (int i = 0; i < abbrev.length(); ++i) {
            if (!acceptor.accept(abbrev.charAt(i))) continue;
            return false;
        }
        return true;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        this.saveCurrentTemplate();
        int index = this.tTemplates.getSelectedRow();
        if (index < 0) {
            this.epDescription.setText("");
            this.epExpandedText.setText("");
            this.bRemove.setEnabled(false);
            this.unsavedTemplateIndex = -1;
            return;
        }
        CodeTemplatesModel.TM tableModel = (CodeTemplatesModel.TM)this.tTemplates.getModel();
        int convertRowIndexToModel = this.tTemplates.convertRowIndexToModel(index);
        CodeTemplatesPanel.setDocumentText(this.epDescription.getDocument(), tableModel.getDescription(convertRowIndexToModel));
        CodeTemplatesPanel.setDocumentText(this.epExpandedText.getDocument(), tableModel.getText(convertRowIndexToModel));
        this.selectedContexts = tableModel.getContexts(convertRowIndexToModel);
        this.lContexts.repaint();
        this.unsavedTemplateIndex = -1;
        this.bRemove.setEnabled(true);
        if (index != convertRowIndexToModel) {
            this.tTemplates.scrollRectToVisible(new Rectangle(this.tTemplates.getCellRect(index, 0, true)));
        }
    }

    private static void setDocumentText(Document doc, String text) {
        try {
            doc.remove(0, doc.getLength());
            doc.insertString(0, text, null);
        }
        catch (BadLocationException ble) {
            LOG.log(Level.WARNING, null, ble);
        }
    }

    private void saveCurrentTemplate() {
        if (this.unsavedTemplateIndex < 0) {
            return;
        }
        CodeTemplatesModel.TM tableModel = (CodeTemplatesModel.TM)this.tTemplates.getModel();
        try {
            tableModel.setDescription(this.unsavedTemplateIndex, CharSequenceUtilities.toString((CharSequence)DocumentUtilities.getText((Document)this.epDescription.getDocument(), (int)0, (int)this.epDescription.getDocument().getLength())));
            tableModel.setText(this.unsavedTemplateIndex, CharSequenceUtilities.toString((CharSequence)DocumentUtilities.getText((Document)this.epExpandedText.getDocument(), (int)0, (int)this.epExpandedText.getDocument().getLength())));
            this.unsavedTemplateIndex = -1;
        }
        catch (BadLocationException ble) {
            Exceptions.printStackTrace((Throwable)ble);
        }
        this.firePropertyChange("changed", null, null);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 32) {
            e.consume();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void textModified() {
        if (this.unsavedTemplateIndex < 0) {
            int row = this.tTemplates.getSelectedRow();
            this.unsavedTemplateIndex = row < 0 ? -1 : this.tTemplates.convertRowIndexToModel(row);
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        this.textModified();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        this.textModified();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    private void initComponents() {
        this.lLanguage = new JLabel();
        this.cbLanguage = new JComboBox();
        this.lTemplates = new JLabel();
        this.bNew = new JButton();
        this.bRemove = new JButton();
        this.lExplandTemplateOn = new JLabel();
        this.cbExpandTemplateOn = new JComboBox();
        this.lOnExpandAction = new JLabel();
        this.cbOnExpandAction = new JComboBox();
        this.jSplitPane1 = new JSplitPane();
        this.tabPane = new JTabbedPane();
        this.spExpandedText = new JScrollPane();
        this.epExpandedText = new JEditorPane();
        this.spDescription = new JScrollPane();
        this.epDescription = new JEditorPane();
        this.spTemplates = new JScrollPane();
        this.tTemplates = new JTable();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.lLanguage.setLabelFor(this.cbLanguage);
        this.lLanguage.setText("Language:");
        this.cbLanguage.setNextFocusableComponent(this.tTemplates);
        this.lTemplates.setLabelFor(this.tTemplates);
        this.lTemplates.setText("Templates:");
        this.bNew.setText("New");
        this.bNew.setNextFocusableComponent(this.bRemove);
        this.bRemove.setText("Remove");
        this.lExplandTemplateOn.setLabelFor(this.cbExpandTemplateOn);
        this.lExplandTemplateOn.setText("Expand Template on:");
        this.cbExpandTemplateOn.setNextFocusableComponent(this.cbOnExpandAction);
        this.lOnExpandAction.setLabelFor(this.cbOnExpandAction);
        this.lOnExpandAction.setText("On Template Expand:");
        this.cbOnExpandAction.setNextFocusableComponent(this.bNew);
        this.jSplitPane1.setDividerLocation(150);
        this.jSplitPane1.setOrientation(0);
        this.jSplitPane1.setCursor(new Cursor(0));
        this.tabPane.setTabLayoutPolicy(1);
        this.tabPane.setFocusCycleRoot(true);
        this.tabPane.setNextFocusableComponent(this.cbExpandTemplateOn);
        this.spExpandedText.setViewportView(this.epExpandedText);
        this.tabPane.addTab("tab1", this.spExpandedText);
        this.spDescription.setViewportView(this.epDescription);
        this.tabPane.addTab("tab2", this.spDescription);
        this.jSplitPane1.setBottomComponent(this.tabPane);
        this.tTemplates.setAutoCreateRowSorter(true);
        this.tTemplates.setModel(new DefaultTableModel(new Object[][]{{null, null, null}, {null, null, null}, {null, null, null}, {null, null, null}}, new String[]{"Abbreviation", "Expanded Text", "Description"}){
            Class[] types;
            boolean[] canEdit;

            public Class getColumnClass(int columnIndex) {
                return this.types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return this.canEdit[columnIndex];
            }
        });
        this.tTemplates.setFocusCycleRoot(true);
        this.spTemplates.setViewportView(this.tTemplates);
        this.jSplitPane1.setLeftComponent(this.spTemplates);
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.lLanguage).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbLanguage, -2, -1, -2)).addComponent(this.lTemplates).addGroup(layout.createSequentialGroup().addComponent(this.lExplandTemplateOn).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbExpandTemplateOn, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.lOnExpandAction).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.cbOnExpandAction, -2, -1, -2)).addGroup(layout.createSequentialGroup().addComponent(this.jSplitPane1, -1, 343, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.bNew, -1, -1, 32767).addComponent(this.bRemove, -1, -1, 32767)))).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.lLanguage).addComponent(this.cbLanguage, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lTemplates).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(this.bNew).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.bRemove).addGap(0, 0, 32767)).addComponent(this.jSplitPane1, -1, 318, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.cbExpandTemplateOn, -2, -1, -2).addComponent(this.lOnExpandAction).addComponent(this.cbOnExpandAction, -2, -1, -2).addComponent(this.lExplandTemplateOn)).addGap(5, 5, 5)));
    }

    private class CheckListener
    implements MouseListener,
    KeyListener {
        private CheckListener() {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (!e.isPopupTrigger()) {
                this.contextsModified();
                e.consume();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == 32 || e.getKeyCode() == 10) {
                this.contextsModified();
                e.consume();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        private void contextsModified() {
            String value = (String)CodeTemplatesPanel.this.lContexts.getSelectedValue();
            if (CodeTemplatesPanel.this.selectedContexts != null && !CodeTemplatesPanel.this.selectedContexts.remove(value)) {
                CodeTemplatesPanel.this.selectedContexts.add(value);
            }
            CodeTemplatesPanel.this.lContexts.repaint();
            if (CodeTemplatesPanel.this.unsavedTemplateIndex < 0) {
                int row = CodeTemplatesPanel.this.tTemplates.getSelectedRow();
                CodeTemplatesPanel.this.unsavedTemplateIndex = row < 0 ? -1 : CodeTemplatesPanel.this.tTemplates.convertRowIndexToModel(row);
            }
        }
    }

    private class ListRenderer
    implements ListCellRenderer<String> {
        private final JCheckBox renderer;

        private ListRenderer() {
            this.renderer = new JCheckBox();
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends String> list, String value, int index, boolean isSelected, boolean cellHasFocus) {
            this.renderer.setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
            this.renderer.setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
            this.renderer.setText(value);
            this.renderer.setSelected(CodeTemplatesPanel.this.selectedContexts != null && CodeTemplatesPanel.this.selectedContexts.contains(value));
            this.renderer.setOpaque(true);
            return this.renderer;
        }
    }

}

