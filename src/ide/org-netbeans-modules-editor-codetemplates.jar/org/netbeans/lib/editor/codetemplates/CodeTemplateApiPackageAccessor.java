/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.List;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager;

public abstract class CodeTemplateApiPackageAccessor {
    private static CodeTemplateApiPackageAccessor INSTANCE;

    public static CodeTemplateApiPackageAccessor get() {
        if (INSTANCE == null) {
            try {
                Class clazz = Class.forName(CodeTemplateManager.class.getName());
            }
            catch (ClassNotFoundException e) {
                // empty catch block
            }
        }
        assert (INSTANCE != null);
        return INSTANCE;
    }

    public static void register(CodeTemplateApiPackageAccessor accessor) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Already registered");
        }
        INSTANCE = accessor;
    }

    public abstract CodeTemplateManager createCodeTemplateManager(CodeTemplateManagerOperation var1);

    public abstract CodeTemplateManagerOperation getOperation(CodeTemplateManager var1);

    public abstract CodeTemplateManagerOperation getOperation(CodeTemplate var1);

    public abstract CodeTemplate createCodeTemplate(CodeTemplateManagerOperation var1, String var2, String var3, String var4, List<String> var5, String var6);

    public abstract String getSingleLineText(CodeTemplate var1);

    public abstract String getCodeTemplateMimePath(CodeTemplate var1);
}

