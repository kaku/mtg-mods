/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.CodeTemplateDescription
 *  org.netbeans.api.editor.settings.CodeTemplateSettings
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.WeakListeners
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.CodeTemplateDescription;
import org.netbeans.api.editor.settings.CodeTemplateSettings;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.lib.editor.codetemplates.CodeTemplateApiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.CodeTemplateComparator;
import org.netbeans.lib.editor.codetemplates.CodeTemplateInsertHandler;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplateManager;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateProcessorFactory;
import org.netbeans.lib.editor.codetemplates.storage.CodeTemplateSettingsImpl;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.RequestProcessor;
import org.openide.util.WeakListeners;

public final class CodeTemplateManagerOperation
implements LookupListener,
Runnable {
    private static final Logger LOG = Logger.getLogger(CodeTemplateManagerOperation.class.getName());
    private static final Map<MimePath, CodeTemplateManagerOperation> mime2operation = new WeakHashMap<MimePath, CodeTemplateManagerOperation>(8);
    private static final KeyStroke DEFAULT_EXPANSION_KEY = KeyStroke.getKeyStroke(9, 0);
    private final CodeTemplateManager manager;
    private final String mimePath;
    private final Lookup.Result<CodeTemplateSettings> ctslr;
    private final EventListenerList listenerList = new EventListenerList();
    private boolean loaded = false;
    private Map<String, CodeTemplate> abbrev2template = Collections.emptyMap();
    private List<CodeTemplate> sortedTemplatesByAbbrev = Collections.emptyList();
    private List<CodeTemplate> sortedTemplatesByParametrizedText = Collections.emptyList();
    private List<CodeTemplate> selectionTemplates = Collections.emptyList();
    private KeyStroke expansionKey = DEFAULT_EXPANSION_KEY;
    private String expansionKeyText = CodeTemplateManagerOperation.getExpandKeyStrokeText(this.expansionKey);
    private static final int MATCH_NO = 0;
    private static final int MATCH_IGNORE_CASE = 1;
    private static final int MATCH = 2;

    public static synchronized CodeTemplateManager getManager(Document doc) {
        String mimeType = (String)doc.getProperty("mimeType");
        return CodeTemplateManagerOperation.get(MimePath.parse((String)mimeType)).getManager();
    }

    public static synchronized CodeTemplateManagerOperation get(Document document, int offset) {
        MimePath mimePath = CodeTemplateManagerOperation.getFullMimePath(document, offset);
        if (mimePath != null) {
            return CodeTemplateManagerOperation.get(mimePath);
        }
        return null;
    }

    public static synchronized CodeTemplateManagerOperation get(MimePath mimePath) {
        CodeTemplateManagerOperation operation = mime2operation.get((Object)mimePath);
        if (operation == null) {
            operation = new CodeTemplateManagerOperation(mimePath);
            mime2operation.put(mimePath, operation);
        }
        return operation;
    }

    private CodeTemplateManagerOperation(MimePath mimePath) {
        this.mimePath = mimePath.getPath();
        this.manager = CodeTemplateApiPackageAccessor.get().createCodeTemplateManager(this);
        assert (this.manager != null);
        this.ctslr = MimeLookup.getLookup((MimePath)mimePath).lookupResult(CodeTemplateSettings.class);
        this.ctslr.addLookupListener((LookupListener)WeakListeners.create(LookupListener.class, (EventListener)((Object)this), this.ctslr));
        RequestProcessor.getDefault().post((Runnable)this);
    }

    public String getMimePath() {
        return this.mimePath;
    }

    public CodeTemplateManager getManager() {
        return this.manager;
    }

    public Collection<? extends CodeTemplate> getCodeTemplates() {
        return this.sortedTemplatesByAbbrev;
    }

    public Collection<? extends CodeTemplate> findSelectionTemplates() {
        return this.selectionTemplates;
    }

    public CodeTemplate findByAbbreviation(String abbreviation) {
        return this.abbrev2template.get(abbreviation);
    }

    public Collection<? extends CodeTemplate> findByAbbreviationPrefix(String prefix, boolean ignoreCase) {
        int i;
        CodeTemplate t;
        int mp;
        ArrayList<CodeTemplate> result = new ArrayList<CodeTemplate>();
        int low = 0;
        int high = this.sortedTemplatesByAbbrev.size() - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            t = this.sortedTemplatesByAbbrev.get(mid);
            int cmp = CodeTemplateManagerOperation.compareTextIgnoreCase(t.getAbbreviation(), prefix);
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            low = mid;
            break;
        }
        for (i = low - 1; i >= 0 && (mp = CodeTemplateManagerOperation.matchPrefix((t = this.sortedTemplatesByAbbrev.get(i)).getAbbreviation(), prefix)) != 0; --i) {
            if (mp == 1) {
                if (!ignoreCase) continue;
                result.add(t);
                continue;
            }
            result.add(t);
        }
        for (i = low; i < this.sortedTemplatesByAbbrev.size() && (mp = CodeTemplateManagerOperation.matchPrefix((t = this.sortedTemplatesByAbbrev.get(i)).getAbbreviation(), prefix)) != 0; ++i) {
            if (mp == 1) {
                if (!ignoreCase) continue;
                result.add(t);
                continue;
            }
            result.add(t);
        }
        return result;
    }

    public Collection<? extends CodeTemplate> findByParametrizedText(String prefix, boolean ignoreCase) {
        int i;
        CodeTemplate t;
        int mp;
        ArrayList<CodeTemplate> result = new ArrayList<CodeTemplate>();
        int low = 0;
        int high = this.sortedTemplatesByParametrizedText.size() - 1;
        while (low <= high) {
            int mid = low + high >> 1;
            t = this.sortedTemplatesByParametrizedText.get(mid);
            int cmp = CodeTemplateManagerOperation.compareTextIgnoreCase(t.getParametrizedText(), prefix);
            if (cmp < 0) {
                low = mid + 1;
                continue;
            }
            if (cmp > 0) {
                high = mid - 1;
                continue;
            }
            low = mid;
            break;
        }
        for (i = low - 1; i >= 0 && (mp = CodeTemplateManagerOperation.matchPrefix((t = this.sortedTemplatesByParametrizedText.get(i)).getParametrizedText(), prefix)) != 0; --i) {
            if (mp == 1) {
                if (!ignoreCase) continue;
                result.add(t);
                continue;
            }
            result.add(t);
        }
        for (i = low; i < this.sortedTemplatesByParametrizedText.size() && (mp = CodeTemplateManagerOperation.matchPrefix((t = this.sortedTemplatesByParametrizedText.get(i)).getParametrizedText(), prefix)) != 0; ++i) {
            if (mp == 1) {
                if (!ignoreCase) continue;
                result.add(t);
                continue;
            }
            result.add(t);
        }
        return result;
    }

    public static Collection<? extends CodeTemplateFilter> getTemplateFilters(JTextComponent component, int offset) {
        MimePath mimeType = CodeTemplateManagerOperation.getFullMimePath(component.getDocument(), offset);
        Collection filterFactories = MimeLookup.getLookup((MimePath)mimeType).lookupAll(CodeTemplateFilter.Factory.class);
        ArrayList<CodeTemplateFilter> result = new ArrayList<CodeTemplateFilter>(filterFactories.size());
        for (CodeTemplateFilter.Factory factory : filterFactories) {
            result.add(factory.createFilter(component, offset));
        }
        return result;
    }

    public static void insert(CodeTemplate codeTemplate, JTextComponent component) {
        String mimePath = CodeTemplateApiPackageAccessor.get().getCodeTemplateMimePath(codeTemplate);
        Collection processorFactories = MimeLookup.getLookup((String)mimePath).lookupAll(CodeTemplateProcessorFactory.class);
        CodeTemplateInsertHandler handler = new CodeTemplateInsertHandler(codeTemplate, component, processorFactories, CodeTemplateSettingsImpl.get(MimePath.parse((String)mimePath)).getOnExpandAction());
        handler.processTemplate();
    }

    private static int matchPrefix(CharSequence text, CharSequence prefix) {
        int i;
        boolean matchCase = true;
        int prefixLength = prefix.length();
        if (prefixLength > text.length()) {
            return 0;
        }
        for (i = 0; i < prefixLength; ++i) {
            char ch2;
            char ch1 = text.charAt(i);
            if (ch1 == (ch2 = prefix.charAt(i))) continue;
            matchCase = false;
            if (Character.toLowerCase(ch1) != Character.toLowerCase(ch2)) break;
        }
        if (i == prefixLength) {
            return matchCase ? 2 : 1;
        }
        return 0;
    }

    private static int compareTextIgnoreCase(CharSequence text1, CharSequence text2) {
        int len = Math.min(text1.length(), text2.length());
        for (int i = 0; i < len; ++i) {
            char ch2;
            char ch1 = Character.toLowerCase(text1.charAt(i));
            if (ch1 == (ch2 = Character.toLowerCase(text2.charAt(i)))) continue;
            return ch1 - ch2;
        }
        return text1.length() - text2.length();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isLoaded() {
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            return this.loaded;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void registerLoadedListener(ChangeListener listener) {
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            if (!this.isLoaded()) {
                this.listenerList.add(ChangeListener.class, listener);
                return;
            }
        }
        listener.stateChanged(new ChangeEvent(this.manager));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void waitLoaded() {
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            while (!this.isLoaded()) {
                try {
                    this.listenerList.wait();
                    continue;
                }
                catch (InterruptedException e) {
                    throw new RuntimeException("Interrupted when waiting to load code templates");
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void fireStateChanged(ChangeEvent evt) {
        Object[] listeners;
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            listeners = this.listenerList.getListenerList();
        }
        for (int i = 0; i < listeners.length; i += 2) {
            if (ChangeListener.class != listeners[i]) continue;
            ((ChangeListener)listeners[i + 1]).stateChanged(evt);
        }
    }

    @Override
    public void run() {
        this.rebuildCodeTemplates();
    }

    private static void processCodeTemplateDescriptions(CodeTemplateManagerOperation operation, Collection<? extends CodeTemplateDescription> ctds, Map<String, CodeTemplate> codeTemplatesMap, List<CodeTemplate> codeTemplatesWithSelection) {
        for (CodeTemplateDescription ctd : ctds) {
            CodeTemplate ct = CodeTemplateApiPackageAccessor.get().createCodeTemplate(operation, ctd.getAbbreviation(), ctd.getDescription(), ctd.getParametrizedText(), ctd.getContexts(), ctd.getMimePath());
            codeTemplatesMap.put(ct.getAbbreviation(), ct);
            if (ct.getParametrizedText().toLowerCase().indexOf("${selection") <= -1) continue;
            codeTemplatesWithSelection.add(ct);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void rebuildCodeTemplates() {
        Collection allCts = this.ctslr.allInstances();
        CodeTemplateSettings cts = allCts.isEmpty() ? null : (CodeTemplateSettings)allCts.iterator().next();
        HashMap<String, CodeTemplate> map = new HashMap<String, CodeTemplate>();
        ArrayList<CodeTemplate> templatesWithSelection = new ArrayList<CodeTemplate>();
        KeyStroke keyStroke = DEFAULT_EXPANSION_KEY;
        if (cts != null) {
            List ctds = cts.getCodeTemplateDescriptions();
            CodeTemplateManagerOperation.processCodeTemplateDescriptions(this, ctds, map, templatesWithSelection);
            keyStroke = CodeTemplateManagerOperation.patchExpansionKey(cts.getExpandKey());
        } else if (LOG.isLoggable(Level.WARNING)) {
            LOG.warning("Can't find CodeTemplateSettings for '" + this.mimePath + "'");
        }
        ArrayList<CodeTemplate> byAbbrev = new ArrayList<CodeTemplate>(map.values());
        Collections.sort(byAbbrev, CodeTemplateComparator.BY_ABBREVIATION_IGNORE_CASE);
        ArrayList<CodeTemplate> byText = new ArrayList<CodeTemplate>(map.values());
        Collections.sort(byText, CodeTemplateComparator.BY_PARAMETRIZED_TEXT_IGNORE_CASE);
        Collections.sort(templatesWithSelection, CodeTemplateComparator.BY_PARAMETRIZED_TEXT_IGNORE_CASE);
        boolean fire = false;
        EventListenerList eventListenerList = this.listenerList;
        synchronized (eventListenerList) {
            fire = this.abbrev2template == null;
            this.abbrev2template = Collections.unmodifiableMap(map);
            this.sortedTemplatesByAbbrev = Collections.unmodifiableList(byAbbrev);
            this.sortedTemplatesByParametrizedText = Collections.unmodifiableList(byText);
            this.selectionTemplates = Collections.unmodifiableList(templatesWithSelection);
            this.expansionKey = keyStroke;
            this.expansionKeyText = CodeTemplateManagerOperation.getExpandKeyStrokeText(keyStroke);
            this.loaded = true;
            this.listenerList.notifyAll();
        }
        if (fire) {
            this.fireStateChanged(new ChangeEvent(this.manager));
        }
    }

    public KeyStroke getExpansionKey() {
        return this.expansionKey;
    }

    public String getExpandKeyStrokeText() {
        return this.expansionKeyText;
    }

    private static String getExpandKeyStrokeText(KeyStroke keyStroke) {
        String expandKeyStrokeText = keyStroke.equals(KeyStroke.getKeyStroke(' ')) ? "SPACE" : (keyStroke.equals(KeyStroke.getKeyStroke(new Character(' '), 1)) ? "Shift-SPACE" : (keyStroke.equals(KeyStroke.getKeyStroke(9, 0)) ? "TAB" : (keyStroke.equals(KeyStroke.getKeyStroke(10, 0)) ? "ENTER" : keyStroke.toString())));
        return expandKeyStrokeText;
    }

    private static KeyStroke patchExpansionKey(KeyStroke eks) {
        if (eks.equals(KeyStroke.getKeyStroke(32, 0))) {
            eks = KeyStroke.getKeyStroke(' ');
        } else if (eks.equals(KeyStroke.getKeyStroke(32, 1))) {
            eks = KeyStroke.getKeyStroke(new Character(' '), 1);
        } else if (eks.equals(KeyStroke.getKeyStroke(9, 0)) || eks.equals(KeyStroke.getKeyStroke(10, 0))) {
            // empty if block
        }
        return eks;
    }

    public void resultChanged(LookupEvent ev) {
        this.rebuildCodeTemplates();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static MimePath getFullMimePath(Document document, int offset) {
        String langPath;
        langPath = null;
        if (document instanceof AbstractDocument) {
            AbstractDocument adoc = (AbstractDocument)document;
            adoc.readLock();
            try {
                List list = TokenHierarchy.get((Document)document).embeddedTokenSequences(offset, true);
                if (list.size() > 1) {
                    langPath = ((TokenSequence)list.get(list.size() - 1)).languagePath().mimePath();
                }
            }
            finally {
                adoc.readUnlock();
            }
        }
        if (langPath == null) {
            langPath = NbEditorUtilities.getMimeType((Document)document);
        }
        if (langPath != null) {
            return MimePath.parse((String)langPath);
        }
        return null;
    }
}

