/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.textsync;

import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManager;
import org.netbeans.lib.editor.codetemplates.textsync.TextSync;
import org.netbeans.lib.editor.codetemplates.textsync.TextSyncGroup;

public final class TextRegionManagerEvent
extends EventObject {
    private final boolean focusChange;
    private final List<TextSyncGroup<?>> removedGroups;
    private final TextSync previousTextSync;

    TextRegionManagerEvent(TextRegionManager source, boolean focusChange, List<TextSyncGroup<?>> removedGroups, TextSync previousTextSync) {
        super(source);
        this.focusChange = focusChange;
        this.removedGroups = removedGroups != null ? removedGroups : Collections.emptyList();
        this.previousTextSync = previousTextSync;
    }

    public TextRegionManager textRegionManager() {
        return (TextRegionManager)this.getSource();
    }

    public boolean isFocusChange() {
        return this.focusChange;
    }

    public <T> List<TextSyncGroup<T>> removedGroups() {
        List<TextSyncGroup<T>> ret = this.removedGroups;
        return ret;
    }

    public TextSync previousTextSync() {
        return this.previousTextSync;
    }
}

