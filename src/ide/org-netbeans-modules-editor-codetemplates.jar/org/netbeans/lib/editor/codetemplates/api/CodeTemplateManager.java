/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.api;

import java.util.Collection;
import java.util.List;
import javax.swing.event.ChangeListener;
import javax.swing.text.Document;
import org.netbeans.lib.editor.codetemplates.CodeTemplateApiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;

public final class CodeTemplateManager {
    private CodeTemplateManagerOperation operation;

    public static CodeTemplateManager get(Document doc) {
        return CodeTemplateManagerOperation.getManager(doc);
    }

    private CodeTemplateManager(CodeTemplateManagerOperation operation) {
        this.operation = operation;
    }

    public Collection<? extends CodeTemplate> getCodeTemplates() {
        this.waitLoaded();
        return this.operation.getCodeTemplates();
    }

    public CodeTemplate createTemporary(String parametrizedText) {
        return new CodeTemplate(this.operation, "", "", parametrizedText, null, this.operation.getMimePath());
    }

    public boolean isLoaded() {
        return this.operation.isLoaded();
    }

    public void waitLoaded() {
        this.operation.waitLoaded();
    }

    public void registerLoadedListener(ChangeListener listener) {
        this.operation.registerLoadedListener(listener);
    }

    CodeTemplateManagerOperation getOperation() {
        return this.operation;
    }

    static {
        CodeTemplateApiPackageAccessor.register(new ApiAccessor());
    }

    private static final class ApiAccessor
    extends CodeTemplateApiPackageAccessor {
        private ApiAccessor() {
        }

        @Override
        public CodeTemplateManager createCodeTemplateManager(CodeTemplateManagerOperation operation) {
            return new CodeTemplateManager(operation);
        }

        @Override
        public CodeTemplateManagerOperation getOperation(CodeTemplateManager manager) {
            return manager.getOperation();
        }

        @Override
        public CodeTemplateManagerOperation getOperation(CodeTemplate codeTemplate) {
            return codeTemplate.getOperation();
        }

        @Override
        public CodeTemplate createCodeTemplate(CodeTemplateManagerOperation managerOperation, String abbreviation, String description, String parametrizedText, List<String> contexts, String mimePath) {
            return new CodeTemplate(managerOperation, abbreviation, description, parametrizedText, contexts, mimePath);
        }

        @Override
        public String getSingleLineText(CodeTemplate codeTemplate) {
            return codeTemplate.getSingleLineText();
        }

        @Override
        public String getCodeTemplateMimePath(CodeTemplate codeTemplate) {
            return codeTemplate.getMimePath();
        }
    }

}

