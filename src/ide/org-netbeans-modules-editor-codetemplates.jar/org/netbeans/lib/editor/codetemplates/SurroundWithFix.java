/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.editor.GuardedDocument
 *  org.netbeans.editor.MarkBlockChain
 *  org.netbeans.spi.editor.hints.ChangeInfo
 *  org.netbeans.spi.editor.hints.Fix
 *  org.openide.util.NbBundle
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.editor.GuardedDocument;
import org.netbeans.editor.MarkBlockChain;
import org.netbeans.lib.editor.codetemplates.CodeTemplateApiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.spi.editor.hints.ChangeInfo;
import org.netbeans.spi.editor.hints.Fix;
import org.openide.util.NbBundle;

public class SurroundWithFix
implements Fix {
    private static String SURROUND_WITH = NbBundle.getMessage(SurroundWithFix.class, (String)"TXT_SurroundWithHint_Prefix");
    private CodeTemplate template;
    private JTextComponent component;

    public static List<Fix> getFixes(JTextComponent component) {
        CodeTemplateManagerOperation op;
        ArrayList<Fix> fixes = new ArrayList<Fix>();
        if (!(component.getDocument() instanceof GuardedDocument && (((GuardedDocument)component.getDocument()).getGuardedBlockChain().compareBlock(component.getSelectionStart(), component.getSelectionEnd()) & 1) != 0 || (op = CodeTemplateManagerOperation.get(component.getDocument(), component.getSelectionStart())) == null)) {
            op.waitLoaded();
            Collection<? extends CodeTemplateFilter> filters = CodeTemplateManagerOperation.getTemplateFilters(component, component.getSelectionStart());
            for (CodeTemplate template : op.findSelectionTemplates()) {
                if (template.getContexts() != null && !template.getContexts().isEmpty() && !SurroundWithFix.accept(template, filters)) continue;
                fixes.add(new SurroundWithFix(template, component));
            }
        }
        return fixes;
    }

    private SurroundWithFix(CodeTemplate template, JTextComponent component) {
        this.template = template;
        this.component = component;
    }

    public String getText() {
        String description = this.template.getDescription();
        if (description == null) {
            description = CodeTemplateApiPackageAccessor.get().getSingleLineText(this.template);
        }
        return SURROUND_WITH + description;
    }

    public ChangeInfo implement() {
        this.template.insert(this.component);
        return null;
    }

    private static boolean accept(CodeTemplate template, Collection<? extends CodeTemplateFilter> filters) {
        for (CodeTemplateFilter filter : filters) {
            if (filter.accept(template)) continue;
            return false;
        }
        return true;
    }
}

