/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.lexer.LanguagePath
 *  org.netbeans.api.lexer.TokenHierarchy
 *  org.netbeans.api.lexer.TokenSequence
 *  org.netbeans.editor.Abbrev
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.modules.editor.NbEditorUtilities
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionProvider
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionQuery
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionTask
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.lexer.LanguagePath;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.editor.Abbrev;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.codetemplates.CodeTemplateCompletionItem;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionProvider;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;

public final class CodeTemplateCompletionProvider
implements CompletionProvider {
    public CompletionTask createTask(int type, JTextComponent component) {
        return (type & 1) == 0 || CodeTemplateCompletionProvider.isAbbrevDisabled(component) ? null : new AsyncCompletionTask((AsyncCompletionQuery)new Query(), component);
    }

    public int getAutoQueryTypes(JTextComponent component, String typedText) {
        return 0;
    }

    private static boolean isAbbrevDisabled(JTextComponent component) {
        return Abbrev.isAbbrevDisabled((JTextComponent)component);
    }

    private static final class Query
    extends AsyncCompletionQuery
    implements ChangeListener {
        private JTextComponent component;
        private int queryCaretOffset;
        private int queryAnchorOffset;
        private List<CodeTemplateCompletionItem> queryResult;
        private String filterPrefix;

        private Query() {
        }

        protected void prepareQuery(JTextComponent component) {
            this.component = component;
        }

        protected boolean canFilter(JTextComponent component) {
            int caretOffset = component.getSelectionStart();
            Document doc = component.getDocument();
            this.filterPrefix = null;
            if (caretOffset >= this.queryCaretOffset && this.queryAnchorOffset < this.queryCaretOffset) {
                try {
                    this.filterPrefix = doc.getText(this.queryAnchorOffset, caretOffset - this.queryAnchorOffset);
                    if (!this.isJavaIdentifierPart(this.filterPrefix)) {
                        this.filterPrefix = null;
                    }
                }
                catch (BadLocationException e) {
                    // empty catch block
                }
            }
            return this.filterPrefix != null;
        }

        protected void filter(CompletionResultSet resultSet) {
            if (this.filterPrefix != null && this.queryResult != null) {
                resultSet.addAllItems(this.getFilteredData(this.queryResult, this.filterPrefix));
            }
            resultSet.finish();
        }

        private boolean isJavaIdentifierPart(CharSequence text) {
            for (int i = 0; i < text.length(); ++i) {
                if (Character.isJavaIdentifierPart(text.charAt(i))) continue;
                return false;
            }
            return true;
        }

        private Collection<? extends CompletionItem> getFilteredData(Collection<? extends CompletionItem> data, String prefix) {
            ArrayList<CompletionItem> ret = new ArrayList<CompletionItem>();
            for (CompletionItem itm : data) {
                if (!itm.getInsertPrefix().toString().startsWith(prefix)) continue;
                ret.add(itm);
            }
            return ret;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void query(CompletionResultSet resultSet, Document doc, int caretOffset) {
            String identifierBeforeCursor;
            String langPath;
            langPath = null;
            identifierBeforeCursor = null;
            if (doc instanceof AbstractDocument) {
                AbstractDocument adoc = (AbstractDocument)doc;
                adoc.readLock();
                try {
                    try {
                        if (adoc instanceof BaseDocument) {
                            identifierBeforeCursor = Utilities.getIdentifierBefore((BaseDocument)((BaseDocument)adoc), (int)caretOffset);
                        }
                    }
                    catch (BadLocationException e) {
                        // empty catch block
                    }
                    List list = TokenHierarchy.get((Document)doc).embeddedTokenSequences(caretOffset, true);
                    if (list.size() > 1) {
                        langPath = ((TokenSequence)list.get(list.size() - 1)).languagePath().mimePath();
                    }
                }
                finally {
                    adoc.readUnlock();
                }
            }
            if (identifierBeforeCursor == null) {
                identifierBeforeCursor = "";
            }
            if (langPath == null) {
                langPath = NbEditorUtilities.getMimeType((Document)doc);
            }
            this.queryCaretOffset = caretOffset;
            this.queryAnchorOffset = caretOffset - identifierBeforeCursor.length();
            if (langPath != null) {
                String mimeType = DocumentUtilities.getMimeType((JTextComponent)this.component);
                MimePath mimePath = mimeType == null ? MimePath.EMPTY : MimePath.get((String)mimeType);
                Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)mimePath).lookup(Preferences.class);
                boolean ignoreCase = prefs.getBoolean("completion-case-sensitive", false);
                CodeTemplateManagerOperation op = CodeTemplateManagerOperation.get(MimePath.parse((String)langPath));
                op.waitLoaded();
                Collection<? extends CodeTemplate> ctsPT = op.findByParametrizedText(identifierBeforeCursor, ignoreCase);
                Collection<? extends CodeTemplate> ctsAb = op.findByAbbreviationPrefix(identifierBeforeCursor, ignoreCase);
                Collection<? extends CodeTemplateFilter> filters = CodeTemplateManagerOperation.getTemplateFilters(this.component, this.queryAnchorOffset);
                this.queryResult = new ArrayList<CodeTemplateCompletionItem>(ctsPT.size() + ctsAb.size());
                HashSet<String> abbrevs = new HashSet<String>(ctsPT.size() + ctsAb.size());
                for (CodeTemplate ct2 : ctsPT) {
                    if (ct2.getContexts() == null || ct2.getContexts().size() <= 0 || !Query.accept(ct2, filters) || !abbrevs.add(ct2.getAbbreviation())) continue;
                    this.queryResult.add(new CodeTemplateCompletionItem(ct2, false));
                }
                for (CodeTemplate ct : ctsAb) {
                    if (ct.getContexts() == null || ct.getContexts().size() <= 0 || !Query.accept(ct, filters) || !abbrevs.add(ct.getAbbreviation())) continue;
                    this.queryResult.add(new CodeTemplateCompletionItem(ct, true));
                }
                resultSet.addAllItems(this.queryResult);
            }
            resultSet.setAnchorOffset(this.queryAnchorOffset);
            resultSet.finish();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void stateChanged(ChangeEvent evt) {
            Query query = this;
            synchronized (query) {
                this.notify();
            }
        }

        private static boolean accept(CodeTemplate template, Collection filters) {
            for (CodeTemplateFilter filter : filters) {
                if (filter.accept(template)) continue;
                return false;
            }
            return true;
        }
    }

}

