/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.lib.editor.util.swing.MutablePositionRegion
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 *  org.openide.util.Exceptions
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Position;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.lib.editor.util.swing.MutablePositionRegion;
import org.netbeans.lib.editor.util.swing.PositionRegion;
import org.openide.util.Exceptions;

public final class SyncDocumentRegion {
    private final Document doc;
    private final List<? extends MutablePositionRegion> regions;
    private final List<? extends MutablePositionRegion> sortedRegions;
    private final boolean regionsSortPerformed;

    public SyncDocumentRegion(Document doc, List<? extends MutablePositionRegion> regions) {
        this.doc = doc;
        this.regions = regions;
        this.regionsSortPerformed = PositionRegion.isRegionsSorted(regions);
        if (this.regionsSortPerformed) {
            this.sortedRegions = regions;
        } else {
            this.sortedRegions = new ArrayList<MutablePositionRegion>(regions);
            Collections.sort(this.sortedRegions, PositionRegion.getComparator());
        }
    }

    public int getRegionCount() {
        return this.regions.size();
    }

    public MutablePositionRegion getRegion(int regionIndex) {
        return this.regions.get(regionIndex);
    }

    public int getFirstRegionStartOffset() {
        return this.getRegion(0).getStartOffset();
    }

    public int getFirstRegionEndOffset() {
        return this.getRegion(0).getEndOffset();
    }

    public int getFirstRegionLength() {
        return this.getFirstRegionEndOffset() - this.getFirstRegionStartOffset();
    }

    public MutablePositionRegion getSortedRegion(int regionIndex) {
        return this.sortedRegions.get(regionIndex);
    }

    public void sync(int moveStartDownLength) {
        String firstRegionText;
        if (moveStartDownLength != 0) {
            MutablePositionRegion firstRegion = this.getRegion(0);
            try {
                Position newStartPos = this.doc.createPosition(firstRegion.getStartOffset() - moveStartDownLength);
                firstRegion.setStartPosition(newStartPos);
            }
            catch (BadLocationException e) {
                Exceptions.printStackTrace((Throwable)e);
            }
        }
        if ((firstRegionText = this.getFirstRegionText()) != null) {
            int regionCount = this.getRegionCount();
            for (int i = 1; i < regionCount; ++i) {
                MutablePositionRegion region = this.getRegion(i);
                int offset = region.getStartOffset();
                int length = region.getEndOffset() - offset;
                try {
                    CharSequence old = DocumentUtilities.getText((Document)this.doc, (int)offset, (int)length);
                    if (!CharSequenceUtilities.textEquals((CharSequence)firstRegionText, (CharSequence)old)) {
                        int res = -1;
                        int k = 0;
                        while (k < Math.min(old.length(), firstRegionText.length()) && old.charAt(k) == firstRegionText.charAt(k)) {
                            res = k++;
                        }
                        String insert = firstRegionText.substring(res + 1);
                        CharSequence remove = old.subSequence(res + 1, old.length());
                        if (insert.length() > 0) {
                            this.doc.insertString(offset + res + 1, insert, null);
                        }
                        if (remove.length() > 0) {
                            this.doc.remove(offset + res + 1 + insert.length(), remove.length());
                        }
                    }
                    Position newStartPos = this.doc.createPosition(offset);
                    region.setStartPosition(newStartPos);
                    continue;
                }
                catch (BadLocationException e) {
                    Exceptions.printStackTrace((Throwable)e);
                }
            }
        }
    }

    private String getFirstRegionText() {
        return this.getRegionText(0);
    }

    private String getRegionText(int regionIndex) {
        try {
            MutablePositionRegion region = this.getRegion(regionIndex);
            int offset = region.getStartOffset();
            int length = region.getEndOffset() - offset;
            return this.doc.getText(offset, length);
        }
        catch (BadLocationException e) {
            Exceptions.printStackTrace((Throwable)e);
            return null;
        }
    }
}

