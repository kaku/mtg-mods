/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.GapList
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 */
package org.netbeans.lib.editor.codetemplates.textsync;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.text.Position;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManager;
import org.netbeans.lib.editor.codetemplates.textsync.TextSync;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.lib.editor.util.swing.PositionRegion;

public final class TextRegion<I> {
    private static final Position FIXED_ZERO_POSITION = PositionRegion.createFixedPosition((int)0);
    private Position startPos;
    private Position endPos;
    private TextSync textSync;
    private TextRegion<?> parent;
    private List<TextRegion<?>> regions;
    private I clientInfo;

    public static Position createFixedPosition(int offset) {
        return PositionRegion.createFixedPosition((int)offset);
    }

    public TextRegion() {
        this(FIXED_ZERO_POSITION, FIXED_ZERO_POSITION);
    }

    public TextRegion(int startOffset, int endOffset) {
        this(PositionRegion.createFixedPosition((int)startOffset), PositionRegion.createFixedPosition((int)endOffset));
    }

    public TextRegion(Position startPos, Position endPos) {
        if (startPos == null) {
            throw new IllegalArgumentException("startPos cannot be null");
        }
        if (endPos == null) {
            throw new IllegalArgumentException("endPos cannot be null");
        }
        this.startPos = startPos;
        this.endPos = endPos;
    }

    public int startOffset() {
        return this.startPos.getOffset();
    }

    public int endOffset() {
        return this.endPos.getOffset();
    }

    public void updateBounds(Position startPos, Position endPos) {
        if (this.textRegionManager() != null) {
            throw new IllegalStateException("Change of bounds of region connected to textRegionManager prohibited.");
        }
        if (startPos != null) {
            this.setStartPos(startPos);
        }
        if (endPos != null) {
            this.setEndPos(endPos);
        }
    }

    public I clientInfo() {
        return this.clientInfo;
    }

    public void setClientInfo(I clientInfo) {
        this.clientInfo = clientInfo;
    }

    public TextSync textSync() {
        return this.textSync;
    }

    void setTextSync(TextSync textSync) {
        this.textSync = textSync;
    }

    TextRegion<?> parent() {
        return this.parent;
    }

    void setParent(TextRegion<?> parent) {
        this.parent = parent;
    }

    List<TextRegion<?>> regions() {
        return this.regions;
    }

    List<TextRegion<?>> validRegions() {
        if (this.regions == null) {
            this.regions = new GapList(2);
        }
        return this.regions;
    }

    void initRegions(TextRegion<?>[] consumedRegions) {
        assert (this.regions == null || this.regions.size() == 0);
        this.regions = new GapList(Arrays.asList(consumedRegions));
    }

    void clearRegions() {
        this.regions = null;
    }

    void setStartPos(Position startPos) {
        this.startPos = startPos;
    }

    void setEndPos(Position endPos) {
        this.endPos = endPos;
    }

    TextRegionManager textRegionManager() {
        return this.textSync != null ? this.textSync.textRegionManager() : null;
    }

    public String toString() {
        return "<" + this.startOffset() + "," + this.endOffset() + ") IHC=" + System.identityHashCode(this) + ", parent=" + (this.parent != null ? Integer.valueOf(System.identityHashCode(this.parent)) : "null") + (this.clientInfo != null ? new StringBuilder().append(" clientInfo:").append(this.clientInfo).toString() : "");
    }
}

