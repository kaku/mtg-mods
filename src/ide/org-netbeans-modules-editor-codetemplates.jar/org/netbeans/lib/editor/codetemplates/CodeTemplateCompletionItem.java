/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.completion.Completion
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.Utilities
 *  org.netbeans.spi.editor.completion.CompletionDocumentation
 *  org.netbeans.spi.editor.completion.CompletionItem
 *  org.netbeans.spi.editor.completion.CompletionResultSet
 *  org.netbeans.spi.editor.completion.CompletionTask
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionQuery
 *  org.netbeans.spi.editor.completion.support.AsyncCompletionTask
 *  org.netbeans.spi.editor.completion.support.CompletionUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package org.netbeans.lib.editor.codetemplates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.net.URL;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.Utilities;
import org.netbeans.lib.editor.codetemplates.CodeTemplateApiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.CodeTemplateManagerOperation;
import org.netbeans.lib.editor.codetemplates.ParametrizedTextParser;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.spi.editor.completion.CompletionDocumentation;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class CodeTemplateCompletionItem
implements CompletionItem {
    private static ImageIcon icon;
    private final CodeTemplate codeTemplate;
    private final boolean abbrevBased;
    private String rightText;

    public CodeTemplateCompletionItem(CodeTemplate codeTemplate, boolean abbrevBased) {
        this.codeTemplate = codeTemplate;
        this.abbrevBased = abbrevBased;
    }

    private String getLeftText() {
        String description = this.codeTemplate.getDescription();
        if (description == null) {
            description = CodeTemplateApiPackageAccessor.get().getSingleLineText(this.codeTemplate);
        }
        return description.trim();
    }

    private String getRightText() {
        if (this.rightText == null) {
            this.rightText = ParametrizedTextParser.toHtmlText(this.codeTemplate.getAbbreviation());
        }
        return this.rightText;
    }

    public int getPreferredWidth(Graphics g, Font defaultFont) {
        return CompletionUtilities.getPreferredWidth((String)this.getLeftText(), (String)this.getRightText(), (Graphics)g, (Font)defaultFont);
    }

    public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
        if (icon == null) {
            icon = ImageUtilities.loadImageIcon((String)"org/netbeans/lib/editor/codetemplates/resources/code_template.png", (boolean)false);
        }
        CompletionUtilities.renderHtml((ImageIcon)icon, (String)this.getLeftText(), (String)this.getRightText(), (Graphics)g, (Font)defaultFont, (Color)defaultColor, (int)width, (int)height, (boolean)selected);
    }

    public void defaultAction(JTextComponent component) {
        Completion.get().hideAll();
        Document doc = component.getDocument();
        int caretOffset = component.getSelectionStart();
        int prefixLength = 0;
        try {
            String ident = Utilities.getIdentifierBefore((BaseDocument)((BaseDocument)doc), (int)caretOffset);
            if (ident != null) {
                prefixLength = ident.length();
            }
        }
        catch (BadLocationException ex) {
            Exceptions.printStackTrace((Throwable)ex);
        }
        if (prefixLength > 0) {
            try {
                doc.remove(caretOffset - prefixLength, prefixLength);
            }
            catch (BadLocationException ble) {
                // empty catch block
            }
        }
        this.codeTemplate.insert(component);
    }

    public void processKeyEvent(KeyEvent evt) {
    }

    public boolean instantSubstitution(JTextComponent component) {
        return false;
    }

    public CompletionTask createDocumentationTask() {
        return new AsyncCompletionTask((AsyncCompletionQuery)new DocQuery(this.codeTemplate));
    }

    public CompletionTask createToolTipTask() {
        return null;
    }

    public int getSortPriority() {
        return 650;
    }

    public CharSequence getSortText() {
        return "";
    }

    public CharSequence getInsertPrefix() {
        if (this.abbrevBased) {
            return this.codeTemplate.getAbbreviation();
        }
        String insertPrefix = this.codeTemplate.getParametrizedText();
        int dollarIndex = insertPrefix.indexOf("${");
        if (dollarIndex >= 0) {
            insertPrefix = insertPrefix.substring(0, dollarIndex);
        }
        return insertPrefix;
    }

    private static final class DocItem
    implements CompletionDocumentation {
        private String text;

        DocItem(String text) {
            this.text = text;
        }

        public String getText() {
            return this.text;
        }

        public CompletionDocumentation resolveLink(String link) {
            return null;
        }

        public URL getURL() {
            return null;
        }

        public Action getGotoSourceAction() {
            return null;
        }
    }

    private static final class DocQuery
    extends AsyncCompletionQuery {
        private CodeTemplate codeTemplate;

        DocQuery(CodeTemplate codeTemplate) {
            this.codeTemplate = codeTemplate;
        }

        protected void query(CompletionResultSet resultSet, Document doc, int caretOffset) {
            StringBuffer sb = new StringBuffer();
            sb.append("<html><pre>");
            ParametrizedTextParser.parseToHtml(sb, this.codeTemplate.getParametrizedText());
            sb.append("</pre>");
            String desc = this.codeTemplate.getDescription();
            if (desc != null && desc.length() > 0) {
                sb.append("<p>").append(desc).append("</p>");
            }
            CodeTemplateManagerOperation operation = CodeTemplateApiPackageAccessor.get().getOperation(this.codeTemplate);
            sb.append("<p>");
            sb.append(NbBundle.getMessage(CodeTemplateCompletionItem.class, (String)"DOC_ITEM_Abbreviation", (Object)ParametrizedTextParser.toHtmlText(this.codeTemplate.getAbbreviation()), (Object)operation.getExpandKeyStrokeText()));
            sb.append("<p>");
            resultSet.setDocumentation((CompletionDocumentation)new DocItem(sb.toString()));
            resultSet.finish();
        }
    }

}

