/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package org.netbeans.lib.editor.codetemplates.storage.ui;

import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import org.netbeans.lib.editor.codetemplates.storage.ui.CodeTemplatesPanel;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class CodeTemplatesPanelController
extends OptionsPanelController {
    private CodeTemplatesPanel codeTemplatesPanel;

    public void update() {
        this.getCodeTemplatesPanel().update();
    }

    public void applyChanges() {
        this.getCodeTemplatesPanel().applyChanges();
    }

    public void cancel() {
        this.getCodeTemplatesPanel().cancel();
    }

    public boolean isValid() {
        return this.getCodeTemplatesPanel().dataValid();
    }

    public boolean isChanged() {
        return this.getCodeTemplatesPanel().isChanged();
    }

    public HelpCtx getHelpCtx() {
        return new HelpCtx("netbeans.optionsDialog.editor.codeTemplates");
    }

    public JComponent getComponent(Lookup masterLookup) {
        return this.getCodeTemplatesPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.getCodeTemplatesPanel().addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.getCodeTemplatesPanel().removePropertyChangeListener(l);
    }

    private CodeTemplatesPanel getCodeTemplatesPanel() {
        if (this.codeTemplatesPanel == null) {
            this.codeTemplatesPanel = new CodeTemplatesPanel();
        }
        return this.codeTemplatesPanel;
    }
}

