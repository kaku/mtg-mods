/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.CodeTemplateDescription
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettings
 *  org.openide.util.NbBundle
 */
package org.netbeans.lib.editor.codetemplates.storage.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.table.DefaultTableModel;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.CodeTemplateDescription;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateFilter;
import org.netbeans.lib.editor.codetemplates.storage.CodeTemplateSettingsImpl;
import org.netbeans.modules.editor.settings.storage.api.EditorSettings;
import org.openide.util.NbBundle;

final class CodeTemplatesModel {
    private static final Logger LOG = Logger.getLogger(CodeTemplatesModel.class.getName());
    private final List<String> languages = new ArrayList<String>();
    private final Map<String, String> languageToMimeType = new HashMap<String, String>();
    private final Map<String, TM> languageToModel = new HashMap<String, TM>();
    private final Map<TM, String> modelToLanguage = new HashMap<TM, String>();
    private KeyStroke expander;
    private CodeTemplateSettingsImpl.OnExpandAction onExpandAction;

    CodeTemplatesModel() {
        Vector<String> columns = new Vector<String>();
        columns.add(CodeTemplatesModel.loc("Abbreviation_Title"));
        columns.add(CodeTemplatesModel.loc("Expanded_Text_Title"));
        columns.add(CodeTemplatesModel.loc("Description_Title"));
        Set mimeTypes = EditorSettings.getDefault().getAllMimeTypes();
        for (String mimeType : mimeTypes) {
            String language;
            MimePath mimePath = MimePath.parse((String)mimeType);
            Map<String, CodeTemplateDescription> abbreviationsMap = CodeTemplateSettingsImpl.get(mimePath).getCodeTemplates();
            if (abbreviationsMap.isEmpty() && this.isCompoundMimeType(mimeType) || (language = EditorSettings.getDefault().getLanguageName(mimeType)).equals(mimeType)) continue;
            this.languages.add(language);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("CodeTemplatesModel: Added language \"" + language + "\" for mimeType \"" + mimeType + "\"\n");
            }
            this.languageToMimeType.put(language, mimeType);
            ArrayList<Vector<String>> table = new ArrayList<Vector<String>>();
            for (String abbreviation : abbreviationsMap.keySet()) {
                CodeTemplateDescription ctd = abbreviationsMap.get(abbreviation);
                Vector<String> line = new Vector<String>(3);
                line.add(abbreviation);
                if (LOG.isLoggable(Level.FINER)) {
                    LOG.finer("CodeTemplatesModel:     Added abbrev \"" + abbreviation + "\"\n");
                }
                line.add(ctd.getParametrizedText());
                line.add(ctd.getDescription());
                table.add(line);
            }
            Collections.sort(table, new MComparator());
            ArrayList<String> supportedContexts = new ArrayList<String>();
            for (CodeTemplateFilter.ContextBasedFactory factory : MimeLookup.getLookup((String)mimeType).lookupAll(CodeTemplateFilter.ContextBasedFactory.class)) {
                supportedContexts.addAll(factory.getSupportedContexts());
            }
            Collections.sort(supportedContexts);
            TM tableModel = new TM(mimeType, abbreviationsMap, columns, table, supportedContexts);
            this.modelToLanguage.put(tableModel, language);
            this.languageToModel.put(language, tableModel);
        }
        Collections.sort(this.languages);
        this.expander = CodeTemplateSettingsImpl.get(MimePath.EMPTY).getExpandKey();
        this.onExpandAction = CodeTemplateSettingsImpl.get(MimePath.EMPTY).getOnExpandAction();
    }

    private boolean isCompoundMimeType(String mimeType) {
        int idx = mimeType.lastIndexOf(43);
        return idx != -1 && idx < mimeType.length() - 1;
    }

    List<String> getLanguages() {
        return Collections.unmodifiableList(this.languages);
    }

    String findLanguage(String mimeType) {
        for (String lang : this.languageToMimeType.keySet()) {
            String mt = this.languageToMimeType.get(lang);
            if (!mt.equals(mimeType)) continue;
            return lang;
        }
        return null;
    }

    String getMimeType(String language) {
        return this.languageToMimeType.get(language);
    }

    TM getTableModel(String language) {
        return this.languageToModel.get(language);
    }

    void saveChanges() {
        for (String language : this.languageToModel.keySet()) {
            TM tableModel = this.languageToModel.get(language);
            if (!tableModel.isModified()) continue;
            String mimeType = this.languageToMimeType.get(language);
            HashMap<String, CodeTemplateDescription> newMap = new HashMap<String, CodeTemplateDescription>();
            for (int idx = 0; idx < tableModel.getRowCount(); ++idx) {
                String abbreviation = tableModel.getAbbreviation(idx);
                CodeTemplateDescription ctd = new CodeTemplateDescription(abbreviation, tableModel.getDescription(idx), tableModel.getText(idx), new ArrayList<String>(tableModel.getContexts(idx)), tableModel.getUniqueId(idx), mimeType);
                newMap.put(abbreviation, ctd);
            }
            MimePath mimePath = MimePath.parse((String)mimeType);
            CodeTemplateSettingsImpl.get(mimePath).setCodeTemplates(newMap);
        }
        if (this.expander != null) {
            CodeTemplateSettingsImpl.get(MimePath.EMPTY).setExpandKey(this.expander);
        }
        if (this.onExpandAction != null) {
            CodeTemplateSettingsImpl.get(MimePath.EMPTY).setOnExpandAction(this.onExpandAction);
        }
    }

    boolean isChanged() {
        if (!CodeTemplateSettingsImpl.get(MimePath.EMPTY).getExpandKey().equals(this.expander)) {
            return true;
        }
        if (CodeTemplateSettingsImpl.get(MimePath.EMPTY).getOnExpandAction() != this.onExpandAction) {
            return true;
        }
        for (String l : this.languageToModel.keySet()) {
            TM tableModel = this.languageToModel.get(l);
            if (!tableModel.isModified()) continue;
            return true;
        }
        return false;
    }

    private static String loc(String key) {
        return NbBundle.getMessage(CodeTemplatesModel.class, (String)key);
    }

    KeyStroke getExpander() {
        return this.expander;
    }

    void setExpander(KeyStroke expander) {
        this.expander = expander;
    }

    CodeTemplateSettingsImpl.OnExpandAction getOnExpandAction() {
        return this.onExpandAction;
    }

    void setOnExpandAction(CodeTemplateSettingsImpl.OnExpandAction action) {
        this.onExpandAction = action;
    }

    static class TM
    extends DefaultTableModel {
        private final String mimeType;
        private final Map<String, CodeTemplateDescription> codeTemplatesMap;
        private final Map<String, Set<String>> contexts;
        private final DefaultListModel<String> supportedContexts;
        private boolean modified = false;

        public TM(String mimeType, Map<String, CodeTemplateDescription> codeTemplatesMap, Vector<String> headers, List<Vector<String>> data, List<String> supportedContexts) {
            super(new Vector<Vector<String>>(data), headers);
            this.mimeType = mimeType;
            this.codeTemplatesMap = codeTemplatesMap;
            this.contexts = new HashMap<String, Set<String>>(codeTemplatesMap.size());
            this.supportedContexts = new DefaultListModel();
            for (String context : supportedContexts) {
                this.supportedContexts.addElement(context);
            }
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }

        public String getAbbreviation(int row) {
            return (String)this.getValueAt(row, 0);
        }

        public String getDescription(int row) {
            return (String)this.getValueAt(row, 2);
        }

        public void setDescription(int row, String description) {
            if (TM.compareTexts(description, this.getDescription(row))) {
                return;
            }
            this.setValueAt(description.isEmpty() ? null : description, row, 2);
            this.fireChanged();
        }

        public String getText(int row) {
            return (String)this.getValueAt(row, 1);
        }

        public void setText(int row, String text) {
            if (TM.compareTexts(text, this.getText(row))) {
                return;
            }
            this.setValueAt(text, row, 1);
            this.fireChanged();
        }

        public Set<String> getContexts(int row) {
            String abbreviation = this.getAbbreviation(row);
            LinkedHashSet ret = this.contexts.get(abbreviation);
            if (ret == null) {
                CodeTemplateDescription ctd = this.codeTemplatesMap.get(abbreviation);
                final boolean[] afterInit = new boolean[]{false};
                ret = new LinkedHashSet(){

                    @Override
                    public boolean add(Object e) {
                        boolean b = super.add(e);
                        if (b && afterInit[0]) {
                            TM.this.fireChanged();
                        }
                        return b;
                    }

                    @Override
                    public boolean remove(Object o) {
                        boolean b = super.remove(o);
                        if (b && afterInit[0]) {
                            TM.this.fireChanged();
                        }
                        return b;
                    }
                };
                if (ctd != null) {
                    ret.addAll(ctd.getContexts());
                }
                afterInit[0] = true;
                this.contexts.put(abbreviation, ()ret);
            }
            return ret;
        }

        public ListModel<String> getSupportedContexts() {
            return this.supportedContexts;
        }

        public String getUniqueId(int row) {
            CodeTemplateDescription ctd = this.codeTemplatesMap.get(this.getAbbreviation(row));
            return ctd == null ? null : ctd.getUniqueId();
        }

        public int addCodeTemplate(String abbreviation) {
            this.addRow(new Object[]{abbreviation, "", null});
            this.fireChanged();
            return this.getRowCount() - 1;
        }

        public void removeCodeTemplate(int row) {
            this.removeRow(row);
            this.fireChanged();
        }

        public boolean isModified() {
            return this.modified;
        }

        private void fireChanged() {
            HashMap<String, CodeTemplateDescription> current = new HashMap<String, CodeTemplateDescription>();
            for (int idx = 0; idx < this.getRowCount(); ++idx) {
                String abbreviation = this.getAbbreviation(idx);
                CodeTemplateDescription ctd = new CodeTemplateDescription(abbreviation, this.getDescription(idx), this.getText(idx), new ArrayList<String>(this.getContexts(idx)), this.getUniqueId(idx), this.mimeType);
                current.put(abbreviation, ctd);
            }
            MimePath mimePath = MimePath.parse((String)this.mimeType);
            Map<String, CodeTemplateDescription> saved = CodeTemplateSettingsImpl.get(mimePath).getCodeTemplates();
            this.modified = !current.equals(saved);
        }

        private static boolean compareTexts(String t1, String t2) {
            if (t1 == null || t1.length() == 0) {
                t1 = null;
            }
            if (t2 == null || t2.length() == 0) {
                t2 = null;
            }
            if (t1 != null && t2 != null) {
                return t1.equals(t2);
            }
            return t1 == null && t2 == null;
        }

    }

    private static class MComparator
    implements Comparator<Vector<String>> {
        private MComparator() {
        }

        @Override
        public int compare(Vector<String> o1, Vector<String> o2) {
            String s1 = o1.get(0);
            String s2 = o2.get(0);
            return s1.compareTo(s2);
        }
    }

}

