/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.CodeTemplateDescription
 *  org.netbeans.api.editor.settings.CodeTemplateSettings
 *  org.netbeans.spi.editor.mimelookup.MimeDataProvider
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package org.netbeans.lib.editor.codetemplates.storage;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.KeyStroke;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.CodeTemplateDescription;
import org.netbeans.api.editor.settings.CodeTemplateSettings;
import org.netbeans.lib.editor.codetemplates.storage.CodeTemplateSettingsImpl;
import org.netbeans.spi.editor.mimelookup.MimeDataProvider;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class SettingsProvider
implements MimeDataProvider {
    private static final Logger LOG = Logger.getLogger(SettingsProvider.class.getName());

    public Lookup getLookup(MimePath mimePath) {
        return new MyLookup(mimePath);
    }

    private static final class CompositeCTS
    extends CodeTemplateSettings {
        private final CodeTemplateSettingsImpl[] allCtsi;
        private List<CodeTemplateDescription> codeTemplates;
        private KeyStroke expansionKey;

        public CompositeCTS(CodeTemplateSettingsImpl[] allCtsi) {
            this.allCtsi = allCtsi;
        }

        public List<CodeTemplateDescription> getCodeTemplateDescriptions() {
            if (this.codeTemplates == null) {
                Map map;
                if (this.allCtsi.length > 1) {
                    map = new HashMap();
                    for (int i = this.allCtsi.length - 1; i >= 0; --i) {
                        map.putAll(this.allCtsi[i].getCodeTemplates());
                    }
                } else {
                    map = this.allCtsi[0].getCodeTemplates();
                }
                this.codeTemplates = Collections.unmodifiableList(new ArrayList(map.values()));
            }
            return this.codeTemplates;
        }

        public KeyStroke getExpandKey() {
            if (this.expansionKey == null) {
                this.expansionKey = this.allCtsi[this.allCtsi.length - 1].getExpandKey();
            }
            return this.expansionKey;
        }
    }

    private static final class MyLookup
    extends AbstractLookup
    implements PropertyChangeListener {
        private final MimePath mimePath;
        private final InstanceContent ic;
        private Object codeTemplateSettings = null;
        private CodeTemplateSettingsImpl[] allCtsi;

        public MyLookup(MimePath mimePath) {
            this(mimePath, new InstanceContent());
        }

        private MyLookup(MimePath mimePath, InstanceContent ic) {
            super((AbstractLookup.Content)ic);
            this.mimePath = mimePath;
            this.ic = ic;
            List allPaths = mimePath.getIncludedPaths();
            this.allCtsi = new CodeTemplateSettingsImpl[allPaths.size()];
            for (int i = 0; i < allPaths.size(); ++i) {
                this.allCtsi[i] = CodeTemplateSettingsImpl.get((MimePath)allPaths.get(i));
                this.allCtsi[i].addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this.allCtsi[i]));
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        protected void initialize() {
            MyLookup myLookup = this;
            synchronized (myLookup) {
                this.codeTemplateSettings = new CompositeCTS(this.allCtsi);
                this.ic.set(Arrays.asList(this.codeTemplateSettings), null);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            MyLookup myLookup = this;
            synchronized (myLookup) {
                if (this.codeTemplateSettings != null) {
                    this.codeTemplateSettings = new CompositeCTS(this.allCtsi);
                    this.ic.set(Arrays.asList(this.codeTemplateSettings), null);
                }
            }
        }
    }

}

