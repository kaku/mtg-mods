/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.AttributesUtilities
 *  org.netbeans.api.editor.settings.EditorStyleConstants
 *  org.netbeans.api.editor.settings.FontColorSettings
 *  org.netbeans.editor.BaseDocument
 *  org.netbeans.editor.BaseTextUI
 *  org.netbeans.editor.EditorUI
 *  org.netbeans.editor.Utilities
 *  org.netbeans.editor.WordMatch
 *  org.netbeans.lib.editor.util.ArrayUtilities
 *  org.netbeans.lib.editor.util.CharSequenceUtilities
 *  org.netbeans.lib.editor.util.GapList
 *  org.netbeans.lib.editor.util.swing.BlockCompare
 *  org.netbeans.lib.editor.util.swing.DocumentUtilities
 *  org.netbeans.spi.editor.highlighting.HighlightsContainer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayer
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory
 *  org.netbeans.spi.editor.highlighting.HighlightsLayerFactory$Context
 *  org.netbeans.spi.editor.highlighting.ZOrder
 *  org.netbeans.spi.editor.highlighting.support.OffsetsBag
 */
package org.netbeans.lib.editor.codetemplates.textsync;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.TextUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.TextAction;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.AttributesUtilities;
import org.netbeans.api.editor.settings.EditorStyleConstants;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.EditorUI;
import org.netbeans.editor.Utilities;
import org.netbeans.editor.WordMatch;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegion;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManagerEvent;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManagerListener;
import org.netbeans.lib.editor.codetemplates.textsync.TextSync;
import org.netbeans.lib.editor.codetemplates.textsync.TextSyncGroup;
import org.netbeans.lib.editor.util.ArrayUtilities;
import org.netbeans.lib.editor.util.CharSequenceUtilities;
import org.netbeans.lib.editor.util.GapList;
import org.netbeans.lib.editor.util.swing.BlockCompare;
import org.netbeans.lib.editor.util.swing.DocumentUtilities;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsLayer;
import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
import org.netbeans.spi.editor.highlighting.ZOrder;
import org.netbeans.spi.editor.highlighting.support.OffsetsBag;

public final class TextRegionManager {
    static final Logger LOG = Logger.getLogger(TextRegionManager.class.getName());
    private static final int INVALID_TEXT_SYNC = -1;
    private static final int SAME_TEXT_SYNC = -2;
    private WeakReference<JTextComponent> componentRef;
    private Document doc;
    private TextRegion<?> rootRegion;
    private EventListenerList listenerList = new EventListenerList();
    private GapList<TextSyncGroup<?>> editGroups;
    private TextSync activeTextSync;
    private int masterRegionStartOffset;
    private int masterRegionEndOffset;
    private int ignoreDocModifications;
    private boolean forceSyncByMaster;
    private final Highlighting highlighting;
    private boolean overridingKeys;
    private ActionMap origActionMap;
    private ActionMap overrideActionMap;

    public static synchronized TextRegionManager reserve(JTextComponent component) {
        JTextComponent activeComponent;
        if (component == null) {
            throw new IllegalArgumentException("component cannot be null");
        }
        Document doc = component.getDocument();
        TextRegionManager manager = TextRegionManager.get(doc, true);
        if (manager == null) {
            manager = TextRegionManager.get(doc, true);
        }
        if ((activeComponent = manager.component()) == null) {
            manager.setComponent(component);
        } else if (activeComponent != component) {
            if (manager.isActive()) {
                manager = null;
            } else {
                manager.setComponent(component);
            }
        }
        return manager;
    }

    public static TextRegionManager get(Document doc, boolean forceCreation) {
        TextRegionManager manager = (TextRegionManager)doc.getProperty(TextRegionManager.class);
        if (manager == null && forceCreation) {
            manager = new TextRegionManager(doc);
            doc.putProperty(TextRegionManager.class, manager);
        }
        return manager;
    }

    TextRegionManager(Document doc) {
        this.highlighting = new Highlighting(this);
        this.doc = doc;
        this.rootRegion = new TextRegion();
        this.editGroups = new GapList(2);
    }

    public void addGroup(TextSyncGroup<?> group, int offsetShift) throws BadLocationException {
        if (group == null) {
            throw new IllegalArgumentException("textSyncGroup cannot be null");
        }
        if (group.textRegionManager() != null) {
            throw new IllegalArgumentException("textSyncGroup=" + group + " already assigned to textRegionManager=" + group.textRegionManager());
        }
        this.activate();
        this.editGroups.add(group);
        this.addGroupUpdate(group, offsetShift);
    }

    public void activateGroup(TextSyncGroup<?> group) {
        this.activateTextSync(null, group, this.findEditableTextSyncIndex(group, 0, 1, true, false), true);
    }

    public void stopGroupEditing(TextSyncGroup group) {
        int groupIndex = this.editGroups.indexOf((Object)group);
        if (groupIndex >= 0) {
            this.releaseLastGroups(this.editGroups.size() - groupIndex);
        }
    }

    public void stopSyncEditing() {
        this.releaseLastGroups(this.editGroups.size());
    }

    public TextSync activeTextSync() {
        return this.activeTextSync;
    }

    TextSyncGroup<?> activeGroup() {
        return this.activeTextSync != null ? this.activeTextSync.group() : null;
    }

    TextSyncGroup<?> lastGroup() {
        return this.editGroups.size() > 0 ? (TextSyncGroup)this.editGroups.get(this.editGroups.size() - 1) : null;
    }

    public void addTextRegionManagerListener(TextRegionManagerListener l) {
        this.listenerList.add(TextRegionManagerListener.class, l);
    }

    public void removeTextRegionManagerListener(TextRegionManagerListener l) {
        this.listenerList.remove(TextRegionManagerListener.class, l);
    }

    private TextRegionManagerEvent createEvent(boolean focusChange, List<TextSyncGroup<?>> removedGroups, TextSync previousTextSync) {
        return new TextRegionManagerEvent(this, focusChange, removedGroups, previousTextSync);
    }

    private void fireEvent(TextRegionManagerEvent evt) {
        Object[] listeners = this.listenerList.getListenerList();
        for (int i = 1; i < listeners.length; i += 2) {
            ((TextRegionManagerListener)listeners[i]).stateChanged(evt);
        }
    }

    public JTextComponent component() {
        return this.componentRef != null ? this.componentRef.get() : null;
    }

    private void setComponent(JTextComponent component) {
        this.componentRef = component != null ? new WeakReference<JTextComponent>(component) : null;
    }

    public Document document() {
        return this.doc;
    }

    void markIgnoreDocModifications() {
        ++this.ignoreDocModifications;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addGroupUpdate(TextSyncGroup<?> textSyncGroup, int offsetShift) throws BadLocationException {
        if (textSyncGroup.textRegionManager() != null) {
            throw new IllegalArgumentException("TextSyncGroup=" + textSyncGroup + " already assigned to " + textSyncGroup.textRegionManager());
        }
        TextRegion lastAdded = null;
        try {
            for (TextSync textSync : textSyncGroup.textSyncsModifiable()) {
                for (TextRegion textRegion : textSync.regions()) {
                    Position startPos = this.doc.createPosition(textRegion.startOffset() + offsetShift);
                    Position endPos = this.doc.createPosition(textRegion.endOffset() + offsetShift);
                    textRegion.setStartPos(startPos);
                    textRegion.setEndPos(endPos);
                    TextRegionManager.addRegion(this.rootRegion, textRegion);
                    lastAdded = textRegion;
                }
            }
            lastAdded = null;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.fine("ADD textSyncGroup: " + textSyncGroup + '\n');
            }
        }
        finally {
            this.removeAddedSoFar(textSyncGroup, lastAdded);
        }
        textSyncGroup.setTextRegionManager(this);
    }

    private void removeAddedSoFar(TextSyncGroup<?> textSyncGroup, TextRegion<?> lastAdded) {
        while (lastAdded != null) {
            for (TextSync textSync : textSyncGroup.textSyncsModifiable()) {
                for (TextRegion textRegion : textSync.regions()) {
                    TextRegionManager.removeRegionFromParent(textRegion);
                    if (textRegion != lastAdded) continue;
                    return;
                }
            }
        }
    }

    private void removeGroupUpdate(TextSyncGroup<?> textSyncGroup) {
        textSyncGroup.setTextRegionManager(null);
        for (TextSync textSync : textSyncGroup.textSyncsModifiable()) {
            for (TextRegion textRegion : textSync.regions()) {
                TextRegionManager.removeRegionFromParent(textRegion);
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("REMOVE textSyncGroup: " + textSyncGroup + '\n');
        }
    }

    void setActiveTextSync(TextSync textSync) {
        if (textSync.masterRegion() == null) {
            throw new IllegalArgumentException("masterRegion expected to be non-null");
        }
        this.activeTextSync = textSync;
        this.updateMasterRegionBounds();
    }

    int findEditableTextSyncIndex(TextSyncGroup<?> group, int textSyncIndex, int direction, boolean cycle, boolean skipCaretMarkers) {
        int tsCount = group.textSyncsModifiable().size();
        if (tsCount == 0) {
            return -1;
        }
        int startTextSyncIndex = -1;
        do {
            TextSync textSync;
            if (textSyncIndex >= tsCount) {
                if (!cycle) break;
                textSyncIndex = 0;
            } else if (textSyncIndex < 0) {
                if (!cycle) break;
                textSyncIndex = tsCount - 1;
            }
            if (startTextSyncIndex == -1) {
                startTextSyncIndex = textSyncIndex;
            }
            if (!(textSync = group.textSyncs().get(textSyncIndex)).isEditable() && (skipCaretMarkers || !textSync.isCaretMarker())) continue;
            return textSyncIndex;
        } while ((textSyncIndex += direction) != startTextSyncIndex);
        return -1;
    }

    private void activateTextSync(List<TextSyncGroup<?>> removedGroups, TextSyncGroup<?> group, int textSyncIndex, boolean selectText) {
        JTextComponent component;
        TextSync previousTextSync = this.activeTextSync;
        boolean removeGroup = false;
        if (textSyncIndex == -1) {
            removeGroup = true;
        }
        if (group != null) {
            if (textSyncIndex == -2) {
                textSyncIndex = group.activeTextSyncIndex();
            }
            group.setActiveTextSyncIndex(textSyncIndex);
            this.activeTextSync = group.activeTextSync();
            if (this.activeTextSync.isCaretMarker()) {
                int offset = this.activeTextSync.regions().get(0).startOffset();
                JTextComponent component2 = this.component();
                if (component2 != null) {
                    component2.setCaretPosition(offset);
                }
                removeGroup = true;
            }
        } else {
            this.activeTextSync = null;
        }
        while (removeGroup && group != null) {
            removeGroup = false;
            selectText = false;
            int groupIndex = this.editGroups.indexOf(group);
            assert (groupIndex >= 0);
            removedGroups = this.removeLastGroups(removedGroups, this.editGroups.size() - groupIndex);
            if (groupIndex > 0) {
                group = (TextSyncGroup)this.editGroups.get(groupIndex - 1);
                textSyncIndex = group.activeTextSyncIndex();
                this.activeTextSync = group.activeTextSync();
                continue;
            }
            group = null;
            this.activeTextSync = null;
        }
        if (group != null && (component = this.component()) != null) {
            this.setActiveTextSync(this.activeTextSync);
            ((BaseTextUI)component.getUI()).getEditorUI().getWordMatch().clear();
            if (selectText) {
                TextRegion activeRegion = this.activeTextSync.masterRegion();
                component.select(activeRegion.startOffset(), activeRegion.endOffset());
            }
            if (!this.overridingKeys) {
                this.overridingKeys = true;
                component.addKeyListener(OverrideKeysListener.INSTANCE);
                ActionMap[] maps = OverrideAction.installOverrideActionMap(component);
                this.origActionMap = maps[0];
                this.overrideActionMap = maps[1];
            }
        }
        TextRegionManagerEvent evt = this.createEvent(true, removedGroups, previousTextSync);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Firing event - focusing of activeTextSync:\n" + this.activeTextSync + "previousTextSync=" + previousTextSync + ", removedGroups=" + removedGroups + '\n');
        }
        this.fireEvent(evt);
        if (removedGroups != null) {
            this.removeGroupsUpdate();
        }
        this.highlighting.requestRepaint();
    }

    private void releaseLastGroups(int count) {
        if (this.editGroups.size() > 0) {
            List removedGroups = this.removeLastGroups(null, count);
            this.activateTextSync(removedGroups, this.lastGroup(), -2, false);
        }
    }

    private List<TextSyncGroup<?>> removeLastGroups(List<TextSyncGroup<?>> removedGroups, int count) {
        assert (count >= 0 && count <= this.editGroups.size());
        int groupIndex = this.editGroups.size() - count;
        if (removedGroups == null) {
            removedGroups = new GapList(count);
        }
        while (--count >= 0) {
            TextSyncGroup group = (TextSyncGroup)this.editGroups.remove(groupIndex + count);
            this.removeGroupUpdate(group);
            removedGroups.add(0, group);
        }
        return removedGroups;
    }

    private void removeGroupsUpdate() {
        if (this.editGroups.size() == 0) {
            JTextComponent component = this.component();
            if (this.doc instanceof BaseDocument) {
                BaseDocument bdoc = (BaseDocument)this.doc;
                bdoc.removePostModificationDocumentListener((DocumentListener)DocListener.INSTANCE);
                bdoc.removeUpdateDocumentListener((DocumentListener)UpdateDocListener.INSTANCE);
            }
            this.activeTextSync = null;
            this.componentRef = null;
            if (this.overridingKeys) {
                this.overridingKeys = false;
                component.removeKeyListener(OverrideKeysListener.INSTANCE);
                if (this.overrideActionMap != component.getActionMap()) {
                    LOG.warning("The action map got tampered with! component=" + component.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(component)) + "; doc=" + component.getDocument());
                } else {
                    component.setActionMap(this.origActionMap);
                }
                this.overrideActionMap.clear();
                this.origActionMap = null;
                this.overrideActionMap = null;
            }
        }
    }

    void activeTextSyncModified() {
        TextRegionManagerEvent evt = this.createEvent(false, Collections.emptyList(), this.activeTextSync);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Firing event - mod of activeTextSync=" + this.activeTextSync + '\n');
        }
        this.fireEvent(evt);
        this.highlighting.requestRepaint();
    }

    boolean enterAction() {
        TextSync textSync = this.activeTextSync();
        if (textSync != null) {
            TextRegion master = textSync.validMasterRegion();
            JTextComponent component = this.component();
            if (master.startOffset() <= component.getCaretPosition() && component.getCaretPosition() <= master.endOffset()) {
                TextSyncGroup group = textSync.group();
                this.activateTextSync(null, group, this.findEditableTextSyncIndex(group, group.activeTextSyncIndex() + 1, 1, false, false), true);
                return true;
            }
            this.releaseLastGroups(1);
        } else {
            this.releaseLastGroups(this.editGroups.size());
        }
        return false;
    }

    void escapeAction() {
        this.releaseLastGroups(1);
    }

    void tabAction() {
        int index;
        TextSyncGroup group;
        TextSync textSync = this.activeTextSync();
        if (textSync != null && (index = this.findEditableTextSyncIndex(group = textSync.group(), group.activeTextSyncIndex() + 1, 1, true, true)) != -1) {
            this.activateTextSync(null, group, index, true);
        }
    }

    void shiftTabAction() {
        int index;
        TextSyncGroup group;
        TextSync textSync = this.activeTextSync();
        if (textSync != null && (index = this.findEditableTextSyncIndex(group = textSync.group(), group.activeTextSyncIndex() - 1, -1, true, true)) != -1) {
            this.activateTextSync(null, group, index, true);
        }
    }

    boolean isActive() {
        return !this.editGroups.isEmpty();
    }

    List<TextRegion<?>> regions() {
        return this.rootRegion.regions();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void insertUpdate(DocumentEvent evt) {
        if (this.ignoreDocModifications == 0) {
            block38 : {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("TextRegionManager.insertUpdate(): evt:" + DocumentUtilities.appendEvent((StringBuilder)null, (DocumentEvent)evt) + "\n");
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("BEFORE-INSERT-UPDATE REGIONS: " + this);
                    }
                }
                if (this.activeTextSync != null) {
                    ++this.ignoreDocModifications;
                    try {
                        int offset = evt.getOffset();
                        int insertLength = evt.getLength();
                        String insertText = DocumentUtilities.getModificationText((DocumentEvent)evt);
                        if (insertText == null) {
                            try {
                                insertText = this.doc.getText(offset, insertLength);
                            }
                            catch (BadLocationException e) {
                                throw new IllegalStateException(e);
                            }
                        }
                        boolean syncSuccess = false;
                        if (offset > this.masterRegionStartOffset) {
                            if (offset <= this.masterRegionEndOffset) {
                                TextRegion master = this.activeTextSync.validMasterRegion();
                                int relOffset = offset - master.startOffset();
                                if (relOffset <= 0) {
                                    this.stopSyncEditing();
                                    return;
                                }
                                this.beforeDocumentModification();
                                boolean oldTM = DocumentUtilities.isTypingModification((Document)this.doc);
                                DocumentUtilities.setTypingModification((Document)this.doc, (boolean)false);
                                try {
                                    if (this.forceSyncByMaster) {
                                        this.forceSyncByMaster = false;
                                        this.syncByMaster(this.activeTextSync);
                                    } else {
                                        for (TextRegion region : this.activeTextSync.regions()) {
                                            if (region == master) continue;
                                            this.doc.insertString(region.startOffset() + relOffset, insertText, null);
                                        }
                                    }
                                }
                                finally {
                                    DocumentUtilities.setTypingModification((Document)this.doc, (boolean)oldTM);
                                    this.afterDocumentModification();
                                }
                                syncSuccess = true;
                            }
                        } else if (offset == this.masterRegionStartOffset) {
                            TextRegion master = this.activeTextSync.validMasterRegion();
                            this.fixRegionStartOffset(master, offset);
                            this.beforeDocumentModification();
                            boolean oldTM = DocumentUtilities.isTypingModification((Document)this.doc);
                            DocumentUtilities.setTypingModification((Document)this.doc, (boolean)false);
                            try {
                                if (this.forceSyncByMaster) {
                                    this.forceSyncByMaster = false;
                                    this.syncByMaster(this.activeTextSync);
                                } else {
                                    for (TextRegion region : this.activeTextSync.regions()) {
                                        if (region == master) continue;
                                        int startOffset = region.startOffset();
                                        this.doc.insertString(startOffset, insertText, null);
                                        this.fixRegionStartOffset(region, startOffset);
                                    }
                                }
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)this.doc, (boolean)oldTM);
                                this.afterDocumentModification();
                            }
                            syncSuccess = true;
                        }
                        if (syncSuccess) {
                            this.activeTextSyncModified();
                        } else if (DocumentUtilities.isTypingModification((Document)this.doc)) {
                            this.stopSyncEditing();
                        }
                        break block38;
                    }
                    catch (BadLocationException e) {
                        LOG.log(Level.WARNING, "Unexpected exception during synchronization", e);
                    }
                    finally {
                        assert (this.ignoreDocModifications > 0);
                        --this.ignoreDocModifications;
                    }
                }
                if (DocumentUtilities.isTypingModification((Document)this.doc)) {
                    this.stopSyncEditing();
                }
            }
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("AFTER-INSERT-UPDATE REGIONS: " + this);
            }
        }
        this.updateMasterRegionBounds();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void removeUpdate(DocumentEvent evt) {
        if (this.ignoreDocModifications == 0) {
            block19 : {
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.fine("TextRegionManager.removeUpdate(): evt:" + DocumentUtilities.appendEvent((StringBuilder)null, (DocumentEvent)evt) + "\n");
                    if (LOG.isLoggable(Level.FINER)) {
                        LOG.finer("BEFORE-REMOVE-UPDATE REGIONS: " + this);
                    }
                }
                if (this.activeTextSync != null) {
                    ++this.ignoreDocModifications;
                    try {
                        int offset = evt.getOffset();
                        int removeLength = evt.getLength();
                        if (offset >= this.masterRegionStartOffset && offset + removeLength <= this.masterRegionEndOffset) {
                            TextRegion master = this.activeTextSync.validMasterRegion();
                            int relOffset = offset - master.startOffset();
                            this.beforeDocumentModification();
                            boolean oldTM = DocumentUtilities.isTypingModification((Document)this.doc);
                            DocumentUtilities.setTypingModification((Document)this.doc, (boolean)false);
                            try {
                                if (Boolean.TRUE.equals(this.doc.getProperty("doc-replace-selection-property"))) {
                                    this.forceSyncByMaster = true;
                                    break block19;
                                }
                                for (TextRegion region : this.activeTextSync.regions()) {
                                    if (region == master) continue;
                                    this.doc.remove(region.startOffset() + relOffset, removeLength);
                                }
                                this.activeTextSyncModified();
                            }
                            catch (BadLocationException e) {
                                this.stopSyncEditing();
                                LOG.log(Level.WARNING, "Unexpected exception during synchronization", e);
                            }
                            finally {
                                DocumentUtilities.setTypingModification((Document)this.doc, (boolean)oldTM);
                                this.afterDocumentModification();
                            }
                        }
                        if (DocumentUtilities.isTypingModification((Document)this.doc)) {
                            this.stopSyncEditing();
                        }
                        break block19;
                    }
                    finally {
                        assert (this.ignoreDocModifications > 0);
                        --this.ignoreDocModifications;
                    }
                }
                if (DocumentUtilities.isTypingModification((Document)this.doc)) {
                    this.stopSyncEditing();
                }
            }
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("AFTER-REMOVE-UPDATE REGIONS: " + this);
            }
        }
        this.updateMasterRegionBounds();
    }

    void removeUpdateUpdate(DocumentEvent evt) {
        TextRegion region;
        int minGroupIndex = Integer.MAX_VALUE;
        int removeStartOffset = evt.getOffset();
        int removeEndOffset = removeStartOffset + evt.getLength();
        List regions = this.rootRegion.regions();
        int index = TextRegionManager.findRegionInsertIndex(regions, removeStartOffset);
        if (index > 0) {
            region = regions.get(index - 1);
            minGroupIndex = this.findMinGroupIndex(minGroupIndex, region, removeStartOffset, removeEndOffset);
        }
        while (index < regions.size() && (region = regions.get(index)).startOffset() < removeEndOffset) {
            minGroupIndex = this.findMinGroupIndex(minGroupIndex, region, removeStartOffset, removeEndOffset);
            ++index;
        }
        if (minGroupIndex != Integer.MAX_VALUE) {
            int removeCount = this.editGroups.size() - minGroupIndex;
            if (LOG.isLoggable(Level.FINE)) {
                StringBuilder sb = new StringBuilder(100);
                sb.append("removeUpdateUpdate(): Text remove <").append(removeStartOffset);
                sb.append(",").append(removeEndOffset).append(">.\n  Removing GROUPS <");
                sb.append(minGroupIndex).append(",").append(this.editGroups.size()).append(">\n");
                LOG.fine(sb.toString());
            }
            this.releaseLastGroups(removeCount);
        }
    }

    private int findMinGroupIndex(int minGroupIndex, TextRegion<?> region, int removeStartOffset, int removeEndOffset) {
        BlockCompare bc = BlockCompare.get((int)removeStartOffset, (int)removeEndOffset, (int)region.startOffset(), (int)region.endOffset());
        TextSyncGroup group = region.textSync().group();
        boolean scanChildren = false;
        if (!bc.emptyY()) {
            List childRegions;
            if (bc.overlap() || bc.containsStrict()) {
                if (LOG.isLoggable(Level.FINE)) {
                    StringBuilder sb = new StringBuilder(100);
                    sb.append("removeUpdateUpdate(): Text remove <").append(removeStartOffset);
                    sb.append(",").append(removeEndOffset).append(">.\n  Conflicting region ").append(region).append('\n');
                    LOG.fine(sb.toString());
                }
                minGroupIndex = Math.min(minGroupIndex, this.editGroups.indexOf(group));
                scanChildren = true;
            } else {
                scanChildren = bc.inside();
            }
            if (scanChildren && (childRegions = region.regions()) != null) {
                for (TextRegion childRegion : childRegions) {
                    minGroupIndex = this.findMinGroupIndex(minGroupIndex, childRegion, removeStartOffset, removeEndOffset);
                }
            }
        }
        return minGroupIndex;
    }

    private void beforeDocumentModification() {
        this.doc.putProperty("abbrev-ignore-modification", Boolean.TRUE);
    }

    private void afterDocumentModification() {
        this.doc.putProperty("abbrev-ignore-modification", Boolean.FALSE);
    }

    private void activate() {
        if (this.editGroups.size() == 0 && this.doc instanceof BaseDocument) {
            BaseDocument bdoc = (BaseDocument)this.doc;
            bdoc.addPostModificationDocumentListener((DocumentListener)DocListener.INSTANCE);
            bdoc.addUpdateDocumentListener((DocumentListener)UpdateDocListener.INSTANCE);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void syncByMaster(TextSync textSync) {
        this.beforeDocumentModification();
        ++this.ignoreDocModifications;
        boolean oldTM = DocumentUtilities.isTypingModification((Document)this.doc);
        DocumentUtilities.setTypingModification((Document)this.doc, (boolean)false);
        try {
            TextRegion masterRegion = textSync.validMasterRegion();
            CharSequence docText = DocumentUtilities.getText((Document)this.doc);
            CharSequence masterRegionText = docText.subSequence(masterRegion.startOffset(), masterRegion.endOffset());
            String masterRegionString = null;
            for (TextRegion region : textSync.regionsModifiable()) {
                int regionStartOffset;
                CharSequence regionText;
                int regionEndOffset;
                if (region == masterRegion || CharSequenceUtilities.textEquals((CharSequence)masterRegionText, (CharSequence)(regionText = docText.subSequence(regionStartOffset = region.startOffset(), regionEndOffset = region.endOffset())))) continue;
                if (masterRegionString == null) {
                    masterRegionString = masterRegionText.toString();
                }
                this.doc.remove(regionStartOffset, regionEndOffset - regionStartOffset);
                this.doc.insertString(regionStartOffset, masterRegionString, null);
                this.fixRegionStartOffset(region, regionStartOffset);
            }
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, "Invalid offset", e);
        }
        finally {
            DocumentUtilities.setTypingModification((Document)this.doc, (boolean)oldTM);
            assert (this.ignoreDocModifications > 0);
            --this.ignoreDocModifications;
            this.afterDocumentModification();
        }
        this.updateMasterRegionBounds();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void setText(TextSync textSync, String text) {
        this.beforeDocumentModification();
        ++this.ignoreDocModifications;
        boolean oldTM = DocumentUtilities.isTypingModification((Document)this.doc);
        DocumentUtilities.setTypingModification((Document)this.doc, (boolean)false);
        try {
            CharSequence docText = DocumentUtilities.getText((Document)this.doc);
            for (TextRegion region : textSync.regionsModifiable()) {
                int regionEndOffset;
                int regionStartOffset = region.startOffset();
                CharSequence regionText = docText.subSequence(regionStartOffset, regionEndOffset = region.endOffset());
                if (CharSequenceUtilities.textEquals((CharSequence)text, (CharSequence)regionText)) continue;
                this.doc.remove(regionStartOffset, regionEndOffset - regionStartOffset);
                this.doc.insertString(regionStartOffset, text, null);
                this.fixRegionStartOffset(region, regionStartOffset);
            }
        }
        catch (BadLocationException e) {
            LOG.log(Level.WARNING, "Invalid offset", e);
        }
        finally {
            DocumentUtilities.setTypingModification((Document)this.doc, (boolean)oldTM);
            assert (this.ignoreDocModifications > 0);
            --this.ignoreDocModifications;
            this.afterDocumentModification();
        }
        this.updateMasterRegionBounds();
    }

    private void fixRegionStartOffset(TextRegion<?> region, int offset) throws BadLocationException {
        assert (!this.isRoot(region));
        TextRegion parent = region.parent();
        if (parent == null) {
            LOG.warning("Region with null parent:\n" + region + "\nRegions:\n" + this + "\n\n");
        }
        List regions = parent.regions();
        int index = TextRegionManager.findRegionIndex(regions, region) - 1;
        Position pos = this.doc.createPosition(offset);
        region.setStartPos(pos);
        while (index >= 0 && (region = regions.get(index)).endOffset() > offset) {
            region.setEndPos(pos);
            if (region.startOffset() <= offset) break;
            region.setStartPos(pos);
        }
        if (index < 0 && !this.isRoot(parent) && parent.startOffset() > offset) {
            parent.setStartPos(pos);
            this.fixRegionStartOffset(parent, offset);
        }
    }

    private boolean isRoot(TextRegion region) {
        return region == this.rootRegion;
    }

    private void updateMasterRegionBounds() {
        if (this.activeTextSync != null) {
            this.masterRegionStartOffset = this.activeTextSync.masterRegion().startOffset();
            this.masterRegionEndOffset = this.activeTextSync.masterRegion().endOffset();
        }
    }

    static int findRegionInsertIndex(List<TextRegion<?>> regions, int offset) {
        int low = 0;
        int high = regions.size() - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            TextRegion midRegion = regions.get(mid);
            int midStartOffset = midRegion.startOffset();
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            for (low = mid + 1; low < regions.size() && regions.get(low).startOffset() == offset; ++low) {
            }
        }
        return low;
    }

    static int findRegionIndex(List<TextRegion<?>> regions, TextRegion<?> region) {
        int low = 0;
        int high = regions.size() - 1;
        int offset = region.startOffset();
        block0 : while (low <= high) {
            int mid = (low + high) / 2;
            TextRegion midRegion = regions.get(mid);
            int midStartOffset = midRegion.startOffset();
            if (midStartOffset < offset) {
                low = mid + 1;
                continue;
            }
            if (midStartOffset > offset) {
                high = mid - 1;
                continue;
            }
            if (midRegion == region) {
                return mid;
            }
            for (low = mid - 1; low >= 0; --low) {
                midRegion = regions.get(low);
                if (midRegion == region) {
                    return low;
                }
                if (midRegion.startOffset() != offset) break;
            }
            for (low = mid + 1; low < regions.size(); ++low) {
                midRegion = regions.get(low);
                if (midRegion == region) {
                    return low;
                }
                if (midRegion.startOffset() != offset) continue block0;
            }
        }
        throw new IllegalStateException("Region: " + region + " not found by binary search:\n" + TextRegionManager.dumpRegions(null, regions, 4));
    }

    static void addRegion(TextRegion<?> parent, TextRegion<?> region) {
        int endConsumeIndex;
        int insertIndex;
        if (region.parent() != null) {
            throw new IllegalArgumentException("Region:" + region + " already added.");
        }
        List regions = parent.validRegions();
        int regionStartOffset = region.startOffset();
        int regionEndOffset = region.endOffset();
        for (endConsumeIndex = insertIndex = TextRegionManager.findRegionInsertIndex(regions, (int)regionStartOffset); endConsumeIndex < regions.size(); ++endConsumeIndex) {
            TextRegion consumeCandidate = regions.get(endConsumeIndex);
            if (regionEndOffset >= consumeCandidate.endOffset()) continue;
            if (regionEndOffset <= consumeCandidate.startOffset()) break;
            throw new IllegalArgumentException("Inserted region " + region + " overlaps with region " + consumeCandidate + " at index=" + endConsumeIndex);
        }
        while (insertIndex > 0) {
            int prevEndOffset;
            TextRegion prev = regions.get(insertIndex - 1);
            if (regionStartOffset == prev.startOffset()) {
                prevEndOffset = prev.endOffset();
                if (regionEndOffset < prevEndOffset) {
                    if (regionStartOffset != regionEndOffset) {
                        TextRegionManager.addRegion(prev, region);
                        return;
                    }
                    endConsumeIndex = --insertIndex;
                    break;
                }
            } else {
                prevEndOffset = prev.endOffset();
                if (regionStartOffset >= prevEndOffset) break;
                if (regionEndOffset <= prevEndOffset) {
                    TextRegionManager.addRegion(prev, region);
                    return;
                }
                throw new IllegalArgumentException("Inserted region " + region + " overlaps with region " + prev + " at index=" + (insertIndex - 1));
            }
            --insertIndex;
        }
        if (endConsumeIndex - insertIndex > 0) {
            GapList regionsGL = (GapList)regions;
            Object[] consumedRegions = new TextRegion[endConsumeIndex - insertIndex];
            regionsGL.copyElements(insertIndex, endConsumeIndex, consumedRegions, 0);
            regionsGL.remove(insertIndex, consumedRegions.length);
            region.initRegions((TextRegion<?>[])consumedRegions);
            for (Object r : consumedRegions) {
                r.setParent(region);
            }
        }
        regions.add(insertIndex, region);
        region.setParent(parent);
    }

    static void removeRegionFromParent(TextRegion<?> region) {
        TextRegion parent = region.parent();
        List regions = parent.regions();
        int index = TextRegionManager.findRegionIndex(regions, region);
        regions.remove(index);
        region.setParent(null);
        List children = region.regions();
        if (children != null) {
            for (TextRegion child : children) {
                regions.add(index++, child);
                child.setParent(parent);
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("Managed regions:\n");
        TextRegionManager.dumpRegions(sb, this.rootRegion.regions(), 4);
        sb.append("Managed groups:\n");
        for (TextSyncGroup group : this.editGroups) {
            sb.append("  ").append(group).append('\n');
        }
        if (this.activeTextSync != null) {
            sb.append("Active textSync: ").append(this.activeTextSync).append('\n');
        }
        sb.append('\n');
        return sb.toString();
    }

    private static StringBuilder dumpRegions(StringBuilder sb, List<TextRegion<?>> regions, int indent) {
        if (sb == null) {
            sb = new StringBuilder(100);
        }
        if (regions == null) {
            return sb;
        }
        for (TextRegion region : regions) {
            ArrayUtilities.appendSpaces((StringBuilder)sb, (int)indent);
            sb.append(region).append('\n');
            TextRegionManager.dumpRegions(sb, region.regions(), indent + 4);
        }
        return sb;
    }

    private static final class Highlighting {
        private static final String BAG_DOC_PROPERTY = TextRegionManager.class.getName() + "-OffsetsBag";
        private TextRegionManager textRegionManager;
        private AttributeSet attribs = null;
        private AttributeSet attribsLeft = null;
        private AttributeSet attribsRight = null;
        private AttributeSet attribsMiddle = null;
        private AttributeSet attribsAll = null;

        Highlighting(TextRegionManager textRegionManager) {
            this.textRegionManager = textRegionManager;
        }

        void requestRepaint() {
            TextRegion masterRegion;
            Document doc = this.textRegionManager.document();
            TextSync activeTextSync = this.textRegionManager.activeTextSync();
            if (activeTextSync != null && (masterRegion = activeTextSync.masterRegion()) != null) {
                if (this.attribs == null) {
                    this.attribs = Highlighting.getSyncedTextBlocksHighlight();
                    Color foreground = (Color)this.attribs.getAttribute(StyleConstants.Foreground);
                    Color background = (Color)this.attribs.getAttribute(StyleConstants.Background);
                    this.attribsLeft = Highlighting.createAttribs(StyleConstants.Background, background, EditorStyleConstants.LeftBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                    this.attribsRight = Highlighting.createAttribs(StyleConstants.Background, background, EditorStyleConstants.RightBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                    this.attribsMiddle = Highlighting.createAttribs(StyleConstants.Background, background, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                    this.attribsAll = Highlighting.createAttribs(StyleConstants.Background, background, EditorStyleConstants.LeftBorderLineColor, foreground, EditorStyleConstants.RightBorderLineColor, foreground, EditorStyleConstants.TopBorderLineColor, foreground, EditorStyleConstants.BottomBorderLineColor, foreground);
                }
                OffsetsBag nue = new OffsetsBag(doc);
                try {
                    int startOffset = masterRegion.startOffset();
                    int endOffset = masterRegion.endOffset();
                    int startLine = Utilities.getLineOffset((BaseDocument)((BaseDocument)doc), (int)startOffset);
                    int endLine = Utilities.getLineOffset((BaseDocument)((BaseDocument)doc), (int)endOffset);
                    for (int i = startLine; i <= endLine; ++i) {
                        int s = Math.max(Utilities.getRowStartFromLineOffset((BaseDocument)((BaseDocument)doc), (int)i), startOffset);
                        int e = Math.min(Utilities.getRowEnd((BaseDocument)((BaseDocument)doc), (int)s), endOffset);
                        int size = e - s;
                        if (size == 1) {
                            nue.addHighlight(s, e, this.attribsAll);
                            continue;
                        }
                        if (size <= 1) continue;
                        nue.addHighlight(s, s + 1, this.attribsLeft);
                        nue.addHighlight(e - 1, e, this.attribsRight);
                        if (size <= 2) continue;
                        nue.addHighlight(s + 1, e - 1, this.attribsMiddle);
                    }
                }
                catch (BadLocationException ble) {
                    TextRegionManager.LOG.log(Level.WARNING, null, ble);
                }
                OffsetsBag bag = Highlighting.getBag(doc);
                bag.setHighlights(nue);
            } else {
                OffsetsBag bag = Highlighting.getBag(doc);
                bag.clear();
                this.attribs = null;
            }
        }

        private static synchronized OffsetsBag getBag(Document doc) {
            OffsetsBag bag = (OffsetsBag)doc.getProperty(BAG_DOC_PROPERTY);
            if (bag == null) {
                bag = new OffsetsBag(doc);
                doc.putProperty(BAG_DOC_PROPERTY, (Object)bag);
            }
            return bag;
        }

        private static AttributeSet getSyncedTextBlocksHighlight() {
            FontColorSettings fcs = (FontColorSettings)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(FontColorSettings.class);
            AttributeSet as = fcs.getFontColors("synchronized-text-blocks-ext");
            return as == null ? SimpleAttributeSet.EMPTY : as;
        }

        private static /* varargs */ AttributeSet createAttribs(Object ... keyValuePairs) {
            assert (keyValuePairs.length % 2 == 0);
            ArrayList<Object> list = new ArrayList<Object>(keyValuePairs.length);
            for (int i = keyValuePairs.length / 2 - 1; i >= 0; --i) {
                Object attrKey = keyValuePairs[2 * i];
                Object attrValue = keyValuePairs[2 * i + 1];
                if (attrKey == null || attrValue == null) continue;
                list.add(attrKey);
                list.add(attrValue);
            }
            return AttributesUtilities.createImmutable((Object[])list.toArray());
        }

        public static final class HLFactory
        implements HighlightsLayerFactory {
            public HighlightsLayer[] createLayers(HighlightsLayerFactory.Context context) {
                return new HighlightsLayer[]{HighlightsLayer.create((String)"org.netbeans.lib.editor.codetemplates.CodeTemplateParametersHighlights", (ZOrder)ZOrder.SHOW_OFF_RACK.forPosition(490), (boolean)true, (HighlightsContainer)Highlighting.getBag(context.getDocument()))};
            }
        }

    }

    private static final class OverrideKeysListener
    implements KeyListener {
        static OverrideKeysListener INSTANCE = new OverrideKeysListener();

        private OverrideKeysListener() {
        }

        @Override
        public void keyPressed(KeyEvent evt) {
            TextRegionManager manager = this.textRegionManager(evt);
            if (manager == null || !manager.isActive() || evt.isConsumed()) {
                return;
            }
            KeyStroke evtKeyStroke = KeyStroke.getKeyStrokeForEvent(evt);
            if (KeyStroke.getKeyStroke(27, 0) == evtKeyStroke) {
                manager.escapeAction();
                evt.consume();
            }
        }

        @Override
        public void keyTyped(KeyEvent evt) {
        }

        @Override
        public void keyReleased(KeyEvent evt) {
        }

        private TextRegionManager textRegionManager(KeyEvent evt) {
            return TextRegionManager.get(((JTextComponent)evt.getSource()).getDocument(), false);
        }
    }

    private static final class OverrideAction
    extends TextAction {
        private static final String ORIGINAL_ACTION_PROPERTY = "original-action";
        private static final int TAB = 1;
        private static final int SHIFT_TAB = 2;
        private static final int ENTER = 3;
        private final int actionType;

        public static ActionMap[] installOverrideActionMap(JTextComponent component) {
            OverrideAction[] actions;
            ActionMap origActionMap = component.getActionMap();
            ActionMap actionMap = new ActionMap();
            for (OverrideAction action : actions = new OverrideAction[]{new OverrideAction(1), new OverrideAction(2), new OverrideAction(3)}) {
                Object actionKey = (String)action.getValue("Name");
                assert (actionKey != null);
                actionKey = action.findActionKey(component);
                if (actionKey == null) continue;
                Action origAction = origActionMap.get(actionKey);
                action.putValue("original-action", origAction);
                actionMap.put(actionKey, action);
            }
            actionMap.setParent(origActionMap);
            component.setActionMap(actionMap);
            return new ActionMap[]{origActionMap, actionMap};
        }

        private static String actionType2Name(int actionType) {
            switch (actionType) {
                case 1: {
                    return "insert-tab";
                }
                case 2: {
                    return "remove-tab";
                }
                case 3: {
                    return "insert-break";
                }
            }
            throw new IllegalArgumentException();
        }

        private OverrideAction(int actionType) {
            super(OverrideAction.actionType2Name(actionType));
            this.actionType = actionType;
        }

        private TextRegionManager textRegionManager(ActionEvent evt) {
            JTextComponent component = this.getTextComponent(evt);
            if (component != null) {
                return TextRegionManager.get(component.getDocument(), false);
            }
            return null;
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            TextRegionManager manager = this.textRegionManager(evt);
            if (manager != null) {
                switch (this.actionType) {
                    case 1: {
                        manager.tabAction();
                        break;
                    }
                    case 2: {
                        manager.shiftTabAction();
                        break;
                    }
                    case 3: {
                        Action original;
                        if (manager.enterAction() || (original = (Action)this.getValue("original-action")) == null) break;
                        original.actionPerformed(evt);
                    }
                }
            }
        }

        Object findActionKey(JTextComponent component) {
            KeyStroke keyStroke;
            switch (this.actionType) {
                case 1: {
                    keyStroke = KeyStroke.getKeyStroke(9, 0);
                    break;
                }
                case 2: {
                    keyStroke = KeyStroke.getKeyStroke(9, 1);
                    break;
                }
                case 3: {
                    keyStroke = KeyStroke.getKeyStroke(10, 0);
                    break;
                }
                default: {
                    throw new IllegalArgumentException();
                }
            }
            Object key = component.getInputMap().get(keyStroke);
            return key;
        }
    }

    private static final class DocChangeListener
    implements PropertyChangeListener {
        static final DocChangeListener INSTANCE = new DocChangeListener();

        private DocChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            TextRegionManager manager;
            if ("document".equals(evt.getPropertyName()) && (manager = TextRegionManager.get(((JTextComponent)evt.getSource()).getDocument(), false)) != null) {
                manager.stopSyncEditing();
            }
        }
    }

    private static final class UpdateDocListener
    implements DocumentListener {
        static final UpdateDocListener INSTANCE = new UpdateDocListener();

        private UpdateDocListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            TextRegionManager.get(e.getDocument(), false).removeUpdateUpdate(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }

    private static final class DocListener
    implements DocumentListener {
        static final DocListener INSTANCE = new DocListener();

        private DocListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            TextRegionManager.get(e.getDocument(), false).insertUpdate(e);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            TextRegionManager.get(e.getDocument(), false).removeUpdate(e);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }

}

