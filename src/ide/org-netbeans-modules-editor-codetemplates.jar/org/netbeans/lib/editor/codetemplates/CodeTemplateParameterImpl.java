/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import org.netbeans.lib.editor.codetemplates.CodeTemplateInsertHandler;
import org.netbeans.lib.editor.codetemplates.CodeTemplateSpiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegion;

public final class CodeTemplateParameterImpl {
    private static final String NULL_PARAMETER_NAME = "<null>";
    private static final String NULL_HINT_NAME = "<null>";
    private static final String TRUE_HINT_VALUE = "true";
    private final CodeTemplateInsertHandler handler;
    private final CodeTemplateParameter parameter;
    private String value;
    private int parametrizedTextStartOffset;
    private int parametrizedTextEndOffset;
    private CodeTemplateParameter master;
    private Collection<CodeTemplateParameter> slaves;
    private String name;
    private Map<String, String> hints;
    private Map<String, String> hintsUnmodifiable;
    private TextRegion<CodeTemplateParameterImpl> textRegion;
    private boolean editable;
    private boolean userModified;

    public static CodeTemplateParameterImpl get(CodeTemplateParameter parameter) {
        return CodeTemplateSpiPackageAccessor.get().getImpl(parameter);
    }

    CodeTemplateParameterImpl(CodeTemplateInsertHandler handler, String parametrizedText, int parametrizedTextOffset) {
        this.handler = handler;
        this.parametrizedTextStartOffset = parametrizedTextOffset;
        this.parameter = CodeTemplateSpiPackageAccessor.get().createParameter(this);
        this.textRegion = new TextRegion();
        this.textRegion.setClientInfo(this);
        this.parseParameterContent(parametrizedText);
    }

    public CodeTemplateParameter getParameter() {
        return this.parameter;
    }

    public CodeTemplateInsertHandler getHandler() {
        return this.handler;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.isSlave() ? this.master.getValue() : (this.handler != null && this.handler.isInserted() ? this.handler.getDocParameterValue(this) : this.value);
    }

    public void setValue(String newValue) {
        if (this.isSlave()) {
            throw new IllegalStateException("Cannot set value for slave parameter");
        }
        if (newValue == null) {
            throw new NullPointerException("newValue cannot be null");
        }
        if (!this.handler.isReleased()) {
            if (this.handler.isInserted()) {
                this.handler.setDocMasterParameterValue(this, newValue);
                return;
            }
            this.value = newValue;
            this.handler.resetCachedInsertText();
        }
    }

    public boolean isEditable() {
        return this.editable;
    }

    public boolean isUserModified() {
        return this.userModified;
    }

    void markUserModified() {
        this.userModified = true;
    }

    public int getParametrizedTextStartOffset() {
        return this.parametrizedTextStartOffset;
    }

    public int getParametrizedTextEndOffset() {
        return this.parametrizedTextEndOffset;
    }

    public int getInsertTextOffset() {
        if (this.handler != null) {
            if (!this.handler.isInserted()) {
                this.handler.checkInsertTextBuilt();
            }
            return this.textRegion.startOffset() - this.handler.getInsertOffset();
        }
        return this.textRegion.startOffset();
    }

    void resetPositions(Position startPosition, Position endPosition) {
        this.textRegion.updateBounds(startPosition, endPosition);
    }

    public TextRegion textRegion() {
        return this.textRegion;
    }

    public Map<String, String> getHints() {
        return this.hintsUnmodifiable != null ? this.hintsUnmodifiable : Collections.emptyMap();
    }

    public CodeTemplateParameter getMaster() {
        return this.master;
    }

    public Collection<? extends CodeTemplateParameter> getSlaves() {
        return this.slaves != null ? this.slaves : Collections.emptyList();
    }

    public boolean isSlave() {
        return this.master != null;
    }

    void markSlave(CodeTemplateParameter master) {
        CodeTemplateParameterImpl masterImpl = CodeTemplateParameterImpl.paramImpl(master);
        if (this.getMaster() != null) {
            throw new IllegalStateException(this.toString() + " already slave of " + master);
        }
        this.setMaster(master);
        masterImpl.addSlave(this.getParameter());
        if (this.slaves != null) {
            Iterator<CodeTemplateParameter> it = this.slaves.iterator();
            while (it.hasNext()) {
                CodeTemplateParameterImpl paramImpl = CodeTemplateParameterImpl.paramImpl(it.next());
                paramImpl.setMaster(master);
                masterImpl.addSlave(paramImpl.getParameter());
            }
            this.slaves.clear();
        }
    }

    private static CodeTemplateParameterImpl paramImpl(CodeTemplateParameter param) {
        return CodeTemplateSpiPackageAccessor.get().getImpl(param);
    }

    private void parseParameterContent(String parametrizedText) {
        String defaultValue;
        int index = this.parametrizedTextStartOffset + 2;
        String hintName = null;
        boolean afterEquals = false;
        int nameStartIndex = -1;
        boolean insideStringLiteral = false;
        StringBuffer stringLiteralText = new StringBuffer();
        do {
            String completedString = null;
            if (index >= parametrizedText.length()) break;
            char ch = parametrizedText.charAt(index);
            if (insideStringLiteral) {
                if (ch == '\"') {
                    insideStringLiteral = false;
                    completedString = stringLiteralText.toString();
                    stringLiteralText.setLength(0);
                } else if (ch == '\\') {
                    index = this.escapedChar(parametrizedText, index + 1, stringLiteralText);
                } else {
                    stringLiteralText.append(ch);
                }
            } else if (Character.isWhitespace(ch) || ch == '=' || ch == '}') {
                if (nameStartIndex != -1) {
                    completedString = parametrizedText.substring(nameStartIndex, index);
                    nameStartIndex = -1;
                }
            } else if (ch == '\"') {
                insideStringLiteral = true;
            } else if (nameStartIndex == -1) {
                nameStartIndex = index;
            }
            if (completedString != null) {
                if (this.name == null) {
                    this.name = completedString;
                } else {
                    if (this.hints == null) {
                        this.hints = new LinkedHashMap<String, String>(4);
                        this.hintsUnmodifiable = Collections.unmodifiableMap(this.hints);
                    }
                    if (hintName == null) {
                        if (afterEquals) {
                            this.hints.put("<null>", completedString);
                            afterEquals = false;
                        } else {
                            hintName = completedString;
                        }
                    } else if (afterEquals) {
                        this.hints.put(hintName, completedString);
                        afterEquals = false;
                        hintName = null;
                    } else {
                        this.hints.put(hintName, "true");
                        hintName = completedString;
                    }
                }
            }
            if (!insideStringLiteral) {
                if (ch == '=') {
                    afterEquals = true;
                } else if (ch == '}') {
                    if (hintName == null) break;
                    this.hints.put(hintName, "true");
                    hintName = null;
                    break;
                }
            }
            ++index;
        } while (true);
        if (this.name == null) {
            this.name = "<null>";
        }
        if ((defaultValue = this.getHints().get("default")) == null) {
            defaultValue = this.name;
        }
        this.value = defaultValue;
        if (this.name.equals("cursor") || this.name.equals("no-format") || this.name.equals("no-indent")) {
            this.editable = false;
            this.value = "";
        } else if (this.name.equals("selection")) {
            this.editable = false;
            if (this.handler != null) {
                this.value = this.handler.getComponent().getSelectedText();
                if (this.value == null) {
                    this.value = "";
                }
                if (this.value.endsWith("\n")) {
                    this.value = this.value.substring(0, this.value.length() - 1);
                }
                if (this.getHints().get("line") != null && !this.value.endsWith("\n")) {
                    this.value = this.value + "\n";
                }
            }
        } else {
            this.editable = !this.isHintValueFalse("editable");
        }
        this.parametrizedTextEndOffset = index + 1;
    }

    private boolean isHintValueFalse(String hintName) {
        String hintValue = this.getHints().get(hintName);
        return hintValue != null && "false".equals(hintValue.toLowerCase());
    }

    private int escapedChar(CharSequence text, int index, StringBuffer output) {
        if (index == text.length()) {
            output.append('\\');
        } else {
            switch (text.charAt(index++)) {
                case '\\': {
                    output.append('\\');
                    break;
                }
                case 'n': {
                    output.append('\n');
                    break;
                }
                case 'r': {
                    output.append('\r');
                    break;
                }
                case '\"': {
                    output.append('\"');
                    break;
                }
                case '\'': {
                    output.append('\'');
                    break;
                }
                case 'u': {
                    int val = 0;
                    for (int i = 0; i < 4; ++i) {
                        if (index < text.length()) {
                            char ch = text.charAt(index);
                            if (ch >= '0' && ch <= '9') {
                                val = (val << 4) + (ch - 48);
                            } else if (ch >= 'a' && ch <= 'f') {
                                val = (val << 4) + 10 + (ch - 97);
                            } else {
                                if (ch < 'A' || ch > 'F') break;
                                val = (val << 4) + 10 + (ch - 70);
                            }
                        }
                        ++index;
                    }
                    output.append(val);
                    break;
                }
                default: {
                    --index;
                    output.append('\\');
                }
            }
        }
        return index;
    }

    private void addSlave(CodeTemplateParameter slave) {
        if (this.slaves == null) {
            this.slaves = new ArrayList<CodeTemplateParameter>(2);
        }
        this.slaves.add(slave);
    }

    private void setMaster(CodeTemplateParameter master) {
        this.master = master;
    }

    public String toString() {
        return "name=" + this.getName() + ", slave=" + this.isSlave() + ", value=\"" + this.getValue() + '\"';
    }
}

