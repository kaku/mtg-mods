/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.EditorRegistry
 *  org.openide.modules.ModuleInstall
 */
package org.netbeans.lib.editor.codetemplates;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.lib.editor.codetemplates.AbbrevDetection;
import org.openide.modules.ModuleInstall;

public final class CodeTemplatesModule
extends ModuleInstall {
    private PropertyChangeListener editorsTracker;

    public CodeTemplatesModule() {
        this.editorsTracker = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName() == null || "focusGained".equals(evt.getPropertyName())) {
                    AbbrevDetection.get((JTextComponent)evt.getNewValue());
                }
            }
        };
    }

    public void restored() {
        EditorRegistry.addPropertyChangeListener((PropertyChangeListener)this.editorsTracker);
    }

    public void close() {
        this.finish();
    }

    public void uninstalled() {
        this.finish();
    }

    private void finish() {
        EditorRegistry.removePropertyChangeListener((PropertyChangeListener)this.editorsTracker);
        for (JTextComponent jtc : EditorRegistry.componentList()) {
            AbbrevDetection.remove(jtc);
        }
    }

}

