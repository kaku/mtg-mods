/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.editor.mimelookup.MimeLocation
 */
package org.netbeans.lib.editor.codetemplates.spi;

import java.util.List;
import javax.swing.text.JTextComponent;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.spi.editor.mimelookup.MimeLocation;

public interface CodeTemplateFilter {
    public boolean accept(CodeTemplate var1);

    @MimeLocation(subfolderName="CodeTemplateFilterFactories")
    public static interface ContextBasedFactory
    extends Factory {
        public List<String> getSupportedContexts();
    }

    @MimeLocation(subfolderName="CodeTemplateFilterFactories")
    public static interface Factory {
        public CodeTemplateFilter createFilter(JTextComponent var1, int var2);
    }

}

