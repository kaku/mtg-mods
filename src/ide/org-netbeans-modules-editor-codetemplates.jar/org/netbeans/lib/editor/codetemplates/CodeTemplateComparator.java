/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.Comparator;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;

public final class CodeTemplateComparator
implements Comparator<CodeTemplate> {
    public static final Comparator<CodeTemplate> BY_ABBREVIATION_IGNORE_CASE = new CodeTemplateComparator(true, true);
    public static final Comparator<CodeTemplate> BY_PARAMETRIZED_TEXT_IGNORE_CASE = new CodeTemplateComparator(false, true);
    private final boolean byAbbreviation;
    private final boolean ignoreCase;

    private CodeTemplateComparator(boolean byAbbreviation, boolean ignoreCase) {
        this.byAbbreviation = byAbbreviation;
        this.ignoreCase = ignoreCase;
    }

    @Override
    public int compare(CodeTemplate t1, CodeTemplate t2) {
        String n1 = this.byAbbreviation ? t1.getAbbreviation() : t1.getParametrizedText();
        String n2 = this.byAbbreviation ? t2.getAbbreviation() : t2.getParametrizedText();
        return this.ignoreCase ? n1.compareToIgnoreCase(n2) : n1.compareTo(n2);
    }
}

