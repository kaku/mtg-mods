/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.api.editor.settings.CodeTemplateDescription
 *  org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage
 *  org.openide.util.Utilities
 */
package org.netbeans.lib.editor.codetemplates.storage;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.KeyStroke;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.CodeTemplateDescription;
import org.netbeans.modules.editor.settings.storage.api.EditorSettingsStorage;
import org.openide.util.Utilities;

public final class CodeTemplateSettingsImpl {
    private static final Logger LOG = Logger.getLogger(CodeTemplateSettingsImpl.class.getName());
    public static final String PROP_CODE_TEMPLATES = "CodeTemplateSettingsImpl.PROP_CODE_TEMPLATES";
    public static final String PROP_EXPANSION_KEY = "CodeTemplateSettingsImpl.PROP_EXPANSION_KEY";
    public static final String PROP_ON_EXPAND_ACTION = "CodeTemplateSettingsImpl.PROP_ON_EXPAND_ACTION";
    private static final String CODE_TEMPLATE_EXPAND_KEY = "code-template-expand-key";
    private static final KeyStroke DEFAULT_EXPANSION_KEY = KeyStroke.getKeyStroke(9, 0);
    private static final String CODE_TEMPLATE_ON_EXPAND_ACTION = "code-template-on-expand-action";
    private static final Map<MimePath, WeakReference<CodeTemplateSettingsImpl>> INSTANCES = new WeakHashMap<MimePath, WeakReference<CodeTemplateSettingsImpl>>();
    private final MimePath mimePath;
    private final PropertyChangeSupport pcs;

    public static synchronized CodeTemplateSettingsImpl get(MimePath mimePath) {
        CodeTemplateSettingsImpl result;
        WeakReference<CodeTemplateSettingsImpl> reference = INSTANCES.get((Object)mimePath);
        CodeTemplateSettingsImpl codeTemplateSettingsImpl = result = reference == null ? null : reference.get();
        if (result == null) {
            result = new CodeTemplateSettingsImpl(mimePath);
            INSTANCES.put(mimePath, new WeakReference<CodeTemplateSettingsImpl>(result));
        }
        return result;
    }

    public Map<String, CodeTemplateDescription> getCodeTemplates() {
        EditorSettingsStorage ess = EditorSettingsStorage.get((String)"CodeTemplates");
        try {
            return ess.load(this.mimePath, null, false);
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
            return Collections.emptyMap();
        }
    }

    public void setCodeTemplates(Map<String, CodeTemplateDescription> map) {
        EditorSettingsStorage ess = EditorSettingsStorage.get((String)"CodeTemplates");
        try {
            if (map == null) {
                ess.delete(this.mimePath, null, false);
            } else {
                ess.save(this.mimePath, null, false, map);
            }
        }
        catch (IOException ioe) {
            LOG.log(Level.WARNING, null, ioe);
        }
        this.pcs.firePropertyChange("CodeTemplateSettingsImpl.PROP_CODE_TEMPLATES", null, null);
    }

    public KeyStroke getExpandKey() {
        KeyStroke keyStroke;
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        String ks = prefs.get("code-template-expand-key", null);
        if (ks != null && (keyStroke = Utilities.stringToKey((String)ks)) != null) {
            return keyStroke;
        }
        return DEFAULT_EXPANSION_KEY;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setExpandKey(KeyStroke expansionKey) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        prefs.put("code-template-expand-key", Utilities.keyToString((KeyStroke)expansionKey));
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException ex) {
            // empty catch block
        }
        ArrayList<CodeTemplateSettingsImpl> all = new ArrayList<CodeTemplateSettingsImpl>();
        Class<CodeTemplateSettingsImpl> class_ = CodeTemplateSettingsImpl.class;
        synchronized (CodeTemplateSettingsImpl.class) {
            for (Reference r : INSTANCES.values()) {
                CodeTemplateSettingsImpl ctsi = (CodeTemplateSettingsImpl)r.get();
                if (ctsi == null) continue;
                all.add(ctsi);
            }
            // ** MonitorExit[class_] (shouldn't be in output)
            for (CodeTemplateSettingsImpl ctsi : all) {
                ctsi.pcs.firePropertyChange("CodeTemplateSettingsImpl.PROP_EXPANSION_KEY", null, null);
            }
            return;
        }
    }

    public OnExpandAction getOnExpandAction() {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        String action = prefs.get("code-template-on-expand-action", null);
        if (action != null) {
            return OnExpandAction.valueOf(action);
        }
        return OnExpandAction.FORMAT;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setOnExpandAction(OnExpandAction action) {
        Preferences prefs = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        prefs.put("code-template-on-expand-action", action.name());
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException ex) {
            // empty catch block
        }
        ArrayList<CodeTemplateSettingsImpl> all = new ArrayList<CodeTemplateSettingsImpl>();
        Class<CodeTemplateSettingsImpl> class_ = CodeTemplateSettingsImpl.class;
        synchronized (CodeTemplateSettingsImpl.class) {
            for (Reference r : INSTANCES.values()) {
                CodeTemplateSettingsImpl ctsi = (CodeTemplateSettingsImpl)r.get();
                if (ctsi == null) continue;
                all.add(ctsi);
            }
            // ** MonitorExit[class_] (shouldn't be in output)
            for (CodeTemplateSettingsImpl ctsi : all) {
                ctsi.pcs.firePropertyChange("CodeTemplateSettingsImpl.PROP_CODE_TEMPLATES", null, null);
            }
            return;
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        this.pcs.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        this.pcs.removePropertyChangeListener(l);
    }

    private CodeTemplateSettingsImpl(MimePath mimePath) {
        this.pcs = new PropertyChangeSupport(this);
        this.mimePath = mimePath;
    }

    public static enum OnExpandAction {
        FORMAT,
        INDENT,
        NOOP;
        

        private OnExpandAction() {
        }
    }

}

