/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.textsync;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegion;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManager;
import org.netbeans.lib.editor.codetemplates.textsync.TextSyncGroup;

public final class TextSync {
    private static int EDITABLE_FLAG = 1;
    private static int CARET_MARKER_FLAG = 2;
    private TextSyncGroup<?> textSyncGroup;
    private List<TextRegion<?>> regions;
    private int masterRegionIndex;
    private int flags;

    public /* varargs */ TextSync(TextRegion<?> ... regions) {
        this.initRegions(regions.length);
        for (TextRegion region : regions) {
            this.addRegion(region);
        }
    }

    public TextSync() {
        this.initRegions(4);
    }

    private void initRegions(int size) {
        this.regions = new ArrayList(size);
    }

    public List<TextRegion<?>> regions() {
        return Collections.unmodifiableList(this.regions);
    }

    public <I> TextRegion<I> region(int index) {
        TextRegion region = this.regions.get(index);
        return region;
    }

    public <I> TextRegion<I> masterRegion() {
        if (this.masterRegionIndex < 0 || this.masterRegionIndex >= this.regions.size()) {
            return null;
        }
        TextRegion region = this.regions.get(this.masterRegionIndex);
        return region;
    }

    public <I> TextRegion<I> validMasterRegion() {
        TextRegion<I> masterRegion = this.masterRegion();
        if (masterRegion == null) {
            throw new IllegalStateException("masterRegion expected to be non-null");
        }
        return masterRegion;
    }

    public int masterRegionIndex() {
        return this.masterRegionIndex;
    }

    public void setMasterRegionIndex(int masterRegionIndex) {
        this.masterRegionIndex = masterRegionIndex;
    }

    public void syncByMaster() {
        this.validTextRegionManager().syncByMaster(this);
    }

    public void setText(String text) {
        this.validTextRegionManager().setText(this, text);
    }

    public boolean isEditable() {
        return (this.flags & EDITABLE_FLAG) != 0;
    }

    public void setEditable(boolean editable) {
        this.flags = editable ? (this.flags |= EDITABLE_FLAG) : (this.flags &= ~ EDITABLE_FLAG);
    }

    public boolean isCaretMarker() {
        return (this.flags & CARET_MARKER_FLAG) != 0;
    }

    public void setCaretMarker(boolean caretMarker) {
        this.flags = caretMarker ? (this.flags |= CARET_MARKER_FLAG) : (this.flags &= ~ CARET_MARKER_FLAG);
    }

    public void addRegion(TextRegion<?> region) {
        if (region == null) {
            throw new IllegalArgumentException("region cannot be null");
        }
        if (region.textSync() != null) {
            throw new IllegalArgumentException("region " + region + " already assigned to textSync=" + region.textSync());
        }
        this.regions.add(region);
        region.setTextSync(this);
    }

    public void removeRegion(TextRegion region) {
        int index = this.regions.indexOf(region);
        if (index == -1) {
            throw new IllegalArgumentException("region " + region + " not part of textSync " + this);
        }
        this.regions.remove(index);
        region.setTextSync(null);
        if (index == this.masterRegionIndex) {
            index = -1;
        } else if (index < this.masterRegionIndex) {
            --this.masterRegionIndex;
        }
    }

    List<TextRegion<?>> regionsModifiable() {
        return this.regions;
    }

    public <T> TextSyncGroup<T> group() {
        TextSyncGroup group = this.textSyncGroup;
        return group;
    }

    void setGroup(TextSyncGroup<?> textSyncGroup) {
        this.textSyncGroup = textSyncGroup;
    }

    TextRegionManager textRegionManager() {
        return this.textSyncGroup != null ? this.textSyncGroup.textRegionManager() : null;
    }

    private TextRegionManager validTextRegionManager() {
        TextRegionManager textRegionManager = this.textRegionManager();
        if (textRegionManager == null) {
            throw new IllegalStateException("Only possible for textSync connected to a TextRegionManager");
        }
        return textRegionManager;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.regions.size() * 8 + 2);
        TextRegion<I> masterRegion = this.masterRegion();
        for (TextRegion textRegion : this.regions) {
            sb.append("    ");
            if (textRegion == masterRegion) {
                sb.append("M:");
            }
            sb.append(textRegion).append('\n');
        }
        return sb.toString();
    }
}

