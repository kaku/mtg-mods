/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.spi;

import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;

public interface CodeTemplateProcessor {
    public void updateDefaultValues();

    public void parameterValueChanged(CodeTemplateParameter var1, boolean var2);

    public void release();
}

