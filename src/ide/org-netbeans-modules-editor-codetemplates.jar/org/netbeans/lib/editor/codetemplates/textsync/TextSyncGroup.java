/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.textsync;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.netbeans.lib.editor.codetemplates.textsync.TextRegionManager;
import org.netbeans.lib.editor.codetemplates.textsync.TextSync;

public final class TextSyncGroup<I> {
    private TextRegionManager manager;
    private List<TextSync> textSyncs;
    private int activeTextSyncIndex = -1;
    private I clientInfo;

    public /* varargs */ TextSyncGroup(TextSync ... textSyncs) {
        this.initTextSyncs(textSyncs.length);
        for (TextSync textSync : textSyncs) {
            this.addTextSync(textSync);
        }
    }

    public TextSyncGroup() {
        this.initTextSyncs(4);
    }

    private void initTextSyncs(int size) {
        this.textSyncs = new ArrayList<TextSync>(size);
    }

    public List<TextSync> textSyncs() {
        return Collections.unmodifiableList(this.textSyncs);
    }

    public void addTextSync(TextSync textSync) {
        if (textSync == null) {
            throw new IllegalArgumentException("textSync cannot be null");
        }
        if (textSync.group() != null) {
            throw new IllegalArgumentException("textSync " + textSync + " already assigned to group " + textSync.group());
        }
        this.textSyncs.add(textSync);
        textSync.setGroup(this);
    }

    public void removeTextSync(TextSync textSync) {
        if (this.textSyncs.remove(textSync)) {
            textSync.setGroup(null);
        }
    }

    List<TextSync> textSyncsModifiable() {
        return this.textSyncs;
    }

    public TextRegionManager textRegionManager() {
        return this.manager;
    }

    void setTextRegionManager(TextRegionManager manager) {
        this.manager = manager;
    }

    public I clientInfo() {
        return this.clientInfo;
    }

    public void setClientInfo(I clientInfo) {
        this.clientInfo = clientInfo;
    }

    public TextSync activeTextSync() {
        return this.activeTextSyncIndex >= 0 && this.activeTextSyncIndex < this.textSyncs.size() ? this.textSyncs.get(this.activeTextSyncIndex) : null;
    }

    public int activeTextSyncIndex() {
        return this.activeTextSyncIndex;
    }

    void setActiveTextSyncIndex(int activeTextSyncIndex) {
        this.activeTextSyncIndex = activeTextSyncIndex;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.textSyncs.size() * 50 + 2);
        sb.append(super.toString());
        if (this.clientInfo != null) {
            sb.append(" ").append(this.clientInfo);
        }
        sb.append('\n');
        for (TextSync ts : this.textSyncs) {
            sb.append("  ").append(ts).append('\n');
        }
        return sb.toString();
    }
}

