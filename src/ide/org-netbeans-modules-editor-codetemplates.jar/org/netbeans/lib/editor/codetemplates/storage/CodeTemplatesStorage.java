/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.settings.CodeTemplateDescription
 *  org.netbeans.lib.editor.util.CharacterConversions
 *  org.netbeans.modules.editor.settings.storage.spi.StorageDescription
 *  org.netbeans.modules.editor.settings.storage.spi.StorageReader
 *  org.netbeans.modules.editor.settings.storage.spi.StorageWriter
 *  org.netbeans.modules.editor.settings.storage.spi.support.StorageSupport
 *  org.openide.filesystems.FileObject
 *  org.openide.xml.XMLUtil
 */
package org.netbeans.lib.editor.codetemplates.storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.api.editor.settings.CodeTemplateDescription;
import org.netbeans.lib.editor.util.CharacterConversions;
import org.netbeans.modules.editor.settings.storage.spi.StorageDescription;
import org.netbeans.modules.editor.settings.storage.spi.StorageReader;
import org.netbeans.modules.editor.settings.storage.spi.StorageWriter;
import org.netbeans.modules.editor.settings.storage.spi.support.StorageSupport;
import org.openide.filesystems.FileObject;
import org.openide.xml.XMLUtil;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public final class CodeTemplatesStorage
implements StorageDescription<String, CodeTemplateDescription> {
    private static final Logger LOG = Logger.getLogger(CodeTemplatesStorage.class.getName());
    public static final String ID = "CodeTemplates";
    private static final String E_ROOT = "codetemplates";
    private static final String E_CODETEMPLATE = "codetemplate";
    private static final String E_DESCRIPTION = "description";
    private static final String E_CODE = "code";
    private static final String A_ABBREV = "abbreviation";
    private static final String A_DESCRIPTION_ID = "descriptionId";
    private static final String A_CONTEXTS = "contexts";
    private static final String A_UUID = "uuid";
    private static final String A_REMOVE = "remove";
    private static final String A_XML_SPACE = "xml:space";
    private static final String V_PRESERVE = "preserve";
    private static final String PUBLIC_ID = "-//NetBeans//DTD Editor Code Templates settings 1.0//EN";
    private static final String SYSTEM_ID = "http://www.netbeans.org/dtds/EditorCodeTemplates-1_0.dtd";
    private static final String MIME_TYPE = "text/x-nbeditor-codetemplatesettings";

    public String getId() {
        return "CodeTemplates";
    }

    public boolean isUsingProfiles() {
        return false;
    }

    public String getMimeType() {
        return "text/x-nbeditor-codetemplatesettings";
    }

    public String getLegacyFileName() {
        return "abbreviations.xml";
    }

    public StorageReader<String, CodeTemplateDescription> createReader(FileObject f, String mimePath) {
        if ("text/x-nbeditor-codetemplatesettings".equals(f.getMIMEType())) {
            return new Reader(f, mimePath);
        }
        return new LegacyReader(f, mimePath);
    }

    public StorageWriter<String, CodeTemplateDescription> createWriter(FileObject f, String mimePath) {
        return new Writer();
    }

    private static final class Writer
    extends StorageWriter<String, CodeTemplateDescription> {
        public Document getDocument() {
            Element element;
            Document doc = XMLUtil.createDocument((String)"codetemplates", (String)null, (String)"-//NetBeans//DTD Editor Code Templates settings 1.0//EN", (String)"http://www.netbeans.org/dtds/EditorCodeTemplates-1_0.dtd");
            Node root = doc.getElementsByTagName("codetemplates").item(0);
            for (CodeTemplateDescription codeTemplate : this.getAdded().values()) {
                String description;
                String uuid;
                element = doc.createElement("codetemplate");
                root.appendChild(element);
                element.setAttribute("abbreviation", codeTemplate.getAbbreviation());
                List contexts = codeTemplate.getContexts();
                if (contexts != null && !contexts.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < contexts.size(); ++i) {
                        String ctx = (String)contexts.get(i);
                        if (ctx == null || (ctx = ctx.trim()).length() <= 0) continue;
                        if (i > 0) {
                            sb.append(",");
                        }
                        sb.append(ctx);
                    }
                    if (sb.length() > 0) {
                        element.setAttribute("contexts", sb.toString());
                    }
                }
                if ((uuid = codeTemplate.getUniqueId()) != null) {
                    element.setAttribute("uuid", uuid);
                }
                element.setAttribute("xml:space", "preserve");
                String code = codeTemplate.getParametrizedText();
                if (code.length() > 0) {
                    Element codeElement = doc.createElement("code");
                    codeElement.appendChild(doc.createCDATASection(code));
                    element.appendChild(codeElement);
                }
                if ((description = codeTemplate.getDescription()) == null || description.length() <= 0) continue;
                Element descriptionElement = doc.createElement("description");
                descriptionElement.appendChild(doc.createCDATASection(description));
                element.appendChild(descriptionElement);
            }
            for (String abbreviation : this.getRemoved()) {
                element = doc.createElement("codetemplate");
                root.appendChild(element);
                element.setAttribute("abbreviation", abbreviation);
                element.setAttribute("remove", Boolean.TRUE.toString());
            }
            return doc;
        }
    }

    private static final class LegacyReader
    extends TemplatesReader {
        private static final String EL_ROOT = "abbrevs";
        private static final String EL_CODETEMPLATE = "abbrev";
        private static final String AL_ABBREV = "key";
        private static final String AL_REMOVE = "remove";
        private Map<String, CodeTemplateDescription> codeTemplatesMap = new HashMap<String, CodeTemplateDescription>();
        private Set<String> removedTemplates = new HashSet<String>();
        private String abbreviation = null;
        private StringBuilder text = null;

        public LegacyReader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        @Override
        public Map<String, CodeTemplateDescription> getAdded() {
            return this.codeTemplatesMap;
        }

        @Override
        public Set<String> getRemoved() {
            return this.removedTemplates;
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            if (this.text != null) {
                this.text.append(ch, start, length);
            }
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (!qName.equals("abbrevs") && qName.equals("abbrev")) {
                boolean removed = Boolean.valueOf(attributes.getValue("remove"));
                if (removed) {
                    String abbrev = attributes.getValue("key");
                    this.removedTemplates.add(abbrev);
                    this.abbreviation = null;
                    this.text = null;
                } else {
                    this.abbreviation = attributes.getValue("key");
                    this.text = new StringBuilder();
                }
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (!qName.equals("abbrevs") && qName.equals("abbrev") && this.abbreviation != null) {
                String parametrizedText = this.text.toString().replaceFirst("([^|]+)[|]([^|]+)", "$1\\${cursor}$2");
                CodeTemplateDescription template = new CodeTemplateDescription(this.abbreviation, null, CharacterConversions.lineSeparatorToLineFeed((CharSequence)parametrizedText), null, null, this.getMimePath());
                this.codeTemplatesMap.put(this.abbreviation, template);
            }
        }
    }

    private static final class Reader
    extends TemplatesReader {
        private Map<String, CodeTemplateDescription> codeTemplatesMap = new HashMap<String, CodeTemplateDescription>();
        private Set<String> removedTemplates = new HashSet<String>();
        private String abbreviation = null;
        private String description = null;
        private String code = null;
        private List<String> contexts = null;
        private String uuid = null;
        private StringBuilder text = null;
        private StringBuilder cdataText = null;
        private boolean insideCdata = false;

        public Reader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        @Override
        public Map<String, CodeTemplateDescription> getAdded() {
            return this.codeTemplatesMap;
        }

        @Override
        public Set<String> getRemoved() {
            return this.removedTemplates;
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            if (this.text != null) {
                this.text.append(ch, start, length);
                if (this.insideCdata) {
                    this.cdataText.append(ch, start, length);
                }
            }
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (!qName.equals("codetemplates")) {
                if (qName.equals("codetemplate")) {
                    boolean removed = Boolean.valueOf(attributes.getValue("remove"));
                    this.abbreviation = null;
                    this.description = null;
                    this.contexts = null;
                    this.uuid = null;
                    this.text = null;
                    this.cdataText = null;
                    if (removed) {
                        String abbrev = attributes.getValue("abbreviation");
                        this.removedTemplates.add(abbrev);
                    } else {
                        String ctxs;
                        String localizedDescription;
                        this.abbreviation = attributes.getValue("abbreviation");
                        this.description = attributes.getValue("descriptionId");
                        if (this.description != null && (localizedDescription = StorageSupport.getLocalizingBundleMessage((FileObject)this.getProcessedFile(), (String)this.description, (String)null)) != null) {
                            this.description = localizedDescription;
                        }
                        if ((ctxs = attributes.getValue("contexts")) != null) {
                            String[] arr = ctxs.split(",");
                            this.contexts = new ArrayList<String>(arr.length);
                            for (String context : arr) {
                                if ((context = context.trim()).length() <= 0) continue;
                                this.contexts.add(context);
                            }
                        } else {
                            this.contexts = null;
                        }
                        this.uuid = attributes.getValue("uuid");
                    }
                } else if (qName.equals("code")) {
                    if (this.abbreviation != null) {
                        this.text = new StringBuilder();
                        this.cdataText = new StringBuilder();
                        this.insideCdata = false;
                    }
                } else if (qName.equals("description") && this.abbreviation != null) {
                    this.text = new StringBuilder();
                    this.cdataText = new StringBuilder();
                    this.insideCdata = false;
                }
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (!qName.equals("codetemplates")) {
                if (qName.equals("codetemplate")) {
                    if (this.abbreviation != null) {
                        CodeTemplateDescription template = new CodeTemplateDescription(this.abbreviation, this.description == null ? null : CharacterConversions.lineSeparatorToLineFeed((CharSequence)this.description), this.code == null ? "" : CharacterConversions.lineSeparatorToLineFeed((CharSequence)this.code), this.contexts, this.uuid, this.getMimePath());
                        this.codeTemplatesMap.put(this.abbreviation, template);
                    }
                } else if (qName.equals("code")) {
                    if (this.text != null) {
                        this.code = this.cdataText.length() > 0 ? this.cdataText.toString() : this.text.toString();
                    }
                } else if (qName.equals("description") && this.text != null) {
                    if (this.cdataText.length() > 0) {
                        this.description = this.cdataText.toString();
                    } else if (this.text.length() > 0) {
                        this.description = this.text.toString();
                    }
                }
            }
        }

        public void startCDATA() throws SAXException {
            if (this.cdataText != null) {
                this.insideCdata = true;
            }
        }

        public void endCDATA() throws SAXException {
            if (this.cdataText != null) {
                this.insideCdata = false;
            }
        }
    }

    private static abstract class TemplatesReader
    extends StorageReader<String, CodeTemplateDescription> {
        protected TemplatesReader(FileObject f, String mimePath) {
            super(f, mimePath);
        }

        public abstract Map<String, CodeTemplateDescription> getAdded();

        public abstract Set<String> getRemoved();
    }

}

