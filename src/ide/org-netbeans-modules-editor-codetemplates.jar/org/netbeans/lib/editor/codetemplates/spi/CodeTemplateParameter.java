/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.spi;

import java.util.Collection;
import java.util.Map;
import org.netbeans.lib.editor.codetemplates.CodeTemplateParameterImpl;

public final class CodeTemplateParameter {
    public static final String CURSOR_PARAMETER_NAME = "cursor";
    public static final String COMPLETION_INVOKE_HINT_NAME = "completionInvoke";
    public static final String SELECTION_PARAMETER_NAME = "selection";
    public static final String NO_FORMAT_PARAMETER_NAME = "no-format";
    public static final String NO_INDENT_PARAMETER_NAME = "no-indent";
    public static final String LINE_HINT_NAME = "line";
    public static final String DEFAULT_VALUE_HINT_NAME = "default";
    public static final String EDITABLE_HINT_NAME = "editable";
    private final CodeTemplateParameterImpl impl;

    CodeTemplateParameter(CodeTemplateParameterImpl impl) {
        this.impl = impl;
    }

    public String getName() {
        return this.impl.getName();
    }

    public String getValue() {
        return this.impl.getValue();
    }

    public void setValue(String newValue) {
        this.impl.setValue(newValue);
    }

    public boolean isEditable() {
        return this.impl.isEditable();
    }

    public boolean isUserModified() {
        return this.impl.isUserModified();
    }

    public int getInsertTextOffset() {
        return this.impl.getInsertTextOffset();
    }

    public int getParametrizedTextStartOffset() {
        return this.impl.getParametrizedTextStartOffset();
    }

    public int getParametrizedTextEndOffset() {
        return this.impl.getParametrizedTextEndOffset();
    }

    public Map<String, String> getHints() {
        return this.impl.getHints();
    }

    public CodeTemplateParameter getMaster() {
        return this.impl.getMaster();
    }

    public Collection<? extends CodeTemplateParameter> getSlaves() {
        return this.impl.getSlaves();
    }

    public boolean isSlave() {
        return this.impl.isSlave();
    }

    CodeTemplateParameterImpl getImpl() {
        return this.impl;
    }
}

