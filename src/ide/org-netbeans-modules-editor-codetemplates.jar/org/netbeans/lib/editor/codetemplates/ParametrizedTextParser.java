/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.lib.editor.util.swing.PositionRegion
 */
package org.netbeans.lib.editor.codetemplates;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.text.Position;
import org.netbeans.lib.editor.codetemplates.CodeTemplateInsertHandler;
import org.netbeans.lib.editor.codetemplates.CodeTemplateParameterImpl;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;
import org.netbeans.lib.editor.util.swing.PositionRegion;

public final class ParametrizedTextParser {
    private final CodeTemplateInsertHandler handler;
    private final String parametrizedText;
    private List<CodeTemplateParameterImpl> paramImpls;
    private List<String> parametrizedTextFragments;

    public ParametrizedTextParser(CodeTemplateInsertHandler handler, String parametrizedText) {
        this.handler = handler;
        this.parametrizedText = parametrizedText;
        if (handler == null) {
            this.paramImpls = new ArrayList<CodeTemplateParameterImpl>();
        }
    }

    public void parse() {
        this.parametrizedTextFragments = new ArrayList<String>();
        StringBuffer textFrag = new StringBuffer();
        int copyStartIndex = 0;
        int index = 0;
        boolean atEOT = false;
        while (!atEOT) {
            int dollarIndex = this.parametrizedText.indexOf(36, index);
            if (dollarIndex != -1 && dollarIndex < this.parametrizedText.length() - 1) {
                switch (this.parametrizedText.charAt(dollarIndex + 1)) {
                    case '{': {
                        textFrag.append(this.parametrizedText.substring(copyStartIndex, dollarIndex));
                        copyStartIndex = dollarIndex;
                        this.parametrizedTextFragments.add(textFrag.toString());
                        textFrag.setLength(0);
                        CodeTemplateParameterImpl paramImpl = new CodeTemplateParameterImpl(this.handler, this.parametrizedText, dollarIndex);
                        int afterClosingBraceIndex = paramImpl.getParametrizedTextEndOffset();
                        if (afterClosingBraceIndex <= this.parametrizedText.length()) {
                            if (this.handler != null) {
                                this.handler.notifyParameterParsed(paramImpl);
                            } else {
                                for (CodeTemplateParameterImpl impl : this.paramImpls) {
                                    if (!impl.getName().equals(paramImpl.getName())) continue;
                                    paramImpl.markSlave(impl.getParameter());
                                    break;
                                }
                                this.paramImpls.add(paramImpl);
                            }
                            copyStartIndex = index = afterClosingBraceIndex;
                            break;
                        }
                        atEOT = true;
                        break;
                    }
                    case '$': {
                        textFrag.append(this.parametrizedText.substring(copyStartIndex, dollarIndex + 1));
                        copyStartIndex = index = dollarIndex + 2;
                        break;
                    }
                    default: {
                        index = dollarIndex + 1;
                        break;
                    }
                }
                continue;
            }
            textFrag.append(this.parametrizedText.substring(copyStartIndex));
            this.parametrizedTextFragments.add(textFrag.toString());
            atEOT = true;
        }
    }

    public String buildInsertText(List allParameters) {
        StringBuffer insertTextBuffer = new StringBuffer(this.parametrizedText.length());
        insertTextBuffer.append(this.parametrizedTextFragments.get(0));
        int fragIndex = 1;
        Iterator it = allParameters.iterator();
        while (it.hasNext()) {
            CodeTemplateParameterImpl param = CodeTemplateParameterImpl.get((CodeTemplateParameter)it.next());
            int startOffset = insertTextBuffer.length();
            insertTextBuffer.append(param.getValue());
            param.resetPositions(PositionRegion.createFixedPosition((int)startOffset), PositionRegion.createFixedPosition((int)insertTextBuffer.length()));
            insertTextBuffer.append(this.parametrizedTextFragments.get(fragIndex));
            ++fragIndex;
        }
        return insertTextBuffer.toString();
    }

    public static StringBuffer parseToHtml(StringBuffer sb, String parametrizedText) {
        ParametrizedTextParser parser = new ParametrizedTextParser(null, parametrizedText);
        parser.parse();
        parser.appendHtmlText(sb);
        return sb;
    }

    public static String toHtmlText(String text) {
        StringBuffer htmlText = null;
        for (int i = 0; i < text.length(); ++i) {
            String rep;
            char ch = text.charAt(i);
            switch (ch) {
                case '<': {
                    rep = "&lt;";
                    break;
                }
                case '>': {
                    rep = "&gt;";
                    break;
                }
                case '\n': {
                    rep = "<br>";
                    break;
                }
                default: {
                    rep = null;
                }
            }
            if (rep != null) {
                if (htmlText == null) {
                    htmlText = new StringBuffer(120 * text.length() / 100);
                    if (i > 0) {
                        htmlText.append(text.substring(0, i));
                    }
                }
                htmlText.append(rep);
                continue;
            }
            if (htmlText == null) continue;
            htmlText.append(ch);
        }
        return htmlText != null ? htmlText.toString() : text;
    }

    private void appendHtmlText(StringBuffer htmlTextBuffer) {
        htmlTextBuffer.append(ParametrizedTextParser.toHtmlText(this.parametrizedTextFragments.get(0)));
        int fragIndex = 1;
        for (CodeTemplateParameterImpl paramImpl : this.paramImpls) {
            htmlTextBuffer.append("<b>");
            if ("cursor".equals(paramImpl.getName())) {
                htmlTextBuffer.append("|");
            } else {
                htmlTextBuffer.append(ParametrizedTextParser.toHtmlText(paramImpl.getValue()));
            }
            htmlTextBuffer.append("</b>");
            htmlTextBuffer.append(ParametrizedTextParser.toHtmlText(this.parametrizedTextFragments.get(fragIndex)));
            ++fragIndex;
        }
    }
}

