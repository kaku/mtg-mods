/*
 * Decompiled with CFR 0_118.
 */
package org.netbeans.lib.editor.codetemplates.spi;

import java.util.List;
import javax.swing.text.JTextComponent;
import org.netbeans.lib.editor.codetemplates.CodeTemplateInsertHandler;
import org.netbeans.lib.editor.codetemplates.CodeTemplateParameterImpl;
import org.netbeans.lib.editor.codetemplates.CodeTemplateSpiPackageAccessor;
import org.netbeans.lib.editor.codetemplates.api.CodeTemplate;
import org.netbeans.lib.editor.codetemplates.spi.CodeTemplateParameter;

public final class CodeTemplateInsertRequest {
    private final CodeTemplateInsertHandler handler;

    CodeTemplateInsertRequest(CodeTemplateInsertHandler handler) {
        this.handler = handler;
    }

    public CodeTemplate getCodeTemplate() {
        return this.handler.getCodeTemplate();
    }

    public JTextComponent getComponent() {
        return this.handler.getComponent();
    }

    public List<? extends CodeTemplateParameter> getMasterParameters() {
        return this.handler.getMasterParameters();
    }

    public CodeTemplateParameter getMasterParameter(String name) {
        for (CodeTemplateParameter master : this.getMasterParameters()) {
            if (!name.equals(master.getName())) continue;
            return master;
        }
        return null;
    }

    public List<? extends CodeTemplateParameter> getAllParameters() {
        return this.handler.getAllParameters();
    }

    public boolean isInserted() {
        return this.handler.isInserted();
    }

    public boolean isReleased() {
        return this.handler.isReleased();
    }

    public String getParametrizedText() {
        return this.handler.getParametrizedText();
    }

    public void setParametrizedText(String parametrizedText) {
        this.handler.setParametrizedText(parametrizedText);
    }

    public String getInsertText() {
        return this.handler.getInsertText();
    }

    public int getInsertTextOffset() {
        return this.handler.getInsertOffset();
    }

    static {
        CodeTemplateSpiPackageAccessor.register(new SpiAccessor());
    }

    private static final class SpiAccessor
    extends CodeTemplateSpiPackageAccessor {
        private SpiAccessor() {
        }

        @Override
        public CodeTemplateInsertRequest createInsertRequest(CodeTemplateInsertHandler handler) {
            return new CodeTemplateInsertRequest(handler);
        }

        @Override
        public CodeTemplateParameter createParameter(CodeTemplateParameterImpl impl) {
            return new CodeTemplateParameter(impl);
        }

        @Override
        public CodeTemplateParameterImpl getImpl(CodeTemplateParameter parameter) {
            return parameter.getImpl();
        }
    }

}

