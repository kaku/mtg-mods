#!/usr/bin/env fish
set _root (dirname (status -f))
set module_dirs (find $_root -maxdepth 2 -type d -name modules)
set src_dir $_root/src

if [ ! -d $src_dir ]
    mkdir $src_dir
end

for dir in $module_dirs
    set_color red 
    set cluster_name (basename (dirname $dir))
    set cluster_src $src_dir/$cluster_name

    if [ ! -d $cluster_src ]
        mkdir $cluster_src
    end

    for jar in $dir/*.jar
        set_color red 
        echo "cluster $cluster_name, module "(basename $jar)
        if [ ! -d $cluster_src/(basename $jar) ]
            set_color grey
            cfr_0_118.jar $jar --outputdir $cluster_src/(basename $jar)
        end
    end
end

set_color normal
